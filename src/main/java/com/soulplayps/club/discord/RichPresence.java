package com.soulplayps.club.discord;

import com.soulplayps.club.discord.rpc.DiscordEventHandlers;
import com.soulplayps.club.discord.rpc.DiscordRPC;
import com.soulplayps.club.discord.rpc.DiscordRichPresence;
import com.sun.jna.Native;


public class RichPresence {

	private final String CLIENT_ID = "841084931056730193";

    private DiscordRPC lib = null;
    private DiscordRichPresence presence = null;

    public void initiate() {
    	
    	try {
    		lib = Native.load("discord-rpc", DiscordRPC.class);//DiscordRPC.INSTANCE;
    	} catch (UnsatisfiedLinkError e) {
    		lib = null;
    		e.printStackTrace();
    		System.err.println("Unable to load Discord RPC (on loadLibrary). Continuing without Rich Presence.");
    		return;
    	} catch (Exception e) {
    		lib = null;
    		e.printStackTrace();
    		System.err.println("Unable to load Discord RPC (on loadLibrary). Continuing without Rich Presence.");
    		return;
    	}
    	
    	DiscordEventHandlers handlers = new DiscordEventHandlers();
    	
    	try {
    		lib.Discord_Initialize(CLIENT_ID, handlers, true, "");
    	} catch (Error e) {
    		lib = null;
    		e.printStackTrace();
    		System.err.println("Unable to load Discord RPC (on initialize). Continuing without Rich Presence.");
    		return;
    	} catch (Exception e) {
    		lib = null;
    		e.printStackTrace();
    		System.err.println("Unable to load Discord RPC (on initialize). Continuing without Rich Presence.");
    		return;
    	}
    	
        presence = new DiscordRichPresence();
        presence.startTimestamp = System.currentTimeMillis() / 1000;
        presence.largeImageKey = "splogo2";
        updatePresence();
        
        System.out.println("Discord RPC successfully loaded.");
        
//        new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//
//	            while (!Thread.currentThread().isInterrupted()) {
//	                lib.Discord_RunCallbacks();
//	                try {
//	                    Thread.sleep(2000);
//	                } catch (InterruptedException ignored) {}
//	            }
//	        
//				
//			}
//		}, "RPC-Callback-Handler").start();
        
    }

    public boolean presenceIsNull() {
        return presence == null;
    }

    public void updateDetails(String details) {
        presence.details = details;
        updatePresence();
    }

    public void updateState(String state) {
        presence.state = state;
        updatePresence();
    }

    public void updateSmallImageKey(String key) {
        presence.smallImageKey = key;
        updatePresence();
    }
    
    public void updateRaidsLobby(int playersInside, int maxCount) {
    	presence.details = "Chambers of Xeric";
    	presence.partySize = playersInside;
    	presence.partyMax = maxCount;
        updatePresence();
    }
    
    public void updateDmmPkLobby(int playersInside, int maxCount) {
    	presence.partySize = playersInside;
    	presence.partyMax = maxCount;
    	presence.state = "In Lobby";
    	presence.details = "PK Tournament";
    	presence.startTimestamp = System.currentTimeMillis();
    	presence.endTimestamp = System.currentTimeMillis() + 3000000;
        updatePresence();
    }
    
    public void updateDiscord(String state, String details, int partySize, int maxPartySize, long startTimestamp, long endTimestamp, String smallImageKey) {
    	presence.state = state;
    	presence.details = details;
    	if (partySize > -1)
    		presence.partySize = partySize;
    	if (maxPartySize > -1)
    		presence.partyMax = maxPartySize;
    	if (startTimestamp >= 0)
    		presence.startTimestamp = startTimestamp;
    	if (endTimestamp >= 0)
    		presence.endTimestamp = endTimestamp;
    	presence.smallImageKey = smallImageKey;

        updatePresence();
    }

    private void updatePresence() {
        lib.Discord_UpdatePresence(presence);
    }
    
    public DiscordRPC getLibrary() {
    	return lib;
    }
    
}
