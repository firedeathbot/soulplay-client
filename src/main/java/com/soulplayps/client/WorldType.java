package com.soulplayps.client;

public enum WorldType {
	ECONOMY(0),
	SPAWN(1),
	SEASONAL(2);

	public static final WorldType[] values = values();
	private final int id;

	private WorldType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static WorldType list(int id) {
		for (WorldType type : values) {
			if (type.getId() == id) {
				return type;
			}
		}

		return ECONOMY;
	}

}
