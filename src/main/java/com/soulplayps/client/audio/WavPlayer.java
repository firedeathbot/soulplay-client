package com.soulplayps.client.audio;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.fs.RSFileStore;
import com.soulplayps.client.settings.Settings;
import com.soulplayps.client.util.CompressionUtils;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class WavPlayer {

    private static final int SOUND_EFFECT_IDX = 8;

    private static final Map<Integer, byte[]> cache = new HashMap<>();

    public static void play(int id, int delay) {
        if (SOUND_EFFECT_IDX >= Client.instance.fileStores.length) {
            return;
        }

        final int volume = Settings.getOrDefault("sound_effect_volume", -2);

        if (volume <= -2 || volume > 2) {
            return;
        }

        byte[] data = cache.get(id);

        if (data == null) {
            try {
                RSFileStore store = Client.instance.fileStores[SOUND_EFFECT_IDX];
                if (store == null) {
                    return;
                }

                byte[] gzippedData = store.readFile(id);

                if (Config.USE_UPDATE_SERVER) {//TODO fix this
                    if (gzippedData == null || gzippedData.length <= 0) {
                        gzippedData = Client.instance.fileStores[SOUND_EFFECT_IDX].readFile(id);
                    }
                }

                if (gzippedData == null || gzippedData.length <= 0) {
                    return;
                }

                playClip(CompressionUtils.degzip(ByteBuffer.wrap(gzippedData)), delay, volume);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            cache.put(id, data);
            playClip(data, delay, volume);
        }
    }

    private static void playClip(final byte[] data, final int delay, final int volume) {
        if (data == null || data.length <= 0) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    if (delay > 0) {
                        Thread.sleep(delay);
                    }

                    AudioInputStream ais = AudioSystem.getAudioInputStream(new ByteArrayInputStream(data));
                    AudioFormat format = ais.getFormat();
                    DataLine.Info info = new DataLine.Info(Clip.class, format);
                    Clip clip = (Clip) AudioSystem.getLine(info);
                    clip.open(ais);

                     FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

                    int volumeLevel = volume;

                    // volume levels get multiplied by a factor of 10
                     if (volumeLevel > 3) {
                         volumeLevel = 3;
                     }

                     if (volumeLevel < -3) {
                         volumeLevel = -3;
                     }

                     gainControl.setValue(limit(gainControl, volumeLevel * 10)); // unit is in decibels (max 30 decibel, min -30 decibel)

                    clip.start();

                    long duration = (long)((double)ais.getFrameLength() / format.getFrameRate()) * 1000;

                    // this makes sure short lived sounds effects play
                    if (duration < 2000) {
                        duration = 2000;
                    }

                    Thread.sleep(duration); // sleep for duration of wav in milliseconds

                    // cleanup so no resource leaks occur
                    clip.close();
                    ais.close();
                } catch (LineUnavailableException | UnsupportedAudioFileException | IOException | InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    private static float limit(FloatControl control,float level)
    { return Math.min(control.getMaximum(), Math.max(control.getMinimum(), level)); }

    public static void clear() {
        cache.clear();
    }

}
