package com.soulplayps.client;

import com.soulplayps.client.Client;
import com.soulplayps.client.login.LoginRenderer;
import com.soulplayps.client.login.TitleState;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;

public class Ping {

	public static void getPing(String hostAddress, int port) {
		String usage = "java Probe <address> [<port>]";
		long timeToRespond = 0; // in milliseconds


		try {
			if (port == 0)
				port = 80;

			if (port == 0)
				Client.ping = timeToRespond = test(hostAddress);
			else
				Client.ping = timeToRespond = test(hostAddress, port);
		} catch (NumberFormatException e) {
			System.out.println("Problem with arguments, usage: " + usage);
			e.printStackTrace();
		}
		
//		if (timeToRespond <= 0 || timeToRespond > 200) {
//			JOptionPane.showMessageDialog(null, "LAG!!!!!");
//		}

		if (timeToRespond >= 0)
			Client.worldOnline = true;
		else
			Client.worldOnline = false;
		
//		if (timeToRespond >= 0)
//			System.out.println(hostAddress + " responded in " + timeToRespond + " ms");
//		else
//			System.out.println("Failed");

	}
	
	public static void runPing() {

		Thread t = new Thread(new Runnable() {

			//@Override
			public void run() {
				while (true) {
					getPing(Client.server, Client.port);

					if (Client.calendar != null) {
						Client.calendar.refreshCalendar();
					}

					if (!Client.loggedIn && LoginRenderer.titleState == TitleState.WORLD_LIST) {
						Client.instance.loginRenderer.reloadWorldCount();
					}
					try {
						Thread.sleep(2000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
			}
		}, "Ping thread");
//		t.setPriority(10);

			t.start();
	}

	public static final String playersOnline(int world) {
		final BufferedReader reader;
		try {
			reader = new BufferedReader(
					new InputStreamReader(new URL("https://www.soulplayps.com/excludecf/" +
							"clientdata/playersize.php?world=" + world).openStream()));
		} catch (IOException e) {
			e.printStackTrace();
			return "0";
		}
		try {
			return reader.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "0";
	}

	/**
	 * Connect using layer3
	 * 
	 * @param hostAddress
	 * @return delay if the specified host responded, -1 if failed
	 */
	static long test(String hostAddress) {
		InetAddress inetAddress = null;

		try {
			inetAddress = InetAddress.getByName(hostAddress);
		} catch (UnknownHostException e) {
			System.out.println("Problem, unknown host:");
			e.printStackTrace();
			return -1;
		}

		Date start, stop;
		try {
			start = new Date();
			if (inetAddress.isReachable(5000)) {
				stop = new Date();
				return (stop.getTime() - start.getTime());
			}

		} catch (IOException e1) {
			System.out.println("Problem, a network error has occurred:");
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			System.out.println("Problem, timeout was invalid:");
			e1.printStackTrace();
		}

		return -1; // to indicate failure

	}

	/**
	 * Connect using layer4 (sockets)
	 * 
	 * @param
	 * @return delay if the specified host responded, -1 if failed
	 */
	static long test(String hostAddress, int port) {
		InetAddress inetAddress = null;
		InetSocketAddress socketAddress = null;
		Socket sc = null;
		long timeToRespond = -1;
		long start, stop;

		try {
			inetAddress = InetAddress.getByName(hostAddress);
		} catch (UnknownHostException e) {
			System.out.println("Problem, unknown host:");
			e.printStackTrace();
			return -1;
		}

		try {
			socketAddress = new InetSocketAddress(inetAddress, port);
		} catch (IllegalArgumentException e) {
			System.out.println("Problem, port may be invalid:");
			e.printStackTrace();
			return -1;
		}

		// Open the channel, set it to non-blocking, initiate connect
		try {
			sc = new Socket();//SocketChannel.open();
			//sc.configureBlocking(true);
			start = System.currentTimeMillis();
			sc.connect(socketAddress, 2000);
			stop = System.currentTimeMillis();
			timeToRespond = (stop - start);
		} catch (IOException e) {
//			JOptionPane.showMessageDialog(null, "PING CRASHED!!!!!");
			//client.displayPing = false;
			//System.out.println("Problem, connection could not be made:");
			//e.printStackTrace();
			
//			client.worldOnline = false;
		}

		try {
			sc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

//		System.out.println("timeToreSpond "+timeToRespond);
//		if (timeToRespond > -1)
//			client.worldOnline = true;
//		else 
//			client.worldOnline = false;
		return timeToRespond;
	}

}