package com.soulplayps.client;

public enum PlayerRenderPriority {

	PRIORITIZED_PLAYER,
	PLAYER_OTHER,
	PRIORITIZED_PLAYER_OTHER,
	SELF_PLAYER;

}
