package com.soulplayps.client.node;

public class JavaObject extends NodeSub {
	
	private final Object value;
	
	public JavaObject(Object value) {
		this.value = value;
	}
	
	public Object get() {
		return value;
	}

}
