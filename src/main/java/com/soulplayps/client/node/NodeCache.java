package com.soulplayps.client.node;

public final class NodeCache {

	public NodeCache(int i) {
		size = i;
		cache = new Node[i];
		for (int k = 0; k < i; k++) {
			Node node = cache[k] = new Node();
			node.prev = node;
			node.next = node;
		}

	}

	public Node findNodeByID(long l) {
		Node node = cache[(int) (l & size - 1)];
		for (Node node_1 = node.prev; node_1 != node; node_1 = node_1.prev) {
			if (node_1.id == l) {
				return node_1;
			}
		}

		return null;
	}

	public void removeFromCache(Node node, long l) {
		if (node.next != null)
			node.unlink();
		Node node_1 = cache[(int) (l & size - 1)];
		node.next = node_1.next;
		node.prev = node_1;
		node.next.prev = node;
		node.prev.next = node;
		node.id = l;
	}

	public void clear() {
		for (int id = 0; id < size; id++) {
			Node currentNode = cache[id];
			for (;;) {
				Node nextNode = currentNode.prev;
				if (nextNode == currentNode) {
					break;
				}

				nextNode.unlink();
			}
		}
	}

	private final int size;
	private final Node[] cache;
}
