package com.soulplayps.client.node;

public final class ObjectCache {

	public ObjectCache(int i) {
		defaultCapacity = i;
		capacity = i;
		int size = 1;
		while (size + size < i) {
			size += size;
		}

		hashtable = new NodeCache(size);
	}

	public Object get(long uid) {
		JavaObject node = (JavaObject) hashtable.findNodeByID(uid);
		if (node == null) {
			return null;
		}

		Object value = node.get();
		if (value == null) {
			node.unlink();
			node.unlinkSub();
			capacity++;
			return null;
		}

		recentlyUsed.insertHead(node);
		return value;
	}
	
	public void remove(long uid) {
		JavaObject node = (JavaObject) hashtable.findNodeByID(uid);
		if (node != null) {
			node.unlink();
			node.unlinkSub();
			capacity++;
		}
	}

	public void put(Object value, long uid) {
		remove(uid);
		if (capacity != 0) {
			capacity--;
		} else {
			JavaObject firstNode = (JavaObject) recentlyUsed.removeFirst();
			if (firstNode != null) {
				firstNode.unlink();
				firstNode.unlinkSub();
			}
		}

		JavaObject newNode = new JavaObject(value);
		hashtable.removeFromCache(newNode, uid);
		recentlyUsed.insertHead(newNode);
	}

	public void clear() {
		recentlyUsed.clear();
		hashtable.clear();
		capacity = defaultCapacity;
	}

	private final int defaultCapacity;
	public int capacity;
	private final NodeCache hashtable;
	private final NodeSubList recentlyUsed = new NodeSubList();
}
