package com.soulplayps.client.node;

import com.soulplayps.client.node.object.*;
import com.soulplayps.client.unknown.*;

public final class Ground extends Node {

	public Ground(int i, int j, int k) {
		obj5Array = new Object5[5];
		anIntArray1319 = new int[5];
		groundHeight = anInt1307 = i;
		anInt1308 = j;
		anInt1309 = k;
	}

	public int anInt1307;
	public final int anInt1308;
	public final int anInt1309;
	public final int groundHeight;
	public Class43 aClass43_1311;
	public Class40 aClass40_1312;
	public Object1 obj1;
	public Object2 obj2;
	public Object3 obj3;
	public Object4 obj4;
	public int anInt1317;
	public final Object5[] obj5Array;
	public final int[] anIntArray1319;
	public int anInt1320;
	public int anInt1321;
	public boolean aBoolean1322;
	public boolean aBoolean1323;
	public boolean aBoolean1324;
	public int anInt1325;
	public int anInt1326;
	public int anInt1327;
	public int anInt1328;
	public Ground aClass30_Sub3_1329;
}
