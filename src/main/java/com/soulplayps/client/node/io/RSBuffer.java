package com.soulplayps.client.node.io;

import com.soulplayps.client.util.ISAACRandomGen;
import com.soulplayps.client.util.Misc;
import com.soulplayps.client.util.SizeConstants;

import java.math.BigInteger;

import com.soulplayps.client.music.Class51;
import com.soulplayps.client.node.NodeList;
import com.soulplayps.client.node.NodeSub;
import com.soulplayps.client.unknown.*;

public final class RSBuffer extends NodeSub {

	public static RSBuffer create() {
		synchronized (nodeList) {
			RSBuffer stream = null;
			if (anInt1412 > 0) {
				anInt1412--;
				stream = (RSBuffer) nodeList.popHead();
			}
			if (stream != null) {
				stream.currentOffset = 0;
				return stream;
			}
		}
		RSBuffer stream_1 = new RSBuffer();
		stream_1.currentOffset = 0;
		stream_1.buffer = new byte[FIVE_THOUSAND];
		return stream_1;
	}

	public RSBuffer() {
	}

	public byte[] getData(byte[] buffer2) {
		for (int i = 0; i < buffer2.length; i++)
			buffer2[i] = buffer[currentOffset++];
		return buffer2;
	}

	public RSBuffer(byte abyte0[]) {
		buffer = abyte0;
		currentOffset = 0;
	}
	
	public void createFrameProxy(RSBuffer stream, int i) {
		Misc.createFrameProxyToHere(stream, i);
	}

	public void sendPacket(int i) {
//		buffer[currentOffset++] = (byte) (i + encryption.getNextKey());
		createFrameProxy(this, i);
	}

	public int readUnsignedWord2() {
		return 1795;
	}

	public int readUnsignedByte2() {
		return -263;
	}

	public int g2() {
		currentOffset += TWO;
		return ((buffer[currentOffset - TWO] & 0xff) << EIGHT)
				+ (buffer[currentOffset - ONE] & 0xff);
	}

	public int g4() {
		currentOffset += FOUR;
		return ((buffer[currentOffset - FOUR] & 0xff) << TWENTYFOUR)
				+ ((buffer[currentOffset - THREE] & 0xff) << SIXTEEN)
				+ ((buffer[currentOffset - TWO] & 0xff) << EIGHT)
				+ (buffer[currentOffset - ONE] & 0xff);
	}

	public int readUSmart2() {
		int baseVal = 0;
		int lastVal = 0;
		while ((lastVal = readUSmart()) == 32767) {
			baseVal += 32767;
		}
		return baseVal + lastVal;
	}

	public String readNewString() {
		int i = currentOffset;
		while (buffer[currentOffset++] != 0)
			;
		return new String(buffer, i, currentOffset - i - ONE);
	}

	public void writeWord(int i) {
		buffer[currentOffset++] = (byte) (i >> EIGHT);
		buffer[currentOffset++] = (byte) i;
	}

	public void method400(int i) {
		buffer[currentOffset++] = (byte) i;
		buffer[currentOffset++] = (byte) (i >> EIGHT);
	}

	public void write3Bytes(int i) {
		buffer[currentOffset++] = (byte) (i >> SIXTEEN);
		buffer[currentOffset++] = (byte) (i >> EIGHT);
		buffer[currentOffset++] = (byte) i;
	}

	public void putCrc32(int offset) {
		int crc = -1;
		for (int id = offset; id < currentOffset; id++) {
			crc = CRC_32[(crc ^ buffer[id]) & 0xff] ^ crc >>> 8;
		}
		crc ^= 0xffffffff;
		writeDWord(crc);
	}

	public void writeDWord(int i) {
		buffer[currentOffset++] = (byte) (i >> TWENTYFOUR);
		buffer[currentOffset++] = (byte) (i >> SIXTEEN);
		buffer[currentOffset++] = (byte) (i >> EIGHT);
		buffer[currentOffset++] = (byte) i;
	}

	public void method403(int j) {
		buffer[currentOffset++] = (byte) j;
		buffer[currentOffset++] = (byte) (j >> EIGHT);
		buffer[currentOffset++] = (byte) (j >> SIXTEEN);
		buffer[currentOffset++] = (byte) (j >> TWENTYFOUR);
	}

	public void writeQWord(long l) {
		buffer[currentOffset++] = (byte) (int) (l >> FIVESIX);
		buffer[currentOffset++] = (byte) (int) (l >> FOUREIGHT);
		buffer[currentOffset++] = (byte) (int) (l >> FOURZERO);
		buffer[currentOffset++] = (byte) (int) (l >> THREETWO);
		buffer[currentOffset++] = (byte) (int) (l >> TWENTYFOUR);
		buffer[currentOffset++] = (byte) (int) (l >> SIXTEEN);
		buffer[currentOffset++] = (byte) (int) (l >> EIGHT);
		buffer[currentOffset++] = (byte) (int) l;
	}

	public void writeString(String s) {
		// s.getBytes(0, s.length(), buffer, currentOffset); //deprecated
		System.arraycopy(s.getBytes(), 0, buffer, currentOffset, s.length());
		currentOffset += s.length();
		buffer[currentOffset++] = 10;
	}

	public void writeBytes(byte abyte0[], int i, int j) {
		for (int k = j; k < j + i; k++)
			buffer[currentOffset++] = abyte0[k];
	}

	public void writeByte(int i) {
		buffer[currentOffset++] = (byte) i;
	}

	public void writeBytes(int i) {
		buffer[currentOffset - i - ONE] = (byte) i;
	}

	public void writeShortAt(int i) {
		buffer[currentOffset - i - TWO] = (byte) (i >> EIGHT);
		buffer[currentOffset - i - ONE] = (byte) i;
	}

	public int readUnsignedByte() {
		return buffer[currentOffset++] & 0xff;
	}

	public byte readSignedByte() {
		return buffer[currentOffset++];
	}

	public int readUnsignedWord() {
		try {
			currentOffset += TWO;
			return ((buffer[currentOffset - TWO] & 0xff) << EIGHT)
					+ (buffer[currentOffset - ONE] & 0xff);
		} catch (Exception e) {
			return readUnsignedWord2();
		}
	}

	public int readSignedWord() {
		currentOffset += TWO;
		int i = ((buffer[currentOffset - TWO] & 0xff) << EIGHT)
				+ (buffer[currentOffset - ONE] & 0xff);
		if (i > 32767)
			i -= 0x10000;
		return i;
	}

	public int read3Bytes() {
		currentOffset += THREE;
		return ((buffer[currentOffset - THREE] & 0xff) << SIXTEEN)
				+ ((buffer[currentOffset - TWO] & 0xff) << EIGHT)
				+ (buffer[currentOffset - ONE] & 0xff);
	}

	public int readDWord() {
		currentOffset += FOUR;
		return ((buffer[currentOffset - FOUR] & 0xff) << TWENTYFOUR)
				+ ((buffer[currentOffset - THREE] & 0xff) << SIXTEEN)
				+ ((buffer[currentOffset - TWO] & 0xff) << EIGHT)
				+ (buffer[currentOffset - ONE] & 0xff);
	}

	public long readQWord() {
		long l = readDWord() & 0xffffffffL;
		long l1 = readDWord() & 0xffffffffL;
		return (l << THREETWO) + l1;
	}

	public String readString() {
		int i = currentOffset;
		while (buffer[currentOffset++] != 10)
			;
		return new String(buffer, i, currentOffset - i - ONE);
	}

	public byte[] readBytes() {
		int i = currentOffset;
		while (buffer[currentOffset++] != 10)
			;
		byte abyte0[] = new byte[currentOffset - i - ONE];
		System.arraycopy(buffer, i, abyte0, i - i, currentOffset - ONE - i);
		return abyte0;
	}

	public void readBytes(int i, int j, byte abyte0[]) {
		for (int l = j; l < j + i; l++)
			abyte0[l] = buffer[currentOffset++];
	}

	public void initBitAccess() {
		bitPosition = currentOffset * EIGHT;
	}

	public int readBits(int i) {
		int k = bitPosition >> THREE;
		int l = EIGHT - (bitPosition & 7);
		int i1 = 0;
		bitPosition += i;
		for (; i > l; l = EIGHT) {
			i1 += (buffer[k++] & anIntArray1409[l]) << i - l;
			i -= l;
		}
		if (i == l)
			i1 += buffer[k] & anIntArray1409[l];
		else
			i1 += buffer[k] >> l - i & anIntArray1409[i];
		return i1;
	}

	public void finishBitAccess() {
		currentOffset = (bitPosition + 7) / EIGHT;
	}

	public int method421() {
		try {
			int i = buffer[currentOffset] & 0xff;
			if (i < HUNTWENTYEIGHT)
				return readUnsignedByte() - SIXFOUR;
			else
				return readUnsignedWord() - 49152;
		} catch (Exception e) {
			return -1;
		}
	}

	public int readUSmart() {
		int i = buffer[currentOffset] & 0xff;
		if (i < HUNTWENTYEIGHT)
			return readUnsignedByte();
		else
			return readUnsignedWord() - 32768;
	}
	
	public void proxyKeys(RSBuffer stream) {
		Misc.processProxyKeys(stream);
	}

	public void doKeys() {
		int i = currentOffset;
		currentOffset = 0;
		byte abyte0[] = new byte[i];
		readBytes(i, 0, abyte0);
		BigInteger biginteger2 = new BigInteger(abyte0).modPow(Class51.RSA_EXPONENT, SizeConstants.getRsaModulus());
		byte abyte1[] = biginteger2.toByteArray();
		currentOffset = 0;
		writeByte(abyte1.length);
		writeBytes(abyte1, abyte1.length, 0);
	}

	public void proxyKeyFake() {
		Class43.doKeysFake(this);
	}

	public void method424(int i) {
		buffer[currentOffset++] = (byte) (-i);
	}

	public void method425(int j) {
		buffer[currentOffset++] = (byte) (HUNTWENTYEIGHT - j);
	}

	public int method426() {
		return buffer[currentOffset++] - HUNTWENTYEIGHT & 0xff;
	}

	public int method427() {
		return -buffer[currentOffset++] & 0xff;
	}

	public int method428() {
		return HUNTWENTYEIGHT - buffer[currentOffset++] & 0xff;
	}

	public byte method429() {
		return (byte) (-buffer[currentOffset++]);
	}

	public byte method430() {
		return (byte) (HUNTWENTYEIGHT - buffer[currentOffset++]);
	}

	public void method431(int i) {
		buffer[currentOffset++] = (byte) i;
		buffer[currentOffset++] = (byte) (i >> EIGHT);
	}

	public void method432(int j) {
		buffer[currentOffset++] = (byte) (j >> EIGHT);
		buffer[currentOffset++] = (byte) (j + HUNTWENTYEIGHT);
	}

	public void method433(int j) {
		buffer[currentOffset++] = (byte) (j + HUNTWENTYEIGHT);
		buffer[currentOffset++] = (byte) (j >> EIGHT);
	}

	public int method434() {
		currentOffset += TWO;
		return ((buffer[currentOffset - ONE] & 0xff) << EIGHT)
				+ (buffer[currentOffset - TWO] & 0xff);
	}

	public int method435() {
		currentOffset += TWO;
		return ((buffer[currentOffset - TWO] & 0xff) << EIGHT)
				+ (buffer[currentOffset - ONE] - HUNTWENTYEIGHT & 0xff);
	}

	public int method436() {
		currentOffset += TWO;
		return ((buffer[currentOffset - ONE] & 0xff) << EIGHT)
				+ (buffer[currentOffset - TWO] - HUNTWENTYEIGHT & 0xff);
	}

	public int method437() {
		currentOffset += TWO;
		int j = ((buffer[currentOffset - ONE] & 0xff) << EIGHT)
				+ (buffer[currentOffset - TWO] & 0xff);
		if (j > 32767)
			j -= 0x10000;
		return j;
	}

	public int method438() {
		currentOffset += TWO;
		int j = ((buffer[currentOffset - ONE] & 0xff) << EIGHT)
				+ (buffer[currentOffset - TWO] - HUNTWENTYEIGHT & 0xff);
		if (j > 32767)
			j -= 0x10000;
		return j;
	}

	public int method439() {
		currentOffset += FOUR;
		return ((buffer[currentOffset - TWO] & 0xff) << TWENTYFOUR)
				+ ((buffer[currentOffset - ONE] & 0xff) << SIXTEEN)
				+ ((buffer[currentOffset - FOUR] & 0xff) << EIGHT)
				+ (buffer[currentOffset - THREE] & 0xff);
	}

	public int method440() {
		currentOffset += FOUR;
		return ((buffer[currentOffset - THREE] & 0xff) << TWENTYFOUR)
				+ ((buffer[currentOffset - FOUR] & 0xff) << SIXTEEN)
				+ ((buffer[currentOffset - ONE] & 0xff) << EIGHT)
				+ (buffer[currentOffset - TWO] & 0xff);
	}

	public void method441(int i, byte abyte0[], int j) {
		for (int k = (i + j) - ONE; k >= i; k--)
			buffer[currentOffset++] = (byte) (abyte0[k] + HUNTWENTYEIGHT);

	}

	public void method442(int i, int j, byte abyte0[]) {
		for (int k = (j + i) - ONE; k >= j; k--)
			abyte0[k] = buffer[currentOffset++];

	}
	
	public byte buffer[];
	public int currentOffset;
	public int bitPosition;
	public static final int[] anIntArray1409 = { 0, 1, 3, 7, 15, 31, 63, 127,
			255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535, 0x1ffff,
			0x3ffff, 0x7ffff, 0xfffff, 0x1fffff, 0x3fffff, 0x7fffff, 0xffffff,
			0x1ffffff, 0x3ffffff, 0x7ffffff, 0xfffffff, 0x1fffffff, 0x3fffffff,
			0x7fffffff, -1 };
	public static long[] CRC_64 = new long[256];
	public static int[] CRC_32 = new int[256];
	public ISAACRandomGen encryption;
	public static int anInt1412;
	public static final NodeList nodeList = new NodeList();
	
	private final int EIGHT = 8;
	private final int TWENTYFOUR = 24;
	private final int SIXTEEN = 16;
	private final int HUNTWENTYEIGHT = 128;
	
	private final int SIXFOUR = 64;
	private final int FIVESIX = 56;
	private final int FOUREIGHT = 48;
	private final int FOURZERO = 40;
	private final int THREETWO = 32;
	private final int ONE = 1;
	private final int TWO = 2;
	private final int THREE = 3;
	private final int FOUR = 4;
	private final static int FIVE_THOUSAND = 5000;

	static {
		for (int index = 0; index < 256; index++) {
			long crc = index;

			for (int bit = 0; bit < 8; bit++) {
				if ((crc & 0x1L) == 1L) {
					crc = crc >>> 1 ^ ~0x3693a86a2878f0bdL;
				} else {
					crc >>>= 1;
				}
			}

			CRC_64[index] = crc;
		}

		for (int index = 0; index < 256; index++) {
			int crc = index;
			for (int bit = 0; bit < 8; bit++) {
				if ((0x1 & crc) == 1) {
					crc = crc >>> 1 ^ ~0x12477cdf;
				} else {
					crc >>>= 1;
				}
			}
			CRC_32[index] = crc;
		}
	}
	
}
