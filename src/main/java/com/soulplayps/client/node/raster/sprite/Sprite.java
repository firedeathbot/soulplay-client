package com.soulplayps.client.node.raster.sprite;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.soulplayps.client.Client;
import com.soulplayps.client.login.LoginRenderer;
import com.soulplayps.client.login.Profiles;
import com.soulplayps.client.node.raster.Background;
import com.soulplayps.client.node.raster.Raster;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.opengl.GraphicsDisplay;

public final class Sprite {

	private static int counter = 1;
	public long UID = 0;
	public static boolean disableHD;

	public Sprite(int i, int j) {
		UID = counter++;
		myPixels = new int[i * j];
		myWidth = maxWidth = i;
		myHeight = maxHeight = j;
		drawOffsetX = drawOffsetY = 0;
	}

	public Sprite(int width, int height, int offsetX, int offsetY, int[] pixels) {
		UID = counter++;
		this.myWidth = width;
		this.myHeight = height;
		this.drawOffsetX = offsetX;
		this.drawOffsetY = offsetY;
		this.myPixels = pixels;
	}

	public Sprite flipVertical() {
		Sprite newSprite = new Sprite(myWidth, myHeight);

		System.arraycopy(myPixels, 0, newSprite.myPixels, 0, myPixels.length);

		int[] pixels = newSprite.myPixels;
		for (int y = (myHeight >> 1) - 1; y >= 0; y--) {
			int pixelId = y * myWidth;
			int pixelIdNext = (myHeight - y - 1) * myWidth;
			for (int widthLoop = -myWidth; widthLoop < 0; widthLoop++) {
				int oldPixel = pixels[pixelId];
				pixels[pixelId] = pixels[pixelIdNext];
				pixels[pixelIdNext] = oldPixel;
				pixelId++;
				pixelIdNext++;
			}
		}

		return newSprite;
	}

	public Sprite flipHorizontal() {
		Sprite newSprite = new Sprite(myWidth, myHeight);

		System.arraycopy(myPixels, 0, newSprite.myPixels, 0, myPixels.length);

		int[] pixels = newSprite.myPixels;
		for (int y = myHeight - 1; y >= 0; y--) {
			int pixelId = y * myWidth;
			for (int pixelIdNext = (y + 1) * myWidth; pixelId < pixelIdNext; pixelId++) {
				pixelIdNext--;
				int oldPixel = pixels[pixelId];
				pixels[pixelId] = pixels[pixelIdNext];
				pixels[pixelIdNext] = oldPixel;
			}
		}

		return newSprite;
	}

	public void drawTransparentSprite(int i, int j, int opacity) {
		i += drawOffsetX;
		j += drawOffsetY;
		int k1 = myHeight;
		int l1 = myWidth;
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addSpritePixels(myPixels, i, j, l1, k1, opacity, UID);
			return;
		}
		int i2 = Raster.width - l1;
		int j2 = 0;
		int i1 = i + j * Raster.width;
		int j1 = 0;
		if (j < Raster.topY) {
			int k2 = Raster.topY - j;
			k1 -= k2;
			j = Raster.topY;
			j1 += k2 * l1;
			i1 += k2 * Raster.width;
		}
		if (j + k1 > Raster.bottomY)
			k1 -= (j + k1) - Raster.bottomY;
		if (i < Raster.topX) {
			int l2 = Raster.topX - i;
			l1 -= l2;
			i = Raster.topX;
			j1 += l2;
			i1 += l2;
			j2 += l2;
			i2 += l2;
		}
		if (i + l1 > Raster.bottomX) {
			int i3 = (i + l1) - Raster.bottomX;
			l1 -= i3;
			j2 += i3;
			i2 += i3;
		}
		if (!(l1 <= 0 || k1 <= 0)) {
			method351(j1, l1, Raster.pixels, myPixels, j2, k1, i2, opacity, i1);
		}
	}

	public Sprite(byte spriteData[]) {
		try {
			InputStream inputStream = new ByteArrayInputStream(spriteData);
			BufferedImage bufferedImage = ImageIO.read(inputStream);
			myWidth = bufferedImage.getWidth();
			myHeight = bufferedImage.getHeight();
			maxWidth = myWidth;
			maxHeight = myHeight;
			drawOffsetX = 0;
			drawOffsetY = 0;
			myPixels = new int[myWidth * myHeight];
			PixelGrabber pixelgrabber = new PixelGrabber(bufferedImage, 0, 0, myWidth, myHeight, myPixels, 0, myWidth);
			pixelgrabber.grabPixels();
			setTransparency(0xff00ff);

			inputStream.close();
			inputStream = null;
			bufferedImage = null;
			pixelgrabber = null;
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}

	public void setAlphaTransparency(int a) {
		for (int pixel = myPixels.length - 1; pixel >= 0; pixel--) {
			if (((myPixels[pixel] >> 24) & 255) == a)
				myPixels[pixel] = 0;
		}
	}

	public void setTransparency(int... transparents) {
		for (int trans : transparents) {
			for (int index = myPixels.length - 1; index >= 0; index--) {
				if (myPixels[index] == trans) {
					myPixels[index] = 0;
				}
			}
		}
	}
	
	public Sprite(RSArchive streamLoader, String s, int i, byte[] mediaIndex) {
		interfaceString = s; // for sprite encoder/writer
		RSBuffer stream = new RSBuffer(streamLoader.readFile(s + ".dat"));
		RSBuffer stream_1 = new RSBuffer(mediaIndex);
		stream_1.currentOffset = stream.readUnsignedWord();
		UID = counter++;
		maxWidth = stream_1.readUnsignedWord();
		maxHeight = stream_1.readUnsignedWord();
		int j = stream_1.readUnsignedByte();
		int ai[] = new int[j];
		for (int k = 0; k < j - 1; k++) {
			ai[k + 1] = stream_1.read3Bytes();
			if (ai[k + 1] == 0)
				ai[k + 1] = 1;
		}
		
		for (int l = 0; l < i; l++) {
			stream_1.currentOffset += 2;
			stream.currentOffset += stream_1.readUnsignedWord()
					* stream_1.readUnsignedWord();
			stream_1.currentOffset++;
		}
		
		drawOffsetX = stream_1.readUnsignedByte();
		drawOffsetY = stream_1.readUnsignedByte();
		myWidth = stream_1.readUnsignedWord();
		myHeight = stream_1.readUnsignedWord();
		int i1 = stream_1.readUnsignedByte();
		int j1 = myWidth * myHeight;
		myPixels = new int[j1];
		if (i1 == 0) {
			for (int k1 = 0; k1 < j1; k1++)
				myPixels[k1] = ai[stream.readUnsignedByte()];
		} else if (i1 == 1) {
			for (int l1 = 0; l1 < myWidth; l1++) {
				for (int i2 = 0; i2 < myHeight; i2++)
					myPixels[l1 + i2 * myWidth] = ai[stream.readUnsignedByte()];
			}
		}
		
		setTransparency(0xff00ff);
	}

	public void method345() {
		int totalPixels[] = new int[maxWidth * maxHeight];
		for (int height = 0; height < myHeight; height++) {
			for (int width = 0; width < myWidth; width++) {
				totalPixels[(height + drawOffsetY) * maxWidth + (width + drawOffsetX)] = myPixels[height * myWidth + width];
			}
		}
		myPixels = totalPixels;
		myWidth = maxWidth;
		myHeight = maxHeight;
		drawOffsetX = 0;
		drawOffsetY = 0;
	}

	public void drawTransparentSpriteTrans(int x, int y, int trans) {
		x += drawOffsetX;
		y += drawOffsetY;
		int j1 = myHeight;
		int k1 = myWidth;
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addSpritePixels(myPixels, x, y, k1, j1, trans, this.UID);
			return;
		}

		int rasterWidth = Raster.width;
		int l = x + y * rasterWidth;
		int i1 = 0;
		int l1 = rasterWidth - k1;
		int i2 = 0;
		if (y < Raster.topY) {
			int j2 = Raster.topY - y;
			j1 -= j2;
			y = Raster.topY;
			i1 += j2 * k1;
			l += j2 * rasterWidth;
		}
		if (y + j1 > Raster.bottomY)
			j1 -= (y + j1) - Raster.bottomY;
		if (x < Raster.topX) {
			int k2 = Raster.topX - x;
			k1 -= k2;
			x = Raster.topX;
			i1 += k2;
			l += k2;
			i2 += k2;
			l1 += k2;
		}
		if (x + k1 > Raster.bottomX) {
			int l2 = (x + k1) - Raster.bottomX;
			k1 -= l2;
			i2 += l2;
			l1 += l2;
		}
		if (k1 > 0 && j1 > 0) {
			plotAlphaPixels(Raster.pixels, myPixels, i1, l, k1, j1, l1, i2, trans);
		}
	}
	
	private void plotAlphaPixels(int[] destPixels, int[] sourcePixels, int sourcePixelsPos, int destPixelsPos, int width, int height, int destPixelsStep, int sourcePixelsStep, int sourceAlpha) {
		int alpha;
		int alpha2;
		int src;
		int dest;
		for (int y = -height; y < 0; y++) {
			for (int x = -width; x < 0; x++) {
				alpha = (sourcePixels[sourcePixelsPos] >>> 24) * sourceAlpha >> 8;
				alpha2 = 256 - alpha;
				src = sourcePixels[sourcePixelsPos++];
				dest = destPixels[destPixelsPos];
				destPixels[destPixelsPos++] = ((src & 0xff00ff) * alpha + (dest & 0xff00ff) * alpha2 & ~0xff00ff) + ((src & 0xff00) * alpha + (dest & 0xff00) * alpha2 & 0xff0000) >>> 8;
			}
			destPixelsPos += destPixelsStep;
			sourcePixelsPos += sourcePixelsStep;
		}
	}

	public void method346(int i, int j) {
		i += drawOffsetX;
		j += drawOffsetY;
		int j1 = myHeight;
		int k1 = myWidth;
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addSprite(this, i, j, k1, j1, 255, UID);
			return;
		}

		int l = i + j * Raster.width;
		int i1 = 0;
		int l1 = Raster.width - k1;
		int i2 = 0;
		if (j < Raster.topY) {
			int j2 = Raster.topY - j;
			j1 -= j2;
			j = Raster.topY;
			i1 += j2 * k1;
			l += j2 * Raster.width;
		}
		if (j + j1 > Raster.bottomY)
			j1 -= (j + j1) - Raster.bottomY;
		if (i < Raster.topX) {
			int k2 = Raster.topX - i;
			k1 -= k2;
			i = Raster.topX;
			i1 += k2;
			l += k2;
			i2 += k2;
			l1 += k2;
		}
		if (i + k1 > Raster.bottomX) {
			int l2 = (i + k1) - Raster.bottomX;
			k1 -= l2;
			i2 += l2;
			l1 += l2;
		}
		if (k1 <= 0 || j1 <= 0) {
		} else {
			method347(l, k1, j1, i2, i1, l1, myPixels, Raster.pixels);
		}
	}
	
	private void method347(int i, int j, int k, int l, int i1, int k1,
	                       int ai[], int ai1[]) {
		int l1 = -(j >> 2);
		j = -(j & 3);
		for (int i2 = -k; i2 < 0; i2++) {
			for (int j2 = l1; j2 < 0; j2++) {
				ai1[i++] = ai[i1++];
				ai1[i++] = ai[i1++];
				ai1[i++] = ai[i1++];
				ai1[i++] = ai[i1++];
			}
			
			for (int k2 = j; k2 < 0; k2++)
				ai1[i++] = ai[i1++];
			
			i += k1;
			i1 += l;
		}
	}

	public void drawSprite(int i, int k, int color) {
		int tempWidth = myWidth + 2;
		int tempHeight = myHeight + 2;
		int[] tempArray = new int[tempWidth * tempHeight];
		for (int x = 0; x < myWidth; x++) {
			for (int y = 0; y < myHeight; y++) {
				if (myPixels[x + y * myWidth] != 0)
					tempArray[(x + 1) + (y + 1) * tempWidth] = myPixels[x + y
							* myWidth];
			}
		}
		for (int x = 0; x < tempWidth; x++) {
			for (int y = 0; y < tempHeight; y++) {
				if (tempArray[(x) + (y) * tempWidth] == 0) {
					if (x < tempWidth - 1
							&& tempArray[(x + 1) + ((y) * tempWidth)] != 0
							&& tempArray[(x + 1) + ((y) * tempWidth)] != 0xffffff) {
						tempArray[(x) + (y) * tempWidth] = color;
					}
					if (x > 0
							&& tempArray[(x - 1) + ((y) * tempWidth)] != 0
							&& tempArray[(x - 1) + ((y) * tempWidth)] != 0xffffff) {
						tempArray[(x) + (y) * tempWidth] = color;
					}
					if (y < tempHeight - 1
							&& tempArray[(x) + ((y + 1) * tempWidth)] != 0
							&& tempArray[(x) + ((y + 1) * tempWidth)] != 0xffffff) {
						tempArray[(x) + (y) * tempWidth] = color;
					}
					if (y > 0
							&& tempArray[(x) + ((y - 1) * tempWidth)] != 0
							&& tempArray[(x) + ((y - 1) * tempWidth)] != 0xffffff) {
						tempArray[(x) + (y) * tempWidth] = color;
					}
				}
			}
		}
		i--;
		k--;
		i += drawOffsetX;
		k += drawOffsetY;
		int j1 = tempHeight;
		int k1 = tempWidth;
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addSpritePixels(tempArray, i, k, k1, j1, 255, -1);
			return;
		}
		int l = i + k * Raster.width;
		int i1 = 0;
		int l1 = Raster.width - k1;
		int i2 = 0;
		if (k < Raster.topY) {
			int j2 = Raster.topY - k;
			j1 -= j2;
			k = Raster.topY;
			i1 += j2 * k1;
			l += j2 * Raster.width;
		}
		if (k + j1 > Raster.bottomY) {
			j1 -= (k + j1) - Raster.bottomY;
		}
		if (i < Raster.topX) {
			int k2 = Raster.topX - i;
			k1 -= k2;
			i = Raster.topX;
			i1 += k2;
			l += k2;
			i2 += k2;
			l1 += k2;
		}
		if (i + k1 > Raster.bottomX) {
			int l2 = (i + k1) - Raster.bottomX;
			k1 -= l2;
			i2 += l2;
			l1 += l2;
		}
		if (!(k1 <= 0 || j1 <= 0)) {
			method349(Raster.pixels, tempArray, i1, l, k1, j1, l1, i2);
		}
	}
	
	public boolean mouseInPosition(int x, int y) {
		Client c = Client.instance;
		return c.mouseX >= x && c.mouseY >= y && c.mouseX <= (x + myWidth) && c.mouseY < +(y + myHeight);
	}
	
	public void drawOr(int x, int y, Sprite other) {
		if (mouseInPosition(x, y)) drawSprite(x, y);
		else other.drawSprite(x, y);
	}
	
	public void drawOr(int x, int y, int offsetX, int offsetY, Sprite other) {
		if (mouseInPosition(x, y)) drawSprite(offsetX, offsetY);
		else other.drawSprite(x, y);
	}
	
	public void drawCheckbox(int x, int y, Sprite other, boolean drawOther) {
		if (!drawOther) {
			//other.drawSprite(x, y);
			drawSprite(x, y);
		} else {
			other.drawSprite(x, y);
		}
	}
	
	public void drawProfiles(int x, int y, Sprite profilesTab, Sprite profilesHover, Sprite playerHead, Sprite playerHeadHover, Sprite xBox, Sprite xHover) {
		if (LoginRenderer.profilesOpen) {
			drawSprite(287, 374);
			profilesHover.drawOr(x, y, profilesTab);
			//Profile 1:
			if (Profiles.username_1.length() != 0) {
				playerHeadHover.drawOr(303, 385, 303 - 13, 385 - 1, playerHead);
				xHover.drawOr(350, 375, xBox);
				try {
					Client.instance.newRegularFont.drawBasicString(Profiles.username_1_raw, 298, 453, 0xFFFFFF, 0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			//Profile 2:
			if (Profiles.username_2.length() != 0) {
				playerHeadHover.drawOr(379, 385, 379 - 13, 385 - 1, playerHead);
				xHover.drawOr(426, 375, xBox);
				try {
					Client.instance.newRegularFont.drawBasicString(Profiles.username_2_raw, 375, 453, 0xFFFFFF, 0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			//Profile 3:
			if (Profiles.username_3.length() != 0) {
				playerHeadHover.drawOr(455, 385, 455 - 13, 385 - 1, playerHead);
				xHover.drawOr(502, 375, xBox);
				try {
					Client.instance.newRegularFont.drawBasicString(Profiles.username_3_raw, 453, 453, 0xFFFFFF, 0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			profilesHover.drawOr(x, y, profilesTab);
		}
	}
	
	public void drawSprite(int x, int y) {
		x += drawOffsetX;
		y += drawOffsetY;
		int j1 = myHeight;
		int k1 = myWidth;
		if (GraphicsDisplay.enabled && !disableHD) {
			GraphicsDisplay.getInstance().addSpritePixels(myPixels, x, y, k1, j1, 255, this.UID);
			return;
		}
		
		int rasterWidth = Raster.width;
		int l = x + y * rasterWidth;
		int i1 = 0;
		int l1 = rasterWidth - k1;
		int i2 = 0;
		if (y < Raster.topY) {
			int j2 = Raster.topY - y;
			j1 -= j2;
			y = Raster.topY;
			i1 += j2 * k1;
			l += j2 * rasterWidth;
		}
		if (y + j1 > Raster.bottomY)
			j1 -= (y + j1) - Raster.bottomY;
		if (x < Raster.topX) {
			int k2 = Raster.topX - x;
			k1 -= k2;
			x = Raster.topX;
			i1 += k2;
			l += k2;
			i2 += k2;
			l1 += k2;
		}
		if (x + k1 > Raster.bottomX) {
			int l2 = (x + k1) - Raster.bottomX;
			k1 -= l2;
			i2 += l2;
			l1 += l2;
		}
		if (k1 > 0 && j1 > 0) {
			method349(Raster.pixels, myPixels, i1, l, k1, j1, l1, i2);
		}
	}

	private void method349(int dest[], int src[], int srcPos, int destPos, int width, int height,
			int destStep, int srcStep) {
		for (int y = -height; y < 0; y++) {
			int goal = destPos + width - 3;
			while (destPos < goal) {
				int pixel = src[srcPos++];
				if (pixel != 0)
					dest[destPos++] = pixel;
				else
					destPos++;
				pixel = src[srcPos++];
				if (pixel != 0)
					dest[destPos++] = pixel;
				else
					destPos++;
				pixel = src[srcPos++];
				if (pixel != 0)
					dest[destPos++] = pixel;
				else
					destPos++;
				pixel = src[srcPos++];
				if (pixel != 0)
					dest[destPos++] = pixel;
				else
					destPos++;
			}
			goal += 3;
			while (destPos < goal) {
				int pixel = src[srcPos++];
				if (pixel != 0)
					dest[destPos++] = pixel;
				else
					destPos++;
			}
			destPos += destStep;
			srcPos += srcStep;
		}
	}

	private void method351(int i, int j, int rasterPixels[], int myPixels[], int l, int i1,
	                       int j1, int k1, int l1) {
		int k;// was parameter
		int j2 = 256 - k1;
		for (int k2 = -i1; k2 < 0; k2++) {
			for (int l2 = -j; l2 < 0; l2++) {
				k = myPixels[i++];
				if (k != 0) {
					int i3 = rasterPixels[l1];
					rasterPixels[l1++] = ((k & 0xff00ff) * k1 + (i3 & 0xff00ff) * j2 & 0xff00ff00)
							+ ((k & 0xff00) * k1 + (i3 & 0xff00) * j2 & 0xff0000) >> 8;
				} else {
					l1++;
				}
			}
			
			l1 += j1;
			i += l;
		}
	}

	private static final int WIDTH = 190;
	private static final int HEIGHT = 169;
	private static final int[] hdMapPixels = new int[WIDTH * HEIGHT];

	public void drawBlackout(int x, int y, int[] yCoords, int[] xCoords) {
		if (!Client.refreshGLMinimap) {
			GraphicsDisplay.getInstance().addSpritePixels(hdMapPixels, x, y, WIDTH, HEIGHT, 255, UID);
			return;
		}

		try {
			for(int pos = hdMapPixels.length - 1; pos >= 0; pos--) {
				hdMapPixels[pos] = 0;
			}

			int yStep = 0;
			for (int yOff = 0; yOff < yCoords.length; yOff++) {
				int pixel = yStep + yCoords[yOff];
				for (int xOff = -xCoords[yOff]; xOff < 0; xOff++) {
					hdMapPixels[pixel++] = 1;
				}

				yStep += WIDTH;
			}

			GraphicsDisplay.getInstance().addSpritePixels(hdMapPixels, x, y, WIDTH, HEIGHT, 255, -this.UID);
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}

	public void method352hd(int i, int j, int ai[], int k, int ai1[], int i1,
	                        int l1, int i2, int x, int y) {
		if (!Client.refreshGLMinimap) {
			GraphicsDisplay.getInstance().addSpritePixels(hdMapPixels, x, y, WIDTH, HEIGHT, 255, UID);
			return;
		}

		try {
			for(int pos = hdMapPixels.length - 1; pos >= 0; pos--) {
				hdMapPixels[pos] = 0;
			}

			int j2 = -l1 / 2;
			int k2 = -i / 2;
			int l2 = SIN_LOOKUP[j];
			int i3 = COS_LOOKUP[j];
			l2 = l2 * k >> 8;
			i3 = i3 * k >> 8;
			int j3 = (i2 << 16) + (k2 * l2 + j2 * i3);
			int k3 = (i1 << 16) + (k2 * i3 - j2 * l2);
			int l3 = 0;
			for (int j1 = 0; j1 < ai1.length; j1++) {
				int i4 = ai1[j1];
				int j4 = l3 + i4;
				int k4 = j3 + i3 * i4;
				int l4 = k3 - l2 * i4;
				for (int k1 = -ai[j1]; k1 < 0; k1++) {
					int pos = (k4 >> 16) + (l4 >> 16) * myWidth;
					int pix = myPixels[pos];
					if (pix == 0) {
						pix = 1;
					}

					hdMapPixels[j4++] = pix;
					k4 += i3;
					l4 -= l2;
				}
				
				j3 += l2;
				k3 += i3;
				l3 += WIDTH;
			}

			GraphicsDisplay.getInstance().addSpritePixels(hdMapPixels, x, y, WIDTH, HEIGHT, 255, -this.UID);
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}
	
	public void method352(int height, int angle, int xOffs[], int zoom, int yOffs[], int yOff,
	                      int drawY, int drawX, int width, int xOff) {
		try {
			int j2 = -width / 2;
			int k2 = -height / 2;
			int l2 = SIN_LOOKUP[angle];
			int i3 = COS_LOOKUP[angle];
			l2 = l2 * zoom >> 8;
			i3 = i3 * zoom >> 8;
			int posX = (xOff << 16) + (k2 * l2 + j2 * i3);
			int posY = (yOff << 16) + (k2 * i3 - j2 * l2);
			int l3 = drawX + drawY * Raster.width;
			for (drawY = 0; drawY < yOffs.length; drawY++) {
				int i4 = yOffs[drawY];
				int j4 = l3 + i4;
				int k4 = posX + i3 * i4;
				int l4 = posY - l2 * i4;
				for (drawX = -xOffs[drawY]; drawX < 0; drawX++) {
					Raster.pixels[j4++] = myPixels[(k4 >> 16) + (l4 >> 16) * myWidth];
					k4 += i3;
					l4 -= l2;
				}

				posX += l2;
				posY += i3;
				l3 += Raster.width;
			}
			
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}

	public void method353HD(int offX, int offY, int width, int height, int pixelOffX, int pixelOffY, double angle, int zoom) {
		GraphicsDisplay.getInstance().addSpritePixels(myPixels, offX, offY, myWidth, myHeight, 255, this.UID, angle);
	}
	
	public void method353(int offX, int offY, int width, int height, int pixelOffX, int pixelOffY, double angle, int zoom) {
		try {
			int i2 = -width / 2;
			int j2 = -height / 2;
			int k2 = (int) (Math.sin(angle) * 65536D);
			int l2 = (int) (Math.cos(angle) * 65536D);
			k2 = k2 * zoom >> 8;
			l2 = l2 * zoom >> 8;
			int i3 = (pixelOffX << 16) + (j2 * k2 + i2 * l2);
			int j3 = (pixelOffY << 16) + (j2 * l2 - i2 * k2);
			int k3 = offX + offY * Raster.width;
			for (offY = 0; offY < height; offY++) {
				int l3 = k3;
				int i4 = i3;
				int j4 = j3;
				for (offX = -width; offX < 0; offX++) {
					int k4 = myPixels[(i4 >> 16) + (j4 >> 16) * myWidth];
					if (k4 != 0) {
						Raster.pixels[l3++] = k4;
					} else l3++;
					i4 += l2;
					j4 -= k2;
				}
				
				i3 += k2;
				j3 += l2;
				k3 += Raster.width;
			}
			
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}
	
	public void method354(Background background, int i, int j) {
		j += drawOffsetX;
		i += drawOffsetY;
		int k = j + i * Raster.width;
		int l = 0;
		int i1 = myHeight;
		int j1 = myWidth;
		int k1 = Raster.width - j1;
		int l1 = 0;
		if (i < Raster.topY) {
			int i2 = Raster.topY - i;
			i1 -= i2;
			i = Raster.topY;
			l += i2 * j1;
			k += i2 * Raster.width;
		}
		if (i + i1 > Raster.bottomY)
			i1 -= (i + i1) - Raster.bottomY;
		if (j < Raster.topX) {
			int j2 = Raster.topX - j;
			j1 -= j2;
			j = Raster.topX;
			l += j2;
			k += j2;
			l1 += j2;
			k1 += j2;
		}
		if (j + j1 > Raster.bottomX) {
			int k2 = (j + j1) - Raster.bottomX;
			j1 -= k2;
			l1 += k2;
			k1 += k2;
		}
		if (!(j1 <= 0 || i1 <= 0)) {
			method355(myPixels, j1, background.aByteArray1450, i1,
					Raster.pixels, 0, k1, k, l1, l);
		}
	}
	
	private void method355(int ai[], int i, byte abyte0[], int j, int ai1[],
	                       int k, int l, int i1, int j1, int k1) {
		int l1 = -(i >> 2);
		i = -(i & 3);
		for (int j2 = -j; j2 < 0; j2++) {
			for (int k2 = l1; k2 < 0; k2++) {
				k = ai[k1++];
				if (k != 0 && abyte0[i1] == 0)
					ai1[i1++] = k;
				else
					i1++;
				k = ai[k1++];
				if (k != 0 && abyte0[i1] == 0)
					ai1[i1++] = k;
				else
					i1++;
				k = ai[k1++];
				if (k != 0 && abyte0[i1] == 0)
					ai1[i1++] = k;
				else
					i1++;
				k = ai[k1++];
				if (k != 0 && abyte0[i1] == 0)
					ai1[i1++] = k;
				else
					i1++;
			}
			
			for (int l2 = i; l2 < 0; l2++) {
				k = ai[k1++];
				if (k != 0 && abyte0[i1] == 0)
					ai1[i1++] = k;
				else
					i1++;
			}
			
			i1 += l;
			k1 += j1;
		}
		
	}

	public void shadow(int color) {
		for (int y = myHeight - 1; y > 0; y--) {
			int pixelPos = y * myWidth;
			for (int x = myWidth - 1; x > 0; x--) {
				if (myPixels[x + pixelPos] == 0 && myPixels[x + pixelPos - 1 - myWidth] != 0) {
					myPixels[x + pixelPos] = color;
				}
			}
		}
	}
	
	public void outline(int color) {
		int[] pixels = new int[myWidth * myHeight];
		int pixelPos = 0;
		for (int y = 0; y < myHeight; y++) {
			for (int x = 0; x < myWidth; x++) {
				int pixel = myPixels[pixelPos];
				if (pixel == 0) {
					if (x > 0 && myPixels[pixelPos - 1] != 0) {
						pixel = color;
					} else if (y > 0 && myPixels[pixelPos - myWidth] != 0) {
						pixel = color;
					} else if (x < myWidth - 1 && myPixels[pixelPos + 1] != 0) {
						pixel = color;
					} else if (y < myHeight - 1 && myPixels[pixelPos + myWidth] != 0) {
						pixel = color;
					}
				}
				pixels[pixelPos++] = pixel;
			}
		}
		myPixels = pixels;
	}
	
	public BufferedImage toImage() {
		int[] pixels = myPixels;
		
		final BufferedImage image = new BufferedImage(myWidth, myHeight,
				BufferedImage.TYPE_INT_ARGB);
		
		int[] data = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		
		System.arraycopy(pixels, 0, data, 0, pixels.length);
		
		return image;
	}
	
	public Sprite() {
	}
	
	//for interface encoder
	public String interfaceString;
	
	public int myPixels[];
	public int myWidth;
	public int myHeight;
	public int drawOffsetX;
	public int drawOffsetY;
	public int maxWidth;
	public int maxHeight;

	public static final int[] SIN_LOOKUP = new int[2048];
	public static final int[] COS_LOOKUP = new int[2048];

	static {
		double magic = 2048 / (Math.PI * 2);

		for (int i = 0; i < 2048; i++) {
			SIN_LOOKUP[i] = (int) (Math.sin(i / magic) * 65536D);
			COS_LOOKUP[i] = (int) (Math.cos(i / magic) * 65536D);
		}
	}

}
