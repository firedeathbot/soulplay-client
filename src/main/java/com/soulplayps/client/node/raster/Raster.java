package com.soulplayps.client.node.raster;

import com.soulplayps.client.node.NodeSub;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.Scissor;

import java.math.BigInteger;

import org.lwjgl.opengl.GL11;

public class Raster extends NodeSub {
	
	public static void initDrawingArea(int height, int width, int pixels[]) {
		Raster.pixels = pixels;
		Raster.width = width;
		Raster.height = height;
		setDrawingArea(0, 0, width, height);
	}
	
	public static void initDrawingArea(int width, int height) {
		Raster.width = width;
		Raster.height = height;
		setDrawingArea(0, 0, width, height);
	}
	
	public static void defaultDrawingAreaSize() {
		topX = 0;
		topY = 0;
		bottomX = width;
		bottomY = height;
		centerX = bottomX / 2;
		centerY = bottomY / 2;
		if (GraphicsDisplay.enabled) {
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
			scissor = null;
		}
	}
	
	public static void setDrawingArea(int x, int y, int w, int h) {
		if (x < 0)
			x = 0;
		if (y < 0)
			y = 0;
		if (w > width)
			w = width;
		if (h > height)
			h = height;
		topX = x;
		topY = y;
		bottomX = w;
		bottomY = h;
		centerX = bottomX / 2;
		centerY = bottomY / 2;

		if (GraphicsDisplay.enabled) {
			scissor = Scissor.create();
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
			if (topX > bottomX || topY > bottomY) {
				GL11.glScissor(0, 0, 0, 0);
			} else {
				GL11.glScissor(scissor.x, scissor.y, scissor.width, scissor.height);
			}
		}
	}
	
	public static void reset() {
		int i = width * height;
		for (int j = 0; j < i; j++) {
			pixels[j] = 0;
		}
	}
	
	public static void fillRect(int x, int y, int w, int h, int rgb, int alpha, boolean ignoreAtlas) {
		if (alpha <= 1) {
			return;
		}

		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addBox(x, y, w, h, rgb, alpha, ignoreAtlas, Raster.scissor);
			return;
		}
		
		if (x < topX) {
			w -= topX - x;
			x = topX;
		}
		if (y < topY) {
			h -= topY - y;
			y = topY;
		}
		if (x + w > bottomX)
			w = bottomX - x;
		if (y + h > bottomY)
			h = bottomY - y;
		int l1 = 256 - alpha;
		int i2 = (rgb >> 16 & 0xff) * alpha;
		int j2 = (rgb >> 8 & 0xff) * alpha;
		int k2 = (rgb & 0xff) * alpha;
		int k3 = width - w;
		int l3 = x + y * width;
		for (int i4 = 0; i4 < h; i4++) {
			for (int j4 = -w; j4 < 0; j4++) {
				int l2 = (pixels[l3] >> 16 & 0xff) * l1;
				int i3 = (pixels[l3] >> 8 & 0xff) * l1;
				int j3 = (pixels[l3] & 0xff) * l1;
				int k4 = ((i2 + l2 >> 8) << 16) + ((j2 + i3 >> 8) << 8)
						+ (k2 + j3 >> 8);
				pixels[l3++] = k4;
			}
			
			l3 += k3;
		}
	}
	
	public static void fillRect(int x, int y, int w, int h, int rgb, boolean ignoreAtlas) {
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addBox(x, y, w, h, rgb, 255, ignoreAtlas, Raster.scissor);
			return;
		}
		if (x < topX) {
			w -= topX - x;
			x = topX;
		}
		if (y < topY) {
			h -= topY - y;
			y = topY;
		}
		if (x + w > bottomX)
			w = bottomX - x;
		if (y + h > bottomY)
			h = bottomY - y;
		int k1 = width - w;
		int l1 = x + y * width;
		for (int i2 = -h; i2 < 0; i2++) {
			for (int j2 = -w; j2 < 0; j2++)
				pixels[l1++] = rgb;
			
			l1 += k1;
		}
		
	}
	
	public static void drawRect(int x, int y, int w, int h, int rgb) {
		drawHorizontalLine(x, y, w, rgb);
		drawHorizontalLine(x, (y + h) - 1, w, rgb);
		drawVerticalLine(x, y, h, rgb);
		drawVerticalLine((x + w) - 1, y, h, rgb);
	}
	
	public static void drawRect(int x, int y, int w, int h, int rgb, int trans) {
		if (trans <= 1) {
			return;
		}

		method340(rgb, w, y, trans, x);
		method340(rgb, w, (y + h) - 1, trans, x);
		if (h >= 3) {
			method342(rgb, x, trans, y + 1, h - 2);
			method342(rgb, (x + w) - 1, trans, y + 1, h - 2);
		}
	}

	public static void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int color) {
		if (GraphicsDisplay.enabled) {
			return;
		}

		Raster.drawLine(x1, y1, x2, y2, 0xffffff);
		Raster.drawLine(x1, y1, x3, y3, 0xffffff);
		Raster.drawLine(x2, y2, x3, y3, 0xffffff);

		//Top left
		//Raster.drawRect(x1, y1, 5, 5, 0xffffff);

		//Top right
		//Raster.drawRect(x2, y2, 5, 5, 0xffff00);

		//Bottom left
		//Raster.drawRect(x3, y3, 5, 5, 0xff0000);
	}

	public static void drawLine(int x1, int y1, int x2, int y2, int color) {
		x2 -= x1;
		y2 -= y1;
		if (y2 == 0) {
			if (x2 >= 0) {
				drawHorizontalLine(x1, y1, x2 + 1, color);
			} else {
				drawHorizontalLine(x1 + x2, y1, 1 - x2, color);
			}
		} else if (x2 == 0) {
			if (y2 >= 0) {
				drawVerticalLine(x1, y1, y2 + 1, color);
			} else {
				drawVerticalLine(x1, y1 + y2, 1 - y2, color);
			}
		} else {
			if (x2 + y2 < 0) {
				x1 += x2;
				x2 = -x2;
				y1 += y2;
				y2 = -y2;
			}
			if (x2 > y2) {
				y1 <<= 16;
				y1 += 0x8000;
				y2 <<= 16;
				int yStep = (int) Math.floor((double) y2 / (double) x2 + 0.5);
				x2 += x1;
				if (x1 < topX) {
					y1 += yStep * (topX - x1);
					x1 = topX;
				}
				if (x2 >= bottomX) {
					x2 = bottomX - 1;
				}
				for (/**/; x1 <= x2; x1++) {
					final int pixelY = y1 >> 16;
					if (pixelY >= topY && pixelY < bottomY) {
						pixels[x1 + pixelY * width] = color;
					}
					y1 += yStep;
				}
			} else {
				x1 <<= 16;
				x1 += 32768;
				x2 <<= 16;
				int xStep = (int) Math.floor((double) x2 / (double) y2 + 0.5);
				y2 += y1;
				if (y1 < topY) {
					x1 += xStep * (topY - y1);
					y1 = topY;
				}
				if (y2 >= bottomY) {
					y2 = bottomY - 1;
				}
				for (/**/; y1 <= y2; y1++) {
					final int pixelX = x1 >> 16;
					if (pixelX >= topX && pixelX < bottomX) {
						pixels[pixelX + y1 * width] = color;
					}
					x1 += xStep;
				}
			}
		}
	}

	public static void drawHorizontalLine(int x, int y, int length, int rgb) {
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addBox(x, y, length, 1, rgb, 255, false, Raster.scissor);
			return;
		}
		
		if (y < topY || y >= bottomY)
			return;
		if (x < topX) {
			length -= topX - x;
			x = topX;
		}
		if (x + length > bottomX)
			length = bottomX - x;
		int i1 = x + y * width;
		for (int j1 = 0; j1 < length; j1++)
			pixels[i1 + j1] = rgb;
		
	}
	
	private static void method340(int i, int j, int k, int l, int i1) {
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().addBox(i1, k, j, 1, i, l, false, Raster.scissor);
			return;
		}
		
		if (k < topY || k >= bottomY)
			return;
		if (i1 < topX) {
			j -= topX - i1;
			i1 = topX;
		}
		if (i1 + j > bottomX)
			j = bottomX - i1;
		int j1 = 256 - l;
		int k1 = (i >> 16 & 0xff) * l;
		int l1 = (i >> 8 & 0xff) * l;
		int i2 = (i & 0xff) * l;
		int i3 = i1 + k * width;
		for (int j3 = 0; j3 < j; j3++) {
			int j2 = (pixels[i3] >> 16 & 0xff) * j1;
			int k2 = (pixels[i3] >> 8 & 0xff) * j1;
			int l2 = (pixels[i3] & 0xff) * j1;
			int k3 = ((k1 + j2 >> 8) << 16) + ((l1 + k2 >> 8) << 8)
					+ (i2 + l2 >> 8);
			pixels[i3++] = k3;
		}
		
	}
	
	public static void drawVerticalLine(int x, int y, int length, int rgb) {
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().drawVerticalLne(x, y, length, rgb, 255, null, Raster.scissor);
			return;
		}
		if (x < topX || x >= bottomX)
			return;
		if (y < topY) {
			length -= topY - y;
			y = topY;
		}
		if (y + length > bottomY)
			length = bottomY - y;
		int j1 = x + y * width;
		for (int k1 = 0; k1 < length; k1++)
			pixels[j1 + k1 * width] = rgb;
		
	}
	
	private static void method342(int i, int j, int k, int l, int i1) {
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().drawVerticalLne(j, l, 11, i, k, null, Raster.scissor);
			return;
		}
		if (j < topX || j >= bottomX)
			return;
		if (l < topY) {
			i1 -= topY - l;
			l = topY;
		}
		if (l + i1 > bottomY)
			i1 = bottomY - l;
		int j1 = 256 - k;
		int k1 = (i >> 16 & 0xff) * k;
		int l1 = (i >> 8 & 0xff) * k;
		int i2 = (i & 0xff) * k;
		int i3 = j + l * width;
		for (int j3 = 0; j3 < i1; j3++) {
			int j2 = (pixels[i3] >> 16 & 0xff) * j1;
			int k2 = (pixels[i3] >> 8 & 0xff) * j1;
			int l2 = (pixels[i3] & 0xff) * j1;
			int k3 = ((k1 + j2 >> 8) << 16) + ((l1 + k2 >> 8) << 8)
					+ (i2 + l2 >> 8);
			pixels[i3] = k3;
			i3 += width;
		}
	}
	
	public static void method336(int i, int j, int k, int l, int i1) {
		if (k < topX) {
			i1 -= topX - k;
			k = topX;
		}
		if (j < topY) {
			i -= topY - j;
			j = topY;
		}
		if (k + i1 > bottomX)
			i1 = bottomX - k;
		if (j + i > bottomY)
			i = bottomY - j;
		int k1 = width - i1;
		int l1 = k + j * width;
		for (int i2 = -i; i2 < 0; i2++) {
			for (int j2 = -i1; j2 < 0; j2++)
				pixels[l1++] = l;
			
			l1 += k1;
		}
	}

	public static void fillPixels(int x, int y, int color, int[] yCoords, int[] xCoords) {
		int step = x + y * width;
		for (y = 0; y < yCoords.length; y++) {
			int pixel = step + yCoords[y];
			for (x = -xCoords[y]; x < 0; x++) {
				pixels[pixel++] = color;
			}
			step += width;
		}
	}

	public static void fillCircle(int x, int y, int radius, int color, int alpha) {
		int y1 = y - radius;
		if (y1 < 0) {
			y1 = 0;
		}
		int y2 = y + radius;
		if (y2 >= height) {
			y2 = height - 1;
		}
		int a2 = 256 - alpha;
		int r1 = (color >> 16 & 0xff) * alpha;
		int g1 = (color >> 8 & 0xff) * alpha;
		int b1 = (color & 0xff) * alpha;
		for (int iy = y1; iy <= y2; iy++) {
			int dy = iy - y;
			int dist = (int) Math.sqrt(radius * radius - dy * dy);
			int x1 = x - dist;
			if (x1 < 0) {
				x1 = 0;
			}
			int x2 = x + dist;
			if (x2 >= width) {
				x2 = width - 1;
			}
			int pos = x1 + iy * width;
			for (int ix = x1; ix <= x2; ix++) {
				int r2 = (pixels[pos] >> 16 & 0xff) * a2;
				int g2 = (pixels[pos] >> 8 & 0xff) * a2;
				int b2 = (pixels[pos] & 0xff) * a2;
				pixels[pos++] = ((r1 + r2 >> 8) << 16) + ((g1 + g2 >> 8) << 8) + (b1 + b2 >> 8);
			}
		}
	}

	public static void draw2SideGradient(int x, int y, int w, int h, int col1, int col2, int trans1, int trans2) {
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().drawGradient(x, y, w, h, col1, col2, trans1, trans2, scissor);
			return;
		}

		int posY = 0;
		int stepY = col1 == col2 && trans1 == trans2 ? -1 : 65536 / h;
		int newTrans = trans1;
		int invTrans = 256 - trans1;
		int newColor = col1;
		if (x < topX) {
			w -= topX - x;
			x = topX;
		}

		if (y < topY) {
			posY += (topY - y) * stepY;
			h -= topY - y;
			y = topY;
		}

		if (x + w > bottomX) {
			w = bottomX - x;
		}

		if (y + h > bottomY) {
			h = bottomY - y;
		}

		int pixelStep = width - w;
		int pixelPos = x + y * width;

		for (int yPos = -h; yPos < 0; yPos++) {
			int xPos;
			int pixel;
			for (xPos = -w; xPos < 0; xPos++) {
				pixel = pixels[pixelPos];
				int var16 = newColor + pixel;
				int var18 = (newColor & 0xff00ff) + (pixel & 0xff00ff);
				int var19 = (var18 & 0x1000100) + (var16 - var18 & 0x10000);
				if (invTrans == 0) {
					pixels[pixelPos++] = var16 - var19 | var19 - (var19 >>> 8);
				} else {
					int var21 = var16 - var19 | var19 - (var19 >>> 8);
					pixels[pixelPos++] = ((var21 & 16711935) * newTrans >> 8 & 16711935)
							+ ((var21 & '\uff00') * newTrans >> 8 & '\uff00')
							+ ((pixel & 16711935) * invTrans >> 8 & 16711935)
							+ ((pixel & '\uff00') * invTrans >> 8 & '\uff00');
				}
			}

			if (stepY > 0) {
				posY += stepY;
				xPos = 65536 - posY >> 8;
				pixel = posY >> 8;
				if (trans1 != trans2) {
					newTrans = trans1 * (65536 - posY) + trans2 * posY >> 16;
					invTrans = 256 - newTrans;
				}

				if (col1 != col2) {
					newColor = ((col1 & 16711935) * xPos + (col2 & 16711935) * pixel & -16711936)
							+ ((col1 & '\uff00') * xPos + (col2 & '\uff00') * pixel & 16711680) >>> 8;
				}
			}

			pixelPos += pixelStep;
		}

	}
	
	public Raster() {
	}
	
	public static int pixels[];
	public static int width;
	public static int height;
	public static int topY;
	public static int bottomY;
	public static int topX;
	public static int bottomX;
	public static int centerX;
	public static int centerY;
	public static Scissor scissor;
	
	public static final BigInteger RSA_MODULUS_FAKE = new BigInteger("119968192592354837617006941227808606033480841368481569877537258832695409327761691266622583067908498894063426281282633202648028252072754970502144258655861205282279470611140670240403240072188410618613508352962023144864824992824423290520999429708517642198735509876162367788493401486801874258074813759295017052233");

}
