package com.soulplayps.client.node.raster;

import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.util.Strings;

public class RSFont {
	
	public int textDrawingAreaID = 0;
	public int baseCharacterHeight = 0;
	public int anInt3494;
	public int anInt3512;
	public int[] characterDrawYOffsets;
	public int[] characterHeights;
	public int[] characterDrawXOffsets;
	public int[] characterWidths;
	public boolean disableHD;
	public byte[][] fontPixels;
	public int[] characterScreenWidths;
	private static String[] aStringArray3516 = new String[100];
	public static Sprite[] chatImages;
	public static Sprite[] clanImages;
	public static String aRSString_4135;
	public static String startTransparency;
	public static String startDefaultShadow;
	public static String endShadow = "/shad";
	public static String endEffect;
	public static String endStrikethrough = "/str";
	public static String aRSString_4147;
	public static String startColor;
	public static String lineBreak;
	public static String startStrikethrough;
	public static String endColor;
	public static String startImage;
	public static String startClanImage;
	public static String endUnderline;
	public static String defaultStrikethrough;
	public static String startShadow;
	public static String startEffect;
	public static String aRSString_4162;
	public static String aRSString_4163;
	public static String endTransparency;
	public static String aRSString_4165;
	public static String startUnderline;
	public static String startDefaultUnderline;
	public static String aRSString_4169;
	public static int defaultColor;
	public static int textShadowColor;
	public static int strikethroughColor;
	public static int defaultTransparency;
	public static int anInt4175;
	public static int underlineColor;
	public static int defaultShadow;
	public static int anInt4178;
	public static int transparency;
	public static int textColor;
	public static int textDrawingAreaCounter;
	private static StringBuffer aStringBuffer3500 = new StringBuffer(100);
	private final double strikethroughOffsetY;

	public RSFont(boolean TypeFont, String s, RSArchive archive, double strikethroughOffsetY) {
		textDrawingAreaID = textDrawingAreaCounter++;
		fontPixels = new byte[256][];
		characterWidths = new int[256];
		characterHeights = new int[256];
		characterDrawXOffsets = new int[256];
		characterDrawYOffsets = new int[256];
		characterScreenWidths = new int[256];
		RSBuffer stream = new RSBuffer(archive.readFile(s + ".dat"));
		RSBuffer stream_1 = new RSBuffer(archive.readFile("index.dat"));
		stream_1.currentOffset = stream.readUnsignedWord() + 4;
		int k = stream_1.readUnsignedByte();
		if (k > 0) {
			stream_1.currentOffset += 3 * (k - 1);
		}
		for (int l = 0; l < 256; l++) {
			characterDrawXOffsets[l] = stream_1.readUnsignedByte();
			characterDrawYOffsets[l] = stream_1.readUnsignedByte();
			int i1 = characterWidths[l] = stream_1.readUnsignedWord();
			int j1 = characterHeights[l] = stream_1.readUnsignedWord();
			int k1 = stream_1.readUnsignedByte();
			int l1 = i1 * j1;
			fontPixels[l] = new byte[l1];
			if (k1 == 0) {
				for (int i2 = 0; i2 < l1; i2++) {
					fontPixels[l][i2] = stream.readSignedByte();
				}
				
			} else if (k1 == 1) {
				for (int j2 = 0; j2 < i1; j2++) {
					for (int l2 = 0; l2 < j1; l2++) {
						fontPixels[l][j2 + l2 * i1] = stream.readSignedByte();
					}
					
				}
				
			}
			if (j1 > baseCharacterHeight && l < 128) {
				baseCharacterHeight = j1;
			}
			characterDrawXOffsets[l] = 1;
			characterScreenWidths[l] = i1 + 2;
			int k2 = 0;
			for (int i3 = j1 / 7; i3 < j1; i3++) {
				k2 += fontPixels[l][i3 * i1];
			}
			
			if (k2 <= j1 / 7) {
				characterScreenWidths[l]--;
				characterDrawXOffsets[l] = 0;
			}
			k2 = 0;
			for (int j3 = j1 / 7; j3 < j1; j3++) {
				k2 += fontPixels[l][(i1 - 1) + j3 * i1];
			}
			
			if (k2 <= j1 / 7) {
				characterScreenWidths[l]--;
			}
		}
		
		if (TypeFont) {
			characterScreenWidths[32] = characterScreenWidths[73];
		} else {
			characterScreenWidths[32] = characterScreenWidths[105];
		}

		int i = 2147483647;
		int i_173_ = -2147483648;
		for (int i_174_ = 0; i_174_ < 256; i_174_++) {
			if (characterDrawYOffsets[i_174_] < i && characterHeights[i_174_] != 0) {
				i = characterDrawYOffsets[i_174_];
			}
			if (characterDrawYOffsets[i_174_] + characterHeights[i_174_] > i_173_) {
				i_173_ = characterDrawYOffsets[i_174_] + characterHeights[i_174_];
			}
		}

		anInt3494 = baseCharacterHeight - i;
		anInt3512 = i_173_ - baseCharacterHeight;
		this.strikethroughOffsetY = strikethroughOffsetY;
	}

	public void drawStringMoveY(String string, int drawX, int drawY, int color,
	                            int shadow, int randomMod, int randomMod2) {
		if (string != null) {
			setColorAndShadow(color, shadow);
			double d = 7.0 - randomMod2 / 8.0;
			if (d < 0.0) {
				d = 0.0;
			}

			int length = string.length();
			int[] yOffset = new int[length];
			for (int index = 0; index < length; index++) {
				yOffset[index] = (int) (Math.sin(index / 1.5 + randomMod) * d);
			}
			drawBaseStringMoveXY(string, drawX - getTextWidth(string) / 2,
					drawY, null, yOffset);
		}
	}
	
	public int getCharacterWidth(int i) {
		if (i == 160) {
			i = 32;
		}

		return characterScreenWidths[i & 0xff];
	}
	
	public void setDefaultTextEffectValues(int color, int shadow, int trans) {
		strikethroughColor = -1;
		underlineColor = -1;
		textShadowColor = defaultShadow = shadow;
		textColor = defaultColor = color;
		transparency = defaultTransparency = trans;
		anInt4178 = 0;
		anInt4175 = 0;
	}
	
	public static int method1014(byte[][] is, byte[][] is_27_, int[] is_28_,
	                             int[] is_29_, int[] is_30_, int i, int i_31_) {
		int i_32_ = is_28_[i];
		int i_33_ = i_32_ + is_30_[i];
		int i_34_ = is_28_[i_31_];
		int i_35_ = i_34_ + is_30_[i_31_];
		int i_36_ = i_32_;
		if (i_34_ > i_32_) {
			i_36_ = i_34_;
		}
		int i_37_ = i_33_;
		if (i_35_ < i_33_) {
			i_37_ = i_35_;
		}
		int i_38_ = is_29_[i];
		if (is_29_[i_31_] < i_38_) {
			i_38_ = is_29_[i_31_];
		}
		byte[] is_39_ = is_27_[i];
		byte[] is_40_ = is[i_31_];
		int i_41_ = i_36_ - i_32_;
		int i_42_ = i_36_ - i_34_;
		for (int i_43_ = i_36_; i_43_ < i_37_; i_43_++) {
			int i_44_ = is_39_[i_41_++] + is_40_[i_42_++];
			if (i_44_ < i_38_) {
				i_38_ = i_44_;
			}
		}
		return -i_38_;
	}
	
	public void drawCenteredStringMoveXY(String string, int drawX, int drawY,
	                                     int color, int shadow, int randomMod) {
		if (string != null) {
			setColorAndShadow(color, shadow);

			int length = string.length();
			int[] xMods = new int[length];
			int[] yMods = new int[length];
			for (int index = 0; index < length; index++) {
				xMods[index] = (int) (Math.sin(index / 5.0 + randomMod / 5.0) * 5.0);
				yMods[index] = (int) (Math.sin(index / 3.0 + randomMod / 5.0) * 5.0);
			}
			drawBaseStringMoveXY(string, drawX - getTextWidth(string) / 2,
					drawY, xMods, yMods);
		}
	}

    public int getBasicWidth(String string) {
        if (string == null) {
            return 0;
        }

        int width = 0;
        for (int i = 0, length = string.length(); i < length; i++) {
            width += characterScreenWidths[string.charAt(i) & 0xff];
        }

        return width;
    }

	public void drawCenteredStringMoveY(String class100, int drawX, int drawY,
	                                    int color, int shadow, int i_54_) {
		if (class100 != null) {
			setColorAndShadow(color, shadow);

			int length = class100.length();
			int[] yOffset = new int[length];
			for (int index = 0; index < length; index++) {
				yOffset[index] = (int) (Math.sin(index / 2.0 + i_54_ / 5.0) * 5.0);
			}
			drawBaseStringMoveXY(class100, drawX - getTextWidth(class100) / 2,
					drawY, null, yOffset);
		}
	}
	
	public static void unpackImages(Sprite[] icons, Sprite[] clan) {
		chatImages = icons;
		clanImages = clan;
	}
	
	public void drawBasicString(String string, int drawX, int drawY) {
		drawY -= baseCharacterHeight;
		int startIndex = -1;
		int length = string.length();
		for (int currentCharacter = 0; currentCharacter < length; currentCharacter++) {
			int character = string.charAt(currentCharacter) & 0xff;
			if (character == 60) {
				startIndex = currentCharacter;
			} else {
				if (character == 62 && startIndex != -1) {
					String effectString = string.substring(startIndex + 1,
							currentCharacter);
					startIndex = -1;
					if (effectString.equals(startEffect)) {
						character = 60;
					} else if (effectString.equals(endEffect)) {
						character = 62;
					} else if (effectString.equals(aRSString_4135)) {
						character = 160;
					} else if (effectString.equals(aRSString_4162)) {
						character = 173;
					} else if (effectString.equals(aRSString_4165)) {
						character = 215;
					} else if (effectString.equals(aRSString_4147)) {
						character = 128;
					} else if (effectString.equals(aRSString_4163)) {
						character = 169;
					} else if (effectString.equals(aRSString_4169)) {
						character = 174;
					} else {
//						System.out.println("effect:"+effectString);
						if (effectString.startsWith(startImage)) {
							try {
								int imageId = Strings.stringToInt10(effectString.substring(4));
								if (imageId < 0) {
									continue;
								}

//								System.out.println("effectS:"+imageId);
								Sprite icon = chatImages[imageId];
								int iconModY = icon.myHeight;
								if (transparency == 256) {
									icon.drawSprite(drawX, (drawY
											+ baseCharacterHeight - iconModY));
								} else {
									icon.drawTransparentSprite(drawX, (drawY
													+ baseCharacterHeight - iconModY),
											transparency);
								}
								drawX += icon.myWidth;
							} catch (Exception exception) {
								/* empty */
							}
						} else if (effectString.startsWith(startClanImage)) {
							try {
								int imageId = Strings.stringToInt10(effectString
										.substring(5));
								if (imageId < 0) {
									continue;
								}

								Sprite icon = clanImages[imageId];
								int iconModY = icon.myHeight + icon.drawOffsetY
										+ 1;
								if (transparency == 256) {
									icon.drawSprite(drawX, (drawY
											+ baseCharacterHeight - iconModY));
								} else {
									icon.drawTransparentSprite(drawX, (drawY
													+ baseCharacterHeight - iconModY),
											transparency);
								}
								drawX += 11;
							} catch (Exception exception) {
								/* empty */
							}
						} else {
							setTextEffects(effectString);
						}
						continue;
					}
				}
				if (startIndex == -1) {
					int width = characterWidths[character];
					int height = characterHeights[character];
					if (character != 32) {
						if (transparency == 256) {
							if (textShadowColor != -1) {
								drawCharacter(
										character,
										drawX
												+ characterDrawXOffsets[character]
												+ 1,
										drawY
												+ characterDrawYOffsets[character]
												+ 1, width, height,
										textShadowColor, true);
							}
							drawCharacter(character, drawX
											+ characterDrawXOffsets[character], drawY
											+ characterDrawYOffsets[character], width,
									height, textColor, false);
						} else {
							if (textShadowColor != -1) {
								drawTransparentCharacter(
										character,
										drawX
												+ characterDrawXOffsets[character]
												+ 1,
										drawY
												+ characterDrawYOffsets[character]
												+ 1, width, height,
										textShadowColor, transparency, true);
							}
							drawTransparentCharacter(character, drawX
											+ characterDrawXOffsets[character], drawY
											+ characterDrawYOffsets[character], width,
									height, textColor, transparency, false);
						}
					} else if (anInt4178 > 0) {
						anInt4175 += anInt4178;
						drawX += anInt4175 >> 8;
						anInt4175 &= 0xff;
					}
					int lineWidth = characterScreenWidths[character & 0xff];
					if (strikethroughColor != -1) {
						Raster.drawHorizontalLine(drawX, drawY + (int) (baseCharacterHeight * strikethroughOffsetY), lineWidth, strikethroughColor);
					}
					if (underlineColor != -1) {
						Raster.drawHorizontalLine(drawX, drawY + baseCharacterHeight, lineWidth, underlineColor);
					}
					drawX += lineWidth;
				}
			}
		}
	}
	
	public void drawRAString(String string, int drawX, int drawY, int color,
	                         int shadow) {
		if (string != null) {
			setColorAndShadow(color, shadow);
			drawBasicString(string, drawX - getTextWidth(string), drawY);
		}
	}
	
	public void drawBaseStringMoveXY(String string, int drawX, int drawY,
	                                 int[] xModifier, int[] yModifier) {
		drawY -= baseCharacterHeight;
		int startIndex = -1;
		int modifierOffset = 0;
		int length = string.length();
		for (int currentCharacter = 0; currentCharacter < length; currentCharacter++) {
			int character = string.charAt(currentCharacter);
			if (character == 60) {
				startIndex = currentCharacter;
			} else {
				if (character == 62 && startIndex != -1) {
					String effectString = string.substring(startIndex + 1,
							currentCharacter);
					startIndex = -1;
					if (effectString.equals(startEffect)) {
						character = 60;
					} else if (effectString.equals(endEffect)) {
						character = 62;
					} else if (effectString.equals(aRSString_4135)) {
						character = 160;
					} else if (effectString.equals(aRSString_4162)) {
						character = 173;
					} else if (effectString.equals(aRSString_4165)) {
						character = 215;
					} else if (effectString.equals(aRSString_4147)) {
						character = 128;
					} else if (effectString.equals(aRSString_4163)) {
						character = 169;
					} else if (effectString.equals(aRSString_4169)) {
						character = 174;
					} else {
						if (effectString.startsWith(startImage)) {
							try {
								int xModI;
								if (xModifier != null) {
									xModI = xModifier[modifierOffset];
								} else {
									xModI = 0;
								}
								int yMod;
								if (yModifier != null) {
									yMod = yModifier[modifierOffset];
								} else {
									yMod = 0;
								}
								modifierOffset++;
								int iconId = Strings.stringToInt10(effectString
										.substring(4));
								Sprite icon = chatImages[iconId];
								int iconOffsetY = icon.maxHeight;
								if (transparency == 256) {
									icon.drawSprite(drawX + xModI,
											(drawY + baseCharacterHeight
													- iconOffsetY + yMod));
								} else {
									icon.drawTransparentSprite(drawX + xModI,
											(drawY + baseCharacterHeight
													- iconOffsetY + yMod),
											transparency);
								}
								drawX += icon.myWidth;
							} catch (Exception exception) {
								/* empty */
							}
						} else {
							setTextEffects(effectString);
						}
						continue;
					}
				}
				if (startIndex == -1) {
					int width = characterWidths[character];
					int height = characterHeights[character];
					int xOff;
					if (xModifier != null) {
						xOff = xModifier[modifierOffset];
					} else {
						xOff = 0;
					}
					int yOff;
					if (yModifier != null) {
						yOff = yModifier[modifierOffset];
					} else {
						yOff = 0;
					}
					modifierOffset++;
					if (character != 32) {
						if (transparency == 256) {
							if (textShadowColor != -1) {
								drawCharacter(
										character,
										(drawX
												+ characterDrawXOffsets[character]
												+ 1 + xOff),
										(drawY
												+ characterDrawYOffsets[character]
												+ 1 + yOff), width, height,
										textShadowColor, true);
							}
							drawCharacter(character, drawX
											+ characterDrawXOffsets[character] + xOff,
									drawY + characterDrawYOffsets[character]
											+ yOff, width, height, textColor,
									false);
						} else {
							if (textShadowColor != -1) {
								drawTransparentCharacter(
										character,
										(drawX
												+ characterDrawXOffsets[character]
												+ 1 + xOff),
										(drawY
												+ characterDrawYOffsets[character]
												+ 1 + yOff), width, height,
										textShadowColor, transparency, true);
							}
							drawTransparentCharacter(character, drawX
											+ characterDrawXOffsets[character] + xOff,
									drawY + characterDrawYOffsets[character]
											+ yOff, width, height, textColor,
									transparency, false);
						}
					} else if (anInt4178 > 0) {
						anInt4175 += anInt4178;
						drawX += anInt4175 >> 8;
						anInt4175 &= 0xff;
					}
					int i_109_ = characterScreenWidths[character & 0xff];
					if (strikethroughColor != -1) {
						Raster.drawHorizontalLine(drawX, drawY + (int) (baseCharacterHeight * strikethroughOffsetY), i_109_, strikethroughColor);
					}
					if (underlineColor != -1) {
						Raster.drawHorizontalLine(drawX, drawY + baseCharacterHeight, i_109_, underlineColor);
					}
					drawX += i_109_;
				}
			}
		}
	}

	public void setTextEffects(String string) {
		try {
			if (string.startsWith(startColor)) {
				textColor = Strings.stringToInt16(string.substring(4));
			} else if (string.equals(endColor)) {
				textColor = defaultColor;
			} else if (string.startsWith(startTransparency)) {
				transparency = Integer.valueOf(string.substring(6));
			} else if (string.equals(endTransparency)) {
				transparency = defaultTransparency;
			} else if (string.startsWith(startStrikethrough)) {
				strikethroughColor = Strings.stringToInt16(string.substring(4));
			} else if (string.equals(defaultStrikethrough)) {
				strikethroughColor = 8388608;
			} else if (string.equals(endStrikethrough)) {
				strikethroughColor = -1;
			} else if (string.startsWith(startUnderline)) {
				underlineColor = Integer.valueOf(string.substring(2));
			} else if (string.equals(startDefaultUnderline)) {
				underlineColor = 0;
			} else if (string.equals(endUnderline)) {
				underlineColor = -1;
			} else if (string.startsWith(startShadow)) {
				textShadowColor = Integer.valueOf(string.substring(5));
			} else if (string.equals(startDefaultShadow)) {
				textShadowColor = 0;
			} else if (string.equals(endShadow)) {
				textShadowColor = defaultShadow;
			} else if (string.equals(lineBreak)) {
				setDefaultTextEffectValues(defaultColor, defaultShadow, defaultTransparency);
			}
		} catch (Exception exception) {
			/* empty */
		}
	}
	
	public void setColorAndShadow(int color, int shadow) {
		strikethroughColor = -1;
		underlineColor = -1;
		textShadowColor = defaultShadow = shadow;
		textColor = defaultColor = color;
		transparency = defaultTransparency = 256;
		anInt4178 = 0;
		anInt4175 = 0;
	}

	public void setTrans(int shadow, int color, int trans) {
		textShadowColor = defaultShadow = shadow;
		textColor = defaultColor = color;
		transparency = defaultTransparency = trans;
	}

	public int getTextWidth(String string) {
		if (string == null) {
			return 0;
		}

		int startIndex = -1;
		int finalWidth = 0;
		int length = string.length();
		for (int currentCharacter = 0; currentCharacter < length; currentCharacter++) {
			int character = string.charAt(currentCharacter) & 0xff;
			if (character == 60) {
				startIndex = currentCharacter;
			} else {
				if (character == 62 && startIndex != -1) {
					String effectString = string.substring(startIndex + 1,
							currentCharacter);
					startIndex = -1;
					if (effectString.equals(startEffect)) {
						character = 60;
					} else if (effectString.equals(endEffect)) {
						character = 62;
					} else if (effectString.equals(aRSString_4135)) {
						character = 160;
					} else if (effectString.equals(aRSString_4162)) {
						character = 173;
					} else if (effectString.equals(aRSString_4165)) {
						character = 215;
					} else if (effectString.equals(aRSString_4147)) {
						character = 128;
					} else if (effectString.equals(aRSString_4163)) {
						character = 169;
					} else if (effectString.equals(aRSString_4169)) {
						character = 174;
					} else {
						if (effectString.startsWith(startImage)) {
							try {// <img=
								int iconId = Strings.stringToInt10(effectString
										.substring(4));
								finalWidth += chatImages[iconId].myWidth;
							} catch (Exception exception) {
								/* empty */
							}
						}
						continue;
					}
				}
				if (startIndex == -1) {
					finalWidth += characterScreenWidths[character & 0xff];
				}
			}
		}
		return finalWidth;
	}
	
	public void drawBasicString(String string, int drawX, int drawY, int color,
	                            int shadow) {
		if (string != null) {
			setColorAndShadow(color, shadow);
			drawBasicString(string, drawX, drawY);
		}
	}
	
	public void drawCenteredString(String string, int drawX, int drawY, int color, int shadow) {
		if (string != null) {
			setColorAndShadow(color, shadow);
			drawBasicString(string, drawX - getTextWidth(string) / 2, drawY);
		}
	}

	public void drawCenteredString(String string, int drawX, int drawY, int color, int shadow, int trans) {
		if (string != null) {
			setTrans(shadow, color, trans);
			drawBasicString(string, drawX - getTextWidth(string) / 2, drawY);
		}
	}

	public static void createTransparentCharacterPixels(int[] is, byte[] is_0_,
	                                                    int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_,
	                                                    int i_7_) {
		i = ((i & 0xff00ff) * i_7_ & ~0xff00ff)
				+ ((i & 0xff00) * i_7_ & 0xff0000) >> 8;
		i_7_ = 256 - i_7_;
		for (int i_8_ = -i_4_; i_8_ < 0; i_8_++) {
			for (int i_9_ = -i_3_; i_9_ < 0; i_9_++) {
				if (is_0_[i_1_++] != 0) {
					int i_10_ = is[i_2_];
					is[i_2_++] = ((((i_10_ & 0xff00ff) * i_7_ & ~0xff00ff) + ((i_10_ & 0xff00)
							* i_7_ & 0xff0000)) >> 8)
							+ i;
				} else {
					i_2_++;
				}
			}
			i_2_ += i_5_;
			i_1_ += i_6_;
		}
	}
	
	public void drawTransparentCharacter(int i, int i_11_, int i_12_,
	                                     int i_13_, int i_14_, int i_15_, int i_16_, boolean bool) {
		if (GraphicsDisplay.enabled && !disableHD) {
			GraphicsDisplay.getInstance().addLetter(fontPixels[i], i_11_, i_12_, i_13_, i_14_, i_15_, (char) i, this, true, Raster.scissor);
			return;
		}
		int i_17_ = i_11_ + i_12_ * Raster.width;
		int i_18_ = Raster.width - i_13_;
		int i_19_ = 0;
		int i_20_ = 0;
		if (i_12_ < Raster.topY) {
			int i_21_ = Raster.topY - i_12_;
			i_14_ -= i_21_;
			i_12_ = Raster.topY;
			i_20_ += i_21_ * i_13_;
			i_17_ += i_21_ * Raster.width;
		}
		if (i_12_ + i_14_ > Raster.bottomY) {
			i_14_ -= i_12_ + i_14_ - Raster.bottomY;
		}
		if (i_11_ < Raster.topX) {
			int i_22_ = Raster.topX - i_11_;
			i_13_ -= i_22_;
			i_11_ = Raster.topX;
			i_20_ += i_22_;
			i_17_ += i_22_;
			i_19_ += i_22_;
			i_18_ += i_22_;
		}
		if (i_11_ + i_13_ > Raster.bottomX) {
			int i_23_ = i_11_ + i_13_ - Raster.bottomX;
			i_13_ -= i_23_;
			i_19_ += i_23_;
			i_18_ += i_23_;
		}
		if (i_13_ > 0 && i_14_ > 0) {
			createTransparentCharacterPixels(Raster.pixels, fontPixels[i],
					i_15_, i_20_, i_17_, i_13_, i_14_, i_18_, i_19_, i_16_);
		}
	}
	
	public static void createCharacterPixels(int[] is, byte[] is_24_, int i,
	                                         int i_25_, int i_26_, int i_27_, int i_28_, int i_29_, int i_30_) {
		int i_31_ = -(i_27_ >> 2);
		i_27_ = -(i_27_ & 0x3);
		for (int i_32_ = -i_28_; i_32_ < 0; i_32_++) {
			for (int i_33_ = i_31_; i_33_ < 0; i_33_++) {
				if (is_24_[i_25_++] != 0) {
					is[i_26_++] = i;
				} else {
					i_26_++;
				}
				if (is_24_[i_25_++] != 0) {
					is[i_26_++] = i;
				} else {
					i_26_++;
				}
				if (is_24_[i_25_++] != 0) {
					is[i_26_++] = i;
				} else {
					i_26_++;
				}
				if (is_24_[i_25_++] != 0) {
					is[i_26_++] = i;
				} else {
					i_26_++;
				}
			}
			for (int i_34_ = i_27_; i_34_ < 0; i_34_++) {
				if (is_24_[i_25_++] != 0) {
					is[i_26_++] = i;
				} else {
					i_26_++;
				}
			}
			i_26_ += i_29_;
			i_25_ += i_30_;
		}
	}
	
	public void drawCharacter(int character, int i_35_, int i_36_, int i_37_,
	                          int i_38_, int i_39_, boolean bool) {
		if (GraphicsDisplay.enabled && !disableHD) {
			GraphicsDisplay.getInstance().addLetter(fontPixels[character], i_35_, i_36_, i_37_, i_38_, i_39_, (char) character, this, true, Raster.scissor);
			return;
		}
		int i_40_ = i_35_ + i_36_ * Raster.width;
		int i_41_ = Raster.width - i_37_;
		int i_42_ = 0;
		int i_43_ = 0;
		if (i_36_ < Raster.topY) {
			int i_44_ = Raster.topY - i_36_;
			i_38_ -= i_44_;
			i_36_ = Raster.topY;
			i_43_ += i_44_ * i_37_;
			i_40_ += i_44_ * Raster.width;
		}
		if (i_36_ + i_38_ > Raster.bottomY) {
			i_38_ -= i_36_ + i_38_ - Raster.bottomY;
		}
		if (i_35_ < Raster.topX) {
			int i_45_ = Raster.topX - i_35_;
			i_37_ -= i_45_;
			i_35_ = Raster.topX;
			i_43_ += i_45_;
			i_40_ += i_45_;
			i_42_ += i_45_;
			i_41_ += i_45_;
		}
		if (i_35_ + i_37_ > Raster.bottomX) {
			int i_46_ = i_35_ + i_37_ - Raster.bottomX;
			i_37_ -= i_46_;
			i_42_ += i_46_;
			i_41_ += i_46_;
		}
		if (i_37_ > 0 && i_38_ > 0) {
			createCharacterPixels(Raster.pixels, fontPixels[character],
					i_39_, i_43_, i_40_, i_37_, i_38_, i_41_, i_42_);
			
		}
	}
	
	public int drawInterfaceText(final String text, final int x, final int y, final int width, final int height, final int textColor, final int shadowColor, final int transparency, final int horizontalAlignment, int verticalAlignment, int verticalSpacing) {
		if (text == null) {
			return 0;
		}

		setDefaultTextEffectValues(textColor, shadowColor, transparency);
		if (verticalSpacing == 0) {
			verticalSpacing = baseCharacterHeight;
		}

		int[] is = { width };
		if (height < anInt3494 + anInt3512 + verticalSpacing && height < verticalSpacing + verticalSpacing) {
			is = null;
		}
		final int lineCount = method1486(text, is, aStringArray3516);
		if (verticalAlignment == 3 && lineCount == 1) {
			verticalAlignment = 1;
		}
		int textY;
		if (verticalAlignment == 0) {
			textY = y + anInt3494;
		} else if (verticalAlignment == 1) {
			textY = y + anInt3494 + (height - anInt3494 - anInt3512 - (lineCount - 1) * verticalSpacing) / 2;
		} else if (verticalAlignment == 2) {
			textY = y + height - anInt3512 - (lineCount - 1) * verticalSpacing;
		} else {
			int i_72_ = (height - anInt3494 - anInt3512 - (lineCount - 1) * verticalSpacing) / (lineCount + 1);
			if (i_72_ < 0) {
				i_72_ = 0;
			}
			textY = y + anInt3494 + i_72_;
			verticalSpacing += i_72_;
		}
		for (int lineId = 0; lineId < lineCount; lineId++) {
			if (horizontalAlignment == 0) {
				drawBasicString(aStringArray3516[lineId], x, textY);
			} else if (horizontalAlignment == 1) {
				drawBasicString(aStringArray3516[lineId], x + (width - getTextWidth(aStringArray3516[lineId])) / 2, textY);
			} else if (horizontalAlignment == 2) {
				drawBasicString(aStringArray3516[lineId], x + width - getTextWidth(aStringArray3516[lineId]), textY);
			} else if (lineId == lineCount - 1) {
				drawBasicString(aStringArray3516[lineId], x, textY);
			} else {
				method1464(aStringArray3516[lineId], width);
				drawBasicString(aStringArray3516[lineId], x, textY);
				anInt4178 = 0;
			}
			textY += verticalSpacing;
		}
		return lineCount;
	}

	private final void method1464(final String string, final int width) {
		int spaceCount = 0;
		boolean effectStart = false;
		int length = string.length();
		for (int charIndex = 0; charIndex < length; charIndex++) {
			final char c = string.charAt(charIndex);
			if (c == '<') {
				effectStart = true;
			} else if (c == '>') {
				effectStart = false;
			} else if (!effectStart && c == ' ') {
				spaceCount++;
			}
		}
		if (spaceCount > 0) {
			anInt4178 = (width - getTextWidth(string) << 8) / spaceCount;
		}
	}

	public int paragraphWidth(String text, int width) {
		int lines = method1486(text, new int[] { width }, aStringArray3516);
		int maxWidth = 0;
		for (int i = 0; i < lines; i++) {
			int paraWidth = getTextWidth(aStringArray3516[i]);
			if (maxWidth < paraWidth) {
				maxWidth = paraWidth;
			}
		}

		return maxWidth;
	}

	public int paragraphHeigth(String text, int width) {
		return method1486(text, new int[] { width }, aStringArray3516);
	}

	final int method1486(final String string, final int[] is, final String[] strings) {
		if (string == null) {
			return 0;
		}

		aStringBuffer3500.setLength(0);
		int width = 0;
		int i_175_ = 0;
		int i_176_ = -1;
		int i_177_ = 0;
		int i_178_ = 0;
		int effectStartIndex = -1;
		int stringsPos = 0;
		final int length = string.length();
		for (int id = 0; id < length; id++) {
			char character = string.charAt(id);
			if (character == 60) {//<
				effectStartIndex = id;
			} else {
				if (character == 62 && effectStartIndex != -1) {//>
					final String effect = string.substring(effectStartIndex + 1, id).toLowerCase();
					effectStartIndex = -1;
					aStringBuffer3500.append('<');
					aStringBuffer3500.append(effect);
					aStringBuffer3500.append('>');
					if (effect.equals("br")) {
						strings[stringsPos] = aStringBuffer3500.toString().substring(i_175_, aStringBuffer3500.length());
						stringsPos++;
						i_175_ = aStringBuffer3500.length();
						width = 0;
						i_176_ = -1;
					} else if (effect.equals("lt")) {
						width += getCharacterWidth('<');
					} else if (effect.equals("gt")) {
						width += getCharacterWidth('>');
					} else if (effect.equals("nbsp")) {
						width += getCharacterWidth('\u00a0');
					} else if (effect.equals("shy")) {
						width += getCharacterWidth('\u00ad');
					} else if (effect.equals("times")) {
						width += getCharacterWidth('\u00d7');
					} else if (effect.equals("euro")) {
						width += getCharacterWidth('\u20ac');
					} else if (effect.equals("copy")) {
						width += getCharacterWidth('\u00a9');
					} else if (effect.equals("reg")) {
						width += getCharacterWidth('\u00ae');
					} else if (effect.startsWith("img=")) {
						try {
							final int nameIconid = Strings.stringToInt10(effect.substring(4));
							width += chatImages[nameIconid].myWidth;
						} catch (final Exception exception) {
							/* empty */
						}
					}
					character = '\0';
				}
				if (effectStartIndex == -1) {
					if (character != 0) {
						aStringBuffer3500.append(character);
						width += getCharacterWidth(character);
					}
					if (character == 32) {//Space
						i_176_ = aStringBuffer3500.length();
						i_177_ = width;
						i_178_ = 1;
					}
					if (is != null && width > is[stringsPos < is.length ? stringsPos : is.length - 1] && i_176_ >= 0) {
						strings[stringsPos] = aStringBuffer3500.toString().substring(i_175_, i_176_ - i_178_);
						stringsPos++;
						i_175_ = i_176_;
						i_176_ = -1;
						width -= i_177_;
					}
					if (character == 45) {//-
						i_176_ = aStringBuffer3500.length();
						i_177_ = width;
						i_178_ = 0;
					}
				}
			}
		}
		if (aStringBuffer3500.length() > i_175_) {
			strings[stringsPos] = aStringBuffer3500.toString().substring(i_175_, aStringBuffer3500.length());
			stringsPos++;
		}
		return stringsPos;
	}

	public static String escape(String text) {
		if (text == null) {
			return null;
		}

		int realLength = text.length();
		int newLength = 0;
		for (int i = 0; i < realLength; i++) {
			char c = text.charAt(i);
			if (c == '<' || c == '>') {
				newLength += 3;
			}
		}
		StringBuffer stringbuffer = new StringBuffer(realLength + newLength);
		for (int i = 0; i < realLength; i++) {
			char c = text.charAt(i);
			if (c == '<') {
				stringbuffer.append("<lt>");
			} else if (c == '>') {
				stringbuffer.append("<gt>");
			} else {
				stringbuffer.append(c);
			}
		}
		return stringbuffer.toString();
	}

	static {
		startTransparency = "trans=";
		startStrikethrough = "str=";
		startDefaultShadow = "shad";
		startColor = "col=";
		lineBreak = "br";
		defaultStrikethrough = "str";
		endUnderline = "/currentY";
		startImage = "img=";
		startClanImage = "clan=";
		startShadow = "shad=";
		startUnderline = "currentY=";
		endColor = "/col";
		startDefaultUnderline = "currentY";
		endTransparency = "/trans";
		
		aRSString_4135 = "nbsp";
		aRSString_4169 = "reg";
		aRSString_4165 = "times";
		aRSString_4162 = "shy";
		aRSString_4163 = "copy";
		endEffect = "gt";
		aRSString_4147 = "euro";
		startEffect = "lt";
		defaultTransparency = 256;
		defaultShadow = -1;
		anInt4175 = 0;
		textShadowColor = -1;
		textColor = 0;
		defaultColor = 0;
		strikethroughColor = -1;
		underlineColor = -1;
		anInt4178 = 0;
		transparency = 256;
	}
}