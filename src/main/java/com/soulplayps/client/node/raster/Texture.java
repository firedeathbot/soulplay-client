package com.soulplayps.client.node.raster;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.io.RSBuffer;

public final class Texture {
	
	public static final int COUNT = 678;
	
	public static Texture[] cache = new Texture[COUNT];
	public static final boolean[] tex64x64 = new boolean[COUNT];
	
	public int[][] mipmaps = new int[8][];
	public int width, height;
	public boolean hasAlpha;
	public boolean transparentTexture;
	static double brightness;
	public int anInt1226;
	
	public static final Texture get(int index) {
		if (index < 0 || index >= cache.length) {
			return null;
		}
		if (cache[index] == null) {
			Client.onDemandFetcher.loadData(4, index);
			return null;
		}
		return cache[index];
	}
	
	public static final void decode(int index, byte[] data) {
		Texture texture = cache[index] = new Texture();
		RSBuffer buffer = new RSBuffer(data);
		texture.width = buffer.readUnsignedWord();
		texture.height = buffer.readUnsignedWord();
		texture.mipmaps[0] = new int[16384];
		for (int y = 0; y < texture.height; y++) {
			for (int x = 0; x < texture.width; x++) {
				int rgb = buffer.read3Bytes();
				if (texture.width <= 64 && texture.height <= 64) {
					int x2 = x << 1;
					int y2 = y << 1;
					texture.set(x2, y2, rgb);
					texture.set(x2 + 1, y2, rgb);
					texture.set(x2 + 1, y2 + 1, rgb);
					texture.set(x2, y2 + 1, rgb);
				} else {
					texture.set(x, y, rgb);
				}
			}
		}
		texture.generate();
	}
	
	public static void setBrightness(double value) {
		brightness = value;
	}
	
	private void set(int x, int y, int rgb) {
		if (x < 128 && y < 128) {
			mipmaps[0][x + (y << 7)] = adjustBrightness(rgb, brightness / 1.25D);
		}
	}
	
	private static int adjustBrightness(int rgb, double brightness) {
		int r = ((int) (Math.pow((rgb >>> 16) / 256.0D, brightness) * 256.0D) << 16);
		int g = ((int) (Math.pow(((rgb >>> 8) & 0xff) / 256.0D, brightness) * 256.0D) << 8);
		int b = (int) (Math.pow((rgb & 0xff) / 256.0D, brightness) * 256.0D);
		return r | g | b;
	}
	
	private void generate() {
		for (int level = 1, size = 64; level < 8; level++) {
			int[] src = mipmaps[level - 1];
			int[] dst = mipmaps[level] = new int[size * size];
			for (int x = 0; x < size; x++) {
				for (int y = 0; y < size; y++) {
					double r = 0, g = 0, b = 0;
					int count = 0;
					for (int rgb : new int[]{src[x + (y * size << 1) << 1], src[(x + (y * size << 1) << 1) + 1], src[(x + (y * size << 1) << 1) + (size << 1)], src[(x + (y * size << 1) << 1) + (size << 1) + 1]}) {
						if (rgb != 0) {
							double dr = (rgb >> 16 & 0xff) / 255d;
							double dg = (rgb >> 8 & 0xff) / 255d;
							double db = (rgb & 0xff) / 255d;
							r += dr * dr;
							g += dg * dg;
							b += db * db;
							count++;
						}
					}
					if (count != 0) {
						int ri = Math.round(255 * (float) Math.sqrt(r / count));
						int gi = Math.round(255 * (float) Math.sqrt(g / count));
						int bi = Math.round(255 * (float) Math.sqrt(b / count));
						dst[x + y * size] = ri << 16 | gi << 8 | bi;
					}
				}
			}
			size >>= 1;
			dst = src = null;
		}
	}
	
	public static final void reset() {
		cache = null;
	}
	
	static {
		tex64x64[4] = true;
		tex64x64[20] = true;
		tex64x64[36] = true;
		tex64x64[39] = true;
		tex64x64[43] = true;
		tex64x64[51] = true;
		tex64x64[56] = true;
		tex64x64[57] = true;
		tex64x64[58] = true;
		tex64x64[59] = true;
		tex64x64[60] = true;
		tex64x64[65] = true;
		tex64x64[68] = true;
		tex64x64[69] = true;
		tex64x64[71] = true;
		tex64x64[74] = true;
		tex64x64[75] = true;
		tex64x64[76] = true;
		tex64x64[77] = true;
		tex64x64[78] = true;
		tex64x64[81] = true;
		tex64x64[82] = true;
		tex64x64[86] = true;
		tex64x64[87] = true;
		tex64x64[88] = true;
		tex64x64[89] = true;
		tex64x64[90] = true;
		tex64x64[91] = true;
		tex64x64[93] = true;
		tex64x64[94] = true;
		tex64x64[95] = true;
		tex64x64[97] = true;
		tex64x64[100] = true;
		tex64x64[106] = true;
		tex64x64[107] = true;
		tex64x64[108] = true;
		tex64x64[109] = true;
		tex64x64[110] = true;
		tex64x64[111] = true;
		tex64x64[113] = true;
		tex64x64[114] = true;
		tex64x64[115] = true;
		tex64x64[116] = true;
		tex64x64[117] = true;
		tex64x64[118] = true;
		tex64x64[119] = true;
		tex64x64[120] = true;
		tex64x64[121] = true;
		tex64x64[122] = true;
		tex64x64[123] = true;
		tex64x64[124] = true;
		tex64x64[125] = true;
		tex64x64[127] = true;
		tex64x64[131] = true;
		tex64x64[133] = true;
		tex64x64[134] = true;
		tex64x64[135] = true;
		tex64x64[136] = true;
		tex64x64[138] = true;
		tex64x64[139] = true;
		tex64x64[140] = true;
		tex64x64[141] = true;
		tex64x64[142] = true;
		tex64x64[143] = true;
		tex64x64[145] = true;
		tex64x64[146] = true;
		tex64x64[147] = true;
		tex64x64[148] = true;
		tex64x64[149] = true;
		tex64x64[150] = true;
		tex64x64[151] = true;
		tex64x64[153] = true;
		tex64x64[155] = true;
		tex64x64[156] = true;
		tex64x64[157] = true;
		tex64x64[158] = true;
		tex64x64[159] = true;
		tex64x64[163] = true;
		tex64x64[164] = true;
		tex64x64[166] = true;
		tex64x64[167] = true;
		tex64x64[169] = true;
		tex64x64[170] = true;
		tex64x64[171] = true;
		tex64x64[172] = true;
		tex64x64[175] = true;
		tex64x64[176] = true;
		tex64x64[177] = true;
		tex64x64[178] = true;
		tex64x64[179] = true;
		tex64x64[180] = true;
		tex64x64[182] = true;
		tex64x64[183] = true;
		tex64x64[184] = true;
		tex64x64[185] = true;
		tex64x64[186] = true;
		tex64x64[187] = true;
		tex64x64[188] = true;
		tex64x64[189] = true;
		tex64x64[190] = true;
		tex64x64[191] = true;
		tex64x64[192] = true;
		tex64x64[195] = true;
		tex64x64[196] = true;
		tex64x64[198] = true;
		tex64x64[199] = true;
		tex64x64[200] = true;
		tex64x64[201] = true;
		tex64x64[202] = true;
		tex64x64[203] = true;
		tex64x64[204] = true;
		tex64x64[205] = true;
		tex64x64[206] = true;
		tex64x64[207] = true;
		tex64x64[208] = true;
		tex64x64[212] = true;
		tex64x64[213] = true;
		tex64x64[214] = true;
		tex64x64[215] = true;
		tex64x64[216] = true;
		tex64x64[217] = true;
		tex64x64[218] = true;
		tex64x64[220] = true;
		tex64x64[221] = true;
		tex64x64[223] = true;
		tex64x64[224] = true;
		tex64x64[226] = true;
		tex64x64[227] = true;
		tex64x64[228] = true;
		tex64x64[229] = true;
		tex64x64[230] = true;
		tex64x64[231] = true;
		tex64x64[232] = true;
		tex64x64[233] = true;
		tex64x64[234] = true;
		tex64x64[235] = true;
		tex64x64[236] = true;
		tex64x64[237] = true;
		tex64x64[238] = true;
		tex64x64[239] = true;
		tex64x64[240] = true;
		tex64x64[241] = true;
		tex64x64[242] = true;
		tex64x64[243] = true;
		tex64x64[244] = true;
		tex64x64[245] = true;
		tex64x64[246] = true;
		tex64x64[247] = true;
		tex64x64[248] = true;
		tex64x64[249] = true;
		tex64x64[250] = true;
		tex64x64[251] = true;
		tex64x64[252] = true;
		tex64x64[253] = true;
		tex64x64[254] = true;
		tex64x64[255] = true;
		tex64x64[256] = true;
		tex64x64[257] = true;
		tex64x64[258] = true;
		tex64x64[259] = true;
		tex64x64[260] = true;
		tex64x64[261] = true;
		tex64x64[262] = true;
		tex64x64[263] = true;
		tex64x64[264] = true;
		tex64x64[265] = true;
		tex64x64[266] = true;
		tex64x64[267] = true;
		tex64x64[268] = true;
		tex64x64[269] = true;
		tex64x64[270] = true;
		tex64x64[271] = true;
		tex64x64[272] = true;
		tex64x64[273] = true;
		tex64x64[275] = true;
		tex64x64[277] = true;
		tex64x64[278] = true;
		tex64x64[279] = true;
		tex64x64[280] = true;
		tex64x64[281] = true;
		tex64x64[282] = true;
		tex64x64[284] = true;
		tex64x64[285] = true;
		tex64x64[286] = true;
		tex64x64[287] = true;
		tex64x64[288] = true;
		tex64x64[289] = true;
		tex64x64[290] = true;
		tex64x64[291] = true;
		tex64x64[292] = true;
		tex64x64[293] = true;
		tex64x64[294] = true;
		tex64x64[295] = true;
		tex64x64[296] = true;
		tex64x64[297] = true;
		tex64x64[298] = true;
		tex64x64[299] = true;
		tex64x64[302] = true;
		tex64x64[303] = true;
		tex64x64[304] = true;
		tex64x64[305] = true;
		tex64x64[306] = true;
		tex64x64[307] = true;
		tex64x64[308] = true;
		tex64x64[310] = true;
		tex64x64[311] = true;
		tex64x64[313] = true;
		tex64x64[314] = true;
		tex64x64[315] = true;
		tex64x64[316] = true;
		tex64x64[317] = true;
		tex64x64[318] = true;
		tex64x64[319] = true;
		tex64x64[320] = true;
		tex64x64[321] = true;
		tex64x64[322] = true;
		tex64x64[324] = true;
		tex64x64[325] = true;
		tex64x64[326] = true;
		tex64x64[327] = true;
		tex64x64[329] = true;
		tex64x64[330] = true;
		tex64x64[331] = true;
		tex64x64[332] = true;
		tex64x64[334] = true;
		tex64x64[337] = true;
		tex64x64[338] = true;
		tex64x64[339] = true;
		tex64x64[340] = true;
		tex64x64[341] = true;
		tex64x64[342] = true;
		tex64x64[343] = true;
		tex64x64[344] = true;
		tex64x64[345] = true;
		tex64x64[346] = true;
		tex64x64[347] = true;
		tex64x64[349] = true;
		tex64x64[350] = true;
		tex64x64[351] = true;
		tex64x64[353] = true;
		tex64x64[354] = true;
		tex64x64[356] = true;
		tex64x64[357] = true;
		tex64x64[358] = true;
		tex64x64[359] = true;
		tex64x64[361] = true;
		tex64x64[362] = true;
		tex64x64[363] = true;
		tex64x64[364] = true;
		tex64x64[365] = true;
		tex64x64[367] = true;
		tex64x64[368] = true;
		tex64x64[369] = true;
		tex64x64[370] = true;
		tex64x64[371] = true;
		tex64x64[372] = true;
		tex64x64[373] = true;
		tex64x64[374] = true;
		tex64x64[375] = true;
		tex64x64[377] = true;
		tex64x64[378] = true;
		tex64x64[379] = true;
		tex64x64[381] = true;
		tex64x64[382] = true;
		tex64x64[383] = true;
		tex64x64[385] = true;
		tex64x64[386] = true;
		tex64x64[387] = true;
		tex64x64[389] = true;
		tex64x64[390] = true;
		tex64x64[391] = true;
		tex64x64[392] = true;
		tex64x64[393] = true;
		tex64x64[394] = true;
		tex64x64[396] = true;
		tex64x64[397] = true;
		tex64x64[398] = true;
		tex64x64[401] = true;
		tex64x64[402] = true;
		tex64x64[403] = true;
		tex64x64[404] = true;
		tex64x64[405] = true;
		tex64x64[406] = true;
		tex64x64[407] = true;
		tex64x64[410] = true;
		tex64x64[411] = true;
		tex64x64[412] = true;
		tex64x64[413] = true;
		tex64x64[414] = true;
		tex64x64[417] = true;
		tex64x64[419] = true;
		tex64x64[420] = true;
		tex64x64[421] = true;
		tex64x64[426] = true;
		tex64x64[427] = true;
		tex64x64[428] = true;
		tex64x64[430] = true;
		tex64x64[431] = true;
		tex64x64[432] = true;
		tex64x64[433] = true;
		tex64x64[434] = true;
		tex64x64[435] = true;
		tex64x64[436] = true;
		tex64x64[437] = true;
		tex64x64[438] = true;
		tex64x64[439] = true;
		tex64x64[440] = true;
		tex64x64[442] = true;
		tex64x64[443] = true;
		tex64x64[444] = true;
		tex64x64[445] = true;
		tex64x64[446] = true;
		tex64x64[447] = true;
		tex64x64[448] = true;
		tex64x64[449] = true;
		tex64x64[450] = true;
		tex64x64[451] = true;
		tex64x64[452] = true;
		tex64x64[453] = true;
		tex64x64[454] = true;
		tex64x64[456] = true;
		tex64x64[458] = true;
		tex64x64[459] = true;
		tex64x64[460] = true;
		tex64x64[461] = true;
		tex64x64[464] = true;
		tex64x64[465] = true;
		tex64x64[466] = true;
		tex64x64[467] = true;
		tex64x64[468] = true;
		tex64x64[469] = true;
		tex64x64[470] = true;
		tex64x64[471] = true;
		tex64x64[472] = true;
		tex64x64[473] = true;
		tex64x64[474] = true;
		tex64x64[475] = true;
		tex64x64[476] = true;
		tex64x64[477] = true;
		tex64x64[478] = true;
		tex64x64[479] = true;
		tex64x64[480] = true;
		tex64x64[481] = true;
		tex64x64[482] = true;
		tex64x64[483] = true;
		tex64x64[484] = true;
		tex64x64[485] = true;
		tex64x64[486] = true;
		tex64x64[487] = true;
		tex64x64[488] = true;
		tex64x64[489] = true;
		tex64x64[490] = true;
		tex64x64[525] = true;
		tex64x64[527] = true;
		tex64x64[528] = true;
		tex64x64[529] = true;
		tex64x64[530] = true;
		tex64x64[531] = true;
		tex64x64[532] = true;
		tex64x64[534] = true;
		tex64x64[536] = true;
		tex64x64[537] = true;
		tex64x64[538] = true;
		tex64x64[539] = true;
		tex64x64[541] = true;
		tex64x64[542] = true;
		tex64x64[543] = true;
		tex64x64[544] = true;
		tex64x64[548] = true;
		tex64x64[549] = true;
		tex64x64[550] = true;
		tex64x64[552] = true;
		tex64x64[553] = true;
		tex64x64[554] = true;
		tex64x64[555] = true;
		tex64x64[556] = true;
		tex64x64[557] = true;
		tex64x64[558] = true;
		tex64x64[559] = true;
		tex64x64[560] = true;
		tex64x64[561] = true;
		tex64x64[562] = true;
		tex64x64[563] = true;
		tex64x64[565] = true;
		tex64x64[567] = true;
		tex64x64[568] = true;
		tex64x64[569] = true;
		tex64x64[570] = true;
		tex64x64[571] = true;
		tex64x64[572] = true;
		tex64x64[573] = true;
		tex64x64[574] = true;
		tex64x64[575] = true;
		tex64x64[576] = true;
		tex64x64[577] = true;
		tex64x64[578] = true;
		tex64x64[579] = true;
		tex64x64[580] = true;
		tex64x64[581] = true;
		tex64x64[582] = true;
		tex64x64[583] = true;
		tex64x64[584] = true;
		tex64x64[585] = true;
		tex64x64[586] = true;
		tex64x64[587] = true;
		tex64x64[588] = true;
		tex64x64[589] = true;
		tex64x64[590] = true;
		tex64x64[591] = true;
		tex64x64[592] = true;
		tex64x64[634] = true;
		tex64x64[635] = true;
		tex64x64[636] = true;
		tex64x64[646] = true;
		tex64x64[647] = true;
		tex64x64[648] = true;
		tex64x64[649] = true;
		tex64x64[653] = true;
		tex64x64[654] = true;
		tex64x64[655] = true;
		tex64x64[656] = true;
		tex64x64[657] = true;
		tex64x64[658] = true;
		tex64x64[659] = true;
		tex64x64[660] = true;
		tex64x64[661] = true;
		tex64x64[662] = true;
		tex64x64[663] = true;
		tex64x64[664] = true;
		tex64x64[665] = true;
		tex64x64[666] = true;
		tex64x64[667] = true;
		tex64x64[672] = true;
		tex64x64[673] = true;
		tex64x64[674] = true;
		tex64x64[676] = true;
		tex64x64[677] = true;
	}
	
}