package com.soulplayps.client.node.raster.sprite;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.IndexColorModel;

public class SpriteTools {

	public static int[] bufferedImageToPixels(BufferedImage image) {
		switch (image.getType()) {
			case BufferedImage.TYPE_INT_ARGB:
				return ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
			case BufferedImage.TYPE_3BYTE_BGR:
			case BufferedImage.TYPE_4BYTE_ABGR:
				return convertABGRtoPixels(image);
			case BufferedImage.TYPE_BYTE_INDEXED:
				IndexColorModel icm = (IndexColorModel) image.getColorModel();
				byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
				int[] pixels = new int[image.getWidth() * image.getHeight()];
				for (int i = 0, length = pixels.length; i < length; i++) {
					pixels[i] = icm.getRGB(data[i]);
				}
	
				return pixels;
			default:
				throw new RuntimeException("Unsupported type = " + image.getType());
		}
	}

	private static int[] convertABGRtoPixels(BufferedImage image) {
		final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		final int pixelsLength = pixels.length;
		final int width = image.getWidth();
		final int height = image.getHeight();
		final boolean hasAlphaChannel = image.getAlphaRaster() != null;

		int[] result = new int[width * height];
		int pixelPos = 0;
		if (hasAlphaChannel) {
			final int pixelLength = 4;
			for (int pixel = 0; pixel < pixelsLength; pixel += pixelLength) {
				int argb = 0;
				argb += (((int) pixels[pixel] & 0xff) << 24);
				argb += ((int) pixels[pixel + 1] & 0xff);
				argb += (((int) pixels[pixel + 2] & 0xff) << 8);
				argb += (((int) pixels[pixel + 3] & 0xff) << 16);
				result[pixelPos++] = argb;
			}
		} else {
			final int pixelLength = 3;
			for (int pixel = 0; pixel < pixelsLength; pixel += pixelLength) {
				int argb = 0;
				argb += -16777216;
				argb += ((int) pixels[pixel] & 0xff);
				argb += (((int) pixels[pixel + 1] & 0xff) << 8);
				argb += (((int) pixels[pixel + 2] & 0xff) << 16);
				result[pixelPos++] = argb;
			}
		}

		return result;
	}

}
