package com.soulplayps.client.node.raster;

import com.soulplayps.client.Client;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.opengl.GraphicsDisplay;

public final class Rasterizer {
	
	public static boolean testTopY = false;
	public static int DEFAULT_GROUND_TEXTURE = GraphicsDisplay.enabled ? 598 : 594;
	public static float depthBuffer[];
	private static int mipMapLevel;
	public static int textureAmount = 60;
	public static boolean aBoolean1462;
	private static boolean aBoolean1463;
	public static boolean aBoolean1464 = true;
	public static int anInt1465;
	public static int center_x;
	public static int center_y;
	private static int[] anIntArray1468;
	public static final int[] anIntArray1469;
	public static int SINE[];
	public static int COSINE[];
	public static float[] SINE_FLOAT;
	public static float[] COSINE_FLOAT;
	public static int lineOffsets[];
	private static int anInt1473;
	public static Background aBackgroundArray1474s[] = new Background[textureAmount];
	private static boolean[] aBooleanArray1475 = new boolean[textureAmount];
	private static int[] anIntArray1476 = new int[textureAmount];
	private static int anInt1477;
	private static int[][] anIntArrayArray1478;
	private static int[][] anIntArrayArray1479 = new int[textureAmount][];
	public static int anIntArray1480[] = new int[textureAmount];
	public static int anInt1481;
	public static int anIntArray1482[] = new int[0x10000];
	private static int[][] anIntArrayArray1483 = new int[textureAmount][];
	
	static {
		anIntArray1468 = new int[512];
		anIntArray1469 = new int[2048];
		SINE = new int[2048];
		COSINE = new int[2048];
		SINE_FLOAT = new float[2048];
		COSINE_FLOAT = new float[2048];
		for (int i = 1; i < 512; i++) {
			anIntArray1468[i] = 32768 / i;
		}
		for (int j = 1; j < 2048; j++) {
			anIntArray1469[j] = 0x10000 / j;
		}

		double magic = 2 * Math.PI / 2048;

		for (int k = 0; k < 2048; k++) {
			SINE[k] = (int) (65536D * Math.sin((double) k * magic));
			COSINE[k] = (int) (65536D * Math.cos((double) k * magic));

			SINE_FLOAT[k] = (float) Math.sin((double) k * magic);
			COSINE_FLOAT[k] = (float) Math.cos((double) k * magic);
		}
	}
	
	public static final void clearDepthBuffer() {
		if (depthBuffer == null || depthBuffer.length != Raster.width * Raster.height) {
			depthBuffer = new float[Raster.width * Raster.height];
		}
		for (int i = 0; i < depthBuffer.length; i++) {
			depthBuffer[i] = 0x7FFF << 16;
		}
	}
	
	public static void drawFog(int rgb, int begin, int end) {
		float length = end - begin;
		for (int index = 0; index < Raster.pixels.length; index++) {
			float factor = (depthBuffer[index] - begin) / length;
			Raster.pixels[index] = blend(Raster.pixels[index], rgb, factor);
		}
	}
	
	private static int blend(int c1, int c2, float factor) {
		if (factor >= 1f) {
			return c2;
		}
		if (factor <= 0f) {
			return c1;
		}
		
		int r1 = (c1 >> 16) & 0xff;
		int g1 = (c1 >> 8) & 0xff;
		int b1 = (c1) & 0xff;
		
		int r2 = (c2 >> 16) & 0xff;
		int g2 = (c2 >> 8) & 0xff;
		int b2 = (c2) & 0xff;
		
		int r3 = r2 - r1;
		int g3 = g2 - g1;
		int b3 = b2 - b1;
		
		int r = (int) (r1 + (r3 * factor));
		int g = (int) (g1 + (g3 * factor));
		int b = (int) (b1 + (b3 * factor));
		
		return (r << 16) + (g << 8) + b;
	}
	
	public static void setDefaultBounds() {
		lineOffsets = new int[Raster.height];
		for (int j = 0; j < Raster.height; j++) {
			lineOffsets[j] = Raster.width * j;
		}
		center_x = Raster.width / 2;
		center_y = Raster.height / 2;
	}

	public static void setViewport(int coordX, int coordY) {
		int offset = lineOffsets[0];
		int offY = offset / Raster.width;
		int offX = offset - offY * Raster.width;
		center_x = coordX - offX;
		center_y = coordY - offY;
	}

	public static void setBounds(int width, int height) {
		lineOffsets = new int[height];
		for (int l = 0; l < height; l++) {
			lineOffsets[l] = width * l;
		}
		center_x = width / 2;
		center_y = height / 2;
	}
	
	public static void method366() {
		anIntArrayArray1478 = null;
		for (int j = 0; j < textureAmount; j++) {
			anIntArrayArray1479[j] = null;
		}
	}
	
	public static void method367() {
		if (anIntArrayArray1478 == null) {
			anInt1477 = 20;
			anIntArrayArray1478 = new int[anInt1477][0x10000];
			for (int k = 0; k < textureAmount; k++) {
				anIntArrayArray1479[k] = null;
			}
		}
	}
	
	public static void unpack(RSArchive streamLoader, byte[] mediaIndex) {
		anInt1473 = 0;
		for (int index = 0; index < textureAmount; index++) {
			try {
				aBackgroundArray1474s[index] = new Background(streamLoader, String.valueOf(index), 0, mediaIndex);
				aBackgroundArray1474s[index].method357();
				anInt1473++;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static int method369(int texture) {
		if (anIntArray1476[texture] != 0) {
			return anIntArray1476[texture];
		}
		int r = 0;
		int g = 0;
		int b = 0;
		final int count = anIntArrayArray1483[texture].length;
		for (int index = 0; index < count; index++) {
			r += anIntArrayArray1483[texture][index] >> 16 & 0xff;
			g += anIntArrayArray1483[texture][index] >> 8 & 0xff;
			b += anIntArrayArray1483[texture][index] & 0xff;
		}
		int rgb = (r / count << 16) + (g / count << 8) + b / count;
		rgb = method373(rgb, 1.3999999999999999D);
		if (rgb == 0) {
			rgb = 1;
		}
		anIntArray1476[texture] = rgb;
		return rgb;
	}
	
	public static void method370(int texture) {
		try {
			if (anIntArrayArray1479[texture] == null) {
				return;
			}
			anIntArrayArray1478[anInt1477++] = anIntArrayArray1479[texture];
			anIntArrayArray1479[texture] = null;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static int[] method371(int texture) {
		if (texture >= anIntArray1480.length) {
			return null;
		}

		anIntArray1480[texture] = anInt1481++;
		if (anIntArrayArray1479[texture] != null) {
			return anIntArrayArray1479[texture];
		}
		int texels[];
		if (anInt1477 > 0) {
			texels = anIntArrayArray1478[--anInt1477];
			anIntArrayArray1478[anInt1477] = null;
		} else {
			int j = 0;
			int k = -1;
			for (int l = 0; l < anInt1473; l++) {
				if (anIntArrayArray1479[l] != null && (anIntArray1480[l] < j || k == -1)) {
					j = anIntArray1480[l];
					k = l;
				}
			}
			
			if (k == -1) {
				return null;
			}

			texels = anIntArrayArray1479[k];
			anIntArrayArray1479[k] = null;
		}
		anIntArrayArray1479[texture] = texels;
		Background background = aBackgroundArray1474s[texture];
		int ai1[] = anIntArrayArray1483[texture];
		if (background.anInt1452 == 64) {
			for (int j1 = 0; j1 < 128; j1++) {
				for (int j2 = 0; j2 < 128; j2++) {
					texels[j2 + (j1 << 7)] = ai1[background.aByteArray1450[(j2 >> 1) + ((j1 >> 1) << 6)] & 0xff];
				}
			}
		} else {
			for (int k1 = 0; k1 < 16384; k1++) {
				texels[k1] = ai1[background.aByteArray1450[k1] & 0xff];
			}
		}
		aBooleanArray1475[texture] = false;
		for (int l1 = 0; l1 < 16384; l1++) {
			texels[l1] &= 0xf8f8ff;
			int k2 = texels[l1];
			if (k2 == 0) {
				aBooleanArray1475[texture] = true;
			}
			texels[16384 + l1] = k2 - (k2 >>> 3) & 0xf8f8ff;
			texels[32768 + l1] = k2 - (k2 >>> 2) & 0xf8f8ff;
			texels[49152 + l1] = k2 - (k2 >>> 2) - (k2 >>> 3) & 0xf8f8ff;
		}
		return texels;
	}
	
	public static double getBrightness()
	{
		return Texture.brightness;
	}
	
	public static void method372(double value) {
		Texture.setBrightness(value);
		int pos = 0;
		for (int index = 0; index < 512; index++) {
			final double d1 = index / 8 / 64D + 0.0078125D;
			final double d2 = (index & 7) / 8D + 0.0625D;
			for (int i = 0; i < 128; i++) {
				final double c = i / 128D;
				double r = c;
				double g = c;
				double b = c;
				if (d2 != 0.0D) {
					double d7;
					if (c < 0.5D) {
						d7 = c * (1.0D + d2);
					} else {
						d7 = c + d2 - c * d2;
					}
					final double d8 = 2D * c - d7;
					double d9 = d1 + 0.33333333333333331D;
					if (d9 > 1.0D) {
						d9--;
					}
					final double d10 = d1;
					double d11 = d1 - 0.33333333333333331D;
					if (d11 < 0.0D) {
						d11++;
					}
					if (6D * d9 < 1.0D) {
						r = d8 + (d7 - d8) * 6D * d9;
					} else if (2D * d9 < 1.0D) {
						r = d7;
					} else if (3D * d9 < 2D) {
						r = d8 + (d7 - d8) * (0.66666666666666663D - d9) * 6D;
					} else {
						r = d8;
					}
					if (6D * d10 < 1.0D) {
						g = d8 + (d7 - d8) * 6D * d10;
					} else if (2D * d10 < 1.0D) {
						g = d7;
					} else if (3D * d10 < 2D) {
						g = d8 + (d7 - d8) * (0.66666666666666663D - d10) * 6D;
					} else {
						g = d8;
					}
					if (6D * d11 < 1.0D) {
						b = d8 + (d7 - d8) * 6D * d11;
					} else if (2D * d11 < 1.0D) {
						b = d7;
					} else if (3D * d11 < 2D) {
						b = d8 + (d7 - d8) * (0.66666666666666663D - d11) * 6D;
					} else {
						b = d8;
					}
				}
				final int red = (int) (r * 256D);
				final int green = (int) (g * 256D);
				final int blue = (int) (b * 256D);
				int color = (red << 16) + (green << 8) + blue;
				color = method373(color, value);
				if (color == 0) {
					color = 1;
				}
				anIntArray1482[pos++] = color;
			}
		}
		for (int index = 0; index < textureAmount; index++) {
			if (aBackgroundArray1474s[index] != null) {
				final int[] colors = aBackgroundArray1474s[index].anIntArray1451;
				anIntArrayArray1483[index] = new int[colors.length];
				for (int i = 0; i < colors.length; i++) {
					anIntArrayArray1483[index][i] = method373(colors[i], value);
					if ((anIntArrayArray1483[index][i] & 0xf8f8ff) == 0 && i != 0) {
						anIntArrayArray1483[index][i] = 1;
					}
				}
			}
		}
		for (int index = 0; index < textureAmount; index++) {
			method370(index);
		}
	}
	
	static int method373(int color, double amt) {
		double red = (color >> 16) / 256D;
		double green = (color >> 8 & 0xff) / 256D;
		double blue = (color & 0xff) / 256D;
		red = Math.pow(red, amt);
		green = Math.pow(green, amt);
		blue = Math.pow(blue, amt);
		final int red2 = (int) (red * 256D);
		final int green2 = (int) (green * 256D);
		final int blue2 = (int) (blue * 256D);
		return (red2 << 16) + (green2 << 8) + blue2;
	}
	
	private static int texelPos(int defaultIndex) {
		int x = (defaultIndex & 127) >> mipMapLevel;
		int y = (defaultIndex >> 7) >> mipMapLevel;
		return x + (y << (7 - mipMapLevel));
	}
	
	private static void setMipmapLevel(int y1, int y2, int y3, int x1, int x2, int x3, int tex) {
		int textureArea = x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2) >> 1;
		if (textureArea < 0) {
			textureArea = -textureArea;
		}
		if (textureArea > 16384) {
			mipMapLevel = 0;
		} else if (textureArea > 4096) {
			mipMapLevel = 1;
		} else if (textureArea > 1024) {
			mipMapLevel = 1;
		} else if (textureArea > 256) {
			mipMapLevel = 2;
		} else if (textureArea > 64) {
			mipMapLevel = 3;
		} else if (textureArea > 16) {
			mipMapLevel = 4;
		} else if (textureArea > 4) {
			mipMapLevel = 5;
		} else if (textureArea > 1) {
			mipMapLevel = 6;
		} else {
			mipMapLevel = 7;
		}
	}
	
	public static void drawMaterializedTriangle(int y1, int y2, int y3, int x1, int x2, int x3, int hsl1, int hsl2, int hsl3, int tx1, int tx2, int tx3, int ty1, int ty2, int ty3, int tz1, int tz2, int tz3, int tex, float z1, float z2, float z3) {
		if (!Client.enableHDTextures || Texture.get(tex) == null) {
			drawGouraudTriangle(y1, y2, y3, x1, x2, x3, hsl1, hsl2, hsl3, z1, z2, z3);
			return;
		}
		setMipmapLevel(y1, y2, y3, x1, x2, x3, tex);
		int[] texels = Texture.get(tex).mipmaps[mipMapLevel];
		tx2 = tx1 - tx2;
		ty2 = ty1 - ty2;
		tz2 = tz1 - tz2;
		tx3 -= tx1;
		ty3 -= ty1;
		tz3 -= tz1;
		int l4 = tx3 * ty1 - ty3 * tx1 << 14;
		int i5 = (int) (((long) (ty3 * tz1 - tz3 * ty1) << 3 << 14) / (long) Client.fov);
		int j5 = (int) (((long) (tz3 * tx1 - tx3 * tz1) << 14) / (long) Client.fov);
		int k5 = tx2 * ty1 - ty2 * tx1 << 14;
		int l5 = (int) (((long) (ty2 * tz1 - tz2 * ty1) << 3 << 14) / (long) Client.fov);
		int i6 = (int) (((long) (tz2 * tx1 - tx2 * tz1) << 14) / (long) Client.fov);
		int j6 = ty2 * tx3 - tx2 * ty3 << 14;
		int k6 = (int) (((long) (tz2 * ty3 - ty2 * tz3) << 3 << 14) / (long) Client.fov);
		int l6 = (int) (((long) (tx2 * tz3 - tz2 * tx3) << 14) / (long) Client.fov);
		int i7 = 0;
		int j7 = 0;
		float dz1 = 0;
		if (y2 != y1) {
			i7 = (x2 - x1 << 16) / (y2 - y1);
			dz1 = (z2 - z1) / (y2 - y1);
			j7 = (hsl2 - hsl1 << 15) / (y2 - y1);
		}
		int k7 = 0;
		int l7 = 0;
		float dz2 = 0;
		if (y3 != y2) {
			k7 = (x3 - x2 << 16) / (y3 - y2);
			dz2 = (z3 - z2) / (y3 - y2);
			l7 = (hsl3 - hsl2 << 15) / (y3 - y2);
		}
		int i8 = 0;
		int j8 = 0;
		float dz3 = 0;
		if (y3 != y1) {
			i8 = (x1 - x3 << 16) / (y1 - y3);
			dz3 = (z1 - z3) / (y1 - y3);
			j8 = (hsl1 - hsl3 << 15) / (y1 - y3);
		}
		if (y1 <= y2 && y1 <= y3) {
			if (y1 >= Raster.bottomY) {
				return;
			}
			if (y2 > Raster.bottomY) {
				y2 = Raster.bottomY;
			}
			if (y3 > Raster.bottomY) {
				y3 = Raster.bottomY;
			}
			if (y2 < y3) {
				x3 = x1 <<= 16;
				z3 = z1;
				hsl3 = hsl1 <<= 15;
				if (y1 < 0) {
					x3 -= i8 * y1;
					z1 -= dz1 * y1;
					x1 -= i7 * y1;
					hsl3 -= j8 * y1;
					hsl1 -= j7 * y1;
					y1 = 0;
				}
				x2 <<= 16;
				hsl2 <<= 15;
				if (y2 < 0) {
					x2 -= k7 * y2;
					hsl2 -= l7 * y2;
					z2 -= dz2 * y2;
					y2 = 0;
				}
				int k8 = y1 - center_y;
				l4 += j5 * k8;
				k5 += i6 * k8;
				j6 += l6 * k8;
				if (y1 != y2 && i8 < i7 || y1 == y2 && i8 > k7) {
					y3 -= y2;
					y2 -= y1;
					y1 = lineOffsets[y1];
					while (--y2 >= 0) {
						drawMaterializedScanline(Raster.pixels, texels, y1, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z3, z1);
						z3 += dz3;
						z1 += dz1;
						x3 += i8;
						x1 += i7;
						hsl3 += j8;
						hsl1 += j7;
						y1 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
					}
					while (--y3 >= 0) {
						drawMaterializedScanline(Raster.pixels, texels, y1, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z3, z2);
						x3 += i8;
						x2 += k7;
						hsl3 += j8;
						hsl2 += l7;
						z3 += dz3;
						z2 += dz2;
						y1 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
					}
					return;
				}
				y3 -= y2;
				y2 -= y1;
				y1 = lineOffsets[y1];
				while (--y2 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y1, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z1, z3);
					z3 += dz3;
					z1 += dz1;
					x3 += i8;
					x1 += i7;
					hsl3 += j8;
					hsl1 += j7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				while (--y3 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y1, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z2, z3);
					z3 += dz3;
					z2 += dz2;
					x3 += i8;
					x2 += k7;
					hsl3 += j8;
					hsl2 += l7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				return;
			}
			x2 = x1 <<= 16;
			hsl2 = hsl1 <<= 15;
			z2 = z1;
			if (y1 < 0) {
				x2 -= i8 * y1;
				x1 -= i7 * y1;
				z2 -= dz3 * y1;
				z1 -= dz1 * y1;
				hsl2 -= j8 * y1;
				hsl1 -= j7 * y1;
				y1 = 0;
			}
			x3 <<= 16;
			hsl3 <<= 15;
			if (y3 < 0) {
				x3 -= k7 * y3;
				z3 -= dz2 * y3;
				hsl3 -= l7 * y3;
				y3 = 0;
			}
			int l8 = y1 - center_y;
			l4 += j5 * l8;
			k5 += i6 * l8;
			j6 += l6 * l8;
			if (y1 != y3 && i8 < i7 || y1 == y3 && k7 > i7) {
				y2 -= y3;
				y3 -= y1;
				y1 = lineOffsets[y1];
				while (--y3 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y1, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z2, z1);
					x2 += i8;
					x1 += i7;
					z2 += dz3;
					z1 += dz1;
					hsl2 += j8;
					hsl1 += j7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				while (--y2 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y1, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z3, z1);
					z3 += dz2;
					z1 += dz1;
					x3 += k7;
					x1 += i7;
					hsl3 += l7;
					hsl1 += j7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				return;
			}
			y2 -= y3;
			y3 -= y1;
			y1 = lineOffsets[y1];
			while (--y3 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y1, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z1, z2);
				x2 += i8;
				x1 += i7;
				z2 += dz3;
				z1 += dz1;
				hsl2 += j8;
				hsl1 += j7;
				y1 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			while (--y2 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y1, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z1, z3);
				x3 += k7;
				x1 += i7;
				z3 += dz2;
				z1 += dz1;
				hsl3 += l7;
				hsl1 += j7;
				y1 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			return;
		}
		if (y2 <= y3) {
			if (y2 >= Raster.bottomY) {
				return;
			}
			if (y3 > Raster.bottomY) {
				y3 = Raster.bottomY;
			}
			if (y1 > Raster.bottomY) {
				y1 = Raster.bottomY;
			}
			if (y3 < y1) {
				x1 = x2 <<= 16;
				hsl1 = hsl2 <<= 15;
				z1 = z2;
				if (y2 < 0) {
					x1 -= i7 * y2;
					x2 -= k7 * y2;
					z1 -= dz1 * y2;
					z2 -= dz2 * y2;
					hsl1 -= j7 * y2;
					hsl2 -= l7 * y2;
					y2 = 0;
				}
				x3 <<= 16;
				hsl3 <<= 15;
				if (y3 < 0) {
					x3 -= i8 * y3;
					hsl3 -= j8 * y3;
					z3 -= dz3 * y3;
					y3 = 0;
				}
				int i9 = y2 - center_y;
				l4 += j5 * i9;
				k5 += i6 * i9;
				j6 += l6 * i9;
				if (y2 != y3 && i7 < k7 || y2 == y3 && i7 > i8) {
					y1 -= y3;
					y3 -= y2;
					y2 = lineOffsets[y2];
					while (--y3 >= 0) {
						drawMaterializedScanline(Raster.pixels, texels, y2, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z1, z2);
						x1 += i7;
						x2 += k7;
						z1 += dz1;
						z2 += dz2;
						hsl1 += j7;
						hsl2 += l7;
						y2 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
					}
					while (--y1 >= 0) {
						drawMaterializedScanline(Raster.pixels, texels, y2, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z1, z3);
						x1 += i7;
						x3 += i8;
						z1 += dz1;
						z3 += dz3;
						hsl1 += j7;
						hsl3 += j8;
						y2 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
					}
					return;
				}
				y1 -= y3;
				y3 -= y2;
				y2 = lineOffsets[y2];
				while (--y3 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y2, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z2, z1);
					x1 += i7;
					x2 += k7;
					z1 += dz1;
					z2 += dz2;
					hsl1 += j7;
					hsl2 += l7;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				while (--y1 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y2, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z3, z1);
					x1 += i7;
					x3 += i8;
					z1 += dz1;
					z3 += dz3;
					hsl1 += j7;
					hsl3 += j8;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				return;
			}
			x3 = x2 <<= 16;
			z3 = z2;
			hsl3 = hsl2 <<= 15;
			if (y2 < 0) {
				x3 -= i7 * y2;
				x2 -= k7 * y2;
				z3 -= dz1 * y2;
				z2 -= dz2 * y2;
				hsl3 -= j7 * y2;
				hsl2 -= l7 * y2;
				y2 = 0;
			}
			x1 <<= 16;
			hsl1 <<= 15;
			if (y1 < 0) {
				x1 -= i8 * y1;
				z1 -= dz3 * y1;
				hsl1 -= j8 * y1;
				y1 = 0;
			}
			int j9 = y2 - center_y;
			l4 += j5 * j9;
			k5 += i6 * j9;
			j6 += l6 * j9;
			if (i7 < k7) {
				y3 -= y1;
				y1 -= y2;
				y2 = lineOffsets[y2];
				while (--y1 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y2, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z3, z2);
					x3 += i7;
					x2 += k7;
					z3 += dz1;
					z2 += dz2;
					hsl3 += j7;
					hsl2 += l7;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				while (--y3 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y2, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z1, z2);
					x1 += i8;
					x2 += k7;
					z1 += dz3;
					z2 += dz2;
					hsl1 += j8;
					hsl2 += l7;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				return;
			}
			y3 -= y1;
			y1 -= y2;
			y2 = lineOffsets[y2];
			while (--y1 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y2, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z2, z3);
				x3 += i7;
				x2 += k7;
				z3 += dz1;
				z2 += dz2;
				hsl3 += j7;
				hsl2 += l7;
				y2 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			while (--y3 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y2, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z2, z1);
				x1 += i8;
				x2 += k7;
				z1 += dz3;
				z2 += dz2;
				hsl1 += j8;
				hsl2 += l7;
				y2 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			return;
		}
		if (y3 >= Raster.bottomY) {
			return;
		}
		if (y1 > Raster.bottomY) {
			y1 = Raster.bottomY;
		}
		if (y2 > Raster.bottomY) {
			y2 = Raster.bottomY;
		}
		if (y1 < y2) {
			x2 = x3 <<= 16;
			z2 = z3;
			hsl2 = hsl3 <<= 15;
			if (y3 < 0) {
				x2 -= k7 * y3;
				x3 -= i8 * y3;
				z2 -= dz2 * y3;
				z3 -= dz3 * y3;
				hsl2 -= l7 * y3;
				hsl3 -= j8 * y3;
				y3 = 0;
			}
			x1 <<= 16;
			hsl1 <<= 15;
			if (y1 < 0) {
				x1 -= i7 * y1;
				z1 -= dz1 * y1;
				hsl1 -= j7 * y1;
				y1 = 0;
			}
			int k9 = y3 - center_y;
			l4 += j5 * k9;
			k5 += i6 * k9;
			j6 += l6 * k9;
			if (k7 < i8) {
				y2 -= y1;
				y1 -= y3;
				y3 = lineOffsets[y3];
				while (--y1 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y3, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z2, z3);
					x2 += k7;
					x3 += i8;
					z2 += dz2;
					z3 += dz3;
					hsl2 += l7;
					hsl3 += j8;
					y3 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				while (--y2 >= 0) {
					drawMaterializedScanline(Raster.pixels, texels, y3, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z2, z1);
					x2 += k7;
					x1 += i7;
					z2 += dz2;
					z1 += dz1;
					hsl2 += l7;
					hsl1 += j7;
					y3 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
				}
				return;
			}
			y2 -= y1;
			y1 -= y3;
			y3 = lineOffsets[y3];
			while (--y1 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y3, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z3, z2);
				x2 += k7;
				x3 += i8;
				z2 += dz2;
				z3 += dz3;
				hsl2 += l7;
				hsl3 += j8;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			while (--y2 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y3, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z1, z2);
				x2 += k7;
				x1 += i7;
				z2 += dz2;
				z1 += dz1;
				hsl2 += l7;
				hsl1 += j7;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			return;
		}
		x1 = x3 <<= 16;
		z1 = z3;
		hsl1 = hsl3 <<= 15;
		if (y3 < 0) {
			x1 -= k7 * y3;
			x3 -= i8 * y3;
			z1 -= dz2 * y3;
			z3 -= dz3 * y3;
			hsl1 -= l7 * y3;
			hsl3 -= j8 * y3;
			y3 = 0;
		}
		x2 <<= 16;
		hsl2 <<= 15;
		if (y2 < 0) {
			x2 -= i7 * y2;
			z2 -= dz1 * y2;
			hsl2 -= j7 * y2;
			y2 = 0;
		}
		int l9 = y3 - center_y;
		l4 += j5 * l9;
		k5 += i6 * l9;
		j6 += l6 * l9;
		if (k7 < i8) {
			y1 -= y2;
			y2 -= y3;
			y3 = lineOffsets[y3];
			while (--y2 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y3, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z1, z3);
				x1 += k7;
				x3 += i8;
				z1 += dz2;
				z3 += dz3;
				hsl1 += l7;
				hsl3 += j8;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			while (--y1 >= 0) {
				drawMaterializedScanline(Raster.pixels, texels, y3, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7, l4, k5, j6, i5, l5, k6, z2, z3);
				x2 += i7;
				x3 += i8;
				z2 += dz1;
				z3 += dz3;
				hsl2 += j7;
				hsl3 += j8;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
			}
			return;
		}
		y1 -= y2;
		y2 -= y3;
		y3 = lineOffsets[y3];
		while (--y2 >= 0) {
			drawMaterializedScanline(Raster.pixels, texels, y3, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7, l4, k5, j6, i5, l5, k6, z3, z1);
			x1 += k7;
			x3 += i8;
			z1 += dz2;
			z3 += dz3;
			hsl1 += l7;
			hsl3 += j8;
			y3 += Raster.width;
			l4 += j5;
			k5 += i6;
			j6 += l6;
		}
		while (--y1 >= 0) {
			drawMaterializedScanline(Raster.pixels, texels, y3, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7, l4, k5, j6, i5, l5, k6, z3, z2);
			x2 += i7;
			x3 += i8;
			z2 += dz1;
			z3 += dz3;
			hsl2 += j7;
			hsl3 += j8;
			y3 += Raster.width;
			l4 += j5;
			k5 += i6;
			j6 += l6;
		}
	}
	
	private static final void drawMaterializedScanline(int[] dest, int[] texels, int offset, int x1, int x2, int hsl1, int hsl2, int t1, int t2, int t3, int t4, int t5, int t6, float z1, float z2) {
		if (x2 <= x1) {
			return;
		}
		int texPos = 0;
		int rgb = 0;
		z2 = (z2 - z1) / (x2 - x1);
		if (aBoolean1462) {
			if (x2 > Raster.bottomX) {
				x2 = Raster.bottomX;
			}
			if (x1 < 0) {
				z1 -= x1 * z2;
				x1 = 0;
			}
		}
		if (x1 < x2) {
			offset += x1;
			int n = x2 - x1 >> 2;
			int dhsl = 0;
			if (n > 0) {
				dhsl = (hsl2 - hsl1) * anIntArray1468[n] >> 15;
			}
			int dist = x1 - center_x;
			t1 += (t4 >> 3) * dist;
			t2 += (t5 >> 3) * dist;
			t3 += (t6 >> 3) * dist;
			int i_57_ = t3 >> 14;
			int i_58_;
			int i_59_;
			if (i_57_ != 0) {
				i_58_ = t1 / i_57_;
				i_59_ = t2 / i_57_;
			} else {
				i_58_ = 0;
				i_59_ = 0;
			}
			t1 += t4;
			t2 += t5;
			t3 += t6;
			i_57_ = t3 >> 14;
			int i_60_;
			int i_61_;
			if (i_57_ != 0) {
				i_60_ = t1 / i_57_;
				i_61_ = t2 / i_57_;
			} else {
				i_60_ = 0;
				i_61_ = 0;
			}
			texPos = (i_58_ << 18) + i_59_;
			int dtexPos = (i_60_ - i_58_ >> 3 << 18) + (i_61_ - i_59_ >> 3);
			n >>= 1;
			int light;
			if (n > 0) {
				do {
					hsl1 += dhsl;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					hsl1 += dhsl;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
					i_58_ = i_60_;
					i_59_ = i_61_;
					t1 += t4;
					t2 += t5;
					t3 += t6;
					i_57_ = t3 >> 14;
					if (i_57_ != 0) {
						i_60_ = t1 / i_57_;
						i_61_ = t2 / i_57_;
					} else {
						i_60_ = 0;
						i_61_ = 0;
					}
					texPos = (i_58_ << 18) + i_59_;
					dtexPos = (i_60_ - i_58_ >> 3 << 18) + (i_61_ - i_59_ >> 3);
				} while (--n > 0);
			}
			n = x2 - x1 & 7;
			if (n > 0) {
				do {
					if ((n & 3) == 0) {
						hsl1 += dhsl;
					}
					rgb = texels[texelPos((texPos & 0x3f80) + (texPos >>> 25))];
					light = ((hsl1 >> 8 & 0x7f) << 1) * (((rgb >> 16 & 0xff) + (rgb >> 8 & 0xff) + (rgb & 0xff)) / 3) / 384;
					if (light > 127) {
						light = 127;
					}
					texPos += dtexPos;
					if (z1 < depthBuffer[offset]) {
						depthBuffer[offset] = z1;
						dest[offset] = anIntArray1482[(hsl1 >> 8 & 0xff80) | light];
					}
					z1 += z2;
					offset++;
				} while (--n > 0);
			}
		}
	}
	
	public static void drawGouraudTriangle(int y1, int y2, int y3, int x1, int x2, int x3, int hsl1, int hsl2, int hsl3, float z1, float z2, float z3) {
		try {
			if (!aBoolean1464) {
				drawGouraudTriangle(y1, y2, y3, x1, x2, x3, hsl1, hsl2, hsl3);
				return;
			}
			final int rgb1 = anIntArray1482[hsl1];
			final int rgb2 = anIntArray1482[hsl2];
			final int rgb3 = anIntArray1482[hsl3];
			int r1 = rgb1 >> 16 & 0xff;
			int g1 = rgb1 >> 8 & 0xff;
			int b1 = rgb1 & 0xff;
			int r2 = rgb2 >> 16 & 0xff;
			int g2 = rgb2 >> 8 & 0xff;
			int b2 = rgb2 & 0xff;
			int r3 = rgb3 >> 16 & 0xff;
			int g3 = rgb3 >> 8 & 0xff;
			int b3 = rgb3 & 0xff;
			int dx1 = 0;
			float dz1 = 0;
			int dr1 = 0;
			int dg1 = 0;
			int db1 = 0;
			if (y2 != y1) {
				final int d = (y2 - y1);
				dx1 = (x2 - x1 << 16) / d;
				dz1 = (z2 - z1) / d;
				dr1 = (r2 - r1 << 16) / d;
				dg1 = (g2 - g1 << 16) / d;
				db1 = (b2 - b1 << 16) / d;
			}
			int dx2 = 0;
			float dz2 = 0;
			int dr2 = 0;
			int dg2 = 0;
			int db2 = 0;
			if (y3 != y2) {
				final int d = (y3 - y2);
				dx2 = (x3 - x2 << 16) / d;
				dz2 = (z3 - z2) / d;
				dr2 = (r3 - r2 << 16) / d;
				dg2 = (g3 - g2 << 16) / d;
				db2 = (b3 - b2 << 16) / d;
			}
			int dx3 = 0;
			float dz3 = 0;
			int dr3 = 0;
			int dg3 = 0;
			int db3 = 0;
			if (y3 != y1) {
				final int d = (y1 - y3);
				dx3 = (x1 - x3 << 16) / d;
				dz3 = (z1 - z3) / d;
				dr3 = (r1 - r3 << 16) / d;
				dg3 = (g1 - g3 << 16) / d;
				db3 = (b1 - b3 << 16) / d;
			}
			if (y1 <= y2 && y1 <= y3) {
				if (y1 >= Raster.bottomY) {
					return;
				}
				if (y2 > Raster.bottomY) {
					y2 = Raster.bottomY;
				}
				if (y3 > Raster.bottomY) {
					y3 = Raster.bottomY;
				}
				if (testTopY) {
					if (y1 < Raster.topY) {
						y1 = Raster.topY;
					}
					if (y2 < Raster.topY) {
						y2 = Raster.topY;
					}
					if (y3 < Raster.topY) {
						y3 = Raster.topY;
					}
				}
				if (y2 < y3) {
					x3 = x1 <<= 16;
					z3 = z1;
					r3 = r1 <<= 16;
					g3 = g1 <<= 16;
					b3 = b1 <<= 16;
					if (y1 < 0) {
						x3 -= dx3 * y1;
						x1 -= dx1 * y1;
						z3 -= dz3 * y1;
						z1 -= dz1 * y1;
						r3 -= dr3 * y1;
						g3 -= dg3 * y1;
						b3 -= db3 * y1;
						r1 -= dr1 * y1;
						g1 -= dg1 * y1;
						b1 -= db1 * y1;
						y1 = 0;
					}
					x2 <<= 16;
					r2 <<= 16;
					g2 <<= 16;
					b2 <<= 16;
					if (y2 < 0) {
						x2 -= dx2 * y2;
						z2 -= dz2 * y2;
						r2 -= dr2 * y2;
						g2 -= dg2 * y2;
						b2 -= db2 * y2;
						y2 = 0;
					}
					if (y1 != y2 && dx3 < dx1 || y1 == y2 && dx3 > dx2) {
						y3 -= y2;
						y2 -= y1;
						for (y1 = lineOffsets[y1]; --y2 >= 0; y1 += Raster.width) {
							drawGouraudScanline(Raster.pixels, y1, x3 >> 16, x1 >> 16, r3, g3, b3, r1, g1, b1, z3, z1);
							x3 += dx3;
							x1 += dx1;
							z3 += dz3;
							z1 += dz1;
							r3 += dr3;
							g3 += dg3;
							b3 += db3;
							r1 += dr1;
							g1 += dg1;
							b1 += db1;
						}
						while (--y3 >= 0) {
							drawGouraudScanline(Raster.pixels, y1, x3 >> 16, x2 >> 16, r3, g3, b3, r2, g2, b2, z3, z2);
							x3 += dx3;
							x2 += dx2;
							z3 += dz3;
							z2 += dz2;
							r3 += dr3;
							g3 += dg3;
							b3 += db3;
							r2 += dr2;
							g2 += dg2;
							b2 += db2;
							y1 += Raster.width;
						}
						return;
					}
					y3 -= y2;
					y2 -= y1;
					for (y1 = lineOffsets[y1]; --y2 >= 0; y1 += Raster.width) {
						drawGouraudScanline(Raster.pixels, y1, x1 >> 16, x3 >> 16, r1, g1, b1, r3, g3, b3, z1, z3);
						x3 += dx3;
						x1 += dx1;
						z3 += dz3;
						z1 += dz1;
						r3 += dr3;
						g3 += dg3;
						b3 += db3;
						r1 += dr1;
						g1 += dg1;
						b1 += db1;
					}
					while (--y3 >= 0) {
						drawGouraudScanline(Raster.pixels, y1, x2 >> 16, x3 >> 16, r2, g2, b2, r3, g3, b3, z2, z3);
						x3 += dx3;
						x2 += dx2;
						z3 += dz3;
						z2 += dz2;
						r3 += dr3;
						g3 += dg3;
						b3 += db3;
						r2 += dr2;
						g2 += dg2;
						b2 += db2;
						y1 += Raster.width;
					}
					return;
				}
				x2 = x1 <<= 16;
				z2 = z1;
				r2 = r1 <<= 16;
				g2 = g1 <<= 16;
				b2 = b1 <<= 16;
				if (y1 < 0) {
					x2 -= dx3 * y1;
					x1 -= dx1 * y1;
					z2 -= dz3 * y1;
					z1 -= dz1 * y1;
					r2 -= dr3 * y1;
					g2 -= dg3 * y1;
					b2 -= db3 * y1;
					r1 -= dr1 * y1;
					g1 -= dg1 * y1;
					b1 -= db1 * y1;
					y1 = 0;
				}
				x3 <<= 16;
				r3 <<= 16;
				g3 <<= 16;
				b3 <<= 16;
				if (y3 < 0) {
					x3 -= dx2 * y3;
					z3 -= dz2 * y3;
					r3 -= dr2 * y3;
					g3 -= dg2 * y3;
					b3 -= db2 * y3;
					y3 = 0;
				}
				if (y1 != y3 && dx3 < dx1 || y1 == y3 && dx2 > dx1) {
					y2 -= y3;
					y3 -= y1;
					for (y1 = lineOffsets[y1]; --y3 >= 0; y1 += Raster.width) {
						drawGouraudScanline(Raster.pixels, y1, x2 >> 16, x1 >> 16, r2, g2, b2, r1, g1, b1, z2, z1);
						x2 += dx3;
						x1 += dx1;
						z2 += dz3;
						z1 += dz1;
						r2 += dr3;
						g2 += dg3;
						b2 += db3;
						r1 += dr1;
						g1 += dg1;
						b1 += db1;
					}
					while (--y2 >= 0) {
						drawGouraudScanline(Raster.pixels, y1, x3 >> 16, x1 >> 16, r3, g3, b3, r1, g1, b1, z3, z1);
						x3 += dx2;
						x1 += dx1;
						z3 += dz2;
						z1 += dz1;
						r3 += dr2;
						g3 += dg2;
						b3 += db2;
						r1 += dr1;
						g1 += dg1;
						b1 += db1;
						y1 += Raster.width;
					}
					return;
				}
				y2 -= y3;
				y3 -= y1;
				for (y1 = lineOffsets[y1]; --y3 >= 0; y1 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y1, x1 >> 16, x2 >> 16, r1, g1, b1, r2, g2, b2, z1, z2);
					x2 += dx3;
					x1 += dx1;
					z2 += dz3;
					z1 += dz1;
					r2 += dr3;
					g2 += dg3;
					b2 += db3;
					r1 += dr1;
					g1 += dg1;
					b1 += db1;
				}
				while (--y2 >= 0) {
					drawGouraudScanline(Raster.pixels, y1, x1 >> 16, x3 >> 16, r1, g1, b1, r3, g3, b3, z1, z3);
					x3 += dx2;
					x1 += dx1;
					z3 += dz2;
					z1 += dz1;
					r3 += dr2;
					g3 += dg2;
					b3 += db2;
					r1 += dr1;
					g1 += dg1;
					b1 += db1;
					y1 += Raster.width;
				}
				return;
			}
			if (y2 <= y3) {
				if (y2 >= Raster.bottomY) {
					return;
				}
				if (y3 > Raster.bottomY) {
					y3 = Raster.bottomY;
				}
				if (y1 > Raster.bottomY) {
					y1 = Raster.bottomY;
				}
				if (testTopY) {
					if (y2 < Raster.topY) {
						y2 = Raster.topY;
					}
					if (y3 < Raster.topY) {
						y3 = Raster.topY;
					}
					if (y1 < Raster.topY) {
						y1 = Raster.topY;
					}
				}
				if (y3 < y1) {
					x1 = x2 <<= 16;
					z1 = z2;
					r1 = r2 <<= 16;
					g1 = g2 <<= 16;
					b1 = b2 <<= 16;
					if (y2 < 0) {
						x1 -= dx1 * y2;
						x2 -= dx2 * y2;
						z1 -= dz1 * y2;
						z2 -= dz2 * y2;
						r1 -= dr1 * y2;
						g1 -= dg1 * y2;
						b1 -= db1 * y2;
						r2 -= dr2 * y2;
						g2 -= dg2 * y2;
						b2 -= db2 * y2;
						y2 = 0;
					}
					x3 <<= 16;
					r3 <<= 16;
					g3 <<= 16;
					b3 <<= 16;
					if (y3 < 0) {
						x3 -= dx3 * y3;
						z3 -= dz3 * y3;
						r3 -= dr3 * y3;
						g3 -= dg3 * y3;
						b3 -= db3 * y3;
						y3 = 0;
					}
					if (y2 != y3 && dx1 < dx2 || y2 == y3 && dx1 > dx3) {
						y1 -= y3;
						y3 -= y2;
						for (y2 = lineOffsets[y2]; --y3 >= 0; y2 += Raster.width) {
							drawGouraudScanline(Raster.pixels, y2, x1 >> 16, x2 >> 16, r1, g1, b1, r2, g2, b2, z1, z2);
							x1 += dx1;
							x2 += dx2;
							z1 += dz1;
							z2 += dz2;
							r1 += dr1;
							g1 += dg1;
							b1 += db1;
							r2 += dr2;
							g2 += dg2;
							b2 += db2;
						}
						while (--y1 >= 0) {
							drawGouraudScanline(Raster.pixels, y2, x1 >> 16, x3 >> 16, r1, g1, b1, r3, g3, b3, z1, z3);
							x1 += dx1;
							x3 += dx3;
							z1 += dz1;
							z3 += dz3;
							r1 += dr1;
							g1 += dg1;
							b1 += db1;
							r3 += dr3;
							g3 += dg3;
							b3 += db3;
							y2 += Raster.width;
						}
						return;
					}
					y1 -= y3;
					y3 -= y2;
					for (y2 = lineOffsets[y2]; --y3 >= 0; y2 += Raster.width) {
						drawGouraudScanline(Raster.pixels, y2, x2 >> 16, x1 >> 16, r2, g2, b2, r1, g1, b1, z2, z1);
						x1 += dx1;
						x2 += dx2;
						z1 += dz1;
						z2 += dz2;
						r1 += dr1;
						g1 += dg1;
						b1 += db1;
						r2 += dr2;
						g2 += dg2;
						b2 += db2;
					}
					while (--y1 >= 0) {
						drawGouraudScanline(Raster.pixels, y2, x3 >> 16, x1 >> 16, r3, g3, b3, r1, g1, b1, z3, z1);
						x1 += dx1;
						x3 += dx3;
						z1 += dz1;
						z3 += dz3;
						r1 += dr1;
						g1 += dg1;
						b1 += db1;
						r3 += dr3;
						g3 += dg3;
						b3 += db3;
						y2 += Raster.width;
					}
					return;
				}
				x3 = x2 <<= 16;
				z3 = z2;
				r3 = r2 <<= 16;
				g3 = g2 <<= 16;
				b3 = b2 <<= 16;
				if (y2 < 0) {
					x3 -= dx1 * y2;
					x2 -= dx2 * y2;
					z3 -= dz1 * y2;
					z2 -= dz2 * y2;
					r3 -= dr1 * y2;
					g3 -= dg1 * y2;
					b3 -= db1 * y2;
					r2 -= dr2 * y2;
					g2 -= dg2 * y2;
					b2 -= db2 * y2;
					y2 = 0;
				}
				x1 <<= 16;
				r1 <<= 16;
				g1 <<= 16;
				b1 <<= 16;
				if (y1 < 0) {
					x1 -= dx3 * y1;
					z1 -= dz3 * y1;
					r1 -= dr3 * y1;
					g1 -= dg3 * y1;
					b1 -= db3 * y1;
					y1 = 0;
				}
				if (dx1 < dx2) {
					y3 -= y1;
					y1 -= y2;
					for (y2 = lineOffsets[y2]; --y1 >= 0; y2 += Raster.width) {
						drawGouraudScanline(Raster.pixels, y2, x3 >> 16, x2 >> 16, r3, g3, b3, r2, g2, b2, z3, z2);
						x3 += dx1;
						x2 += dx2;
						z3 += dz1;
						z2 += dz2;
						r3 += dr1;
						g3 += dg1;
						b3 += db1;
						r2 += dr2;
						g2 += dg2;
						b2 += db2;
					}
					while (--y3 >= 0) {
						drawGouraudScanline(Raster.pixels, y2, x1 >> 16, x2 >> 16, r1, g1, b1, r2, g2, b2, z1, z2);
						x1 += dx3;
						x2 += dx2;
						z1 += dz3;
						z2 += dz2;
						r1 += dr3;
						g1 += dg3;
						b1 += db3;
						r2 += dr2;
						g2 += dg2;
						b2 += db2;
						y2 += Raster.width;
					}
					return;
				}
				y3 -= y1;
				y1 -= y2;
				for (y2 = lineOffsets[y2]; --y1 >= 0; y2 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y2, x2 >> 16, x3 >> 16, r2, g2, b2, r3, g3, b3, z2, z3);
					x3 += dx1;
					x2 += dx2;
					z3 += dz1;
					z2 += dz2;
					r3 += dr1;
					g3 += dg1;
					b3 += db1;
					r2 += dr2;
					g2 += dg2;
					b2 += db2;
				}
				while (--y3 >= 0) {
					drawGouraudScanline(Raster.pixels, y2, x2 >> 16, x1 >> 16, r2, g2, b2, r1, g1, b1, z2, z1);
					x1 += dx3;
					x2 += dx2;
					z1 += dz3;
					z2 += dz2;
					r1 += dr3;
					g1 += dg3;
					b1 += db3;
					r2 += dr2;
					g2 += dg2;
					b2 += db2;
					y2 += Raster.width;
				}
				return;
			}
			if (y3 >= Raster.bottomY) {
				return;
			}
			if (y1 > Raster.bottomY) {
				y1 = Raster.bottomY;
			}
			if (y2 > Raster.bottomY) {
				y2 = Raster.bottomY;
			}
			if (testTopY) {
				if (y3 < Raster.topY) {
					y3 = Raster.topY;
				}
				if (y1 < Raster.topY) {
					y1 = Raster.topY;
				}
				if (y2 < Raster.topY) {
					y2 = Raster.topY;
				}
			}
			if (y1 < y2) {
				x2 = x3 <<= 16;
				z2 = z3;
				r2 = r3 <<= 16;
				g2 = g3 <<= 16;
				b2 = b3 <<= 16;
				if (y3 < 0) {
					x2 -= dx2 * y3;
					x3 -= dx3 * y3;
					z2 -= dz2 * y3;
					z3 -= dz3 * y3;
					r2 -= dr2 * y3;
					g2 -= dg2 * y3;
					b2 -= db2 * y3;
					r3 -= dr3 * y3;
					g3 -= dg3 * y3;
					b3 -= db3 * y3;
					y3 = 0;
				}
				x1 <<= 16;
				r1 <<= 16;
				g1 <<= 16;
				b1 <<= 16;
				if (y1 < 0) {
					x1 -= dx1 * y1;
					z1 -= dz1 * y1;
					r1 -= dr1 * y1;
					g1 -= dg1 * y1;
					b1 -= db1 * y1;
					y1 = 0;
				}
				if (dx2 < dx3) {
					y2 -= y1;
					y1 -= y3;
					for (y3 = lineOffsets[y3]; --y1 >= 0; y3 += Raster.width) {
						drawGouraudScanline(Raster.pixels, y3, x2 >> 16, x3 >> 16, r2, g2, b2, r3, g3, b3, z2, z3);
						x2 += dx2;
						x3 += dx3;
						z2 += dz2;
						z3 += dz3;
						r2 += dr2;
						g2 += dg2;
						b2 += db2;
						r3 += dr3;
						g3 += dg3;
						b3 += db3;
					}
					while (--y2 >= 0) {
						drawGouraudScanline(Raster.pixels, y3, x2 >> 16, x1 >> 16, r2, g2, b2, r1, g1, b1, z2, z1);
						x2 += dx2;
						x1 += dx1;
						z2 += dz2;
						z1 += dz1;
						r2 += dr2;
						g2 += dg2;
						b2 += db2;
						r1 += dr1;
						g1 += dg1;
						b1 += db1;
						y3 += Raster.width;
					}
					return;
				}
				y2 -= y1;
				y1 -= y3;
				for (y3 = lineOffsets[y3]; --y1 >= 0; y3 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y3, x3 >> 16, x2 >> 16, r3, g3, b3, r2, g2, b2, z3, z2);
					x2 += dx2;
					x3 += dx3;
					z2 += dz2;
					z3 += dz3;
					r2 += dr2;
					g2 += dg2;
					b2 += db2;
					r3 += dr3;
					g3 += dg3;
					b3 += db3;
				}
				while (--y2 >= 0) {
					drawGouraudScanline(Raster.pixels, y3, x1 >> 16, x2 >> 16, r1, g1, b1, r2, g2, b2, z1, z2);
					x2 += dx2;
					x1 += dx1;
					z2 += dz2;
					z1 += dz1;
					r2 += dr2;
					g2 += dg2;
					b2 += db2;
					r1 += dr1;
					g1 += dg1;
					b1 += db1;
					y3 += Raster.width;
				}
				return;
			}
			x1 = x3 <<= 16;
			z1 = z3;
			r1 = r3 <<= 16;
			g1 = g3 <<= 16;
			b1 = b3 <<= 16;
			if (y3 < 0) {
				x1 -= dx2 * y3;
				x3 -= dx3 * y3;
				z1 -= dz2 * y3;
				z3 -= dz3 * y3;
				r1 -= dr2 * y3;
				g1 -= dg2 * y3;
				b1 -= db2 * y3;
				r3 -= dr3 * y3;
				g3 -= dg3 * y3;
				b3 -= db3 * y3;
				y3 = 0;
			}
			x2 <<= 16;
			r2 <<= 16;
			g2 <<= 16;
			b2 <<= 16;
			if (y2 < 0) {
				x2 -= dx1 * y2;
				z2 -= dz1 * y2;
				r2 -= dr1 * y2;
				g2 -= dg1 * y2;
				b2 -= db1 * y2;
				y2 = 0;
			}
			if (dx2 < dx3) {
				y1 -= y2;
				y2 -= y3;
				for (y3 = lineOffsets[y3]; --y2 >= 0; y3 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y3, x1 >> 16, x3 >> 16, r1, g1, b1, r3, g3, b3, z1, z3);
					x1 += dx2;
					x3 += dx3;
					z1 += dz2;
					z3 += dz3;
					r1 += dr2;
					g1 += dg2;
					b1 += db2;
					r3 += dr3;
					g3 += dg3;
					b3 += db3;
				}
				while (--y1 >= 0) {
					drawGouraudScanline(Raster.pixels, y3, x2 >> 16, x3 >> 16, r2, g2, b2, r3, g3, b3, z2, z3);
					x2 += dx1;
					x3 += dx3;
					z2 += dz1;
					z3 += dz3;
					r2 += dr1;
					g2 += dg1;
					b2 += db1;
					r3 += dr3;
					g3 += dg3;
					b3 += db3;
					y3 += Raster.width;
				}
				return;
			}
			y1 -= y2;
			y2 -= y3;
			for (y3 = lineOffsets[y3]; --y2 >= 0; y3 += Raster.width) {
				drawGouraudScanline(Raster.pixels, y3, x3 >> 16, x1 >> 16, r3, g3, b3, r1, g1, b1, z3, z1);
				x1 += dx2;
				x3 += dx3;
				z1 += dz2;
				z3 += dz3;
				r1 += dr2;
				g1 += dg2;
				b1 += db2;
				r3 += dr3;
				g3 += dg3;
				b3 += db3;
			}
			while (--y1 >= 0) {
				drawGouraudScanline(Raster.pixels, y3, x3 >> 16, x2 >> 16, r3, g3, b3, r2, g2, b2, z3, z2);
				x2 += dx1;
				x3 += dx3;
				z2 += dz1;
				z3 += dz3;
				r2 += dr1;
				g2 += dg1;
				b2 += db1;
				r3 += dr3;
				g3 += dg3;
				b3 += db3;
				y3 += Raster.width;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void drawGouraudScanline(int[] dest, int offset, int x1, int x2, int r1, int g1, int b1, int r2, int g2, int b2, float z1, float z2) {
		int n = x2 - x1;
		if (n <= 0) {
			return;
		}
		z2 = (z2 - z1) / n;
		r2 = (r2 - r1) / n;
		g2 = (g2 - g1) / n;
		b2 = (b2 - b1) / n;
		if (aBoolean1462) {
			if (x2 > Raster.bottomX) {
				n -= x2 - Raster.bottomX;
				x2 = Raster.bottomX;
			}
			if (x1 < 0) {
				n = x2;
				z1 -= x1 * z2;
				r1 -= x1 * r2;
				g1 -= x1 * g2;
				b1 -= x1 * b2;
				x1 = 0;
			}
		}
		if (x1 < x2) {
			offset += x1 - 1;
			if (anInt1465 == 0) {
				while (--n >= 0) {
					if (z1 < depthBuffer[++offset]) {
						dest[offset] = (r1 & 0xff0000) | (g1 >> 8 & 0xff00) | (b1 >> 16 & 0xff);
						depthBuffer[offset] = z1;
					}
					z1 += z2;
					r1 += r2;
					g1 += g2;
					b1 += b2;
				}
			} else {
				int rgb;
				int dst;
				final int a1 = anInt1465;
				final int a2 = 256 - anInt1465;
				double alphaPercentage = (a1 / 256D);
				while (--n >= 0) {
					if (z1 < depthBuffer[++offset]) {
						rgb = (r1 & 0xff0000) | (g1 >> 8 & 0xff00) | (b1 >> 16 & 0xff);
						rgb = ((rgb & 0xff00ff) * a2 >> 8 & 0xff00ff) + ((rgb & 0xff00) * a2 >> 8 & 0xff00);
						dst = dest[offset];
						dest[offset] = rgb + ((dst & 0xff00ff) * a1 >> 8 & 0xff00ff) + ((dst & 0xff00) * a1 >> 8 & 0xff00);
						depthBuffer[offset] = (int) (z1 + ((depthBuffer[offset] - z1) * alphaPercentage));
					}
					z1 += z2;
					r1 += r2;
					g1 += g2;
					b1 += b2;
				}
			}
		}
	}
	
	public static void drawGouraudTriangle(int y1, int y2, int y3, int x1, int x2, int x3, int hsl1, int hsl2, int hsl3) {
		int j2 = 0;
		int k2 = 0;
		if (y2 != y1) {
			j2 = (x2 - x1 << 16) / (y2 - y1);
			k2 = (hsl2 - hsl1 << 15) / (y2 - y1);
		}
		int l2 = 0;
		int i3 = 0;
		if (y3 != y2) {
			l2 = (x3 - x2 << 16) / (y3 - y2);
			i3 = (hsl3 - hsl2 << 15) / (y3 - y2);
		}
		int j3 = 0;
		int k3 = 0;
		if (y3 != y1) {
			j3 = (x1 - x3 << 16) / (y1 - y3);
			k3 = (hsl1 - hsl3 << 15) / (y1 - y3);
		}
		if (y1 <= y2 && y1 <= y3) {
			if (y1 >= Raster.bottomY)
				return;
			if (y2 > Raster.bottomY)
				y2 = Raster.bottomY;
			if (y3 > Raster.bottomY)
				y3 = Raster.bottomY;
			if (y2 < y3) {
				x3 = x1 <<= 16;
				hsl3 = hsl1 <<= 15;
				if (y1 < 0) {
					x3 -= j3 * y1;
					x1 -= j2 * y1;
					hsl3 -= k3 * y1;
					hsl1 -= k2 * y1;
					y1 = 0;
				}
				x2 <<= 16;
				hsl2 <<= 15;
				if (y2 < 0) {
					x2 -= l2 * y2;
					hsl2 -= i3 * y2;
					y2 = 0;
				}
				if (y1 != y2 && j3 < j2 || y1 == y2 && j3 > l2) {
					y3 -= y2;
					y2 -= y1;
					for (y1 = lineOffsets[y1]; --y2 >= 0; y1 += Raster.width) {
						drawGouraudScanline(Raster.pixels, y1, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7);
						x3 += j3;
						x1 += j2;
						hsl3 += k3;
						hsl1 += k2;
					}
					
					while (--y3 >= 0) {
						drawGouraudScanline(Raster.pixels, y1, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7);
						x3 += j3;
						x2 += l2;
						hsl3 += k3;
						hsl2 += i3;
						y1 += Raster.width;
					}
					return;
				}
				y3 -= y2;
				y2 -= y1;
				for (y1 = lineOffsets[y1]; --y2 >= 0; y1 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y1, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7);
					x3 += j3;
					x1 += j2;
					hsl3 += k3;
					hsl1 += k2;
				}
				
				while (--y3 >= 0) {
					drawGouraudScanline(Raster.pixels, y1, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7);
					x3 += j3;
					x2 += l2;
					hsl3 += k3;
					hsl2 += i3;
					y1 += Raster.width;
				}
				return;
			}
			x2 = x1 <<= 16;
			hsl2 = hsl1 <<= 15;
			if (y1 < 0) {
				x2 -= j3 * y1;
				x1 -= j2 * y1;
				hsl2 -= k3 * y1;
				hsl1 -= k2 * y1;
				y1 = 0;
			}
			x3 <<= 16;
			hsl3 <<= 15;
			if (y3 < 0) {
				x3 -= l2 * y3;
				hsl3 -= i3 * y3;
				y3 = 0;
			}
			if (y1 != y3 && j3 < j2 || y1 == y3 && l2 > j2) {
				y2 -= y3;
				y3 -= y1;
				for (y1 = lineOffsets[y1]; --y3 >= 0; y1 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y1, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7);
					x2 += j3;
					x1 += j2;
					hsl2 += k3;
					hsl1 += k2;
				}
				
				while (--y2 >= 0) {
					drawGouraudScanline(Raster.pixels, y1, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7);
					x3 += l2;
					x1 += j2;
					hsl3 += i3;
					hsl1 += k2;
					y1 += Raster.width;
				}
				return;
			}
			y2 -= y3;
			y3 -= y1;
			for (y1 = lineOffsets[y1]; --y3 >= 0; y1 += Raster.width) {
				drawGouraudScanline(Raster.pixels, y1, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7);
				x2 += j3;
				x1 += j2;
				hsl2 += k3;
				hsl1 += k2;
			}
			
			while (--y2 >= 0) {
				drawGouraudScanline(Raster.pixels, y1, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7);
				x3 += l2;
				x1 += j2;
				hsl3 += i3;
				hsl1 += k2;
				y1 += Raster.width;
			}
			return;
		}
		if (y2 <= y3) {
			if (y2 >= Raster.bottomY)
				return;
			if (y3 > Raster.bottomY)
				y3 = Raster.bottomY;
			if (y1 > Raster.bottomY)
				y1 = Raster.bottomY;
			if (y3 < y1) {
				x1 = x2 <<= 16;
				hsl1 = hsl2 <<= 15;
				if (y2 < 0) {
					x1 -= j2 * y2;
					x2 -= l2 * y2;
					hsl1 -= k2 * y2;
					hsl2 -= i3 * y2;
					y2 = 0;
				}
				x3 <<= 16;
				hsl3 <<= 15;
				if (y3 < 0) {
					x3 -= j3 * y3;
					hsl3 -= k3 * y3;
					y3 = 0;
				}
				if (y2 != y3 && j2 < l2 || y2 == y3 && j2 > j3) {
					y1 -= y3;
					y3 -= y2;
					for (y2 = lineOffsets[y2]; --y3 >= 0; y2 += Raster.width) {
						drawGouraudScanline(Raster.pixels, y2, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7);
						x1 += j2;
						x2 += l2;
						hsl1 += k2;
						hsl2 += i3;
					}
					
					while (--y1 >= 0) {
						drawGouraudScanline(Raster.pixels, y2, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7);
						x1 += j2;
						x3 += j3;
						hsl1 += k2;
						hsl3 += k3;
						y2 += Raster.width;
					}
					return;
				}
				y1 -= y3;
				y3 -= y2;
				for (y2 = lineOffsets[y2]; --y3 >= 0; y2 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y2, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7);
					x1 += j2;
					x2 += l2;
					hsl1 += k2;
					hsl2 += i3;
				}
				
				while (--y1 >= 0) {
					drawGouraudScanline(Raster.pixels, y2, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7);
					x1 += j2;
					x3 += j3;
					hsl1 += k2;
					hsl3 += k3;
					y2 += Raster.width;
				}
				return;
			}
			x3 = x2 <<= 16;
			hsl3 = hsl2 <<= 15;
			if (y2 < 0) {
				x3 -= j2 * y2;
				x2 -= l2 * y2;
				hsl3 -= k2 * y2;
				hsl2 -= i3 * y2;
				y2 = 0;
			}
			x1 <<= 16;
			hsl1 <<= 15;
			if (y1 < 0) {
				x1 -= j3 * y1;
				hsl1 -= k3 * y1;
				y1 = 0;
			}
			if (j2 < l2) {
				y3 -= y1;
				y1 -= y2;
				for (y2 = lineOffsets[y2]; --y1 >= 0; y2 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y2, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7);
					x3 += j2;
					x2 += l2;
					hsl3 += k2;
					hsl2 += i3;
				}
				
				while (--y3 >= 0) {
					drawGouraudScanline(Raster.pixels, y2, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7);
					x1 += j3;
					x2 += l2;
					hsl1 += k3;
					hsl2 += i3;
					y2 += Raster.width;
				}
				return;
			}
			y3 -= y1;
			y1 -= y2;
			for (y2 = lineOffsets[y2]; --y1 >= 0; y2 += Raster.width) {
				drawGouraudScanline(Raster.pixels, y2, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7);
				x3 += j2;
				x2 += l2;
				hsl3 += k2;
				hsl2 += i3;
			}
			
			while (--y3 >= 0) {
				drawGouraudScanline(Raster.pixels, y2, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7);
				x1 += j3;
				x2 += l2;
				hsl1 += k3;
				hsl2 += i3;
				y2 += Raster.width;
			}
			return;
		}
		if (y3 >= Raster.bottomY)
			return;
		if (y1 > Raster.bottomY)
			y1 = Raster.bottomY;
		if (y2 > Raster.bottomY)
			y2 = Raster.bottomY;
		if (y1 < y2) {
			x2 = x3 <<= 16;
			hsl2 = hsl3 <<= 15;
			if (y3 < 0) {
				x2 -= l2 * y3;
				x3 -= j3 * y3;
				hsl2 -= i3 * y3;
				hsl3 -= k3 * y3;
				y3 = 0;
			}
			x1 <<= 16;
			hsl1 <<= 15;
			if (y1 < 0) {
				x1 -= j2 * y1;
				hsl1 -= k2 * y1;
				y1 = 0;
			}
			if (l2 < j3) {
				y2 -= y1;
				y1 -= y3;
				for (y3 = lineOffsets[y3]; --y1 >= 0; y3 += Raster.width) {
					drawGouraudScanline(Raster.pixels, y3, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7);
					x2 += l2;
					x3 += j3;
					hsl2 += i3;
					hsl3 += k3;
				}
				
				while (--y2 >= 0) {
					drawGouraudScanline(Raster.pixels, y3, x2 >> 16, x1 >> 16, hsl2 >> 7, hsl1 >> 7);
					x2 += l2;
					x1 += j2;
					hsl2 += i3;
					hsl1 += k2;
					y3 += Raster.width;
				}
				return;
			}
			y2 -= y1;
			y1 -= y3;
			for (y3 = lineOffsets[y3]; --y1 >= 0; y3 += Raster.width) {
				drawGouraudScanline(Raster.pixels, y3, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7);
				x2 += l2;
				x3 += j3;
				hsl2 += i3;
				hsl3 += k3;
			}
			
			while (--y2 >= 0) {
				drawGouraudScanline(Raster.pixels, y3, x1 >> 16, x2 >> 16, hsl1 >> 7, hsl2 >> 7);
				x2 += l2;
				x1 += j2;
				hsl2 += i3;
				hsl1 += k2;
				y3 += Raster.width;
			}
			return;
		}
		x1 = x3 <<= 16;
		hsl1 = hsl3 <<= 15;
		if (y3 < 0) {
			x1 -= l2 * y3;
			x3 -= j3 * y3;
			hsl1 -= i3 * y3;
			hsl3 -= k3 * y3;
			y3 = 0;
		}
		x2 <<= 16;
		hsl2 <<= 15;
		if (y2 < 0) {
			x2 -= j2 * y2;
			hsl2 -= k2 * y2;
			y2 = 0;
		}
		if (l2 < j3) {
			y1 -= y2;
			y2 -= y3;
			for (y3 = lineOffsets[y3]; --y2 >= 0; y3 += Raster.width) {
				drawGouraudScanline(Raster.pixels, y3, x1 >> 16, x3 >> 16, hsl1 >> 7, hsl3 >> 7);
				x1 += l2;
				x3 += j3;
				hsl1 += i3;
				hsl3 += k3;
			}
			
			while (--y1 >= 0) {
				drawGouraudScanline(Raster.pixels, y3, x2 >> 16, x3 >> 16, hsl2 >> 7, hsl3 >> 7);
				x2 += j2;
				x3 += j3;
				hsl2 += k2;
				hsl3 += k3;
				y3 += Raster.width;
			}
			return;
		}
		y1 -= y2;
		y2 -= y3;
		for (y3 = lineOffsets[y3]; --y2 >= 0; y3 += Raster.width) {
			drawGouraudScanline(Raster.pixels, y3, x3 >> 16, x1 >> 16, hsl3 >> 7, hsl1 >> 7);
			x1 += l2;
			x3 += j3;
			hsl1 += i3;
			hsl3 += k3;
		}
		
		while (--y1 >= 0) {
			drawGouraudScanline(Raster.pixels, y3, x3 >> 16, x2 >> 16, hsl3 >> 7, hsl2 >> 7);
			x2 += j2;
			x3 += j3;
			hsl2 += k2;
			hsl3 += k3;
			y3 += Raster.width;
		}
	}
	
	private static void drawGouraudScanline(int ai[], int i, int l, int i1, int j1, int k1) {
		int j;//was parameter
		int k;//was parameter
		if (aBoolean1464) {
			int l1;
			if (aBoolean1462) {
				if (i1 - l > 3)
					l1 = (k1 - j1) / (i1 - l);
				else
					l1 = 0;
				if (i1 > Raster.bottomX)
					i1 = Raster.bottomX;
				if (l < 0) {
					j1 -= l * l1;
					l = 0;
				}
				if (l >= i1)
					return;
				i += l;
				k = i1 - l >> 2;
				l1 <<= 2;
			} else {
				if (l >= i1)
					return;
				i += l;
				k = i1 - l >> 2;
				if (k > 0)
					l1 = (k1 - j1) * anIntArray1468[k] >> 15;
				else
					l1 = 0;
			}
			if (anInt1465 == 0) {
				while (--k >= 0) {
					j = anIntArray1482[j1 >> 8];
					j1 += l1;
					ai[i++] = j;
					ai[i++] = j;
					ai[i++] = j;
					ai[i++] = j;
				}
				k = i1 - l & 3;
				if (k > 0) {
					j = anIntArray1482[j1 >> 8];
					do
						ai[i++] = j;
					while (--k > 0);
					return;
				}
			} else {
				int j2 = anInt1465;
				int l2 = 256 - anInt1465;
				while (--k >= 0) {
					j = anIntArray1482[j1 >> 8];
					j1 += l1;
					j = ((j & 0xff00ff) * l2 >> 8 & 0xff00ff) + ((j & 0xff00) * l2 >> 8 & 0xff00);
					ai[i] = j + ((ai[i] & 0xff00ff) * j2 >> 8 & 0xff00ff) + ((ai[i] & 0xff00) * j2 >> 8 & 0xff00);
					i++;
					ai[i] = j + ((ai[i] & 0xff00ff) * j2 >> 8 & 0xff00ff) + ((ai[i] & 0xff00) * j2 >> 8 & 0xff00);
					i++;
					ai[i] = j + ((ai[i] & 0xff00ff) * j2 >> 8 & 0xff00ff) + ((ai[i] & 0xff00) * j2 >> 8 & 0xff00);
					i++;
					ai[i] = j + ((ai[i] & 0xff00ff) * j2 >> 8 & 0xff00ff) + ((ai[i] & 0xff00) * j2 >> 8 & 0xff00);
					i++;
				}
				k = i1 - l & 3;
				if (k > 0) {
					j = anIntArray1482[j1 >> 8];
					j = ((j & 0xff00ff) * l2 >> 8 & 0xff00ff) + ((j & 0xff00) * l2 >> 8 & 0xff00);
					do {
						ai[i] = j + ((ai[i] & 0xff00ff) * j2 >> 8 & 0xff00ff) + ((ai[i] & 0xff00) * j2 >> 8 & 0xff00);
						i++;
					} while (--k > 0);
				}
			}
			return;
		}
		if (l >= i1)
			return;
		int i2 = (k1 - j1) / (i1 - l);
		if (aBoolean1462) {
			if (i1 > Raster.bottomX)
				i1 = Raster.bottomX;
			if (l < 0) {
				j1 -= l * i2;
				l = 0;
			}
			if (l >= i1)
				return;
		}
		i += l;
		k = i1 - l;
		if (anInt1465 == 0) {
			do {
				ai[i++] = anIntArray1482[j1 >> 8];
				j1 += i2;
			} while (--k > 0);
			return;
		}
		int k2 = anInt1465;
		int i3 = 256 - anInt1465;
		do {
			j = anIntArray1482[j1 >> 8];
			j1 += i2;
			j = ((j & 0xff00ff) * i3 >> 8 & 0xff00ff) + ((j & 0xff00) * i3 >> 8 & 0xff00);
			ai[i] = j + ((ai[i] & 0xff00ff) * k2 >> 8 & 0xff00ff) + ((ai[i] & 0xff00) * k2 >> 8 & 0xff00);
			i++;
		} while (--k > 0);
	}
	
	public static void drawFlatTriangle(int y1, int y2, int y3, int x1, int x2, int x3, int rgb, float z1, float z2, float z3) {
		int dx1 = 0;
		float dz1 = 0;
		if (y2 != y1) {
			final int d = (y2 - y1);
			dx1 = (x2 - x1 << 16) / d;
			dz1 = (z2 - z1) / d;
		}
		int dx2 = 0;
		float dz2 = 0;
		if (y3 != y2) {
			final int d = (y3 - y2);
			dx2 = (x3 - x2 << 16) / d;
			dz2 = (z3 - z2) / d;
		}
		int dx3 = 0;
		float dz3 = 0;
		if (y3 != y1) {
			final int d = (y1 - y3);
			dx3 = (x1 - x3 << 16) / d;
			dz3 = (z1 - z3) / d;
		}
		if (y1 <= y2 && y1 <= y3) {
			if (y1 >= Raster.bottomY) {
				return;
			}
			if (y2 > Raster.bottomY) {
				y2 = Raster.bottomY;
			}
			if (y3 > Raster.bottomY) {
				y3 = Raster.bottomY;
			}
			if (y2 < y3) {
				x3 = x1 <<= 16;
				z3 = z1;
				if (y1 < 0) {
					x3 -= dx3 * y1;
					x1 -= dx1 * y1;
					z3 -= dz3 * y1;
					z1 -= dz1 * y1;
					y1 = 0;
				}
				x2 <<= 16;
				if (y2 < 0) {
					x2 -= dx2 * y2;
					z2 -= dz2 * y2;
					y2 = 0;
				}
				if (y1 != y2 && dx3 < dx1 || y1 == y2 && dx3 > dx2) {
					y3 -= y2;
					y2 -= y1;
					for (y1 = lineOffsets[y1]; --y2 >= 0; y1 += Raster.width) {
						drawFlatScanline(Raster.pixels, y1, rgb, x3 >> 16, x1 >> 16, z3, z1);
						x3 += dx3;
						x1 += dx1;
						z3 += dz3;
						z1 += dz1;
					}
					while (--y3 >= 0) {
						drawFlatScanline(Raster.pixels, y1, rgb, x3 >> 16, x2 >> 16, z3, z2);
						x3 += dx3;
						x2 += dx2;
						z3 += dz3;
						z2 += dz2;
						y1 += Raster.width;
					}
					return;
				}
				y3 -= y2;
				y2 -= y1;
				for (y1 = lineOffsets[y1]; --y2 >= 0; y1 += Raster.width) {
					drawFlatScanline(Raster.pixels, y1, rgb, x1 >> 16, x3 >> 16, z1, z3);
					x3 += dx3;
					x1 += dx1;
					z3 += dz3;
					z1 += dz1;
				}
				while (--y3 >= 0) {
					drawFlatScanline(Raster.pixels, y1, rgb, x2 >> 16, x3 >> 16, z2, z3);
					x3 += dx3;
					x2 += dx2;
					z3 += dz3;
					z2 += dz2;
					y1 += Raster.width;
				}
				return;
			}
			x2 = x1 <<= 16;
			z2 = z1;
			if (y1 < 0) {
				x2 -= dx3 * y1;
				x1 -= dx1 * y1;
				z2 -= dz3 * y1;
				z1 -= dz1 * y1;
				y1 = 0;
			}
			x3 <<= 16;
			if (y3 < 0) {
				x3 -= dx2 * y3;
				z3 -= dz2 * y3;
				y3 = 0;
			}
			if (y1 != y3 && dx3 < dx1 || y1 == y3 && dx2 > dx1) {
				y2 -= y3;
				y3 -= y1;
				for (y1 = lineOffsets[y1]; --y3 >= 0; y1 += Raster.width) {
					drawFlatScanline(Raster.pixels, y1, rgb, x2 >> 16, x1 >> 16, z2, z1);
					x2 += dx3;
					x1 += dx1;
					z2 += dz3;
					z1 += dz1;
				}
				while (--y2 >= 0) {
					drawFlatScanline(Raster.pixels, y1, rgb, x3 >> 16, x1 >> 16, z3, z1);
					x3 += dx2;
					x1 += dx1;
					z3 += dz2;
					z1 += dz1;
					y1 += Raster.width;
				}
				return;
			}
			y2 -= y3;
			y3 -= y1;
			for (y1 = lineOffsets[y1]; --y3 >= 0; y1 += Raster.width) {
				drawFlatScanline(Raster.pixels, y1, rgb, x1 >> 16, x2 >> 16, z1, z2);
				x2 += dx3;
				x1 += dx1;
				z2 += dz3;
				z1 += dz1;
			}
			while (--y2 >= 0) {
				drawFlatScanline(Raster.pixels, y1, rgb, x1 >> 16, x3 >> 16, z1, z3);
				x3 += dx2;
				x1 += dx1;
				z3 += dz2;
				z1 += dz1;
				y1 += Raster.width;
			}
			return;
		}
		if (y2 <= y3) {
			if (y2 >= Raster.bottomY) {
				return;
			}
			if (y3 > Raster.bottomY) {
				y3 = Raster.bottomY;
			}
			if (y1 > Raster.bottomY) {
				y1 = Raster.bottomY;
			}
			if (y3 < y1) {
				x1 = x2 <<= 16;
				z1 = z2;
				if (y2 < 0) {
					x1 -= dx1 * y2;
					x2 -= dx2 * y2;
					z1 -= dz1 * y2;
					z2 -= dz2 * y2;
					y2 = 0;
				}
				x3 <<= 16;
				if (y3 < 0) {
					x3 -= dx3 * y3;
					z3 -= dz3 * y3;
					y3 = 0;
				}
				if (y2 != y3 && dx1 < dx2 || y2 == y3 && dx1 > dx3) {
					y1 -= y3;
					y3 -= y2;
					for (y2 = lineOffsets[y2]; --y3 >= 0; y2 += Raster.width) {
						drawFlatScanline(Raster.pixels, y2, rgb, x1 >> 16, x2 >> 16, z1, z2);
						x1 += dx1;
						x2 += dx2;
						z1 += dz1;
						z2 += dz2;
					}
					while (--y1 >= 0) {
						drawFlatScanline(Raster.pixels, y2, rgb, x1 >> 16, x3 >> 16, z1, z3);
						x1 += dx1;
						x3 += dx3;
						z1 += dz1;
						z3 += dz3;
						y2 += Raster.width;
					}
					return;
				}
				y1 -= y3;
				y3 -= y2;
				for (y2 = lineOffsets[y2]; --y3 >= 0; y2 += Raster.width) {
					drawFlatScanline(Raster.pixels, y2, rgb, x2 >> 16, x1 >> 16, z2, z1);
					x1 += dx1;
					x2 += dx2;
					z1 += dz1;
					z2 += dz2;
				}
				while (--y1 >= 0) {
					drawFlatScanline(Raster.pixels, y2, rgb, x3 >> 16, x1 >> 16, z3, z1);
					x1 += dx1;
					x3 += dx3;
					z1 += dz1;
					z3 += dz3;
					y2 += Raster.width;
				}
				return;
			}
			x3 = x2 <<= 16;
			z3 = z2;
			if (y2 < 0) {
				x3 -= dx1 * y2;
				x2 -= dx2 * y2;
				z3 -= dz1 * y2;
				z2 -= dz2 * y2;
				y2 = 0;
			}
			x1 <<= 16;
			if (y1 < 0) {
				x1 -= dx3 * y1;
				z1 -= dz3 * y1;
				y1 = 0;
			}
			if (dx1 < dx2) {
				y3 -= y1;
				y1 -= y2;
				for (y2 = lineOffsets[y2]; --y1 >= 0; y2 += Raster.width) {
					drawFlatScanline(Raster.pixels, y2, rgb, x3 >> 16, x2 >> 16, z3, z2);
					x3 += dx1;
					x2 += dx2;
					z3 += dz1;
					z2 += dz2;
				}
				while (--y3 >= 0) {
					drawFlatScanline(Raster.pixels, y2, rgb, x1 >> 16, x2 >> 16, z1, z2);
					x1 += dx3;
					x2 += dx2;
					z1 += dz3;
					z2 += dz2;
					y2 += Raster.width;
				}
				return;
			}
			y3 -= y1;
			y1 -= y2;
			for (y2 = lineOffsets[y2]; --y1 >= 0; y2 += Raster.width) {
				drawFlatScanline(Raster.pixels, y2, rgb, x2 >> 16, x3 >> 16, z2, z3);
				x3 += dx1;
				x2 += dx2;
				z3 += dz1;
				z2 += dz2;
			}
			while (--y3 >= 0) {
				drawFlatScanline(Raster.pixels, y2, rgb, x2 >> 16, x1 >> 16, z2, z1);
				x1 += dx3;
				x2 += dx2;
				z1 += dz3;
				z2 += dz2;
				y2 += Raster.width;
			}
			return;
		}
		if (y3 >= Raster.bottomY) {
			return;
		}
		if (y1 > Raster.bottomY) {
			y1 = Raster.bottomY;
		}
		if (y2 > Raster.bottomY) {
			y2 = Raster.bottomY;
		}
		if (y1 < y2) {
			x2 = x3 <<= 16;
			z2 = z3;
			if (y3 < 0) {
				x2 -= dx2 * y3;
				x3 -= dx3 * y3;
				z2 -= dz2 * y3;
				z3 -= dz3 * y3;
				y3 = 0;
			}
			x1 <<= 16;
			if (y1 < 0) {
				x1 -= dx1 * y1;
				z1 -= dz1 * y1;
				y1 = 0;
			}
			if (dx2 < dx3) {
				y2 -= y1;
				y1 -= y3;
				for (y3 = lineOffsets[y3]; --y1 >= 0; y3 += Raster.width) {
					drawFlatScanline(Raster.pixels, y3, rgb, x2 >> 16, x3 >> 16, z2, z3);
					x2 += dx2;
					x3 += dx3;
					z2 += dz2;
					z3 += dz3;
				}
				while (--y2 >= 0) {
					drawFlatScanline(Raster.pixels, y3, rgb, x2 >> 16, x1 >> 16, z2, z1);
					x2 += dx2;
					x1 += dx1;
					z2 += dz2;
					z1 += dz1;
					y3 += Raster.width;
				}
				return;
			}
			y2 -= y1;
			y1 -= y3;
			for (y3 = lineOffsets[y3]; --y1 >= 0; y3 += Raster.width) {
				drawFlatScanline(Raster.pixels, y3, rgb, x3 >> 16, x2 >> 16, z3, z2);
				x2 += dx2;
				x3 += dx3;
				z2 += dz2;
				z3 += dz3;
			}
			while (--y2 >= 0) {
				drawFlatScanline(Raster.pixels, y3, rgb, x1 >> 16, x2 >> 16, z1, z2);
				x2 += dx2;
				x1 += dx1;
				z2 += dz2;
				z1 += dz1;
				y3 += Raster.width;
			}
			return;
		}
		x1 = x3 <<= 16;
		z1 = z3;
		if (y3 < 0) {
			x1 -= dx2 * y3;
			x3 -= dx3 * y3;
			z1 -= dz2 * y3;
			z3 -= dz3 * y3;
			y3 = 0;
		}
		x2 <<= 16;
		if (y2 < 0) {
			x2 -= dx1 * y2;
			z2 -= dz1 * y2;
			y2 = 0;
		}
		if (dx2 < dx3) {
			y1 -= y2;
			y2 -= y3;
			for (y3 = lineOffsets[y3]; --y2 >= 0; y3 += Raster.width) {
				drawFlatScanline(Raster.pixels, y3, rgb, x1 >> 16, x3 >> 16, z1, z3);
				x1 += dx2;
				x3 += dx3;
				z1 += dz2;
				z3 += dz3;
			}
			while (--y1 >= 0) {
				drawFlatScanline(Raster.pixels, y3, rgb, x2 >> 16, x3 >> 16, z2, z3);
				x2 += dx1;
				x3 += dx3;
				z2 += dz1;
				z3 += dz3;
				y3 += Raster.width;
			}
			return;
		}
		y1 -= y2;
		y2 -= y3;
		for (y3 = lineOffsets[y3]; --y2 >= 0; y3 += Raster.width) {
			drawFlatScanline(Raster.pixels, y3, rgb, x3 >> 16, x1 >> 16, z3, z1);
			x1 += dx2;
			x3 += dx3;
			z1 += dz2;
			z3 += dz3;
		}
		while (--y1 >= 0) {
			drawFlatScanline(Raster.pixels, y3, rgb, x3 >> 16, x2 >> 16, z3, z2);
			x2 += dx1;
			x3 += dx3;
			z2 += dz1;
			z3 += dz3;
			y3 += Raster.width;
		}
	}
	
	private static void drawFlatScanline(int[] dest, int offset, int rgb, int x1, int x2, float z1, float z2) {
		int len;
		float dz = 0;
		if (x1 != x2) {
			dz = (z2 - z1) / (x2 - x1);
		}
		if (aBoolean1462) {
			if (x2 > Raster.bottomX) {
				x2 = Raster.bottomX;
			}
			if (x1 < 0) {
				z1 -= x1 * dz;
				x1 = 0;
			}
		}
		if (x1 >= x2) {
			return;
		}
		offset += x1;
		len = x2 - x1 >> 2;
		if (anInt1465 == 0) {
			while (--len >= 0) {
				if (z1 < depthBuffer[offset]) {
					depthBuffer[offset] = z1;
					dest[offset] = rgb;
				}
				z1 += dz;
				offset++;
				if (z1 < depthBuffer[offset]) {
					depthBuffer[offset] = z1;
					dest[offset] = rgb;
				}
				z1 += dz;
				offset++;
				if (z1 < depthBuffer[offset]) {
					depthBuffer[offset] = z1;
					dest[offset] = rgb;
				}
				z1 += dz;
				offset++;
				if (z1 < depthBuffer[offset]) {
					depthBuffer[offset] = z1;
					dest[offset] = rgb;
				}
				z1 += dz;
				offset++;
			}
			for (len = x2 - x1 & 3; --len >= 0; ) {
				if (z1 < depthBuffer[offset]) {
					depthBuffer[offset] = z1;
					dest[offset] = rgb;
				}
				z1 += dz;
				offset++;
			}
			return;
		}
		int a1 = anInt1465;
		int a2 = 256 - anInt1465;
		double alphaPercentage = (a1 / 256D);
		rgb = ((rgb & 0xff00ff) * a2 >> 8 & 0xff00ff) + ((rgb & 0xff00) * a2 >> 8 & 0xff00);
		int pixel = rgb + ((dest[offset] & 0xff00ff) * a1 >> 8 & 0xff00ff) + ((dest[offset] & 0xff00) * a1 >> 8 & 0xff00);
		while (--len >= 0) {
			if (z1 < depthBuffer[offset]) {
				depthBuffer[offset] = (int) (z1 + ((depthBuffer[offset] - z1) * alphaPercentage));
				dest[offset] = pixel;
			}
			z1 += dz;
			offset++;
			if (z1 < depthBuffer[offset]) {
				depthBuffer[offset] = (int) (z1 + ((depthBuffer[offset] - z1) * alphaPercentage));
				dest[offset] = pixel;
			}
			z1 += dz;
			offset++;
			if (z1 < depthBuffer[offset]) {
				depthBuffer[offset] = (int) (z1 + ((depthBuffer[offset] - z1) * alphaPercentage));
				dest[offset] = pixel;
			}
			z1 += dz;
			offset++;
			if (z1 < depthBuffer[offset]) {
				depthBuffer[offset] = (int) (z1 + ((depthBuffer[offset] - z1) * alphaPercentage));
				dest[offset] = pixel;
			}
			z1 += dz;
			offset++;
		}
		for (len = x2 - x1 & 3; --len >= 0; ) {
			if (z1 < depthBuffer[offset]) {
				depthBuffer[offset] = (int) (z1 + ((depthBuffer[offset] - z1) * alphaPercentage));
				dest[offset] = pixel;
			}
			z1 += dz;
			offset++;
		}
	}

	public static void drawTexturedTriangle(int y1, int y2, int y3, int x1, int x2, int x3, int c1, int c2,
	                                           int c3, int tx1, int tx2, int tx3, int ty1, int ty2, int ty3,
	                                           int tz1, int tz2, int tz3, int tex, float z1, float z2, float z3) {
		int ai[] = method371(tex);
		aBoolean1463 = !aBooleanArray1475[tex];
		tx2 = tx1 - tx2;
		ty2 = ty1 - ty2;
		tz2 = tz1 - tz2;
		tx3 -= tx1;
		ty3 -= ty1;
		tz3 -= tz1;
		int l4 = tx3 * ty1 - ty3 * tx1 << 14;
		int i5 = (int) (((long) (ty3 * tz1 - tz3 * ty1) << 3 << 14) / (long) Client.fov);
		int j5 = (int) (((long) (tz3 * tx1 - tx3 * tz1) << 14) / (long) Client.fov);
		int k5 = tx2 * ty1 - ty2 * tx1 << 14;
		int l5 = (int) (((long) (ty2 * tz1 - tz2 * ty1) << 3 << 14) / (long) Client.fov);
		int i6 = (int) (((long) (tz2 * tx1 - tx2 * tz1) << 14) / (long) Client.fov);
		int j6 = ty2 * tx3 - tx2 * ty3 << 14;
		int k6 = (int) (((long) (tz2 * ty3 - ty2 * tz3) << 3 << 14) / (long) Client.fov);
		int l6 = (int) (((long) (tx2 * tz3 - tz2 * tx3) << 14) / (long) Client.fov);
		int i7 = 0;
		int j7 = 0;
		float dz1 = 0.0f;
		if (y2 != y1) {
			int delta = y2 - y1;
			i7 = (x2 - x1 << 16) / delta;
			j7 = (c2 - c1 << 16) / delta;
			dz1 = (z2 - z1) / delta;
		}

		int k7 = 0;
		int l7 = 0;
		float dz2 = 0.0f;
		if (y3 != y2) {
			int delta = y3 - y2;
			k7 = (x3 - x2 << 16) / delta;
			l7 = (c3 - c2 << 16) / delta;
			dz2 = (z3 - z2) / delta;
		}

		int i8 = 0;
		int j8 = 0;
		float dz3 = 0;
		if (y3 != y1) {
			int delta = y1 - y3;
			i8 = (x1 - x3 << 16) / delta;
			j8 = (c1 - c3 << 16) / delta;
			dz3 = (z1 - z3) / delta;
		}

		if (y1 <= y2 && y1 <= y3) {
			if (y1 >= Raster.bottomY)
				return;
			if (y2 > Raster.bottomY)
				y2 = Raster.bottomY;
			if (y3 > Raster.bottomY)
				y3 = Raster.bottomY;
			if (y2 < y3) {
				x3 = x1 <<= 16;
				c3 = c1 <<= 16;
				z3 = z1;
				if (y1 < 0) {
					z1 -= dz1 * y1;
					x3 -= i8 * y1;
					x1 -= i7 * y1;
					c3 -= j8 * y1;
					c1 -= j7 * y1;
					y1 = 0;
				}
				x2 <<= 16;
				c2 <<= 16;
				if (y2 < 0) {
					x2 -= k7 * y2;
					c2 -= l7 * y2;
					z2 -= dz2 * y2;
					y2 = 0;
				}
				int k8 = y1 - center_y;
				l4 += j5 * k8;
				k5 += i6 * k8;
				j6 += l6 * k8;
				if (y1 != y2 && i8 < i7 || y1 == y2 && i8 > k7) {
					y3 -= y2;
					y2 -= y1;
					y1 = lineOffsets[y1];
					while (--y2 >= 0) {
						drawLDTexturedScanline(Raster.pixels, ai, y1, x3 >> 16, x1 >> 16, c3 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z3, z1);
						x3 += i8;
						x1 += i7;
						c3 += j8;
						c1 += j7;
						y1 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
						z3 += dz3;
						z1 += dz1;
					}
					while (--y3 >= 0) {
						drawLDTexturedScanline(Raster.pixels, ai, y1, x3 >> 16, x2 >> 16, c3 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z3, z2);
						x3 += i8;
						x2 += k7;
						c3 += j8;
						c2 += l7;
						y1 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
						z3 += dz3;
						z2 += dz2;
					}
					return;
				}
				y3 -= y2;
				y2 -= y1;
				y1 = lineOffsets[y1];
				while (--y2 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y1, x1 >> 16, x3 >> 16, c1 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z1, z3);
					x3 += i8;
					x1 += i7;
					c3 += j8;
					c1 += j7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z3 += dz3;
					z1 += dz1;
				}
				while (--y3 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y1, x2 >> 16, x3 >> 16, c2 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z2, z3);
					x3 += i8;
					x2 += k7;
					c3 += j8;
					c2 += l7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z3 += dz3;
					z2 += dz2;
				}
				return;
			}
			x2 = x1 <<= 16;
			c2 = c1 <<= 16;
			z2 = z1;
			if (y1 < 0) {
				x2 -= i8 * y1;
				x1 -= i7 * y1;
				z2 -= dz3 * y1;
				z1 -= dz1 * y1;
				c2 -= j8 * y1;
				c1 -= j7 * y1;
				y1 = 0;
			}
			x3 <<= 16;
			c3 <<= 16;
			if (y3 < 0) {
				x3 -= k7 * y3;
				z3 -= dz2 * y3;
				c3 -= l7 * y3;
				y3 = 0;
			}
			int l8 = y1 - center_y;
			l4 += j5 * l8;
			k5 += i6 * l8;
			j6 += l6 * l8;
			if (y1 != y3 && i8 < i7 || y1 == y3 && k7 > i7) {
				y2 -= y3;
				y3 -= y1;
				y1 = lineOffsets[y1];
				while (--y3 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y1, x2 >> 16, x1 >> 16, c2 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z2, z1);
					x2 += i8;
					x1 += i7;
					c2 += j8;
					c1 += j7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z2 += dz3;
					z1 += dz1;
				}
				while (--y2 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y1, x3 >> 16, x1 >> 16, c3 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z3, z1);
					x3 += k7;
					x1 += i7;
					c3 += l7;
					c1 += j7;
					y1 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z3 += dz2;
					z1 += dz1;
				}
				return;
			}
			y2 -= y3;
			y3 -= y1;
			y1 = lineOffsets[y1];
			while (--y3 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y1, x1 >> 16, x2 >> 16, c1 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z1, z2);
				x2 += i8;
				x1 += i7;
				c2 += j8;
				c1 += j7;
				y1 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z2 += dz3;
				z1 += dz1;
			}
			while (--y2 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y1, x1 >> 16, x3 >> 16, c1 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z1, z3);
				x3 += k7;
				x1 += i7;
				c3 += l7;
				c1 += j7;
				y1 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z3 += dz2;
				z1 += dz1;
			}
			return;
		}
		if (y2 <= y3) {
			if (y2 >= Raster.bottomY)
				return;
			if (y3 > Raster.bottomY)
				y3 = Raster.bottomY;
			if (y1 > Raster.bottomY)
				y1 = Raster.bottomY;
			if (y3 < y1) {
				x1 = x2 <<= 16;
				c1 = c2 <<= 16;
				z1 = z2;
				if (y2 < 0) {
					x1 -= i7 * y2;
					x2 -= k7 * y2;
					z1 -= dz1 * y2;
					z2 -= dz2 * y2;
					c1 -= j7 * y2;
					c2 -= l7 * y2;
					y2 = 0;
				}
				x3 <<= 16;
				c3 <<= 16;
				if (y3 < 0) {
					x3 -= i8 * y3;
					c3 -= j8 * y3;
					z3 -= dz3 * y3;
					y3 = 0;
				}
				int i9 = y2 - center_y;
				l4 += j5 * i9;
				k5 += i6 * i9;
				j6 += l6 * i9;
				if (y2 != y3 && i7 < k7 || y2 == y3 && i7 > i8) {
					y1 -= y3;
					y3 -= y2;
					y2 = lineOffsets[y2];
					while (--y3 >= 0) {
						drawLDTexturedScanline(Raster.pixels, ai, y2, x1 >> 16, x2 >> 16, c1 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z1, z2);
						x1 += i7;
						x2 += k7;
						c1 += j7;
						c2 += l7;
						y2 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
						z1 += dz1;
						z2 += dz2;
					}
					while (--y1 >= 0) {
						drawLDTexturedScanline(Raster.pixels, ai, y2, x1 >> 16, x3 >> 16, c1 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z1, z3);
						x1 += i7;
						x3 += i8;
						c1 += j7;
						c3 += j8;
						y2 += Raster.width;
						l4 += j5;
						k5 += i6;
						j6 += l6;
						z1 += dz1;
						z3 += dz3;
					}
					return;
				}
				y1 -= y3;
				y3 -= y2;
				y2 = lineOffsets[y2];
				while (--y3 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y2, x2 >> 16, x1 >> 16, c2 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z2, z1);
					x1 += i7;
					x2 += k7;
					c1 += j7;
					c2 += l7;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z1 += dz1;
					z2 += dz2;
				}
				while (--y1 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y2, x3 >> 16, x1 >> 16, c3 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z3, z1);
					x1 += i7;
					x3 += i8;
					c1 += j7;
					c3 += j8;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z1 += dz1;
					z3 += dz3;
				}
				return;
			}
			x3 = x2 <<= 16;
			c3 = c2 <<= 16;
			z3 = z2;
			if (y2 < 0) {
				x3 -= i7 * y2;
				x2 -= k7 * y2;
				z3 -= dz1 * y2;
				z2 -= dz2 * y2;
				c3 -= j7 * y2;
				c2 -= l7 * y2;
				y2 = 0;
			}
			x1 <<= 16;
			c1 <<= 16;
			if (y1 < 0) {
				x1 -= i8 * y1;
				z1 -= dz3 * y1;
				c1 -= j8 * y1;
				y1 = 0;
			}
			int j9 = y2 - center_y;
			l4 += j5 * j9;
			k5 += i6 * j9;
			j6 += l6 * j9;
			if (i7 < k7) {
				y3 -= y1;
				y1 -= y2;
				y2 = lineOffsets[y2];
				while (--y1 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y2, x3 >> 16, x2 >> 16, c3 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z3, z2);
					x3 += i7;
					x2 += k7;
					c3 += j7;
					c2 += l7;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z3 += dz1;
					z2 += dz2;
				}
				while (--y3 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y2, x1 >> 16, x2 >> 16, c1 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z1, z2);
					x1 += i8;
					x2 += k7;
					c1 += j8;
					c2 += l7;
					y2 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z1 += dz3;
					z2 += dz2;
				}
				return;
			}
			y3 -= y1;
			y1 -= y2;
			y2 = lineOffsets[y2];
			while (--y1 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y2, x2 >> 16, x3 >> 16, c2 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z2, z3);
				x3 += i7;
				x2 += k7;
				c3 += j7;
				c2 += l7;
				y2 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z3 += dz1;
				z2 += dz2;
			}
			while (--y3 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y2, x2 >> 16, x1 >> 16, c2 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z2, z1);
				x1 += i8;
				x2 += k7;
				c1 += j8;
				c2 += l7;
				y2 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z1 += dz3;
				z2 += dz2;
			}
			return;
		}
		if (y3 >= Raster.bottomY)
			return;
		if (y1 > Raster.bottomY)
			y1 = Raster.bottomY;
		if (y2 > Raster.bottomY)
			y2 = Raster.bottomY;
		if (y1 < y2) {
			x2 = x3 <<= 16;
			c2 = c3 <<= 16;
			z2 = z3;
			if (y3 < 0) {
				x2 -= k7 * y3;
				x3 -= i8 * y3;
				c2 -= l7 * y3;
				c3 -= j8 * y3;
				z2 -= dz2 * y3;
				z3 -= dz3 * y3;
				y3 = 0;
			}
			x1 <<= 16;
			c1 <<= 16;
			if (y1 < 0) {
				x1 -= i7 * y1;
				c1 -= j7 * y1;
				z1 -= dz1 * y1;
				y1 = 0;
			}
			int k9 = y3 - center_y;
			l4 += j5 * k9;
			k5 += i6 * k9;
			j6 += l6 * k9;
			if (k7 < i8) {
				y2 -= y1;
				y1 -= y3;
				y3 = lineOffsets[y3];
				while (--y1 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y3, x2 >> 16, x3 >> 16, c2 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z2, z3);
					x2 += k7;
					x3 += i8;
					c2 += l7;
					c3 += j8;
					y3 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z2 += dz2;
					z3 += dz3;
				}
				while (--y2 >= 0) {
					drawLDTexturedScanline(Raster.pixels, ai, y3, x2 >> 16, x1 >> 16, c2 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z2, z1);
					x2 += k7;
					x1 += i7;
					c2 += l7;
					c1 += j7;
					y3 += Raster.width;
					l4 += j5;
					k5 += i6;
					j6 += l6;
					z2 += dz2;
					z1 += dz1;
				}
				return;
			}
			y2 -= y1;
			y1 -= y3;
			y3 = lineOffsets[y3];
			while (--y1 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y3, x3 >> 16, x2 >> 16, c3 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z3, z2);
				x2 += k7;
				x3 += i8;
				c2 += l7;
				c3 += j8;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z2 += dz2;
				z3 += dz3;
			}
			while (--y2 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y3, x1 >> 16, x2 >> 16, c1 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z1, z2);
				x2 += k7;
				x1 += i7;
				c2 += l7;
				c1 += j7;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z2 += dz2;
				z1 += dz1;
			}
			return;
		}
		x1 = x3 <<= 16;
		z1 = z3;
		c1 = c3 <<= 16;
		if (y3 < 0) {
			x1 -= k7 * y3;
			x3 -= i8 * y3;
			z1 -= dz2 * y3;
			z3 -= dz3 * y3;
			c1 -= l7 * y3;
			c3 -= j8 * y3;
			y3 = 0;
		}
		x2 <<= 16;
		c2 <<= 16;
		if (y2 < 0) {
			x2 -= i7 * y2;
			z2 -= dz1 * y2;
			c2 -= j7 * y2;
			y2 = 0;
		}
		int l9 = y3 - center_y;
		l4 += j5 * l9;
		k5 += i6 * l9;
		j6 += l6 * l9;
		if (k7 < i8) {
			y1 -= y2;
			y2 -= y3;
			y3 = lineOffsets[y3];
			while (--y2 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y3, x1 >> 16, x3 >> 16, c1 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z1, z3);
				x1 += k7;
				x3 += i8;
				c1 += l7;
				c3 += j8;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z1 += dz2;
				z3 += dz3;
			}
			while (--y1 >= 0) {
				drawLDTexturedScanline(Raster.pixels, ai, y3, x2 >> 16, x3 >> 16, c2 >> 8, c3 >> 8, l4, k5, j6, i5, l5, k6, z2, z3);
				x2 += i7;
				x3 += i8;
				c2 += j7;
				c3 += j8;
				y3 += Raster.width;
				l4 += j5;
				k5 += i6;
				j6 += l6;
				z2 += dz1;
				z3 += dz3;
			}
			return;
		}
		y1 -= y2;
		y2 -= y3;
		y3 = lineOffsets[y3];
		while (--y2 >= 0) {
			drawLDTexturedScanline(Raster.pixels, ai, y3, x3 >> 16, x1 >> 16, c3 >> 8, c1 >> 8, l4, k5, j6, i5, l5, k6, z3, z1);
			x1 += k7;
			x3 += i8;
			c1 += l7;
			c3 += j8;
			y3 += Raster.width;
			l4 += j5;
			k5 += i6;
			j6 += l6;
			z1 += dz2;
			z3 += dz3;
		}
		while (--y1 >= 0) {
			drawLDTexturedScanline(Raster.pixels, ai, y3, x3 >> 16, x2 >> 16, c3 >> 8, c2 >> 8, l4, k5, j6, i5, l5, k6, z3, z2);
			x2 += i7;
			x3 += i8;
			c2 += j7;
			c3 += j8;
			y3 += Raster.width;
			l4 += j5;
			k5 += i6;
			j6 += l6;
			z2 += dz1;
			z3 += dz3;
		}
	}
	
	private static void drawLDTexturedScanline(int ai[], int ai1[], int k, int l, int i1, int j1,
	                                           int k1, int l1, int i2, int j2, int k2, int l2, int i3, float z1, float z2) {
		int i = 0;//was parameter
		int j = 0;//was parameter
		if (l >= i1)
			return;
		int j3;
		int k3;
		float dz;
		if (aBoolean1462) {
			int delta = i1 - l;
			dz = (z2 - z1) / delta;
			j3 = (k1 - j1) / delta;
			if (i1 > Raster.bottomX)
				i1 = Raster.bottomX;
			if (l < 0) {
				j1 -= l * j3;
				l = 0;
			}
			if (l >= i1)
				return;
			k3 = i1 - l >> 3;
			j3 <<= 12;
			j1 <<= 9;
		} else {
			int delta = i1 - l;
			dz = (z2 - z1) / delta;

			if (delta > 7) {
				k3 = delta >> 3;
				j3 = (k1 - j1) * anIntArray1468[k3] >> 6;
			} else {
				k3 = 0;
				j3 = 0;
			}
			j1 <<= 9;
		}
		k += l;
		int j4 = 0;
		int l4 = 0;
		int l6 = l - center_x;
		l1 += (k2 >> 3) * l6;
		i2 += (l2 >> 3) * l6;
		j2 += (i3 >> 3) * l6;
		int l5 = j2 >> 14;
		if (l5 != 0) {
			i = l1 / l5;
			j = i2 / l5;
			if (i < 0)
				i = 0;
			else if (i > 16256)
				i = 16256;
		}
		l1 += k2;
		i2 += l2;
		j2 += i3;
		l5 = j2 >> 14;
		if (l5 != 0) {
			j4 = l1 / l5;
			l4 = i2 / l5;
			if (j4 < 7)
				j4 = 7;
			else if (j4 > 16256)
				j4 = 16256;
		}
		int j7 = j4 - i >> 3;
		int l7 = l4 - j >> 3;
		i += j1 & 0x600000;
		int j8 = j1 >> 23;
		if (aBoolean1463) {
			while (k3-- > 0) {
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i = j4;
				j = l4;
				l1 += k2;
				i2 += l2;
				j2 += i3;
				int i6 = j2 >> 14;
				if (i6 != 0) {
					j4 = l1 / i6;
					l4 = i2 / i6;
					if (j4 < 7)
						j4 = 7;
					else if (j4 > 16256)
						j4 = 16256;
				}
				j7 = j4 - i >> 3;
				l7 = l4 - j >> 3;
				j1 += j3;
				i += j1 & 0x600000;
				j8 = j1 >> 23;
			}
			for (k3 = i1 - l & 7; k3-- > 0; ) {
				if (z1 < depthBuffer[k]) {
					ai[k] = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				}
				k++;
				z1 += dz;
				i += j7;
				j += l7;
			}
			
			return;
		}
		while (k3-- > 0) {
			int i9;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i += j7;
			j += l7;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i += j7;
			j += l7;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i += j7;
			j += l7;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i += j7;
			j += l7;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i += j7;
			j += l7;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i += j7;
			j += l7;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i += j7;
			j += l7;
			if (z1 < depthBuffer[k]) {
				if ((i9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8) != 0)
					ai[k] = i9;
			}
			k++;
			z1 += dz;
			i = j4;
			j = l4;
			l1 += k2;
			i2 += l2;
			j2 += i3;
			int j6 = j2 >> 14;
			if (j6 != 0) {
				j4 = l1 / j6;
				l4 = i2 / j6;
				if (j4 < 7)
					j4 = 7;
				else if (j4 > 16256)
					j4 = 16256;
			}
			j7 = j4 - i >> 3;
			l7 = l4 - j >> 3;
			j1 += j3;
			i += j1 & 0x600000;
			j8 = j1 >> 23;
		}
		for (int l3 = i1 - l & 7; l3-- > 0; ) {
			if (z1 < depthBuffer[k]) {
				int j9 = ai1[(j & 0x3f80) + (i >> 7)] >>> j8;
				if (j9 != 0)
					ai[k] = j9;
			}
			k++;
			i += j7;
			j += l7;
			z1 += dz;
		}
	}
	
	/**
	 * Lobby Screen:
	 */
	private static int[] OFFSETS_512_334 = null;
	private static int[] OFFSETS_765_503 = null;
	// smoothen ground?
	public static boolean smoothGround = true;
	public static boolean firstPerson = false;
	
	public static int[] getOffsets(int j, int k) {
		if (j == 512 && k == 334 && OFFSETS_512_334 != null)
			return OFFSETS_512_334;
		
		if (j == 765 + 1 && k == 503 && OFFSETS_765_503 != null)
			return OFFSETS_765_503;
		
		int[] t = new int[k];
		for (int l = 0; l < k; l++)
			t[l] = j * l;
		
		if (j == 512 && k == 334)
			OFFSETS_512_334 = t;
		
		if (j == 765 + 1 && k == 503)
			OFFSETS_765_503 = t;
		
		return t;
	}
	
}