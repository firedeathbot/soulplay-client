package com.soulplayps.client.node.raster.sprite;

import com.soulplayps.client.Config;
import com.soulplayps.client.ErrorHandler;
import com.soulplayps.client.data.SignLink;
import com.soulplayps.client.util.HttpUpdate;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Arrays;

public final class SpriteCache {

	private static boolean sentError;
    private static Sprite[] cache;
    public static int spriteCount;
    private static RandomAccessFile dataRaf;
    private static RandomAccessFile metaRaf;

    public static BufferedImage background;
    public static Sprite backgroundSprite;
    
    private static final String newfileName = "main_file_sprites";

    private final static Sprite placeholder = new Sprite(32, 32);

    public static void setLoadingBackground() {
        HttpUpdate.updateCacheFile(Config.BACKGROUND_URL, "background.png");

        try {
            File onDisk = new File(SignLink.cacheDir, "background.png");
            if (onDisk.exists()) {
                background = ImageIO.read(onDisk);
                if (background != null) {
                    backgroundSprite = new Sprite(background.getWidth(), background.getHeight(), 0, 0, SpriteTools.bufferedImageToPixels(background));
                }
            } else {
                updateBackgroundFromIMGUR();
            }
        } catch (IOException e1) {
            ErrorHandler.submitError("Failed to set background.", e1);
        }
    }

    public static void updateBackgroundFromIMGUR() {
        try {
            File onDisk = new File(SignLink.cacheDir, "background.png");
            if (onDisk.exists()) {
                background = ImageIO.read(onDisk);
                if (background != null) {
                    backgroundSprite = new Sprite(background.getWidth(), background.getHeight(), 0, 0, SpriteTools.bufferedImageToPixels(background));
                }
            } else {
                background = ImageIO.read(new URL("http://i.imgur.com/OJFfbal.png")); // download
                // the
                // background
                ImageIO.write(background, "png", onDisk); // then save it to disk

                if (background != null) {
                    backgroundSprite = new Sprite(background.getWidth(), background.getHeight(), 0, 0, SpriteTools.bufferedImageToPixels(background));
                }
            }
        } catch (IOException e) {
            ErrorHandler.submitError("Failed to retrieve background.", e);
        }
    }

    public static void init() {
        try {
            final File cacheDir = new File(SignLink.cacheDir);

            if (!cacheDir.exists()) {
                return;
            }
            
            final File dataFile = new File(cacheDir, newfileName + ".dat");
            final File metaFile = new File(cacheDir, newfileName + ".idx");

            if (!dataFile.exists() || dataFile.length() == 0) {
            	HttpUpdate.updateCacheFile(Config.SPRITES_DATA, newfileName + ".dat");
            }

            if (!metaFile.exists() || metaFile.length() == 0) {
            	HttpUpdate.updateCacheFile(Config.SPRITES_META, newfileName + ".idx");
            }

            try {
                if (dataRaf != null) {
                    dataRaf.close();
                }

                if (metaRaf != null) {
                    metaRaf.close();
                }
            } catch (Exception ex) {
                ErrorHandler.submitError("Failed to close sprite channels.", ex);
            }

            try {
                dataRaf = new RandomAccessFile(dataFile.toPath().toString(), "r");
                metaRaf = new RandomAccessFile(metaFile.toPath().toString(), "r");
            } catch (Exception ex) {
                ErrorHandler.submitError("Failed to open sprite rafs.", ex);
            }

            spriteCount = (int) (metaRaf.length() / 10);
            cache = new Sprite[spriteCount];
        } catch (Exception ex) {
            ErrorHandler.submitError("Failed to load sprites.", ex);
        }
    }

    public static Sprite get(int id) {
        try {
            if (isInvalid(id)) {
                throw new IllegalArgumentException(String.format("Sprite=%d is not valid. Valid range is from [0, %d]", id, spriteCount - 1));
            } else if (contains(id)) {
                return cache[id];
            }

            metaRaf.seek(id * 10);

            final int pos = ((metaRaf.readByte() & 0xFF) << 16) | ((metaRaf.readByte() & 0xFF) << 8) | (metaRaf.readByte() & 0xFF);
            final int len = ((metaRaf.readByte() & 0xFF) << 16) | ((metaRaf.readByte() & 0xFF) << 8) | (metaRaf.readByte() & 0xFF);
            final int offsetX = metaRaf.readShort() & 0xFFFF;
            final int offsetY = metaRaf.readShort() & 0xFFFF;

            dataRaf.seek(pos);

            final byte[] data = new byte[len];

            dataRaf.read(data);

            try (InputStream is = new ByteArrayInputStream(data)) {

                BufferedImage bimage = ImageIO.read(is);

                if (bimage == null) {
                    throw new IllegalStateException(String.format("Could not read image at %d", id));
                }

                final int[] pixels = SpriteTools.bufferedImageToPixels(bimage);

                final Sprite sprite = new Sprite(bimage.getWidth(), bimage.getHeight(), offsetX, offsetY, pixels);
                fixSprites(sprite, id);
                // cache so we don't have to perform I/O calls again
                cache[id] = sprite;
                return sprite;
            }
        } catch (Exception ex) {
        	if (!sentError) {
        		ErrorHandler.submitError(String.format("Failed to retrieve sprite=%d", id), ex);
        		sentError = true;
        	}

            return placeholder;
        }
    }
    
    private static void fixSprites(Sprite sprite, int id) {
    	switch (id) {
        	case 804:
        	case 805:
        	case 806:
        	case 807:
        	case 808:
        	case 822:
        	case 823:
        	case 821:
        		sprite.drawOffsetX = -2;
    			sprite.drawOffsetY = -2;
    			whiteFix(sprite);
    			break;
        	case 820:
        	case 819:
        	case 818:
        	case 817:
        	case 816:
        	case 815:
        	case 814:
        	case 813:
    		case 812:
    		case 811:
    		case 810:
    		case 809:
	    	case 469:
	    	case 525:
	    	case 1256:
	    	case 1257:
	    	case 1274:
	    	case 1275:
	    	case 1276:
	    	case 1277:
	    	case 1278:
	    	case 1279:
	    	case 1282:
	    	case 1283:
	    	case 1285:
	    	case 1210:
	    		whiteFix(sprite);
	    		break;
	    	case 1247:
	    	case 1248:
	    	case 1249:
	    		sprite.drawOffsetY = 2;
	    		whiteFix(sprite);
	    		break;
    	}
    }
   
    private static void whiteFix(Sprite sprite) {
   		int[] pixels = sprite.myPixels;
		for (int i = 0, length = pixels.length; i < length; i++) {
			int pixel = pixels[i] & ~0xff000000;
			if (pixel == 0xffffff) {
				pixel = 0;
			}

			pixels[i] = pixel;
		}
    }

    public static boolean contains(int id) {
        return cache[id] != null;
    }

    private static boolean isInvalid(int id) {
        return id < 0 || id >= spriteCount;
    }

    public void set(int id, Sprite sprite) {
        if (!contains(id)) {
            return;
        }

        cache[id] = sprite;
    }

    public static void clear() {
        Arrays.fill(cache, null);
    }

    private SpriteCache() {

    }

}

