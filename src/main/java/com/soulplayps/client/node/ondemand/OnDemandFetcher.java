package com.soulplayps.client.node.ondemand;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.ErrorHandler;
import com.soulplayps.client.data.SignLink;
import com.soulplayps.client.node.NodeList;
import com.soulplayps.client.node.NodeSubList;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.fs.RSArchive;

import java.io.*;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.CRC32;

public final class OnDemandFetcher implements Runnable {

	private boolean crcMatches(int crc, byte buffer[]) {
		if (buffer == null || buffer.length < 2) {
			return false;
		}
		
		int length = buffer.length - 2;
		crc32.reset();
		crc32.update(buffer, 0, length);
		int ourCrc = (int) crc32.getValue();
		return ourCrc == crc;
	}

	/**
	 * Grabs the checksum of a file from the cache.
	 *
	 * @param type The type of file (0 = model, 1 = anim, 2 = midi, 3 = map).
	 * @param id   The id of the file.
	 * @return
	 */
	public int getChecksum(int type, int id) {
		byte[] data = clientInstance.fileStores[type + 1].readFile(id);
		if (data == null) {
			return 0;
		}
		
		int length = data.length - 2;
		crc32.reset();
		crc32.update(data, 0, length);
		return (int) crc32.getValue();
	}
	
	/**
	 * Writes the checksum list for the specified archive type and length.
	 *
	 * @param type   The type of archive (0 = model, 1 = anim, 2 = midi, 3 = map, 4, textures, 6 osrs models, 7 osrs maps, 8 sfx, 9 667 models ).
	 * @param type The number of files in the archive.
	 */
	public void writeChecksumList(String file, int type) {
		final DataOutputStream out;
		try {
			out = new DataOutputStream(new FileOutputStream(SignLink.cacheDir + "crc/" + file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		try {
			long length = clientInstance.fileStores[type + 1].getFileCount();
			System.out.println("writing " + length + ", index="+(type + 1));
			for (int index = 0; index < length; index++) {
				out.writeInt(getChecksum(type, index));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		
		// Get the size of the file
		long length = file.length();
		
		// You cannot create an array using a long interfaceType.
		// It needs to be an int interfaceType.
		// Before converting to an int interfaceType, check
		// to ensure that file is not larger than Integer.MAX_VALUE.
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		
		// Create the byte array to hold the data
		byte[] bytes = new byte[(int) length];
		
		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
		
		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			is.close();
			throw new IOException("Could not completely read file " + file.getName());
		}
		
		// Close the input stream and return bytes
		is.close();
		return bytes;
	}
	
	private void readData() {
		try {
			int available = inputStream.available();
			if (expectedSize == 0 && available >= 11) {
				waiting = true;
				inputStream.read(ioBuffer, 0, 11);
				int indexId = ioBuffer[0] & 0xff;
				int fileId = ((ioBuffer[1] & 0xff) << 24) + ((ioBuffer[2] & 0xff) << 16) + ((ioBuffer[3] & 0xff) << 8) + (ioBuffer[4] & 0xff);
				int fileLength = ((ioBuffer[5] & 0xff) << 24) + ((ioBuffer[6] & 0xff) << 16) + ((ioBuffer[7] & 0xff) << 8) + (ioBuffer[8] & 0xff);
				int chunkId = ((ioBuffer[9] & 0xff) << 8) + (ioBuffer[10] & 0xff);
				current = null;
				for (OnDemandData onDemandData = (OnDemandData) requested
						.reverseGetFirst(); onDemandData != null; onDemandData = (OnDemandData) requested
						.reverseGetNext()) {
					if (onDemandData.indexId == indexId && onDemandData.fileId == fileId)
						current = onDemandData;
					if (current != null)
						onDemandData.timeout = 0;
				}
				
				if (current != null) {
					socketTimeout = 0;
					if (fileLength == 0) {
						SignLink.reporterror("Rej: " + indexId + "," + fileId);
						current.buffer = null;
						if (current.incomplete)
							synchronized (doneLoading) {
								doneLoading.insertHead(current);
							}
						else
							current.unlink();
						current = null;
					} else {
						if (current.buffer == null && chunkId == 0)
							current.buffer = new byte[fileLength];
						if (current.buffer == null && chunkId != 0)
							throw new IOException("missing start of file");
					}
				}
				bufferOffset = chunkId * 500;
				expectedSize = 500;
				if (expectedSize > fileLength - bufferOffset) {
					expectedSize = fileLength - bufferOffset;
				}
			}
			if (expectedSize > 0 && available >= expectedSize) {
				waiting = true;
				byte buffer[] = ioBuffer;
				int offset = 0;
				if (current != null) {
					buffer = current.buffer;
					offset = bufferOffset;
				}

				inputStream.read(buffer, offset, expectedSize);
				if (expectedSize + bufferOffset >= buffer.length
						&& current != null) {
					if (clientInstance.fileStores[0] != null) {
						Client.fileSystemWorker.requestWrite(clientInstance.fileStores[current.indexId + 1], buffer, current.fileId);
					}
					if (!current.incomplete && current.indexId == 3) {
						current.incomplete = true;
						current.indexId = 93;
					}
					if (current.incomplete)
						synchronized (doneLoading) {
							doneLoading.insertHead(current);
						}
					else
						current.unlink();
				}
				expectedSize = 0;
			}
		} catch (IOException ioexception) {
			ioexception.printStackTrace();
			closeConnection();
		}
	}
	
	public int mapAmount = 0;
	
	public void start(RSArchive streamLoader, Client client1) {
		String[] crc_files = { "model_crc", "anim_crc", "midi_crc", "map_crc", "texture_crc", "model2_crc", "map2_crc", "midi2_crc", "model3_crc" };

		if (Config.USE_UPDATE_SERVER) {
			for (int k = 0; k < crc_files.length; k++) {
				byte abyte1[] = streamLoader.readFile(crc_files[k]);
				int i1 = abyte1.length / 4;
				RSBuffer stream_1 = new RSBuffer(abyte1);
				
				crcs[k] = new int[i1 + 1];
				for (int l1 = 0; l1 < i1; l1++)
					crcs[k][l1] = stream_1.readDWord();
			}
		} else {
			emptyMapFiles = new Set[2];
			emptyMapFiles[0] = new HashSet<>();
			emptyMapFiles[1] = new HashSet<>();
		}

		byte[] abyte2 = streamLoader.readFile("map_index");
		RSBuffer stream2 = new RSBuffer(abyte2);
		int j1 = stream2.readUnsignedWord();//abyte2.length / 6; // this is 317 maps size /6 thing
		
		byte[] osrs = streamLoader.readFile("map_index_osrs");
		RSBuffer streamOsrs = new RSBuffer(osrs);
		int osrsSize = streamOsrs.readUnsignedWord();
		System.out.println("osrs 171 map size:" + osrsSize);
		
		int loopval = j1;
		j1 = j1 + 30 + osrsSize;
		mapIndices1 = new int[j1];
		mapIndices2 = new int[j1];
		mapIndices3 = new int[j1];
		osrsMap = new boolean[j1];
		for (int i2 = 0; i2 < loopval; i2++) {
			mapIndices1[i2] = stream2.readUnsignedWord();
			mapIndices2[i2] = stream2.readUnsignedWord();
			mapIndices3[i2] = stream2.readUnsignedWord();
			mapAmount++;

//			if (mapIndices3[i2] == 4466)
//				mapIndices3[i2] = 4484;
			
			switch (mapIndices3[i2]) {
			case 4432:
			case 4848:
			case 4808:
			case 4287: 
			case 5172:
			case 5174:
				mapIndices3[i2] = -1;
				mapIndices2[i2] = -1;
				break;
			}
			
			switch (mapIndices2[i2]) {
			case 5175:
				mapIndices3[i2] = -1;
				mapIndices2[i2] = -1;
				break;
			}
			
			if (mapIndices1[i2] >= 25600) { // disable loading all maps past x=6400 cuz of osrs maps.
				mapIndices1[i2] = -1;
				mapIndices2[i2] = -1;
				mapIndices3[i2] = -1;
			}

//			if (mapIndices1[i2] == 324) {
////				mapIndices2[i2] = 3601;
////				mapIndices3[i2] = 3602;
//				System.out.println("Map1:"+mapIndices2[i2]+" map2:"+mapIndices3[i2]);
//			}

//			System.out.println("mapIndices1["+i2+"] = "+mapIndices1[i2]+";");
//			System.out.println("mapIndices2["+i2+"] = "+mapIndices2[i2]+";");
//			System.out.println("mapIndices3["+i2+"] = "+mapIndices3[i2]+";");
		}
		
		lastIndex = mapAmount;
		
		System.out.println("last mapIndex:" + mapAmount);
		
		//index,regionId,mapFile1,mapFile2
//		loadNewMap(13881, 4161, 4162); // dungeoneering lobby right side
		
		
		loadNewMap(9007, 5180, 5181, false); // zulrah shrine
		loadNewMap(8495, 5182, 5183, false);    // port tyras
		loadNewMap(8751, 5184, 5185, false); // port tyras
		loadNewMap(8756, 5186, 5187, false); // prifddinas
		loadNewMap(54618, 5188, 5189, false);// kraken cave //14682
		loadNewMap(54873, 5190, 5191, false);// smoke devils
		loadNewMap(7227, 7000, 7001, false);// last man standing //13658
		loadNewMap(7228, 7002, 7003, false);// last man standing //13659
		loadNewMap(7483, 7004, 7005, false);// last man standing //13914
		loadNewMap(7484, 7006, 7007, false);// last man standing //13915
		
		loadNewMap(4919, 7008, 7009, false); // raids aboveground
		loadNewMap(13137, 7010, 7011, false); // raids
		
		loadNewMap(9043, 7012, 7013, false); // inferno
		
		loadNewMap(4709, 7014, 7015, false); // 7155 7156
		loadNewMap(5731, 7016, 7017, false); // 7174 7175
		
		loadNewMap(4883, 7018, 7019, false); // cerberus

		loadNewMap(5444, 6000, 6001, false);// donator thing
		loadNewMap(5445, 6002, 6003, false);// donator thing
		loadNewMap(5700, 6004, 6005, false);// donator thing
		loadNewMap(5701, 6006, 6007, false);// donator thing
		
		loadNewMap(4676, 6008, 6009, false);// clan citadel
		loadNewMap(4677, 6010, 6011, false);// clan citadel
		loadNewMap(4932, 6012, 6013, false);// clan citadel
		loadNewMap(4933, 6014, 6015, false);// clan citadel

//		System.out.println("Map Amount: " + mapAmount + "");
//		int highestId = 0;
//		System.out.println("Beggining Map Extraction");
//		for (int i2 = 1000; i2 < 1235/*mapAmount*/; i2++) {
//			if (mapIndices2[i2] > highestId)
//				highestId = mapIndices2[i2];
//			if (mapIndices3[i2] > highestId)
//				highestId = mapIndices3[i2];
//			System.out.println("mapIndices1["+i2+"] = "+ mapIndices1[i2]+";");
//			System.out.println("mapIndices2["+i2+"] = "+ mapIndices2[i2]+";");
//			System.out.println("mapIndices3["+i2+"] = "+ mapIndices3[i2]+";");
//		}
//		System.out.println("Ending Map Extraction");
//		System.out.println("highest map model ID:"+highestId);
		
		// osrs map loading
		for (int i2 = 0; i2 < osrsSize; i2++) {
			int regionId = streamOsrs.readUnsignedWord();
			int map1 = streamOsrs.readUnsignedWord();
			int map2 = streamOsrs.readUnsignedWord();
			int chunkX = regionId >> 8;
			int chunkY = regionId & 0xFF;
			chunkX += 100; // set new region area
//			int oldId = regionId;
			regionId = (chunkX << 8) + chunkY;
//			if (regionId == 37942)
//				System.out.println("regionId:"+regionId+" oldId:"+oldId);
			switch (regionId) {
				case 40757: // Broken, empty map near morytania
					map2 = -1;
					break;
			}

			loadNewMap(regionId, map1, map2, true);
		}

		clientInstance = client1;
		running = true;
		clientInstance.startRunnable(this, 2, "OnDemand thread");
        /*for(int i = 0; i < crc_files.length; i++) {
        	writeChecksumList(crc_files[i], i);
        }*/
	}
	
	private int lastIndex = 1226;
	
	public void loadNewMap(int regionId, int landscapeId, int objectsMapId, boolean osrs) {
		if (lastIndex >= mapIndices1.length) {
			System.out.println("cannot add anymore maps!!!! increase map index size. LastIndex:" + lastIndex + " regionId:" + regionId);
			return;
		}

		mapIndices1[lastIndex] = regionId;
		mapIndices2[lastIndex] = landscapeId;
		mapIndices3[lastIndex] = objectsMapId;
		osrsMap[lastIndex] = osrs;
		lastIndex++;
		mapAmount++;
	}
	
	public int getWaitingToLoadCount() {
		synchronized (waitingToLoad) {
			return waitingToLoad.getNodeCount();
		}
	}

	public void removeEmpties() {
		synchronized (waitingToLoad) {
			for (OnDemandData a = (OnDemandData) emptyFiles.reverseGetFirst(); a != null; a = (OnDemandData) emptyFiles.reverseGetNext()) {
				a.unlinkSub();
				emptyMapFiles[a.indexId == 3 ? 0 : 1].add(a.fileId);
			}
		}
	}

	public void disable() {
		running = false;
	}
	
	public int getVersionCount(int j) {
		return crcs[j].length;
	}
	
	private void sendRequestToServer(OnDemandData onDemandData) {
		try {
			if (socket == null) {
				long currentTime = System.currentTimeMillis();
				if (currentTime - openSocketTime < 4000L) {
					return;
				}

				openSocketTime = currentTime;
				socket = clientInstance.openSocket(Config.UPDATE_SERVER_IP, Config.ONDEMAND_PORT);
				inputStream = socket.getInputStream();
				outputStream = socket.getOutputStream();
				outputStream.write(15);
				for (int j = 0; j < 8; j++) {
					inputStream.read();
				}

				socketTimeout = 0;
			}

			ioBuffer[0] = (byte) onDemandData.indexId;
			ioBuffer[1] = (byte) (onDemandData.fileId >> 24);
			ioBuffer[2] = (byte) (onDemandData.fileId >> 16);
			ioBuffer[3] = (byte) (onDemandData.fileId >> 8);
			ioBuffer[4] = (byte) onDemandData.fileId;
			if (onDemandData.incomplete)
				ioBuffer[5] = 2;
			else if (!Client.loggedIn)
				ioBuffer[5] = 1;
			else
				ioBuffer[5] = 0;
			outputStream.write(ioBuffer, 0, 6);
			outputStreamTimeout = 0;
			anInt1349 = -10000;
			return;
		} catch (IOException ioexception) {
			ioexception.printStackTrace();
		}

		closeConnection();
		anInt1349++;
	}
	
	public int getAnimCount() {
		return 29192;//return anIntArray1360.length;
	}
	
	public int getModelCount() {
		return 65331;
	}
	
	public boolean loadData(int indexId, int fileId) {
		try {
			if (indexId < 0 || fileId < 0) {
				return true;
			}

			if (!Config.USE_UPDATE_SERVER) {
				int arrayIndex;
				switch (indexId) {
					case 3:
						arrayIndex = 0;
						break;
					case 6:
						arrayIndex = 1;
						break;
					default:
						arrayIndex = -1;
						break;
				}

				if (arrayIndex != -1 && emptyMapFiles[arrayIndex].contains(fileId)) {
					return false;
				}
			}

			synchronized (waitingToLoad) {
				for (OnDemandData onDemandData = (OnDemandData) waitingToLoad.reverseGetFirst(); onDemandData != null; onDemandData = (OnDemandData) waitingToLoad.reverseGetNext()) {
					if (onDemandData.indexId == indexId && onDemandData.fileId == fileId) {
						return true;
					}
				}

				OnDemandData onDemandData_1 = new OnDemandData();
				onDemandData_1.indexId = indexId;
				onDemandData_1.fileId = fileId;
				synchronized (aClass19_1370) {
					aClass19_1370.insertHead(onDemandData_1);
				}
				waitingToLoad.insertHead(onDemandData_1);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return true;
	}
	
	public void run() {
		try {
			while (running) {
				onDemandCycle++;
				int sleep = 20;
				if (anInt1332 == 0 && clientInstance.fileStores[0] != null) {
					sleep = 50;
				}
				
				try {
					Thread.sleep(sleep);
				} catch (Exception _ex) {
					_ex.printStackTrace();
				}
				
				waiting = true;
				for (int j = 0; j < 100; j++) {
					if (!waiting)
						break;
					waiting = false;
					checkReceived();
					handleFailed();
					if (uncompletedCount == 0 && j >= 5)
						break;
					if (inputStream != null)
						readData();
				}
				
				boolean flag = false;
				for (OnDemandData onDemandData = (OnDemandData) requested
						.reverseGetFirst(); onDemandData != null; onDemandData = (OnDemandData) requested
						.reverseGetNext())
					if (onDemandData.incomplete) {
						flag = true;
						onDemandData.timeout++;
						if (onDemandData.timeout > 50) {
							onDemandData.timeout = 0;
							sendRequestToServer(onDemandData);
						}
					}
				
				if (!flag) {
					for (OnDemandData onDemandData_1 = (OnDemandData) requested
							.reverseGetFirst(); onDemandData_1 != null; onDemandData_1 = (OnDemandData) requested
							.reverseGetNext()) {
						flag = true;
						onDemandData_1.timeout++;
						if (onDemandData_1.timeout > 50) {
							onDemandData_1.timeout = 0;
							sendRequestToServer(onDemandData_1);
						}
					}
					
				}
				if (flag) {
					socketTimeout++;
					if (socketTimeout > 750) {
						closeConnection();
					}
				} else {
					socketTimeout = 0;
				}
				if (Client.loggedIn
						&& socket != null
						&& outputStream != null
						&& (anInt1332 > 0 || clientInstance.fileStores[0] == null)) {
					outputStreamTimeout++;
					if (outputStreamTimeout > 500) {
						outputStreamTimeout = 0;
						ioBuffer[0] = 0;
						ioBuffer[1] = 0;
						ioBuffer[2] = 0;
						ioBuffer[3] = 0;
						ioBuffer[4] = 0;
						ioBuffer[5] = 10;
						try {
							outputStream.write(ioBuffer, 0, 6);
						} catch (IOException _ex) {
							socketTimeout = 5000;
						}
					}
				}
			}
			
			closeConnection();
		} catch (Exception exception) {
			ErrorHandler.submitError("ondemand", exception);
			SignLink.reporterror("od_ex " + exception.getMessage());
			exception.printStackTrace();
		}
	}
	
	private void closeConnection() {
		if (socket != null) {
			try {
				socket.close();
			} catch (Exception _ex) {
				_ex.printStackTrace();
			}
			socket = null;
		}
		
		inputStream = null;
		outputStream = null;
		expectedSize = 0;
	}
	
	public void method560(int i, int j) { //method560 //insertExtraFilesRequest
		if (clientInstance.fileStores[0] == null)
			return;
		if (anInt1332 == 0)
			return;
		OnDemandData onDemandData = new OnDemandData();
		onDemandData.indexId = j;
		onDemandData.fileId = i;
		onDemandData.incomplete = false;
		synchronized (aClass19_1344) {
			aClass19_1344.insertHead(onDemandData);
		}
	}
	
	public OnDemandData checkDoneLoading() {
		OnDemandData entry;
		synchronized (doneLoading) {
			entry = (OnDemandData) doneLoading.popHead();
		}
		if (entry == null) {
			return null;
		}
		synchronized (waitingToLoad) {
			entry.unlinkSub();
		}
		if (entry.buffer == null) {
			return entry;
		}
		int bufferLen = gzipDecompressor.decompress(entry.buffer, gzipInputBuffer);
		entry.buffer = new byte[bufferLen];
		System.arraycopy(gzipInputBuffer, 0, entry.buffer, 0, bufferLen);
		return entry;
	}
	
	public int method562(int i, int regionId) {
		try {
//			System.out.println("i1:"+i1);
			for (int j1 = 0; j1 < mapIndices1.length; j1++) {
				if (mapIndices1[j1] == regionId) {
					if (i == 0)
						return mapIndices2[j1];
					else
						return mapIndices3[j1];
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return -1;
	}

//	public int method562(int i, int k, int l) {
//		int i1 = (l << 8) + k;
//		int mapNigga2;
//		int mapNigga3;
//		for (int j1 = 0; j1 < mapIndices1.length; j1++) {
//			if (mapIndices1[j1] == i1) {
//				if (i == 0) {
//					mapNigga2 = mapIndices2[j1] > 3535 ? -1 : mapIndices2[j1];
//					return mapNigga2;
//				} else {
//					mapNigga3 = mapIndices3[j1] > 3535 ? -1 : mapIndices3[j1];
//					return mapNigga3;
//				}
//			}
//		}
//		return -1;
//	}
	
	public boolean method564(int i) {
		for (int k = 0; k < mapIndices1.length; k++)
			if (mapIndices3[k] == i)
				return true;
		return false;
	}
	
	private void handleFailed() {
		uncompletedCount = 0;
		for (OnDemandData onDemandData = (OnDemandData) requested
				.reverseGetFirst(); onDemandData != null; onDemandData = (OnDemandData) requested
				.reverseGetNext()) {
			if (onDemandData.incomplete) {
				uncompletedCount++;
				/*System.out
						.println("Error: model is incomplete or missing  [ type = "
								+ onDemandData.dataType
								+ "]  [id = "
								+ onDemandData.ID + "]");*/
				
				//JOptionPane.showMessageDialog(null, "Error to report: model is incomplete or missing  [ type = " + onDemandData.dataType + "]  [id = " + onDemandData.ID + "]");
			}
		}
		
		while (uncompletedCount < 10) {
			OnDemandData onDemandData_1 = (OnDemandData) aClass19_1368.popHead();
			try {
				if (!Config.USE_UPDATE_SERVER || onDemandData_1 == null) {
					break;
				}

				requested.insertHead(onDemandData_1);
				uncompletedCount++;
				sendRequestToServer(onDemandData_1);
				waiting = true;
				/*System.out.println("Error: file is missing  [ type = "
						+ onDemandData_1.dataType + "]  [id = "
						+ onDemandData_1.ID + "]");*/
			} catch (Exception _ex) {
				System.err.println("missing: type: " + onDemandData_1.indexId + " ID" + onDemandData_1.fileId);
				_ex.printStackTrace();
				//JOptionPane.showMessageDialog(null, "Please report - missing: type: "+onDemandData_1.dataType+" ID"+onDemandData_1.ID);
			}
		}
	}
	
	public void method566() {
		synchronized (aClass19_1344) {
			aClass19_1344.removeAll();
		}
	}

	private void checkReceived() {
		OnDemandData onDemandData;
		synchronized (aClass19_1370) {
			onDemandData = (OnDemandData) aClass19_1370.popHead();
		}
		while (onDemandData != null) {
			waiting = true;
			byte data[] = null;
			if (clientInstance.fileStores[0] != null)
				data = clientInstance.fileStores[onDemandData.indexId + 1].readFile(onDemandData.fileId);
			try {
				if (Config.USE_UPDATE_SERVER && !crcMatches(crcs[onDemandData.indexId][onDemandData.fileId], data)) {
					data = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(onDemandData.indexId + " " + onDemandData.fileId + " " + crcs[onDemandData.indexId].length);
			}
			
			synchronized (aClass19_1370) {
				if (data == null) {
					aClass19_1368.insertHead(onDemandData);
					if (!Config.USE_UPDATE_SERVER) {
						emptyFiles.insertHead(onDemandData);
					}
				} else {
					onDemandData.buffer = data;
					synchronized (doneLoading) {
						doneLoading.insertHead(onDemandData);
					}
				}
				onDemandData = (OnDemandData) aClass19_1370.popHead();
			}
		}
	}
	
	public OnDemandFetcher() {
		requested = new NodeList();
		crc32 = new CRC32();
		ioBuffer = new byte[500];
		aClass19_1344 = new NodeList();
		running = true;
		waiting = false;
		doneLoading = new NodeList();
		gzipInputBuffer = new byte[999999];//new byte[0x71868]; //9999999
		waitingToLoad = new NodeSubList();
		aClass19_1368 = new NodeList();
		emptyFiles = new NodeList();
		aClass19_1370 = new NodeList();
	}
	
	private CRC32 crc32;
	private final NodeList requested;
	private int anInt1332;
	private int outputStreamTimeout;
	private long openSocketTime;
	public int[] mapIndices3;
	public boolean[] osrsMap;
	private final byte[] ioBuffer;
	public int onDemandCycle;
	private Client clientInstance;
	private final NodeList aClass19_1344;
	private int bufferOffset;
	private int expectedSize;
	public int anInt1349;
	public int[] mapIndices2;
	private boolean running;
	private OutputStream outputStream;
	private boolean waiting;
	private final NodeList doneLoading;
	private final byte[] gzipInputBuffer;
	private final NodeSubList waitingToLoad;
	private InputStream inputStream;
	private Socket socket;
	private int uncompletedCount;
	private final NodeList aClass19_1368;
	private final NodeList emptyFiles;
	private OnDemandData current;
	private final NodeList aClass19_1370;
	public int[] mapIndices1;
	private int socketTimeout;
	private Set<Integer>[] emptyMapFiles;
	private GzipDecompressor gzipDecompressor = new GzipDecompressor();
	private final int[][] crcs = new int[SignLink.INDEX_SIZE - 1][];
}
