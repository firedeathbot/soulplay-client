package com.soulplayps.client.node.ondemand;

import com.soulplayps.client.node.NodeSub;

public final class OnDemandData extends NodeSub {
	public int indexId;
	public byte buffer[];
	public int fileId;
	public boolean incomplete = true;
	public int timeout;
}
