package com.soulplayps.client.node.animable.entity;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.ObjectCache;
import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.node.animable.item.CustomizedItem;
import com.soulplayps.client.node.animable.item.ItemDef;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.unknown.Class36;
import com.soulplayps.client.unpack.IDK;

public class PlayerAppearance {

	public static final int COLOR_LENGTH = 5;
	public static final int EQUIPMENT_LENGTH = 12;
	public static ObjectCache mruNodes = new ObjectCache(260);
	private long lastUsedHash = -1L;
	private long appearanceHash;
	public int npcId;
	public int[] equipment;
	public boolean gender;
	public int[] appearance;
	public CustomizedItem[] customizedItems;

	public void updateHash() {
		final long oldHash = appearanceHash;
		long[] crcTable = RSBuffer.CRC_64;
		appearanceHash = -1L;
		for (int i = 0, length = equipment.length; i < length; i++) {
			int id = equipment[i];
			appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ id >> 24) & 0xffL)]);
			appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ id >> 16) & 0xffL)]);
			appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ id >> 8) & 0xffL)]);
			appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ id) & 0xffL)]);
		}
		
		if (customizedItems != null) {
			for (CustomizedItem customizedItem : customizedItems) {
				if (customizedItem != null) {
					if (customizedItem.newModelColors != null) {
						for (int id = 0; id < customizedItem.newModelColors.length; id++) {
							appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ customizedItem.newModelColors[id] >> 8) & 0xffL)]);
							appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ customizedItem.newModelColors[id]) & 0xffL)]);
						}
					}
				}
			}
		}

		for (int i = 0, length = appearance.length; i < length; i++) {
			int color = appearance[i];
			appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ color) & 0xffL)]);
		}

		appearanceHash = (appearanceHash >>> 8 ^ crcTable[(int) ((appearanceHash ^ (gender ? 1 : 0)) & 0xffL)]);
		if (oldHash != 0L && appearanceHash != oldHash) {
			mruNodes.remove(oldHash);
		}
	}

	public Model method452(Animation seq, Animation baseSeq, int currentFrame, int nextFrame, int currentDelay, int baseAnimFrame, int baseAnimNextFrame, int baseAnimDelay) {
		if (npcId != -1) {
			int j = -1;
			if (seq != null) {
				j = seq.frame2Ids[currentFrame];
			} else if (baseSeq != null) {
				j = baseSeq.frame2Ids[baseAnimFrame];
			}

			return EntityDef.forID(npcId).method164(-1, j, null, false, 128);
		}

		int[] appData = equipment;
		long l = appearanceHash;
		if (seq != null && (seq.shieldDisplayed >= 0 || seq.weaponDisplayed >= 0)) {
			appData = new int[12];
			for (int id = 0; id < 12; id++) {
				appData[id] = equipment[id];
			}

			if (seq.shieldDisplayed >= 0) {
				if (seq.shieldDisplayed == 65535) {//65535 to remove
					appData[5] = 0;
					l ^= ~0xffffffffL;
				} else {
					appData[5] = seq.shieldDisplayed + 512;
					l ^= (long) appData[5] << 32;
				}
			}

			if (seq.weaponDisplayed >= 0) {
				if (seq.weaponDisplayed == 65535) {//65535 to remove
					appData[3] = 0;
					l ^= 0xffffffffL;
				} else {
					appData[3] = seq.weaponDisplayed + 512;
					l ^= appData[3];
				}
			}
		}

		Model model_1 = (Model) mruNodes.get(l);
		if (model_1 == null) {
			boolean flag = false;
			for (int i2 = 0; i2 < 12; i2++) {
				int k2 = appData[i2];
				if (k2 >= 256 && k2 < 512 && !IDK.cache[k2 - 256].method537())
					flag = true;
				if (k2 >= 512 && !ItemDef.forID(k2 - 512).method195(gender))
					flag = true;
			}

			if (flag) {
				if (lastUsedHash != -1L)
					model_1 = (Model) mruNodes.get(lastUsedHash);
				if (model_1 == null)
					return null;
			}
		}
		if (model_1 == null) {
			Model aclass30_sub2_sub4_sub6s[] = new Model[12];
			int j2 = 0;
			boolean fixLabels = false;
			for (int l2 = 0; l2 < 12; l2++) {
				int i3 = appData[l2];
				CustomizedItem customizedItem = null;
				if (i3 >= 256 && i3 < 512) {
					Model model_3 = IDK.cache[i3 - 256].method538();
					if (model_3 != null)
						aclass30_sub2_sub4_sub6s[j2++] = model_3;
				}
				if (i3 >= 512) {
					if (customizedItems != null && customizedItems[l2] != null) {
						customizedItem = customizedItems[l2];
					}

					int itemId = i3 - 512;
					Model model_4 = ItemDef.forID(itemId)
							.getEquippedModel(gender, customizedItem);
					if (model_4 != null) {
						aclass30_sub2_sub4_sub6s[j2++] = model_4;

						if (l2 == 3) {//Weapon
							switch(itemId) {
								case 12006:
								case 12926:
								case 30023:
								case 30024:
								case 30088:
								case 30090:
								case 30092:
								case 30094: // dihns bulwark
								case 30127:
								case 30327:
								case 30328:
								case 30382:
								case 30390:
								case 30392:
								case 30068: // infernal harpoon equip
								case 30069: // dragon harpoon equip
								case 30396://dragon harpoon anim
								case 30397://infernal harpoon anim
								case 30420://skeleton lantern
								case 122324://Ghrazi rapier
								case 124423://Harmonised nightmare staff
								case 124424://Volatile nightmare staff
								case 124425://Eldritch nightmare staff
								case 124417://Inquisitor's mace
									fixLabels = true;
									break;
							}
						}
					}
				}
			}

			model_1 = new Model(j2, aclass30_sub2_sub4_sub6s);
			for (int j3 = 0; j3 < 5; j3++) {
				if (appearance[j3] != 0) {
					model_1.replaceHs1(Client.anIntArrayArray1003[j3][0], Client.anIntArrayArray1003[j3][appearance[j3]]);

		            if (j3 == 4) {
		            	model_1.replaceHs1(Client.skinColor2[0], Client.skinColor2[appearance[j3]]);
                    } else if (j3 == 1) {
						model_1.replaceHs1(Client.anIntArray1204[0], Client.anIntArray1204[appearance[j3]]);
					}
				}
			}

			if (fixLabels) {
				for (int i = 0; i < model_1.anIntArray1655.length; i++) {
					int id = model_1.anIntArray1655[i];
					switch (id) {
						case 232:
						case 234:
							model_1.anIntArray1655[i] = 45;
							break;
						case 236:
						case 237:
							model_1.anIntArray1655[i] = 46;
							break;
						case 207:
						case 209:
						case 196:
							model_1.anIntArray1655[i] = 27;
							break;
						case 204:
						case 194:
						case 195:
							model_1.anIntArray1655[i] = 28;
							break;
					}
				}
			}

			model_1.applyEffects();
			// model_1.calculateLighting(64, 850, -30, -50, -30, true);
			model_1.calculateLighting(84, 1000, -90, -580, -90, true);
			mruNodes.put(model_1, l);
			lastUsedHash = l;
		}

		if (seq == null && baseSeq == null) {
			return model_1;
		}

		int currentFrameFile = -1;
		int nextFrameFile = -1;
		int currrentFrameDelay = 0;
		int baseAnimFrameFile = -1;
		int baseAnimNextFrameFile = -1;
		int baseAnimFrameDelay = 0;
		if (seq != null) {
			currentFrameFile = seq.frame2Ids[currentFrame];
			if (Client.tweening && nextFrame != -1) {
				nextFrameFile = seq.frame2Ids[nextFrame];
				currrentFrameDelay = seq.frameLengths[currentFrame];
			}

			if (baseSeq != null) {
				baseAnimFrameFile = baseSeq.frame2Ids[baseAnimFrame];
				if (Client.tweening && baseAnimNextFrame != -1) {
					baseAnimNextFrameFile = baseSeq.frame2Ids[baseAnimNextFrame];
					baseAnimFrameDelay = baseSeq.frameLengths[baseAnimFrame];
				}
			}
		} else if (baseSeq != null) {
			currentFrameFile = baseSeq.frame2Ids[baseAnimFrame];
			if (Client.tweening && baseAnimNextFrame != -1) {
				nextFrameFile = baseSeq.frame2Ids[baseAnimNextFrame];
				currrentFrameDelay = baseSeq.frameLengths[baseAnimFrame];
			}
		}

		boolean leaveAlpha = false;
		if (currentFrameFile != -1) {
			leaveAlpha |= Class36.hasAlpha(currentFrameFile);
		}
		if (nextFrameFile != -1) {
			leaveAlpha |= Class36.hasAlpha(nextFrameFile);
		}
		if (baseAnimFrameFile != -1) {
			leaveAlpha |= Class36.hasAlpha(baseAnimFrameFile);
		}
		if (baseAnimNextFrameFile != -1) {
			leaveAlpha |= Class36.hasAlpha(baseAnimNextFrameFile);
		}

		Model model_2 = model_1.copyEntity(!leaveAlpha);
		if (currentFrameFile != -1 && baseAnimFrameFile != -1) {
			model_2.method471(seq.flowControl, baseAnimFrameFile, baseAnimNextFrameFile, baseAnimDelay - 1, baseAnimFrameDelay, currentFrameFile, nextFrameFile, currentDelay - 1, currrentFrameDelay);
		} else if (currentFrameFile != -1) {
			if (nextFrameFile != -1) {
				model_2.applyAnimationTween(currentFrameFile, nextFrameFile, currentDelay - 1, currrentFrameDelay, null, false);
			} else {
				model_2.applyAnimation(currentFrameFile);
			}
		}

		model_2.method466();
		return model_2;
	}

	public Model getChatHeadModel() {
		if (npcId != -1) {
			return EntityDef.forID(npcId).method160();
		}

		boolean flag = false;
		for (int i = 0; i < 12; i++) {
			int j = equipment[i];
			if (j >= 256 && j < 512 && !IDK.cache[j - 256].cacheHeadModel())
				flag = true;
			if (j >= 512 && !ItemDef.forID(j - 512).dialogueHeadModel(gender))
				flag = true;
		}

		if (flag)
			return null;
		Model aclass30_sub2_sub4_sub6s[] = new Model[12];
		int k = 0;
		for (int l = 0; l < 12; l++) {
			int i1 = equipment[l];
			if (i1 >= 256 && i1 < 512) {
				Model model_1 = IDK.cache[i1 - 256].getHeadModel();
				if (model_1 != null)
					aclass30_sub2_sub4_sub6s[k++] = model_1;
			}
			if (i1 >= 512) {
				Model model_2 = ItemDef.forID(i1 - 512).method194(gender);
				if (model_2 != null)
					aclass30_sub2_sub4_sub6s[k++] = model_2;
			}
		}

		Model model = new Model(k, aclass30_sub2_sub4_sub6s);
		for (int j1 = 0; j1 < 5; j1++)
			if (appearance[j1] != 0) {
				model.replaceHs1(Client.anIntArrayArray1003[j1][0], Client.anIntArrayArray1003[j1][appearance[j1]]);

				if (j1 == 1) {
					model.replaceHs1(Client.anIntArray1204[0], Client.anIntArray1204[appearance[j1]]);
				}
			}

		return model;
	}

	public int getHeadModelUid() {
		if (npcId != -1) {
			return (int) (0x12345678L + npcId);
		}

		return (appearance[0] << 25) + (appearance[4] << 20) + (equipment[0] << 15) + (equipment[8] << 10)
				+ (equipment[11] << 5) + equipment[1];
	}

	public void init(int npcId, int[] appData, int[] colors, boolean isFemale) {
		this.equipment = appData;
		this.appearance = colors;
		this.gender = isFemale;
		this.npcId = npcId;
		updateHash();
	}

}
