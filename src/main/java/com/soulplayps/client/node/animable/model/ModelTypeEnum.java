package com.soulplayps.client.node.animable.model;

public enum ModelTypeEnum {

	REGULAR(0, false),
	OSRS(5, true),
	NEWEST(8, false);

	private final int index;
	private final boolean osrs;

	ModelTypeEnum(int index, boolean osrs) {
		this.index = index;
		this.osrs = osrs;
	}

	public int getIndex() {
		return index;
	}

	public boolean isOsrs() {
		return osrs;
	}

}
