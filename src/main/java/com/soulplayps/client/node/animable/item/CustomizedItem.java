package com.soulplayps.client.node.animable.item;

import com.soulplayps.client.node.io.RSBuffer;

public class CustomizedItem {

	public int[] newModelColors;
	public int id;

	public CustomizedItem(ItemDef itemDefinition) {
		id = itemDefinition.id;
		if (itemDefinition.recolorModified != null) {
			newModelColors = new int[itemDefinition.recolorModified.length];
			System.arraycopy(itemDefinition.recolorModified, 0, newModelColors, 0, newModelColors.length);
		}
	}

	public static CustomizedItem decode(ItemDef itemDefinition, RSBuffer buffer) {
		CustomizedItem customized = new CustomizedItem(itemDefinition);
		int mask = buffer.readUnsignedByte();
		boolean readModelColors = (mask & 0x1) != 0;
		if (readModelColors) {
			int length = buffer.readUnsignedByte();
			for (int i = 0; i < length; i++) {
				int index = buffer.readUnsignedByte();
				int color = buffer.readUnsignedWord();
				customized.newModelColors[index] = (short) color;
			}
		}

		return customized;
	}

}