package com.soulplayps.client.node.animable;

// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

import com.soulplayps.client.unknown.Vertex;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.NodeSub;

public class Animable extends NodeSub {

	public void method443(int i, float j, float k, float l, float i1, int j1, int k1,
			int l1, long i2, boolean animated, int level, long gluid) {
		Model model = getRotatedModel();
		if (model != null) {
			animated = model.animated;
			modelHeight = model.modelHeight;
			model.method443(i, j, k, l, i1, j1, k1, l1, i2, this.animated, level, gluid);
		}
	}
	
	public Model getRotatedModel() {
		return null;
	}

	public Animable() {
		modelHeight = 1000;
	}

	public Vertex[] aClass33Array1425;
	public int modelHeight;
	public boolean animated;
}
