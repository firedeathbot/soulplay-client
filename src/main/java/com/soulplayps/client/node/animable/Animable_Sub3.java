package com.soulplayps.client.node.animable;

// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.unpack.SpotAnim;

public final class Animable_Sub3 extends Animable {

	public Animable_Sub3(int i, int j, int l, int i1, int j1, int k1, int l1) {
		aBoolean1567 = false;
		aSpotAnim_1568 = SpotAnim.cache[i1];
		anInt1560 = i;
		anInt1561 = l1;
		anInt1562 = k1;
		anInt1563 = j1;
		anInt1564 = j + l;
		aBoolean1567 = false;
	}

	@Override
	public Model getRotatedModel() {
		super.animated = false;
		Model model = aSpotAnim_1568.getModel(frame, nextFrame, frameDelay);
		if (model == null) {
			return null;
		}

		model.method466();
		if (!aBoolean1567) {
			super.animated = true;
		}
		return model;
	}

	public void method454(int i) {
		Animation seq = aSpotAnim_1568.animation;
		if (seq == null || aBoolean1567) {
			return;
		}

		frameDelay += i;
		while (frameDelay > seq.frameLengths[frame]) {
			frameDelay -= seq.frameLengths[frame];
			frame++;
			if (frame >= seq.frameCount) {
				frame = 0;
				aBoolean1567 = true;
			}

			nextFrame = frame + 1;
			if (nextFrame >= seq.frameCount) {
				nextFrame = -1;
			}
		}
	}

	public final int anInt1560;
	public final int anInt1561;
	public final int anInt1562;
	public final int anInt1563;
	public final int anInt1564;
	public boolean aBoolean1567;
	public final SpotAnim aSpotAnim_1568;
	private int frame;
	private int nextFrame = -1;
	private int frameDelay;
}
