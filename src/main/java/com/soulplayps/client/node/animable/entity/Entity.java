package com.soulplayps.client.node.animable.entity;

import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.Client;
import com.soulplayps.client.node.animable.Animable;

public class Entity extends Animable {

	public static final int OLD_HIT_CYCLE = 70;
	public static final int NEW_HIT_CYCLE = 50;
	public static final int NEW_HIT_Y_MOVE = -10;

	public final void setPos(int i, int j, boolean flag) {
		if (animId != -1 && Animation.anims[animId].anInt364 == 1)
			animId = -1;
		if (!flag) {
			int k = i - smallX[0];
			int l = j - smallY[0];
			if (k >= -8 && k <= 8 && l >= -8 && l <= 8) {
				if (smallXYIndex < 9)
					smallXYIndex++;
				for (int i1 = smallXYIndex; i1 > 0; i1--) {
					smallX[i1] = smallX[i1 - 1];
					smallY[i1] = smallY[i1 - 1];
					aBooleanArray1553[i1] = aBooleanArray1553[i1 - 1];
				}

				smallX[0] = i;
				smallY[0] = j;
				aBooleanArray1553[0] = false;
				return;
			}
		}
		smallXYIndex = 0;
		anInt1542 = 0;
		anInt1503 = 0;
		smallX[0] = i;
		smallY[0] = j;
		x = smallX[0] * 128 + anInt1540 * 64;
		z = smallY[0] * 128 + anInt1540 * 64;
	}

	public final void method446() {
		smallXYIndex = 0;
		anInt1542 = 0;
	}

	public final void updateHitData(int markType, int damage, int l, int icon) {
		for (int i1 = 0; i1 < 4; i1++) {
			if (hitsLoopCycle[i1] <= l) {
				hitIcon[i1] = icon;
				hitArray[i1] = damage * Client.hpModifier;
				if (Client.toggleNewHitMarks) {
					hitMarkTypes[i1] = markType;
					hitsLoopCycle[i1] = l + NEW_HIT_CYCLE;
				} else {
					if (markType == 0) {
						if (damage > 0)
							hitMarkTypes[i1] = 1;
						else if (damage <= 0)
							hitMarkTypes[i1] = 0;
					} else {
						hitMarkTypes[i1] = markType;
					}
					hitsLoopCycle[i1] = l + OLD_HIT_CYCLE;
				}
				return;
			}
		}
	}

	public final void moveInDir(boolean flag, int i) {
		int j = smallX[0];
		int k = smallY[0];
		if (i == 0) {
			j--;
			k++;
		}
		if (i == 1)
			k++;
		if (i == 2) {
			j++;
			k++;
		}
		if (i == 3)
			j--;
		if (i == 4)
			j++;
		if (i == 5) {
			j--;
			k--;
		}
		if (i == 6)
			k--;
		if (i == 7) {
			j++;
			k--;
		}
		if (animId != -1 && Animation.anims[animId].anInt364 == 1)
			animId = -1;
		if (smallXYIndex < 9)
			smallXYIndex++;
		for (int l = smallXYIndex; l > 0; l--) {
			smallX[l] = smallX[l - 1];
			smallY[l] = smallY[l - 1];
			aBooleanArray1553[l] = aBooleanArray1553[l - 1];
		}
		smallX[0] = j;
		smallY[0] = k;
		aBooleanArray1553[0] = flag;
	}

	public int entScreenX;
	public int entScreenY;
	public int index = -1;

	public boolean isVisible() {
		return false;
	}

	public void setInteractingEntity(int index) {
		if (index == 65535) {
			index = -1;
		}

		this.interactingEntity = index;
	}

	public Entity() {
		smallX = new int[10];
		smallY = new int[10];
		interactingEntity = -1;
		anInt1504 = 32;
		anInt1505 = -1;
		height = 200;
		anInt1511 = -1;
		anInt1512 = -1;
		hitArray = new int[4];
		hitMarkTypes = new int[4];
		hitsLoopCycle = new int[4];
		hitIcon = new int[4];
		baseAnimId = -1;
		spotAnimId = -1;
		animId = -1;
		loopCycleStatus = -1000;
		textCycle = 100;
		anInt1540 = 1;
		aBoolean1541 = false;
		aBooleanArray1553 = new boolean[10];
		anInt1554 = -1;
		anInt1555 = -1;
		anInt1556 = -1;
		anInt1557 = -1;
	}

	public final int[] smallX;
	public final int[] smallY;
	public int interactingEntity;
	public int anInt1503;
	public int anInt1504;
	public int anInt1505;
	public String textSpoken;
	public int height;
	public int turnDirection;
	public int anInt1511;
	public int anInt1512;
	public int anInt1513;
	public final int[] hitArray;
	public final int[] hitMarkTypes;
	public final int[] hitsLoopCycle;
	public final int[] hitIcon;
	public int baseAnimId;
	public int baseAnimFrame;
	public int baseAnimNextFrame = -1;
	public int baseAnimFrameDelay;
	public int spotAnimId;
	public int spotAnimFrame;
	public int spotAnimNextFrame = -1;
	public int spotAnimFrameDelay;
	public int spotAnimDelay;
	public int anInt1524;
	public int smallXYIndex;
	public int runTiles = 2;
	public int animId;
	public int animFrame;
	public int animNextFrame = -1;
	public int animFrameDelay;
	public int animDelay;
	public int animCyclesElapsed;
	public int anInt1531;
	public int loopCycleStatus;
	public int currentHealth;
	public int maxHealth;
	public int textCycle;
	public int anInt1537;
	public int anInt1538;
	public int anInt1539;
	public int anInt1540;
	public boolean aBoolean1541;
	public int anInt1542;
	public int anInt1543;
	public int anInt1544;
	public int anInt1545;
	public int anInt1546;
	public int anInt1547;
	public int anInt1548;
	public int anInt1549;
	public int x;
	public int z;
	public int anInt1552;
	public final boolean[] aBooleanArray1553;
	public int anInt1554;
	public int anInt1555;
	public int anInt1556;
	public int anInt1557;
	public int startFill;
	public int stayCycleAfterFill;
	public int fill;
	public int maxFill;
	public int totalBarCycle;
	public int fillColor1;
	public int fillColor2;
	public boolean reverse;

}
