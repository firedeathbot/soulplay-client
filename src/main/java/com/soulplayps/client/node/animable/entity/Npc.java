package com.soulplayps.client.node.animable.entity;

import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.unknown.Class36;
import com.soulplayps.client.unpack.SpotAnim;
import com.soulplayps.client.node.animable.model.Model;

public final class Npc extends Entity {

	public boolean isPet = false;
	public int scale = 128;
	public String displayName = null;

	public String getName() {
		if (displayName != null) {
			return displayName;
		}

		return desc.name;
	}

	public boolean renderPriority() {
		if (isPet) {
			return false;
		}

		return desc.renderPriority;
	}

	private Model method450() {
		if (super.animId >= 0 && super.animDelay == 0) {
			int k = Animation.anims[super.animId].frame2Ids[super.animFrame];
			int i1 = -1;
			if (super.baseAnimId >= 0 && super.baseAnimId != super.anInt1511) {
				i1 = Animation.anims[super.baseAnimId].frame2Ids[super.baseAnimFrame];
			}

			Model model = desc.method164(i1, k,
					Animation.anims[super.animId].flowControl, isPet, scale);
			if (model != null) {
				super.animated = model.animated;
			}
			return model;
		}
		int l = -1;
		if (super.baseAnimId >= 0) {
			l = Animation.anims[super.baseAnimId].frame2Ids[super.baseAnimFrame];
		}

		Model model = desc.method164(-1, l, null, isPet, scale);

		if (model != null) {
			super.animated = model.animated;
		}

		return model;
	}

	@Override
	public void method443(int i, float j, float k, float l, float i1, int j1, int k1,
			int l1, long i2, boolean animated, int level, long gluid) {
		this.animated = false;
		if (desc == null) {
			return;
		}

		Model model = method450();
		if (model == null) {
			return;
		}

		Model spotAnimModel = null;
		super.height = model.modelHeight;
		if (super.spotAnimId != -1 && super.spotAnimFrame != -1) {
			SpotAnim spotAnim = SpotAnim.cache[super.spotAnimId];
			spotAnimModel = spotAnim.getModel(super.spotAnimFrame, super.spotAnimNextFrame, super.spotAnimFrameDelay);

			if(spotAnim.animation != null && Class36.animationlist[spotAnim.animation.frame2Ids[0] >> 16].length == 0) {
				spotAnimModel = null;
			}

		}

		model.aBoolean1659 = desc.aByte68 == 1;
		modelHeight = model.modelHeight;
		model.method443(i, j, k, l, i1, j1, k1, l1, i2, this.animated, level, gluid);
		if (spotAnimModel != null) {
			if (super.anInt1524 != 0) {
				spotAnimModel.translate(0, -super.anInt1524, 0);
			}

			if (isPet && scale != 128) {
				spotAnimModel.method478(scale, scale, scale);
			}

			spotAnimModel.method466();
			spotAnimModel.method443(i, j, k, l, i1, j1, k1, l1, i2, this.animated, level, gluid + 1);
		}
	}

	@Override
	public boolean isVisible() {
		return desc != null;
	}

	public Npc() {
	}

	public EntityDef desc;
}
