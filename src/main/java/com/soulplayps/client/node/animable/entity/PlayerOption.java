package com.soulplayps.client.node.animable.entity;

public class PlayerOption {

	public static final short[] OPTION_IDS = {561, 779, 27, 577, 729, 730, 731};
	public static final int OPTION_COUNT = 7;

	private final String option;
	private final boolean lowPrio;
	private final boolean attackOption;

	public PlayerOption(String option, boolean lowPrio) {
		this.option = option;
		this.lowPrio = lowPrio;
		this.attackOption = option.equalsIgnoreCase("attack");
	}

	public String getOption() {
		return option;
	}

	public boolean isLowPrio() {
		return lowPrio;
	}

	public boolean isAttackOption() {
		return attackOption;
	}

}
