package com.soulplayps.client.node.animable.item;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.node.ObjectCache;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.animable.model.ModelTypeEnum;
import com.soulplayps.client.node.raster.Raster;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.fs.RSArchive;

public final class ItemDef {
	
	@Override
	public String toString() {
		return "ItemDef [womanwearxoff=" + womanwearxoff + ", womanwearyoff=" + womanwearyoff + ", womanwearzoff="
				+ womanwearzoff + ", value=" + cost + ", recolorOriginal=" + Arrays.toString(recolorOriginal)
				+ ", id=" + id + ", recolorModified=" + Arrays.toString(recolorModified) + ", membersObject="
				+ members + ", anInt162=" + womanWear3 + ", certTemplateID=" + certTemplateID + ", womanWear2="
				+ womanWear2 + ", manWear=" + manWear + ", anInt166=" + manhead2 + ", anInt167=" + scaleX
				+ ", options=" + Arrays.toString(options) + ", xof2d=" + xof2d + ", name="
				+ name + ", anInt173=" + womanhead2 + ", modelId=" + modelId + ", anInt175=" + manhead + ", stackable="
				+ stackable + ", description=" + getDescription() + ", certID=" + noteId + ", zoom2d="
				+ zoom2d + ", anInt184=" + contrast + ", anInt185=" + manWear3 + ", manWear2=" + manWear2
				+ ", inventoryOptions=" + Arrays.toString(inventoryOptions) + ", xan2d=" + xan2d + ", anInt191="
				+ scaleZ + ", anInt192=" + scaleY + ", stackIDs=" + Arrays.toString(stackIDs) + ", yof2d="
				+ yof2d + ", anInt196=" + ambient + ", anInt197=" + womanhead + ", yan2d="
				+ yan2d + ", womanWear=" + womanWear + ", stackAmounts=" + Arrays.toString(stackAmounts)
				+ ", team=" + team + ", zan2d=" + zan2d + ", manwearxoff=" + manwearxoff + ", manwearyoff="
				+ manwearyoff + ", manwearzoff=" + manwearzoff + ", lendID=" + lendID + ", lentItemID=" + lentItemID
				+ "]";
	}
	
	private String getDescription() {
		if (description == null) {
			return "null desc";
		}
		return description.toString();
	}

	public boolean dialogueHeadModel(boolean j) {
		int k = manhead;
		int l = manhead2;
		if (j) {
			k = womanhead;
			l = womanhead2;
		}

		if (k == -1)
			return true;
		boolean flag = true;
		if (!Model.method463(k, modelType))
			flag = false;
		if (l != -1 && !Model.method463(l, modelType))
			flag = false;
		return flag;
	}

	public static void unpackConfig(RSArchive archive) {
		stream = new RSBuffer(archive.readFile("obj.dat"));
		RSBuffer stream = new RSBuffer(archive.readFile("obj.idx"));
		totalItems = stream.readUnsignedWord();
		streamIndices = new int[totalItems];
		System.out.println("Loaded 667 Items Amount: " + totalItems);
		int i = 2;
		for (int j = 0; j < totalItems; j++) {
			streamIndices[j] = i;
			i += stream.readUnsignedWord();
		}
		//System.out.println("gold:"+forID(995).toString());
		
		
		streamOsrs = new RSBuffer(archive.readFile("osrsobj.dat")); //171
		RSBuffer idx = new RSBuffer(archive.readFile("osrsobj.idx")); //171
		totalOsrsItems = idx.readUnsignedWord();
		streamIndicesOsrs = new int[totalOsrsItems];
		System.out.println("Loaded 182 Items Amount: " + totalOsrsItems);
		i = 2;
		for (int j = 0; j < totalOsrsItems; j++) {
			streamIndicesOsrs[j] = i;
			i += idx.readUnsignedWord();
		}

		int[] allowedItems = { 121905, 121932, 121934, 121936, 121938, 121940, 121942, 121944, 121946, 121948, 121950, 121930, 122103, 121918, 121921, 121902, 124268, 119724, 120008, 120050, 120056, 120062, 120065, 120110, 120146, 120149, 120152, 120155, 120158, 120161, 122246, 122326, 122327, 122328, 123242, 123276, 123279, 123282, 123342, 123258, 123261, 123264, 122367, 123348, 124422, 124517, 124511, 124514, 122477, 122804, 111889, 122966, 122978, 122323, 122324, 122975, 124419, 124420, 124421, 124417, 121634, 113233, 123351, 123997, 121733, 113235, 113237, 113239, 120724, 111998, 124144, 122954, 119997, 112802, 120716, 120718, 122951, 113439, 113441, 113431, 113432, 111883, 119669, 123037, 123047, 123050, 123053, 123056, 123059, 122731, 30018 };
		for (int id : allowedItems) {
			availableOsrsItems.add(id);
		}
	}

	public Model method194(boolean j) {
		int k = manhead;
		int l = manhead2;
		if (j) {
			k = womanhead;
			l = womanhead2;
		}

		if (k == -1)
			return null;
		Model model = Model.loadDropModel(k, modelType);
		if (l != -1) {
			Model model_1 = Model.loadDropModel(l, modelType);
			Model aclass30_sub2_sub4_sub6s[] = {model, model_1};
			model = new Model(2, aclass30_sub2_sub4_sub6s);
		}
		if (recolorOriginal != null) {
			for (int i1 = 0; i1 < recolorOriginal.length; i1++)
				model.replaceHs1(recolorOriginal[i1],
						recolorModified[i1]);
			
		}
		return model;
	}
	
	public boolean method195(boolean j) {
		int k = manWear;
		int l = manWear2;
		int i1 = manWear3;
		if (j) {
			k = womanWear;
			l = womanWear2;
			i1 = womanWear3;
		}
		if (k == -1)
			return true;
		boolean flag = true;
		if (!Model.method463(k, modelType))
			flag = false;
		if (l != -1 && !Model.method463(l, modelType))
			flag = false;
		if (i1 != -1 && !Model.method463(i1, modelType))
			flag = false;
		return flag;
	}
	
	public Model getEquippedModel(boolean gender, CustomizedItem customizedItem) {
		int j = manWear;
		int k = manWear2;
		int l = manWear3;
		if (gender) {
			j = womanWear;
			k = womanWear2;
			l = womanWear3;
		}
		if (j == -1)
			return null;
		Model model = Model.loadDropModel(j, modelType);
		if (model == null)
			return null;
		if (k != -1)
			if (l != -1) {
				Model model_1 = Model.loadDropModel(k, modelType);
				Model model_3 = Model.loadDropModel(l, modelType);
				Model aclass30_sub2_sub4_sub6_1s[] = {model, model_1, model_3};
				model = new Model(3, aclass30_sub2_sub4_sub6_1s);
			} else {
				Model model_2 = Model.loadDropModel(k, modelType);
				Model aclass30_sub2_sub4_sub6s[] = {model, model_2};
				model = new Model(2, aclass30_sub2_sub4_sub6s);
			}

		if (gender) {
			if (womanwearxoff != 0 || womanwearyoff != 0 || womanwearzoff != 0) {
				model.translate(womanwearxoff, womanwearyoff, womanwearzoff);
			}
		} else {
			if (manwearxoff != 0 || manwearyoff != 0 || manwearzoff != 0) {
				model.translate(manwearxoff, manwearyoff, manwearzoff);
			}
		}

		if (recolorOriginal != null) {
			int[] newModelColors;
			if (customizedItem != null && customizedItem.id == id && customizedItem.newModelColors != null) {
				newModelColors = customizedItem.newModelColors;
			} else {
				newModelColors = this.recolorModified;
			}
			for (int i1 = 0; i1 < recolorOriginal.length; i1++)
				model.replaceHs1(recolorOriginal[i1],
						newModelColors[i1]/*originalModelColors[i1]*/);
			
		}
		return model;
	}
	
	public void printNoneDefaults() {
		if (!name.equals("null")) {
			System.out.println("name = \""+name + "\";");
		}
		if (description != null) {
			System.out.println("description = \""+ new String(description) + "\".getBytes();");
		}
		if (modelId != 0) {
			System.out.println("modelId = "+modelId + ";");
		}
		if (recolorOriginal != null) {
			System.out.println("modifiedModelColors = new int[] "+ Arrays.toString(recolorOriginal).replace("[", "{").replace("]", "}") + ";");
		}
		if (recolorModified != null) {
			System.out.println("originalModelColors = new int[] "+ Arrays.toString(recolorModified).replace("[", "{").replace("]", "}") + ";");
		}
		if (zoom2d != 2000) {
			System.out.println("zoom2d = "+zoom2d + ";");
		}
		if (xan2d != 0) {
			System.out.println("xan2d = "+xan2d + ";");
		}
		if (yan2d != 0) {
			System.out.println("yan2d = "+yan2d + ";");
		}
		if (zan2d != 0) {
			System.out.println("zan2d = "+zan2d + ";");
		}
		if (xof2d != 0) {
			System.out.println("xof2d = "+xof2d + ";");
		}
		if (yof2d != 0) {
			System.out.println("yof2d = "+yof2d + ";");
		}
		if (stackable != false) {
			System.out.println("stackable = "+stackable + ";");
		}
		if (cost != 1) {
			System.out.println("value = "+cost + ";");
		}
		if (members != false) {
			System.out.println("membersObject = "+members + ";");
		}
		if (options != null) {
			System.out.println("options = new String[] "+Arrays.toString(options).replace("[", "{").replace("]", "}").replaceAll("Take", "\"Take\"") + ";");
		}
		if (inventoryOptions != null) {
			System.out.println("inventoryOptions = new String[] " + Arrays.toString(inventoryOptions).replace("[", "{").replace("]", "}").replaceAll("Drop", "\"Drop\"").replaceAll("Wield", "\"Wield\"") + ";");
		}
		if (manWear != -1) {
			System.out.println("manWear = "+manWear + ";");
		}
		if (manWear2 != -1) {
			System.out.println("manWear2 = "+manWear2 + ";");
		}
		if (manwearxoff != 0) {
			System.out.println("manwearxoff = "+manwearxoff + ";");
		}
		if (manwearyoff != 0) {
			System.out.println("manwearyoff = "+manwearyoff + ";");
		}
		if (manwearzoff != 0) {
			System.out.println("manwearzoff = "+manwearzoff + ";");
		}
		if (womanWear != -1) {
			System.out.println("womanWear = "+womanWear + ";");
		}
		if (womanWear2 != -1) {
			System.out.println("womanWear2 = "+womanWear2 + ";");
		}
		if (womanwearxoff != 0) {
			System.out.println("womanwearxoff = "+womanwearxoff + ";");
		}
		if (womanwearyoff != 0) {
			System.out.println("womanwearyoff = "+womanwearyoff + ";");
		}
		if (womanwearzoff != 0) {
			System.out.println("womanwearzoff = "+womanwearzoff + ";");
		}
		if (manWear3 != -1) {
			System.out.println("anInt185 = "+manWear3 + ";");
		}
		if (womanWear3 != -1) {
			System.out.println("anInt162 = "+womanWear3 + ";");
		}
		if (manhead != -1) {
			System.out.println("anInt175 = "+manhead + ";");
		}
		if (manhead2 != -1) {
			System.out.println("anInt166 = "+manhead2 + ";");
		}
		if (womanhead != -1) {
			System.out.println("anInt197 = "+womanhead + ";");
		}
		if (womanhead2 != -1) {
			System.out.println("anInt173 = "+womanhead2 + ";");
		}
		if (stackIDs != null) {
			System.out.println("stackIDs = new int[] "+Arrays.toString(stackIDs).replace("[", "{").replace("]", "}") + ";");
		}
		if (stackAmounts != null) {
			System.out.println("stackAmounts = new int[] "+Arrays.toString(stackAmounts).replace("[", "{").replace("]", "}") + ";");
		}
		if (noteId != -1) {
			System.out.println("certID = "+noteId + ";");
		}
		if (certTemplateID != -1) {
			System.out.println("certTemplateID = "+certTemplateID + ";");
		}
		if (scaleX != 128) {
			System.out.println("anInt167 = "+scaleX + ";");
		}
		if (scaleY != 128) {
			System.out.println("anInt192 = "+scaleY + ";");
		}
		if (scaleZ != 128) {
			System.out.println("anInt191 = "+scaleZ + ";");
		}
		if (ambient != 0) {
			System.out.println("anInt196 = "+ambient + ";");
		}
		if (contrast != 0) {
			System.out.println("anInt184 = "+contrast + ";");
		}
		if (team != 0) {
			System.out.println("team = "+team + ";");
		}
		if (lendID != -1) {
			System.out.println("lendID = "+lendID + ";");
		}
		if (lentItemID != -1) {
			System.out.println("lentItemID = "+lentItemID + ";");
		}
	}
	
	public static void clearCache() {
		recentUse.clear();
	}
	
	public void setDefaults() {
		modelId = 0;
		name = "null";
		description = null;
		recolorOriginal = null;
		recolorModified = null;
		zoom2d = 2000;
		xan2d = 0;
		yan2d = 0;
		zan2d = 0;
		xof2d = 0;
		yof2d = 0;
		stackable = false;
		cost = 1;
		members = false;
		options = null;
		inventoryOptions = null;
		manWear = -1;
		manWear2 = -1;
		manwearxoff = 0;
		manwearyoff = 0;
		manwearzoff = 0;
		womanWear = -1;
		womanWear2 = -1;
		womanwearxoff = 0;
		womanwearyoff = 0;
		womanwearzoff = 0;
		manWear3 = -1;
		womanWear3 = -1;
		manhead = -1;
		manhead2 = -1;
		womanhead = -1;
		womanhead2 = -1;
		stackIDs = null;
		stackAmounts = null;
		noteId = -1;
		certTemplateID = -1;
		scaleX = 128;
		scaleY = 128;
		scaleZ = 128;
		ambient = 0;
		contrast = 0;
		team = 0;
		lendID = -1;
		lentItemID = -1;
	}

	public static ItemDef forIDOsrs(int itemId) {
		ItemDef itemDef = (ItemDef) recentUse.get(itemId);
		if (itemDef != null) {
			return itemDef;
		}

		itemDef = new ItemDef();
		itemDef.id = itemId;
		itemDef.setDefaults();
		itemDef.options = new String[] { null, null, "Take", null, null };
		itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };

		int indexId = itemId - OSRS_OFFSET;
		if (indexId >= 0 && indexId < totalOsrsItems) {
			streamOsrs.currentOffset = streamIndicesOsrs[indexId];
			itemDef.readValues667(streamOsrs, true);
		}

		if (itemDef.certTemplateID != -1) {
			itemDef.toNote();
		}

		itemDef.modelType = ModelTypeEnum.OSRS;
		recentUse.put(itemDef, itemId);
		
		return itemDef;
	}

	public static ItemDef forIDOsrs(int itemId, int newId) {
		ItemDef itemDef = (ItemDef) recentUse.get(newId);
		if (itemDef != null) {
			return itemDef;
		}

		itemDef = new ItemDef();
		itemDef.id = newId;
		itemDef.setDefaults();
		if (itemId >= 0 && itemId < streamIndicesOsrs.length) {
			streamOsrs.currentOffset = streamIndicesOsrs[itemId];
			itemDef.readValues667(streamOsrs, true);
		}

		if (itemDef.certTemplateID != -1) {
			itemDef.toNote();
		}

		itemDef.modelType = ModelTypeEnum.OSRS;
		recentUse.put(itemDef, newId);
		
		return itemDef;
	}
	
	public static ItemDef forID(int i) {
//		if(i > totalItems2) {
//			System.out.println("com.soulplayps.client.Item id "+i+" out of bounds. Error 334");
//			return null;
//		}

		if (i >= OSRS_OFFSET) {
			return forIDOsrs(i);
		}

		ItemDef itemDef = ItemDefOsrs.loadOsrs(i);
		
		//NOTE: ADD OSRS ITEMS TO ItemDefOsrs switch!!!!!---------------------------------------------------
		
		if (itemDef != null) { //grabbed osrs items
			return itemDef;
		}
		
//		synchronized(recentUse) {
			itemDef = (ItemDef) recentUse.get(i);
//		}

		if (itemDef != null) {
			return itemDef;
		}

		itemDef = new ItemDef();
		itemDef.id = i;
		itemDef.setDefaults();
		if (i >= 0 && i < streamIndices.length) {
			stream.currentOffset = streamIndices[i];
			itemDef.readValues667(stream, false);
			
			//cheaphax 667 model offsets for weapons/shields...
			for (String string : itemDef.inventoryOptions) {
				if (string == null || string.length() == 0) {
					continue;
				}
						
				if (string.equalsIgnoreCase("Wield")) {
					itemDef.womanwearyoff = -10;
					itemDef.womanwearzoff = 10;
					itemDef.womanwearxoff = 1;
					break;
				}
			}
					
		}
		
		if (itemDef.certTemplateID != -1)
			itemDef.toNote();
		
//		synchronized(recentUse) {
			recentUse.put(itemDef, i);
//		}
		
		switch (i) {
		//NOTE: ADD OSRS ITEMS TO ItemDefOsrs switch!!!!!---------------------------------------------------
		case 10943:
			itemDef.name = "Instance Token";
			itemDef.description = "Used to enter private instances.".getBytes();
			itemDef.inventoryOptions = new String[]{"Read", "Redeem", null, null, "Drop"};
			break;
				
		case 9762: // magic cape
		case 9763: // magic cape (t)
			itemDef.inventoryOptions[2] = "Spellbook swap";
			return itemDef;
		
		case 229: // vial
			itemDef.inventoryOptions[3] = "Toggle Vial Break";
			return itemDef;
			
		case 19624:
			itemDef.name = "Voting Box";
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			return itemDef;
		
		case 4278: // ecto-token
			itemDef.name = "Pk Points Token";
			itemDef.inventoryOptions = new String[]{"Redeem", null, "Redeem 5", null, "Drop"};
			itemDef.description = "Redeem the token for 100 Pk Points.".getBytes();
			return itemDef;
			
		case 290:
			itemDef.name = "Wilderness package";
			itemDef.inventoryOptions = new String[] {"Open", null, null, null, "Drop"};
			return itemDef;
		case 8465: // wings
			itemDef.zoom2d = 1500;
//			itemDef.xan2d = 1454;
//			itemDef.yan2d = 997;
//			itemDef.yof2d = 8;
			itemDef.modelId = 65517;
			itemDef.manWear = 65520; // male wings
			itemDef.manWear2 = 3189; // male cape
			itemDef.womanWear = 65518; // female wings
			itemDef.womanWear2 = 62582; // female cape
			itemDef.name = "Wings";
			itemDef.inventoryOptions = new String[]{null, "Wear", "Activate Cosmetic", null, "Drop"};
			itemDef.description = "Wings.".getBytes();
			return itemDef;
			
		case 24144: //peahat
			itemDef.zoom2d = 464;
			itemDef.xan2d = 569;
			itemDef.yan2d = 238;
			itemDef.xof2d = -1;
			itemDef.yof2d = 34;
			itemDef.modelId = 68915;
			itemDef.manWear = 68913;
			itemDef.womanWear = 68914;
			itemDef.name = "Leanbow's hat";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.description = "The ultimate coding hat.".getBytes();
			return itemDef;
			
		case 24145: //eggsterminator
			itemDef.zoom2d = 1053;
			itemDef.yan2d = 1791;
			itemDef.yof2d = -1;
			itemDef.modelId = 68949;
			itemDef.manWear = 68966;
			itemDef.womanWear = 68967;
			itemDef.womanwearyoff -= 10;
			itemDef.womanwearxoff += 5;
			itemDef.womanwearzoff += 7;
			itemDef.name = "Eggsterminator";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			return itemDef;
			
		case 24149:
			itemDef.zoom2d = 592;
			itemDef.xan2d = 54;
			itemDef.yof2d = -1;
			itemDef.modelId = 68918;
			itemDef.manWear = 68962;
			itemDef.womanWear = 68963;
			itemDef.name = "Egg on face mask";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.description = "SoulPlay Easter event 2019".getBytes();
			return itemDef;
			
		case 24150:
			itemDef.zoom2d = 855;
			itemDef.xan2d = 54;
			itemDef.yan2d = 28;
			itemDef.xof2d = 4;
			itemDef.modelId = 68950;
			itemDef.manWear = 68961;
			itemDef.womanWear = 68964;
			itemDef.name = "Chocolate egg on face mask";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.description = "SoulPlay Easter event 2019".getBytes();
			return itemDef;
			
		case 24303:
			itemDef.zoom2d = 1184;
			itemDef.xan2d = 256;
			itemDef.yan2d = 1993;
			itemDef.xof2d = 1;
			itemDef.yof2d = -18;
			itemDef.modelId = 70131;
			itemDef.manWear = 70440;
			itemDef.womanWear = 70440;
			itemDef.name = "Coral crossbow";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			return itemDef;
			
		case 24304: //coral bolts
			itemDef.zoom2d = 1220;
			itemDef.xan2d = 216;
			itemDef.yan2d = 100;
			itemDef.xof2d = 6;
			itemDef.yof2d = -29;
			itemDef.modelId = 70145;
			itemDef.name = "Coral bolts";
			itemDef.stackable = true;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.stackIDs = new int[]{24305, 24306, 24307, 24308, 0, 0, 0, 0, 0, 0};
			itemDef.stackAmounts = new int[]{2, 3, 4, 5, 0, 0, 0, 0, 0, 0};
			return itemDef;
			
		case 24305: //coral bolts
			itemDef.zoom2d = 1220;
			itemDef.xan2d = 216;
			itemDef.yan2d = 100;
			itemDef.xof2d = 6;
			itemDef.yof2d = -29;
			itemDef.modelId = 70152;
			return itemDef;
			
		case 24306: //coral bolts
			itemDef.zoom2d = 1220;
			itemDef.xan2d = 216;
			itemDef.yan2d = 100;
			itemDef.xof2d = 6;
			itemDef.yof2d = -29;
			itemDef.modelId = 70141;
			return itemDef;
			
		case 24307: //coral bolts
			itemDef.zoom2d = 1220;
			itemDef.xan2d = 216;
			itemDef.yan2d = 100;
			itemDef.xof2d = 6;
			itemDef.yof2d = -29;
			itemDef.modelId = 70137;
			return itemDef;
			
		case 24308: //coral bolts
			itemDef.zoom2d = 1220;
			itemDef.xan2d = 216;
			itemDef.yan2d = 100;
			itemDef.xof2d = 6;
			itemDef.yof2d = -29;
			itemDef.modelId = 70138;
			return itemDef;
			
		case 24365: //dragon kiteshield
			itemDef.zoom2d = 1378;
			itemDef.xan2d = 264;
			itemDef.yan2d = 1913;
			itemDef.xof2d = 7;
			itemDef.yof2d = 58;
			itemDef.modelId = 70128;
			itemDef.manWear = 70672;
			itemDef.womanWear = 70672;
			itemDef.name = "Dragon kiteshield";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			return itemDef;
		
		case 22448: // polypore spore
			itemDef.zoom2d = 789;
			itemDef.xan2d = 241;
			itemDef.yan2d = 0;
			itemDef.xof2d = -5;
			itemDef.yof2d = 39;
			itemDef.modelId = 10865;
			itemDef.name = "Polypore spore";
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			return itemDef;
			
		case 22494: // polypore staff
			itemDef.zoom2d = 3750;
			itemDef.xan2d = 1454;
			itemDef.yan2d = 997;
			itemDef.yof2d = 8;
			itemDef.modelId = 13426;
			itemDef.manWear = 13417;
			itemDef.womanWear = 13417;
			itemDef.womanwearyoff -= 10;
			itemDef.womanwearxoff += 3;
			itemDef.womanwearzoff += 5;
			itemDef.name = "Polypore staff";
			itemDef.inventoryOptions = new String[]{null, "Wear", "Uncharge", null, "Drop"};
			return itemDef;
			
		case 22496: // polypore staff (deg)
			itemDef.zoom2d = 3750;
			itemDef.xan2d = 1454;
			itemDef.yan2d = 997;
			itemDef.yof2d = 8;
			itemDef.modelId = 13436;
			itemDef.manWear = 13416;
			itemDef.womanWear = 13416;
			itemDef.womanwearyoff -= 10;
			itemDef.womanwearxoff += 3;
			itemDef.womanwearzoff += 5;
			itemDef.name = "Polypore staff (uncharged)";
			itemDef.noteId = 22497;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			return itemDef;
			
		case 22497: // polypore staff (deg) note
			itemDef.noteId = 22496;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
			
		case 22498: // polypore stick
			itemDef.zoom2d = 2303;
			itemDef.xan2d = 1481;
			itemDef.xof2d = -1;
			itemDef.yof2d = 12;
			itemDef.modelId = 13435;
			itemDef.manWear = 13418;
			itemDef.womanWear = 13418;
			itemDef.womanwearyoff -= 10;
			itemDef.womanwearxoff += 3;
			itemDef.womanwearzoff += 5;
			itemDef.name = "Polypore stick";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			return itemDef;
			
		case 10008: // box trap
			itemDef.xan2d = 180;
			itemDef.yan2d = 747;
			itemDef.zan2d = 1535;
			itemDef.zoom2d = 2411;
			itemDef.xof2d = 15;
			itemDef.yof2d = -169;
			return itemDef;
		
			case 5943:
			case 5944:
			case 5945:
			case 5946:
			case 5947:
			case 5948:
			case 5949:
			case 5950:
				itemDef.name = itemDef.name.replace("Antipoison+", "Anti-venom");
				return itemDef;

			case 7509:
			case 7510:
				itemDef.inventoryOptions = new String[] {"Eat", null, "Guzzle", null, "Drop"};
				return itemDef;
				
			case 8871:
				itemDef.name = "Magical Crate";
				itemDef.inventoryOptions = new String[]{"Open", "Wield", null, null, "Drop"};
				return itemDef;

			case 13685:
			case 13686:
			case 13687:
				itemDef.name = "A Stylish Hat (Male)";
				return itemDef;

			case 13688:
			case 13689:
			case 13690:
				itemDef.name = "A Stylish Hat (Female)";
				return itemDef;

			case 13691:
			case 13692:
			case 13693:
				itemDef.name = "Shirt (Male)";
				return itemDef;

			case 13694:
			case 13695:
			case 13696:
				itemDef.name = "Shirt (Female)";
				return itemDef;

			case 13697:
			case 13698:
			case 13699:
				itemDef.name = "Leggings (Male)";
				return itemDef;

			case 13700:
			case 13701:
			case 13702:
				itemDef.name = "Skirt (Female)";
				return itemDef;
				
			case 21805:
				itemDef.name = "Wilderness key";
				itemDef.inventoryOptions = new String[]{"Info", null, null, null, "Drop"};
				return itemDef;
				
			case 30423:
				itemDef.name = "Gold Santa Hat";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.recolorOriginal = new int[1];
				itemDef.recolorModified = new int[1];
				itemDef.recolorOriginal[0] = 933;
				itemDef.recolorModified[0] = YELLOW;
				itemDef.modelId = 2537;
				itemDef.zoom2d = 540;
				itemDef.yan2d = 136;
				itemDef.xan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -3;
				itemDef.manWear = 189; // male wield model
				itemDef.womanWear = 366; // femArmModel
				itemDef.description = "Wow a santa hat".getBytes();
				return itemDef;
				
			case 30422:
				itemDef.name = "Cyan Partyhat";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.recolorOriginal = new int[1];
				itemDef.recolorModified = new int[1];
				itemDef.recolorOriginal[0] = 926;
				itemDef.recolorModified[0] = CYAN;
				itemDef.modelId = 2635;
				itemDef.zoom2d = 440;
				itemDef.xan2d = 76;
				itemDef.yan2d = 1850;
				itemDef.zan2d = 0;
				itemDef.xof2d = 1;
				itemDef.yof2d = 0;
				itemDef.manWear = 187;
				itemDef.womanWear = 363;
				itemDef.manWear2 = -1;
				itemDef.womanWear2 = -1;
				itemDef.manhead = -1;
				itemDef.womanhead = -1;
				itemDef.description = "A Cyan Partyhat, one of the most expensive item in the game".getBytes();
				return itemDef;
				
			case 30378: // reset wild kill count placeholder
				break;
				
			case 30377: // osrs boss pet placeholder
				return itemDef;
			case 30376: // osrs boss pet placeholder
				return itemDef;
			case 30375: // osrs boss pet placeholder
				return itemDef;
			case 30374: // osrs boss pet placeholder
				return itemDef;
			case 30373: // osrs boss pet placeholder
				return itemDef;
			case 30372: // osrs boss pet placeholder
				return itemDef;
			case 30371: // osrs boss pet placeholder
				return itemDef;
			case 30370: // vet'ion jr osrs placeholder
				return itemDef;
				
			case 30369:// vial of blood placeholder
				return itemDef;
				
			case 30368:
				itemDef.name = "Infernal cape";
				itemDef.zoom2d = 2086;
				itemDef.modelId = 33144;
				itemDef.xan2d = 567;
				itemDef.yan2d = 2031;
				itemDef.xof2d = -4;
				itemDef.manWear = 33103;
				itemDef.womanWear = 33111;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manwearyoff -= 2;
				itemDef.manwearzoff += 6;
				itemDef.womanwearzoff += 6;
				itemDef.womanwearyoff -= 10;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30367:
				itemDef.setDefaults();
				itemDef.name = "OSRS Coins";
				itemDef.zoom2d = 710;
				itemDef.xan2d = 184;
				itemDef.yan2d = 2012;
				itemDef.xof2d = 3;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2484;
				itemDef.stackable = true;
				itemDef.stackIDs = new int[]{30366, 30365, 30364, 30363, 30362, 30361, 30360, 30359, 30358, 0};
				itemDef.stackAmounts = new int[]{2, 3, 4, 5, 25, 100, 250, 1000, 10000, 0};
				itemDef.cost = 1;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30366:
				itemDef.zoom2d = 710;
				itemDef.xan2d = 184;
				itemDef.yan2d = 2012;
				itemDef.xof2d = 3;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2485;
				itemDef.stackable = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.excludeFromSearch = true;
				return itemDef;
				
			case 30365:
				itemDef.zoom2d = 710;
				itemDef.xan2d = 184;
				itemDef.yan2d = 2012;
				itemDef.xof2d = 3;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2486;
				itemDef.stackable = true;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30364:
				itemDef.zoom2d = 710;
				itemDef.xan2d = 184;
				itemDef.yan2d = 2012;
				itemDef.xof2d = 3;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2487;
				itemDef.stackable = true;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30363:
				itemDef.zoom2d = 710;
				itemDef.xan2d = 184;
				itemDef.yan2d = 2012;
				itemDef.xof2d = 3;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2488;
				itemDef.stackable = true;
				itemDef.cost = 1;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30362:
				itemDef.zoom2d = 710;
				itemDef.xan2d = 184;
				itemDef.yan2d = 2012;
				itemDef.xof2d = 3;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2667;
				itemDef.stackable = true;
				itemDef.cost = 1;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30361:
				itemDef.zoom2d = 710;
				itemDef.xan2d = 184;
				itemDef.yan2d = 2012;
				itemDef.xof2d = 3;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2825;
				itemDef.stackable = true;
				itemDef.cost = 1;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30360:
				itemDef.zoom2d = 650;
				itemDef.xan2d = 160;
				itemDef.yan2d = 2044;
				itemDef.xof2d = 2;
				itemDef.yof2d = -2;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2423;
				itemDef.stackable = true;
				itemDef.cost = 1;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30359:
				itemDef.zoom2d = 980;
				itemDef.xan2d = 172;
				itemDef.yan2d = 64;
				itemDef.xof2d = 11;
				itemDef.yof2d = 13;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2710;
				itemDef.stackable = true;
				itemDef.cost = 1;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30358:
				itemDef.zoom2d = 1000;
				itemDef.xan2d = 168;
				itemDef.yan2d = 80;
				itemDef.xof2d = 11;
				itemDef.inventoryOptions = new String[]{"Claim", null, null, null, "Drop"};
				itemDef.recolorOriginal = new int[]{8128};
				itemDef.recolorModified = new int[]{76};
				itemDef.modelId = 2775;
				itemDef.stackable = true;
				itemDef.cost = 1;
				itemDef.excludeFromSearch = true;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30357:
				itemDef.name = "Orange Partyhat";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.recolorOriginal = new int[1];
				itemDef.recolorModified = new int[1];
				itemDef.recolorOriginal[0] = 926;
				itemDef.recolorModified[0] = 3016;
				itemDef.modelId = 2635;
				itemDef.zoom2d = 440;
				itemDef.xan2d = 76;
				itemDef.yan2d = 1850;
				itemDef.zan2d = 0;
				itemDef.xof2d = 1;
				itemDef.yof2d = 0;
				itemDef.manWear = 187;
				itemDef.womanWear = 363;
				itemDef.manWear2 = -1;
				itemDef.womanWear2 = -1;
				itemDef.manhead = -1;
				itemDef.womanhead = -1;
				itemDef.description = "A Orange Partyhat, one of the most expensive item in the game".getBytes();
				return itemDef;
				
			case 30356:
				itemDef.setDefaults();
				itemDef.name = "Black chinchompa";
				itemDef.description = "A black chinchompa.".getBytes();
				itemDef.stackable = true;
				itemDef.zoom2d = 1000;
				itemDef.xan2d = 284;
				itemDef.yan2d = 1800;
				itemDef.modelId = 19962;
				itemDef.manWear = 19841;
				itemDef.womanWear = 19841;
				itemDef.xof2d = 1;
				itemDef.yof2d = 27;
				itemDef.recolorOriginal = new int[] { 5169, 7343, 7335, 7339, 7343, 5165 };
				itemDef.recolorModified = new int[] { 20, 33, 12, 37, 45, 49 };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Release" };
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30355:
				itemDef.setDefaults();
				itemDef.name = "Black chinchompa";
				itemDef.description = "A black chinchompa.".getBytes();
				itemDef.stackable = true;
				itemDef.zoom2d = 1000;
				itemDef.xan2d = 284;
				itemDef.yan2d = 1800;
				itemDef.modelId = 19962;
				itemDef.manWear = 19841;
				itemDef.womanWear = 19841;
				itemDef.xof2d = 1;
				itemDef.yof2d = 27;
				itemDef.recolorOriginal = new int[] { 5169, 7343, 7335, 7339, 7343, 5165 };
				itemDef.recolorModified = new int[] { 20, 33, 12, 37, 45, 49 };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Release" };
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
						
			case 30354:
				itemDef.setDefaults();
				itemDef.name = "Golden Scythe";
				itemDef.description = "A golden scythe.".getBytes();
				itemDef.zoom2d = 1930;
				itemDef.xan2d = 336;
				itemDef.xof2d = 20;
				itemDef.modelId = 2511;
				itemDef.manWear = 40287;
				itemDef.womanWear = 40287;
				itemDef.xof2d = 1;
				itemDef.yof2d = 17;
				itemDef.recolorOriginal = new int[]{61};
				itemDef.recolorModified = new int[]{8128};
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				return itemDef;
				
			case 30353:
				itemDef.setDefaults();
				itemDef.name = "Golden h'ween mask";
				itemDef.description = "Aaaarrrghhh, I'm an old-school monster!".getBytes();
				itemDef.zoom2d = 730;
				itemDef.xan2d = 516;
				itemDef.xof2d = -10;
				itemDef.manWear = 3188;
				itemDef.womanWear = 3192;
				itemDef.modelId = 2438;
				itemDef.xof2d = 0;
				itemDef.yof2d = -10;
				itemDef.recolorOriginal = new int[]{926, 0};
				itemDef.recolorModified = new int[]{8128, 0};
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -7;
				itemDef.manwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30352:
				itemDef.setDefaults();
				itemDef.name = "White h'ween mask";
				itemDef.description = "Aaaarrrghhh, I'm an old-school monster!".getBytes();
				itemDef.zoom2d = 730;
				itemDef.xan2d = 516;
				itemDef.xof2d = -10;
				itemDef.manWear = 3188;
				itemDef.womanWear = 3192;
				itemDef.modelId = 2438;
				itemDef.xof2d = 0;
				itemDef.yof2d = -10;
				itemDef.recolorOriginal = new int[]{926, 0};
				itemDef.recolorModified = new int[]{127, 0};
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -7;
				itemDef.manwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30351:
				itemDef.setDefaults();
				itemDef.name = "Pink h'ween mask";
				itemDef.description = "Aaaarrrghhh, I'm an cute old-school monster!".getBytes();
				itemDef.zoom2d = 730;
				itemDef.xan2d = 516;
				itemDef.xof2d = -10;
				itemDef.manWear = 3188;
				itemDef.womanWear = 3192;
				itemDef.modelId = 2438;
				itemDef.xof2d = 0;
				itemDef.yof2d = -10;
				itemDef.recolorOriginal = new int[]{926, 0};
				itemDef.recolorModified = new int[]{123770, 0};
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -7;
				itemDef.manwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30350:
				itemDef.setDefaults();
				itemDef.name = "Uncharged toxic trident";
				itemDef.zoom2d = 760;
				itemDef.xan2d = 552;
				itemDef.yan2d = 28;
				itemDef.xof2d = 2;
				itemDef.modelId = 2429;
				itemDef.certTemplateID = 799;
				itemDef.noteId = 30349;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30349:
				itemDef.setDefaults();
				itemDef.name = "Uncharged toxic trident";
				itemDef.zoom2d = 2421;
				itemDef.yan2d = 1818;
				itemDef.xan2d = 1549;
				itemDef.xof2d = 0;
				itemDef.yof2d = 9;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, "Wield", "Check", "Dismantle", null};
				itemDef.modelId = 19226;
				itemDef.manWear = 14401;
				itemDef.womanWear = 14401;
				itemDef.manwearyoff = 5;
				itemDef.manwearxoff = -5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearxoff = -2;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30348:
				itemDef.setDefaults();
				itemDef.name = "Trident of the swamp";
				itemDef.zoom2d = 2421;
				itemDef.yan2d = 1818;
				itemDef.xan2d = 1549;
				itemDef.xof2d = 0;
				itemDef.yof2d = 9;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, "Wield", "Check", "Uncharge", "Drop"};
				itemDef.modelId = 19223;//60100; //19223;
				itemDef.manWear = 14400;//60101; //14400;
				itemDef.womanWear = 14400;//60101; //14400;
				itemDef.manwearyoff = 5;
				itemDef.manwearxoff = -5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearxoff = -2;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30347:
				itemDef.name = "Giant present";
				itemDef.zoom2d = 1424;
				itemDef.xan2d = 167;
				itemDef.yan2d = 1763;
				itemDef.yof2d = -12;
				itemDef.xof2d = 4;
				itemDef.manWear = 32380;
				itemDef.womanWear = 32380; 
				itemDef.modelId = 32601; 
				itemDef.recolorOriginal = new int[] {61};
				itemDef.recolorModified = new int[] {-29403};
				itemDef.inventoryOptions = new String[]{null, "Wear", "Open", null, "Drop"};
				itemDef.womanwearyoff = -4;
				itemDef.manwearyoff = -1;
				itemDef.manwearxoff = 7;
				itemDef.manwearzoff = 5;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30346:
				itemDef.name = "Antisanta boots";
				itemDef.zoom2d = 653;
				itemDef.xan2d = 54;
				itemDef.yan2d = 1966;
				itemDef.yof2d = 0;
				itemDef.xof2d = 0;
				itemDef.manWear = 28988;
				itemDef.womanWear = 29006; 
				itemDef.modelId = 29021; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30345:
				itemDef.name = "Antisanta gloves";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 512;
				itemDef.yan2d = 121;
				itemDef.zan2d = 1953;
				itemDef.yof2d = 0;
				itemDef.xof2d = -1;
				itemDef.manWear = 28977;
				itemDef.womanWear = 28999; 
				itemDef.modelId = 29039; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30344:
				itemDef.name = "Antisanta pantaloons";
				itemDef.zoom2d = 1663;
				itemDef.xan2d = 512;
				itemDef.yan2d = 1939;
				itemDef.zan2d = 1899;
				itemDef.yof2d = 0;
				itemDef.xof2d = 0;
				itemDef.manWear = 28980;
				itemDef.womanWear = 29000; 
				itemDef.modelId = 29023; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30343:
				itemDef.name = "Antisanta jacket";
				itemDef.zoom2d = 1537;
				itemDef.xan2d = 512;
				itemDef.yan2d = 0;
				itemDef.yof2d = 5;
				itemDef.xof2d = 0;
				itemDef.manWear = 28984;
				itemDef.manWear2 = 28982;
				itemDef.womanWear = 29004; 
				itemDef.womanWear2 = 29003; 
				itemDef.modelId = 29025; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30342:
				itemDef.name = "Antisanta mask";
				itemDef.zoom2d = 653;
				itemDef.xan2d = 512;
				itemDef.yan2d = 0;
				itemDef.zan2d = 1966;
				itemDef.yof2d = 0;
				itemDef.xof2d = 0;
				itemDef.manWear = 28975; 
				itemDef.manhead = 29015;
				itemDef.womanWear = 28996; 
				itemDef.womanhead = 29018;
				itemDef.modelId = 29030;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30341:
				itemDef.name = "Santa boots";
				itemDef.zoom2d = 653;
				itemDef.xan2d = 54;
				itemDef.yan2d = 1966;
				itemDef.yof2d = 0;
				itemDef.xof2d = 0;
				itemDef.manWear = 28989;
				itemDef.womanWear = 29007; 
				itemDef.modelId = 29026; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30340:
				itemDef.name = "Santa gloves";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 512;
				itemDef.yan2d = 1832;
				itemDef.zan2d = 1953;
				itemDef.yof2d = -1;
				itemDef.xof2d = 1;
				itemDef.manWear = 28978;
				itemDef.womanWear = 28998; 
				itemDef.modelId = 29031; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30339:
				itemDef.name = "Santa pantaloons";
				itemDef.zoom2d = 1663;
				itemDef.xan2d = 512;
				itemDef.yan2d = 1939;
				itemDef.zan2d = 1899;
				itemDef.yof2d = 0;
				itemDef.xof2d = 0;
				itemDef.manWear = 28979;
				itemDef.womanWear = 29001; 
				itemDef.modelId = 29028; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30338:
				itemDef.name = "Santa jacket";
				itemDef.zoom2d = 1663;
				itemDef.xan2d = 512;
				itemDef.yan2d = 0;
				itemDef.yof2d = 3;
				itemDef.xof2d = 0;
				itemDef.manWear = 28983;
				itemDef.manWear2 = 28981;
				itemDef.womanWear = 29005; 
				itemDef.womanWear2 = 29002;
				itemDef.modelId = 29022; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30337:
				itemDef.name = "Santa mask";
				itemDef.zoom2d = 725;
				itemDef.xan2d = 512;
				itemDef.yan2d = 0;
				itemDef.zan2d = 1966;
				itemDef.yof2d = 1;
				itemDef.xof2d = 1;
				itemDef.manWear = 28976; 
				itemDef.manhead = 29016;
				itemDef.womanWear = 28997; 
				itemDef.womanhead = 29017;
				itemDef.modelId = 29027; //31545;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30336:
				itemDef.name = "Dragonfire ward";
				itemDef.zoom2d = 1424;
				itemDef.xan2d = 283;
				itemDef.yan2d = 81;
				itemDef.zan2d = 27;
				itemDef.yof2d = -4;
				itemDef.xof2d = 7;
				itemDef.manWear = 34648; 
				itemDef.womanWear = 34648; 
				itemDef.modelId = 35106;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wield", "Inspect", "Empty", "Drop"};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30335:
				itemDef.name = "Skeletal visage";
				itemDef.modelId = 35104;
				itemDef.zoom2d = 1697;
				itemDef.xan2d = 567;
				itemDef.yan2d = 152;
				itemDef.zan2d = 2047;
				itemDef.xof2d = -5;
				itemDef.yof2d = -5;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.members = true;
				itemDef.noteId = 30334;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30334:
				itemDef.noteId = 30333;
				itemDef.certTemplateID = 799;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.toNote();
				return itemDef;
				
			case 30333:
				itemDef.name = "Superior dragon bones";
				itemDef.modelId = 2768;
				itemDef.zoom2d = 2232;
				itemDef.xan2d = 498;
				itemDef.yan2d = 1145;
				itemDef.xof2d = -16;
				itemDef.yof2d = 4;
				itemDef.recolorOriginal = new int[] {127};
				itemDef.recolorModified = new int[] {31192};
				itemDef.inventoryOptions = new String[]{"Bury", null, null, null, "Drop"};
				itemDef.members = true;
				itemDef.noteId = 30334;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30332:
				itemDef.name = "Dragonbone necklace";
				itemDef.zoom2d = 716;
				itemDef.xan2d = 417;
				itemDef.yan2d = 81;
				itemDef.zan2d = 27;
				itemDef.yof2d = 20;
				itemDef.xof2d = 1;
				itemDef.manWear = 34627; 
				itemDef.womanWear = 34627; 
				itemDef.modelId = 35102; 
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.manwearzoff = -5;
				itemDef.womanwearzoff = -2;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30331:
				itemDef.name = "Turquoise slayer helmet (i)";
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manWear = 34626; //31549;
				itemDef.manhead = 35090;
				itemDef.womanWear = 34638; //31552;
				itemDef.womanhead = 35090;
				itemDef.modelId = 34619; //31545;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", "Disassemble", null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30330:
				itemDef.name = "Turquoise slayer helmet";
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manWear = 34626; //31549;
				itemDef.manhead = 35090;
				itemDef.womanWear = 34638; //31552;
				itemDef.womanhead = 35090;
				itemDef.modelId = 34619; //31545;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", "Disassemble", null};
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;

			case 30328:
				itemDef.name = "Scythe of vitur";
				itemDef.zoom2d = 2105;
				itemDef.xan2d = 327;
				itemDef.yan2d = 23;
				itemDef.xof2d = 1;
				itemDef.yof2d = 17;
				itemDef.inventoryOptions = new String[]{null, "Wield", "Check", "Uncharge", "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 35742;
				itemDef.manWear = 35371; //male
				itemDef.womanWear = 35371; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30327:
				itemDef.name = "Scythe of vitur (uncharged)";
				itemDef.zoom2d = 2105;
				itemDef.xan2d = 327;
				itemDef.yan2d = 23;
				itemDef.xof2d = 1;
				itemDef.yof2d = 17;
				itemDef.inventoryOptions = new String[]{null, "Wield", "Charge", null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 35748;
				itemDef.manWear = 35373; //male
				itemDef.womanWear = 35373; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30326:
				itemDef.name = "Hunting knife";
				itemDef.zoom2d = 656;
				itemDef.xan2d = 575;
				itemDef.yan2d = 1534;
				itemDef.xof2d = 4;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32353;
				itemDef.manWear = 32332; //male
				itemDef.womanWear = 32332; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30325:
				itemDef.name = "Banshee robe";
				itemDef.modelId = 32355;
				itemDef.zoom2d = 1982;
				itemDef.xan2d = 611;
				itemDef.yan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = 3;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.recolorOriginal = new int[] {-22440, 70, 5037};
				itemDef.recolorModified = new int[] {-29257, -29137, 10392};
				itemDef.manWear = 32325; //male
				itemDef.womanWear = 32329; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30324:
				itemDef.name = "Banshee top";
				itemDef.modelId = 32354;
				itemDef.zoom2d = 1414;
				itemDef.xan2d = 680;
				itemDef.yan2d = 18;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 32327; //male
				itemDef.manWear2 = 32326;  //male
				itemDef.womanWear = 32331; // female
				itemDef.womanWear2 = 32330; //female
			    itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;
				
			case 30323:
				itemDef.name = "Banshee mask";
				itemDef.modelId = 32352;
				itemDef.zoom2d = 717;
				itemDef.xan2d = 365;
				itemDef.yan2d = 197;
				itemDef.xof2d = 0;
				itemDef.yof2d = 3;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 32324; //male
				itemDef.manhead = 32351;
				itemDef.womanWear = 32328; // female
				itemDef.womanhead = 32351;
				itemDef.manwearyoff = -6;
				itemDef.manwearzoff = 3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
				
			case 30322:
				itemDef.name = "Soulplay bond";
				itemDef.modelId = 29210;
				itemDef.zoom2d = 2300;
				itemDef.xan2d = 512;
				itemDef.yan2d = 0;
				itemDef.xof2d = 3;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Redeem", null, null, "Drop"};
				itemDef.members = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30321:
				itemDef.name = "Prospector boots";
				itemDef.modelId = 28471;
				itemDef.zoom2d = 968;
				itemDef.xan2d = 242;
				itemDef.yan2d = 202;
				itemDef.xof2d = 3;
				itemDef.yof2d = -100;
				itemDef.members = true;				
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.recolorOriginal = new int[] {4626, 8101};
				itemDef.recolorModified = new int[] {5272, 5157};
				itemDef.manWear = 28458; //male
				itemDef.womanWear = 28458; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
			    
			case 30320:
				itemDef.name = "Prospector legs";
				itemDef.modelId = 28470;
				itemDef.zoom2d = 1697;
				itemDef.xan2d = 498;
				itemDef.yan2d = 180;
				itemDef.zan2d = 2047;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.recolorOriginal = new int[] {-22440, 70, 5037};
				itemDef.recolorModified = new int[] {-29257, -29137, 10392};
				itemDef.manWear = 28450; //male
				itemDef.womanWear = 28459; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
					
			case 30319:
				itemDef.name = "Prospector jacket";
				itemDef.modelId = 28474;
				itemDef.zoom2d = 1474;
				itemDef.xan2d = 404;
				itemDef.yan2d = 94;
				itemDef.xof2d = 7;
				itemDef.yof2d = 13;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.recolorOriginal = new int[] {-22440, 70, 5037, 5027, 5400};
				itemDef.recolorModified = new int[] {-29257, -29137, 10392, -29143, 10392};
				itemDef.manWear = 28457; //male
				itemDef.manWear2 = 28454;  //male
				itemDef.womanWear = 28461; // female
				itemDef.womanWear2 = 28460; //female
			    itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;
					
			case 30318:
				itemDef.name = "Prospector helmet";
				itemDef.modelId = 28473;
				itemDef.zoom2d = 905;
				itemDef.xan2d = 189;
				itemDef.yan2d = 1885;
				itemDef.xof2d = -8;
				itemDef.yof2d = -25;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 28453; //male
				itemDef.manhead = 28476;
				itemDef.womanWear = 28453; // female
				itemDef.womanhead = 28897;
				itemDef.manwearyoff = -6;
				itemDef.manwearzoff = 3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30317:
				itemDef.name = "Tanzanite helm";
				itemDef.modelId = 29213;
				itemDef.zoom2d = 700;
				itemDef.xan2d = 215;
				itemDef.yan2d = 1724;
				itemDef.xof2d = 0;
				itemDef.yof2d = -17;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", null, "Uncharge"};
				itemDef.manhead = 29195;
				itemDef.manWear = 14421;
				itemDef.womanhead = 29199;
				itemDef.womanWear = 23994;
				itemDef.members = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
			    
			case 30316:
				itemDef.name = "Tanzanite helm (uncharged)";
				itemDef.modelId = 29216;
				itemDef.zoom2d = 700;
				itemDef.xan2d = 0;
				itemDef.yan2d = 1724;
				itemDef.xof2d = -1;
				itemDef.yof2d = -1;
				//itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manhead = 29197;
				itemDef.manWear = 14423;
				itemDef.womanhead = 29202;
				itemDef.womanWear = 14425;
				itemDef.members = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30315:
				itemDef.name = "Magma helm";
				itemDef.modelId = 29205;
				itemDef.zoom2d = 700;
				itemDef.xan2d = 215;
				itemDef.yan2d = 1724;
				itemDef.xof2d = 0;
				itemDef.yof2d = -17;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", null, "Uncharge"};
				itemDef.manhead = 29196;
				itemDef.manWear = 14424;
				itemDef.womanhead = 29200;
				itemDef.womanWear = 14426;
				itemDef.members = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
			    
			case 30314:
				itemDef.name = "Magma helm (uncharged)";
				itemDef.modelId = 29217;
				itemDef.zoom2d = 700;
				itemDef.xan2d = 0;
				itemDef.yan2d = 1724;
				itemDef.xof2d = -1;
				itemDef.yof2d = -1;
				//itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manhead = 29198;
				itemDef.manWear = 14422;
				itemDef.womanhead = 29201;
				itemDef.womanWear = 28436;
				itemDef.members = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30313:
				itemDef.name = "Magma mutagen";
				itemDef.modelId = 2705;
				itemDef.zoom2d = 905;
				itemDef.xan2d = 189;
				itemDef.yan2d = 215;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.recolorOriginal = new int[] {61};
				itemDef.recolorModified = new int[] {3008};
				itemDef.members = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30312:
				itemDef.name = "Tanzanite mutagen";
				itemDef.modelId = 2705;
				itemDef.zoom2d = 905;
				itemDef.xan2d = 189;
				itemDef.yan2d = 215;
				itemDef.recolorOriginal = new int[] {61};
				itemDef.recolorModified = new int[] {29659};
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.members = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30311:
				itemDef.name = "Angler boots";
				itemDef.modelId = 29418;
				itemDef.zoom2d = 905;
				itemDef.xan2d = 202;
				itemDef.yan2d = 283;
				itemDef.xof2d = 4;
				itemDef.yof2d = -4;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 29409; //male
				itemDef.womanWear = 29414; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30310:
				itemDef.name = "Angler waders";
				itemDef.modelId = 29420;
				itemDef.zoom2d = 2042;
				itemDef.xan2d = 1522;
				itemDef.yan2d = 1024;
				itemDef.zan2d = 1024;
				itemDef.xof2d = 0;
				itemDef.yof2d = -1;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 29113; //male
				itemDef.womanWear = 29410; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30309:
				itemDef.name = "Angler top";
				itemDef.modelId = 29419;
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 579;
				itemDef.yan2d = 2047;
				itemDef.xof2d = 0;
				itemDef.yof2d = 5;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 29118; //male
				itemDef.manWear2 = 29116;  //male
				itemDef.womanWear = 29413; // female
				itemDef.womanWear2 = 29412; //female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30308:
				itemDef.name = "Angler hat";
				itemDef.modelId = 29417;
				itemDef.zoom2d = 842;
				itemDef.xan2d = 2047;
				itemDef.yan2d = 1751;
				itemDef.zan2d = 189;
				itemDef.xof2d = 1;
				itemDef.yof2d = -4;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 29114; //male
				itemDef.manhead = 29415;
				itemDef.womanWear = 29411; // female
				itemDef.womanhead = 29416; 
				itemDef.manwearyoff = -6;
				itemDef.manwearzoff = 3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30307:
				itemDef.name = "Warm gloves";
				itemDef.modelId = 32297;
				itemDef.zoom2d = 789;
				itemDef.xan2d = 609;
				itemDef.yan2d = 111;
				itemDef.xof2d = 1;
				itemDef.yof2d = 0;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 32212; //male
				itemDef.womanWear = 32220; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
			    
			case 30306:
				itemDef.name = "Pyromancer boots";
				itemDef.modelId = 32291;
				itemDef.zoom2d = 770;
				itemDef.xan2d = 164;
				itemDef.yan2d = 156;
				itemDef.xof2d = 3;
				itemDef.yof2d = -3;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 32216; //male
				itemDef.womanWear = 32224; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
			    
			case 30305:
				itemDef.name = "Pyromancer hood";
				itemDef.modelId = 32293;
				itemDef.zoom2d = 789;
				itemDef.xan2d = 24;
				itemDef.yan2d = 2047;
				itemDef.zan2d = 2047;
				itemDef.xof2d = 1;
				itemDef.yof2d = 1;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 32211; //male
				itemDef.manhead = 32287;
				itemDef.womanWear = 32218; // female
				itemDef.womanhead = 32287; 
				itemDef.manwearyoff = -5;
				itemDef.manwearzoff = 5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 6;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30304:
				itemDef.name = "Pyromancer robe";
				itemDef.modelId = 32295;
				itemDef.zoom2d = 1897;
				itemDef.xan2d = 512;
				itemDef.yan2d = 14;
				itemDef.zan2d = 69;
				itemDef.xof2d = 1;
				itemDef.yof2d = 7;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 32213; //male
				itemDef.womanWear = 32221; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30303:
				itemDef.name = "Pyromancer garb";
				itemDef.modelId = 32292;
				itemDef.zoom2d = 1375;
				itemDef.xan2d = 553;
				itemDef.yan2d = 0;
				itemDef.xof2d = 1;
				itemDef.yof2d = -1;
				itemDef.members = true;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.manWear = 32215; //male
				itemDef.manWear2 = 32214;  //male
				itemDef.womanWear = 32223; // female
				itemDef.womanWear2 = 32222; //female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
			    break;
				
			case 30302:
				itemDef.name = "Black skin color";
				itemDef.description = "Your skin will become black after drinking this.".getBytes();
				itemDef.modelId = 54183;
				itemDef.recolorOriginal = new int[] {33872, 33877, 33882, 33887};
				itemDef.recolorModified = new int[] {0, 0, 0, 0};
				itemDef.zoom2d = 658;
				itemDef.xan2d = 144;
				itemDef.yan2d = 1264;
				itemDef.xof2d = 8;
				itemDef.yof2d = -4;
				itemDef.cost = 5;
				itemDef.members = true;
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
			    break;
				
			case 30301:
				itemDef.name = "Purple skin color";
				itemDef.description = "Your skin will become purple after drinking this.".getBytes();
				itemDef.modelId = 54183;
				itemDef.recolorOriginal = new int[] {33872, 33877, 33882, 33887};
				itemDef.recolorModified = new int[] {47563, 47563, 47563, 47563};
				itemDef.zoom2d = 658;
				itemDef.xan2d = 144;
				itemDef.yan2d = 1264;
				itemDef.xof2d = 8;
				itemDef.yof2d = -4;
				itemDef.cost = 5;
				itemDef.members = true;
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
			    break;
				
			case 30300:
				itemDef.name = "Red skin color";
				itemDef.description = "Your skin will become red after drinking this.".getBytes();
				itemDef.modelId = 54183;
				itemDef.recolorOriginal = new int[] {33872, 33877, 33882, 33887};
				itemDef.recolorModified = new int[] {64959, 64959, 64959, 64959};
				itemDef.zoom2d = 658;
				itemDef.xan2d = 144;
				itemDef.yan2d = 1264;
				itemDef.xof2d = 8;
				itemDef.yof2d = -4;
				itemDef.cost = 5;
				itemDef.members = true;
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
			    break;
				
			case 30279:
				itemDef.name = "Elite task legs";
				itemDef.zoom2d = 1730;
				itemDef.xan2d = 470;
				itemDef.yan2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 29127; 
				itemDef.manWear = 28210; //male
				itemDef.womanWear = 29057; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30278:
				itemDef.name = "Hard task legs";
				itemDef.zoom2d = 1730;
				itemDef.xan2d = 470;
				itemDef.yan2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 29138; 
				itemDef.manWear = 28211; //male
				itemDef.womanWear = 29059; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30277:
				itemDef.name = "Medium task legs";
				itemDef.zoom2d = 1730;
				itemDef.xan2d = 470;
				itemDef.yan2d = 0;
				itemDef.zan2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 29155; 
				itemDef.manWear = 28209; //male
				itemDef.womanWear = 29058; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30276:
				itemDef.name = "Easy task legs";
				itemDef.zoom2d = 1730;
				itemDef.xan2d = 470;
				itemDef.yan2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 29152; 
				itemDef.manWear = 28208; //male
				itemDef.womanWear = 29060; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30275:
				itemDef.setDefaults();
				itemDef.name = "Elite task armour";
				itemDef.modelId = 29151;
				itemDef.zoom2d = 1540;
				itemDef.xan2d = 512;
				itemDef.yof2d = 1;
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28435; //male
				itemDef.manWear2 = 28214;  //male
				itemDef.womanWear = 29066; // female
				itemDef.womanWear2 = 29062; //female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30274:
				itemDef.setDefaults();
				itemDef.name = "Hard task armour";
				itemDef.modelId = 29147;
				itemDef.zoom2d = 1510;
				itemDef.xan2d = 512;
				itemDef.yof2d = 1;
				itemDef.xof2d = 1;
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28434; //male
				itemDef.manWear2 = 28213;  //male
				itemDef.womanWear = 29068; // female
				itemDef.womanWear2 = 29065; //female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30273:
				itemDef.setDefaults();
				itemDef.name = "Medium task armour";
				itemDef.modelId = 29124;
				itemDef.zoom2d = 1510;
				itemDef.xan2d = 512;
				itemDef.yof2d = 1;
				itemDef.xof2d = 1;
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 29010; //male
				itemDef.manWear2 = 28242;  //male
				itemDef.womanWear = 29069; // female
				itemDef.womanWear2 = 29064; //female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30272:
				itemDef.setDefaults();
				itemDef.name = "Easy task armour";
				itemDef.modelId = 29148;
				itemDef.zoom2d = 1221;
				itemDef.xan2d = 512;
				itemDef.yof2d = 7;
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28317; //male
				itemDef.manWear2 = 28287;  //male
				itemDef.womanWear = 29067; // female
				itemDef.womanWear2 = 29063; //female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
						
			case 30271:
				itemDef.name = "Elite task headgear";
				itemDef.zoom2d = 716;
				itemDef.xan2d = 54;
				itemDef.yan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -5;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.modelId = 29129;
				itemDef.manWear = 27610;
				itemDef.womanWear = 29042;
				itemDef.manwearyoff = -4;
				itemDef.manwearzoff = 4;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
						
			case 30270:
				itemDef.name = "Hard task headgear";
				itemDef.zoom2d = 590;
				itemDef.xan2d = 27;
				itemDef.yan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.modelId = 29159;
				itemDef.manWear = 10482;
				itemDef.womanWear = 29043;
				itemDef.manwearyoff = -4;
				itemDef.manwearzoff = 5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
						
			case 30269:
				itemDef.name = "Medium task headgear";
				itemDef.zoom2d = 550;
				itemDef.xan2d = 27;
				itemDef.yan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.modelId = 29150;
				itemDef.manWear = 25749;
				itemDef.womanWear = 29041;
				itemDef.manwearyoff = -4;
				itemDef.manwearzoff = 5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30268:
				itemDef.name = "Easy task headgear";
				itemDef.zoom2d = 500;
				itemDef.xan2d = 120;
				itemDef.yan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -7;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.modelId = 29161;
				itemDef.manWear = 28212;
				itemDef.womanWear = 29061;
				itemDef.manwearyoff = -4;
				itemDef.manwearzoff = 5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
							
				case 30267:
					itemDef.name = "Bounty teleport scroll";
					itemDef.zoom2d = 960;
					itemDef.xan2d = 512;
					itemDef.yan2d = 512;
					itemDef.xof2d = 0;
					itemDef.yof2d = 3;
					itemDef.recolorModified = new int[] {6464, 6587, 6604, 6608, 6583};
					itemDef.recolorOriginal = new int[] {-9016, -9020, -9024, -9012, -9022};
					itemDef.inventoryOptions = new String[]{"Read", null, null, null, "Drop"};
					itemDef.options = new String[] {null, null, "Take", null, null};
					itemDef.modelId = 10137;
					itemDef.modelType = ModelTypeEnum.OSRS;
					return itemDef;
				
			case 30266:
				itemDef.name = "Elder chaos hood";
				itemDef.zoom2d = 760;
				itemDef.xan2d = 346;
				itemDef.yan2d = 55;
				itemDef.zan2d = 1757;
				itemDef.xof2d = -4;
				itemDef.yof2d = -7;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32074; 
				itemDef.manWear = 28285; //male
				itemDef.womanWear = 32073; // female
				itemDef.manwearyoff = -3;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30265:
				return itemDef;
				
			case 30264:
				return itemDef;

			case 30262://done
				itemDef.cost = 1;
				itemDef.modelId = 19213;
				itemDef.xan2d = 512;
				itemDef.yan2d = 40;
				itemDef.zoom2d = 1537;
				itemDef.xof2d = 2;
				itemDef.yof2d = 0;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;

			case 30261://done
				itemDef.cost = 1;
				itemDef.modelId = 19210;
				itemDef.xan2d = 512;
				itemDef.yan2d = 202;
				itemDef.zoom2d = 1347;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;

			case 30260://done
				itemDef.cost = 1;
				itemDef.modelId = 19211;
				itemDef.xan2d = 512;
				itemDef.yan2d = 121;
				itemDef.zoom2d = 1230;
				itemDef.xof2d = -3;
				itemDef.yof2d = 1;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;

			case 30259://done
				itemDef.cost = 1;
				itemDef.modelId = 14416;
				itemDef.xan2d = 512;
				itemDef.yan2d = 121;
				itemDef.zoom2d = 1230;
				itemDef.xof2d = -1;
				itemDef.yof2d = -1;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;

			case 30005://done
				itemDef.name = "Zulrah's scales";
				itemDef.zoom2d = 1370;
				itemDef.xan2d = 212;
				itemDef.yan2d = 148;
				itemDef.xof2d = 7;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.stackIDs = new int[] { 30259, 30260, 30261, 30262, 0, 0, 0, 0, 0, 0  };
				itemDef.stackAmounts = new int[]{ 2, 3, 4, 5, 0, 0, 0, 0, 0, 0 };
				itemDef.modelId = 19212;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.stackable = true;
				return itemDef;

			case 30258:
				itemDef.name = "ring of suffering (ri)";
				itemDef.zoom2d = 830;
				itemDef.xan2d = 370;
				itemDef.yan2d = 1910;
				itemDef.xof2d = 3;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31519;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30257:
				itemDef.name = "Nunchaku";
				itemDef.zoom2d = 1108;
				itemDef.xan2d = 290;
				itemDef.yan2d = 906;
				itemDef.xof2d = -3;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32005;
				itemDef.manWear = 31906; //male
				itemDef.womanWear = 31906; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30256:
				itemDef.name = "Cyclops head";
				itemDef.zoom2d = 653;
				itemDef.xan2d = 539;
				itemDef.yan2d = 1;
				itemDef.xof2d = -3;
				itemDef.yof2d = -3;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31974;
				itemDef.manWear = 31745; //male
				itemDef.womanWear = 31832; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30255:
				itemDef.name = "Zombie head";
				itemDef.zoom2d = 779;
				itemDef.xan2d = 1885;
				itemDef.yan2d = 2047;
				itemDef.xof2d = 9;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32008;
				itemDef.manWear = 31912; //male
				itemDef.womanWear = 31910; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30254:
				itemDef.name = "Explorer backpack";
				itemDef.zoom2d = 1040;
				itemDef.xan2d = 454;
				itemDef.yan2d = 1283;
				itemDef.xof2d = -4;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 28637;
				itemDef.manWear = 28485; //male
				itemDef.womanWear = 28485; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30253:
				itemDef.name = "Wooden shield (g)";
				itemDef.zoom2d = 1390;
				itemDef.xan2d = 328;
				itemDef.yan2d = 32;
				itemDef.xof2d = 3;
				itemDef.yof2d = 40;
				itemDef.recolorModified = new int[]{5784, 7114};  // 772
				itemDef.recolorOriginal = new int[]{61, 7054}; // 774
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 2601;
				itemDef.manWear = 517; //male
				itemDef.womanWear = 517; // female
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30252:
				itemDef.name = "Team cape i";
				itemDef.zoom2d = 2130;
				itemDef.xan2d = 364;
				itemDef.yan2d = 992;
				itemDef.xof2d = 3;
				itemDef.yof2d = 12;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32011;
				itemDef.manWear = 31770; //male
				itemDef.womanWear = 31770; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30251:
				itemDef.name = "Team cape x";
				itemDef.zoom2d = 2130;
				itemDef.xan2d = 364;
				itemDef.yan2d = 992;
				itemDef.xof2d = 3;
				itemDef.yof2d = 12;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31988;
				itemDef.manWear = 31773; //male
				itemDef.womanWear = 31773; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30250:
				itemDef.name = "Team cape zero";
				itemDef.zoom2d = 2130;
				itemDef.xan2d = 364;
				itemDef.yan2d = 992;
				itemDef.xof2d = 3;
				itemDef.yof2d = 12;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31971;
				itemDef.manWear = 31772; //male
				itemDef.womanWear = 31772; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30249:
				itemDef.name = "Imp mask";
				itemDef.zoom2d = 587;
				itemDef.xan2d = 566;
				itemDef.yan2d = 0;
				itemDef.xof2d = -1;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 28666;
				itemDef.manWear = 18556; //male
				itemDef.womanWear = 28553; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30248:
				itemDef.name = "Imp mask";
				itemDef.zoom2d = 779;
				itemDef.xan2d = 593;
				itemDef.yan2d = 13;
				itemDef.xof2d = 0;
				itemDef.yof2d = 17;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 28741;
				itemDef.manWear = 18558; //male
				itemDef.womanWear = 28555; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30247:
				itemDef.name = "Large spade";
				itemDef.zoom2d = 659;
				itemDef.xan2d = 498;
				itemDef.yan2d = 484;
				itemDef.xof2d = -1;
				itemDef.yof2d = -1;
				itemDef.inventoryOptions = new String[]{null, "Wield", "Dig", null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 2418;
				itemDef.manWear = 13938; //male
				itemDef.womanWear = 13938; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30246:
				itemDef.name = "Torn clue scroll (part 3)";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 360;
				itemDef.yan2d = 175;
				itemDef.xof2d = 3;
				itemDef.yof2d = 4;
				itemDef.inventoryOptions = new String[]{"Combine", "Inspect", null, null, "Destroy"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31956;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30245:
				itemDef.name = "Torn clue scroll (part 2)";
				itemDef.zoom2d = 1010;
				itemDef.xan2d = 360;
				itemDef.yan2d = 672;
				itemDef.xof2d = 1;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{"Combine", "Inspect", null, null, "Destroy"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31959;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30244:
				itemDef.name = "Torn clue scroll (part 1)";
				itemDef.zoom2d = 716;
				itemDef.xan2d = 360;
				itemDef.yan2d = 672;
				itemDef.xof2d = 5;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{"Combine", "Inspect", null, null, "Destroy"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31958;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30243:
				itemDef.name = "Light box";
				itemDef.zoom2d = 2180;
				itemDef.xan2d = 520;
				itemDef.yan2d = 0;
				itemDef.xof2d = -1;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Detroy"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31708;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30242:
				itemDef.name = "Strange device";
				itemDef.zoom2d = 968;
				itemDef.xan2d = 283;
				itemDef.yan2d = 1535;
				itemDef.xof2d = 1;
				itemDef.yof2d = 8;
				itemDef.inventoryOptions = new String[]{"Feel", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31914;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30241:
				itemDef.name = "Revenant ether";
				itemDef.zoom2d = 560;
				itemDef.xan2d = 471;
				itemDef.yan2d = 687;
				itemDef.xof2d = 8;
				itemDef.yof2d = -7;
				itemDef.modelId = 15347;//34497;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.excludeFromSearch = true;
				return itemDef;
				
			case 30240:
				itemDef.name = "Revenant ether";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 350;
				itemDef.yan2d = 795;
				itemDef.xof2d = 4;
				itemDef.yof2d = -14;
				itemDef.modelId = 15346;//34498;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.excludeFromSearch = true;
				return itemDef;
				
			case 30239:
				itemDef.name = "Revenant ether";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 337;
				itemDef.yan2d = 754;
				itemDef.xof2d = 7;
				itemDef.yof2d = -22;
				itemDef.modelId = 15345;//34500;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.excludeFromSearch = true;
				return itemDef;
				
			case 30238:
				itemDef.name = "Revenant ether";
				itemDef.zoom2d = 530;
				itemDef.xan2d = 364;
				itemDef.yan2d = 96;
				itemDef.xof2d = 2;
				itemDef.yof2d = 19;
				itemDef.modelId = 15344;//34496;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.excludeFromSearch = true;
				return itemDef;
			
			case 30237:
				itemDef.name = "Revenant ether";
				itemDef.zoom2d = 550;
				itemDef.xan2d = 202;
				itemDef.yan2d = 1764;
				itemDef.zan2d = 108;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.stackable = true;
				itemDef.stackIDs = new int[]{30238, 30239, 30240, 30241, 0, 0, 0, 0, 0, 0};
				itemDef.stackAmounts = new int[]{2, 3, 4, 5, 0, 0, 0, 0, 0, 0};
				itemDef.modelId = 15343;//34499;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30236:
				itemDef.name = "Bracelet of ethereum (uncharged)";
				itemDef.zoom2d = 659;
				itemDef.xan2d = 184;
				itemDef.yan2d = 1909;
				itemDef.xof2d = 0;
				itemDef.yof2d = -7;
				itemDef.recolorModified = new int[]{30400, 30668, 30400};  // 772
				itemDef.recolorOriginal = new int[]{-22464, 9152, 8119}; // 774
				itemDef.inventoryOptions = new String[]{null, "Wear", "Toggle-absorption", "Dismantle", "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 24566;
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30235:
				itemDef.name = "Bracelet of ethereum";
				itemDef.zoom2d = 659;
				itemDef.xan2d = 184;
				itemDef.yan2d = 1909;
				itemDef.xof2d = 0;
				itemDef.yof2d = -7;
				itemDef.recolorModified = new int[]{12, 30668, 30400};  // 772
				itemDef.recolorOriginal = new int[]{-22464, 9152, 8119}; // 774
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", "Toggle-absorption", "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 24566;
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30234:
				itemDef.name = "Clue bottle (elite)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 0;
				itemDef.yan2d = 1093;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.recolorModified = new int[]{11212};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31044;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30233:
				itemDef.name = "Clue bottle (hard)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 0;
				itemDef.yan2d = 1093;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.recolorModified = new int[]{-21593};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31044;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30232:
				itemDef.name = "Clue bottle (medium)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 0;
				itemDef.yan2d = 1093;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.recolorModified = new int[]{918};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31044;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30231:
				itemDef.name = "Clue bottle (easy)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 0;
				itemDef.yan2d = 1093;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31044;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30230:
				itemDef.name = "Puzzle box (master)";
				itemDef.zoom2d = 900;
				itemDef.xan2d = 420;
				itemDef.yan2d = 80;
				itemDef.xof2d = 0;
				itemDef.yof2d = 4;
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Destroy"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31983;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30229:
				itemDef.name = "Puzzle box (master)";
				itemDef.zoom2d = 900;
				itemDef.xan2d = 420;
				itemDef.yan2d = 80;
				itemDef.xof2d = 0;
				itemDef.yof2d = 4;
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Destroy"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32032;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30228:
				itemDef.name = "Puzzle box (master)";
				itemDef.zoom2d = 900;
				itemDef.xan2d = 420;
				itemDef.yan2d = 80;
				itemDef.xof2d = 0;
				itemDef.yof2d = 4;
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Destroy"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32007;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 3569:
			case 3565:
			case 2798:
				itemDef.name = "Puzzle box (hard)";
				return itemDef;
				
			case 10279:
				itemDef.name = "Casket (easy)";
				return itemDef;
				
			case 13077:
				itemDef.name = "Casket (master)";
				return itemDef;
				
			case 10287:
				itemDef.name = "Clue scroll (easy)";
				return itemDef;
				
			case 19064:
				itemDef.name = "Clue scroll (master)";
				return itemDef;
			
			case 30227:
				itemDef.name = "Clue geode (elite)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 180;
				itemDef.yan2d = 763;
				itemDef.xof2d = 4;
				itemDef.yof2d = -38;
				itemDef.recolorModified = new int[]{11212};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31953;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30226:
				itemDef.name = "Clue geode (hard)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 180;
				itemDef.yan2d = 763;
				itemDef.xof2d = 4;
				itemDef.yof2d = -38;
				itemDef.recolorModified = new int[]{-21593};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31953;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30225:
				itemDef.name = "Clue geode (medium)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 180;
				itemDef.yan2d = 763;
				itemDef.xof2d = 4;
				itemDef.yof2d = -38;
				itemDef.recolorModified = new int[]{918};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31953;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30224:
				itemDef.name = "Clue geode (easy)";
				itemDef.zoom2d = 1242;
				itemDef.xan2d = 180;
				itemDef.yan2d = 763;
				itemDef.xof2d = 4;
				itemDef.yof2d = -38;
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31953;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30223:
				itemDef.name = "Clue nest (elite)";
				itemDef.zoom2d = 1114;
				itemDef.xan2d = 152;
				itemDef.yan2d = 1245;
				itemDef.xof2d = -3;
				itemDef.yof2d = 0;
				itemDef.recolorModified = new int[]{11212};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31957;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30222:
				itemDef.name = "Clue nest (hard)";
				itemDef.zoom2d = 1114;
				itemDef.xan2d = 152;
				itemDef.yan2d = 1245;
				itemDef.xof2d = -3;
				itemDef.yof2d = 0;
				itemDef.recolorModified = new int[]{-21593};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31957;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30221:
				itemDef.name = "Clue nest (medium)";
				itemDef.zoom2d = 1114;
				itemDef.xan2d = 152;
				itemDef.yan2d = 1245;
				itemDef.xof2d = -3;
				itemDef.yof2d = 0;
				itemDef.recolorModified = new int[]{918};  // 772
				itemDef.recolorOriginal = new int[]{22422}; // 774
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31957;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30220:
				itemDef.name = "Clue nest (easy)";
				itemDef.zoom2d = 1114;
				itemDef.xan2d = 152;
				itemDef.yan2d = 1245;
				itemDef.xof2d = -3;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31957;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30219:
				itemDef.name = "Dragon scimitar (or)";
				itemDef.zoom2d = 1640;
				itemDef.xan2d = 456;
				itemDef.yan2d = 12;
				itemDef.xof2d = 0;
				itemDef.yof2d = 6;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31993;
				itemDef.manWear = 31898; //male
				itemDef.womanWear = 31898; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30218:
				itemDef.name = "Dragon scimitar ornament kit";
				itemDef.zoom2d = 1600;
				itemDef.xan2d = 564;
				itemDef.yan2d = 1943;
				itemDef.xof2d = -12;
				itemDef.yof2d = 5;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32012;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30217:
				itemDef.name = "Dragon defender (t)";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 498;
				itemDef.yan2d = 256;
				itemDef.xof2d = 5;
				itemDef.yof2d = 8;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31990;
				itemDef.manWear = 31904; //male
				itemDef.womanWear = 31897; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30216:
				itemDef.name = "Dragon defender ornament kit";
				itemDef.zoom2d = 1600;
				itemDef.xan2d = 564;
				itemDef.yan2d = 1943;
				itemDef.xof2d = -12;
				itemDef.yof2d = 5;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32030;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30215:
				itemDef.name = "Arclight";
				itemDef.zoom2d = 980;
				itemDef.xan2d = 196;
				itemDef.yan2d = 864;
				itemDef.xof2d = -2;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31661;
				itemDef.manWear = 31652; //male
				itemDef.womanWear = 31651; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30214:
				itemDef.name = "Fancy tiara";
				itemDef.zoom2d = 500;
				itemDef.xan2d = 175;
				itemDef.yan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -7;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32009;
				itemDef.manWear = 31795; //male
				itemDef.womanWear = 31864; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30213:
				itemDef.name = "Half moon spectacles";
				itemDef.zoom2d = 463;
				itemDef.xan2d = 121;
				itemDef.yan2d = 2047;
				itemDef.xof2d = 0;
				itemDef.yof2d = 4;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31955;
				itemDef.manWear = 31791; //male
				itemDef.womanWear = 31861; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30212:
				itemDef.name = "Ale of the gods";
				itemDef.zoom2d = 653;
				itemDef.xan2d = 0;
				itemDef.yan2d = 1535;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32035;
				itemDef.manWear = 31899; //male
				itemDef.womanWear = 31902; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30211:
				itemDef.name = "Bowl wig";
				itemDef.zoom2d = 526;
				itemDef.xan2d = 54;
				itemDef.yan2d = 1993;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31986;
				itemDef.manWear = 31753; //male
				itemDef.womanWear = 31822; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30210:
				itemDef.name = "Bloodhound";
				itemDef.zoom2d = 1616;
				itemDef.xan2d = 3;
				itemDef.yan2d = 213;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31740;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30209:
				itemDef.name = "$500 Casket";
				itemDef.zoom2d = 1552;
				itemDef.xan2d = 104;
				itemDef.yan2d = 1973;
				itemDef.xof2d = 3;
				itemDef.yof2d = 3;
				itemDef.inventoryOptions = new String[]{null, "Wield", "Open", null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32028;
				itemDef.manWear = 31907; //male
				itemDef.womanWear = 31907; // female
				itemDef.womanwearyoff = -4;
				itemDef.manwearyoff = -16;
				itemDef.manwearxoff = 7;
				itemDef.manwearzoff = 5;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30208:
				itemDef.name = "Amulet of torture";
				itemDef.zoom2d = 620;
				itemDef.xan2d = 424;
				itemDef.yan2d = 68;
				itemDef.xof2d = 1;
				itemDef.yof2d = 16;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31524;
				itemDef.manWear = 31227; //male
				itemDef.womanWear = 31233; // female
				itemDef.manwearxoff = 0;
				itemDef.manwearyoff = -8;
				itemDef.manwearzoff = -2;
				itemDef.womanwearxoff = 0;
				itemDef.womanwearyoff = -11;
				itemDef.womanwearzoff = 2;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30207:
				itemDef.name = "Tormented bracelet";
				itemDef.zoom2d = 659;
				itemDef.xan2d = 184;
				itemDef.yan2d = 1909;
				itemDef.xof2d = 1;
				itemDef.yof2d = 4;
				itemDef.recolorModified = new int[]{-22008, -22008};  // 772
				itemDef.recolorOriginal = new int[]{8119, 9152}; // 774
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31515;
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30206:
				itemDef.name = "Necklace of anguish";
				itemDef.zoom2d = 1020;
				itemDef.xan2d = 280;
				itemDef.yan2d = 2020;
				itemDef.zan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -16;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31510;
				itemDef.manWear = 31228; //male
				itemDef.womanWear = 31232; // female
				itemDef.manwearxoff = 0;
				itemDef.manwearyoff = -8;
				itemDef.manwearzoff = -2;
				itemDef.womanwearxoff = 0;
				itemDef.womanwearyoff = -11;
				itemDef.womanwearzoff = 2;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30205:
				itemDef.name = "Ring of suffering (r)";
				itemDef.zoom2d = 830;
				itemDef.xan2d = 370;
				itemDef.yan2d = 1910;
				itemDef.xof2d = 3;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31519;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30204:
				itemDef.name = "Ring of suffering (i)";
				itemDef.zoom2d = 830;
				itemDef.xan2d = 370;
				itemDef.yan2d = 1910;
				itemDef.xof2d = 3;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31519;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30203:
				itemDef.name = "Ring of suffering";
				itemDef.zoom2d = 830;
				itemDef.xan2d = 370;
				itemDef.yan2d = 1910;
				itemDef.xof2d = 3;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31519;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30202:
				itemDef.name = "Zenyte amulet";
				itemDef.zoom2d = 700;
				itemDef.xan2d = 471;
				itemDef.yan2d = 1926;
				itemDef.xof2d = -5;
				itemDef.yof2d = 16;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31524;
				itemDef.manWear = 31227; //male
				itemDef.womanWear = 31233; // female
				itemDef.manwearzoff = -5;
				itemDef.womanwearzoff = -2;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30201:
				itemDef.name = "Zenyte ring";
				itemDef.zoom2d = 830;
				itemDef.xan2d = 370;
				itemDef.yan2d = 1910;
				itemDef.xof2d = 3;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31519;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30200:
				itemDef.name = "Zenyte necklace";
				itemDef.zoom2d = 1296;
				itemDef.xan2d = 323;
				itemDef.yan2d = 1818;
				itemDef.xof2d = 3;
				itemDef.yof2d = -1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31510;
				itemDef.manWear = 31228; //male
				itemDef.womanWear = 31232; // female
				itemDef.manwearzoff = -5;
				itemDef.womanwearzoff = -2;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30199:
				itemDef.name = "Zenyte bracelet";
				itemDef.zoom2d = 659;
				itemDef.xan2d = 346;
				itemDef.yan2d = 249;
				itemDef.xof2d = 1;
				itemDef.yof2d = 4;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31515;
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30198:
				itemDef.name = "Zenyte shard";
				itemDef.zoom2d = 636;
				itemDef.xan2d = 625;
				itemDef.yan2d = 1028;
				itemDef.xof2d = 0;
				itemDef.yof2d = 0;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31514;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30197:
				itemDef.name = "Zenyte amulet (u)";
				itemDef.zoom2d = 600;
				itemDef.xan2d = 332;
				itemDef.yan2d = 2020;
				itemDef.xof2d = 0;
				itemDef.yof2d = 11;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31513;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30196:
				itemDef.name = "Uncut zenyte";
				itemDef.zoom2d = 780;
				itemDef.xan2d = 368;
				itemDef.yan2d = 220;
				itemDef.xof2d = -5;
				itemDef.yof2d = 7;
				itemDef.recolorModified = new int[]{5056};  // 772
				itemDef.recolorOriginal = new int[]{61}; // 774
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 2528;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30195:
				itemDef.name = "Zenyte";
				itemDef.zoom2d = 780;
				itemDef.xan2d = 368;
				itemDef.yan2d = 220;
				itemDef.xof2d = -5;
				itemDef.yof2d = 7;
				itemDef.recolorModified = new int[]{5056, 3008, 5056};  // 772
				itemDef.recolorOriginal = new int[]{478, 931, 127}; // 774
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 2586;
				//itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30194:
				itemDef.name = "Ring of illusion";
				itemDef.zoom2d = 845;
				itemDef.xan2d = 370;
				itemDef.yan2d = 1910;
				itemDef.xof2d = 3;
				itemDef.yof2d = 1;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 2677;
				return itemDef;
			
			case 30193:
				itemDef.name = "Ring of nature";
				itemDef.zoom2d = 830;
				itemDef.xan2d = 322;
				itemDef.yan2d = 135;
				itemDef.xof2d = 4;
				itemDef.yof2d = -16;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32021;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30192:
				itemDef.name = "Ring of coins";
				itemDef.zoom2d = 830;
				itemDef.xan2d = 322;
				itemDef.yan2d = 135;
				itemDef.xof2d = 4;
				itemDef.yof2d = -16;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32004;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30191:
				itemDef.name = "Jungle demon mask";
				itemDef.zoom2d = 1040;
				itemDef.xan2d = 264;
				itemDef.yan2d = 1772;
				itemDef.xof2d = -3;
				itemDef.yof2d = -2;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31995;
				itemDef.manWear = 31744; //male
				itemDef.womanWear = 31827; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30190:
				itemDef.name = "Old demon mask";
				itemDef.zoom2d = 1104;
				itemDef.xan2d = 420;
				itemDef.yan2d = 174;
				itemDef.xof2d = 2;
				itemDef.yof2d = 8;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32003;
				itemDef.manWear = 31748; //male
				itemDef.womanWear = 31835; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30189:
				itemDef.name = "Black demon mask";
				itemDef.zoom2d = 1104;
				itemDef.xan2d = 144;
				itemDef.yan2d = 1826;
				itemDef.xof2d = 7;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31984;
				itemDef.manWear = 31746; //male
				itemDef.womanWear = 31824; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30188:
				itemDef.name = "Greater demon mask";
				itemDef.zoom2d = 902;
				itemDef.xan2d = 216;
				itemDef.yan2d = 138;
				itemDef.xof2d = -3;
				itemDef.yof2d = 16;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 32015;
				itemDef.manWear = 31749; //male
				itemDef.womanWear = 31829; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30187:
				itemDef.name = "Lesser demon mask";
				itemDef.zoom2d = 912;
				itemDef.xan2d = 246;
				itemDef.yan2d = 1859;
				itemDef.xof2d = 1;
				itemDef.yof2d = -58;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 31977;
				itemDef.manWear = 31755; //male
				itemDef.womanWear = 31838; // female
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30186:
				itemDef.name = "Ancient relic";
				itemDef.zoom2d = 1411;
				itemDef.xan2d = 0;
				itemDef.yan2d = 0;
				itemDef.xof2d = -5;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 5445;//change this to correct model when updated. 
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30185:
				itemDef.name = "Ancient effigy";
				itemDef.zoom2d = 1411;
				itemDef.xan2d = 0;
				itemDef.yan2d = 0;
				itemDef.xof2d = -5;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 3679;//change this to correct model when updated. 
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30184:
				itemDef.name = "Ancient medallion";
				itemDef.zoom2d = 1411;
				itemDef.xan2d = 0;
				itemDef.yan2d = 0;
				itemDef.xof2d = -5;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 5444;//change this to correct model when updated. 
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30183:
				itemDef.name = "Ancient statuette";
				itemDef.zoom2d = 1411;
				itemDef.xan2d = 0;
				itemDef.yan2d = 0;
				itemDef.xof2d = -5;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 5447;//change this to correct model when updated. 
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 30182:
				itemDef.name = "Ancient totem";
				itemDef.zoom2d = 1411;
				itemDef.xan2d = 0;
				itemDef.yan2d = 0;
				itemDef.xof2d = -5;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 3676;//change this to correct model when updated. 
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
			case 30181:
				itemDef.name = "Ancient emblem";
				itemDef.zoom2d = 1411;
				itemDef.xan2d = 0;
				itemDef.yan2d = 0;
				itemDef.xof2d = -5;
				itemDef.yof2d = -4;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.options = new String[] {null, null, "Take", null, null};
				itemDef.modelId = 3680;//change this to correct model when updated. 
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
				
		case 30180:
			itemDef.name = "White partyhat";
			itemDef.description = "A nice hat from a cracker.".getBytes();
			itemDef.modelId = 2635;
			itemDef.recolorOriginal = new int[] {926};
			itemDef.recolorModified = new int[] {127};
			itemDef.zoom2d = 440;
			itemDef.xan2d = 121;
			itemDef.yan2d = 1845;
			itemDef.xof2d = 1;
			itemDef.yof2d = 1;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 187;
			itemDef.womanWear = 363;
			itemDef.manhead = 39372;
			itemDef.womanhead = 38795;
			itemDef.noteId = 1049;
			itemDef.lendID = 13536;
			break;
			
		case 30179:
			itemDef.name = "Green skin color";
			itemDef.description = "Your skin will become blue after drinking this.".getBytes();
			itemDef.modelId = 54183;
			itemDef.recolorOriginal = new int[] {33872, 33877, 33882, 33887};
			itemDef.recolorModified = new int[] {350770, 350770, 350770, 350770};
			itemDef.zoom2d = 658;
			itemDef.xan2d = 144;
			itemDef.yan2d = 1264;
			itemDef.xof2d = 8;
			itemDef.yof2d = -4;
			itemDef.cost = 5;
			itemDef.members = true;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
		    break;
		   
		case 30178:
			itemDef.name = "Blue skin color";
			itemDef.description = "Your skin will become blue after drinking this.".getBytes();
			itemDef.modelId = 54183;
			itemDef.recolorOriginal = new int[] {33872, 33877, 33882, 33887};
			itemDef.recolorModified = new int[] {296770, 296770, 296770, 296770};
			itemDef.zoom2d = 658;
			itemDef.xan2d = 144;
			itemDef.yan2d = 1264;
			itemDef.xof2d = 8;
			itemDef.yof2d = -4;
			itemDef.cost = 5;
			itemDef.members = true;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
		    break;
		case 30177:
			itemDef.name = "White skin color";
			itemDef.description = "Your skin will become white after drinking this.".getBytes();
			itemDef.modelId = 54183;
			itemDef.recolorOriginal = new int[] {33872, 33877, 33882, 33887};
			itemDef.recolorModified = new int[] {127, 127, 127, 127};
			itemDef.zoom2d = 658;
			itemDef.xan2d = 144;
			itemDef.yan2d = 1264;
			itemDef.xof2d = 8;
			itemDef.yof2d = -4;
			itemDef.cost = 5;
			itemDef.members = true;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
		    break;
		case 30176:
			itemDef.name = "Ultra Mystery box";
			itemDef.description = "Oooh, I wonder what could be inside?".getBytes();
			itemDef.modelId = 2426;
			itemDef.recolorOriginal = new int[] { 22410 };
			itemDef.recolorModified = new int[] { 470770 };
			itemDef.zoom2d = 1100;
			itemDef.xan2d = 160;
			itemDef.yan2d = 172;
			itemDef.yof2d = -14;
			itemDef.options = new String[] { null, null, "Take", null, null };
			itemDef.inventoryOptions = new String[] { "Open", null, null, null, "Drop" };
		    break;
		case 30175:
			itemDef.name = "Legendary Mystery box";
			itemDef.description = "Oooh, I wonder what could be inside?".getBytes();
			itemDef.modelId = 2426;
			itemDef.recolorOriginal = new int[] { 22410 };
			itemDef.recolorModified = new int[] { 23421 };
			itemDef.zoom2d = 1100;
			itemDef.xan2d = 160;
			itemDef.yan2d = 172;
			itemDef.yof2d = -14;
			itemDef.options = new String[] { null, null, "Take", null, null };
			itemDef.inventoryOptions = new String[] { "Open", null, null, null, "Drop" };
		    break;
		case 30174:
			itemDef.name = "Super Mystery box";
			itemDef.description = "Oooh, I wonder what could be inside?".getBytes();
			itemDef.modelId = 2426;
			itemDef.recolorOriginal = new int[] { 22410 };
			itemDef.recolorModified = new int[] { 96993 };
			itemDef.zoom2d = 1100;
			itemDef.xan2d = 160;
			itemDef.yan2d = 172;
			itemDef.yof2d = -14;
			itemDef.options = new String[] { null, null, "Take", null, null };
			itemDef.inventoryOptions = new String[] { "Open", null, null, null, "Drop" };
		    break;

		case 6570:
			if (itemDef.inventoryOptions != null) {
				itemDef.inventoryOptions[4] = "Destroy";
			}
		    break;

		case 5733:
			itemDef.inventoryOptions = new String[]{"Eat", "Slice", "Peel", "Mush", "Drop"};
		break;
		
		case 30173:
			itemDef.name = "Mummy's feet";
			itemDef.zoom2d = 653;
			itemDef.xan2d = 131;
			itemDef.yan2d = 69;
			itemDef.xof2d = 3;
			itemDef.yof2d = -7;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31965; 
			itemDef.manWear = 31815; //male
			itemDef.womanWear = 31881; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30172:
			itemDef.name = "Mummy's legs";
			itemDef.zoom2d = 1720;
			itemDef.xan2d = 450;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 9;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32041; 
			itemDef.manWear = 31784; //male
			itemDef.womanWear = 31855; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30171:
			itemDef.name = "Mummy's hands";
			itemDef.zoom2d = 450;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 1;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32043; 
			itemDef.manWear = 31778; //male
			itemDef.womanWear = 31853; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30170:
			itemDef.name = "Mummy's body";
			itemDef.zoom2d = 1160;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32039; 
			itemDef.manWear = 31808; //male
			itemDef.manWear2 = 31803;  //male
			itemDef.womanWear = 31878; // female
			itemDef.womanWear2 = 31869; //female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30169:
			itemDef.name = "Mummy's head";
			itemDef.zoom2d = 530;
			itemDef.xan2d = 500;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 8;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32013; 
			itemDef.manWear = 31758; //male
			itemDef.womanWear = 31831; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30168:
			itemDef.name = "Boots of darkness";
			itemDef.zoom2d = 653;
			itemDef.xan2d = 81;
			itemDef.yan2d = 67;
			itemDef.xof2d = 3;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32034; 
			itemDef.manWear = 31818; //male
			itemDef.womanWear = 31885; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30167:
			itemDef.name = "Robe bottom of darkness";
			itemDef.zoom2d = 1979;
			itemDef.xan2d = 450;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 3;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31997; 
			itemDef.manWear = 31788; //male
			itemDef.womanWear = 31858; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30166:
			itemDef.name = "Gloves of darkness";
			itemDef.zoom2d = 450;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32001; 
			itemDef.manWear = 31774; //male
			itemDef.womanWear = 31852; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30165:
			itemDef.name = "Robe top of darkness";
			itemDef.zoom2d = 1284;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 1;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32036; 
			itemDef.manWear = 31814; //male
			itemDef.manWear2 = 31802;  //male
			itemDef.womanWear = 31877; // female
			itemDef.womanWear2 = 31865; //female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30164:
			itemDef.name = "Hood of darkness";
			itemDef.zoom2d = 905;
			itemDef.xan2d = 500;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 7;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31969; 
			itemDef.manWear = 31752; //male
			itemDef.womanWear = 31836; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -15;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30163:
			itemDef.name = "Ankou socks";
			itemDef.zoom2d = 653;
			itemDef.xan2d = 579;
			itemDef.yan2d = 67;
			itemDef.xof2d = 4;
			itemDef.yof2d = 3;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32002; 
			itemDef.manWear = 31820; //male
			itemDef.womanWear = 31880; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30162:
			itemDef.name = "Ankou's leggings";
			itemDef.zoom2d = 1720;
			itemDef.xan2d = 450;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 9;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32024; 
			itemDef.manWear = 31789; //male
			itemDef.womanWear = 31856; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30161:
			itemDef.name = "Ankou gloves";
			itemDef.zoom2d = 450;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 1;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31964; 
			itemDef.manWear = 31779; //male
			itemDef.womanWear = 31851; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30160:
			itemDef.name = "Ankou top";
			itemDef.zoom2d = 1160;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32017; 
			itemDef.manWear = 31811; //male
			itemDef.manWear2 = 31798;  //male
			itemDef.womanWear = 31874; // female
			itemDef.womanWear2 = 31870; //female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30159:
			itemDef.name = "Ankou mask";
			itemDef.zoom2d = 530;
			itemDef.xan2d = 500;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 8;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32031; 
			itemDef.manWear = 31747; //male
			itemDef.womanWear = 31825; // female
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30158:
			itemDef.name = "Dragon cane";
			itemDef.zoom2d = 3280;
			itemDef.xan2d = 1679;
			itemDef.yan2d = 258;
			itemDef.xof2d = 21;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 28679; 
			itemDef.manWear = 28626; //male
			itemDef.womanWear = 28626; // female
			itemDef.manwearyoff = 2;
			itemDef.manwearzoff = -20;
			itemDef.womanwearyoff = -9;
			itemDef.womanwearzoff = -10;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30157:
			itemDef.name = "Shayzien house hood";
			itemDef.zoom2d = 716;
			itemDef.xan2d = 108;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 3;
			itemDef.recolorModified = new int[]{920, 916, 16, 8, 0};  // 772
			itemDef.recolorOriginal = new int[]{22461, 22441, 960, 939, -21571}; // 774
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31999; 
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			itemDef.manwearyoff = -7;
			itemDef.womanwearyoff = -11;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30156:
			itemDef.name = "Shayzien house scarf";
			itemDef.zoom2d = 1350;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31989; 
			itemDef.manWear = 31765; //male
			itemDef.womanWear = 31843; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 3;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30155:
			itemDef.name = "Shayzien banner";
			itemDef.zoom2d = 2230;
			itemDef.xan2d = 512;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32047; 
			itemDef.manWear = 31908; //male
			itemDef.womanWear = 31908; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -7;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30154:
			itemDef.name = "Piscarilius house hood";
			itemDef.zoom2d = 716;
			itemDef.xan2d = 108;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 3;
			itemDef.recolorModified = new int[]{-27322, -27331, 49, 37, 7104};  // 772
			itemDef.recolorOriginal = new int[]{22461, 22441, 960, 939, -21571}; // 774
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31999; 
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			itemDef.manwearyoff = -7;
			itemDef.womanwearyoff = -11;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30153:
			itemDef.name = "Piscarilius house scarf";
			itemDef.zoom2d = 1350;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31980; 
			itemDef.manWear = 31762; //male
			itemDef.womanWear = 31844; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 3;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30152:
			itemDef.name = "Piscarilius banner";
			itemDef.zoom2d = 2230;
			itemDef.xan2d = 512;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32006; 
			itemDef.manWear = 31901; //male
			itemDef.womanWear = 31901; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -7;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30151:
			itemDef.name = "Lovakengj house hood";
			itemDef.zoom2d = 716;
			itemDef.xan2d = 108;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 3;
			itemDef.recolorModified = new int[]{9152, 7104, 10392, 10512, 5039};  // 772
			itemDef.recolorOriginal = new int[]{22461, 22441, 960, 939, -21571}; // 774
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31999; 
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			itemDef.manwearyoff = -7;
			itemDef.womanwearyoff = -11;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30150:
			itemDef.name = "Lovakengj house scarf";
			itemDef.zoom2d = 1350;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32045; 
			itemDef.manWear = 31763; //male
			itemDef.womanWear = 31841; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 3;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30149:
			itemDef.name = "Lovakengj banner";
			itemDef.zoom2d = 2242;
			itemDef.xan2d = 512;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32014; 
			itemDef.manWear = 31909; //male
			itemDef.womanWear = 31909; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -7;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30148:
			itemDef.name = "Hosidius house hood";
			itemDef.zoom2d = 716;
			itemDef.xan2d = 108;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 3;
			itemDef.recolorModified = new int[]{24088, 24084, 6695, 6814, 6806};  // 772
			itemDef.recolorOriginal = new int[]{22461, 22441, 960, 939, -21571}; // 774
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31999; 
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			itemDef.manwearyoff = -7;
			itemDef.womanwearyoff = -11;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30147:
			itemDef.name = "Hosidius house scarf";
			itemDef.zoom2d = 1350;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31968; 
			itemDef.manWear = 31760; //male
			itemDef.womanWear = 31842; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 3;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30146:
			itemDef.name = "Hosidius banner";
			itemDef.zoom2d = 2230;
			itemDef.xan2d = 512;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31963; 
			itemDef.manWear = 31900; //male
			itemDef.womanWear = 31900; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -7;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30145:
			itemDef.name = "Arceuus house hood";
			itemDef.zoom2d = 716;
			itemDef.xan2d = 108;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 3;
			itemDef.recolorModified = new int[]{-21967, -21851, 6348, 6340, 6973};  // 772
			itemDef.recolorOriginal = new int[]{22461, 22441, 960, 939, -21571}; // 774
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31999; 
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			itemDef.manwearyoff = -7;
			itemDef.womanwearyoff = -11;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30144:
			itemDef.name = "Arceuus house scarf";
			itemDef.zoom2d = 1350;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31985; 
			itemDef.manWear = 31764; //male
			itemDef.womanWear = 31845; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 3;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30143:
			itemDef.name = "Arceuus banner";
			itemDef.zoom2d = 2230;
			itemDef.xan2d = 512;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32025; 
			itemDef.manWear = 31896; //male
			itemDef.womanWear = 31896; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -7;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30142:
			itemDef.name = "Samurai boots";
			itemDef.zoom2d = 700;
			itemDef.xan2d = 131;
			itemDef.yan2d = 69;
			itemDef.xof2d = 3;
			itemDef.yof2d = -7;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32046; 
			itemDef.manWear = 31816; //male
			itemDef.womanWear = 31882; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30141:
			itemDef.name = "Samurai greaves";
			itemDef.zoom2d = 1720;
			itemDef.xan2d = 450;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 13;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32018; 
			itemDef.manWear = 31786; //male
			itemDef.womanWear = 31860; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30140:
			itemDef.name = "Samurai gloves";
			itemDef.zoom2d = 789;
			itemDef.xan2d = 609;
			itemDef.yan2d = 111;
			itemDef.xof2d = 1;
			itemDef.yof2d = 1;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32019; 
			itemDef.manWear = 31776; //male
			itemDef.womanWear = 31847; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30139:
			itemDef.name = "Samurai shirt";
			itemDef.zoom2d = 1348;
			itemDef.xan2d = 550;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31970; 
			itemDef.manWear = 31809; //male
			itemDef.womanWear = 31875; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30138:
			itemDef.name = "Samurai kasa";
			itemDef.zoom2d = 1296;
			itemDef.xan2d = 212;
			itemDef.yan2d = 1408;
			itemDef.xof2d = -4;
			itemDef.yof2d = -33;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 31972; 
			itemDef.manWear = 31756; //male
			itemDef.womanWear = 31830; // female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
		
		case 30137:
			itemDef.name = "Rangers' tunic";
			itemDef.zoom2d = 1616;
			itemDef.xan2d = 564;
			itemDef.yan2d = 75;
			itemDef.xof2d = 0;
			itemDef.yof2d = 5;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 28816; 
			itemDef.manWear = 28810; //male
			itemDef.manWear2 = 28809;  //male
			itemDef.womanWear = 28813; // female
			itemDef.womanWear2 = 28812; //female
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30136:
			itemDef.name = "Ranger gloves";
			itemDef.zoom2d = 930;
			itemDef.xan2d = 420;
			itemDef.yan2d = 828;
			itemDef.xof2d = 8;
			itemDef.yof2d = -8;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.modelId = 32042; 
			itemDef.manWear = 31781; //male
			itemDef.womanWear = 31849; // female
			itemDef.manwearyoff = 5;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30100:
			itemDef.name = "Catalytic rune pack";
			itemDef.zoom2d = 1411;
			itemDef.xan2d = 2047;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -3;
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			itemDef.modelId = 5467; 
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30101:
			itemDef.name = "Elemental rune pack";
			itemDef.zoom2d = 1411;
			itemDef.xan2d = 2047;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -3;
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			itemDef.modelId = 5616; 
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30102:
			itemDef.name = "Adamant arrow pack";
			itemDef.zoom2d = 1411;
			itemDef.xan2d = 2047;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -3;
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			itemDef.modelId = 21845; 
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30103:
			itemDef.name = "Rune arrow pack";
			itemDef.zoom2d = 1411;
			itemDef.xan2d = 2047;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -3;
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			itemDef.modelId = 21845; 
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30104:
			itemDef.name = "Ancient magicks tablet";
			itemDef.zoom2d = 465;
			itemDef.xan2d = 500;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -1;
			itemDef.inventoryOptions = new String[]{"Break", null, null, null, "Drop"};
			itemDef.modelId = 5461; 
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30105:
			itemDef.name = "Bloody key";
			itemDef.zoom2d = 710;
			itemDef.xan2d = 550;
			itemDef.yan2d = 50;
			itemDef.xof2d = -1;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			itemDef.modelId = 32070; 
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30106:
			itemDef.name = "Bloodier key";
			itemDef.zoom2d = 710;
			itemDef.xan2d = 550;
			itemDef.yan2d = 50;
			itemDef.xof2d = -1;
			itemDef.yof2d = 0;
			itemDef.recolorOriginal = new int[]{2966};  // 772
			itemDef.recolorModified = new int[]{8784}; // 774
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			itemDef.modelId = 32070; 
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30107:
			itemDef.name = "Survival tokens";
			itemDef.zoom2d = 550;
			itemDef.xan2d = 202;
			itemDef.yan2d = 1764;
			itemDef.zan2d = 108;
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			itemDef.recolorModified = new int[]{576, 561, -574};  // 772
			itemDef.recolorOriginal = new int[]{5813, 9139, 26006}; // 774
			itemDef.modelId = 15343;
			itemDef.stackable = true;
			itemDef.stackIDs = new int[]{30108, 30109, 30110, 30111, 0, 0, 0, 0, 0, 0};
			itemDef.stackAmounts = new int[]{2, 3, 4, 5, 0, 0, 0, 0, 0, 0};
			itemDef.cost = 1000;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30108:
			itemDef.name = "Survival tokens";
			itemDef.zoom2d = 530;
			itemDef.xan2d = 364;
			itemDef.yan2d = 96;
			itemDef.xof2d = 2;
			itemDef.yof2d = 19;
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			itemDef.recolorModified = new int[]{576, 561, -574};  // 772
			itemDef.recolorOriginal = new int[]{5813, 9139, 26006}; // 774
			itemDef.modelId = 15344;
			itemDef.stackable = true;
			//itemDef.stackIDs = new int[]{20528, 20529, 20530, 20531, 0, 0, 0, 0, 0, 0};
			itemDef.cost = 1000;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30109:
			itemDef.name = "Survival tokens";
			itemDef.zoom2d = 589;
			itemDef.xan2d = 337;
			itemDef.yan2d = 754;
			itemDef.xof2d = 7;
			itemDef.yof2d = -22;
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			itemDef.recolorModified = new int[]{576, 561, -574};  // 772
			itemDef.recolorOriginal = new int[]{5813, 9139, 26006}; // 774
			itemDef.modelId = 15345;
			itemDef.stackable = true;
			//itemDef.stackIDs = new int[]{20528, 20529, 20530, 20531, 0, 0, 0, 0, 0, 0};
			itemDef.cost = 1000;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30110:
			itemDef.name = "Survival tokens";
			itemDef.zoom2d = 589;
			itemDef.xan2d = 350;
			itemDef.yan2d = 795;
			itemDef.xof2d = 4;
			itemDef.yof2d = -14;
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			itemDef.recolorModified = new int[]{576, 561, -574};  // 772
			itemDef.recolorOriginal = new int[]{5813, 9139, 26006}; // 774
			itemDef.modelId = 15346;
			itemDef.stackable = true;
			//itemDef.stackIDs = new int[]{20528, 20529, 20530, 20531, 0, 0, 0, 0, 0, 0};
			itemDef.cost = 1000;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30111:
			itemDef.name = "Survival tokens";
			itemDef.zoom2d = 560;
			itemDef.xan2d = 471;
			itemDef.yan2d = 687;
			itemDef.xof2d = 8;
			itemDef.yof2d = -7;
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			itemDef.recolorModified = new int[]{576, 561, -574};  // 772
			itemDef.recolorOriginal = new int[]{5813, 9139, 26006}; // 774
			itemDef.modelId = 15347;
			itemDef.stackable = true;
			//itemDef.stackIDs = new int[]{20528, 20529, 20530, 20531, 0, 0, 0, 0, 0, 0};
			itemDef.cost = 1000;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30116:
			itemDef.name = "Bunny feet";
			itemDef.zoom2d = 1095;
			itemDef.xan2d = 242;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
			itemDef.modelId = 29208;
			itemDef.manWear = 29189;
			itemDef.womanWear = 29190;
			itemDef.manwearyoff = -7;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30117:
			itemDef.name = "Bunny top";
			itemDef.zoom2d = 1250;
			itemDef.xan2d = 488;
			itemDef.yan2d = 0;
			itemDef.xof2d = -1;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
			itemDef.modelId = 31243;
			itemDef.manWear = 31188;
			itemDef.manWear2 = 31187;
			itemDef.womanWear = 31192;
			itemDef.womanWear2 = 31191;
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30118:
			itemDef.name = "Bunny legs";
			itemDef.zoom2d = 1740;
			itemDef.xan2d = 444;
			itemDef.yan2d = 0;
			itemDef.xof2d = 0;
			itemDef.yof2d = -8;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
			itemDef.modelId = 31244;
			itemDef.manWear = 31186;
			itemDef.womanWear = 31190;
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30119:
			itemDef.name = "Bunny paws";
			itemDef.zoom2d = 972;
			itemDef.xan2d = 354;
			itemDef.yan2d = 1220;
			itemDef.xof2d = 0;
			itemDef.yof2d = -8;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
			itemDef.modelId = 31246;
			itemDef.manWear = 31185;
			itemDef.womanWear = 31189;
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
		case 30120:
			itemDef.name = "Easter egg helm";
			itemDef.zoom2d = 972;
			itemDef.xan2d = 128;
			itemDef.yan2d = 1856;
			itemDef.xof2d = 0;
			itemDef.yof2d = 0;
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
			itemDef.modelId = 32965;
			itemDef.manWear = 6695;
			itemDef.womanWear = 32939;
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.modelType = ModelTypeEnum.OSRS;
			return itemDef;
			
			//LMS items
		case 30121:
			itemDef.setDefaults();
			itemDef.modelId = 28162;
			itemDef.name = "Armadyl godsword";
			itemDef.description = "A beautiful, heavy sword.".getBytes();
			itemDef.zoom2d = 1957;
			itemDef.xan2d = 444;
			itemDef.yan2d = 498;
			itemDef.xof2d = 1;
			itemDef.yof2d = -8;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
			itemDef.manWear = 27731;
			itemDef.womanWear = 27731;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30122:
			itemDef.setDefaults();
			itemDef.name = "Dragon claws";
			itemDef.description = "A pair of dragon claws.".getBytes();
			itemDef.modelId = 44590;
			itemDef.zoom2d = 789;
			itemDef.xan2d = 240;
			itemDef.yan2d = 60;
			itemDef.xof2d = -1;
			itemDef.yof2d = -23;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 43660;
			itemDef.womanWear = 43651;
			itemDef.ambient = 15;
			itemDef.contrast = 75;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30123:
			itemDef.setDefaults();
			itemDef.name = "Dragon 2h sword";
			itemDef.description = "A two-handed Dragon Sword.".getBytes();
			itemDef.modelId = 11215;
			itemDef.zoom2d = 1908;
			itemDef.xan2d = 444;
			itemDef.yan2d = 1522;
			itemDef.yof2d = -7;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
			itemDef.manWear = 11225;
			itemDef.womanWear = 11225;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30124:
			itemDef.setDefaults();
			itemDef.name = "Abyssal whip";
			itemDef.description = "A weapon from the abyss.".getBytes();
			itemDef.modelId = 5412;
			itemDef.zoom2d = 840;
			itemDef.xan2d = 280;
			itemDef.xof2d = -2;
			itemDef.yof2d = 56;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
			itemDef.manWear = 5409;
			itemDef.womanWear = 5409;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30125:
			itemDef.setDefaults();
			itemDef.name = "Dragon warhammer";
			itemDef.description = "The body of a Dwarf savaged by Goblins.".getBytes();
			itemDef.modelId = 4041;
			itemDef.zoom2d = 1600;
			itemDef.xan2d = 1510;
			itemDef.yan2d = 1785;
			itemDef.xof2d = 9;
			itemDef.yof2d = -4;
			itemDef.members = true;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
			itemDef.manWear = 4037;
			itemDef.manwearyoff = 5;
			itemDef.womanWear = 4038;
			itemDef.womanwearyoff = -9;
			itemDef.womanwearzoff = 5;
			itemDef.modelType = ModelTypeEnum.OSRS;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30126:
			itemDef.setDefaults();
			itemDef.name = "Dragon sword";
			itemDef.description = "A razor sharp sword.".getBytes();
			itemDef.zoom2d = 1168;
			itemDef.xan2d = 691;
			itemDef.yan2d = 478;
			itemDef.manWear = 32676; 
			itemDef.womanWear = 32676; 
			itemDef.modelId = 32791; 
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			itemDef.manwearyoff = 7;
			itemDef.manwearzoff = -3;
			itemDef.recolorOriginal = new int[]{61};
			itemDef.recolorModified = new int[]{36133};
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, null};
			itemDef.modelType = ModelTypeEnum.OSRS;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30127:
			itemDef.setDefaults();
			itemDef.name = "Elder maul";
			itemDef.description = "A maul crafted from the component parts of Tekton.".getBytes();
			itemDef.zoom2d = 1744;
			itemDef.xan2d = 237;
			itemDef.yan2d = 429;
			itemDef.manWear = 32678; 
			itemDef.womanWear = 32678; 
			itemDef.modelId = 32792; 
			itemDef.womanwearyoff = -6;
			itemDef.womanwearzoff = 5;
			itemDef.manwearyoff = 6;
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, null};
			itemDef.modelType = ModelTypeEnum.OSRS;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30128:
			itemDef.setDefaults();
			itemDef.name = "Dragon chainbody";
			itemDef.description = "A series of connected metal rings.".getBytes();
			itemDef.modelId = 3823;
			itemDef.zoom2d = 1100;
			itemDef.xan2d = 568;
			itemDef.yof2d = 2;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 3820;
			itemDef.manWear2 = 44804;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30129: 
			itemDef.setDefaults();
			itemDef.name = "Dragon platelegs";
			itemDef.description = "These look pretty heavy.".getBytes();
			itemDef.modelId = 5026;
			itemDef.zoom2d = 1740;
			itemDef.xan2d = 444;
			itemDef.yof2d = -8;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 5024;
			itemDef.womanWear = 5025;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30130:
			itemDef.setDefaults();
			itemDef.name = "Dark bow";
			itemDef.description = "A bow from a darker dimension.".getBytes();
			itemDef.modelId = 26386;
			itemDef.zoom2d = 2128;
			itemDef.xan2d = 591;
			itemDef.yan2d = 1034;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
			itemDef.manWear = 26279;
			itemDef.womanWear = 26279;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30131:
			itemDef.setDefaults();
			itemDef.name = "Master wand";
			itemDef.description = "A master level wand.".getBytes();
			itemDef.modelId = 10565;
			itemDef.zoom2d = 1115;
			itemDef.xan2d = 185;
			itemDef.yan2d = 1185;
			itemDef.xof2d = 5;
			itemDef.yof2d = -22;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
			itemDef.manWear = 10702;
			itemDef.womanWear = 10702;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30132:
			itemDef.setDefaults();
			itemDef.name = "Third-age robe top";
			itemDef.modelId = 20245;
			itemDef.zoom2d = 2143;
			itemDef.xan2d = 553;
			itemDef.xof2d = 1;
			itemDef.yof2d = 1;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 20160;
			itemDef.manWear2 = 20124;
			itemDef.womanWear = 20207;
			itemDef.womanWear2 = 20173;
			itemDef.contrast = 100;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30133:
			itemDef.setDefaults();
			itemDef.name = "Third-age robe";
			itemDef.modelId = 20244;
			itemDef.zoom2d = 2083;
			itemDef.xan2d = 438;
			itemDef.yof2d = -1;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 20143;
			itemDef.womanWear = 20191;
			itemDef.contrast = 100;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30134:
			itemDef.setDefaults();
			itemDef.name = "Ahrim's robe top";
			itemDef.modelId = 6578;
			itemDef.recolorOriginal = new int[] {8741, 14490};
			itemDef.recolorModified = new int[] {10512, 10512};
			itemDef.zoom2d = 1250;
			itemDef.xan2d = 468;
			itemDef.yof2d = 3;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 6669;
			itemDef.manWear2 = 170;
			itemDef.womanWear = 6697;
			itemDef.womanWear2 = 348;
			itemDef.excludeFromSearch = true;
			return itemDef;
			
		case 30135:
			itemDef.setDefaults();
			itemDef.name = "Ahrim's robe skirt";
			itemDef.modelId = 6577;
			itemDef.recolorOriginal = new int[] {14490};
			itemDef.recolorModified = new int[] {10512};
			itemDef.zoom2d = 1730;
			itemDef.xan2d = 504;
			itemDef.yof2d = -4;
			itemDef.options = new String[] {null, null, "Take", null, null};
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			itemDef.manWear = 6659;
			itemDef.womanWear = 40595;
			itemDef.excludeFromSearch = true;
			return itemDef;
			//END of LMS items
		
			//Overloads
			case 15332:
				itemDef.noteId = 22322;
				return itemDef;
			case 22322:
				itemDef.noteId = 15332;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15333:
				itemDef.noteId = 22323;
				return itemDef;
			case 22323:
				itemDef.noteId = 15333;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15334:
				itemDef.noteId = 22324;
				return itemDef;
			case 22324:
				itemDef.noteId = 15334;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15335:
				itemDef.noteId = 22325;
				return itemDef;
			case 22325:
				itemDef.noteId = 15335;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
				
			//extreme range
			case 15324:
				itemDef.noteId = 22326;
				return itemDef;
			case 22326:
				itemDef.noteId = 15324;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15325:
				itemDef.noteId = 22327;
				return itemDef;
			case 22327:
				itemDef.noteId = 15325;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15326:
				itemDef.noteId = 22328;
				return itemDef;
			case 22328:
				itemDef.noteId = 15326;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15327:
				itemDef.noteId = 22329;
				return itemDef;
			case 22329:
				itemDef.noteId = 15327;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			//extreme strength
			case 15312:
				itemDef.noteId = 22330;
				return itemDef;
			case 22330:
				itemDef.noteId = 15312;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15313:
				itemDef.noteId = 22331;
				return itemDef;
			case 22331:
				itemDef.noteId = 15313;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15314:
				itemDef.noteId = 22332;
				return itemDef;
			case 22332:
				itemDef.noteId = 15314;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15315:
				itemDef.noteId = 22333;
				return itemDef;
			case 22333:
				itemDef.noteId = 15315;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			//extreme mage
			case 15320:
				itemDef.noteId = 22334;
				return itemDef;
			case 22334:
				itemDef.noteId = 15320;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15321:
				itemDef.noteId = 22335;
				return itemDef;
			case 22335:
				itemDef.noteId = 15321;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15322:
				itemDef.noteId = 22336;
				return itemDef;
			case 22336:
				itemDef.noteId = 15322;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15323:
				itemDef.noteId = 22337;
				return itemDef;
			case 22337:
				itemDef.noteId = 15323;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			//extreme attack
			case 15308:
				itemDef.noteId = 22338;
				return itemDef;
			case 22338:
				itemDef.noteId = 15308;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15309:
				itemDef.noteId = 22339;
				return itemDef;
			case 22339:
				itemDef.noteId = 15309;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15310:
				itemDef.noteId = 22340;
				return itemDef;
			case 22340:
				itemDef.noteId = 15310;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15311:
				itemDef.noteId = 22341;
				return itemDef;
			case 22341:
				itemDef.noteId = 15311;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			//extreme defence
			case 15316:
				itemDef.noteId = 22342;
				return itemDef;
			case 22342:
				itemDef.noteId = 15316;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15317:
				itemDef.noteId = 22343;
				return itemDef;
			case 22343:
				itemDef.noteId = 15317;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15318:
				itemDef.noteId = 22344;
				return itemDef;
			case 22344:
				itemDef.noteId = 15318;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15319:
				itemDef.noteId = 22345;
				return itemDef;
			case 22345:
				itemDef.noteId = 15319;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			//super prayer
			case 15328:
				itemDef.noteId = 22346;
				return itemDef;
			case 22346:
				itemDef.noteId = 15328;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15329:
				itemDef.noteId = 22347;
				return itemDef;
			case 22347:
				itemDef.noteId = 15329;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15330:
				itemDef.noteId = 22348;
				return itemDef;
			case 22348:
				itemDef.noteId = 15330;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15331:
				itemDef.noteId = 22349;
				return itemDef;
			case 22349:
				itemDef.noteId = 15331;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			//super antifire
			case 15304:
				itemDef.noteId = 22350;
				return itemDef;
			case 22350:
				itemDef.noteId = 15304;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15305:
				itemDef.noteId = 22351;
				return itemDef;
			case 22351:
				itemDef.noteId = 15305;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15306:
				itemDef.noteId = 22352;
				return itemDef;
			case 22352:
				itemDef.noteId = 15306;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15307:
				itemDef.noteId = 22353;
				return itemDef;
			case 22353:
				itemDef.noteId = 15307;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			//spec restore
			case 15300:
				itemDef.noteId = 22354;
				return itemDef;
			case 22354:
				itemDef.noteId = 15300;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15301:
				itemDef.noteId = 22355;
				return itemDef;
			case 22355:
				itemDef.noteId = 15301;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15302:
				itemDef.noteId = 22356;
				return itemDef;
			case 22356:
				itemDef.noteId = 15302;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			case 15303:
				itemDef.noteId = 22357;
				return itemDef;
			case 22357:
				itemDef.noteId = 15303;
				itemDef.certTemplateID = 799;
				itemDef.toNote();
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
				
			case 19668:
			    itemDef.name = "Strong taming rope";
			    itemDef.modelId = 59660;
				itemDef.recolorModified = new int[]{5931};
			    itemDef.recolorOriginal = new int[]{8128};
			    itemDef.zoom2d = 1320;
			    itemDef.xan2d = 303;
			    itemDef.yan2d = 15;
			    itemDef.xof2d = -3;
			    itemDef.yof2d = 21;
			    return itemDef;
				
		    case 19667:
		    	itemDef.name = "Taming rope";
		    	return itemDef;
		    	
		    case 19702:
		    	itemDef.name = "Taming rope";
		    	return itemDef;
		    	
		    case 19665:
		    	itemDef.modelId = 59660;
		    	itemDef.name = "Training rope";
		    	return itemDef;
		    	
		    case 19666:
		    	itemDef.modelId = 2429;
		    	itemDef.name = "Training rope";
		    	return itemDef;
		    	
		    case 20061:
		    	itemDef.name = "Random Pet Box";
		    	return itemDef;
		    	
		    case 15360:
		    	itemDef.name = "Pet Double Exp Scroll";
		    	return itemDef;
		    	
		    case 21821:
		    	itemDef.name = "Pet Stat Reset Scroll";
		    	itemDef.inventoryOptions = new String[]{"Reset pet stats", null, null, null, null};
		    	return itemDef;
		    	
		    case 21403:
		    	itemDef.name = "Pet Boost";
		    	return itemDef;
			
			case 995:
			case 996:
			case 997:
			case 998:
			case 999:
			case 1000:
			case 1001:
			case 1002:
			case 1003:
			case 1004:
				itemDef.inventoryOptions = new String[]{null, null, null, "Add-To-Pouch", "Drop"};
				return itemDef;
			
			case 15390:
				itemDef.name = "Hunter lamp";
				return itemDef;
				
			case 20863:
				itemDef.name = "Gatestone 2";
				return itemDef;
			
			//castle wars capes
			case 18739:
				itemDef.manWear = 60062;
				return itemDef;
			case 18740:
				itemDef.manWear = 60063;
				return itemDef;
			case 18741:
				itemDef.manWear = 60064;
				return itemDef;
			case 18742:
				itemDef.manWear = 60065;
				return itemDef;
			case 18743:
				itemDef.manWear = 60066;
				return itemDef;
			//decorative items for castle wars
			case 18708:
				itemDef.manWear = 60052;
				return itemDef;
			case 18707:
				itemDef.manWear = 60051;
				return itemDef;
			case 18706:
				itemDef.manWear = 60050;
				return itemDef;
			
			case 4069:
				itemDef.manWear = 60053;
				return itemDef;
			case 4070:
				itemDef.manWear = 60054;
				return itemDef;
			case 4071:
				itemDef.manWear = itemDef.womanWear = 60055;
				return itemDef;
			
			case 4504:
				itemDef.manWear = 60057;
				return itemDef;
			case 4505:
				itemDef.manWear = 60056;
				return itemDef;
			case 4506:
				itemDef.manWear = itemDef.womanWear = 60058;
				return itemDef;
			
			case 4511:
				itemDef.manWear = itemDef.womanWear = 60059;
				return itemDef;
			case 4509:
				itemDef.manWear = 60060;
				return itemDef;
			case 4510:
				itemDef.manWear = 60061;
				return itemDef;
			// end decorative items for castle wars
			
			case 4006:
				itemDef.name = "Pet dagannoth supreme";
				itemDef.zoom2d = 4560;
				itemDef.xan2d = 2042;
				itemDef.yan2d = 1868;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 9941;
				return itemDef;
			
			case 4007:
				itemDef.name = "Pet dagannoth prime";
				itemDef.zoom2d = 4560;
				itemDef.xan2d = 2042;
				itemDef.yan2d = 1868;
				itemDef.recolorModified = new int[]{5931, 1688, 21530, 21534};
				itemDef.recolorOriginal = new int[]{11930, 27144, 16536, 16540};
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 9941;
				return itemDef;
			case 4008:
				itemDef.name = "Pet dagannoth rex";
				itemDef.zoom2d = 4560;
				itemDef.xan2d = 2042;
				itemDef.yan2d = 1868;
				itemDef.recolorModified = new int[]{7322, 7326, 10403, 2595};
				itemDef.recolorOriginal = new int[]{16536, 16540, 27144, 2477};
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 9941;
				return itemDef;
			case 3998:
				itemDef.name = "Baby mole";
				itemDef.zoom2d = 2256;
				itemDef.xan2d = 369;
				itemDef.yan2d = 1874;
				itemDef.yof2d = 20;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 12073;
				return itemDef;
			case 3997://green
				itemDef.name = "Kalphite princess";
				itemDef.zoom2d = 8016;
				itemDef.xan2d = 342;
				itemDef.yan2d = 1778;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 24597;
				return itemDef;
			
			case 4002:
				itemDef.name = "Pet chaos elemental";
				itemDef.zoom2d = 1600;
				itemDef.yan2d = 20;
				itemDef.xan2d = 175;
				itemDef.yof2d = 150;
				itemDef.xof2d = 10;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 66003;//original id 28256
				return itemDef;
			
			case 3999:
				itemDef.name = "Pet kree'arra";
				itemDef.zoom2d = 976;
				itemDef.yan2d = 1892; // 2042
				itemDef.xan2d = 2042; //1892
				itemDef.xof2d = -20;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 66002;//original id 28873
				return itemDef;
			case 4001:
				itemDef.name = "Pet general graardor";
				itemDef.zoom2d = 1300;
				itemDef.xof2d = -3;
				itemDef.yof2d = -3;
				itemDef.yan2d = 1660; // 1660
				itemDef.xan2d = 204; // 204
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 66001;//original id 28874
				return itemDef;
			case 4005:
				itemDef.name = "Pet zilyana";
				itemDef.zoom2d = 900;
				itemDef.yan2d = 9;
				itemDef.xan2d = 1931;
				itemDef.xof2d = 5;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 66000;//original id 28870
				return itemDef;
			case 4004:
				itemDef.name = "Pet k'ril tsutsaroth";
				itemDef.zoom2d = 1168;
				itemDef.yan2d = 2012;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 66004;//original id 28868
				return itemDef;
			case 4011:
				itemDef.name = "Prince black dragon";
				itemDef.yan2d = 6;
				itemDef.xan2d = 10;
				itemDef.zoom2d = 1500;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 66005;//original id 28872
				return itemDef;
			case 4010: //red
				itemDef.name = "Kalphite princess";
				itemDef.zoom2d = 740;
				itemDef.yan2d = 279;
				itemDef.xan2d = 1808;
				itemDef.options = new String[]{null, null, "Take", null, null};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 66006;//original id 28871
				return itemDef;
			
			case 15412:
				itemDef.name = "Monster egg";
				itemDef.zoom2d = 550;
				return itemDef;
			
			case 17676:
				itemDef.name = "Lava dragon bones";
				return itemDef;
			
			case 17677:
				itemDef.name = "Lava dragon bones";
				return itemDef;
			
			case 19967:
				itemDef.name = "Vote reward bag";//"Nomad's Teleporting Bag";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
				
		    case 18201:
			    itemDef.name = "Rusty coins";
			    return itemDef;
			
			case 8890:
			case 8891:
			case 8892:
			case 8893:
			case 8894:
			case 8895:
			case 8896:
			case 8897:
			case 8898:
			case 8899:
				if (Config.SERVER_PORT_PVP == Client.port) {//if ("SoulPvP".equalsIgnoreCase(com.soulplayps.client.Config.SERVER_NAME)) {
					itemDef.recolorOriginal = new int[]{8128};
					itemDef.recolorModified = new int[]{947};
					itemDef.name = "Blood money";
				}
				return itemDef;
			case 4273:
				itemDef.name = "Golden Key";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop"};
				return itemDef;
				
			case 6759:
				itemDef.name = "Golden Chest";
				itemDef.inventoryOptions = new String[] {null, null, null, "Info", "Drop"};
				return itemDef;
			case 21473: // vanguard top
				itemDef.modelId = 44627;
				itemDef.zoom2d = 1513;
				itemDef.yan2d = 2041;
				itemDef.xan2d = 593;
				itemDef.xof2d = -11;
				itemDef.yof2d = 3;
				itemDef.manWear = 44812;
				itemDef.womanWear = 44812;
				return itemDef;
			case 21474: // vanguard legs
				itemDef.manWear = 45232;
				return itemDef;
			case 21468: // Trickster robe
				itemDef.womanWear = 44786;
				itemDef.manWear = 44786;
				itemDef.modelId = 45329;
				itemDef.xan2d = 593;
				itemDef.yan2d = 2041;
				itemDef.zoom2d = 1420;
				itemDef.yof2d = 0;
				itemDef.xof2d = 0;
				return itemDef;
			case 21469: // Trickster legs
				itemDef.womanWear = 44770;
				itemDef.manWear = 44770;
				itemDef.modelId = 45335;
				itemDef.xan2d = 567;
				itemDef.yan2d = 1023;
				itemDef.zoom2d = 2105;
				itemDef.yof2d = 0;
				itemDef.xof2d = 0;
				return itemDef;
			case 12391:
				itemDef.name = "Gilded boots";
				itemDef.zoom2d = 770;
				itemDef.xan2d = 164;
				itemDef.yan2d = 156;
				itemDef.xof2d = 3;
				itemDef.yof2d = -3;
				itemDef.recolorModified = new int[]{7114, 7104};
				itemDef.recolorOriginal = new int[]{61, 5400};
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				itemDef.modelId = 5037;
				itemDef.manWear = 4954;
				itemDef.womanWear = 5031;
				return itemDef;
			case 12399:
				itemDef.name = "Partyhat & specs";
				itemDef.zoom2d = 653;
				itemDef.yan2d = 0;
				itemDef.xan2d = 242;
				itemDef.xof2d = -1;
				itemDef.yof2d = -59;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.womanwearyoff = -5;
				itemDef.womanwearzoff = 5;
				itemDef.modelId = 28693;
				itemDef.manWear = 28505;
				itemDef.womanWear = 28576;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -7;
				itemDef.manwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			case 12002:
				itemDef.name = "Occult necklace";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 431;
				itemDef.yan2d = 81;
				itemDef.xof2d = 3;
				itemDef.yof2d = 19;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				itemDef.womanwearyoff = -9;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -9;
				itemDef.manwearzoff = -3;
				itemDef.modelId = 28438;
				itemDef.manWear = 28445;
				itemDef.womanWear = 28445;
				return itemDef;
			
			case 11791:
				itemDef.name = "Staff of the dead";
				itemDef.zoom2d = 1490;
				itemDef.xan2d = 138;
				itemDef.yan2d = 1300;
				itemDef.xof2d = -3;
				itemDef.yof2d = 2;
				itemDef.recolorModified = new int[]{18626, 18626, 18626,
						18626};
				itemDef.recolorOriginal = new int[]{17467, -22419, 7073, 61};
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null,
						"Drop"};
				itemDef.modelId = 2810;
				itemDef.manWear = 5232;
				itemDef.womanWear = 5232;
				itemDef.womanwearxoff = 2;
				itemDef.womanwearyoff = -16;
				itemDef.womanwearzoff = 5;
				return itemDef;
			
			case 11785:
				itemDef.name = "Armadyl crossbow";
				itemDef.zoom2d = 1325;
				itemDef.xan2d = 240;
				itemDef.yan2d = 110;
				itemDef.xof2d = -4;
				itemDef.yof2d = -40;
				itemDef.recolorModified = new int[]{115, 107, 10167, 10171};
				itemDef.recolorOriginal = new int[]{5409, 5404, 6449, 7390};
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null,
						"Drop"};
				itemDef.modelId = 19967;
				itemDef.manWear = 19839;
				itemDef.womanWear = 19839;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 10;
				return itemDef;
			
			case 12004:
				itemDef.name = "Kraken tentacle";
				itemDef.zoom2d = 1095;
				itemDef.xan2d = 593;
				itemDef.yan2d = 741;
				itemDef.xof2d = 4;
				itemDef.yof2d = 4;
				itemDef.recolorModified = new int[]{8097, 9121, 8092, 9118};
				itemDef.recolorOriginal = new int[]{11148, 10772, 10652,
						10533};
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.modelId = 28437;
				return itemDef;
			
			case 12006:
				itemDef.name = "Abyssal tentacle";
				itemDef.zoom2d = 840;
				itemDef.xan2d = 280;
				itemDef.yan2d = 121;
				itemDef.xof2d = 3;
				itemDef.yof2d = 56;
				itemDef.modelId = 28439;
				itemDef.inventoryOptions = new String[]{null, "Weld", null, "Check", "Dissolve"};
				itemDef.manWear = 28446;
				itemDef.womanWear = 28446;
				return itemDef;
			
			case 11924:
				itemDef.name = "Malediction ward";
				itemDef.zoom2d = 1200;
				itemDef.xan2d = 568;
				itemDef.yan2d = 1836;
				itemDef.xof2d = 2;
				itemDef.yof2d = 3;
				itemDef.recolorModified = new int[]{-21608};
				itemDef.recolorOriginal = new int[]{908};
				itemDef.modelId = 9354;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 9347;
				itemDef.womanwearyoff = -4;
				itemDef.manwearyoff = 10;
				itemDef.manwearxoff = 5;
				itemDef.womanWear = 9347;
				return itemDef;
			
			case 11926:
				itemDef.name = "Odium ward";
				itemDef.zoom2d = 1200;
				itemDef.xan2d = 568;
				itemDef.yan2d = 1836;
				itemDef.xof2d = 2;
				itemDef.yof2d = 3;
				itemDef.recolorModified = new int[]{15252};
				itemDef.recolorOriginal = new int[]{908};
				itemDef.modelId = 9354;
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 9347;
				itemDef.womanwearyoff = -4;
				itemDef.manwearyoff = 10;
				itemDef.manwearxoff = 5;
				itemDef.womanWear = 9347;
				return itemDef;
			
			case 2550:
				itemDef.inventoryOptions = new String[]{null, "Wield", null, "Break", "Drop"};
				return itemDef;
			
			case 9721:
				itemDef.name = "Yell scroll";
				itemDef.description = "A yell scroll.".getBytes();
				return itemDef;
			
			case 13663:
				itemDef.name = "Spin ticket";
				itemDef.description = "A spinning wheel ticket.".getBytes();
				itemDef.inventoryOptions = new String[]{"Open ticket", null, null, null, "Drop"};
				return itemDef;
			
			case 15501:
				itemDef.name = "Skilling package";
				itemDef.description = "A box with skilling items.".getBytes();
				return itemDef;
			
			case 6542:
				itemDef.name = "PK package";
				itemDef.description = "A box with PK items.".getBytes();
				return itemDef;
			
			case 3062:
				itemDef.name = "Coin box";
				itemDef.description = "A box with coins in it.".getBytes();
				return itemDef;
			
			case 786:
				itemDef.name = "$10 Scroll";
				itemDef.description = "A scroll worth $10.".getBytes();
				return itemDef;
			
			case 1505:
				itemDef.name = "$50 Scroll";
				itemDef.description = "A scroll worth $50".getBytes();
				return itemDef;
			
			case 2396:
				itemDef.name = "$100 Scroll";
				itemDef.description = "A scroll worth $100".getBytes();
				return itemDef;
			
			case 10831:
				itemDef.name = "Empty money bag";
				itemDef.description = "It's a empty money bag".getBytes();
				return itemDef;
			
			case 10832:
				itemDef.name = "Light money bag";
				itemDef.description = "It's a light money bag".getBytes();
				return itemDef;
			
			case 10833:
				itemDef.name = "Normal money bag";
				itemDef.description = "It's a Normal money bag".getBytes();
				return itemDef;
			
			case 10834:
				itemDef.name = "Hefty money bag";
				itemDef.description = "It's a hefty money bag".getBytes();
				return itemDef;
			
			case 10835:
				itemDef.name = "Bulging money bag";
				itemDef.stackable = true;
				itemDef.description = "It's a bulging money bag".getBytes();
				return itemDef;
			
			case 10867:
				itemDef.name = "Achievement diary hood";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.modelId = 19058;
				itemDef.zoom2d = 720;
				itemDef.xan2d = 21;
				itemDef.yan2d = 18;
				itemDef.recolorModified = new int[]{-22440, -22423, -22427, 21619, 21733, 21733};
				itemDef.recolorOriginal = new int[]{-8256, -11353, -11033, 960, 22464, -21568};
				itemDef.xof2d = 1;
				itemDef.yof2d = 1;
				itemDef.zan2d = 0;
				itemDef.manWear = 18914;
				itemDef.womanWear = 18967;
				itemDef.description = "An Achievement diary hood.".getBytes();
				itemDef.stackable = false;
				return itemDef;
			
			
			case 10868:
				itemDef.modelId = 19062;
				itemDef.options = new String[5];
				itemDef.options[2] = "Take";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.zoom2d = 2128;
				itemDef.xan2d = 504;
				itemDef.yan2d = 0;
				itemDef.recolorModified = new int[]{21733, 21619, -22427, 21619, 21733, 21733}; //419770
				itemDef.recolorOriginal = new int[]{57280, 54183, 54503, 960, 22464, 43968};
				itemDef.yof2d = 0;
				itemDef.xof2d = 1;
				itemDef.manWear = 18946;
				itemDef.womanWear = 18984;
				itemDef.name = "Achievement diary cape (t)";
				itemDef.description = "Achievement diary cape (t)".getBytes();
				itemDef.stackable = false;
				return itemDef;
			
			case 15740:
				itemDef.name = "Black Santa Hat";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.recolorOriginal = new int[1];
				itemDef.recolorModified = new int[1];
				itemDef.recolorOriginal[0] = 933;
				itemDef.recolorModified[0] = 196608;
				itemDef.modelId = 2537;
				itemDef.zoom2d = 540;
				itemDef.yan2d = 136;
				itemDef.xan2d = 0;
				itemDef.xof2d = 0;
				itemDef.yof2d = -3;
				itemDef.manWear = 189; // male wield model
				itemDef.womanWear = 366; // femArmModel
				itemDef.description = "Wow a santa hat".getBytes();
				return itemDef;
			
			case 15741:
				itemDef.name = "Black Partyhat";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.recolorOriginal = new int[1];
				itemDef.recolorModified = new int[1];
				itemDef.recolorOriginal[0] = 926;
				itemDef.recolorModified[0] = 6020;
				itemDef.modelId = 2635;
				itemDef.zoom2d = 440;
				itemDef.xan2d = 76;
				itemDef.yan2d = 1850;
				itemDef.zan2d = 0;
				itemDef.xof2d = 1;
				itemDef.yof2d = 0;
				itemDef.manWear = 187;
				itemDef.womanWear = 363;
				itemDef.manWear2 = -1;
				itemDef.womanWear2 = -1;
				itemDef.manhead = -1;
				itemDef.womanhead = -1;
				itemDef.description = "A Black Partyhat, the most expensive item in the game".getBytes();
				return itemDef;
			
			case 15742:
				itemDef.name = "Pink Partyhat";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.recolorOriginal = new int[1];
				itemDef.recolorModified = new int[1];
				itemDef.recolorOriginal[0] = 926;
				itemDef.recolorModified[0] = PINK;
				itemDef.modelId = 2635;
				itemDef.zoom2d = 440;
				itemDef.xan2d = 76;
				itemDef.yan2d = 1850;
				itemDef.zan2d = 0;
				itemDef.xof2d = 1;
				itemDef.yof2d = 0;
				itemDef.manWear = 187;
				itemDef.womanWear = 363;
				itemDef.manWear2 = -1;
				itemDef.womanWear2 = -1;
				itemDef.manhead = -1;
				itemDef.womanhead = -1;
				itemDef.description = "A Pink Partyhat, the most popular item ingame".getBytes();
				return itemDef;
			
			case 23639:
				itemDef.setDefaults();
				itemDef.name = "TokHaar-Kal";
				itemDef.cost = 60000;
				itemDef.manWear = 62575;
				itemDef.womanWear = 62582;
				itemDef.options = new String[5];
				itemDef.options[2] = "Take";
				itemDef.modelId = 62592;
				itemDef.stackable = false;
				itemDef.description = "A cape made of ancient, enchanted rocks.".getBytes();
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.xof2d = -4;
				itemDef.yof2d = 0;
				itemDef.zoom2d = 1616;
				itemDef.xan2d = 339;
				itemDef.yan2d = 192;
				return itemDef;
				
			case 23659:
				itemDef.setDefaults();
				itemDef.name = "TokHaar-Kal";
				itemDef.cost = 60000;
				itemDef.manWear = 65527;
				itemDef.womanWear = 65528;
				itemDef.options = new String[5];
				itemDef.options[2] = "Take";
				itemDef.modelId = 65526;
				itemDef.stackable = false;
				itemDef.description = "A cape made of ancient, enchanted rocks.".getBytes();
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.xof2d = -4;
				itemDef.yof2d = 0;
				itemDef.zoom2d = 1616;
				itemDef.xan2d = 339;
				itemDef.yan2d = 192;
				itemDef.manwearzoff += 15;
				itemDef.womanwearzoff += 18;
				return itemDef;
				
			case 2996:
				itemDef.name = "Vote Ticket";
				itemDef.description = "A ticket used to purchase rewards for voting.".getBytes();
				return itemDef;
			
			case 15098:
				itemDef.name = "Dice (up to 100)";
				itemDef.zoom2d = 1104;
				itemDef.yan2d = 215;
				itemDef.xan2d = 94;
				itemDef.yof2d = 1;
				itemDef.options = new String[]{null, null, "Take", null,
						null};
				itemDef.inventoryOptions = new String[]{null, "Public-roll", "Switch-dice", null, "Drop"};
				itemDef.modelId = 47852;
				itemDef.ambient = 15;
				itemDef.contrast = 25;
				return itemDef;
			case 15088:
				itemDef.name = "Dice (2, 6 sides)";
				itemDef.zoom2d = 1104;
				itemDef.yan2d = 215;
				itemDef.xan2d = 94;
				itemDef.yof2d = 1;
				itemDef.options = new String[]{null, null, "Take", null,
						null};
				itemDef.inventoryOptions = new String[]{null, "Public-roll", "Switch-dice", null, "Drop"};
				itemDef.modelId = 47841;
				itemDef.ambient = 15;
				itemDef.contrast = 25;
				return itemDef;
			case 13263:
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.zoom2d = 789;
				itemDef.xan2d = 69;
				itemDef.yan2d = 1743;
				itemDef.xof2d = -4;
				itemDef.yof2d = -3;
				itemDef.modelId = 34411;
				itemDef.manWear = 6775; // male wield model
				itemDef.womanWear = 14112; // femArmModel
				itemDef.name = "Slayer helmet";
				itemDef.description = "You don't want to wear it inside-out."
						.getBytes();// examine.
				return itemDef;
			case 405:
				itemDef.name = "2.5m Cash";
				itemDef.description = "Opening this casket will give you 2,500,000 coins."
						.getBytes();
				return itemDef;
			
			case 761:
				itemDef.name = "Double Experience";
				itemDef.description = "Reading this will reward you will 2x experience for all skills. (1 hour)"
						.getBytes();
				return itemDef;
			
			case 607:
				itemDef.name = "Rare Drop Boost";
				itemDef.description = "Reading this will reward you will increase your rare drops by 20%. (1 Hour)"
						.getBytes();
				return itemDef;
			
			case 608:
				itemDef.name = "Pure PK Point boost";
				itemDef.description = "Reading this will reward you extra PK Points when killing your opponent in the wilderness. (1 Hour)"
						.getBytes();
				return itemDef;
				
			case 2403:
				itemDef.name = "Main PK Point boost";
				itemDef.inventoryOptions = new String[]{"Read", null, null, null, "Drop"};
				itemDef.description = "Reading this will reward you extra PK Points when killing your opponent in the wilderness. (1 Hour)"
						.getBytes();
				return itemDef;

			case 6950:
				itemDef.inventoryOptions = new String[]{"Cast", null, null, null, "Drop"};
				itemDef.name = "<col=ff00>Vengeance";
				itemDef.description = "Rebound damage to an opponent.".getBytes();
				return itemDef;
			
			case 12601:
				itemDef.name = "Ring of the gods";
				itemDef.zoom2d = 900;
				itemDef.xan2d = 393;
				itemDef.yan2d = 1589;
				itemDef.xof2d = -9;
				itemDef.yof2d = -12;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.modelId = 28824;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			case 12603:
				itemDef.name = "Tyrannical ring";
				itemDef.zoom2d = 592;
				itemDef.xan2d = 285;
				itemDef.yan2d = 1163;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.modelId = 28823;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 12605:
				itemDef.name = "Treasonous ring";
				itemDef.zoom2d = 750;
				itemDef.xan2d = 342;
				itemDef.yan2d = 252;
				itemDef.xof2d = -3;
				itemDef.yof2d = -12;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.modelId = 28825;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			case 12602:
				itemDef.name = "Ring of the gods (i)";
				itemDef.zoom2d = 900;
				itemDef.xan2d = 393;
				itemDef.yan2d = 1589;
				itemDef.xof2d = -9;
				itemDef.yof2d = -12;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.modelId = 28824;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			case 12604:
				itemDef.name = "Tyrannical ring (i)";
				itemDef.zoom2d = 592;
				itemDef.xan2d = 285;
				itemDef.yan2d = 1163;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.modelId = 28823;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			
			case 12606:
				itemDef.name = "Treasonous ring (i)";
				itemDef.zoom2d = 750;
				itemDef.xan2d = 342;
				itemDef.yan2d = 252;
				itemDef.xof2d = -3;
				itemDef.yof2d = -12;
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.modelId = 28825;
				itemDef.modelType = ModelTypeEnum.OSRS;
				return itemDef;
			case 2568:
				//itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions = new String[]{null, null, "Check charges", null, "Drop"};
				return itemDef;
			case 14001: // White
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (White)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = 127; // top
				itemDef.recolorModified[1] = 127; // top
				itemDef.recolorModified[2] = 127; // bottom
				itemDef.recolorModified[3] = 127; // bottom
				return itemDef;
			
			case 14002: // grey
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Grey)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = 10388; // top
				itemDef.recolorModified[1] = 10388; // top
				itemDef.recolorModified[2] = 10388; // bottom
				itemDef.recolorModified[3] = 10388; // bottom
				return itemDef;
			
			case 14003: // black
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Black)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = BLACK; // top
				itemDef.recolorModified[1] = BLACK; // top
				itemDef.recolorModified[2] = BLACK; // bottom
				itemDef.recolorModified[3] = BLACK; // bottom
				return itemDef;
			
			case 14004: // black white
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Black-White)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = WHITE; // top
				itemDef.recolorModified[1] = BLACK; // top
				itemDef.recolorModified[2] = BLACK; // bottom
				itemDef.recolorModified[3] = WHITE; // bottom
				return itemDef;
			
			case 14005: // Black Yellow
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Yellow-Black)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = YELLOW; // top
				itemDef.recolorModified[1] = BLACK; // top
				itemDef.recolorModified[2] = BLACK; // bottom
				itemDef.recolorModified[3] = YELLOW; // bottom
				return itemDef;
			
			case 14006: // cyan
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Cyan)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = CYAN; // top
				itemDef.recolorModified[1] = CYAN; // top
				itemDef.recolorModified[2] = CYAN; // bottom
				itemDef.recolorModified[3] = CYAN; // bottom
				return itemDef;
			
			case 14007: // red
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Red)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = RED; // top
				itemDef.recolorModified[1] = RED; // top
				itemDef.recolorModified[2] = RED; // bottom
				itemDef.recolorModified[3] = RED; // bottom
				return itemDef;
			
			case 14008: // purple
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Purple)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = PURPLE; // top
				itemDef.recolorModified[1] = PURPLE; // top
				itemDef.recolorModified[2] = PURPLE; // bottom
				itemDef.recolorModified[3] = PURPLE; // bottom
				return itemDef;
			
			case 14009: // yellow
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Yellow)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = YELLOW; // top
				itemDef.recolorModified[1] = YELLOW; // top
				itemDef.recolorModified[2] = YELLOW; // bottom
				itemDef.recolorModified[3] = YELLOW; // bottom
				return itemDef;
			
			case 14010: // orange
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Orange)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = ORANGE; // top
				itemDef.recolorModified[1] = ORANGE; // top
				itemDef.recolorModified[2] = ORANGE; // bottom
				itemDef.recolorModified[3] = ORANGE; // bottom
				return itemDef;
			
			case 14011: // yellow White
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Yellow-White)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = WHITE; // top
				itemDef.recolorModified[1] = YELLOW; // top
				itemDef.recolorModified[2] = YELLOW; // bottom
				itemDef.recolorModified[3] = WHITE; // bottom
				return itemDef;
			
			case 14012: // blue white
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Blue-White)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = WHITE; // top
				itemDef.recolorModified[1] = BLUE; // top
				itemDef.recolorModified[2] = BLUE; // bottom
				itemDef.recolorModified[3] = WHITE; // bottom
				return itemDef;
			
			case 14013: // blue black
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Blue-Black)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = BLUE; // top
				itemDef.recolorModified[1] = BLACK; // top
				itemDef.recolorModified[2] = BLACK; // bottom
				itemDef.recolorModified[3] = BLUE; // bottom
				return itemDef;
			
			case 14014: // red black
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Red-Black)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = RED; // top
				itemDef.recolorModified[1] = BLACK; // top
				itemDef.recolorModified[2] = BLACK; // bottom
				itemDef.recolorModified[3] = RED; // bottom
				return itemDef;
			
			case 14015: // red white
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Red-White)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = RED; // top
				itemDef.recolorModified[1] = WHITE; // top
				itemDef.recolorModified[2] = WHITE; // bottom
				itemDef.recolorModified[3] = RED; // bottom
				return itemDef;
			
			case 14016: // Green
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Green)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = GREEN; // top
				itemDef.recolorModified[1] = GREEN; // top
				itemDef.recolorModified[2] = GREEN; // bottom
				itemDef.recolorModified[3] = GREEN; // bottom
				return itemDef;
			
			case 14017: // Blue
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Blue)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = BLUE; // top
				itemDef.recolorModified[1] = BLUE; // top
				itemDef.recolorModified[2] = BLUE; // bottom
				itemDef.recolorModified[3] = BLUE; // bottom
				return itemDef;
			
			case 14018: // Pink
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Pink)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = PINK; // top
				itemDef.recolorModified[1] = PINK; // top
				itemDef.recolorModified[2] = PINK; // bottom
				itemDef.recolorModified[3] = PINK; // bottom
				return itemDef;
			
			case 14019: // Green black
//				itemDef.modelId = 65270;
//				itemDef.name = "Completionist cape (Green-Black)";
//				itemDef.zoom2d = 1316;
//				itemDef.xan2d = 252;
//				itemDef.yan2d = 1000;
//				itemDef.xof2d = -1;
//				itemDef.yof2d = 24;
//				itemDef.stackable = false;
//				itemDef.cost = 19264;
//				itemDef.members = true;
//				itemDef.manWear = 65297;
//				itemDef.womanWear = 65316;
//				itemDef.inventoryOptions = new String[5];
//				itemDef.inventoryOptions[1] = "Wear";
//				itemDef.inventoryOptions[2] = "Customise";
//				itemDef.inventoryOptions[3] = "Features";
//				itemDef.inventoryOptions[4] = "Destroy";
//				itemDef.recolorOriginal = new int[4];
//				itemDef.recolorModified = new int[4];
//				itemDef.recolorOriginal[0] = 65214;
//				itemDef.recolorOriginal[1] = 65200;
//				itemDef.recolorOriginal[2] = 65186;
//				itemDef.recolorOriginal[3] = 62995;
//				itemDef.recolorModified[0] = GREEN; // top
//				itemDef.recolorModified[1] = BLACK; // top
//				itemDef.recolorModified[2] = BLACK; // bottom
//				itemDef.recolorModified[3] = GREEN; // bottom
				return itemDef;
			
			case 14020: // Green White
//				itemDef.modelId = 65270;
//				itemDef.name = "Completionist cape (Green-White)";
//				itemDef.zoom2d = 1316;
//				itemDef.xan2d = 252;
//				itemDef.yan2d = 1000;
//				itemDef.xof2d = -1;
//				itemDef.yof2d = 24;
//				itemDef.stackable = false;
//				itemDef.cost = 19264;
//				itemDef.members = true;
//				itemDef.manWear = 65297;
//				itemDef.womanWear = 65316;
//				itemDef.inventoryOptions = new String[5];
//				itemDef.inventoryOptions[1] = "Wear";
//				itemDef.inventoryOptions[2] = "Customise";
//				itemDef.inventoryOptions[3] = "Features";
//				itemDef.inventoryOptions[4] = "Destroy";
//				itemDef.recolorOriginal = new int[4];
//				itemDef.recolorModified = new int[4];
//				itemDef.recolorOriginal[0] = 65214;
//				itemDef.recolorOriginal[1] = 65200;
//				itemDef.recolorOriginal[2] = 65186;
//				itemDef.recolorOriginal[3] = 62995;
//				itemDef.recolorModified[0] = GREEN; // top
//				itemDef.recolorModified[1] = WHITE; // top
//				itemDef.recolorModified[2] = WHITE; // bottom
//				itemDef.recolorModified[3] = GREEN; // bottom
				return itemDef;
			
			case 14021: // Purple Black
//				itemDef.modelId = 65270;
//				itemDef.name = "Completionist cape (Purple-Black)";
//				itemDef.zoom2d = 1316;
//				itemDef.xan2d = 252;
//				itemDef.yan2d = 1000;
//				itemDef.xof2d = -1;
//				itemDef.yof2d = 24;
//				itemDef.stackable = false;
//				itemDef.cost = 19264;
//				itemDef.members = true;
//				itemDef.manWear = 65297;
//				itemDef.womanWear = 65316;
//				itemDef.inventoryOptions = new String[5];
//				itemDef.inventoryOptions[1] = "Wear";
//				itemDef.inventoryOptions[2] = "Customise";
//				itemDef.inventoryOptions[3] = "Features";
//				itemDef.inventoryOptions[4] = "Destroy";
//				itemDef.recolorOriginal = new int[4];
//				itemDef.recolorModified = new int[4];
//				itemDef.recolorOriginal[0] = 65214;
//				itemDef.recolorOriginal[1] = 65200;
//				itemDef.recolorOriginal[2] = 65186;
//				itemDef.recolorOriginal[3] = 62995;
//				itemDef.recolorModified[0] = PURPLE; // top
//				itemDef.recolorModified[1] = BLACK; // top
//				itemDef.recolorModified[2] = BLACK; // bottom
//				itemDef.recolorModified[3] = PURPLE; // bottom
				return itemDef;
			
			case 14022: // Purple White
//				itemDef.modelId = 65270;
//				itemDef.name = "Completionist cape (Purple-White)";
//				itemDef.zoom2d = 1316;
//				itemDef.xan2d = 252;
//				itemDef.yan2d = 1000;
//				itemDef.xof2d = -1;
//				itemDef.yof2d = 24;
//				itemDef.stackable = false;
//				itemDef.cost = 19264;
//				itemDef.members = true;
//				itemDef.manWear = 65297;
//				itemDef.womanWear = 65316;
//				itemDef.inventoryOptions = new String[5];
//				itemDef.inventoryOptions[1] = "Wear";
//				itemDef.inventoryOptions[2] = "Customise";
//				itemDef.inventoryOptions[3] = "Features";
//				itemDef.inventoryOptions[4] = "Destroy";
//				itemDef.recolorOriginal = new int[4];
//				itemDef.recolorModified = new int[4];
//				itemDef.recolorOriginal[0] = 65214;
//				itemDef.recolorOriginal[1] = 65200;
//				itemDef.recolorOriginal[2] = 65186;
//				itemDef.recolorOriginal[3] = 62995;
//				itemDef.recolorModified[0] = PURPLE; // top
//				itemDef.recolorModified[1] = WHITE; // top
//				itemDef.recolorModified[2] = WHITE; // bottom
//				itemDef.recolorModified[3] = PURPLE; // bottom
				return itemDef;
			
			case 14023: // Brown
//				itemDef.modelId = 65270;
//				itemDef.name = "Completionist cape (Brown)";
//				itemDef.zoom2d = 1316;
//				itemDef.xan2d = 252;
//				itemDef.yan2d = 1000;
//				itemDef.xof2d = -1;
//				itemDef.yof2d = 24;
//				itemDef.stackable = false;
//				itemDef.cost = 19264;
//				itemDef.members = true;
//				itemDef.manWear = 65297;
//				itemDef.womanWear = 65316;
//				itemDef.inventoryOptions = new String[5];
//				itemDef.inventoryOptions[1] = "Wear";
//				itemDef.inventoryOptions[2] = "Customise";
//				itemDef.inventoryOptions[3] = "Features";
//				itemDef.inventoryOptions[4] = "Destroy";
//				itemDef.recolorOriginal = new int[4];
//				itemDef.recolorModified = new int[4];
//				itemDef.recolorOriginal[0] = 65214;
//				itemDef.recolorOriginal[1] = 65200;
//				itemDef.recolorOriginal[2] = 65186;
//				itemDef.recolorOriginal[3] = 62995;
//				itemDef.recolorModified[0] = BROWN; // top
//				itemDef.recolorModified[1] = BROWN; // top
//				itemDef.recolorModified[2] = BROWN; // bottom
//				itemDef.recolorModified[3] = BROWN; // bottom
				return itemDef;
			
			case 14024: // Brown White
				itemDef.modelId = 65270;
				itemDef.name = "Completionist cape (Brown-White)";
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 252;
				itemDef.yan2d = 1000;
				itemDef.xof2d = -1;
				itemDef.yof2d = 24;
				itemDef.stackable = false;
				itemDef.cost = 19264;
				itemDef.members = true;
				itemDef.manWear = 65297;
				itemDef.womanWear = 65316;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[2] = "Customise";
				itemDef.inventoryOptions[3] = "Features";
				itemDef.inventoryOptions[4] = "Destroy";
				itemDef.recolorOriginal = new int[4];
				itemDef.recolorModified = new int[4];
				itemDef.recolorOriginal[0] = 65214;
				itemDef.recolorOriginal[1] = 65200;
				itemDef.recolorOriginal[2] = 65186;
				itemDef.recolorOriginal[3] = 62995;
				itemDef.recolorModified[0] = BROWN; // top
				itemDef.recolorModified[1] = WHITE; // top
				itemDef.recolorModified[2] = WHITE; // bottom
				itemDef.recolorModified[3] = BROWN; // bottom
				return itemDef;
		}
		
		ItemDefExt.forId(itemDef);
		
		return itemDef;
	}
	
	void toNote() {
		ItemDef itemDef = forID(certTemplateID);
		modelId = itemDef.modelId;
		zoom2d = itemDef.zoom2d;
		xan2d = itemDef.xan2d;
		yan2d = itemDef.yan2d;
		
		zan2d = itemDef.zan2d;
		xof2d = itemDef.xof2d;
		yof2d = itemDef.yof2d;
		recolorOriginal = itemDef.recolorOriginal;
		recolorModified = itemDef.recolorModified;
		if (noteId != -1) {
			ItemDef itemDef_1 = forID(noteId);
			name = itemDef_1.name;
			members = itemDef_1.members;
			cost = itemDef_1.cost;
			String s = "a";
			char c = itemDef_1.name.charAt(0);
			if (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U')
				s = "an";
			description = ("Swap this note at any bank for " + s + " "
					+ itemDef_1.name + ".").getBytes();
		}
		stackable = true;
	}
	
	/*private void toLend() {
		com.soulplayps.client.ItemDef itemDef = forID(lentItemID);
		inventoryOptions = new String[5];
		modelId = itemDef.modelId;
		xof2d = itemDef.xof2d;
		yan2d = itemDef.yan2d;
		yof2d = itemDef.yof2d;
		zoom2d = itemDef.zoom2d;
		xan2d = itemDef.xan2d;
		zan2d = itemDef.zan2d;
		value = 0;
		com.soulplayps.client.ItemDef itemDef_1 = forID(lendID);
		anInt166 = itemDef_1.anInt166;
		modifiedModelColors = itemDef_1.modifiedModelColors;
		anInt185 = itemDef_1.anInt185;
		manWear2 = itemDef_1.manWear2;
		anInt173 = itemDef_1.anInt173;
		anInt175 = itemDef_1.anInt175;
		options = itemDef_1.options;
		manWear = itemDef_1.manWear;
		name = itemDef_1.name;
		womanWear = itemDef_1.womanWear;
		membersObject = itemDef_1.membersObject;
		anInt197 = itemDef_1.anInt197;
		womanWear2 = itemDef_1.womanWear2;
		anInt162 = itemDef_1.anInt162;
		originalModelColors = itemDef_1.originalModelColors;
		team = itemDef_1.team;
		if (itemDef_1.actions != null) {
			for (int i_33_ = 0; i_33_ < 4; i_33_++)
				actions[i_33_] = itemDef_1.actions[i_33_];
		}
		actions[4] = "Discard";
	}*/
	
	public static Sprite getSprite(int id, int count, int outline, int shadow, boolean shrink, boolean drawCount) {
		final int uid1 = (drawCount ? 65536 : 0) + id + (outline << 17) + (shadow << 19);
		final long uid = 3147483667L * count + 3849834839L * uid1;
		Sprite cachedSprite = (Sprite) itemSpriteCache.get(uid);

		if (cachedSprite != null) {
			return cachedSprite;
		}

		ItemDef itemDef = forID(id);
		if (count > 1 && itemDef.stackIDs != null) {
			int newItemId = -1;
			for (int j1 = 0; j1 < 10; j1++)
				if (count >= itemDef.stackAmounts[j1]
						&& itemDef.stackAmounts[j1] != 0)
					newItemId = itemDef.stackIDs[j1];
			
			if (newItemId != -1)
				itemDef = forID(newItemId);
		}
		Model model = itemDef.getItemModelFinalised(1);
		if (model == null)
			return null;
		Sprite sprite = null;
		if (itemDef.certTemplateID != -1) {
			sprite = getSprite(itemDef.noteId, 10, 1, 0, true, false);
			if (sprite == null)
				return null;
		}
		if (itemDef.lentItemID != -1) {
			sprite = getSprite(itemDef.lendID, count, outline, shadow, false, false);
			if (sprite == null)
				return null;
		}
		
		Sprite enabledSprite = new Sprite(36, 32);
		float[] depthBuffer = Rasterizer.depthBuffer;
		int k1 = Rasterizer.center_x;
		int l1 = Rasterizer.center_y;
		int ai[] = Rasterizer.lineOffsets;
		int ai1[] = Raster.pixels;
		int i2 = Raster.width;
		int j2 = Raster.height;
		int k2 = Raster.topX;
		int l2 = Raster.bottomX;
		int i3 = Raster.topY;
		int j3 = Raster.bottomY;
		Rasterizer.aBoolean1464 = false;
		Rasterizer.clearDepthBuffer();
		Raster.initDrawingArea(32, 36, enabledSprite.myPixels);
		Rasterizer.setDefaultBounds();
		Rasterizer.setViewport(16, 16);
		float k3 = itemDef.zoom2d;
		if (shrink)
			k3 *= 1.5f;
		if (outline == 2)
			k3 *= 1.04f;
		float l3 = Rasterizer.SINE_FLOAT[itemDef.xan2d] * k3;
		float i4 = Rasterizer.COSINE_FLOAT[itemDef.xan2d] * k3;
		model.method482(itemDef.yan2d, itemDef.zan2d,
				itemDef.xan2d, itemDef.xof2d, l3
						+ model.modelHeight / 2 + itemDef.yof2d, i4
						+ itemDef.yof2d, 0, false, false, false);
		
		if (outline >= 1) {
			enabledSprite.outline(1);
			if (outline >= 2) {
				enabledSprite.outline(16777215);
			}
			Raster.initDrawingArea(32, 36, enabledSprite.myPixels);
		}
		if (shadow != 0) {
			enabledSprite.shadow(shadow);
		}
		
		if (itemDef.certTemplateID != -1) {
			Sprite.disableHD = true;
			sprite.drawSprite(0, 0);
			Sprite.disableHD = false;
		} else if (itemDef.lentItemID != -1) {
			Raster.initDrawingArea(32, 36, sprite.myPixels);
			Sprite.disableHD = true;
			enabledSprite.drawSprite(0, 0);
			Sprite.disableHD = false;
			enabledSprite = sprite;
		}
		
		if (drawCount && (itemDef.stackable || count != 1) && count != -1) {
			Client.newSmallFont.disableHD = true;
			Client.newSmallFont.drawBasicString(Client.formatObjCount(count), 0, 9, 16776960, 1);
			Client.newSmallFont.disableHD = false;
		}
		
		Raster.initDrawingArea(j2, i2, ai1);
		Raster.setDrawingArea(k2, i3, l2, j3);
		Rasterizer.center_x = k1;
		Rasterizer.center_y = l1;
		Rasterizer.lineOffsets = ai;
		Rasterizer.aBoolean1464 = true;
		Rasterizer.depthBuffer = depthBuffer;

		if (enabledSprite != null) {
			itemSpriteCache.put(enabledSprite, uid);
		}

		return enabledSprite;
	}
	
	public Model getItemModelFinalised(int i) { //method201 //getItemModelFinalised
		if (stackIDs != null && i > 1) {
			int j = -1;
			for (int k = 0; k < 10; k++)
				if (i >= stackAmounts[k] && stackAmounts[k] != 0)
					j = stackIDs[k];
			
			if (j != -1)
				return forID(j).getItemModelFinalised(1);
		}
		Model model;
//		synchronized(mruNodes2) {
			model = (Model) mruNodes2.get(id);
//		}

		if (model != null)
			return model;
		model = Model.loadDropModel(modelId, modelType);
		if (model == null)
			return null;
		if (scaleX != 128 || scaleY != 128 || scaleZ != 128)
			model.method478(scaleX, scaleZ, scaleY);
		if (recolorOriginal != null) {
			for (int l = 0; l < recolorOriginal.length; l++)
				model.replaceHs1(recolorOriginal[l], recolorModified[l]);
			
		}
		model.calculateLighting(64 + ambient, 768 + contrast, -50, -10, -50, true);
		model.aBoolean1659 = true;
//		synchronized(mruNodes2) {
			mruNodes2.put(model, id);
//		}
		return model;
	}
	
	public Model method202(int i) {
		if (stackIDs != null && i > 1) {
			int j = -1;
			for (int k = 0; k < 10; k++)
				if (i >= stackAmounts[k] && stackAmounts[k] != 0)
					j = stackIDs[k];
			
			if (j != -1)
				return forID(j).method202(1);
		}
		Model model = Model.loadDropModel(modelId, modelType);
		if (model == null)
			return null;
		if (recolorOriginal != null) {
			for (int l = 0; l < recolorOriginal.length; l++)
				model.replaceHs1(recolorOriginal[l], recolorModified[l]);
			
		}
		return model;
	}
	
	private void readValues(RSBuffer stream) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				return;
			if (i == 1)
				modelId = stream.readUnsignedWord();
			else if (i == 2)
				name = stream.readNewString();
			else if (i == 3)
				description = stream.readBytes();
			else if (i == 4)
				zoom2d = stream.readUnsignedWord();
			else if (i == 5)
				xan2d = stream.readUnsignedWord();
			else if (i == 6)
				yan2d = stream.readUnsignedWord();
			else if (i == 7) {
				xof2d = stream.readUnsignedWord();
				if (xof2d > 32767)
					xof2d -= 0x10000;
			} else if (i == 8) {
				yof2d = stream.readUnsignedWord();
				if (yof2d > 32767)
					yof2d -= 0x10000;
			} else if (i == 10)
				stream.readUnsignedWord();
			else if (i == 11)
				stackable = true;
			else if (i == 12)
				cost = stream.readDWord();
			else if (i == 16)
				members = true;
			else if (i == 23) {
				manWear = stream.readUnsignedWord();
				manwearyoff = stream.readSignedByte();
			} else if (i == 24)
				manWear2 = stream.readUnsignedWord();
			else if (i == 25) {
				womanWear = stream.readUnsignedWord();
				womanwearyoff = stream.readSignedByte();
			} else if (i == 26)
				womanWear2 = stream.readUnsignedWord();
			else if (i >= 30 && i < 35) {
				if (options == null)
					options = new String[5];
				options[i - 30] = stream.readNewString();
				if (options[i - 30].equalsIgnoreCase("hidden"))
					options[i - 30] = null;
			} else if (i >= 35 && i < 40) {
				if (inventoryOptions == null)
					inventoryOptions = new String[5];
				inventoryOptions[i - 35] = stream.readNewString();
			} else if (i == 40) {
				int j = stream.readUnsignedByte();
				recolorOriginal = new int[j];
				recolorModified = new int[j];
				for (int k = 0; k < j; k++) {
					recolorOriginal[k] = stream.readUnsignedWord();
					recolorModified[k] = stream.readUnsignedWord();
				}
			} else if (i == 78)
				manWear3 = stream.readUnsignedWord();
			else if (i == 79)
				womanWear3 = stream.readUnsignedWord();
			else if (i == 90)
				manhead = stream.readUnsignedWord();
			else if (i == 91)
				womanhead = stream.readUnsignedWord();
			else if (i == 92)
				manhead2 = stream.readUnsignedWord();
			else if (i == 93)
				womanhead2 = stream.readUnsignedWord();
			else if (i == 95)
				zan2d = stream.readUnsignedWord();
			else if (i == 97)
				noteId = stream.readUnsignedWord();
			else if (i == 98)
				certTemplateID = stream.readUnsignedWord();
			else if (i >= 100 && i < 110) {
				if (stackIDs == null) {
					stackIDs = new int[10];
					stackAmounts = new int[10];
				}
				stackIDs[i - 100] = stream.readUnsignedWord();
				stackAmounts[i - 100] = stream.readUnsignedWord();
			} else if (i == 110)
				scaleX = stream.readUnsignedWord();
			else if (i == 111)
				scaleY = stream.readUnsignedWord();
			else if (i == 112)
				scaleZ = stream.readUnsignedWord();
			else if (i == 113)
				ambient = stream.readSignedByte();
			else if (i == 114)
				contrast = stream.readSignedByte() * 5;
			else if (i == 115)
				team = stream.readUnsignedByte();
		} while (true);
	}
	
	private void readValues667(RSBuffer stream, boolean osrs) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0) {
				return;
			}

			switch (i) {
				case 1:
					modelId = stream.readUnsignedWord();
					break;
				case 2:
					name = stream.readString();
					break;
				case 3:
					description = stream.readBytes();
					break;
				case 4:
					zoom2d = stream.readUnsignedWord();
					break;
				case 5:
					xan2d = stream.readUnsignedWord();
					break;
				case 6:
					yan2d = stream.readUnsignedWord();
					break;
				case 7:
					xof2d = stream.readUnsignedWord();
					if (xof2d > 32767)
						xof2d -= 0x10000;
					break;
				case 8:
					yof2d = stream.readUnsignedWord();
					if (yof2d > 32767)
						yof2d -= 0x10000;
					break;
				case 10:
					stream.readUnsignedWord();
					break;
				case 11:
					stackable = true;
					break;
				case 12:
					cost = stream.readDWord();
					break;
				case 16:
					members = true;
					break;
				case 23:
					manWear = stream.readUnsignedWord();
					manwearyoff = stream.readSignedByte();
					break;
				case 24:
					manWear2 = stream.readUnsignedWord();
					break;
				case 25:
					womanWear = stream.readUnsignedWord();
					womanwearyoff = stream.readSignedByte();
					break;
				case 26:
					womanWear2 = stream.readUnsignedWord();
					break;
				case 30:
				case 31:
				case 32:
				case 33:
				case 34:
					if (options == null) {
						options = new String[5];
					}
	
					int id = i - 30;
					options[id] = stream.readString();
					if (options[id] != null && "hidden".equalsIgnoreCase(options[id]))
						options[id] = null;
					break;
				case 35:
				case 36:
				case 37:
				case 38:
				case 39:
					if (inventoryOptions == null) {
						inventoryOptions = new String[5];
					}
	
					inventoryOptions[i - 35] = stream.readString();
					break;
				case 40:
					int j = stream.readUnsignedByte();
					recolorOriginal = new int[j];
					recolorModified = new int[j];
					for (int k = 0; k < j; k++) {
						recolorOriginal[k] = stream.readUnsignedWord();
						recolorModified[k] = stream.readUnsignedWord();
					}
					break;
				case 78:
					manWear3 = stream.readUnsignedWord();
					break;
				case 79:
					womanWear3 = stream.readUnsignedWord();
					break;
				case 90:
					manhead = stream.readUnsignedWord();
					break;
				case 91:
					womanhead = stream.readUnsignedWord();
					break;
				case 92:
					manhead2 = stream.readUnsignedWord();
					break;
				case 93:
					womanhead2 = stream.readUnsignedWord();
					break;
				case 95:
					zan2d = stream.readUnsignedWord();
					break;
				case 97:
					noteId = stream.readUnsignedWord();
					if (osrs && noteId >= 0) {
						noteId += OSRS_OFFSET;
					}
					break;
				case 98:
					certTemplateID = stream.readUnsignedWord();
					if (osrs && certTemplateID >= 0) {
						certTemplateID += OSRS_OFFSET;
					}
					break;
				case 100:
				case 101:
				case 102:
				case 103:
				case 104:
				case 105:
				case 106:
				case 107:
				case 108:
				case 109: {
					if (stackIDs == null) {
						stackIDs = new int[10];
						stackAmounts = new int[10];
					}

					int index = i - 100;
					stackIDs[index] = stream.readUnsignedWord();
					stackAmounts[index] = stream.readUnsignedWord();
					if (osrs && stackIDs[index] >= 0) {
						stackIDs[index] += OSRS_OFFSET;
					}
					break;
				}
				case 110:
					scaleX = stream.readUnsignedWord();
					break;
				case 111:
					scaleY = stream.readUnsignedWord();
					break;
				case 112:
					scaleZ = stream.readUnsignedWord();
					break;
				case 113:
					ambient = stream.readSignedByte();
					break;
				case 114:
					contrast = stream.readSignedByte() * 5;
					break;
				case 115:
					team = stream.readUnsignedByte();
					break;
				case 121:
					lendID = stream.readUnsignedWord();
					break;
				case 122:
					lentItemID = stream.readUnsignedWord();
					break;
					
				//181 osrs decode
				case 139: // unnoted id?
					stream.readUnsignedWord();
					break;
					
				case 140: // noted id?
					stream.readUnsignedWord();
					break;
					
				case 148://placeholder id
					stream.readUnsignedWord();
					break;
				
				case 149: // placeholder template
					stream.readUnsignedWord();
					break;
					
			}
		} while (true);
	}
	
	private ItemDef() {
		id = -1;
	}
	
	public static final int BLACK = 1;
	public static final int WHITE = 127;
	public static final int YELLOW = 8128;
	public static final int BLUE = 43848;
	public static final int RED = 926;
	public static final int CYAN = 34503;
	public static final int GREY = 50;
	private static final int ORANGE = 3016;
	private static final int PURPLE = 51136;
	private static final int GREEN = 22428;
	private static final int PINK = 123770;
	private static final int BROWN = 266770;

	 byte womanwearxoff;
	byte womanwearyoff;
	byte womanwearzoff;
	public int cost;
	public int[] recolorOriginal;
	public int id;
	public static ObjectCache mruNodes2 = new ObjectCache(50);
	public static ObjectCache itemSpriteCache = new ObjectCache(100);
	public int[] recolorModified;
	public boolean members;
	private int womanWear3;
	public int certTemplateID;
	public int womanWear2;
	public int manWear;
	private int manhead2;
	private int scaleX;
	public String[] options;
	public int xof2d;
	public String name = "null";
	private int womanhead2;
	public int modelId;
	public int manhead;
	public boolean stackable;
	public byte[] description;
	public int noteId;
	public int zoom2d;
	public static boolean isMembers = true;
	private static RSBuffer stream;
	public int contrast;
	private int manWear3;
	public int manWear2;
	public String[] inventoryOptions;
	public int xan2d;
	private int scaleZ;
	private int scaleY;
	public int[] stackIDs;
	public int yof2d;
	private static int[] streamIndices;
	public int ambient;
	public int womanhead;
	public int yan2d;
	public int womanWear;
	public int[] stackAmounts;
	public int team;
	public static int totalItems;
	public int zan2d;
	byte manwearxoff;
	byte manwearyoff;
	byte manwearzoff;
	public int lendID;
	public int lentItemID;
	public ModelTypeEnum modelType = ModelTypeEnum.REGULAR;
	public boolean excludeFromSearch;
	public static ObjectCache recentUse = new ObjectCache(64);
	
	private static RSBuffer streamOsrs;
	private static int[] streamIndicesOsrs;
	public static int totalOsrsItems;
	public static final int OSRS_OFFSET = 100_000;
	public static Set<Integer> availableOsrsItems = new HashSet<>();

}