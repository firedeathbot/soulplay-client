package com.soulplayps.client.node.animable.model;

public class ModelCalculations
{
	public static float clampUV(float uv)
	{
		if (uv >= 0f && uv <= 1f)
			return uv;
		
		while (uv < 0)
			uv++;
		
		while (uv > 1)
			uv--;
		
		/*float nuv = uv;
		if (uv > 1)
		{
			//int min = (int) uv;
			uv -= 1;
		}
		else if (uv < 0)
		{
			uv += 1;
			
			//return uv;
			uv *= -1;
			int plus = (int) uv;
			plus *= -1;
			if (plus <= 0)
				plus = 1;
			
			System.out.println(uv+" plus "+plus);
			uv += plus;
		}

		System.out.println("uv: "+nuv+" --> "+uv);*/
		return uv;
	}
	
	public static float roundUV(float uv)
	{
		if (uv >= 0f && uv <= 1f)
			return uv;
		
		if (uv < 0)
			uv *= -1;

		float uv2 = (int) uv;
		
		return (uv - uv2);
	}
	
	
	   public static int method1337(float argument, float argument_2_,
	            float argument_3_, int argument_4_)
	     {
	         float f = argument < 0.0F ? -argument : argument;
	         float f_5_ = argument_2_ < 0.0F ? -argument_2_ : argument_2_;
	         float f_6_ = !(argument_3_ < 0.0F) ? argument_3_ : -argument_3_;
	         if (!(f < f_5_) || !(f_5_ > f_6_))
	         {
	             if (f_6_ > f && f_5_ < f_6_)
	             {
	                 if (argument_3_ > 0.0F)
	                     return 2;
	                 return 3;
	             }
	             if (!(argument > 0.0F))
	                 return 5;
	             return 4;
	         }
	         if (argument_2_ > 0.0F)
	             return 0;
	         return 1;
	     }
		
	    public static float[] method1689(int argument, float[] argument_0_, int argument_1_, int argument_2_,
	            int argument_3_, float g, int argument_5_, int argument_6_,
	            float[] argument_7_, float argument_8_, float h,
	            float i, float argument_11_, float argument_12_)
	        {
	            i -= argument_2_;
	            h -= argument_3_;
	            g -= argument_5_;
	            float f = (argument_7_[2] * (float)h
	                   + (argument_7_[0] * (float)g
	                      + argument_7_[1] * (float)i));
	            float f_13_ = (argument_7_[5] * (float)h
	                       + (argument_7_[4] * (float)i
	                      + argument_7_[3] * (float)g));
	            float f_14_ = (argument_7_[7] * (float)i
	                       + (float)g * argument_7_[6]
	                       + (float)h * argument_7_[8]);
	            float f_15_;
	            float f_16_;
	            if (argument != 0)
	            {
	                if (argument != 1)
	                {
	                    if (argument == 2)
	                    {
	                        f_16_ = argument_11_ + -f + 0.5F;
	                        f_15_ = argument_12_ + -f_13_ + 0.5F;
	                    }
	                    else if (argument == 3)
	                    {
	                        f_15_ = argument_12_ + -f_13_ + 0.5F;
	                        f_16_ = f + argument_11_ + 0.5F;
	                    }
	                    else if (argument == 4)
	                    {
	                        f_16_ = argument_8_ + f_14_ + 0.5F;
	                        f_15_ = -f_13_ + argument_12_ + 0.5F;
	                    }
	                    else
	                    {
	                        f_16_ = argument_8_ + -f_14_ + 0.5F;
	                        f_15_ = argument_12_ + -f_13_ + 0.5F;
	                    }
	                }
	                else
	                {
	                    f_16_ = f + argument_11_ + 0.5F;
	                    f_15_ = argument_8_ + f_14_ + 0.5F;
	                }
	            }
	            else
	            {
	                f_15_ = argument_8_ + -f_14_ + 0.5F;
	                f_16_ = argument_11_ + f + 0.5F;
	            }
	            if (argument_1_ == 1)
	            {
	                float f_17_ = f_16_;
	                f_16_ = -f_15_;
	                f_15_ = f_17_;
	            }
	            else if (argument_1_ == 2)
	            {
	                f_15_ = -f_15_;
	                f_16_ = -f_16_;
	            }
	            else if (argument_1_ == 3)
	            {
	                float f_18_ = f_16_;
	                f_16_ = f_15_;
	                f_15_ = -f_18_;
	            }
	            argument_0_[1] = f_15_;
	            argument_0_[argument_6_] = f_16_;
	            return argument_0_;
	        }
	
	    public static float[] method699(int argument, float[] argument_1_, int lifespanY, float argument_3_,
	            int argument_4_, float g, float argument_6_, int argument_7_,
	            float h, float[] argument_9_, float i,
	            int argument_11_)
	        {
	            g -= argument_4_;
	            h -= argument_7_;
	            i -= argument;
	            float f = (argument_1_[0] * (float)i
	                   + argument_1_[1] * (float)g
	                   + argument_1_[2] * (float)h);
	            float f_12_ = (argument_1_[5] * (float)h
	                       + ((float)i * argument_1_[3]
	                      + argument_1_[4] * (float)g));
	            float f_13_ = ((float)h * argument_1_[8]
	                       + ((float)i * argument_1_[6]
	                      + (float)g * argument_1_[7]));
	            float f_14_
	                = ((float)Math.atan2((double)f, (double)f_13_) / 6.2831855F
	                   + 0.5F);
	            if (argument_6_ != 1.0F)
	                f_14_ *= argument_6_;
	            float f_15_ = f_12_ + 0.5F + argument_3_;
	            if (lifespanY != 1)
	            {
	                if (lifespanY == 2)
	                {
	                    f_15_ = -f_15_;
	                    f_14_ = -f_14_;
	                }
	                else if (lifespanY == 3)
	                {
	                    float f_16_ = f_14_;
	                    f_14_ = f_15_;
	                    f_15_ = -f_16_;
	                }
	            }
	            else
	            {
	                float f_17_ = f_14_;
	                f_14_ = -f_15_;
	                f_15_ = f_17_;
	            }
	            argument_9_[0] = f_14_;
	            argument_9_[1] = f_15_;

	            return argument_9_;
	        }
	
	    public static float[] method226(float[] argument, int argument_34_, float g,
	            int argument_36_, float h, float i,
	            float[] argument_39_, int argument_40_, boolean argument_41_,
	            int argument_42_, float argument_43_)
	        {
	            i -= argument_36_;
	            h -= argument_40_;
	            g -= argument_34_;
	            float f = ((float)h * argument_39_[2]
	                   + ((float)g * argument_39_[0]
	                      + argument_39_[1] * (float)i));
	            float f_44_ = (argument_39_[5] * (float)h
	                       + ((float)g * argument_39_[3]
	                      + argument_39_[4] * (float)i));
	            float f_45_ = (argument_39_[8] * (float)h
	                       + ((float)i * argument_39_[7]
	                      + argument_39_[6] * (float)g));
	            float f_46_ = (float)Math.sqrt((double)(f_44_ * f_44_ + f * f
	                                  + f_45_ * f_45_));
	            float f_47_
	                = ((float)Math.atan2((double)f, (double)f_45_) / 6.2831855F
	                   + 0.5F);
	            float f_48_
	                = (argument_43_
	                   + ((float)Math.asin((double)(f_44_ / f_46_)) / 3.1415927F
	                  + 0.5F));
	            if (argument_42_ == 1)
	            {
	                float f_49_ = f_47_;
	                f_47_ = -f_48_;
	                f_48_ = f_49_;
	            }
	            else if (argument_42_ != 2)
	            {
	                if (argument_42_ == 3)
	                {
	                    float f_50_ = f_47_;
	                    f_47_ = f_48_;
	                    f_48_ = -f_50_;
	                }
	            }
	            else
	            {
	                f_48_ = -f_48_;
	                f_47_ = -f_47_;
	            }
	            argument[0] = f_47_;
	            argument[1] = f_48_;
	            return argument;
	        }
	    
}
