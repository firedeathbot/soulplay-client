package com.soulplayps.client.node.animable.entity;

import com.soulplayps.client.Client;
import com.soulplayps.client.ErrorHandler;
import com.soulplayps.client.util.TextClass;
import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.animable.item.CustomizedItem;
import com.soulplayps.client.node.animable.item.ItemDef;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.unpack.SpotAnim;
import com.soulplayps.client.unknown.*;

public final class Player extends Entity {

	@Override
	public void method443(int i, float j, float k, float l, float i1, int j1, int k1,
			int l1, long i2, boolean animated, int level, long gluid) {
		if (appearance == null) {
			return;
		}

		Animation seq = super.animId != -1 && this.animDelay == 0 ? Animation.anims[super.animId] : null;
		Animation baseSeq = super.baseAnimId != -1 && !aBoolean1699 && (super.baseAnimId != super.anInt1511 || seq == null) ? Animation.anims[super.baseAnimId] : null;
		Model model = appearance.method452(seq, baseSeq, super.animFrame, super.animNextFrame, super.animFrameDelay, super.baseAnimFrame, super.baseAnimNextFrame, super.baseAnimFrameDelay);
		if (model == null) {
			super.animated = false;
			return;
		}

		Model spotAnimModel = null;
		super.height = model.modelHeight;
		model.aBoolean1659 = true;
		if (!aBoolean1699) {
			if (super.spotAnimId != -1 && super.spotAnimFrame != -1) {
				SpotAnim spotAnim = SpotAnim.cache[super.spotAnimId];
				spotAnimModel = spotAnim.getModel(super.spotAnimFrame, super.spotAnimNextFrame, super.spotAnimFrameDelay);

				if(spotAnim.animation != null && Class36.animationlist[spotAnim.animation.frame2Ids[0] >> 16].length == 0) {
					spotAnimModel = null;
				}
			}
			if (aModel_1714 != null) {
				if (Client.loopCycle >= anInt1708)
					aModel_1714 = null;
				if (Client.loopCycle >= anInt1707 && Client.loopCycle < anInt1708) {
					Model model_1 = aModel_1714;
					model_1.translate(anInt1711 - super.x, anInt1712 - y,
							anInt1713 - super.z);
					if (super.turnDirection == 512) {
						model_1.rotate270();
					} else if (super.turnDirection == 1024) {
						model_1.rotate180();
					} else if (super.turnDirection == 1536)
						model_1.rotate90();
					Model aclass30_sub2_sub4_sub6s[] = { model, model_1 };
					model = new Model(aclass30_sub2_sub4_sub6s);
					if (super.turnDirection == 512)
						model_1.rotate90();
					else if (super.turnDirection == 1024) {
						model_1.rotate180();
					} else if (super.turnDirection == 1536) {
						model_1.rotate270();
					}

					model_1.translate(super.x - anInt1711, y - anInt1712,
							super.z - anInt1713);
				}
			}
		}

		if (playerClone != null && Client.loopCycle >= playerClone.getTime()) {
			playerClone = null;
		}

		super.animated = true;
		modelHeight = model.modelHeight;
		model.method443(i, j, k, l, i1, j1, k1, l1, i2, this.animated, level, gluid);
		if (spotAnimModel != null) {
			if (super.anInt1524 != 0) {
				spotAnimModel.translate(0, -super.anInt1524, 0);
			}

			spotAnimModel.method466();
			spotAnimModel.method443(i, j, k, l, i1, j1, k1, l1, i2, this.animated, level, gluid + 1);
		}
	}

	public void updatePlayer(RSBuffer stream) {
		team = 0;
		title = null;
		titleColor = 0;
		clanId = 0;
		clanTag = null;

		stream.currentOffset = 0;
		int bitPack = stream.readUnsignedByte();
		final int gender = bitPack & 0x1;
		boolean hasTitle = (bitPack & 0x2) != 0;
		boolean hasRecolors = (bitPack & 0x4) != 0;
		boolean inClan = (bitPack & 0x8) != 0;
		if (hasTitle) {
			title = stream.readString();
			titleColor = stream.readUnsignedByte();
		}

		if (inClan) {
			this.clanId = stream.readDWord();
			this.clanTag = stream.readString();
		}

		headIcon = stream.readSignedByte();
		skullIcon = stream.readSignedByte();

		int npcId = -1;
		final int[] equipment = new int[PlayerAppearance.EQUIPMENT_LENGTH];
		for (int index = 0; index < PlayerAppearance.EQUIPMENT_LENGTH; index++) {
			int k = stream.read3Bytes();
			if (k == 0) {
				equipment[index] = 0;
				continue;
			}

			equipment[index] = k;
			if (index == 0 && equipment[0] == 16777215) {
				npcId = stream.read3Bytes();
				break;
			}

			if (equipment[index] >= 512 && equipment[index] - 512 < ItemDef.totalItems) {
				int teamNum = ItemDef.forID(equipment[index] - 512).team;
				if (teamNum != 0) {
					team = teamNum;
					//System.out.println("Set team of " + name + " to team " + teamNum);
				}
			}
		}

		CustomizedItem[] customizedItems;
		if (hasRecolors) {
			customizedItems = new CustomizedItem[PlayerAppearance.EQUIPMENT_LENGTH];

			int mask = stream.readUnsignedWord();

			for (int i = 0; i < 12; i++) {
				if ((mask & 1 << i) != 0) {
					int itemId = stream.readUnsignedWord();
					customizedItems[i] = CustomizedItem.decode(ItemDef.forID(itemId), stream);
				}
			}
		}

		int[] colors = new int[PlayerAppearance.COLOR_LENGTH];
		for (int l = 0; l < PlayerAppearance.COLOR_LENGTH; l++) {
			int j1 = stream.readUnsignedByte();
			if (j1 < 0 || j1 >= Client.anIntArrayArray1003[l].length)
				j1 = 0;
			colors[l] = j1;
		}
		
		super.anInt1511 = stream.readUnsignedWord();
		if (super.anInt1511 == 65535)
			super.anInt1511 = -1;
		super.anInt1512 = stream.readUnsignedWord();
		if (super.anInt1512 == 65535)
			super.anInt1512 = -1;
		super.anInt1554 = stream.readUnsignedWord();
		if (super.anInt1554 == 65535)
			super.anInt1554 = -1;
		super.anInt1555 = stream.readUnsignedWord();
		if (super.anInt1555 == 65535)
			super.anInt1555 = -1;
		super.anInt1556 = stream.readUnsignedWord();
		if (super.anInt1556 == 65535)
			super.anInt1556 = -1;
		super.anInt1557 = stream.readUnsignedWord();
		if (super.anInt1557 == 65535)
			super.anInt1557 = -1;
		super.anInt1505 = stream.readUnsignedWord();
		if (super.anInt1505 == 65535)
			super.anInt1505 = -1;
		name = TextClass.fixName(TextClass.nameForLong(stream.readQWord()));
		if (this == Client.myPlayer) {
			ErrorHandler.errorOwner = name;
		}

		combatLevel = stream.readUnsignedByte();
		skill = stream.readUnsignedWord();

		if (appearance == null) {
			appearance = new PlayerAppearance();
		}

		appearance.init(npcId, equipment, colors, gender == 1);
	}

	@Override
	public boolean isVisible() {
		return appearance != null;
	}

	@Override
	public void setInteractingEntity(int index) {
		super.setInteractingEntity(index);
		if (playerClone != null) {
			playerClone.interactingEntity = interactingEntity;
		}
	}

	public Player() {
		aBoolean1699 = false;
	}

	public String title;
	public int titleColor;
	public String clanTag;
	public int clanId = 0;
	public boolean aBoolean1699;
	public int team;
	public String name;
	public int combatLevel;
	public int headIcon;
	public int skullIcon;
	public int hintIcon;
	public int anInt1707;
	public int anInt1708;
	public int y;
	public int anInt1711;
	public int anInt1712;
	public int anInt1713;
	public Model aModel_1714;
	public int anInt1719;
	public int anInt1720;
	public int anInt1721;
	public int anInt1722;
	int skill;
	public PlayerClone playerClone;
	public PlayerAppearance appearance;

}
