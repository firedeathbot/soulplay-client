package com.soulplayps.client.node.animable.entity;

import java.util.Arrays;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.node.ObjectCache;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.animable.model.ModelTypeEnum;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.unpack.VarBit;
import com.soulplayps.client.unknown.*;

public final class EntityDef {
	
	@Override
	public String toString() {
		return "EntityDef [anInt55=" + anInt55 + ", anInt57=" + anInt57 + ", anInt58=" + anInt58 + ", anInt59=" + anInt59 + ", combatLevel=" + combatLevel + ", name=" + name + ", actions=" + Arrays.toString(actions) + ", walkAnim=" + walkAnim + ", aByte68=" + aByte68 + ", originalModelColors=" + Arrays.toString(originalModelColors) + ", anIntArray73="
				+ Arrays.toString(anIntArray73) + ", headIcon=" + headIcon + ", modifiedModelColors=" + Arrays.toString(modifiedModelColors) + ", standAnim=" + standAnim + ", interfaceType=" + myId + ", anInt79=" + anInt79 + ", anInt83=" + anInt83 + ", aBoolean84=" + aBoolean84 + ", anInt85=" + anInt85 + ", anInt86=" + anInt86 + ", aBoolean87=" + aBoolean87 + ", childrenIDs="
				+ Arrays.toString(childrenIDs) + ", description=" + getDescription() + ", anInt91=" + anInt91 + ", sizeZ=" + sizeZ + ", anInt92=" + anInt92 + ", aBoolean93=" + renderPriority + ", modelIds=" + Arrays.toString(modelIds) + "]";
	}
	
	private String getDescription() {
		try {
			return description.toString();
		} catch (Exception ex) {
			return "null";
		}
	}

	public static EntityDef forIDOsrs(int osrsId, int newId) {
		EntityDef entityDef = (EntityDef) recentUse.get(newId);

		if (entityDef != null) {
			return entityDef;
		}

		entityDef = new EntityDef();
		streamOsrs.currentOffset = streamIndicesOsrs[osrsId];
		entityDef.myId = newId;
		entityDef.readValues667(streamOsrs);
		if (entityDef.standAnim != -1) {
			entityDef.standAnim += Animation.NEW_OSRS_ID_START;
		}
		if (entityDef.walkAnim != -1) {
			entityDef.walkAnim += Animation.NEW_OSRS_ID_START;
		}
		if (entityDef.anInt58 != -1) {
			entityDef.anInt58 += Animation.NEW_OSRS_ID_START;
		}
		if (entityDef.anInt83 != -1) {
			entityDef.anInt83 += Animation.NEW_OSRS_ID_START;
		}
		if (entityDef.anInt55 != -1) {
			entityDef.anInt55 += Animation.NEW_OSRS_ID_START;
		}
		entityDef.modelType = ModelTypeEnum.OSRS;
		recentUse.put(entityDef, newId);
		return entityDef;
	}

	public static EntityDef forIDOsrs(int id) {
		EntityDef entityDef = (EntityDef) recentUse.get(id);
		if (entityDef != null) {
			return entityDef;
		}

		entityDef = new EntityDef();
		entityDef.myId = id;

		int indexId = id - OSRS_OFFSET;
		if (indexId >= 0 && indexId < streamIndicesOsrs.length) {
			streamOsrs.currentOffset = streamIndicesOsrs[indexId];
			entityDef.readValues667(streamOsrs);
		}

		if (entityDef.standAnim != -1) {
			entityDef.standAnim += Animation.NEW_OSRS_ID_START;
		}

		if (entityDef.walkAnim != -1) {
			entityDef.walkAnim += Animation.NEW_OSRS_ID_START;
		}

		if (entityDef.anInt58 != -1) {
			entityDef.anInt58 += Animation.NEW_OSRS_ID_START;
		}

		if (entityDef.anInt83 != -1) {
			entityDef.anInt83 += Animation.NEW_OSRS_ID_START;
		}

		if (entityDef.anInt55 != -1) {
			entityDef.anInt55 += Animation.NEW_OSRS_ID_START;
		}

		switch (id) {
			case 107559:
				entityDef.name = "Dominion archer";
				entityDef.actions = new String[]{null, "Attack", null, null, null};
				entityDef.combatLevel = 168;
				break;
			case 106477: {
				entityDef.anInt91 = 70;
				entityDef.sizeZ = 70;
				entityDef.anInt86 = 70;
				entityDef.aByte68 = 1;
				break;
			}
		}

		entityDef.modelType = ModelTypeEnum.OSRS;
		recentUse.put(entityDef, id);
		return entityDef;
	}
	
	public static EntityDef forID634(int i) {
		if (i >= npcSize634) {
			//System.out.println("Loaded npc id "+i);
			return forID(i); // 667
		}
		
		EntityDef entityDef;
//		synchronized(recentUse) {
			entityDef = (EntityDef) recentUse.get(i);
//		}
		if (entityDef != null) {
			return entityDef;
		}

		entityDef = new EntityDef();
		stream.currentOffset = streamIndices[i];
		entityDef.myId = i;
		entityDef.readValues634(stream);
		
		if (entityDef.headIcon == 20) { // soulsplit icon id on real runescape
			entityDef.headIcon = 17; // our soulsplit icon id
		} else if (entityDef.headIcon == 19) { // wrath icon id on real runescape
			entityDef.headIcon = 16; // our wrath icon id
		} else if (entityDef.headIcon == 12) { // curse melee protect icon id on real runescape
			entityDef.headIcon = 9; // our curse melee protect icon id
		}
		
//		synchronized(recentUse) {
			recentUse.put(entityDef, i);
//		}
		return entityDef;
	}
	
	
	public static EntityDef forID(int i) {
		if (i >= OSRS_OFFSET) {
			return forIDOsrs(i);
		}

		switch (i) {
			case 4190: // chaos fanatic
				return forIDOsrs(6619, i);
			case 4189: // ent trunk
				return forIDOsrs(6595, i);
			case 4188: // ent
				return forIDOsrs(6594, i);
			case 4187: // boulder in godwars wild dungeon
				return forIDOsrs(6621, i);
			case 4186: // crazy archaeologist
				return forIDOsrs(6618, i);
			case 917: // jail guard
				return forIDOsrs(4276, i);
		}

		EntityDef def = null;
		if (i == 2240) {//GE NPC
			def = forIDOsrs(2148, i);
		}
		
		if (i == 2241) {//GE NPC
			def = forIDOsrs(2149, i);
		}
		
		
		if (i == 4176) { //vet'ion transform
			def = forIDOsrs(6612, i);
		}
		
		if (i == 4177) { //vet'ion jr
			def = forIDOsrs(5536, i);
		}
		if (i == 4178) { // vet'ion jr2
			def = forIDOsrs(5537, i);
		}
		if (i == 4179) { // scorpia's offspring
			def = forIDOsrs(5547, i);
		}
		if (i == 4180) { // venenatis pet
			def = forIDOsrs(5557, i);
		}
		if (i == 4181) { // callisto cub
			def = forIDOsrs(5558, i);
		}
		if (i == 4182) { // pet kraken
			def = forIDOsrs(6656, i);
		}
		if (i == 4183) { // great olm pet olmlet
			def = forIDOsrs(7519, i);
		}
		if (i == 4184) { // great olm pet olmlet
			def = forIDOsrs(7520, i);
		}
		if (i == 4185) { // zuk pet  TzRek-Zuk
			def = forIDOsrs(8011, i);
		}
		
		if (i == 7941/*Emblem trader*/ 
				|| i == 7663/*krystilia slayer*/
				|| i == 6607/*elder chaos druid*/
				|| i == 7314/*ring of nature*/
				|| i == 7315/*ring of coins*/) {
			def = forIDOsrs(i, i);
		}
		
		if (i == 7232) {//Bloodhound
			def = forIDOsrs(i, i);
		}

		if (i == 6766 || i == 6767 || i == 6768) {
			def = forIDOsrs(i, i);
		}
		
		if (i == 5000) {//LMS Lisa
			def = forIDOsrs(7317, i);
		}

		if (i == 7602) {
			def = forIDOsrs(7604, i);
		}

		if (i == 7603) {
			def = forIDOsrs(7605, i);
		}

		if (i == 7604) {
			def = forIDOsrs(7606, i);
		}

		if (i == 7540 || i == 7541 || i == 7542 || i == 7543 || i == 7544 || i ==7545) {
			def = forIDOsrs(i, i);
		}
		
		if (i == 8970) {//vorkath
			def = forIDOsrs(8061, i);
			def.aByte68 = 7;
		}
		
		if (i == 8971) {//sleeping vorkath
			def = forIDOsrs(8059, i);
			def.aByte68 = 7;
		}
		
		if (i == 8981) {//zombified spawn
			def = forIDOsrs(8063, i);
			def.aByte68 = 1;
		}
		
		if (i == 9000) {//great olm
			def = forIDOsrs(7554, i);
			def.aByte68 = 5;
			def.anInt86 = 128;
		}
		if (i == 8990) {//olm left claw
			def = forIDOsrs(7555, i);
			def.aByte68 = 5;
			def.anInt86 = 100;
			def.sizeZ = 40;
		}
		if (i == 8991) {//olm right hand
			def = forIDOsrs(7553, i);
			def.aByte68 = 5;
			def.anInt86 = 128;
			def.sizeZ = 40;
		}
		if (i == 9001) {//olm left claw
			def = forIDOsrs(7552, i);
			def.aByte68 = 5;
		}
		if (i == 9010) {//olm right hand
			def = forIDOsrs(7550, i);
			def.aByte68 = 5;
		}
		
		//inferno minigame npcs
		if (i == 9011) {//jal-nib
			def = forIDOsrs(7691, i);
			def.aByte68 = 1;
		}
		if (i == 9020) {//Jal-ImKot
			def = forIDOsrs(7697, i);
			def.aByte68 = 4;
		}
		if (i == 9021) {//jal-xil
			def = forIDOsrs(7698, i);
			def.aByte68 = 3;
			def.translateY = 3;
		}
		if (i == 9030) {//jal-zek
			def = forIDOsrs(7699, i);
			def.aByte68 = 4;
			def.translateY = 4;
		}
		if (i == 9031) {//yt-hurkot
			def = forIDOsrs(7701, i);
			def.aByte68 = 1;
		}
		if (i == 9040) {//JalTok-Jad
			def = forIDOsrs(7700, i);
			def.aByte68 = 5;
			def.translateY = 2;
		}
		if (i == 9041) {//TzKal-Zuk
			def = forIDOsrs(7706, i);
			def.aByte68 = 7;
		}
		if (i == 9062) {//Jal-MejJak	
			def = forIDOsrs(7708, i);
			def.aByte68 = 1;
		}
		if (i == 9063) {//Ancestral Glyph	
			def = forIDOsrs(7707, i);
			def.aByte68 = 3;
		}
		if (i == 9064) {//jal-xil
			def = forIDOsrs(7698, i);
			def.aByte68 = 3;
			def.translateY = 3;
		}
		if (i == 9065) {//jal-zek
			def = forIDOsrs(7699, i);
			def.aByte68 = 4;
			def.translateY = 4;
		}
		if (i == 9104) {//JalTok-Jad
			def = forIDOsrs(7700, i);
			def.aByte68 = 5;
			def.translateY = 2;
		}
		//kraken boss
		if (i == 8729) {//tentacle whirlpool
			def = forIDOsrs(493, i);
			def.translateY = 50;
		}
		if (i == 8732) {//boss whirlpool
			def = forIDOsrs(496, i);
			def.translateY = 50;
		}
		if (i == 8931) {//enormous tentacle
			def = forIDOsrs(5535, i);
		}
		if (i == 8980) {//kraken boss
			def = forIDOsrs(494, i);
		}

		if (def != null) {
			switch(i) {
                case 7540:
                case 7541:
                case 7542:
                case 7543:
                case 7544:
                case 7545:
                    def.translateY = 10;
                    break;
			}
			return def;
		}

		EntityDef entityDef;
//		synchronized(recentUse) {
			entityDef = (EntityDef) recentUse.get(i);
//		}
		if (entityDef != null) {
			return entityDef;
		}

		entityDef = new EntityDef();
		stream667.currentOffset = streamIndices667[i];
		entityDef.myId = i;
		entityDef.readValues667(stream667);
		
		if (entityDef.headIcon == 20) { // soulsplit icon id on real runescape
			entityDef.headIcon = 17; // our soulsplit icon id
		} else if (entityDef.headIcon == 19) { // wrath icon id on real runescape
			entityDef.headIcon = 16; // our wrath icon id
		} else if (entityDef.headIcon == 12) { // curse melee protect icon id on real runescape
			entityDef.headIcon = 9; // our curse melee protect icon id
		}

		/*if(i >= 13463 && i <= 13839) {
			return EntityDef667.forId1(i);
		} else
		if(i >= 13840 && i <= 14020) {
			return EntityDef667.forId2(i);
		} else
		if(i >= 14021 && i <= 14507) {
			return EntityDef667.forId3(i);
		} else
		if(i >= 14508 && i <= 15039) {
			return EntityDef667.forId4(i);
		} else
		if(i >= 15040 && i <= 15378) {
			return EntityDef667.forId5(i);
		} else
		if(i >= 15379) {
			return EntityDef667.forId6(i);
		}*/
		
//		if(entityDef.originalModelColors != null) {
//			for (int i22 = 0; i22 < entityDef.originalModelColors.length; i22++) {
//				if(entityDef.originalModelColors[i22] == 0) {
//					entityDef.originalModelColors[i22] = 1;
//				}
//			}
//		}
			
//		if (/*i != 6250 && i < 13000*/(entityDef.name != null &&(entityDef.name.contains("Black") || entityDef.name.contains("black")  || entityDef.name.contains("demon")))) { // If npcs have black triangles, add them to this if check
//			if (entityDef.anIntArray76 == null) {
//				entityDef.anIntArray76 = new int [1];
//				entityDef.anIntArray70 = new int [1];
//				entityDef.anIntArray76[0] = 0;
//				entityDef.anIntArray70[0] = 1;
//			}
//		}

		switch (i) { //NPCS CANNOT BE HIGHER THAN ID 13463 MUST BE ADDED IN forID667() function
		
		case 7666:
			entityDef.actions = new String[] {"Talk-to", null, "Trade", null, null};
			break;
		case 494:
			entityDef.actions = new String[] {"Talk-to", null, "Bank", "Collect", "Open presets"};
			break;
			
		case 5421: 
				entityDef.anInt91 = entityDef.anInt91 + 125;
				entityDef.sizeZ = entityDef.sizeZ + 125;
				entityDef.anInt86 = entityDef.anInt91 + 125;
			
			//anInt91, sizeZ, anInt86
			break;
			
		case 7134: 
			entityDef.anInt91 = entityDef.anInt91 + 125;
			entityDef.sizeZ = entityDef.sizeZ + 125;
			entityDef.anInt86 = entityDef.anInt91 + 125;
		
		//anInt91, sizeZ, anInt86
		break;
				
		case 13986:
			entityDef.name = "Dominion trader";
			entityDef.actions = new String[] {"Talk-to", null, "Trade", null, null};
			break;
        case 8441:
			entityDef.name = "Dominion mage";
			entityDef.actions = new String[]{null, "Attack", null, null, null};
			entityDef.combatLevel = 174;
			break;
			
		case 9310: // lola wut turned into queen black dragon qbd
			
			entityDef.setDefaults();
			entityDef.name = "Easter Bunny";
			entityDef.modelIds = new int[]{32970};
			entityDef.standAnim = 11501;
			entityDef.walkAnim = 11504;
			entityDef.actions = new String[]{null, "Attack", null, null, null};
			entityDef.anIntArray73 = new int[]{32814};
			entityDef.combatLevel = 1000;
			entityDef.aByte68 = 5;
			entityDef.anInt91 = entityDef.sizeZ = 528;
			entityDef.anInt86 = 528;
			
			//kbd backup
//			entityDef.name = "Queen Black Dragon";
//			entityDef.modelIds = new int[] { 70260, 69766 };
//			entityDef.actions = new String[] { null, "Attack", null, null, null };
//			entityDef.combatLevel = 900;
//			entityDef.aByte68 = 5;
//			entityDef.standAnim = 4001;
//			entityDef.walkAnim = 0;
//			//entityDef.anInt91 = entityDef.sizeZ = 128;
//			// entityDef.anInt86 = 128;
//			entityDef.anInt79 = 0;
//			entityDef.description = "Worms burrow through her rotting flesh.".getBytes();
			break;
			
			//bunny bet
		case 4447:
			entityDef.actions = new String[] {"Talk-to", null, "Pick-up", null, null};
			break;

		case 3606: //castlwars chinchompas to black chinchompa
			entityDef.name = "Black chinchompa";
			entityDef.description = "Just plain nasty.".getBytes();
			entityDef.walkAnim = 5181;
			entityDef.standAnim = 5182;
			entityDef.combatLevel = 2;
			entityDef.modelIds = new int[] { 19371};
			entityDef.modifiedModelColors = new int[] { 5169, 7343, 7335, 7339, 7343, 5165};
			entityDef.originalModelColors = new int[] { 20, 33, 12, 37, 45, 49};
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			break;
		
		case 3494: //flambeed
			entityDef.walkAnim += Animation.NEW_OSRS_ID_START;
			entityDef.standAnim += Animation.NEW_OSRS_ID_START;
			break;
		
		case 13281:
			entityDef.name = "Daily Task Trader";
			break;

		case 1866:
			entityDef.name = "LMS Point Store";
			break;
			
		case 6537:
			entityDef.actions = new String[] {"Talk-to", null, "Trade", null, null};
			break;
			
		case 8678:
			entityDef.actions = new String[] {"Talk-to", null, "View Rewards", null, null};
			break;

		case 1188:
			entityDef.actions = new String[] { "Talk-to", null, "Get-task", "Trade", "Rewards"};
			break;
		
		case 9710:		
		    entityDef.standAnim = 13412;
		    break;
		case 13450: // wrath nex changing it to protect from range
			entityDef.headIcon = 11;
			break;
		
		case 9948:
		case 9949:
		case 9950:
		case 9951:
		case 9952:
		case 9953:
		case 9954:
		case 9955:
		case 9956:
		case 9957:
		case 9958:
		case 9959:
		case 9960:
		case 9961:
		case 9962:
		case 9963:
		case 9964:
			entityDef.standAnim = 13712;
			return entityDef;
			
		
		case 11226:
			entityDef.actions[3] = "Bind-setup";
			return entityDef;
		
		case 1281:
			entityDef.name = "Taming Master";
			return entityDef;
		
		case 7150:
			entityDef.name = "Tortured gorilla";
			entityDef.description = "One of Glough's tortured experiments.".getBytes();
			entityDef.modelIds = new int[] { 60225};
			entityDef.combatLevel = 141;
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 64;
			entityDef.anInt91 = entityDef.sizeZ = 64;
			entityDef.aByte68 = 2;
			entityDef.walkAnim = 7233;
			entityDef.standAnim = 7230;
			return entityDef;
		
		case 7151:
			entityDef.name = "Demonic gorilla";
			entityDef.description = "A demonic gorilla".getBytes();
			entityDef.modelIds = new int[] { 31241/*60226*/};
			entityDef.combatLevel = 275;
			entityDef.originalModelColors = new int[] { 10266};
			entityDef.modifiedModelColors = new int[] { 28};
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 64;
			entityDef.anInt91 = entityDef.sizeZ = 64;
			entityDef.aByte68 = 2;
			entityDef.walkAnim = Animation.getOsrsAnimId(7233);
			entityDef.standAnim = Animation.getOsrsAnimId(7230);
			entityDef.modelType = ModelTypeEnum.OSRS;
			return entityDef;
			
		case 7152: // melee protect
			entityDef.name = "Demonic gorilla";
			entityDef.description = "A demonic gorilla".getBytes();
			entityDef.modelIds = new int[] { 31241/*60226*/};
			entityDef.combatLevel = 275;
			entityDef.originalModelColors = new int[] { 10266};
			entityDef.modifiedModelColors = new int[] { 28};
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 64;
			entityDef.anInt91 = entityDef.sizeZ = 64;
			entityDef.aByte68 = 2;
			entityDef.walkAnim = Animation.getOsrsAnimId(7233);
			entityDef.standAnim = Animation.getOsrsAnimId(7230);
			entityDef.headIcon = 0;
			entityDef.modelType = ModelTypeEnum.OSRS;
			return entityDef;
			
		case 7153: // range protect
			entityDef.name = "Demonic gorilla";
			entityDef.description = "A demonic gorilla".getBytes();
			entityDef.modelIds = new int[] {31241/* 60226*/};
			entityDef.combatLevel = 275;
			entityDef.originalModelColors = new int[] { 10266};
			entityDef.modifiedModelColors = new int[] { 28};
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 64;
			entityDef.anInt91 = entityDef.sizeZ = 64;
			entityDef.aByte68 = 2;
			entityDef.walkAnim = Animation.getOsrsAnimId(7233);
			entityDef.standAnim = Animation.getOsrsAnimId(7230);
			entityDef.headIcon = 1;
			entityDef.modelType = ModelTypeEnum.OSRS;
			return entityDef;
			
		case 7154: // magic protect
			entityDef.name = "Demonic gorilla";
			entityDef.description = "A demonic gorilla".getBytes();
			entityDef.modelIds = new int[] { 31241/*60226*/};
			entityDef.combatLevel = 275;
			entityDef.originalModelColors = new int[] { 10266};
			entityDef.modifiedModelColors = new int[] { 28};
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 64;
			entityDef.anInt91 = entityDef.sizeZ = 64;
			entityDef.aByte68 = 2;
			entityDef.walkAnim = Animation.getOsrsAnimId(7233);
			entityDef.standAnim = Animation.getOsrsAnimId(7230);
			entityDef.headIcon = 2;
			entityDef.modelType = ModelTypeEnum.OSRS;
			return entityDef;
		
		case 498:
			entityDef.name = "Smoke devil";
			entityDef.description = "Stay away from smoke, kids.".getBytes();
			entityDef.modelIds = new int[] { 60224};
			entityDef.combatLevel = 160;
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.walkAnim = 1828;
			entityDef.standAnim = 1829;
			entityDef.renderPriority = true;
		    return entityDef;
		
		case 499:
			entityDef.name = "Thermonuclear smoke devil";
			entityDef.description = "Woah.".getBytes();
			entityDef.modelIds = new int[] { 60224};
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.combatLevel = 301;
			entityDef.anInt86 = 280;
			entityDef.anInt91 = entityDef.sizeZ = 280;
			entityDef.aByte68 = 4;
			entityDef.walkAnim = 1828;
			entityDef.standAnim = 1829;
			entityDef.renderPriority = true;
		    return entityDef;
		
		case 2011:
			entityDef.name = "Lava dragon";
			entityDef.combatLevel = 252;
			entityDef.modelIds = new int[4];
			entityDef.modelIds[0] = 60067;
			entityDef.modelIds[1] = 60068;
			entityDef.modelIds[2] = 60069;
			entityDef.modelIds[3] = 60070; // large dragon shadow model
			entityDef.originalModelColors = new int[2];
			entityDef.modifiedModelColors = new int[2];
			entityDef.originalModelColors[0] = 8741;
			entityDef.originalModelColors[1] = 25238;
			entityDef.modifiedModelColors[0] = 94;
			entityDef.modifiedModelColors[1] = 94;
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.aByte68 = 5;
			entityDef.walkAnim = 4635;
			entityDef.standAnim = 90;
			entityDef.description = "It's dripping with molten lava.".getBytes();

			entityDef.anInt55 = -1;
			entityDef.anInt57 = -1;
			entityDef.anInt58 = -1;
			entityDef.anInt59 = -1;
			entityDef.headIcon = -1;
			entityDef.anInt79 = 32;
			entityDef.anInt83 = -1;
			entityDef.aBoolean84 = true;
			entityDef.anInt85 = 0;
			entityDef.anInt86 = 128;
			entityDef.aBoolean87 = true;
			entityDef.anInt91 = entityDef.sizeZ = 128;
			entityDef.renderPriority = false;
			entityDef.childrenIDs = null;
			
			break;
			
		case 2012:
			entityDef.setDefaults();
			entityDef.name = "Zulrah";
			entityDef.combatLevel = 725;
			entityDef.modelIds = new int[] { 60072 };
			entityDef.originalModelColors = new int[6];
			entityDef.originalModelColors[0] = 7756;
			entityDef.originalModelColors[1] = 7845;
			entityDef.originalModelColors[2] = 61;
			entityDef.originalModelColors[3] = 24;
			entityDef.originalModelColors[4] = 41;
			entityDef.originalModelColors[5] = 57;
			entityDef.modifiedModelColors = new int[6];
			entityDef.modifiedModelColors[0] = 274;
			entityDef.modifiedModelColors[1] = 1831;
			entityDef.modifiedModelColors[2] = 36133;
			entityDef.modifiedModelColors[3] = 36133;
			entityDef.modifiedModelColors[4] = 37270;
			entityDef.modifiedModelColors[5] = 36133;
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 128;
			entityDef.anInt91 = entityDef.sizeZ = 128;
			entityDef.renderPriority = true;
			entityDef.aByte68 = 5;
			entityDef.walkAnim = Animation.getOsrsAnimId(5070);
			entityDef.standAnim = Animation.getOsrsAnimId(5070);
			entityDef.description = "The turqoise hooded serpent of the poison waste.".getBytes();
			entityDef.childrenIDs = null;
			break;
			 
		case 2043:
			entityDef.name = "Zulrah";
			entityDef.combatLevel = 725;
			entityDef.modelIds = new int[] { 60073 };
			entityDef.originalModelColors = new int[6];
			entityDef.originalModelColors[0] = 7756;
			entityDef.originalModelColors[1] = 7845;
			entityDef.originalModelColors[2] = 61;
			entityDef.originalModelColors[3] = 24;
			entityDef.originalModelColors[4] = 41;
			entityDef.originalModelColors[5] = 57;
			entityDef.modifiedModelColors = new int[6];
			entityDef.modifiedModelColors[0] = 274;
			entityDef.modifiedModelColors[1] = 1831;
			entityDef.modifiedModelColors[2] = 36133;
			entityDef.modifiedModelColors[3] = 36133;
			entityDef.modifiedModelColors[4] = 37270;
			entityDef.modifiedModelColors[5] = 36133;
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 128;
			entityDef.anInt91 = entityDef.sizeZ = 128;
			entityDef.renderPriority = true;
			entityDef.aByte68 = 5;
			entityDef.walkAnim = Animation.getOsrsAnimId(5070);
			entityDef.standAnim = Animation.getOsrsAnimId(5070);
			entityDef.description = "The turqoise hooded serpent of the poison waste.".getBytes();
			entityDef.childrenIDs = null;
			break;
			 
		case 2044:
			entityDef.name = "Zulrah";
			entityDef.combatLevel = 725;
			entityDef.modelIds = new int[] { 60071 };
			entityDef.originalModelColors = new int[6];
			entityDef.originalModelColors[0] = 7756;
			entityDef.originalModelColors[1] = 7845;
			entityDef.originalModelColors[2] = 61;
			entityDef.originalModelColors[3] = 24;
			entityDef.originalModelColors[4] = 41;
			entityDef.originalModelColors[5] = 57;
			entityDef.modifiedModelColors = new int[6];
			entityDef.modifiedModelColors[0] = 274;
			entityDef.modifiedModelColors[1] = 1831;
			entityDef.modifiedModelColors[2] = 36133;
			entityDef.modifiedModelColors[3] = 36133;
			entityDef.modifiedModelColors[4] = 37270;
			entityDef.modifiedModelColors[5] = 36133;
			entityDef.actions = new String[5];
			entityDef.actions = new String[] { null, "Attack", null, null, null};
			entityDef.anInt86 = 128;
			entityDef.anInt91 = entityDef.sizeZ = 128;
			entityDef.renderPriority = true;
			entityDef.aByte68 = 5;
			entityDef.walkAnim = Animation.getOsrsAnimId(5070);
			entityDef.standAnim = Animation.getOsrsAnimId(5070);
			entityDef.description = "The turqoise hooded serpent of the poison waste.".getBytes();
			entityDef.childrenIDs = null;
			break;
			 
		case 2045:
			entityDef = forIDOsrs(2045, i);
			break;
		
		case 7605:
			entityDef.name = "Julius The Shop Manager";
			entityDef.actions[2] = "Open Market";
			entityDef.actions[3] = "Open your Own Shop";
		break;
		
		case 8948:
			entityDef.name = "Armour set exchange";
			entityDef.actions[2] = "Exchange armour set";
			entityDef.actions[3] = "Bank";
		break;
		
		case 8575:
			entityDef.name = "Igneel";
			entityDef.actions[0] = "Pick-up";
			entityDef.renderPriority = false;
		break;
		
		case 3222:
			entityDef.name = "Lahey";
			//entityDef.aByte68 = 3;
			entityDef.actions[0] = "Pick-up";
			entityDef.actions[1] = "Talk-to";
			entityDef.actions[2] = null;
	        entityDef.combatLevel = 0;
			//entityDef.anInt91 = entityDef.sizeZ = 190;
			//entityDef.anInt86 = 190;
			//entityDef.anInt85 = 20;
	        entityDef.renderPriority = false;
		break;
	   
		case 3996:
			entityDef.name = "Julius's pet";
			entityDef.modelIds = new int[]{27718};
			entityDef.aByte68 = 3;
			entityDef.standAnim = 7014;
			entityDef.walkAnim = 7015;
			entityDef.actions[0] = "Pick-up";
			entityDef.anInt91 = entityDef.sizeZ = 190;
			entityDef.anInt86 = 190;
			entityDef.anInt85 = 20;
			entityDef.renderPriority = false;
		break;
		
		case 3997:
			entityDef.name = "Baby Mole";
			entityDef.modelIds = new int[]{12076, 12075, 12074, 12077};
			entityDef.aByte68 = 1;
			entityDef.standAnim = 3309;
			entityDef.walkAnim = 3313;
			entityDef.actions[0] = "Pick-up";
			entityDef.anInt91 = entityDef.sizeZ = 40;
			entityDef.anInt86 = 40;
			entityDef.renderPriority = false;
		break;
		
		case 3998: 
            entityDef.actions = new String[5];
            entityDef.actions[0] = "Pick-up";
            entityDef.aBoolean84 = true;
            entityDef.modelIds = new int[] { 24602, 24605, 24606 };
            entityDef.walkAnim = 6236;
            entityDef.standAnim = 6236;
            entityDef.name = "Kalphite Princess";
            entityDef.anInt91 = entityDef.sizeZ = 40;
            entityDef.anInt86 = 40;
            entityDef.description = "desc!".getBytes();
            entityDef.aByte68 = 1;
            entityDef.renderPriority = false;
        break;
            
        case 3999: 
            entityDef.actions = new String[5];
            entityDef.actions[0] = "Pick-up";
            entityDef.aBoolean84 = true;
            entityDef.modelIds = new int[] { 24597, 24598 };
            entityDef.walkAnim = 6238;
            entityDef.standAnim = 6239;
            entityDef.name = "Kalphite Princess";
            entityDef.anInt91 = entityDef.sizeZ = 40;
            entityDef.anInt86 = 40;
            entityDef.description = "desc!".getBytes();
            entityDef.aByte68 = 1;
            entityDef.renderPriority = false;
        break;
    
		case 4000:
			entityDef.name = "Prince Black Dragon";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {17414, 17415, 17429, 17422};
			entityDef.standAnim = 90;
			entityDef.walkAnim = 4635;
			entityDef.anInt86 = 50;
			entityDef.anInt91 = entityDef.sizeZ = 50;
			entityDef.aByte68 = 1;
			entityDef.renderPriority = false;
		break;
		
		case 4001:
			entityDef.name = "General Graardor";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {27785, 27789};
			entityDef.standAnim = 7059;
			entityDef.walkAnim = 7058;
			entityDef.anInt86 = 35;
			entityDef.anInt91 = entityDef.sizeZ = 35;
			entityDef.renderPriority = false;
		break;	
		
		case 4002:
			entityDef.name = "TzRek-Jad";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {34131};
			entityDef.standAnim = 9274;
			entityDef.walkAnim = 9273;
			entityDef.anInt86 = 35;
			entityDef.anInt91 = entityDef.sizeZ = 35;
			entityDef.renderPriority = false;
		break;
		
		case 4003:
			entityDef.name = "Chaos Elemental";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {11216};
			entityDef.standAnim = 3144;
			entityDef.walkAnim = 3145;
			entityDef.anInt86 = 40;
			entityDef.anInt91 = entityDef.sizeZ = 40;
			entityDef.renderPriority = false;
		break;
		
		case 4004:// missing item
			entityDef.name = "Corporeal Beast";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {40955};
			entityDef.standAnim = 10056;
			entityDef.walkAnim = 10055;
			entityDef.anInt86 = 35;
			entityDef.anInt91 = entityDef.sizeZ = 35;
			entityDef.aByte68 = 1;
			entityDef.renderPriority = false;
		break;
		
		case 4005:
			entityDef.name = "Kree Arra";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {28003, 28004};
			entityDef.standAnim = 6972;
			entityDef.walkAnim = 6973;
			entityDef.anInt86 = 35;
			entityDef.anInt91 = entityDef.sizeZ = 35;
			entityDef.renderPriority = false;
		break;
		
		case 4006:
			entityDef.name = "K'ril Tsutsaroth";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {27768, 27773, 27764, 27765, 27770};
			entityDef.standAnim = 6943;
			entityDef.walkAnim = 6942;
			entityDef.anInt86 = 35;
			entityDef.anInt91 = entityDef.sizeZ = 35;
			entityDef.aByte68 = 1;
			entityDef.renderPriority = false;
		break;
		
		case 4007:
			entityDef.name = "Commander Zilyana";
			entityDef.modelIds = new int[]{55881};
			entityDef.aByte68 = 1;
			entityDef.standAnim = 6963;
			entityDef.walkAnim = 6962;
			entityDef.actions[0] = "Pick-up";
			entityDef.renderPriority = false;
			entityDef.anInt86 = 75;
			entityDef.anInt91 = entityDef.sizeZ = 75;
		break;
		
		case 4008:
			entityDef.name = "Dagannoth Supreme";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {9941, 9943};
			entityDef.standAnim = 2850;
			entityDef.walkAnim = 2849;
			entityDef.anInt86 = 70;
			entityDef.anInt91 = entityDef.sizeZ = 70;
			entityDef.renderPriority = false;
		break;
		
		case 4009:
			entityDef.name = "Dagannoth Prime"; //9940, 9943, 9942
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {9940, 9943, 9942};
			entityDef.modifiedModelColors = new int[]{11930, 27144, 16536, 16540};
			entityDef.originalModelColors = new int[]{5931, 1688, 21530, 21534};
			entityDef.standAnim = 2850;
			entityDef.walkAnim = 2849;
			entityDef.anInt86 = 70;
			entityDef.anInt91 = entityDef.sizeZ = 70;
			entityDef.renderPriority = false;
		break;
		
		case 4010:
			entityDef.name = "Dagannoth Rex";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {9941};
			entityDef.modifiedModelColors = new int[]{16536, 16540, 27144, 2477};
			entityDef.originalModelColors = new int[]{7322, 7326, 10403, 2595};
			entityDef.standAnim = 2850;
			entityDef.walkAnim = 2849;
			entityDef.anInt86 = 70;
			entityDef.anInt91 = entityDef.sizeZ = 70;
			entityDef.renderPriority = false;
		break;
		
		case 4011:
			entityDef.name = "Ahrim the Blighted";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {6668};
			entityDef.standAnim = 813;
			entityDef.walkAnim = 1205;
			entityDef.anInt86 = 100;
			entityDef.renderPriority = false;
			entityDef.anInt91 = entityDef.sizeZ = 100;
		break;
		
		case 4012:
			entityDef.name = "Dharok the Wretched";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {6652, 6671, 6640, 6661, 6703, 6679};
			entityDef.standAnim = 2065;
			entityDef.walkAnim = 2064;
			entityDef.anInt86 = 100;
			entityDef.anInt91 = entityDef.sizeZ = 100;
			entityDef.renderPriority = false;
		break;
		
		case 4013:
			entityDef.name = "Guthan the Infested";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {6654, 6673, 6642, 6666, 6679, 6710};
			entityDef.standAnim = 813;
			entityDef.walkAnim = 1205;
			entityDef.anInt86 = 100;
			entityDef.anInt91 = entityDef.sizeZ = 100;
			entityDef.renderPriority = false;
		break;
		
		case 4014:
			entityDef.name = "Karil the Tainted";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {6675};
			entityDef.standAnim = 2074;
			entityDef.walkAnim = 2076;
			entityDef.anInt86 = 100;
			entityDef.anInt91 = entityDef.sizeZ = 100;
			entityDef.renderPriority = false;
		break;
		
		case 4015:
			entityDef.name = "Torag the Corrupted";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Pick-up";
			entityDef.modelIds = new int[] {6657, 6677, 6645, 6663, 6708, 6679};
			entityDef.standAnim = 808;
			entityDef.walkAnim = 819;
			entityDef.anInt86 = 100;
			entityDef.anInt91 = entityDef.sizeZ = 100;
			entityDef.renderPriority = false;
		break;
		
		case 4016:
			entityDef.name = "Verac the Defiled";
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Talk to";
			entityDef.actions[1] = "Pick-up";
			entityDef.modelIds = new int[] {6678, 6705};
			entityDef.standAnim = 2061;
			entityDef.walkAnim = 2060;
			entityDef.anInt86 = 100;
			entityDef.anInt91 = entityDef.sizeZ = 100;
			entityDef.renderPriority = false;
		break;

		case 4174:
			entityDef = forIDOsrs(6503, i);
	        break;

		case 4172:
	    	entityDef = forIDOsrs(6615, i);
	        break;

	    case 4173:
	    	entityDef = forIDOsrs(6504, i);
	        break;

	    case 4175:
	    	entityDef = forIDOsrs(6611, i);
	        break;
	    case 4176:
	    	entityDef = forIDOsrs(6612, i);
	    	break;

		case 820:
			entityDef.name = "Spin Ticket Salesman";
			entityDef.description = "He sells spin the wheel tickets.".getBytes();
            break;
		
		case 3374:
			entityDef.name = "Max";
			String desc = "He's mastered the many skills of "+ Config.SERVER_NAME+".";
			entityDef.description = desc.getBytes();
			entityDef.combatLevel = 138;
			entityDef.actions = new String[5];
			entityDef.actions[0] = "Talk-to";
			entityDef.actions[2] = "Follow";
			entityDef.actions[3] = "Trade";
			entityDef.modelIds = new int[8];
			entityDef.modelIds[0] = 65291;
			entityDef.modelIds[1] = 62746;
			entityDef.modelIds[2] = 62743;
			entityDef.modelIds[3] = 65322;
			entityDef.modelIds[4] = 13319;
			entityDef.modelIds[5] = 27738;
			entityDef.modelIds[6] = 20147;
			entityDef.modelIds[7] = 11732;
			entityDef.anInt86 = 130;
			entityDef.anInt91 = entityDef.sizeZ = 130;
			break;
		
		case 303:
			entityDef.name = "Vote Manager";
			entityDef.actions[0] = "Open store";
			entityDef.actions[2] = "Open old store";
			break;
			
		case 644:
			entityDef.name = "Donate Manager";
			break;
		
		case 6090:
			entityDef.actions = new String[5];
			entityDef.actions[1] = "Attack";
			entityDef.modelIds = new int[1]; 
			entityDef.modelIds[0] = 63604;
			entityDef.standAnim = 12790;
			entityDef.walkAnim = 12790;
			entityDef.anInt86 = 200;
			entityDef.anInt91 = entityDef.sizeZ = 200;
			entityDef.aByte68 = 5;
			entityDef.name = "Wildywyrm";
			entityDef.combatLevel = 755;
			entityDef.description = "It's made of pure fire.".getBytes();
	        break;
	        
		case 6089:
			entityDef.actions = new String[5];
			entityDef.actions[1] = "Attack";
			entityDef.modelIds = new int[1]; 
			entityDef.modelIds[0] = 63604;
			entityDef.standAnim = 12790;
			entityDef.walkAnim = 12790;
			entityDef.anInt86 = 80;
			entityDef.anInt91 = entityDef.sizeZ = 80;
			entityDef.renderPriority = false;
			entityDef.aByte68 = 5;
			entityDef.name = "Baby Wildywyrm";
			entityDef.combatLevel = 112;
			entityDef.description = "It's made of pure fire.".getBytes();
	        break;

		case 5529: // yaks
		case 6873:
		case 6874:
		case 9491:
		case 9603:
		case 9604:
		case 9605:
		case 9606:
		    entityDef.walkAnim = 543;
		    break;
		case 181:
			entityDef.walkAnim = 819;
			break;
		case 1382:
		entityDef.name = "Glacor";
		entityDef.modelIds = new int[]{58940};
		entityDef.aByte68 = 3;
		entityDef.anInt86 = 200;
		entityDef.standAnim = 10867;
		entityDef.walkAnim = 10901;
		entityDef.actions = new String[]{null, "Attack", null, null, null};
		entityDef.combatLevel = 475;
		entityDef.anInt91 = entityDef.sizeZ = 123;
		entityDef.renderPriority = true;
		break;

		case 1383:
		entityDef.name = "Unstable glacyte";
		entityDef.modelIds = new int[]{58942};
		entityDef.standAnim = 10867;
		entityDef.walkAnim = 10901;
		entityDef.actions = new String[]{null, "Attack", null, null, null};
		entityDef.combatLevel = 101;
		entityDef.renderPriority = false;
	break;
		case 1384:
		entityDef.name = "Sapping glacyte";
		entityDef.modelIds = new int[]{58939};
		entityDef.standAnim = 10867;
		entityDef.walkAnim = 10901;
		entityDef.actions = new String[]{null, "Attack", null, null, null};
		entityDef.combatLevel = 101;
		entityDef.renderPriority = true;
	break;
		case 1385:
		entityDef.name = "Enduring glacyte";
		entityDef.modelIds = new int[]{58937};
		entityDef.standAnim = 10867;
		entityDef.walkAnim = 10901;
		entityDef.actions = new String[]{null, "Attack", null, null, null};
		entityDef.combatLevel = 101;
		entityDef.renderPriority = true;
	break;
	    case 945:
		entityDef.name = Config.SERVER_NAME+" Guide";
	break;
		//case 178:
			//entityDef.name = "Black Knight";
			//entityDef.anIntArray94 = new int[]{218, 306, 164, 179, 268, 185, 1109358339};
			//entityDef.standAnim = 808;
			//entityDef.walkAnim = 1065;
			//entityDef.actions = new String[]{null, "Attack", null, null, null};
			//entityDef.colour1 = new int[]{61, 10004, 24, 41, 57, 4626};
			//entityDef.colour2 = new int[]{8, 8, 8, 0, 8, 8};
		//	entityDef.anIntArray76 = new int[]{61, 10004, 24, 41, 57, 4626};
		//	entityDef.anIntArray70 = new int[]{8, 8, 8, 0, 8, 8};
			//entityDef.combatLevel = 33;
		//break;
	
		/*case 50:
			//entityDef.name = "King Black Dragon";
			//entityDef.anIntArray94 = new int[]{17414, 17415, 17429, 17422};
			//entityDef.aByte68 = 5;
			//entityDef.standAnim = 90;
			//entityDef.walkAnim = 4635;
			//entityDef.actions = new String[]{null, "Attack", null, null, null};
			//entityDef.combatLevel = 276;
			entityDef.anInt86 = 110; //Resize
			entityDef.anInt91 = entityDef.sizeZ = 110; //Resize
		break;*/

		case 608:
			entityDef.name = "Sir Amik Varze";
			entityDef.actions[2] = "Exchange Scrolls";
			break;
			
			

			
			/*case 6203:
				entityDef.name = "K'ril Tsutsaroth";
				entityDef.anIntArray94 = new int[]{53192};
				entityDef.aByte68 = 5;
				entityDef.standAnim = 14967;
				entityDef.walkAnim = 14966;
				entityDef.actions = new String[]{null, "Attack", null, null, null};
				entityDef.combatLevel = 650;
				entityDef.anInt86 = 50; //Resize
				entityDef.anInt91 = entityDef.sizeZ = 50; //Resize	
			break;*/
			
			/*case 6203:
				entityDef.name = "K'ril Tsutsaroth";
				//entityDef.anIntArray94 = new int[]{53192, 53194, 53197};
				entityDef.aByte68 = 5;
				entityDef.standAnim = 14967;
				entityDef.walkAnim = 14966;
				entityDef.actions = new String[]{null, "Attack", null, null, null};
				entityDef.combatLevel = 650;
				entityDef.anInt91 = entityDef.sizeZ = 100;
				entityDef.anInt86 = 100;
			break;*/
			
			case 6222:
				//entityDef.name = "Kree'arra";
				entityDef.aByte68 = 5;
				//entityDef.standAnim = 6972;
				//entityDef.walkAnim = 6973;
				//entityDef.actions = new String[] {null, "Attack", null, null, null};
				entityDef.anInt86 = 110; //Resize
				entityDef.anInt91 = entityDef.sizeZ = 110; //Resize
				break;
				
			case 6247:
				entityDef.name = "Commander Zilyana";
				entityDef.modelIds = new int[]{55881};
				entityDef.aByte68 = 2;
				entityDef.standAnim = 6963;
				entityDef.walkAnim = 6962;
				entityDef.actions = new String[]{null, "Attack", null, null, null};
				entityDef.combatLevel = 596;
				entityDef.renderPriority = true;
				break;
				
				
			/*case 6250:
				entityDef.name = "Growler";
				entityDef.anIntArray94 = new int[]{27718};
				entityDef.aByte68 = 3;
				entityDef.standAnim = 7014;
				entityDef.walkAnim = 7015;
				entityDef.actions = new String[]{null, "Attack", null, null, null};
				entityDef.combatLevel = 139;
				entityDef.anInt91 = entityDef.sizeZ = 144;
				entityDef.anInt86 = 144;
				entityDef.anInt85 = 20;
			break;*/
			
			/*case 6203:
				entityDef.name = "K'ril Tsutsaroth";
				//entityDef.anIntArray94 = new int[] {53192, 53194, 53197};
				entityDef.aByte68 = 5;
				entityDef.actions = new String[] { null, "Attack", null, null, null};
				//entityDef.aBoolean93 = true;
				entityDef.combatLevel = 650;
				entityDef.standAnim = 14967;
				entityDef.walkAnim = 14966;
				entityDef.anInt86 = 175; //Resize
				entityDef.anInt91 = entityDef.sizeZ = 175; //Resize
			break;*/
			
			
			/*case 6203:
				entityDef.name = "K'ril Tsutsaroth";
				//entityDef.anIntArray94 = new int[]{53192, 53194, 53197};
				entityDef.actions = new String[5];
				entityDef.anIntArray94 = new int[3];
				entityDef.anIntArray94[0] = 53192;
				entityDef.anIntArray94[1] = 53194;
				entityDef.anIntArray94[2] = 53197;
				
				entityDef.standAnim = 14967;
				entityDef.walkAnim = 14966;
				entityDef.actions[1] = "Attack";
				entityDef.combatLevel = 650;
			break;*/
			
		/*case 8350: // works
			entityDef.actions = new String[5];
			entityDef.actions[1] = "Attack"; 
			entityDef.childrenIDs = new int[1];
			entityDef.childrenIDs[0] = 44733;
			entityDef.standAnim = 10921;
			entityDef.walkAnim  = 10920;
			entityDef.combatLevel = 450;
			entityDef.name = "Tormented demon";
			entityDef.description = "Lucien must be incredibly powerful if he can bind such demons to his will.".getBytes();
			break;*/
			
			
		/*case 8350: //new?
			entityDef.name = "Tormented demon";
			entityDef.componentModels = new int[]{44733};
			entityDef.aByte68 = 3;
			entityDef.standAnimation = 10921;
			entityDef.walkAnimation = 10920;
			entityDef.actions = new String[]{null, "Attack", null, null, null};
			entityDef.combatLevel = 450;
			entityDef.anInt85 = 25;
			entityDef.anInt92 = 125;
			entityDef.anInt75 = 2;
			break;*/

		}
		
//		synchronized(recentUse) {
			recentUse.put(entityDef, i);
//		}
		return entityDef;
		

//		} catch(Exception ex) {
//			System.out.println("Error on id: "+i);
//		}
//		return null;
	}

	public Model method160() {
		if (childrenIDs != null) {
			EntityDef entityDef = method161();
			if (entityDef == null)
				return null;
			else
				return entityDef.method160();
		}
		if (anIntArray73 == null)
			return null;
		Model model;
//		synchronized(headCache) {
			model = (Model) headCache.get(myId);
//		}
		if (model == null) {
			boolean flag1 = false;
			for (int i = 0; i < anIntArray73.length; i++)
				if (!Model.method463(anIntArray73[i], modelType))
					flag1 = true;
	
			if (flag1)
				return null;
			Model aclass30_sub2_sub4_sub6s[] = new Model[anIntArray73.length];
			for (int j = 0; j < anIntArray73.length; j++)
				aclass30_sub2_sub4_sub6s[j] = Model.loadDropModel(anIntArray73[j], modelType);
	
			if (aclass30_sub2_sub4_sub6s.length == 1)
				model = aclass30_sub2_sub4_sub6s[0];
			else
				model = new Model(aclass30_sub2_sub4_sub6s.length,
						aclass30_sub2_sub4_sub6s);
			if (modifiedModelColors != null) {
				for (int k = 0; k < modifiedModelColors.length; k++)
					model.replaceHs1(modifiedModelColors[k], originalModelColors[k]);
	
			}
//			synchronized(headCache) {
				headCache.put(model, myId);
//			}
		}

		return model;
	}

	public EntityDef method161() {
		int j = -1;
		if (anInt57 != -1 && anInt57 <= 2113) {
			VarBit varBit = VarBit.cache[anInt57];
			int k = varBit.anInt648;
			int l = varBit.anInt649;
			int i1 = varBit.anInt650;
			int j1 = Client.anIntArray1232[i1 - l];
			j = clientInstance.variousSettings[k] >> l & j1;
		} else if (anInt59 != -1)
			j = clientInstance.variousSettings[anInt59];
		if (j < 0 || j >= childrenIDs.length || childrenIDs[j] == -1)
			return null;
		else
			return forID(childrenIDs[j]);
	}

	public static void unpackConfig(RSArchive streamLoader) {
		stream = new RSBuffer(streamLoader.readFile("npc.dat"));
		RSBuffer stream2 = new RSBuffer(streamLoader.readFile("npc.idx"));
		
		stream667 = new RSBuffer(streamLoader.readFile("npc2.dat"));
		RSBuffer stream667_2 = new RSBuffer(streamLoader.readFile("npc2.idx"));
		
//		int totalNPCs = stream2.readUnsignedWord();
		npcSize634 = stream2.readUnsignedWord();
		int totalNPCs = stream667_2.readUnsignedWord();
		
		System.out.println("Loaded Npcs Total:"+totalNPCs+"   634Npcs:"+npcSize634);
		
		streamIndices = new int[npcSize634];
		streamIndices667 = new int[totalNPCs/*+16000*/];
		int i = 2;
		for (int j = 0; j < totalNPCs; j++) {
			streamIndices667[j] = i;
//			if(j < npcSize634) {
//				i += stream2.readUnsignedWord();
//				stream667_2.readUnsignedWord();
//			} else/* if(j >= npcSize634)*/ {
				i += stream667_2.readUnsignedWord();
//			}
		}
		
		i = 2;
		for (int j = 0; j < npcSize634; j++) {
			streamIndices[j] = i;
				i += stream2.readUnsignedWord();
		}

		streamOsrs = new RSBuffer(streamLoader.readFile("npcosrs.dat"));
		RSBuffer streamOsrs = new RSBuffer(streamLoader.readFile("npcosrs.idx"));
		int osrsSize = streamOsrs.readUnsignedWord();
		System.out.println("loading OSRS 182 npcs. Count:"+osrsSize);
		streamIndicesOsrs = new int[osrsSize];
		i = 2;
		for (int j = 0; j < osrsSize; j++) {
			streamIndicesOsrs[j] = i;
				i += streamOsrs.readUnsignedWord();
		}
		
	}

	public Model method164() {
		if (childrenIDs != null) {
			EntityDef entityDef = method161();
			if (entityDef == null)
				return null;
			else {
				return entityDef.method164();
			}
		}
		Model model;
//		synchronized(modelCache) {
			model = (Model) modelCache.get(myId);
//		}
		if (model == null) {
			boolean flag = false;
			for (int i1 = 0; i1 < modelIds.length; i1++)
				if (!Model.method463(modelIds[i1], modelType))
					flag = true;

			if (flag)
				return null;
			Model aclass30_sub2_sub4_sub6s[] = new Model[modelIds.length];
			for (int j1 = 0; j1 < modelIds.length; j1++)
				aclass30_sub2_sub4_sub6s[j1] = Model
						.loadDropModel(modelIds[j1], modelType);
			if (aclass30_sub2_sub4_sub6s.length == 1) {
				model = aclass30_sub2_sub4_sub6s[0];
			} else {
				model = new Model(aclass30_sub2_sub4_sub6s.length, aclass30_sub2_sub4_sub6s);
			}
			if (modifiedModelColors != null) {
				for (int k1 = 0; k1 < modifiedModelColors.length; k1++)
					model.replaceHs1(modifiedModelColors[k1], originalModelColors[k1]);

			}
			model.applyEffects();
			// model.calculateLighting(64 + anInt85, 850 + anInt92, -30, -50, -30,
			// true);
			model.calculateLighting(84 + anInt85, 1000 + anInt92, -90, -580, -90, true);
//			synchronized(modelCache) {
				modelCache.put(model, myId);
//			}
		}

		return model;
	}

	public Model method164(int j, int k, boolean ai[], boolean isPet, int scale) {
		if (childrenIDs != null) {
			EntityDef entityDef = method161();
			if (entityDef == null)
				return null;
			else {
				return entityDef.method164(j, k, ai, isPet, scale);
			}
		}
		Model model;
//		synchronized(modelCache) {
			model = (Model) modelCache.get(myId);
//		}
		if (model == null) {
			boolean flag = false;
			for (int i1 = 0; i1 < modelIds.length; i1++)
				if (!Model.method463(modelIds[i1], modelType))
					flag = true;

			if (flag)
				return null;
			Model aclass30_sub2_sub4_sub6s[] = new Model[modelIds.length];
			for (int j1 = 0; j1 < modelIds.length; j1++)
				aclass30_sub2_sub4_sub6s[j1] = Model
						.loadDropModel(modelIds[j1], modelType);
			if (aclass30_sub2_sub4_sub6s.length == 1) {
				model = aclass30_sub2_sub4_sub6s[0];
			} else {
				model = new Model(aclass30_sub2_sub4_sub6s.length, aclass30_sub2_sub4_sub6s);
			}

			if (translateY != 0) {
				model.translate(0, -translateY, 0);
			}

			if (modifiedModelColors != null) {
				for (int k1 = 0; k1 < modifiedModelColors.length; k1++)
					model.replaceHs1(modifiedModelColors[k1], originalModelColors[k1]);

			}
			model.applyEffects();
			// model.calculateLighting(64 + anInt85, 850 + anInt92, -30, -50, -30,
			// true);
			model.calculateLighting(84 + anInt85, 1000 + anInt92, -90, -580, -90, true);
//			synchronized(modelCache) {
				modelCache.put(model, myId);
//			}
		}

		boolean leaveAlpha = false;
		if (k != -1) {
			leaveAlpha |= Class36.hasAlpha(k);
		}
		if (j != -1) {
			leaveAlpha |= Class36.hasAlpha(j);
		}

		Model model_1 = model.copyEntity(!leaveAlpha);
		model_1.animated = true;
		if (k != -1 && j != -1) {
			if (!model_1.method471(ai, j, 0, 0, 0, k, 0, 0, 0)) {
				return null;
			}
		} else if (k != -1) {
			if (!model_1.applyAnimation(k)) {
				return null;
			}
		}

		if (anInt91 != 128 || anInt86 != 128 || sizeZ != 128)
			model_1.method478(anInt91, sizeZ, anInt86);
		if (isPet && scale != 128) {
			model_1.method478(scale, scale, scale);
		}
		if (aByte68 == 1)
			model_1.aBoolean1659 = true;

		model_1.method466();

		return model_1;
	}

	public void readValues(RSBuffer stream) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				return;
			if (i == 1) {
				int j = stream.readUnsignedByte();
				modelIds = new int[j];
				for (int j1 = 0; j1 < j; j1++)
					modelIds[j1] = stream.readUnsignedWord();

			} else if (i == 2)
				name = stream.readNewString();
			else if (i == 3)
				description = stream.readBytes();
			else if (i == 12)
				aByte68 = stream.readSignedByte();
			else if (i == 13)
				standAnim = stream.readUnsignedWord();
			else if (i == 14)
				walkAnim = stream.readUnsignedWord();
			else if (i == 17) {
				walkAnim = stream.readUnsignedWord();
				anInt58 = stream.readUnsignedWord();
				anInt83 = stream.readUnsignedWord();
				anInt55 = stream.readUnsignedWord();
			} else if (i >= 30 && i < 40) {
				if (actions == null)
					actions = new String[5];
				actions[i - 30] = stream.readNewString();
				if (actions[i - 30].equalsIgnoreCase("hidden"))
					actions[i - 30] = null;
			} else if (i == 40) {
				int k = stream.readUnsignedByte();
				modifiedModelColors = new int[k];
				originalModelColors = new int[k];
				for (int k1 = 0; k1 < k; k1++) {
					modifiedModelColors[k1] = stream.readUnsignedWord();
					originalModelColors[k1] = stream.readUnsignedWord();
				}

			} else if (i == 60) {
				int l = stream.readUnsignedByte();
				anIntArray73 = new int[l];
				for (int l1 = 0; l1 < l; l1++)
					anIntArray73[l1] = stream.readUnsignedWord();

			} else if (i == 90)
				stream.readUnsignedWord();
			else if (i == 91)
				stream.readUnsignedWord();
			else if (i == 92)
				stream.readUnsignedWord();
			else if (i == 93)
				aBoolean87 = false;
			else if (i == 95)
				combatLevel = stream.readUnsignedWord();
			else if (i == 97)
				anInt91 = sizeZ = stream.readUnsignedWord();
			else if (i == 98)
				anInt86 = stream.readUnsignedWord();
			else if (i == 99)
				renderPriority = true;
			else if (i == 100)
				anInt85 = stream.readSignedByte();
			else if (i == 101)
				anInt92 = stream.readSignedByte() * 5;
			else if (i == 102)
				headIcon = stream.readUnsignedWord();
			else if (i == 103)
				anInt79 = stream.readUnsignedWord();
			else if (i == 106) {
				anInt57 = stream.readUnsignedWord();
				if (anInt57 == 65535)
					anInt57 = -1;
				anInt59 = stream.readUnsignedWord();
				if (anInt59 == 65535)
					anInt59 = -1;
				int i1 = stream.readUnsignedByte();
				childrenIDs = new int[i1 + 1];
				for (int i2 = 0; i2 <= i1; i2++) {
					childrenIDs[i2] = stream.readUnsignedWord();
					if (childrenIDs[i2] == 65535)
						childrenIDs[i2] = -1;
				}

			} else if (i == 107)
				aBoolean84 = false;
		} while (true);
	}
	
	private void readValues667(RSBuffer stream)
	{
		do
		{
			int i = stream.readUnsignedByte();
			if(i == 0)
				return;
			if(i == 1)
			{
				int j = stream.readUnsignedByte();
				modelIds = new int[j];
				for(int j1 = 0; j1 < j; j1++)
					modelIds[j1] = stream.readUnsignedWord();

			} else
			if(i == 2)
				name = stream.readString();
			else
			if(i == 3)
				description = stream.readBytes();
			else
			if(i == 12)
				aByte68 = stream.readSignedByte();
			else
				if(i == 13) {
					standAnim = stream.readUnsignedWord();
					if (standAnim == 65535) {
						standAnim = -1;
					}
				} else
				if(i == 14) {
					walkAnim = stream.readUnsignedWord();
					if (walkAnim == 65535) {
						walkAnim = -1;
					}
				} else
				if(i == 17)
				{
					walkAnim = stream.readUnsignedWord();
					if (walkAnim == 65535) {
						walkAnim = -1;
					}
					anInt58 = stream.readUnsignedWord();
					if (anInt58 == 65535) {
						anInt58 = -1;
					}
					anInt83 = stream.readUnsignedWord();
					if (anInt83 == 65535) {
						anInt83 = -1;
					}
					anInt55 = stream.readUnsignedWord();
					if (anInt55 == 65535) {
						anInt55 = -1;
					}
			} else
			if(i >= 30 && i < 40)
			{
				if(actions == null)
					actions = new String[5];
				actions[i - 30] = stream.readString();
				if(actions[i - 30].equalsIgnoreCase("hidden"))
					actions[i - 30] = null;
			} else
			if(i == 40)
			{
				int k = stream.readUnsignedByte();
				modifiedModelColors = new int[k];
				originalModelColors = new int[k];
				for(int k1 = 0; k1 < k; k1++)
				{
					modifiedModelColors[k1] = stream.readUnsignedWord();
					originalModelColors[k1] = stream.readUnsignedWord();
				}

			} else
			if(i == 60)
			{
				int l = stream.readUnsignedByte();
				anIntArray73 = new int[l];
				for(int l1 = 0; l1 < l; l1++)
					anIntArray73[l1] = stream.readUnsignedWord();

			} else
			if(i == 90)
				stream.readUnsignedWord();
			else
			if(i == 91)
				stream.readUnsignedWord();
			else
			if(i == 92)
				stream.readUnsignedWord();
			else
			if(i == 93)
				aBoolean87 = false;
			else
			if(i == 95)
				combatLevel = stream.readUnsignedWord();
			else
			if(i == 97)
				anInt91 = sizeZ = stream.readUnsignedWord();
			else
			if(i == 98)
				anInt86 = stream.readUnsignedWord();
			else
			if(i == 99)
				renderPriority = true;
			else
			if(i == 100)
				anInt85 = stream.readSignedByte();
			else
			if(i == 101)
				anInt92 = stream.readSignedByte() * 5;
			else
			if(i == 102)
				headIcon = stream.readUnsignedWord();
			else
			if(i == 103)
				anInt79 = stream.readUnsignedWord();
			else
			if(i == 106)
			{
				anInt57 = stream.readUnsignedWord();
				if(anInt57 == 65535)
					anInt57 = -1;
				anInt59 = stream.readUnsignedWord();
				if(anInt59 == 65535)
					anInt59 = -1;
				int i1 = stream.readUnsignedByte();
				childrenIDs = new int[i1 + 1];
				for(int i2 = 0; i2 <= i1; i2++)
				{
					childrenIDs[i2] = stream.readUnsignedWord();
					if(childrenIDs[i2] == 65535)
						childrenIDs[i2] = -1;
				}

			} else
			if(i == 107)
				aBoolean84 = false;
		} while(true);
	}
	
	private void readValues634(RSBuffer stream)
	{
		do
		{
			int i = stream.readUnsignedByte();
			if(i == 0)
				return;
			if(i == 1)
			{
				int j = stream.readUnsignedByte();
				modelIds = new int[j];
				for(int j1 = 0; j1 < j; j1++)
					modelIds[j1] = stream.readUnsignedWord();

			} else
			if(i == 2)
				name = stream.readNewString();
			else
			if(i == 3)
				description = stream.readNewString().getBytes();
			else
			if(i == 12)
				aByte68 = stream.readSignedByte();
			else
			if(i == 13) {
				standAnim = stream.readUnsignedWord();
				if (standAnim == 65535) {
					standAnim = -1;
				}
			} else
			if(i == 14) {
				walkAnim = stream.readUnsignedWord();
				if (walkAnim == 65535) {
					walkAnim = -1;
				}
			} else
			if(i == 17)
			{
				walkAnim = stream.readUnsignedWord();
				if (walkAnim == 65535) {
					walkAnim = -1;
				}
				anInt58 = stream.readUnsignedWord();
				if (anInt58 == 65535) {
					anInt58 = -1;
				}
				anInt83 = stream.readUnsignedWord();
				if (anInt83 == 65535) {
					anInt83 = -1;
				}
				anInt55 = stream.readUnsignedWord();
				if (anInt55 == 65535) {
					anInt55 = -1;
				}
			} else
			if(i >= 30 && i < 40)
			{
				if(actions == null)
					actions = new String[5];
				actions[i - 30] = stream.readNewString();
				if(actions[i - 30].equalsIgnoreCase("hidden"))
					actions[i - 30] = null;
			} else
			if(i == 40)
			{
				int k = stream.readUnsignedByte();
				modifiedModelColors = new int[k];
				originalModelColors = new int[k];
				for(int k1 = 0; k1 < k; k1++)
				{
					modifiedModelColors[k1] = stream.readUnsignedWord();
					originalModelColors[k1] = stream.readUnsignedWord();
				}

			} else
			if(i == 60)
			{
				int l = stream.readUnsignedByte();
				anIntArray73 = new int[l];
				for(int l1 = 0; l1 < l; l1++)
					anIntArray73[l1] = stream.readUnsignedWord();

			} else
			if(i == 90)
				stream.readUnsignedWord();
			else
			if(i == 91)
				stream.readUnsignedWord();
			else
			if(i == 92)
				stream.readUnsignedWord();
			else
			if(i == 93)
				aBoolean87 = false;
			else
			if(i == 95)
				combatLevel = stream.readUnsignedWord();
			else
			if(i == 97)
				anInt91 = sizeZ = stream.readUnsignedWord();
			else
			if(i == 98)
				anInt86 = stream.readUnsignedWord();
			else
			if(i == 99)
				renderPriority = true;
			else
			if(i == 100)
				anInt85 = stream.readSignedByte();
			else
			if(i == 101)
				anInt92 = stream.readSignedByte() * 5;
			else
			if(i == 102)
				headIcon = stream.readUnsignedWord();
			else
			if(i == 103)
				anInt79 = stream.readUnsignedWord();
			else
			if(i == 106)
			{
				anInt57 = stream.readUnsignedWord();
				if(anInt57 == 65535)
					anInt57 = -1;
				anInt59 = stream.readUnsignedWord();
				if(anInt59 == 65535)
					anInt59 = -1;
				int i1 = stream.readUnsignedByte();
				childrenIDs = new int[i1 + 1];
				for(int i2 = 0; i2 <= i1; i2++)
				{
					childrenIDs[i2] = stream.readUnsignedWord();
					if(childrenIDs[i2] == 65535)
						childrenIDs[i2] = -1;
				}

			} else
			if(i == 107)
				aBoolean84 = false;
		} while(true);
	}
	
	public EntityDef() {
		anInt55 = -1;
		anInt57 = -1;
		anInt58 = -1;
		anInt59 = -1;
		combatLevel = -1;
		walkAnim = -1;
		aByte68 = 1;
		headIcon = -1;
		standAnim = -1;
		myId = 0;
		anInt79 = 32;
		anInt83 = -1;
		aBoolean84 = true;
		anInt86 = 128;
		aBoolean87 = true;
		anInt91 = sizeZ = 128;
		renderPriority = false;
	}
	
	public void setDefaults() {
		anInt55 = -1;
		anInt57 = -1;
		anInt58 = -1;
		anInt59 = -1;
		combatLevel = -1;
		walkAnim = -1;
		aByte68 = 1;
		headIcon = -1;
		standAnim = -1;
		//interfaceType = -1L;
		anInt79 = 32;
		anInt83 = -1;
		aBoolean84 = true;
		anInt86 = 128;
		aBoolean87 = true;
		anInt91 = sizeZ = 128;
		renderPriority = false;
	}

	public int anInt55; //turnLeftAnimationId
	public int anInt57; //varBitId
	public int anInt58; //turnAroundAnimationId
	public int anInt59; //settingId
	public static RSBuffer stream;
	private static RSBuffer stream667;
	public static int npcSize634;
	public int combatLevel;
	public String name;
	public String actions[];
	public int walkAnim;
	public byte aByte68; //boundaryDimension
	public int[] originalModelColors; //originalModelColors //anIntArray70
	public static int[] streamIndices; //bufferOffsets
	public static int[] streamIndices667; //bufferOffsets
	public int[] anIntArray73; //headModelIndexes
	public int headIcon; //headIcon //anInt75
	public int[] modifiedModelColors; //modifiedModelColors // anIntArray76
	public int standAnim;
	public int myId; //id 
	public int anInt79;  //degreesToTurn 
	public static Client clientInstance;
	public int anInt83; //turnRightAnimationId 
	public boolean aBoolean84; //clickable 
	public int anInt85; //brightness
	public int anInt86; //sizeY 
	public boolean aBoolean87; //minimapVisible 
	public int childrenIDs[];
	public byte description[];
	public int anInt91; //sizeXZ 
	public int sizeZ;
	public int anInt92; //contrast
	public boolean renderPriority = false; //visible
	public int[] modelIds; //modelIds // anIntArray94
    public int translateY;
	public static ObjectCache recentUse = new ObjectCache(64);
	public static ObjectCache modelCache = new ObjectCache(50);
	public static ObjectCache headCache = new ObjectCache(5);
	public ModelTypeEnum modelType = ModelTypeEnum.REGULAR;
	private static RSBuffer streamOsrs;
	private static int[] streamIndicesOsrs;
	public static final int OSRS_OFFSET = 100_000;

}