package com.soulplayps.client.node.animable;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.animable.model.ModelTypeEnum;
import com.soulplayps.client.node.object.ObjectDef;
import com.soulplayps.client.node.object.ObjectManager;
import com.soulplayps.client.unpack.VarBit;

public final class Animable_Sub5 extends Animable {
	private int anInt1599;
	private final int[] anIntArray1600;
	private final int anInt1601;
	private final int anInt1602;
	private final int anInt1603;
	private final int anInt1604;
	private final int anInt1605;
	private final int anInt1606;
	private Animation aAnimation_1607;
	private int anInt1608;
	public static Client clientInstance;
	private final int anInt1610;
	private final int anInt1611;
	private final int anInt1612;
	private final boolean osrs;

	/*
	 * private com.soulplayps.client.ObjectDef method457() { int i = -1; if(anInt1601 != -1) { try {
	 * com.soulplayps.client.VarBit varBit = com.soulplayps.client.VarBit.cache[anInt1601]; int k = varBit.anInt648; int l =
	 * varBit.anInt649; int i1 = varBit.anInt650; int j1 =
	 * client.anIntArray1232[i1 - l]; i = clientInstance.variousSettings[k] >> l
	 * & j1; } catch(Exception ex){} } else if(anInt1602 != -1) i =
	 * clientInstance.variousSettings[anInt1602]; if(i < 0 || i >=
	 * anIntArray1600.length || anIntArray1600[i] == -1) return null; else
	 * return com.soulplayps.client.ObjectDef.forID(anIntArray1600[i]); }
	 */
	private ObjectDef method457() {
		int i = -1;
		if (anInt1601 != -1 && anInt1601 < VarBit.cache.length) {
			VarBit varBit = VarBit.cache[anInt1601];
			int k = varBit.anInt648;
			int l = varBit.anInt649;
			int i1 = varBit.anInt650;
			int j1 = Client.anIntArray1232[i1 - l];
			i = clientInstance.variousSettings[k] >> l & j1;
		} else if (anInt1602 != -1
				&& anInt1602 < clientInstance.variousSettings.length)
			i = clientInstance.variousSettings[anInt1602];
		if (i < 0 || i >= anIntArray1600.length || anIntArray1600[i] == -1)
			return null;
		else
			return ObjectDef.forID(anIntArray1600[i]);
	}

	@Override
	public Model getRotatedModel() {
		super.animated = false;
		int j = -1;
		if (aAnimation_1607 != null) {
			int k = Client.loopCycle - anInt1608;
			if (k > 100 && aAnimation_1607.loop > 0)
				k = 100;
			while (k > aAnimation_1607.method258(anInt1599)) {
				k -= aAnimation_1607.method258(anInt1599);
				anInt1599++;
				if (anInt1599 < aAnimation_1607.frameCount)
					continue;
				anInt1599 -= aAnimation_1607.loop;
				if (anInt1599 >= 0 && anInt1599 < aAnimation_1607.frameCount)
					continue;
				aAnimation_1607 = null;
				break;
			}
			anInt1608 = Client.loopCycle - k;
			if (aAnimation_1607 != null)
				j = aAnimation_1607.frame2Ids[anInt1599];
		}
		ObjectDef class46;
		if (anIntArray1600 != null)
			class46 = method457();
		else
			class46 = osrs ? ObjectDef.forIDOSRS(anInt1610) : ObjectDef.forID(anInt1610);
		if (class46 == null) {
			return null;
		}

		Model model = class46.method578(anInt1611, anInt1612, anInt1603,
				anInt1604, anInt1605, anInt1606, j);
		if (model != null) {
			super.animated = model.animated;
		}

		return model;
	}

	public Animable_Sub5(int objectId, int j, int k, int l, int i1, int j1, int k1,
			int l1, boolean flag, int regionType) {
		anInt1610 = objectId;
		anInt1611 = k;
		anInt1612 = j;
		anInt1603 = j1;
		anInt1604 = l;
		anInt1605 = i1;
		anInt1606 = k1;
		if (l1 != -1) {
			aAnimation_1607 = Animation.anims[l1];
			anInt1599 = 0;
			anInt1608 = Client.loopCycle;
			if (flag && aAnimation_1607.loop != -1) {
				anInt1599 = (int) (Math.random() * aAnimation_1607.frameCount);
				anInt1608 -= (int) (Math.random() * aAnimation_1607
						.method258(anInt1599));
			}
		}
		ObjectDef class46 = ObjectManager.getObjectDef(anInt1610, regionType);//ObjectDef.forID(anInt1610);
		osrs = class46.modelType == ModelTypeEnum.OSRS;
		anInt1601 = class46.anInt774;
		anInt1602 = class46.configID;
		anIntArray1600 = class46.childrenIDs;
	}
}