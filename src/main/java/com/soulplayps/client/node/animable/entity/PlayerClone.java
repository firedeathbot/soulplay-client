package com.soulplayps.client.node.animable.entity;

import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.unknown.Class36;
import com.soulplayps.client.unpack.SpotAnim;

public class PlayerClone extends Entity {

	private final int time;
	private final Player player;

	public PlayerClone(Player player, int x, int z, int time) {
		this.player = player;
		this.interactingEntity = player.interactingEntity;
		this.x = player.x + x;
		this.z = player.z + z;
		this.time = time;
	}

	public int getTime() {
		return time;
	}

	@Override
	public void method443(int i, float j, float k, float l, float i1, int j1, int k1, int l1, long i2, boolean animated,
			int level, long gluid) {
		Animation seq = player.animId != -1 && player.animDelay == 0 ? Animation.anims[player.animId] : null;
		Animation baseSeq = seq == null ? Animation.anims[player.anInt1511] : null;
		Model model = player.appearance.method452(seq, baseSeq, player.animFrame, player.animNextFrame,
				player.animFrameDelay, player.baseAnimFrame, player.baseAnimNextFrame, player.baseAnimFrameDelay);
		if (model == null) {
			super.animated = false;
			return;
		}

		Model spotAnimModel = null;
		super.height = model.modelHeight;
		if (player.spotAnimId != -1 && player.spotAnimFrame != -1) {
			SpotAnim spotAnim = SpotAnim.cache[player.spotAnimId];
			spotAnimModel = spotAnim.getModel(player.spotAnimFrame, player.spotAnimNextFrame,
					player.spotAnimFrameDelay);

			if (spotAnim.animation != null
					&& Class36.animationlist[spotAnim.animation.frame2Ids[0] >> 16].length == 0) {
				spotAnimModel = null;
			}
		}

		super.animated = true;
		modelHeight = model.modelHeight;
		model.method443(i, j, k, l, i1, j1, k1, l1, i2, this.animated, level, gluid);
		if (spotAnimModel != null) {
			if (player.anInt1524 != 0) {
				spotAnimModel.translate(0, -player.anInt1524, 0);
			}

			spotAnimModel.method466();
			spotAnimModel.method443(i, j, k, l, i1, j1, k1, l1, i2, this.animated, level, gluid + 1);
		}
	}

}
