package com.soulplayps.client.node.animable.item;

import com.soulplayps.client.node.animable.model.ModelTypeEnum;

public class ItemDefExt {

	public static void forId(ItemDef itemDef) {
		switch (itemDef.id) {
			case 455:
				itemDef.name = "Powers point";
				break;
			case 25000:
				itemDef.name = "Power card (Random)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				itemDef.modelId = 21562;
				itemDef.xof2d = 1;
				itemDef.yof2d = 4;
				itemDef.xan2d = 576;
				itemDef.yan2d = 2047;
				itemDef.zoom2d = 659;
				itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
				itemDef.recolorModified = new int[] {0, 0, 0, 0, 0, 0, 0, 10322, 10322, 10322, 10322, 10322};
				break;
			case 25001:
					itemDef.name = "Power card (Health)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25002:
					itemDef.name = "Power card (Melee Damage)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25003:
					itemDef.name = "Power card (Magic Damage)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25004:
					itemDef.name = "Power card (Range Damage)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25005:
					itemDef.name = "Power card (Strength)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25006:
					itemDef.name = "Power card (Prayer Drain)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25007:
					itemDef.name = "Power card (Defence)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25008:
					itemDef.name = "Power card (Block)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25009:
					itemDef.name = "Power card (Surge)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25010:
					itemDef.name = "Power card (Spiked Shield)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25011:
					itemDef.name = "Power card (Two Hander)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25012:
					itemDef.name = "Power card (One Hander)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25013:
					itemDef.name = "Power card (Ranger)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25014:
					itemDef.name = "Power card (Minor Crit)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25015:
					itemDef.name = "Power card (Health Regen)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25016:
					itemDef.name = "Power card (Melee Accuracy)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25017:
					itemDef.name = "Power card (Range Accuracy)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25018:
					itemDef.name = "Power card (Magic Accuracy)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25019:
					itemDef.name = "Power card (Poison)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25020:
					itemDef.name = "Power card (Poison Lasting)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25021:
					itemDef.name = "Power card (Magic knowledge)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25022:
					itemDef.name = "Power card (Necromancer)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25023:
					itemDef.name = "Power card (Hybrid)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25024:
					itemDef.name = "Power card (Block Mastery)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25025:
					itemDef.name = "Power card (Ignite)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25026:
					itemDef.name = "Power card (Earth Root)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25027:
					itemDef.name = "Power card (Vengeance Heal)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25028:
					itemDef.name = "Power card (Holy Food)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25029:
					itemDef.name = "Power card (Heavyweight)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25030:
					itemDef.name = "Power card (Dragonbreath)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25031:
					itemDef.name = "Power card (Quick)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25032:
					itemDef.name = "Power card (Battlemage)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25033:
					itemDef.name = "Power card (The Beast)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25034:
					itemDef.name = "Power card (Critter)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25035:
					itemDef.name = "Power card (Explosive Ranger)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25036:
					itemDef.name = "Power card (Ninja)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25037:
					itemDef.name = "Power card (Ironman)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25038:
					itemDef.name = "Power card (Mirror)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25039:
					itemDef.name = "Power card (Poison Crit)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25040:
					itemDef.name = "Power card (Lifesteal)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25041:
					itemDef.name = "Power card (Caster)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25042:
					itemDef.name = "Power card (Advanced Ranger)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25043:
					itemDef.name = "Power card (Warrior)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25044:
					itemDef.name = "Power card (Crits)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25045:
					itemDef.name = "Power card (Crit Sprinter)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25046:
					itemDef.name = "Power card (Haste)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25047:
					itemDef.name = "Power card (Range Crit)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25048:
					itemDef.name = "Power card (Fire Wind)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
				case 25049:
					itemDef.name = "Power card (Perfected ingredients)";
					itemDef.inventoryOptions = new String[]{null, null, null, "Info", "Drop"};
					itemDef.modelId = 21562;
					itemDef.xof2d = 1;
					itemDef.yof2d = 4;
					itemDef.xan2d = 576;
					itemDef.yan2d = 2047;
					itemDef.zoom2d = 659;
					itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};
					itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};
					break;
			case 11180:
				itemDef.name = "Soulplay Token";
				itemDef.stackable = true;
				break;
			case 11341:
				itemDef.name = "Page of Destiny";
				itemDef.inventoryOptions = new String[]{"Learn", null, null, null, "Drop"};
				break;
			case 19705:
				itemDef.name = "Beginner Power Book";
				itemDef.inventoryOptions = new String[]{"Learn", null, null, null, "Drop"};
				break;
			// ------------- new osrs items start

			case 12924: // done
				itemDef.name = "Toxic blowpipe (empty)";
				itemDef.zoom2d = 1158;
				itemDef.xan2d = 768;
				itemDef.yan2d = 189;
				itemDef.xof2d = -7;
				itemDef.yof2d = 4;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, "Wield", "Drop" };
				itemDef.modelId = 19221;// 60077; //19221
				itemDef.manWear = 14405;
				itemDef.womanWear = 14405;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 12926: // done
				itemDef.name = "Toxic blowpipe";
				itemDef.zoom2d = 1158;
				itemDef.xan2d = 768;
				itemDef.yan2d = 189;
				itemDef.xof2d = -7;
				itemDef.yof2d = 4;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", "Unload", "Uncharge" };
				itemDef.modelId = 19219;// 60076; //19219
				itemDef.manWear = 14403;
				itemDef.womanWear = 14403;
				itemDef.womanwearxoff = -3;
				itemDef.manwearxoff = -7;
				itemDef.manwearyoff = 10;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30000:
				itemDef.name = "Tanzanite fang";
				itemDef.zoom2d = 1411;
				itemDef.xan2d = 202;
				itemDef.yan2d = 1939;
				itemDef.xof2d = -4;
				itemDef.yof2d = -8;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60083; // 19228;
				break;

			case 30001:
				itemDef.name = "Serpentine visage";
				itemDef.zoom2d = 716;
				itemDef.xan2d = 498;
				itemDef.yan2d = 0;
				itemDef.zan2d = 1656;
				itemDef.xof2d = -18;
				itemDef.yof2d = 7;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 19218;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

//						case 30002:
//							itemDef.name = "Serpentine helm (uncharged)";
//							itemDef.zoom2d = 700;
//							itemDef.yan2d = 1724;
//							itemDef.xan2d = 0;
//							itemDef.xof2d = -1;
//							itemDef.yof2d = -1;
//							itemDef.options = new String[]{null, null, "Take", null, null};
//							itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
//							itemDef.modelId = 19222;
//							itemDef.modelType = ModelTypeEnum.OSRS;
//							break;

//						case 30003:
//							itemDef.name = "Serpentine helm";
//							itemDef.zoom2d = 700;
//							itemDef.xan2d = 215;
//							itemDef.yan2d = 1724;
//							itemDef.yof2d = -17;
//							itemDef.xof2d = 0;
//							itemDef.options = new String[]{null, null, "Take", null, null};
//							itemDef.inventoryOptions = new String[]{null, "Wear", "Check", null, "Uncharge"};
//							itemDef.modelId = 60082; //19220
//							itemDef.manWear = 60084; //14395;
//							itemDef.womanWear = 60085; //14398;
//							itemDef.womanwearyoff = -4;
//							break;

			case 30004:
				itemDef.name = "Magic fang";
				itemDef.zoom2d = 1095;
				itemDef.xan2d = 579;
				itemDef.yan2d = 1832;
				itemDef.xof2d = 7;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60079; // 19227
				break;

			case 30006:
				itemDef.name = "Zul-andra teleport";
				itemDef.zoom2d = 779;
				itemDef.xan2d = 512;
				itemDef.xof2d = 2;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { "Teleport", null, null, null, "Drop" };
				itemDef.modelId = 14406;
				break;

			case 30007:
				itemDef.name = "Pet snakeling";
				itemDef.zoom2d = 4000;
				itemDef.yan2d = 67;
				itemDef.yof2d = 89;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60086; // 10416;
				break;

			case 30008:
				itemDef.name = "Pet snakeling";
				itemDef.zoom2d = 4000;
				itemDef.yan2d = 67;
				itemDef.yof2d = 89;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60087; // 10414;
				break;

			case 30009:
				itemDef.name = "Pet snakeling";
				itemDef.zoom2d = 2000;
				itemDef.yan2d = 67;
				itemDef.yof2d = 87;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60088; // 10413;
				break;

			case 30010:
				itemDef.name = "Pegasian boots";
				itemDef.modelId = 60089; // 29396;
				itemDef.zoom2d = 900;
				itemDef.xan2d = 165;
				itemDef.yan2d = 99;
				itemDef.xof2d = 3;
				itemDef.yof2d = -7;
				itemDef.manWear = 60090; // 29252;
				itemDef.womanWear = 60091; // 29253;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				break;

			case 30011:
				itemDef.name = "Toxic staff of the dead";
				itemDef.description = "A ghastly weapon with evil origins, with a toxic fang attached.".getBytes();
				itemDef.zoom2d = 2150;
				itemDef.xan2d = 512;
				itemDef.yan2d = 1010;
				itemDef.manWear = 60142; // 14402;
				itemDef.womanWear = 60142; // 14402;
				itemDef.modelId = 60143; // 19224;
				itemDef.manwearyoff = 7;
				itemDef.womanwearyoff = -4;
				itemDef.recolorOriginal = new int[] { 17467 };
				itemDef.recolorModified = new int[] { 21947 };
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", null, "Uncharge" };
				break;

			case 30012:
				itemDef.name = "Primordial Boots";
				itemDef.modelId = 60092; // 29397;
				itemDef.zoom2d = 900;
				itemDef.xan2d = 165;
				itemDef.yan2d = 99;
				itemDef.xof2d = 3;
				itemDef.yof2d = -7;
				itemDef.manWear = 60093; // 29250;
				itemDef.womanWear = 60094; // 29255;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				break;

			case 30013:
				itemDef.name = "Eternal boots";
				itemDef.modelId = 60095; // 29394;
				itemDef.zoom2d = 900;
				itemDef.xan2d = 165;
				itemDef.yan2d = 99;
				itemDef.xof2d = 3;
				itemDef.yof2d = -7;
				itemDef.manWear = 60096; // 29249;
				itemDef.womanWear = 60097; // 29254;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				break;

			case 30014:
				itemDef.setDefaults();
				itemDef.name = "Trident of the seas (Charged)";
				itemDef.zoom2d = 2350;
				itemDef.xan2d = 1505;
				itemDef.yan2d = 1850;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", "Uncharge", "Drop" };
				itemDef.modelId = 28232; // 28232;
				itemDef.manWear = 28230; // 28230;
				itemDef.womanWear = 28230; // 28230;
				itemDef.manwearyoff = 5;
				itemDef.manwearxoff = -5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearxoff = -2;
				itemDef.noteId = 30015;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30015:
				itemDef.setDefaults();
				itemDef.name = "Trident of the seas (Charged)";
				itemDef.zoom2d = 760;
				itemDef.xan2d = 552;
				itemDef.yan2d = 28;
				itemDef.modelId = 2429;
				itemDef.xof2d = 2;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.certTemplateID = 799;
				itemDef.noteId = 30014;
				itemDef.toNote();
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30016:
				itemDef.setDefaults();
				itemDef.name = "Uncharged Trident of the seas";
				itemDef.zoom2d = 2350;
				itemDef.xan2d = 1505;
				itemDef.yan2d = 1850;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", null, "Drop" };
				itemDef.modelId = 28232; // 28232;
				itemDef.manWear = 28230; // 28230;
				itemDef.womanWear = 28230; // 28230;
				itemDef.manwearyoff = 5;
				itemDef.manwearxoff = -5;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearxoff = -2;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.noteId = 30017;
				break;

			case 30017: // noted
				itemDef.setDefaults();
				itemDef.name = "Uncharged Trident of the seas";
				itemDef.zoom2d = 760;
				itemDef.xan2d = 552;
				itemDef.yan2d = 28;
				itemDef.xof2d = 2;
				itemDef.modelId = 2429;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.certTemplateID = 799;
				itemDef.noteId = 30016;
				itemDef.toNote();
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30019:
				// itemDef.setDefaults();
				itemDef.name = "Volcanic abyssal whip";
				itemDef.zoom2d = 840;
				itemDef.xan2d = 280;
				itemDef.xof2d = -2;
				itemDef.yof2d = 56;
				itemDef.recolorOriginal = new int[1];
				itemDef.recolorModified = new int[1];
				itemDef.recolorOriginal[0] = 528;
				itemDef.recolorModified[0] = 5056;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.modelId = 5455;// 60104; //5455;
				itemDef.manWear = 8950;// 60105; //8950;
				itemDef.womanWear = 8950;// 60105; //8950;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30020:
				itemDef.name = "Abyssal dagger";
				itemDef.zoom2d = 1347;
				itemDef.xan2d = 1589;
				itemDef.yan2d = 781;
				itemDef.yof2d = 3;
				itemDef.xof2d = -5;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", "Uncharge", "Drop" };
				itemDef.modelId = 60106; // 29598;
				itemDef.manWear = 60107; // 29425;
				itemDef.womanWear = 60107; // 29425;
				itemDef.manwearxoff = -3;
				itemDef.manwearyoff = 6;
				itemDef.womanwearzoff = 5;
				itemDef.womanwearyoff = -5;
				break;

			case 30021:
				itemDef.name = "Abyssal dagger(p++)";
				itemDef.zoom2d = 1347;
				itemDef.xan2d = 1589;
				itemDef.yan2d = 781;
				itemDef.yof2d = 3;
				itemDef.xof2d = -5;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", "Uncharge", "Drop" };
				itemDef.modelId = 60106;
				itemDef.manWear = 60107;
				itemDef.womanWear = 60107;
				itemDef.manwearxoff = -3;
				itemDef.manwearyoff = 6;
				itemDef.womanwearzoff = 5;
				itemDef.womanwearyoff = -5;
				break;

			case 30022:
				itemDef.name = "Dragon warhammer";
				itemDef.zoom2d = 1600;
				itemDef.xan2d = 1510;
				itemDef.yan2d = 1785;
				itemDef.yof2d = -4;
				itemDef.xof2d = 9;
				itemDef.modelId = 60108; // 4041;
				itemDef.manWear = 60109; // 4037;
				itemDef.womanwearzoff = 5;
				itemDef.womanwearyoff = -9;
				itemDef.manwearyoff = 5;
				itemDef.womanWear = 60110; // 4038;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop", };
				break;

			case 30023:
				itemDef.setDefaults();
				itemDef.name = "Heavy ballista";
				itemDef.description = "It's a Heavy ballista.".getBytes();
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 189;
				itemDef.yan2d = 148;
				itemDef.xof2d = 8;
				itemDef.yof2d = -18;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 60111; // 31523;
				itemDef.manWear = 60112; // 31236;
				itemDef.womanWear = 60112; // 31236;
				break;

			case 30024:
				itemDef.setDefaults();
				itemDef.name = "Light ballista";
				itemDef.description = "It's a Light ballista.".getBytes();
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 189;
				itemDef.yan2d = 148;
				itemDef.xof2d = 8;
				itemDef.yof2d = -18;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 60128; // 31522;
				itemDef.manWear = 60129; // 31237;
				itemDef.womanWear = 60129; // 31237;
				break;

			case 30025:
				itemDef.name = "Dragon javelin";
				itemDef.description = "It's a Dragon javelin.".getBytes();
				itemDef.zoom2d = 1400;
				itemDef.yan2d = 2009;
				itemDef.xan2d = 282;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.modelId = 60130; // 31497
				itemDef.stackable = true;
				break;

			case 30026:
				itemDef.name = "Ballista limbs";
				itemDef.description = "It's Ballista limbs.".getBytes();
				itemDef.zoom2d = 1100;
				itemDef.yan2d = 0;
				itemDef.xan2d = 512;
				itemDef.xof2d = 0;
				itemDef.yof2d = 4;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60131; // 31501
				break;

			case 30027:
				itemDef.setDefaults();
				itemDef.name = "Heavy frame";
				itemDef.description = "A heavy wooden frame.".getBytes();
				itemDef.zoom2d = 1095;
				itemDef.yan2d = 0;
				itemDef.xan2d = 579;
				itemDef.xof2d = 2;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60132; // 31512;
				break;

			case 30028:
				itemDef.setDefaults();
				itemDef.name = "Light frame";
				itemDef.description = "A light wooden frame.".getBytes();
				itemDef.zoom2d = 1095;
				itemDef.xan2d = 40;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60133; // 31508;
				break;

			case 30029:
				itemDef.setDefaults();
				itemDef.name = "Unstrung light ballista";
				itemDef.description = "Needs a suitably simian rope.".getBytes();
				itemDef.zoom2d = 1250;
				itemDef.xan2d = 72;
				itemDef.yan2d = 151;
				itemDef.xof2d = 13;
				itemDef.modelId = 60134; // 31522;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30030:
				itemDef.setDefaults();
				itemDef.name = "Unstrung heavy ballista";
				itemDef.description = "Needs a suitably simian rope.".getBytes();
				itemDef.zoom2d = 1250;
				itemDef.xan2d = 72;
				itemDef.yan2d = 151;
				itemDef.xof2d = 13;
				itemDef.modelId = 60135; // 31523;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30031:
				itemDef.setDefaults();
				itemDef.name = "Ballista spring";
				itemDef.description = "A spring-loaded mechanism.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 45;
				itemDef.yan2d = 1124;
				itemDef.yof2d = -11;
				itemDef.xof2d = 0;
				itemDef.modelId = 60136; // 31518;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30032:
				itemDef.setDefaults();
				itemDef.name = "Incomplete light ballista";
				itemDef.description = "The limbs have been attached to the frame.".getBytes();
				itemDef.zoom2d = 968;
				/*
				 * itemDef.xan2d = 144; itemDef.yan2d = 666; itemDef.yof2d = 24; itemDef.xof2d =
				 * 1;
				 */
				itemDef.xan2d = 81;
				itemDef.yan2d = 699;
				itemDef.yof2d = 0;
				itemDef.xof2d = 23;
				itemDef.modelId = 60137; // 31527;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30033:
				itemDef.name = "Incomplete heavy ballista";
				itemDef.description = "The limbs have been attached to the frame.".getBytes();
				itemDef.zoom2d = 968;
				itemDef.xan2d = 81;
				itemDef.yan2d = 699;
				itemDef.yof2d = 9;
				itemDef.xof2d = 13;
				itemDef.modelId = 60138; // 31505;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30034:
				itemDef.name = "Monkey tail";
				itemDef.description = "Smells like the south end of a northbound monkey, because it is.".getBytes();
				itemDef.zoom2d = 400;
				itemDef.xan2d = 99;
				itemDef.yan2d = 84;
				itemDef.yof2d = 0;
				itemDef.xof2d = -6;
				itemDef.modelId = 60139; // 31507;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30035:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.description = "It's not very strong, but it could be pointy if you put a pointy tip on it."
						.getBytes();
				itemDef.zoom2d = 1916;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1953;
				itemDef.modelId = 60140; // 31499;
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30036:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.description = "It's not very strong, but it could be pointy if you put a pointy tip on it."
						.getBytes();
				itemDef.zoom2d = 1916;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1953;
				itemDef.modelId = 60200; // 31509;
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30037:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.description = "It's not very strong, but it could be pointy if you put a pointy tip on it."
						.getBytes();
				itemDef.zoom2d = 1916;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1953;
				itemDef.modelId = 60201; // 31517;
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30038:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.description = "It's not very strong, but it could be pointy if you put a pointy tip on it."
						.getBytes();
				itemDef.zoom2d = 1916;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1953;
				itemDef.modelId = 60202; // 31520;
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30039:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.description = "It's not very strong, but it could be pointy if you put a pointy tip on it."
						.getBytes();
				itemDef.zoom2d = 1916;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1953;
				itemDef.modelId = 60203; // 31528;
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30040:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.description = "Needs a shaft.".getBytes();
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 431;
				itemDef.xof2d = -8;
				itemDef.modelId = 60141; // 31502;
				itemDef.recolorOriginal = new int[] { 61 };
				itemDef.recolorModified = new int[] { 10456 };
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30041:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.description = "Needs a shaft.".getBytes();
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 431;
				itemDef.xof2d = -8;
				itemDef.modelId = 60204; // 31529;
				itemDef.recolorOriginal = new int[] { 61 };
				itemDef.recolorModified = new int[] { 10456 };
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30042:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.description = "Needs a shaft.".getBytes();
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 431;
				itemDef.xof2d = -8;
				itemDef.modelId = 60205; // 31521;
				itemDef.recolorOriginal = new int[] { 61 };
				itemDef.recolorModified = new int[] { 10456 };
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30043:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.description = "Needs a shaft.".getBytes();
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 431;
				itemDef.xof2d = -8;
				itemDef.modelId = 60206; // 31516;
				itemDef.recolorOriginal = new int[] { 61 };
				itemDef.recolorModified = new int[] { 10456 };
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30044:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.description = "Needs a shaft.".getBytes();
				itemDef.zoom2d = 1284;
				itemDef.xan2d = 431;
				itemDef.xof2d = -8;
				itemDef.modelId = 60207; // 31503;
				itemDef.recolorOriginal = new int[] { 61 };
				itemDef.recolorModified = new int[] { 10456 };
				itemDef.stackable = true;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30045:
				itemDef.name = "Platinum token";
				itemDef.zoom2d = 550;
				itemDef.xan2d = 202;
				itemDef.yan2d = 1764;
				itemDef.zan2d = 108;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.recolorOriginal = new int[] { 5813, 9139, 26006 };
				itemDef.recolorModified = new int[] { -32667, -27566, -27554 };
				itemDef.modelId = 15343;
				itemDef.stackable = true;
				itemDef.stackIDs = new int[] { 30112, 30113, 30114, 30115, 0, 0, 0, 0, 0, 0 };
				itemDef.stackAmounts = new int[] { 2, 3, 4, 5, 0, 0, 0, 0, 0, 0 };
				itemDef.cost = 1000;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30112:
				itemDef.name = "Platinum token";
				itemDef.zoom2d = 530;
				itemDef.xan2d = 364;
				itemDef.yan2d = 96;
				itemDef.xof2d = 2;
				itemDef.yof2d = 19;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.recolorOriginal = new int[] { 5813, 9139, 26006 };
				itemDef.recolorModified = new int[] { -32667, -27566, -27554 };
				itemDef.modelId = 15344;
				itemDef.stackable = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30113:
				itemDef.name = "Platinum token";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 337;
				itemDef.yan2d = 754;
				itemDef.xof2d = 7;
				itemDef.yof2d = -22;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.recolorOriginal = new int[] { 5813, 9139, 26006 };
				itemDef.recolorModified = new int[] { -32667, -27566, -27554 };
				itemDef.modelId = 15345;
				itemDef.stackable = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30114:
				itemDef.name = "Platinum token";
				itemDef.zoom2d = 589;
				itemDef.xan2d = 350;
				itemDef.yan2d = 795;
				itemDef.xof2d = 4;
				itemDef.yof2d = -14;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.recolorOriginal = new int[] { 5813, 9139, 26006 };
				itemDef.recolorModified = new int[] { -32667, -27566, -27554 };
				itemDef.modelId = 15346;
				itemDef.stackable = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30115:
				itemDef.name = "Platinum token";
				itemDef.zoom2d = 560;
				itemDef.xan2d = 471;
				itemDef.yan2d = 687;
				itemDef.xof2d = 8;
				itemDef.yof2d = -7;
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.recolorOriginal = new int[] { 5813, 9139, 26006 };
				itemDef.recolorModified = new int[] { -32667, -27566, -27554 };
				itemDef.modelId = 15347;
				itemDef.stackable = true;
				itemDef.cost = 1000;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30046:
				itemDef.name = "Saradomin's tear";
				itemDef.zoom2d = 905;
				itemDef.xan2d = 512;
				itemDef.yof2d = -4;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60114; // 8959;
				break;

			case 30047:
				itemDef.setDefaults();
				itemDef.name = "Rune pouch";
				itemDef.zoom2d = 350;
				itemDef.xan2d = 512;
				itemDef.yan2d = 475;
				itemDef.xof2d = -4;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { "Open", null, null, "Empty", "Destroy" };
				itemDef.modelId = 60115; // 5453;
				break;

			case 30048:
				itemDef.setDefaults();
				itemDef.name = "Looting bag";
				itemDef.zoom2d = 740;
				itemDef.xan2d = 67;
				itemDef.yan2d = 471;
				itemDef.yof2d = -4;
				itemDef.recolorOriginal = new int[] { 6430, 7467, 6798, 7223 };
				itemDef.recolorModified = new int[] { -32729, -32738, -32750, -6867 };
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { "Check", null, "Deposit", null, "Destroy" };
				itemDef.modelId = 60127; // 7508;
				break;

			case 30049:
				itemDef.name = "Dragon pickaxe upgrade kit";
				itemDef.zoom2d = 1310;
				itemDef.xan2d = 163;
				itemDef.yan2d = 73;
				itemDef.recolorOriginal = new int[] { -32709, 10295, 10304, 10287, 10275, 10283 };
				itemDef.recolorModified = new int[] { 171, 1447, 1455, 1566, 1938, 1690 };
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60116; // 4847;
				break;

			case 30050:
				itemDef.name = "Frozen whip mix";
				itemDef.zoom2d = 842;
				itemDef.yof2d = 0;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60121; // 8957;
				break;

			case 30051:
				itemDef.name = "Volcanic whip mix";
				itemDef.zoom2d = 842;
				itemDef.yof2d = 0;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60120; // 8956;
				break;

			case 30052:
				itemDef.name = "Gilded scimitar";
				itemDef.zoom2d = 1180;
				itemDef.xan2d = 300;
				itemDef.yan2d = 1120;
				itemDef.xof2d = -6;
				itemDef.yof2d = 4;
				itemDef.recolorOriginal = new int[] { 61 };
				itemDef.recolorModified = new int[] { 7114 };
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.modelId = 60122; // 2373;
				itemDef.manWear = 60123; // 490;
				itemDef.womanwearyoff = -4;
				itemDef.manwearyoff = 6;
				itemDef.manwearzoff = -7;
				itemDef.womanWear = 60123; // 490;
				break;

			case 30053:
				itemDef.setDefaults();
				itemDef.name = "Bucket helm (g)";
				itemDef.description = "A helm made from a golden bucket.".getBytes();
				itemDef.zoom2d = 750;
				itemDef.manWear = 60146; // 31759;
				itemDef.womanWear = 60145; // 31833;
				itemDef.modelId = 60144; // 31967;
				itemDef.womanwearzoff = 6;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null };
				break;

			case 30054:
				itemDef.name = "Dragon pickaxe (or)";
				itemDef.zoom2d = 1663;
				itemDef.xan2d = 512;
				itemDef.yan2d = 929;
				itemDef.xof2d = 3;
				itemDef.yof2d = -1;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Revert" };
				itemDef.modelId = 60117; // 9597;
				itemDef.manWear = 60118; // 8962;
				itemDef.womanWear = 60118; // 8962;
				itemDef.manwearyoff = 6;
				itemDef.womanwearyoff = -4;
				break;

			case 30055:
				itemDef.name = "Magic shortbow scroll";
				itemDef.zoom2d = 842;
				itemDef.xan2d = 404;
				itemDef.yan2d = 1064;
				itemDef.xof2d = 1;
				itemDef.recolorOriginal = new int[] { 6464, 6587, 6604, 6608, 6740 };
				itemDef.recolorModified = new int[] { 6699, 4399, 5437, 5442, 5446 };
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.modelId = 60119; // 3374;
				break;

			case 30056:
				itemDef.name = "Magic shortbow (i)";
				itemDef.zoom2d = 1200;
				itemDef.xan2d = 508;
				itemDef.yan2d = 124;
				itemDef.xof2d = 7;
				itemDef.yof2d = 2;
				itemDef.recolorOriginal = new int[] { 6674 };
				itemDef.recolorModified = new int[] { 31516 };
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.modelId = 2562;
				itemDef.manWear = 512;
				itemDef.womanWear = 512;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 8;
				itemDef.womanwearxoff = 4;
				break;

			case 30057:
				itemDef.name = "Lava dragon mask";
				itemDef.zoom2d = 905;
				itemDef.xan2d = 202;
				itemDef.yan2d = 121;
				itemDef.xof2d = -1;
				itemDef.yof2d = 22;
				itemDef.options = new String[] { null, null, "Take", null, null };
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = /* 60124; */ 28714;
				itemDef.manWear = /* 60125; */ 28512;
				itemDef.womanWear = /* 60126; */ 28581;
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -7;
				itemDef.manwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30058:
				itemDef.setDefaults();
				itemDef.name = "Black h'ween mask";
				itemDef.description = "Aaaarrrghhh, I'm an old-school monster!".getBytes();
				itemDef.zoom2d = 730;
				itemDef.xan2d = 516;
				itemDef.xof2d = -10;
				itemDef.manWear = /* 60149; */ 3188;
				itemDef.womanWear = /* 60148; */ 3192;
				itemDef.modelId = /* 60147; */ 2438;
				itemDef.xof2d = 0;
				itemDef.yof2d = -10;
				itemDef.recolorOriginal = new int[] { 926, 0 };
				itemDef.recolorModified = new int[] { 8, 9152 };
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Destroy" };
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -7;
				itemDef.manwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30059:
				itemDef.name = "Rainbow partyhat";
				itemDef.description = "A colourful hat produced by Shanty Claws.".getBytes();
				itemDef.zoom2d = 440;
				itemDef.xan2d = 76;
				itemDef.yan2d = 1852;
				itemDef.yof2d = 1;
				itemDef.xof2d = 1;
				itemDef.manWear = /* 60152; */ 16246;
				itemDef.womanWear = /* 60151; */ 16248;
				itemDef.modelId = /* 60150; */ 16252;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Destroy" };
				itemDef.womanwearyoff = -10;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -7;
				itemDef.manwearzoff = 4;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30060:
				itemDef.setDefaults();
				itemDef.name = "3rd age cloak";
				itemDef.description = "A beautiful cloak woven by ancient tailors.".getBytes();
				itemDef.xan2d = 282;
				itemDef.yan2d = 962;
				itemDef.manWear = 60155; // 28483;
				itemDef.womanWear = 60154; // 28560;
				itemDef.modelId = 60153; // 28648;
				itemDef.manwearyoff = -5;
				itemDef.manwearzoff = 4;
				itemDef.womanwearyoff = -13;
				itemDef.womanwearzoff = 6;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null };
				break;

			case 30061:
				itemDef.name = "3rd age bow";
				itemDef.description = "A beautifully crafted bow carved by ancient archers.".getBytes();
				itemDef.zoom2d = 1500;
				itemDef.xan2d = 300;
				itemDef.yan2d = 200;
				itemDef.yof2d = 35;
				itemDef.xof2d = -20;
				itemDef.manWear = 60157; // 28622;
				itemDef.womanWear = 60157; // 28622;
				itemDef.modelId = 60156; // 28678;
				itemDef.womanwearzoff = 5;
				itemDef.womanwearyoff = -3;
				itemDef.manwearyoff = 10;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				break;

			case 30062:
				itemDef.setDefaults();
				itemDef.name = "3rd age longsword";
				itemDef.description = "A beautifully crafted sword forged by ancient blacksmiths.".getBytes();
				itemDef.zoom2d = 1726;
				itemDef.xan2d = 1576;
				itemDef.yan2d = 242;
				itemDef.yof2d = 5;
				itemDef.xof2d = 4;
				itemDef.manwearyoff = 7;
				itemDef.womanwearyoff = -2;
				itemDef.manWear = 28618; // 28618;
				itemDef.womanWear = 28618; // 28618;
				itemDef.modelId = 28633; // 28633;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30063:
				itemDef.setDefaults();
				itemDef.name = "3rd age wand";
				itemDef.description = "A beautifully crafted wand infused by ancient wizards.".getBytes();
				itemDef.zoom2d = 1347;
				itemDef.xan2d = 1468;
				itemDef.yan2d = 1805;
				itemDef.xof2d = 1;
				itemDef.womanwearzoff = 3;
				itemDef.womanwearyoff = -4;
				itemDef.manwearyoff = 6;
				itemDef.manwearzoff = -4;
				itemDef.manWear = 28619; // 28619;
				itemDef.womanWear = 28619; // 28619;
				itemDef.modelId = 28654; // 28654;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30064:
				itemDef.setDefaults();
				itemDef.name = "3rd age pickaxe";
				itemDef.description = "A beautifully crafted pickaxe, shaped by ancient smiths.".getBytes();
				itemDef.zoom2d = 1095;
				itemDef.xan2d = 224;
				itemDef.yan2d = 1056;
				itemDef.yof2d = 3;
				itemDef.xof2d = 7;
				itemDef.manWear = 31887; // 31887;
				itemDef.womanWear = 31894; // 31894;
				itemDef.modelId = 31962; // 31962;
				itemDef.manwearyoff = 6;
				itemDef.womanwearyoff = -13;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30065:
				itemDef.setDefaults();
				itemDef.name = "3rd age axe";
				itemDef.description = "A beautifully crafted axe, shaped by ancient smiths.".getBytes();
				itemDef.zoom2d = 968;
				itemDef.xan2d = 520;
				itemDef.yan2d = 1288;
				itemDef.yof2d = -1;
				itemDef.xof2d = 1;
				itemDef.manWear = 31891; // 31891;
				itemDef.womanWear = 31888; // 31888;
				itemDef.modelId = 31961; // 31961;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30066:
				itemDef.name = "Granite maul";
				itemDef.description = "Simplicity is the best weapon.".getBytes();
				itemDef.zoom2d = 1789;
				itemDef.xan2d = 157;
				itemDef.yan2d = 148;
				itemDef.yof2d = -5;
				itemDef.xof2d = 0;
				itemDef.manWear = 28992; // 28992;
				itemDef.womanWear = 28992; // 28992;
				itemDef.modelId = 28990; // 28990;
				itemDef.manwearyoff = 8;
				itemDef.manwearzoff = -5;
				itemDef.womanwearyoff = -4;
				itemDef.womanwearzoff = 3;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Revert" };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30067:
				itemDef.setDefaults();
				itemDef.name = "Granite clamp";
				itemDef.description = "A strong clamp, used for hardening granite maul blocks.".getBytes();
				itemDef.zoom2d = 1280;
				itemDef.yof2d = 5;
				itemDef.xof2d = 0;
				itemDef.modelId = 28994; // 28994;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30068:
				itemDef.name = "Infernal harpoon";
				itemDef.description = "A very powerful, fiery harpoon.".getBytes();
				itemDef.zoom2d = 1550;
				itemDef.xan2d = 600;
				itemDef.yan2d = 54;
				itemDef.yof2d = 5;
				itemDef.xof2d = 3;
				itemDef.manWear = 32673; // 32673;
				itemDef.womanWear = 32673; // 32673;
				itemDef.modelId = 32785; // 32785;
				itemDef.womanwearyoff = -6;
				itemDef.manwearyoff = 5;
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30069:
				itemDef.setDefaults();
				itemDef.name = "Dragon harpoon";
				itemDef.description = "A very powerful harpoon.".getBytes();
				itemDef.zoom2d = 1347;
				itemDef.xan2d = 600;
				itemDef.yan2d = 54;
				itemDef.yof2d = 4;
				itemDef.xof2d = 2;
				itemDef.womanwearyoff = -6;
				itemDef.manwearyoff = 5;
				itemDef.manWear = 32670; // 32670;
				itemDef.womanWear = 32670; // 32670;
				itemDef.modelId = 32796; // 32796;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30070:
				itemDef.setDefaults();
				itemDef.name = "Slayer helmet";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.manWear = 16274; // 16274;
				itemDef.womanWear = 16276; // 16276;
				itemDef.modelId = 16269; // 16269;
				itemDef.recolorOriginal = new int[] { 37925, 10332, 50244, 50244, 50227, 54311 };
				itemDef.recolorModified = new int[] { 33, 10320, 59, 59, 47, 47 };
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30071:
				itemDef.name = "Slayer helmet (i)";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manWear = 16274; // 16274;
				itemDef.womanWear = 16276; // 16276;
				itemDef.modelId = 16269; // 16269;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30072:
				itemDef.name = "Black slayer helmet";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manhead = 31558;
				itemDef.manWear = 31549;
				itemDef.womanhead = 31558;
				itemDef.womanWear = 31552;
				itemDef.modelId = 31545;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30073:
				itemDef.name = "Black slayer helmet (i)";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manhead = 31558;
				itemDef.manWear = 31549;
				itemDef.womanhead = 31558;
				itemDef.womanWear = 31552;
				itemDef.modelId = 31545;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30074:
				itemDef.name = "Green slayer helmet";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.manhead = 31557;
				itemDef.manWear = 31548;
				itemDef.womanhead = 31557;
				itemDef.womanWear = 31553;
				itemDef.modelId = 31546;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30075:
				itemDef.name = "Green slayer helmet (i)";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.yan2d = 1773;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manhead = 31557;
				itemDef.manWear = 31548;
				itemDef.womanhead = 31557;
				itemDef.womanWear = 31553;
				itemDef.modelId = 31546;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30076:
				itemDef.name = "Red slayer helmet";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manhead = 31556;
				itemDef.manWear = 31550;
				itemDef.womanhead = 31551;
				itemDef.womanWear = 31551;
				itemDef.modelId = 31547;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30077:
				itemDef.name = "Red slayer helmet (i)";
				itemDef.description = "You really don't want to wear it inside-out.".getBytes();
				itemDef.zoom2d = 779;
				itemDef.xan2d = 30;
				itemDef.yan2d = 1773;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.yof2d = -4;
				itemDef.xof2d = -3;
				itemDef.manhead = 31556;
				itemDef.manWear = 31550;
				itemDef.womanhead = 31551;
				itemDef.womanWear = 31551;
				itemDef.modelId = 31547;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30081:
				itemDef.name = "Unholy helmet";
				itemDef.description = "Unlocks the Unholy helmet".getBytes();
				itemDef.zoom2d = 960;
				itemDef.xan2d = 512;
				itemDef.yan2d = 512;
				itemDef.xof2d = 3;
				itemDef.modelId = 10137;
				itemDef.recolorOriginal = new int[] { 6464, 6587, 6604, 6608, 6583 };
				itemDef.recolorModified = new int[] { 938, 938, 930, 920, 937 };
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30082:
				itemDef.name = "King black bonnet";
				itemDef.description = "Unlocks the King black bonnet".getBytes();
				itemDef.zoom2d = 960;
				itemDef.xan2d = 512;
				itemDef.yan2d = 512;
				itemDef.xof2d = 3;
				itemDef.modelId = 10137;
				itemDef.recolorOriginal = new int[] { 6464, 6587, 6604, 6608, 6583 };
				itemDef.recolorModified = new int[] { 30, 30, 25, 15, 30 };
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30083:
				itemDef.name = "Kalhpite khat";
				itemDef.description = "Unlocks the Kalphite khat".getBytes();
				itemDef.zoom2d = 960;
				itemDef.xan2d = 512;
				itemDef.yan2d = 512;
				itemDef.xof2d = 3;
				itemDef.modelId = 10137; // 10137;
				itemDef.recolorOriginal = new int[] { 6464, 6587, 6604, 6608, 6583 };
				itemDef.recolorModified = new int[] { 21662, 21662, 21655, 21645, 21662 };
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;
			// RAID ITEMS

			case 30084:
				itemDef.name = "Ancestral robe bottom";
				itemDef.description = "The robe bottoms of a powerful sorceress from a bygone era.".getBytes();
				itemDef.zoom2d = 1690;
				itemDef.xan2d = 435;
				itemDef.yan2d = 9;
				itemDef.xof2d = -1;
				itemDef.yof2d = 7;
				itemDef.manWear = 32653; // 32653;
				itemDef.womanWear = 32662; // 32662;
				itemDef.modelId = 32787; // 32787;
				itemDef.womanwearyoff = -4;
				itemDef.womanwearzoff = 4;
				itemDef.manwearyoff = -2;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30085:
				itemDef.name = "Ancestral robe top";
				itemDef.description = "The robe top of a powerful sorceress from a bygone era.".getBytes();
				itemDef.zoom2d = 1358;
				itemDef.xan2d = 514;
				itemDef.yan2d = 2041;
				itemDef.yof2d = -3;
				itemDef.manWear = 32657;
				itemDef.manWear2 = 32658;
				itemDef.womanWear = 32664;
				itemDef.womanWear2 = 32665;
				itemDef.modelId = 32790;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null };
				break;

			case 30086:
				itemDef.name = "Ancestral hat";
				itemDef.description = "The hat of a powerful sorceress from a bygone era.".getBytes();
				itemDef.modelId = 32794;
				itemDef.xan2d = 118;
				itemDef.yan2d = 10;
				itemDef.zoom2d = 1236;
				itemDef.yof2d = -12;
				itemDef.manWear = 32655;
				itemDef.womanWear = 32663;
				itemDef.modelType = ModelTypeEnum.OSRS;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null };
				break;

			case 30087:
				itemDef.setDefaults();
				itemDef.name = "Dragon hunter crossbow";
				itemDef.description = "A crossbow used for dragon hunting.".getBytes();
				itemDef.zoom2d = 926;
				itemDef.xan2d = 432;
				itemDef.yan2d = 258;
				itemDef.yof2d = 9;
				itemDef.manWear = 32668;
				itemDef.womanWear = 32668;
				itemDef.modelId = 32797;
				itemDef.manwearyoff = 8;
				itemDef.manwearxoff = -3;
				itemDef.womanwearzoff = 8;
				itemDef.womanwearyoff = -4;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30088:
				itemDef.setDefaults();
				itemDef.name = "Dragon sword";
				itemDef.description = "A razor sharp sword.".getBytes();
				itemDef.zoom2d = 1168;
				itemDef.xan2d = 691;
				itemDef.yan2d = 478;
				itemDef.manWear = 32676;
				itemDef.womanWear = 32676;
				itemDef.modelId = 32791;
				itemDef.womanwearyoff = -3;
				itemDef.womanwearzoff = 3;
				itemDef.manwearyoff = 7;
				itemDef.manwearzoff = -3;
				itemDef.recolorOriginal = new int[] { 61 };
				itemDef.recolorModified = new int[] { 36133 };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30089:
				itemDef.setDefaults();
				itemDef.name = "Kodai wand";
				itemDef.description = "A wand of an ancient Kodai mage.".getBytes();
				itemDef.zoom2d = 668;
				itemDef.xan2d = 140;
				itemDef.yan2d = 1416;
				itemDef.manWear = 32669;
				itemDef.womanWear = 32669;
				itemDef.modelId = 32789;
				itemDef.manwearyoff = 7;
				itemDef.womanwearzoff = 5;
				itemDef.womanwearyoff = -6;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30090:
				itemDef.setDefaults();
				itemDef.name = "Elder maul";
				itemDef.description = "A maul crafted from the component parts of Tekton.".getBytes();
				itemDef.zoom2d = 1744;
				itemDef.xan2d = 237;
				itemDef.yan2d = 429;
				itemDef.manWear = 32678;
				itemDef.womanWear = 32678;
				itemDef.modelId = 32792;
				itemDef.womanwearyoff = -6;
				itemDef.womanwearzoff = 5;
				itemDef.manwearyoff = 6;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30091:
				itemDef.name = "Twisted buckler";
				itemDef.description = "A buckler carved from the twisted remains of the Great Olm.".getBytes();
				itemDef.zoom2d = 792;
				itemDef.xan2d = 494;
				itemDef.yan2d = 525;
				itemDef.yof2d = 1;
				itemDef.xof2d = 1;
				itemDef.manWear = 32667; // 32667;
				itemDef.womanWear = 32667; // 32667;
				itemDef.modelId = 32793; // 32793;
				itemDef.womanwearxoff = -4;
				itemDef.womanwearyoff = -6;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30092:
				itemDef.setDefaults();
				itemDef.name = "Twisted bow";
				itemDef.description = "A mystical bow carved from the twisted remains of the Great Olm.".getBytes();
				itemDef.xan2d = 720;
				itemDef.yan2d = 1500;
				itemDef.yof2d = -3;
				itemDef.xof2d = 1;
				itemDef.womanwearzoff = 5;
				itemDef.womanwearxoff = 4;
				itemDef.womanwearyoff = -5;
				itemDef.manwearyoff = 6;
				itemDef.manWear = 32674; // 32674;
				itemDef.womanWear = 32674; // 32674;
				itemDef.modelId = 32799; // 32799;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30093:
				itemDef.setDefaults();
				itemDef.name = "Dragon thrownaxe";
				itemDef.description = "A razor sharp throwing axe.".getBytes();
				itemDef.zoom2d = 912;
				itemDef.xan2d = 525;
				itemDef.yan2d = 1799;
				itemDef.manWear = 32672; // 32672;
				itemDef.womanWear = 32672; // 32672;
				itemDef.modelId = 32788; // 32788;
				itemDef.manwearyoff = 9;
				itemDef.womanwearzoff = 5;
				itemDef.womanwearyoff = -3;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null };
				itemDef.stackable = true;
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30094:
				itemDef.name = "Dinh's bulwark";
				itemDef.description = "Is it... looking at me?".getBytes();
				itemDef.zoom2d = 2276;
				itemDef.xan2d = 402;
				itemDef.yan2d = 1409;
				itemDef.xof2d = 4;
				itemDef.yof2d = 4;
				itemDef.manWear = 32671; // 32671;
				itemDef.womanWear = 32671; // 32671;
				itemDef.modelId = 32801; // 32801;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30095:
				itemDef.name = "Dexterous prayer scroll";
				itemDef.description = "Scrawled words that can invoke the power of the gods.".getBytes();
				itemDef.zoom2d = 1032;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1454;
				itemDef.yof2d = -3;
				itemDef.modelId = 32770;
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30096:
				itemDef.name = "Arcane prayer scroll";
				itemDef.description = "Scrawled words that can invoke the power of the gods.".getBytes();
				itemDef.zoom2d = 1032;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1454;
				itemDef.yof2d = -3;
				itemDef.modelId = 32770;
				itemDef.recolorModified = new int[] { 5351 };
				itemDef.recolorOriginal = new int[] { 5563 };
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30097:
				itemDef.name = "Torn prayer scroll";
				itemDef.description = "Scrawled words that can invoke the power of the gods.".getBytes();
				itemDef.zoom2d = 1032;
				itemDef.xan2d = 364;
				itemDef.yan2d = 1454;
				itemDef.yof2d = -3;
				itemDef.modelId = 32770;
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			// END OF RAID ITEMS

			case 30098:
				itemDef.name = "Toxic staff (uncharged)";
				itemDef.description = "A ghastly weapon with evil origins, with a toxic fang attached.".getBytes();
				itemDef.zoom2d = 2150;
				itemDef.xan2d = 512;
				itemDef.yan2d = 1010;
				itemDef.manWear = 14402;
				itemDef.womanWear = 14402;
				itemDef.modelId = 19224;
				itemDef.manwearyoff = 7;
				itemDef.womanwearyoff = -4;
				itemDef.recolorOriginal = new int[] { 16309, 55225, 55219, 55207, 54715, 127, 10475, 10448, 10462,
						54585, 16578 };// missing recolouring
				itemDef.recolorModified = new int[] { ItemDef.GREY, ItemDef.BLACK, ItemDef.GREY, ItemDef.BLACK,
						ItemDef.GREY, ItemDef.GREY, ItemDef.BLACK, ItemDef.BLACK, ItemDef.BLACK, ItemDef.GREY,
						ItemDef.GREY };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, "Dismantle", "Drop" };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 30099:
				itemDef.name = "Toxic staff (uncharged)";
				itemDef.description = "A ghastly weapon with evil origins, with a toxic fang attached.".getBytes();
				itemDef.zoom2d = 2150;
				itemDef.xan2d = 512;
				itemDef.yan2d = 1010;
				itemDef.manWear = 14404;
				itemDef.womanWear = 14404;
				itemDef.modelId = 19225;
				itemDef.recolorOriginal = new int[] { 17467 };
				itemDef.recolorModified = new int[] { 21947 };
				itemDef.inventoryOptions = new String[] { null, "Wield", null, "Dismantle", null };
				itemDef.modelType = ModelTypeEnum.OSRS;
				break;

			case 12502:
				itemDef.name = "Level-up Season Pass";
				itemDef.description = "Instantly Level up 5 Levels on your Season Pass.".getBytes();
				itemDef.inventoryOptions = new String[] { "Redeem", null, null, null, "Drop" };
				break;

			// 667+

			case 24324:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70553;
				itemDef.manWear = 70559;
				itemDef.womanWear = 70568;
				itemDef.zoom2d = 592;
				itemDef.xan2d = 148;
				itemDef.yan2d = 121;
				itemDef.xof2d = 0;
				itemDef.yof2d = -1;
				itemDef.name = "Queen's guard hat";
				break;

			case 24325:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70554;
				itemDef.manWear = 70562;
				itemDef.womanWear = 70571;
				itemDef.zoom2d = 1382;
				itemDef.xan2d = 486;
				itemDef.yan2d = 0;
				itemDef.xof2d = 1;
				itemDef.yof2d = -1;
				itemDef.name = "Queen's guard shirt";
				break;

			case 24326:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70552;
				itemDef.manWear = 70560;
				itemDef.womanWear = 70569;
				itemDef.zoom2d = 1645;
				itemDef.xan2d = 458;
				itemDef.yan2d = 269;
				itemDef.xof2d = 1;
				itemDef.yof2d = 1;
				itemDef.name = "Queen's guard trousers";
				break;

			case 24327:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70555;
				itemDef.manWear = 70556;
				itemDef.womanWear = 70565;
				itemDef.zoom2d = 789;
				itemDef.xan2d = 229;
				itemDef.yan2d = 94;
				itemDef.xof2d = 4;
				itemDef.yof2d = -84;
				itemDef.name = "Queen's guard shoes";
				break;

			case 24328:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70551;
				itemDef.manWear = 70564;
				itemDef.womanWear = 70564;
				itemDef.zoom2d = 2171;
				itemDef.xan2d = 633;
				itemDef.yan2d = 13;
				itemDef.xof2d = 3;
				itemDef.yof2d = -5;
				itemDef.womanwearyoff = -10;
				itemDef.name = "Queen's guard staff";
				break;

			case 24329:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70547;
				itemDef.manWear = 70558;
				itemDef.womanWear = 70567;
				itemDef.zoom2d = 855;
				itemDef.xan2d = 229;
				itemDef.yan2d = 135;
				itemDef.xof2d = -3;
				itemDef.yof2d = -5;
				itemDef.name = "Dragon ceremonial hat";
				break;

			case 24330:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70573;
				itemDef.manWear = 70574;
				itemDef.womanWear = 70575;
				itemDef.zoom2d = 1316;
				itemDef.xan2d = 485;
				itemDef.yan2d = 0;
				itemDef.xof2d = 1;
				itemDef.yof2d = -1;
				itemDef.name = "Dragon ceremonial breastplate";
				break;

			case 24331:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70548;
				itemDef.manWear = 70561;
				itemDef.womanWear = 70570;
				itemDef.zoom2d = 1645;
				itemDef.xan2d = 485;
				itemDef.yan2d = 256;
				itemDef.xof2d = 3;
				itemDef.yof2d = 0;
				itemDef.name = "Dragon ceremonial greaves";
				break;

			case 24332:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70550;
				itemDef.manWear = 70557;
				itemDef.womanWear = 70566;
				itemDef.zoom2d = 658;
				itemDef.xan2d = 269;
				itemDef.yan2d = 229;
				itemDef.xof2d = 7;
				itemDef.yof2d = 72;
				itemDef.name = "Dragon ceremonial boots";
				break;

			case 24333:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70549;
				itemDef.manWear = 70563;
				itemDef.womanWear = 70563;
				itemDef.zoom2d = 2171;
				itemDef.xan2d = 512;
				itemDef.yan2d = 242;
				itemDef.xof2d = 5;
				itemDef.yof2d = 8;
				itemDef.name = "Dragon ceremonial cape";
				break;

			case 24334:
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.modelId = 70783;
				itemDef.manWear = 70781;
				itemDef.womanWear = 70782;
				itemDef.zoom2d = 592;
				itemDef.xan2d = 552;
				itemDef.yan2d = 242;
				itemDef.xof2d = -3;
				itemDef.yof2d = -3;
				itemDef.name = "Mad necklace";
				break;

			case 24338:
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check state", null, "Destroy" };
				itemDef.modelId = 70257;
				itemDef.manWear = 70671;
				itemDef.womanWear = 70671;
				itemDef.zoom2d = 1250;
				itemDef.xan2d = 432;
				itemDef.yan2d = 258;
				itemDef.xof2d = 3;
				itemDef.name = "Royal crossbow";
				break;

			case 24433:
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.modelId = 70899;
				itemDef.manWear = 70900;
				itemDef.womanWear = 70900;
				itemDef.zoom2d = 1897;
				itemDef.xan2d = 308;
				itemDef.yan2d = 520;
				itemDef.xof2d = 6;
				itemDef.yof2d = 1;
				itemDef.name = "Golden katana";
				break;
		}
	}

}
