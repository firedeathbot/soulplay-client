package com.soulplayps.client.node.animable.item;

import com.soulplayps.client.node.animable.Animable;
import com.soulplayps.client.node.animable.model.Model;

public final class Item extends Animable {

	@Override
	public final Model getRotatedModel() {
		ItemDef itemDef = ItemDef.forID(ID);
		return itemDef.getItemModelFinalised(anInt1559);
	}

	public Item() {
	}

	public int ID;
	public int x;
	public int y;
	public int anInt1559;
}
