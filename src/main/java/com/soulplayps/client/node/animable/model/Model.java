package com.soulplayps.client.node.animable.model;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.animable.Animable;
import com.soulplayps.client.node.animable.particles.Particle;
import com.soulplayps.client.node.animable.particles.ParticleAttachment;
import com.soulplayps.client.node.animable.particles.ParticleDefinition;
import com.soulplayps.client.node.animable.particles.ParticleVector;
import com.soulplayps.client.node.object.ObjectDef;
import com.soulplayps.client.node.ondemand.OnDemandFetcher;
import com.soulplayps.client.node.raster.Raster;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.VBORenderer;
import com.soulplayps.client.opengl.gl15.model.ModelConfig;
import com.soulplayps.client.opengl.gl15.model.ModelVBO;
import com.soulplayps.client.opengl.gl15.model.simple.ModelLoader;
import com.soulplayps.client.opengl.gl15.particle.ParticleGenerator;
import com.soulplayps.client.opengl.model.DrawableModel;
import com.soulplayps.client.opengl.util.VBOManager;
import com.soulplayps.client.unknown.Class18;
import com.soulplayps.client.unknown.Class36;
import com.soulplayps.client.unknown.ModelDef;
import com.soulplayps.client.unknown.Vertex;

import java.io.File;
import java.util.*;

public class Model extends Animable {

//	private final void removeColors(int[] colors) {
//		if (anIntArray1640 == null) {
//			return;
//		}
//		for (int triangle = 0; triangle < anInt1630; triangle++) {
//			if (triangle < anIntArray1640.length) {
//				for(int color : colors) {
//					if(anIntArray1640[triangle] == color) {
//						anIntArray1631[triangle] = 0;
//						anIntArray1632[triangle] = 0;
//						anIntArray1633[triangle] = 0;
//					}
//				}
//			}
//		}
//	}
	
	public void convertTexturesTo317(short[] textureIds, int[] texa, int[] texb, int[] texc, byte[] texture_coordinates, boolean osrs) {
		int set = 0;
		int set2 = 0;
		int max = Rasterizer.textureAmount;
		if (textureIds != null) {
			textureTrianglePIndex = new int[triangleCount];
			textureTriangleMIndex = new int[triangleCount];
			textureTriangleNIndex = new int[triangleCount];
			for (int i = 0; i < triangleCount; i++) {
				int textureId = textureIds[i];
				if (textureId == -1 && drawTypes[i] == 2) {
					colorValues[i] = 65535;
					drawTypes[i] = 0;
				}

				if (textureId >= max || textureId < 0 || textureId == 39 || textureId == 42) {
					drawTypes[i] = 0;
					continue;
				}

				if (textureId > 50 && !osrs) {//Osrs textures to not show new models fix
					continue;
				}

				drawTypes[i] = 2 + set2;
				set2 += 4;
				int a = triangleViewSpaceX[i];
				int b = triangleViewSpaceY[i];
				int c = triangleViewSpaceZ[i];
				colorValues[i] = textureId;
				int texture_type = -1;
				
				if (texture_coordinates != null) {
					texture_type = texture_coordinates[i] & 0xff;
					if (texture_type != 0xff) {
						if (texa[texture_type] >= anIntArray1669.length || texb[texture_type] >= anIntArray1668.length || texc[texture_type] >= anIntArray1670.length) {
							texture_type = -1;
						}
					}
				}
				
				if (texture_type == 0xff) {
					texture_type = -1;
				}
				
				textureTrianglePIndex[set] = texture_type == -1 ? a : texa[texture_type];
				textureTriangleMIndex[set] = texture_type == -1 ? b : texb[texture_type];
				textureTriangleNIndex[set++] = texture_type == -1 ? c : texc[texture_type];
			}
			texTriangleCount = set;
		}
	}
	
	public void read525Model(byte abyte0[], int modelID, boolean osrs) {
		RSBuffer nc1 = new RSBuffer(abyte0);
		RSBuffer nc2 = new RSBuffer(abyte0);
		RSBuffer nc3 = new RSBuffer(abyte0);
		RSBuffer nc4 = new RSBuffer(abyte0);
		RSBuffer nc5 = new RSBuffer(abyte0);
		RSBuffer nc6 = new RSBuffer(abyte0);
		RSBuffer nc7 = new RSBuffer(abyte0);
		nc1.currentOffset = abyte0.length - 23;
		int numVertices = nc1.readUnsignedWord();
		int numTriangles = nc1.readUnsignedWord();
		int numTexTriangles = nc1.readUnsignedByte();
		ModelDef ModelDef_1;
		if (osrs)
			ModelDef_1 = osrsModelDefCache[modelID] = new ModelDef();
		else
			ModelDef_1 = aClass21Array1661[modelID] = new ModelDef();
		ModelDef_1.aByteArray368 = abyte0;
		ModelDef_1.anInt369 = numVertices;
		ModelDef_1.anInt370 = numTriangles;
		ModelDef_1.anInt371 = numTexTriangles;
		int l1 = nc1.readUnsignedByte();
		boolean bool = (l1 & 0x1) != 0;
		int i2 = nc1.readUnsignedByte();
		int j2 = nc1.readUnsignedByte();
		int k2 = nc1.readUnsignedByte();
		int l2 = nc1.readUnsignedByte();
		int i3 = nc1.readUnsignedByte();
		int j3 = nc1.readUnsignedWord();
		int k3 = nc1.readUnsignedWord();
		int l3 = nc1.readUnsignedWord();
		int i4 = nc1.readUnsignedWord();
		int j4 = nc1.readUnsignedWord();
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		byte[] x = null;
		byte[] O = null;
		byte[] J = null;
		byte[] F = null;
		byte[] cb = null;
		byte[] gb = null;
		byte[] lb = null;
		int[] kb = null;
		int[] y = null;
		int[] N = null;
		short[] D = null;
		int[] triangleColours2;
		if (numTexTriangles > 0) {
			O = new byte[numTexTriangles];
			nc1.currentOffset = 0;
			for (int j5 = 0; j5 < numTexTriangles; j5++) {
				byte byte0 = O[j5] = nc1.readSignedByte();
				if (byte0 == 0)
					k4++;
				if (byte0 >= 1 && byte0 <= 3)
					l4++;
				if (byte0 == 2)
					i5++;
			}
		}
		int k5 = numTexTriangles;
		int l5 = k5;
		k5 += numVertices;
		int i6 = k5;
		if (bool)
			k5 += numTriangles;
		int j6 = k5;
		k5 += numTriangles;
		int k6 = k5;
		if (i2 == 255)
			k5 += numTriangles;
		int l6 = k5;
		if (k2 == 1)
			k5 += numTriangles;
		int i7 = k5;
		if (i3 == 1)
			k5 += numVertices;
		int j7 = k5;
		if (j2 == 1)
			k5 += numTriangles;
		int k7 = k5;
		k5 += i4;
		int l7 = k5;
		if (l2 == 1)
			k5 += numTriangles * 2;
		int i8 = k5;
		k5 += j4;
		int j8 = k5;
		k5 += numTriangles * 2;
		int k8 = k5;
		k5 += j3;
		int l8 = k5;
		k5 += k3;
		int i9 = k5;
		k5 += l3;
		int j9 = k5;
		k5 += k4 * 6;
		int k9 = k5;
		k5 += l4 * 6;
		int l9 = k5;
		k5 += l4 * 6;
		int i10 = k5;
		k5 += l4;
		int j10 = k5;
		k5 += l4;
		int k10 = k5;
		k5 += l4 + i5 * 2;
		initVertices(numVertices);
		int[] facePoint1 = new int[numTriangles];
		int[] facePoint2 = new int[numTriangles];
		int[] facePoint3 = new int[numTriangles];
		verticesParticle = new int[numVertices];
		anIntArray1655 = new int[numVertices];
		drawTypes = new int[numTriangles];
		trianglePriorities = new int[numTriangles];
		triangleAlphaValues = new int[numTriangles];
		anIntArray1656 = new int[numTriangles];
		if (i3 == 1)
			anIntArray1655 = new int[numVertices];
		if (bool)
			drawTypes = new int[numTriangles];
		if (i2 == 255)
			trianglePriorities = new int[numTriangles];
		if (j2 == 1)
			triangleAlphaValues = new int[numTriangles];
		if (k2 == 1)
			anIntArray1656 = new int[numTriangles];
		if (l2 == 1)
			D = new short[numTriangles];
		if (l2 == 1 && numTexTriangles > 0)
			x = new byte[numTriangles];
		triangleColours2 = new int[numTriangles];
		int[] texTrianglesPoint1 = null;
		int[] texTrianglesPoint2 = null;
		int[] texTrianglesPoint3 = null;
		if (numTexTriangles > 0) {
			texTrianglesPoint1 = new int[numTexTriangles];
			texTrianglesPoint2 = new int[numTexTriangles];
			texTrianglesPoint3 = new int[numTexTriangles];
			if (l4 > 0) {
				kb = new int[l4];
				N = new int[l4];
				y = new int[l4];
				gb = new byte[l4];
				lb = new byte[l4];
				F = new byte[l4];
			}
			if (i5 > 0) {
				cb = new byte[i5];
				J = new byte[i5];
			}
		}
		nc1.currentOffset = l5;
		nc2.currentOffset = k8;
		nc3.currentOffset = l8;
		nc4.currentOffset = i9;
		nc5.currentOffset = i7;
		float l10 = 0;
		float i11 = 0;
		float j11 = 0;
		for (int k11 = 0; k11 < numVertices; k11++) {
			int l11 = nc1.readUnsignedByte();
			float j12 = 0;
			if ((l11 & 1) != 0)
				j12 = nc2.method421();
			float l12 = 0;
			if ((l11 & 2) != 0)
				l12 = nc3.method421();
			float j13 = 0;
			if ((l11 & 4) != 0)
				j13 = nc4.method421();
			setVertexX(k11, l10 + j12);
			setVertexY(k11, i11 + l12);
			setVertexZ(k11, j11 + j13);
			l10 = getVertexX(k11);
			i11 = getVertexY(k11);
			j11 = getVertexZ(k11);
			if (anIntArray1655 != null)
				anIntArray1655[k11] = nc5.readUnsignedByte();
		}
		nc1.currentOffset = j8;
		nc2.currentOffset = i6;
		nc3.currentOffset = k6;
		nc4.currentOffset = j7;
		nc5.currentOffset = l6;
		nc6.currentOffset = l7;
		nc7.currentOffset = i8;
		for (int i12 = 0; i12 < numTriangles; i12++) {
			triangleColours2[i12] = nc1.readUnsignedWord();
			if (bool) {
				drawTypes[i12] = nc2.readSignedByte();
				if (drawTypes[i12] == 2)
					triangleColours2[i12] = 65535;
				drawTypes[i12] = 0;
			}
			if (i2 == 255) {
				trianglePriorities[i12] = nc3.readSignedByte();
			}
			if (j2 == 1) {
				triangleAlphaValues[i12] = nc4.readSignedByte();
				if (triangleAlphaValues[i12] < 0)
					triangleAlphaValues[i12] = (256 + triangleAlphaValues[i12]);
			}
			if (k2 == 1)
				anIntArray1656[i12] = nc5.readUnsignedByte();
			if (l2 == 1)
				D[i12] = (short) (nc6.readUnsignedWord() - 1);
			if (x != null)
				if (D[i12] != -1)
					x[i12] = (byte) (nc7.readUnsignedByte() - 1);
				else
					x[i12] = -1;
		}
		nc1.currentOffset = k7;
		nc2.currentOffset = j6;
		int k12 = 0;
		int i13 = 0;
		int k13 = 0;
		int l13 = 0;
		for (int i14 = 0; i14 < numTriangles; i14++) {
			int j14 = nc2.readUnsignedByte();
			if (j14 == 1) {
				k12 = nc1.method421() + l13;
				l13 = k12;
				i13 = nc1.method421() + l13;
				l13 = i13;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
			if (j14 == 2) {
				i13 = k13;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
			if (j14 == 3) {
				k12 = k13;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
			if (j14 == 4) {
				int l14 = k12;
				k12 = i13;
				i13 = l14;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
		}
		nc1.currentOffset = j9;
		nc2.currentOffset = k9;
		nc3.currentOffset = l9;
		nc4.currentOffset = i10;
		nc5.currentOffset = j10;
		nc6.currentOffset = k10;
		for (int k14 = 0; k14 < numTexTriangles; k14++) {
			int i15 = O[k14] & 0xff;
			if (i15 == 0) {
				texTrianglesPoint1[k14] = nc1.readUnsignedWord();
				texTrianglesPoint2[k14] = nc1.readUnsignedWord();
				texTrianglesPoint3[k14] = nc1.readUnsignedWord();
			}
			if (i15 == 1) {
				texTrianglesPoint1[k14] = nc2.readUnsignedWord();
				texTrianglesPoint2[k14] = nc2.readUnsignedWord();
				texTrianglesPoint3[k14] = nc2.readUnsignedWord();
				kb[k14] = nc3.readUnsignedWord();
				N[k14] = nc3.readUnsignedWord();
				y[k14] = nc3.readUnsignedWord();
				gb[k14] = nc4.readSignedByte();
				lb[k14] = nc5.readSignedByte();
				F[k14] = nc6.readSignedByte();
			}
			if (i15 == 2) {
				texTrianglesPoint1[k14] = nc2.readUnsignedWord();
				texTrianglesPoint2[k14] = nc2.readUnsignedWord();
				texTrianglesPoint3[k14] = nc2.readUnsignedWord();
				kb[k14] = nc3.readUnsignedWord();
				N[k14] = nc3.readUnsignedWord();
				y[k14] = nc3.readUnsignedWord();
				gb[k14] = nc4.readSignedByte();
				lb[k14] = nc5.readSignedByte();
				F[k14] = nc6.readSignedByte();
				cb[k14] = nc6.readSignedByte();
				J[k14] = nc6.readSignedByte();
			}
			if (i15 == 3) {
				texTrianglesPoint1[k14] = nc2.readUnsignedWord();
				texTrianglesPoint2[k14] = nc2.readUnsignedWord();
				texTrianglesPoint3[k14] = nc2.readUnsignedWord();
				kb[k14] = nc3.readUnsignedWord();
				N[k14] = nc3.readUnsignedWord();
				y[k14] = nc3.readUnsignedWord();
				gb[k14] = nc4.readSignedByte();
				lb[k14] = nc5.readSignedByte();
				F[k14] = nc6.readSignedByte();
			}
		}
		if (i2 != 255) {
			for (int i12 = 0; i12 < numTriangles; i12++)
				trianglePriorities[i12] = i2;
		}
		colorValues = triangleColours2;
		vertexCount = numVertices;
		triangleCount = numTriangles;
		triangleViewSpaceX = facePoint1;
		triangleViewSpaceY = facePoint2;
		triangleViewSpaceZ = facePoint3;
		convertTexturesTo317(D, texTrianglesPoint1, texTrianglesPoint2, texTrianglesPoint3, x, osrs);
		textureTypes = O;
		textureBackgrounds = D;
		texturePointers = x;
		//filterTriangles();
	}
	
	public Model(int modelId, boolean osrs) {
		ModelDef modelData = osrs ? osrsModelDefCache[modelId] : aClass21Array1661[modelId];
		byte[] is = modelData.aByteArray368;
		if (is == null) return;
		
		if (is[is.length - 1] == -1 && is[is.length - 2] == -1)
			read622Model(is, modelId, osrs);
		else
			readOldModel(modelId, osrs);
		
		attachParticles(modelId);
		if (GraphicsDisplay.enabled) {
			calculateUV(false, modelId, modelData);
		}
//		if (newmodel[modelId]) {
//			scale21(2);
//			method475(0, 6, 0);
//		}
	}

	public void attachParticles(int modelId) {
		int[][] attachments = ParticleAttachment.getAttachments(modelId);
		if (attachments != null) {
			for (int n = 0; n < attachments.length; n++) {
				int[] attach = attachments[n];
				if (attach[0] == -1) {
					for (int z = 0; z < triangleViewSpaceX.length; ++z) {
						verticesParticle[triangleViewSpaceX[z]] = attach[1] + 1;
					}
				} else if (attach[0] == -2) {
					for (int z = 0; z < triangleViewSpaceY.length; ++z) {
						verticesParticle[triangleViewSpaceY[z]] = attach[1] + 1;
					}
				} else if (attach[0] == -3) {
					for (int z = 0; z < triangleViewSpaceZ.length; ++z) {
						verticesParticle[triangleViewSpaceZ[z]] = attach[1] + 1;
					}
				} else if (attach[0] == -4) {
					for (int z = 0; z < triangleViewSpaceX.length; ++z) {
						verticesParticle[triangleViewSpaceX[z]] = attach[1] + 1;
					}
					
					for (int z = 0; z < triangleViewSpaceY.length; ++z) {
						verticesParticle[triangleViewSpaceY[z]] = attach[1] + 1;
					}
					
					for (int z = 0; z < triangleViewSpaceZ.length; ++z) {
						verticesParticle[triangleViewSpaceZ[z]] = attach[1] + 1;
					}
				} else {
					verticesParticle[attach[0]] = attach[1] + 1;
				}
			}
		}
	}
	
	public void read622Model(byte abyte0[], int modelID, boolean osrs) {
		RSBuffer nc1 = new RSBuffer(abyte0);
		nc1.currentOffset = abyte0.length - 23;
		int numVertices = nc1.readUnsignedWord();
		int numTriangles = nc1.readUnsignedWord();
		int numTexTriangles = nc1.readUnsignedByte();
		int l1 = nc1.readUnsignedByte();
		boolean bool_26_ = (l1 & 0x8) != 0;
		if (!bool_26_) {
			read525Model(abyte0, modelID, osrs);
			return;
		}

		RSBuffer nc2 = new RSBuffer(abyte0);
		RSBuffer nc3 = new RSBuffer(abyte0);
		RSBuffer nc4 = new RSBuffer(abyte0);
		RSBuffer nc5 = new RSBuffer(abyte0);
		RSBuffer nc6 = new RSBuffer(abyte0);
		RSBuffer nc7 = new RSBuffer(abyte0);
		ModelDef ModelDef_1;
		if (osrs)
			ModelDef_1 = osrsModelDefCache[modelID] = new ModelDef();
		else
			ModelDef_1 = aClass21Array1661[modelID] = new ModelDef();
		ModelDef_1.aByteArray368 = abyte0;
		ModelDef_1.anInt369 = numVertices;
		ModelDef_1.anInt370 = numTriangles;
		ModelDef_1.anInt371 = numTexTriangles;
		boolean bool = (l1 & 0x1) != 0;
		int newformat = 0;
		if (bool_26_) {
			nc1.currentOffset -= 7;
			newformat = nc1.readUnsignedByte();
			nc1.currentOffset += 6;
		}
		int i2 = nc1.readUnsignedByte();
		int j2 = nc1.readUnsignedByte();
		int k2 = nc1.readUnsignedByte();
		int l2 = nc1.readUnsignedByte();
		int i3 = nc1.readUnsignedByte();
		int j3 = nc1.readUnsignedWord();
		int k3 = nc1.readUnsignedWord();
		int l3 = nc1.readUnsignedWord();
		int i4 = nc1.readUnsignedWord();
		int j4 = nc1.readUnsignedWord();
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		byte[] x = null;
		short[] D = null;
		int[] triangleColours2;
		if (numTexTriangles > 0) {
			textureTypes = new byte[numTexTriangles];
			nc1.currentOffset = 0;
			for (int j5 = 0; j5 < numTexTriangles; j5++) {
				byte byte0 = textureTypes[j5] = nc1.readSignedByte();
				if (byte0 == 0)
					k4++;
				if (byte0 >= 1 && byte0 <= 3)
					l4++;
				if (byte0 == 2)
					i5++;
			}
		}
		int k5 = numTexTriangles;
		int l5 = k5;
		k5 += numVertices;
		int i6 = k5;
		if (bool)
			k5 += numTriangles;
		int j6 = k5;
		k5 += numTriangles;
		int k6 = k5;
		if (i2 == 255)
			k5 += numTriangles;
		int l6 = k5;
		if (k2 == 1)
			k5 += numTriangles;
		int i7 = k5;
		if (i3 == 1)
			k5 += numVertices;
		int j7 = k5;
		if (j2 == 1)
			k5 += numTriangles;
		int k7 = k5;
		k5 += i4;
		int l7 = k5;
		if (l2 == 1)
			k5 += numTriangles * 2;
		int i8 = k5;
		k5 += j4;
		int j8 = k5;
		k5 += numTriangles * 2;
		int k8 = k5;
		k5 += j3;
		int l8 = k5;
		k5 += k3;
		int i9 = k5;
		k5 += l3;
		int j9 = k5;
		k5 += k4 * 6;
		int k9 = k5;
		k5 += l4 * 6;
		int i_59_ = 6;
		if (newformat != 14) {
			if (newformat >= 15)
				i_59_ = 9;
		} else
			i_59_ = 7;
		int l9 = k5;
		k5 += i_59_ * l4;
		int i10 = k5;
		k5 += l4;
		int j10 = k5;
		k5 += l4;
		int k10 = k5;
		k5 += l4 + i5 * 2;
		initVertices(numVertices);
		int[] facePoint1 = new int[numTriangles];
		int[] facePoint2 = new int[numTriangles];
		int[] facePoint3 = new int[numTriangles];
		verticesParticle = new int[numVertices];
		anIntArray1655 = new int[numVertices];
		drawTypes = new int[numTriangles];
		trianglePriorities = new int[numTriangles];
		triangleAlphaValues = new int[numTriangles];
		anIntArray1656 = new int[numTriangles];
		if (i3 == 1)
			anIntArray1655 = new int[numVertices];
		if (bool)
			drawTypes = new int[numTriangles];
		if (i2 == 255)
			trianglePriorities = new int[numTriangles];
		if (j2 == 1)
			triangleAlphaValues = new int[numTriangles];
		if (k2 == 1)
			anIntArray1656 = new int[numTriangles];
		if (l2 == 1)
			D = new short[numTriangles];
		if (l2 == 1 && numTexTriangles > 0)
			x = new byte[numTriangles];
		triangleColours2 = new int[numTriangles];
		int[] texTrianglesPoint1 = null;
		int[] texTrianglesPoint2 = null;
		int[] texTrianglesPoint3 = null;
		if (numTexTriangles > 0) {
			texTrianglesPoint1 = new int[numTexTriangles];
			texTrianglesPoint2 = new int[numTexTriangles];
			texTrianglesPoint3 = new int[numTexTriangles];
			if (l4 > 0) {
				particleDirectionX = new int[numTexTriangles];
				particleLifespanZ = new int[numTexTriangles];
				particleDirectionZ = new int[numTexTriangles];
				particleLifespanX = new int[numTexTriangles];
				particleLifespanY = new int[numTexTriangles];
				particleDirectionY = new int[numTexTriangles];
				texturePrimaryColor = new int[numTexTriangles];
				textureSecondaryColor = new int[numTexTriangles];
			}
			if (i5 > 0) {
				int pc = i5;
				i5 = numTexTriangles;
				texturePrimaryColor = new int[i5];
				textureSecondaryColor = new int[i5];
				i5 = pc;
			}
		}
		nc1.currentOffset = l5;
		nc2.currentOffset = k8;
		nc3.currentOffset = l8;
		nc4.currentOffset = i9;
		nc5.currentOffset = i7;
		float l10 = 0;
		float i11 = 0;
		float j11 = 0;
		for (int k11 = 0; k11 < numVertices; k11++) {
			int l11 = nc1.readUnsignedByte();
			float j12 = 0;
			if ((l11 & 1) != 0)
				j12 = nc2.method421();
			float l12 = 0;
			if ((l11 & 2) != 0)
				l12 = nc3.method421();
			float j13 = 0;
			if ((l11 & 4) != 0)
				j13 = nc4.method421();
			setVertexX(k11, l10 + j12);
			setVertexY(k11, i11 + l12);
			setVertexZ(k11, j11 + j13);
			l10 = getVertexX(k11);
			i11 = getVertexY(k11);
			j11 = getVertexZ(k11);
			if (anIntArray1655 != null)
				anIntArray1655[k11] = nc5.readUnsignedByte();
		}
		nc1.currentOffset = j8;
		nc2.currentOffset = i6;
		nc3.currentOffset = k6;
		nc4.currentOffset = j7;
		nc5.currentOffset = l6;
		nc6.currentOffset = l7;
		nc7.currentOffset = i8;
		for (int i12 = 0; i12 < numTriangles; i12++) {
			triangleColours2[i12] = nc1.readUnsignedWord();
			if (bool) {
				drawTypes[i12] = nc2.readSignedByte();
				if (drawTypes[i12] == 2)
					triangleColours2[i12] = 65535;
				drawTypes[i12] = 0;
			}
			if (i2 == 255) {
				trianglePriorities[i12] = nc3.readSignedByte();
			}
			if (j2 == 1) {
				triangleAlphaValues[i12] = nc4.readSignedByte();
				if (triangleAlphaValues[i12] < 0)
					triangleAlphaValues[i12] = (256 + triangleAlphaValues[i12]);
			}
			if (k2 == 1)
				anIntArray1656[i12] = nc5.readUnsignedByte();
			if (l2 == 1)
				D[i12] = (short) (nc6.readUnsignedWord() - 1);
			if (x != null)
				if (D[i12] == -1) {
					x[i12] = -1;
				} else {
					if (newformat >= 16)
						x[i12] = (byte) (nc7.readUSmart() - 1);
					else
						x[i12] = (byte) (nc7.readUnsignedByte() - 1);
				}
		}
		nc1.currentOffset = k7;
		nc2.currentOffset = j6;
		int k12 = 0;
		int i13 = 0;
		int k13 = 0;
		int l13 = 0;
		for (int i14 = 0; i14 < numTriangles; i14++) {
			int j14 = nc2.readUnsignedByte();
			if (j14 == 1) {
				k12 = nc1.method421() + l13;
				l13 = k12;
				i13 = nc1.method421() + l13;
				l13 = i13;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
			if (j14 == 2) {
				i13 = k13;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
			if (j14 == 3) {
				k12 = k13;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
			if (j14 == 4) {
				int l14 = k12;
				k12 = i13;
				i13 = l14;
				k13 = nc1.method421() + l13;
				l13 = k13;
				facePoint1[i14] = k12;
				facePoint2[i14] = i13;
				facePoint3[i14] = k13;
			}
		}
		nc1.currentOffset = j9;
		nc2.currentOffset = k9;
		nc3.currentOffset = l9;
		nc4.currentOffset = i10;
		nc5.currentOffset = j10;
		nc6.currentOffset = k10;
		for (int k14 = 0; k14 < numTexTriangles; k14++) {
			int i15 = textureTypes[k14] & 0xff;
			if (i15 == 0) {
				texTrianglesPoint1[k14] = nc1.readUnsignedWord();
				texTrianglesPoint2[k14] = nc1.readUnsignedWord();
				texTrianglesPoint3[k14] = nc1.readUnsignedWord();
			}
			if (i15 == 1) {
				texTrianglesPoint1[k14] = nc2.readUnsignedWord();
				texTrianglesPoint2[k14] = nc2.readUnsignedWord();
				texTrianglesPoint3[k14] = nc2.readUnsignedWord();
				if (newformat < 15) {
					particleDirectionX[k14] = nc3.readUnsignedWord();
					if (newformat >= 14)
						particleDirectionY[k14] = nc3.read3Bytes();
					else
						particleDirectionY[k14] = nc3.readUnsignedWord();
					particleDirectionZ[k14] = nc3.readUnsignedWord();
				} else {
					particleDirectionX[k14] = nc3.read3Bytes();
					particleDirectionY[k14] = nc3.read3Bytes();
					particleDirectionZ[k14] = nc3.read3Bytes();
				}
				particleLifespanX[k14] = nc4.readSignedByte();
				particleLifespanY[k14] = nc5.readSignedByte();
				particleLifespanZ[k14] = nc6.readSignedByte();
			}
			if (i15 == 2) {
				texTrianglesPoint1[k14] = nc2.readUnsignedWord();
				texTrianglesPoint2[k14] = nc2.readUnsignedWord();
				texTrianglesPoint3[k14] = nc2.readUnsignedWord();
				if (newformat >= 15) {
					particleDirectionX[k14] = nc3.read3Bytes();
					particleDirectionY[k14] = nc3.read3Bytes();
					particleDirectionZ[k14] = nc3.read3Bytes();
				} else {
					particleDirectionX[k14] = nc3.readUnsignedWord();
					if (newformat < 14)
						particleDirectionY[k14] = nc3.readUnsignedWord();
					else
						particleDirectionY[k14] = nc3.read3Bytes();
					particleDirectionZ[k14] = nc3.readUnsignedWord();
				}
				particleLifespanX[k14] = nc4.readSignedByte();
				particleLifespanY[k14] = nc5.readSignedByte();
				particleLifespanZ[k14] = nc6.readSignedByte();
				texturePrimaryColor[k14] = nc6.readSignedByte();
				textureSecondaryColor[k14] = nc6.readSignedByte();
			}
			if (i15 == 3) {
				texTrianglesPoint1[k14] = nc2.readUnsignedWord();
				texTrianglesPoint2[k14] = nc2.readUnsignedWord();
				texTrianglesPoint3[k14] = nc2.readUnsignedWord();
				if (newformat < 15) {
					particleDirectionX[k14] = nc3.readUnsignedWord();
					if (newformat < 14)
						particleDirectionY[k14] = nc3.readUnsignedWord();
					else
						particleDirectionY[k14] = nc3.read3Bytes();
					particleDirectionZ[k14] = nc3.readUnsignedWord();
				} else {
					particleDirectionX[k14] = nc3.read3Bytes();
					particleDirectionY[k14] = nc3.read3Bytes();
					particleDirectionZ[k14] = nc3.read3Bytes();
				}
				particleLifespanX[k14] = nc4.readSignedByte();
				particleLifespanY[k14] = nc5.readSignedByte();
				particleLifespanZ[k14] = nc6.readSignedByte();
			}
		}
		if (i2 != 255) {
			for (int i12 = 0; i12 < numTriangles; i12++)
				trianglePriorities[i12] = i2;
		}
		colorValues = triangleColours2;
		vertexCount = numVertices;
		triangleCount = numTriangles;
		triangleViewSpaceX = facePoint1;
		triangleViewSpaceY = facePoint2;
		triangleViewSpaceZ = facePoint3;
		convertTexturesTo317(D, texTrianglesPoint1, texTrianglesPoint2, texTrianglesPoint3, x, osrs);
		textureBackgrounds = D;
		texturePointers = x;
		//filterTriangles();
		downscale();
	}

	public byte[] textureTypes = null;
	public short[] textureBackgrounds = null;
	public byte[] texturePointers = null;
	
	private void readOldModel(int i, boolean osrs) {
		aBoolean1659 = false;
		ModelDef class21 = osrs ? osrsModelDefCache[i] : aClass21Array1661[i];
		vertexCount = class21.anInt369;
		triangleCount = class21.anInt370;
		texTriangleCount = class21.anInt371;
		verticesParticle = new int[vertexCount];
		initVertices(vertexCount);
		triangleViewSpaceX = new int[triangleCount];
		triangleViewSpaceY = new int[triangleCount];
		triangleViewSpaceZ = new int[triangleCount];
		textureTrianglePIndex = new int[texTriangleCount];
		textureTriangleMIndex = new int[texTriangleCount];
		textureTriangleNIndex = new int[texTriangleCount];
		if (class21.anInt376 >= 0)
			anIntArray1655 = new int[vertexCount];
		if (class21.anInt380 >= 0)
			drawTypes = new int[triangleCount];
		if (class21.anInt381 >= 0)
			trianglePriorities = new int[triangleCount];
		else
			anInt1641 = -class21.anInt381 - 1;
		if (class21.anInt382 >= 0)
			triangleAlphaValues = new int[triangleCount];
		if (class21.anInt383 >= 0)
			anIntArray1656 = new int[triangleCount];
		colorValues = new int[triangleCount];
		RSBuffer stream = new RSBuffer(class21.aByteArray368);
		stream.currentOffset = class21.anInt372;
		RSBuffer stream_1 = new RSBuffer(class21.aByteArray368);
		stream_1.currentOffset = class21.anInt373;
		RSBuffer stream_2 = new RSBuffer(class21.aByteArray368);
		stream_2.currentOffset = class21.anInt374;
		RSBuffer stream_3 = new RSBuffer(class21.aByteArray368);
		stream_3.currentOffset = class21.anInt375;
		RSBuffer stream_4 = new RSBuffer(class21.aByteArray368);
		stream_4.currentOffset = class21.anInt376;
		float k = 0;
		float l = 0;
		float i1 = 0;
		for (int j1 = 0; j1 < vertexCount; j1++) {
			int k1 = stream.readUnsignedByte();
			float i2 = 0;
			if ((k1 & 1) != 0)
				i2 = stream_1.method421();
			float k2 = 0;
			if ((k1 & 2) != 0)
				k2 = stream_2.method421();
			float i3 = 0;
			if ((k1 & 4) != 0)
				i3 = stream_3.method421();
			setVertexX(j1, k + i2);
			setVertexY(j1, l + k2);
			setVertexZ(j1, i1 + i3);
			k = getVertexX(j1);
			l = getVertexY(j1);
			i1 = getVertexZ(j1);
			if (anIntArray1655 != null)
				anIntArray1655[j1] = stream_4.readUnsignedByte();
		}
		stream.currentOffset = class21.anInt379;
		stream_1.currentOffset = class21.anInt380;
		stream_2.currentOffset = class21.anInt381;
		stream_3.currentOffset = class21.anInt382;
		stream_4.currentOffset = class21.anInt383;
		for (int l1 = 0; l1 < triangleCount; l1++) {
			colorValues[l1] = stream.readUnsignedWord();
			if (drawTypes != null)
				drawTypes[l1] = stream_1.readUnsignedByte();
			if (trianglePriorities != null)
				trianglePriorities[l1] = stream_2.readUnsignedByte();
			if (triangleAlphaValues != null) {
				triangleAlphaValues[l1] = stream_3.readUnsignedByte();
			}
			if (anIntArray1656 != null)
				anIntArray1656[l1] = stream_4.readUnsignedByte();
		}
		stream.currentOffset = class21.anInt377;
		stream_1.currentOffset = class21.anInt378;
		int j2 = 0;
		int l2 = 0;
		int j3 = 0;
		int k3 = 0;
		for (int l3 = 0; l3 < triangleCount; l3++) {
			int i4 = stream_1.readUnsignedByte();
			if (i4 == 1) {
				j2 = stream.method421() + k3;
				k3 = j2;
				l2 = stream.method421() + k3;
				k3 = l2;
				j3 = stream.method421() + k3;
				k3 = j3;
				triangleViewSpaceX[l3] = j2;
				triangleViewSpaceY[l3] = l2;
				triangleViewSpaceZ[l3] = j3;
			}
			if (i4 == 2) {
				l2 = j3;
				j3 = stream.method421() + k3;
				k3 = j3;
				triangleViewSpaceX[l3] = j2;
				triangleViewSpaceY[l3] = l2;
				triangleViewSpaceZ[l3] = j3;
			}
			if (i4 == 3) {
				j2 = j3;
				j3 = stream.method421() + k3;
				k3 = j3;
				triangleViewSpaceX[l3] = j2;
				triangleViewSpaceY[l3] = l2;
				triangleViewSpaceZ[l3] = j3;
			}
			if (i4 == 4) {
				int k4 = j2;
				j2 = l2;
				l2 = k4;
				j3 = stream.method421() + k3;
				k3 = j3;
				triangleViewSpaceX[l3] = j2;
				triangleViewSpaceY[l3] = l2;
				triangleViewSpaceZ[l3] = j3;
			}
		}
		stream.currentOffset = class21.anInt384;
		for (int j4 = 0; j4 < texTriangleCount; j4++) {
			textureTrianglePIndex[j4] = stream.readUnsignedWord();
			textureTriangleMIndex[j4] = stream.readUnsignedWord();
			textureTriangleNIndex[j4] = stream.readUnsignedWord();
		}
	}
	
	public static void method460(byte abyte0[], int j, boolean osrs) {
		try {
			if (abyte0 == null) {
				ModelDef class21;
				if (osrs)
					 class21 = osrsModelDefCache[j] = new ModelDef();
				else
					 class21 = aClass21Array1661[j] = new ModelDef();
				class21.anInt369 = 0;
				class21.anInt370 = 0;
				class21.anInt371 = 0;
				return;
			}
			RSBuffer stream = new RSBuffer(abyte0);
			stream.currentOffset = abyte0.length - 18;
			ModelDef class21_1;
			if (osrs)
				class21_1 = osrsModelDefCache[j] = new ModelDef();
			else
				class21_1 = aClass21Array1661[j] = new ModelDef();
			class21_1.aByteArray368 = abyte0;
			class21_1.anInt369 = stream.readUnsignedWord();
			class21_1.anInt370 = stream.readUnsignedWord();
			class21_1.anInt371 = stream.readUnsignedByte();
			int k = stream.readUnsignedByte();
			int l = stream.readUnsignedByte();
			int i1 = stream.readUnsignedByte();
			int j1 = stream.readUnsignedByte();
			int k1 = stream.readUnsignedByte();
			int l1 = stream.readUnsignedWord();
			int i2 = stream.readUnsignedWord();
			int j2 = stream.readUnsignedWord();
			int k2 = stream.readUnsignedWord();
			int l2 = 0;
			class21_1.anInt372 = l2;
			l2 += class21_1.anInt369;
			class21_1.anInt378 = l2;
			l2 += class21_1.anInt370;
			class21_1.anInt381 = l2;
			if (l == 255)
				l2 += class21_1.anInt370;
			else
				class21_1.anInt381 = -l - 1;
			class21_1.anInt383 = l2;
			if (j1 == 1)
				l2 += class21_1.anInt370;
			else
				class21_1.anInt383 = -1;
			class21_1.anInt380 = l2;
			if (k == 1)
				l2 += class21_1.anInt370;
			else
				class21_1.anInt380 = -1;
			class21_1.anInt376 = l2;
			if (k1 == 1)
				l2 += class21_1.anInt369;
			else
				class21_1.anInt376 = -1;
			class21_1.anInt382 = l2;
			if (i1 == 1)
				l2 += class21_1.anInt370;
			else
				class21_1.anInt382 = -1;
			class21_1.anInt377 = l2;
			l2 += k2;
			class21_1.anInt379 = l2;
			l2 += class21_1.anInt370 * 2;
			class21_1.anInt384 = l2;
			l2 += class21_1.anInt371 * 6;
			class21_1.anInt373 = l2;
			l2 += l1;
			class21_1.anInt374 = l2;
			l2 += i2;
			class21_1.anInt375 = l2;
			l2 += j2;
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}
	
	public static void method459(int i,
	                             OnDemandFetcher onDemandFetcher) {
		aClass21Array1661 = new ModelDef[80000];
		osrsModelDefCache = new ModelDef[65535];
		aOnDemandFetcher = onDemandFetcher;
	}
	
	public static void method461(int j) {
		aClass21Array1661[j] = null;
	}
	
	public static Model loadDropModel(int j) {
		return loadDropModel(j, ModelTypeEnum.REGULAR);
	}
	
	public static Model loadDropModel(int j, ModelTypeEnum modelType) {
		boolean osrs = modelType.isOsrs();
		ModelDef class21 = osrs ? osrsModelDefCache[j] : aClass21Array1661[j];
		if (class21 == null) {
			aOnDemandFetcher.loadData(modelType.getIndex(), j);
			return null;
		}

		return new Model(j, osrs);
	}
	
	public static boolean grabModelIds = false;
	public static ArrayList<Integer> temp = new ArrayList<Integer>();
	
	public static void copyModels() {
		for (int id : temp) {
			File file = new File("S:/Users/Lesik/Desktop/Random RSPS Data/727 cache/models/"+id+".gz");
			if (!file.exists()) {
				file = new File("S:/Users/Lesik/Desktop/RSPS Junk/659 Models/"+id+".gz");
				System.out.println("loaded model from 659 cache: "+id);
			}
			if (!file.exists()) {
				System.out.println("missing model id:"+id);
			} else {
				try {
					
					int fileIndex = Integer.parseInt(Client.getFileNameWithoutExtension(file.toString()));
					byte[] data = Client.fileToByteArray(file);
					if (data != null && data.length > 0) {
						Client.instance.fileStores[1].writeFile(data.length, data,
								fileIndex);
						System.out.println("Repacked " + fileIndex + ".");
					} else {
						System.out.println(
								"Unable to locate index " + fileIndex + ".");
					}
					
					// only copy files
//					FileUtils.copyFile(file, new File("C:/Users/Lesik/soulplaycacheTEST/index1grab/"+id+".gz"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		ObjectDef.mruNodes1.clear();
		ObjectDef.mruNodes2.clear();
	}
	
	public static boolean method463(int i) {
		return method463(i, ModelTypeEnum.REGULAR);
	}
	
	public static boolean method463(int i, ModelTypeEnum modelType) {//TODO what to do about the cache.
		boolean osrs = modelType.isOsrs();
		ModelDef class21 = osrs ? osrsModelDefCache[i] : aClass21Array1661[i];
		if (class21 != null) {
			return true;
		}

		if (grabModelIds && !temp.contains(i)) {
			temp.add(i);
			Collections.sort(temp);
			System.out.println("list:"+Arrays.toString(temp.toArray()));
		}

		aOnDemandFetcher.loadData(modelType.getIndex(), i);
		return false;
	}

	private Model() {
		/* empty */
	}

	public Model(int i, Model amodel[]) {
		try {
			aBoolean1659 = false;
			boolean flag = false;
			boolean flag1 = false;
			boolean flag2 = false;
			boolean flag3 = false;
			boolean hasTexturePointers = false;
			boolean hasTextures = false;
			boolean hasUv = false;
			vertexCount = 0;
			triangleCount = 0;
			texTriangleCount = 0;
			anInt1641 = -1;
			for (int k = 0; k < i; k++) {
				Model model = amodel[k];
				if (model != null) {
					vertexCount += model.vertexCount;
					triangleCount += model.triangleCount;
					texTriangleCount += model.texTriangleCount;
					flag |= model.drawTypes != null;
					if (model.trianglePriorities != null) {
						flag1 = true;
					} else {
						if (anInt1641 == -1)
							anInt1641 = model.anInt1641;
						if (anInt1641 != model.anInt1641)
							flag1 = true;
					}
					
					hasTexturePointers |= model.texturePointers != null;
					flag2 |= model.triangleAlphaValues != null;
					flag3 |= model.anIntArray1656 != null;
					hasTextures |= model.textureBackgrounds != null;
					hasUv |= model.uv != null;
				}
			}
			verticesParticle = new int[vertexCount];
			initVertices(vertexCount);
			anIntArray1655 = new int[vertexCount];
			triangleViewSpaceX = new int[triangleCount];
			triangleViewSpaceY = new int[triangleCount];
			triangleViewSpaceZ = new int[triangleCount];
			textureTrianglePIndex = new int[texTriangleCount];
			textureTriangleMIndex = new int[texTriangleCount];
			textureTriangleNIndex = new int[texTriangleCount];
			if (flag)
				drawTypes = new int[triangleCount];
			if (flag1)
				trianglePriorities = new int[triangleCount];
			if (flag2)
				triangleAlphaValues = new int[triangleCount];
			if (flag3)
				anIntArray1656 = new int[triangleCount];
			if (hasTexturePointers)
				texturePointers = new byte[triangleCount];
			if (hasTextures)
				textureBackgrounds = new short[triangleCount];
			colorValues = new int[triangleCount];
			if (hasUv) {
				uv = new float[triangleCount][6];
			}
			vertexCount = 0;
			triangleCount = 0;
			texTriangleCount = 0;
			int l = 0;
			for (int i1 = 0; i1 < i; i1++) {
				Model model_1 = amodel[i1];
				if (model_1 != null) {
					// int x = model_1.anIntArray1627[i1];
					// int y = model_1.anIntArray1628[i1];
					// int z = model_1.anIntArray1629[i1];
					// anIntArray1627[anInt1626] = x;
					// anIntArray1628[anInt1626] = y;
					// anIntArray1629[anInt1626] = z;
					// ++anInt1626;
					for (int j1 = 0; j1 < model_1.triangleCount; j1++) {
						if (hasUv) {
							if (model_1.uv == null) {
								uv[triangleCount][0] = 0;
								uv[triangleCount][1] = 0;
								uv[triangleCount][2] = 0;
								uv[triangleCount][3] = 0;
								uv[triangleCount][4] = 0;
								uv[triangleCount][5] = 0;
							} else {
								uv[triangleCount][0] = model_1.uv[j1][0];
								uv[triangleCount][1] = model_1.uv[j1][1];
								uv[triangleCount][2] = model_1.uv[j1][2];
								uv[triangleCount][3] = model_1.uv[j1][3];
								uv[triangleCount][4] = model_1.uv[j1][4];
								uv[triangleCount][5] = model_1.uv[j1][5];
							}
						}
						if (flag)
							if (model_1.drawTypes == null) {
								drawTypes[triangleCount] = 0;
							} else {
								int k1 = model_1.drawTypes[j1];
								if ((k1 & 2) == 2)
									k1 += l << 2;
								drawTypes[triangleCount] = k1;
							}
						if (flag1)
							if (model_1.trianglePriorities == null)
								trianglePriorities[triangleCount] = model_1.anInt1641;
							else
								trianglePriorities[triangleCount] = model_1.trianglePriorities[j1];
						if (flag2)
							if (model_1.triangleAlphaValues == null)
								triangleAlphaValues[triangleCount] = 0;
							else
								triangleAlphaValues[triangleCount] = model_1.triangleAlphaValues[j1];
						
						if (flag3 && model_1.anIntArray1656 != null)
							anIntArray1656[triangleCount] = model_1.anIntArray1656[j1];
						
						if (hasTextures) {
							if (model_1.textureBackgrounds == null)
								textureBackgrounds[triangleCount] = 0;
							else
								textureBackgrounds[triangleCount] = model_1.textureBackgrounds[j1];
						}
						
						colorValues[triangleCount] = model_1.colorValues[j1];
						triangleViewSpaceX[triangleCount] = method465(model_1, model_1.triangleViewSpaceX[j1]);
						triangleViewSpaceY[triangleCount] = method465(model_1, model_1.triangleViewSpaceY[j1]);
						triangleViewSpaceZ[triangleCount] = method465(model_1, model_1.triangleViewSpaceZ[j1]);
						triangleCount++;
					}
					
					for (int l1 = 0; l1 < model_1.texTriangleCount; l1++) {
						textureTrianglePIndex[texTriangleCount] = method465(model_1, model_1.textureTrianglePIndex[l1]);
						textureTriangleMIndex[texTriangleCount] = method465(model_1, model_1.textureTriangleMIndex[l1]);
						textureTriangleNIndex[texTriangleCount] = method465(model_1, model_1.textureTriangleNIndex[l1]);
						texTriangleCount++;
					}
					
					l += model_1.texTriangleCount;
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	public Model(Model amodel[]) {
		try {
			int i = 2;
			aBoolean1659 = false;
			boolean flag1 = false;
			boolean flag2 = false;
			boolean flag3 = false;
			boolean flag4 = false;
			boolean hasUv = false;
			vertexCount = 0;
			triangleCount = 0;
			texTriangleCount = 0;
			anInt1641 = -1;
			for (int k = 0; k < i; k++) {
				Model model = amodel[k];
				if (model != null) {
					vertexCount += model.vertexCount;
					triangleCount += model.triangleCount;
					texTriangleCount += model.texTriangleCount;
					flag1 |= model.drawTypes != null;
					if (model.trianglePriorities != null) {
						flag2 = true;
					} else {
						if (anInt1641 == -1)
							anInt1641 = model.anInt1641;
						if (anInt1641 != model.anInt1641)
							flag2 = true;
					}
					flag3 |= model.triangleAlphaValues != null;
					flag4 |= model.colorValues != null;
					hasUv |= model.uv != null;
				}
			}
			verticesParticle = new int[vertexCount];
			initVertices(vertexCount);
			triangleViewSpaceX = new int[triangleCount];
			triangleViewSpaceY = new int[triangleCount];
			triangleViewSpaceZ = new int[triangleCount];
			colorValues1 = new int[triangleCount];
			colorValues2 = new int[triangleCount];
			colorValues3 = new int[triangleCount];
			textureTrianglePIndex = new int[texTriangleCount];
			textureTriangleMIndex = new int[texTriangleCount];
			textureTriangleNIndex = new int[texTriangleCount];
			if (flag1)
				drawTypes = new int[triangleCount];
			if (flag2)
				trianglePriorities = new int[triangleCount];
			if (flag3)
				triangleAlphaValues = new int[triangleCount];
			if (flag4)
				colorValues = new int[triangleCount];
			if (hasUv) {
				uv = new float[triangleCount][6];
			}
			vertexCount = 0;
			triangleCount = 0;
			texTriangleCount = 0;

			int i1 = 0;
			for (int j1 = 0; j1 < i; j1++) {
				Model model_1 = amodel[j1];
				if (model_1 != null) {
					int k1 = vertexCount;
					for (int l1 = 0; l1 < model_1.vertexCount; l1++) {
						float x = model_1.getVertexX(l1);
						float y = model_1.getVertexY(l1);
						float z = model_1.getVertexZ(l1);
						setVertexX(vertexCount, x);
						setVertexY(vertexCount, y);
						setVertexZ(vertexCount, z);
						++vertexCount;
					}
					
					for (int i2 = 0; i2 < model_1.triangleCount; i2++) {
						if (hasUv) {
							if (model_1.uv == null) {
								uv[triangleCount][0] = 0;
								uv[triangleCount][1] = 0;
								uv[triangleCount][2] = 0;
								uv[triangleCount][3] = 0;
								uv[triangleCount][4] = 0;
								uv[triangleCount][5] = 0;
							} else {
								uv[triangleCount][0] = model_1.uv[i2][0];
								uv[triangleCount][1] = model_1.uv[i2][1];
								uv[triangleCount][2] = model_1.uv[i2][2];
								uv[triangleCount][3] = model_1.uv[i2][3];
								uv[triangleCount][4] = model_1.uv[i2][4];
								uv[triangleCount][5] = model_1.uv[i2][5];
							}
						}

						triangleViewSpaceX[triangleCount] = model_1.triangleViewSpaceX[i2] + k1;
						triangleViewSpaceY[triangleCount] = model_1.triangleViewSpaceY[i2] + k1;
						triangleViewSpaceZ[triangleCount] = model_1.triangleViewSpaceZ[i2] + k1;
						colorValues1[triangleCount] = model_1.colorValues1[i2];
						colorValues2[triangleCount] = model_1.colorValues2[i2];
						colorValues3[triangleCount] = model_1.colorValues3[i2];
						if (flag1)
							if (model_1.drawTypes == null) {
								drawTypes[triangleCount] = 0;
							} else {
								int j2 = model_1.drawTypes[i2];
								if ((j2 & 2) == 2)
									j2 += i1 << 2;
								drawTypes[triangleCount] = j2;
							}
						if (flag2)
							if (model_1.trianglePriorities == null)
								trianglePriorities[triangleCount] = model_1.anInt1641;
							else
								trianglePriorities[triangleCount] = model_1.trianglePriorities[i2];
						if (flag3)
							if (model_1.triangleAlphaValues == null)
								triangleAlphaValues[triangleCount] = 0;
							else
								triangleAlphaValues[triangleCount] = model_1.triangleAlphaValues[i2];
						if (flag4 && model_1.colorValues != null)
							colorValues[triangleCount] = model_1.colorValues[i2];
						
						triangleCount++;
					}
					
					for (int k2 = 0; k2 < model_1.texTriangleCount; k2++) {
						textureTrianglePIndex[texTriangleCount] = model_1.textureTrianglePIndex[k2] + k1;
						textureTriangleMIndex[texTriangleCount] = model_1.textureTriangleMIndex[k2] + k1;
						textureTriangleNIndex[texTriangleCount] = model_1.textureTriangleNIndex[k2] + k1;
						texTriangleCount++;
					}
					
					i1 += model_1.texTriangleCount;
				}
			}
			
			method466();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Model(boolean keepColors, boolean keepAlphas, boolean keepVertices, Model model) {
		aBoolean1659 = false;
		vertexCount = model.vertexCount;
		triangleCount = model.triangleCount;
		texTriangleCount = model.texTriangleCount;
		if (keepVertices) {
			verticesData = model.verticesData;
			verticesLen = model.verticesLen;
		} else {
			copyVertices(model.verticesData);
		}

		if (keepColors) {
			colorValues = model.colorValues;
		} else {
			colorValues = new int[triangleCount];
			System.arraycopy(model.colorValues, 0, colorValues, 0, triangleCount);
		}

		if (keepAlphas) {
			triangleAlphaValues = model.triangleAlphaValues;
		} else {
			triangleAlphaValues = new int[triangleCount];
			if (model.triangleAlphaValues == null) {
				for (int l = 0; l < triangleCount; l++) {
					triangleAlphaValues[l] = 0;
				}
			} else {
				System.arraycopy(model.triangleAlphaValues, 0, triangleAlphaValues, 0, triangleCount);
			}
		}

		colorValues1 = model.colorValues1;
		colorValues2 = model.colorValues2;
		colorValues3 = model.colorValues3;
		texturePointers = model.texturePointers;
		textureTypes = model.textureTypes;
		textureBackgrounds = model.textureBackgrounds;
		anIntArray1655 = model.anIntArray1655;
		anIntArray1656 = model.anIntArray1656;
		anIntArrayArray1657 = model.anIntArrayArray1657;
		anIntArrayArray1658 = model.anIntArrayArray1658;
		drawTypes = model.drawTypes;
		triangleViewSpaceX = model.triangleViewSpaceX;
		triangleViewSpaceY = model.triangleViewSpaceY;
		triangleViewSpaceZ = model.triangleViewSpaceZ;
		trianglePriorities = model.trianglePriorities;
		anInt1641 = model.anInt1641;
		textureTrianglePIndex = model.textureTrianglePIndex;
		textureTriangleMIndex = model.textureTriangleMIndex;
		textureTriangleNIndex = model.textureTriangleNIndex;
		uv = model.uv;

		texturePrimaryColor = model.texturePrimaryColor;
		textureSecondaryColor = model.textureSecondaryColor;
		particleDirectionZ = model.particleDirectionZ;
		particleDirectionY = model.particleDirectionY;
		particleDirectionX = model.particleDirectionX;
		particleLifespanX = model.particleLifespanX;
		particleLifespanZ = model.particleLifespanZ;
		particleLifespanY = model.particleLifespanY;
	}
	
	public Model(boolean keepAlphas, boolean copyNormals, Model model) {
		aBoolean1659 = false;
		vertexCount = model.vertexCount;
		triangleCount = model.triangleCount;
		texTriangleCount = model.texTriangleCount;
		animated = model.animated;
		copyVertices(model.verticesData);
		if (copyNormals) {
			colorValues1 = new int[triangleCount];
			colorValues2 = new int[triangleCount];
			colorValues3 = new int[triangleCount];
			for (int k = 0; k < triangleCount; k++) {
				colorValues1[k] = model.colorValues1[k];
				colorValues2[k] = model.colorValues2[k];
				colorValues3[k] = model.colorValues3[k];
			}
			
			drawTypes = new int[triangleCount];
			if (model.drawTypes == null) {
				for (int l = 0; l < triangleCount; l++)
					drawTypes[l] = 0;
				
			} else {
				System.arraycopy(model.drawTypes, 0, drawTypes, 0, triangleCount);
			}
			super.aClass33Array1425 = new Vertex[vertexCount];
			for (int j1 = 0; j1 < vertexCount; j1++) {
				Vertex class33 = super.aClass33Array1425[j1] = new Vertex();
				Vertex class33_1 = model.aClass33Array1425[j1];
				class33.x = class33_1.x;
				class33.y = class33_1.y;
				class33.z = class33_1.z;
				class33.length = class33_1.length;
			}
			
			aClass33Array1660 = model.aClass33Array1660;
		} else {
			colorValues1 = model.colorValues1;
			colorValues2 = model.colorValues2;
			colorValues3 = model.colorValues3;
			drawTypes = model.drawTypes;
		}
		colorValues = model.colorValues;
		if (keepAlphas) {
			triangleAlphaValues = model.triangleAlphaValues;
		} else {
			if (locAlphas.length < triangleCount)
				locAlphas = new int[triangleCount + 100];
			triangleAlphaValues = locAlphas;
			if (model.triangleAlphaValues == null) {
				for (int l = 0; l < triangleCount; l++) {
					triangleAlphaValues[l] = 0;
				}
			} else {
				System.arraycopy(model.triangleAlphaValues, 0, triangleAlphaValues, 0, triangleCount);
			}
		}
		trianglePriorities = model.trianglePriorities;
		anInt1641 = model.anInt1641;
		triangleViewSpaceX = model.triangleViewSpaceX;
		triangleViewSpaceY = model.triangleViewSpaceY;
		triangleViewSpaceZ = model.triangleViewSpaceZ;
		textureTrianglePIndex = model.textureTrianglePIndex;
		textureTriangleMIndex = model.textureTriangleMIndex;
		textureTriangleNIndex = model.textureTriangleNIndex;
		anIntArrayArray1658 = model.anIntArrayArray1658;
		anIntArrayArray1657 = model.anIntArrayArray1657;
		
		texturePrimaryColor = model.texturePrimaryColor;
		textureSecondaryColor = model.textureSecondaryColor;
		particleDirectionZ = model.particleDirectionZ;
		particleDirectionY = model.particleDirectionY;
		particleDirectionX = model.particleDirectionX;
		particleLifespanX = model.particleLifespanX;
		particleLifespanZ = model.particleLifespanZ;
		particleLifespanY = model.particleLifespanY;
		super.modelHeight = model.modelHeight;
		uv = model.uv;

		anInt1654 = model.anInt1654;
		anInt1650 = model.anInt1650;
		anInt1653 = model.anInt1653;
		anInt1652 = model.anInt1652;
		minX = model.minX;
		maxZ = model.maxZ;
		minZ = model.minZ;
		maxX = model.maxX;
	}

	public Model copyEntity(boolean keepAlpha) {
		if (!keepAlpha && entityAlphas.length < triangleCount) {
			entityAlphas = new int[triangleCount + 100];
		}

		return copyModel(entityModel, keepAlpha, entityAlphas);
	}

	public Model copySpotAnim(boolean keepAlpha) {
		if (!keepAlpha && spotAnimAlphas.length < triangleCount) {
			spotAnimAlphas = new int[triangleCount + 100];
		}

		return copyModel(spotAnimModel, keepAlpha, spotAnimAlphas);
	}

	public Model copyLoc(boolean keepAlpha) {
		if (!keepAlpha && locAlphas.length < triangleCount) {
			locAlphas = new int[triangleCount + 100];
		}

		return copyModel(locModel, keepAlpha, locAlphas);
	}

	public Model copyModel(Model copyTemplate, boolean keepAlpha, int[] newAlphas) {
		copyTemplate.vertexCount = vertexCount;
		copyTemplate.triangleCount = triangleCount;
		copyTemplate.texTriangleCount = texTriangleCount;
		copyTemplate.verticesParticle = verticesParticle;

		int arraySize = verticesData.length;

		copyTemplate.verticesLen = arraySize / 3;
		if (copyTemplate.verticesData == null || copyTemplate.verticesData.length < arraySize) {
			copyTemplate.verticesData = new float[arraySize + 300];
		}

		System.arraycopy(verticesData, 0, copyTemplate.verticesData, 0, arraySize);

		if (keepAlpha) {
			copyTemplate.triangleAlphaValues = triangleAlphaValues;
		} else {
			copyTemplate.triangleAlphaValues = newAlphas;
			if (triangleAlphaValues == null) {
				for (int l = 0; l < triangleCount; l++) {
					copyTemplate.triangleAlphaValues[l] = 0;
				}
			} else {
				System.arraycopy(triangleAlphaValues, 0, copyTemplate.triangleAlphaValues, 0, triangleCount);
			}
		}

		copyTemplate.colorValues1 = colorValues1;
		copyTemplate.colorValues2 = colorValues2;
		copyTemplate.colorValues3 = colorValues3;
		copyTemplate.drawTypes = drawTypes;

		copyTemplate.colorValues = colorValues;
		copyTemplate.trianglePriorities = trianglePriorities;
		copyTemplate.anInt1641 = anInt1641;
		copyTemplate.anIntArrayArray1658 = anIntArrayArray1658;
		copyTemplate.anIntArrayArray1657 = anIntArrayArray1657;
		copyTemplate.triangleViewSpaceX = triangleViewSpaceX;
		copyTemplate.triangleViewSpaceY = triangleViewSpaceY;
		copyTemplate.triangleViewSpaceZ = triangleViewSpaceZ;
		copyTemplate.uv = uv;
		copyTemplate.textureTrianglePIndex = textureTrianglePIndex;
		copyTemplate.textureTriangleMIndex = textureTriangleMIndex;
		copyTemplate.textureTriangleNIndex = textureTriangleNIndex;

		copyTemplate.texturePrimaryColor = texturePrimaryColor;
		copyTemplate.textureSecondaryColor = textureSecondaryColor;
		copyTemplate.particleDirectionZ = particleDirectionZ;
		copyTemplate.particleDirectionY = particleDirectionY;
		copyTemplate.particleDirectionX = particleDirectionX;
		copyTemplate.particleLifespanX = particleLifespanX;
		copyTemplate.particleLifespanZ = particleLifespanZ;
		copyTemplate.particleLifespanY = particleLifespanY;
		return copyTemplate;
	}

	private final int method465(Model model, int i) {
		int j = -1;
		int particle = model.verticesParticle[i];
		float k = model.getVertexX(i);
		float l = model.getVertexY(i);
		float i1 = model.getVertexZ(i);
		for (int j1 = 0; j1 < vertexCount; j1++) {
			if (k != getVertexX(j1) || l != getVertexY(j1) || i1 != getVertexZ(j1))
				continue;
			j = j1;
			break;
		}
		
		if (j == -1) {
			verticesParticle[vertexCount] = particle;
			setVertexX(vertexCount, k);
			setVertexY(vertexCount, l);
			setVertexZ(vertexCount, i1);
			if (model.anIntArray1655 != null)
				anIntArray1655[vertexCount] = model.anIntArray1655[i];
			j = vertexCount++;
		}
		return j;
	}
	
	public void method466() {
		super.modelHeight = 0;
		anInt1650 = 0;
		minY = 0;
		for (int i = 0; i < vertexCount; i++) {
			int j = (int) getVertexX(i);
			int k = (int) getVertexY(i);
			int l = (int) getVertexZ(i);
			if (-k > super.modelHeight)
				super.modelHeight = -k;
			if (k > minY)
				minY = k;
			int i1 = j * j + l * l;
			if (i1 > anInt1650)
				anInt1650 = i1;
		}

		anInt1650 = (int) (Math.sqrt((double) anInt1650) + 0.99D);
		anInt1653 = (int) (Math.sqrt((double) (anInt1650 * anInt1650 + super.modelHeight * super.modelHeight)) + 0.99D);
		anInt1652 = anInt1653 + (int) (Math.sqrt((double) (anInt1650 * anInt1650 + minY * minY)) + 0.99D);
	}
	
	public void method467() {
		super.modelHeight = 0;
		minY = 0;
		int plus = 1;
		/*if (vertexCount > 250)
			plus = 4;
		else if (vertexCount > 100)
			plus = 2;*///TODO this code?
		
		for (int i = 0; i < vertexCount; i += plus) {
			int j = (int) getVertexY(i);
			if (-j > super.modelHeight)
				super.modelHeight = -j;
			if (j > minY)
				minY = j;
		}
		
		anInt1653 = (int) (Math.sqrt(anInt1650 * anInt1650 + super.modelHeight
				* super.modelHeight) + 0.98999999999999999D);
		anInt1652 = anInt1653
				+ (int) (Math.sqrt(anInt1650 * anInt1650 + minY
				* minY) + 0.98999999999999999D);
	}
	
	public void method468() {
		super.modelHeight = 0;
		anInt1650 = 0;
		minY = 0;
		minX = 0xf423f;
		maxX = 0xfff0bdc1;
		maxZ = 0xfffe7961;
		minZ = 0x1869f;
		for (int vertexId = 0; vertexId < vertexCount; vertexId++) {
			int x = (int) getVertexX(vertexId);
			int y = (int) getVertexY(vertexId);
			int z = (int) getVertexZ(vertexId);
			if (x < minX) {
				minX = x;
			}
			if (x > maxX) {
				maxX = x;
			}
			if (z < minZ) {
				minZ = z;
			}
			if (z > maxZ) {
				maxZ = z;
			}
			if (-y > super.modelHeight) {
				super.modelHeight = -y;
			}
			if (y > minY) {
				minY = y;
			}
			int j1 = x * x + z * z;
			if (j1 > anInt1650) {
				anInt1650 = j1;
			}
		}
		
		anInt1650 = (int) Math.sqrt(anInt1650);
		anInt1653 = (int) Math.sqrt(anInt1650 * anInt1650 + super.modelHeight * super.modelHeight);
		anInt1652 = anInt1653 + (int) Math.sqrt(anInt1650 * anInt1650 + minY * minY);
	}
	
	public void applyEffects() {
		if (anIntArray1655 != null) {
			int ai[] = new int[256];
			int j = 0;
			for (int l = 0; l < vertexCount; l++) {
				int j1 = anIntArray1655[l];
				ai[j1]++;
				if (j1 > j)
					j = j1;
			}
			
			anIntArrayArray1657 = new int[j + 1][];
			for (int k1 = 0; k1 <= j; k1++) {
				anIntArrayArray1657[k1] = new int[ai[k1]];
				ai[k1] = 0;
			}
			
			for (int j2 = 0; j2 < vertexCount; j2++) {
				int l2 = anIntArray1655[j2];
				anIntArrayArray1657[l2][ai[l2]++] = j2;
			}
			
			anIntArray1655 = null;
		}
		if (anIntArray1656 != null) {
			int ai1[] = new int[256];
			int k = 0;
			for (int i1 = 0; i1 < triangleCount; i1++) {
				int l1 = anIntArray1656[i1];
				ai1[l1]++;
				if (l1 > k)
					k = l1;
			}
			
			anIntArrayArray1658 = new int[k + 1][];
			for (int i2 = 0; i2 <= k; i2++) {
				anIntArrayArray1658[i2] = new int[ai1[i2]];
				ai1[i2] = 0;
			}
			
			for (int k2 = 0; k2 < triangleCount; k2++) {
				int i3 = anIntArray1656[k2];
				anIntArrayArray1658[i3][ai1[i3]++] = k2;
			}
			
			anIntArray1656 = null;
		}
	}

	public boolean applyAnimation(int i) {
		if (anIntArrayArray1657 == null)
			return false;
		if (i == -1)
			return false;
		Class36 class36 = Class36.method531(i);
		if (class36 == null)
			return false;
		Class18 class18 = class36.aClass18_637;
		anInt1681 = 0;
		anInt1682 = 0;
		anInt1683 = 0;
		for (int k = 0; k < class36.anInt638; k++) {
			int l = class36.anIntArray639[k];
			method472(class18.anIntArray342[l], class18.anIntArrayArray343[l],
					class36.anIntArray640[k], class36.anIntArray641[k],
					class36.anIntArray642[k]);
		}

		return true;
	}

	public boolean applyAnimationTween(int frame, int nextFrame, int cycle, int length, boolean[] animateLabels, boolean condition) {
		if (anIntArrayArray1657 == null || frame == -1) {
			return false;
		}

		Class36 currAnim = Class36.method531(frame);
		if (currAnim == null) {
			return false;
		}

		Class18 skinList = currAnim.aClass18_637;
		Class36 nextAnim = null;
		if (nextFrame != -1) {
			nextAnim = Class36.method531(nextFrame);
			if (nextAnim != null && nextAnim.aClass18_637 != skinList) {
				nextAnim = null;
			}
		}

		anInt1681 = 0;
		anInt1682 = 0;
		anInt1683 = 0;

		if (nextAnim == null || cycle == 0) {
			for (int k = 0; k < currAnim.anInt638; k++) {
				int l = currAnim.anIntArray639[k];
				if (animateLabels == null || animateLabels[l] == condition || skinList.anIntArray342[l] == 0) {
					method472(skinList.anIntArray342[l], skinList.anIntArrayArray343[l],
							currAnim.anIntArray640[k], currAnim.anIntArray641[k],
							currAnim.anIntArray642[k]);
				}
			}
			return true;
		}

		int currFrameId = 0;
    	int nextFrameId = 0;
	    for (int index = 0; index < skinList.anInt341; index++) {
	    	boolean interpolateCurr = false;
	    	if (currFrameId < currAnim.anInt638 && currAnim.anIntArray639[currFrameId] == index) {
	    		interpolateCurr = true;
	    	}

	    	boolean interpolateNext = false;
	    	if (nextFrameId < nextAnim.anInt638 && nextAnim.anIntArray639[nextFrameId] == index) {
	    		interpolateNext = true;
	    	}

	    	if (interpolateCurr || interpolateNext) {
	    		if (animateLabels != null && animateLabels[index] != condition && skinList.anIntArray342[index] != 0) {
	    			if (interpolateCurr) {
	    				currFrameId++;
	    			}
	    			if (interpolateNext) {
	    				nextFrameId++;
	    			}
	    		} else {
					int defaultModifier = 0;
					int opcode = skinList.anIntArray342[index];
					if (opcode == 3) {
						defaultModifier = 128;
					}
					int currAnimX;
					int currAnimY;
					int currAnimZ;
					if (interpolateCurr) {
						currAnimX = currAnim.anIntArray640[currFrameId];
						currAnimY = currAnim.anIntArray641[currFrameId];
						currAnimZ = currAnim.anIntArray642[currFrameId];
						currFrameId++;
					} else {
						currAnimX = defaultModifier;
						currAnimY = defaultModifier;
						currAnimZ = defaultModifier;
					}
					int nextAnimX;
					int nextAnimY;
					int nextAnimZ;
					if (interpolateNext) {
						nextAnimX = nextAnim.anIntArray640[nextFrameId];
						nextAnimY = nextAnim.anIntArray641[nextFrameId];
						nextAnimZ = nextAnim.anIntArray642[nextFrameId];
						nextFrameId++;
					} else {
						nextAnimX = defaultModifier;
						nextAnimY = defaultModifier;
						nextAnimZ = defaultModifier;
					}
					int interpolatedX;
					int interpolatedY;
					int interpolatedZ;
					if (opcode == 2) {
						int deltaX = nextAnimX - currAnimX & 0x7ff;
						int deltaY = nextAnimY - currAnimY & 0x7ff;
						int deltaZ = nextAnimZ - currAnimZ & 0x7ff;
						if (deltaX >= 1024) {
							deltaX -= 2048;
						}
						if (deltaY >= 1024) {
							deltaY -= 2048;
						}
						if (deltaZ >= 1024) {
							deltaZ -= 2048;
						}
						interpolatedX = currAnimX + deltaX * cycle / length & 0x7ff;
						interpolatedY = currAnimY + deltaY * cycle / length & 0x7ff;
						interpolatedZ = currAnimZ + deltaZ * cycle / length & 0x7ff;
					} else {
						interpolatedX = currAnimX + (nextAnimX - currAnimX) * cycle / length;
						interpolatedY = currAnimY + (nextAnimY - currAnimY) * cycle / length;
						interpolatedZ = currAnimZ + (nextAnimZ - currAnimZ) * cycle / length;
					}
					method472(opcode, skinList.anIntArrayArray343[index], interpolatedX, interpolatedY, interpolatedZ);
	    		}
	    	}
	    }

	    return true;
	}
	
	public boolean method471(boolean ai[], int baseAnimFrameId, int baeAnimNextFrameId, int baseAnimFrameCycle, int baseAnimFrameLength, int animFrameId, int animNextFrameId, int animFrameCycle, int animFrameLength) {
		try {
			if (animFrameId == -1) {
				return false;
			}

			if (ai == null || baseAnimFrameId == -1) {
				return applyAnimation(animFrameId);
			}

			anInt1681 = 0;
			anInt1682 = 0;
			anInt1683 = 0;
			applyAnimationTween(animFrameId, animNextFrameId, animFrameCycle, animFrameLength, ai, false);
			anInt1681 = 0;
			anInt1682 = 0;
			anInt1683 = 0;
			applyAnimationTween(baseAnimFrameId, baeAnimNextFrameId, baseAnimFrameCycle, baseAnimFrameLength, ai, true);
			return true;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	private void method472(int i, int ai[], int j, int k, int l) {
		int i1 = ai.length;
		if (i == 0) {
			int j1 = 0;
			anInt1681 = 0;
			anInt1682 = 0;
			anInt1683 = 0;
			for (int k2 = 0; k2 < i1; k2++) {
				int l3 = ai[k2];
				if (l3 < anIntArrayArray1657.length) {
					int ai5[] = anIntArrayArray1657[l3];
					for (int i5 = 0; i5 < ai5.length; i5++) {
						int j6 = ai5[i5];
						anInt1681 += getVertexX(j6);
						anInt1682 += getVertexY(j6);
						anInt1683 += getVertexZ(j6);
						j1++;
					}
					
				}
			}
			
			if (j1 > 0) {
				anInt1681 = anInt1681 / j1 + j;
				anInt1682 = anInt1682 / j1 + k;
				anInt1683 = anInt1683 / j1 + l;
				return;
			} else {
				anInt1681 = j;
				anInt1682 = k;
				anInt1683 = l;
				return;
			}
		}
		if (i == 1) {
			for (int k1 = 0; k1 < i1; k1++) {
				int l2 = ai[k1];
				if (l2 < anIntArrayArray1657.length) {
					int ai1[] = anIntArrayArray1657[l2];
					for (int i4 = 0; i4 < ai1.length; i4++) {
						int j5 = ai1[i4];
						setVertexX(j5, getVertexX(j5) + j);
						setVertexY(j5, getVertexY(j5) + k);
						setVertexZ(j5, getVertexZ(j5) + l);
					}
					
				}
			}
			
			return;
		}
		if (i == 2) {
			for (int l1 = 0; l1 < i1; l1++) {
				int i3 = ai[l1];
				if (i3 < anIntArrayArray1657.length) {
					int ai2[] = anIntArrayArray1657[i3];
					for (int j4 = 0; j4 < ai2.length; j4++) {
						int k5 = ai2[j4];
						setVertexX(k5, getVertexX(k5) - anInt1681);
						setVertexY(k5, getVertexY(k5) - anInt1682);
						setVertexZ(k5, getVertexZ(k5) - anInt1683);
						if (l != 0) {
							float j7 = Rasterizer.SINE_FLOAT[l];
							float i8 = Rasterizer.COSINE_FLOAT[l];
							float l8 = getVertexY(k5) * j7 + getVertexX(k5) * i8;
							setVertexY(k5, getVertexY(k5) * i8 - getVertexX(k5) * j7);
							setVertexX(k5, l8);
						}
						if (j != 0) {
							float k7 = Rasterizer.SINE_FLOAT[j];
							float j8 = Rasterizer.COSINE_FLOAT[j];
							float i9 = getVertexY(k5) * j8 - getVertexZ(k5) * k7;
							setVertexZ(k5, getVertexY(k5) * k7 + getVertexZ(k5) * j8);
							setVertexY(k5, i9);
						}
						if (k != 0) {
							float l7 = Rasterizer.SINE_FLOAT[k];
							float k8 = Rasterizer.COSINE_FLOAT[k];
							float j9 = getVertexZ(k5) * l7 + getVertexX(k5) * k8;
							setVertexZ(k5, getVertexZ(k5) * k8 - getVertexX(k5) * l7);
							setVertexX(k5, j9);
						}
						setVertexX(k5, getVertexX(k5) + anInt1681);
						setVertexY(k5, getVertexY(k5) + anInt1682);
						setVertexZ(k5, getVertexZ(k5) + anInt1683);
					}
					
				}
			}
			return;
		}
		if (i == 3) {
			for (int i2 = 0; i2 < i1; i2++) {
				int j3 = ai[i2];
				if (j3 < anIntArrayArray1657.length) {
					int ai3[] = anIntArrayArray1657[j3];
					for (int k4 = 0; k4 < ai3.length; k4++) {
						int l5 = ai3[k4];
						setVertexX(l5, getVertexX(l5) - anInt1681);
						setVertexY(l5, getVertexY(l5) - anInt1682);
						setVertexZ(l5, getVertexZ(l5) - anInt1683);
						
						setVertexX(l5, ((getVertexX(l5) * j) / 128));
						setVertexY(l5, ((getVertexY(l5) * k) / 128));
						setVertexZ(l5, ((getVertexZ(l5) * l) / 128));
						
						setVertexX(l5, getVertexX(l5) + anInt1681);
						setVertexY(l5, getVertexY(l5) + anInt1682);
						setVertexZ(l5, getVertexZ(l5) + anInt1683);
					}
				}
			}
			return;
		}
		if (i == 5 && anIntArrayArray1658 != null && triangleAlphaValues != null) {
			for (int j2 = 0; j2 < i1; j2++) {
				int k3 = ai[j2];
				if (k3 < anIntArrayArray1658.length) {
					int ai4[] = anIntArrayArray1658[k3];
					for (int l4 = 0; l4 < ai4.length; l4++) {
						int i6 = ai4[l4];
						triangleAlphaValues[i6] += j * 8;
						if (triangleAlphaValues[i6] < 0)
							triangleAlphaValues[i6] = 0;
						if (triangleAlphaValues[i6] > 255)
							triangleAlphaValues[i6] = 255;
					}
				}
			}
		}
	}
	
	public void rotate90() {
		for (int j = 0; j < vertexCount; j++) {
			float k = getVertexX(j);
			setVertexX(j, getVertexZ(j));
			setVertexZ(j, -k);
		}
	}
	
	public void rotate180() {
		for (int j = 0; j < vertexCount; j++) {
			setVertexX(j, -getVertexX(j));
			setVertexZ(j, -getVertexZ(j));
		}
	}
	
	public void rotate270() {
		for (int j = 0; j < vertexCount; j++) {
			float k = getVertexZ(j);
			setVertexZ(j, getVertexX(j));
			setVertexX(j, -k);
		}
	}
	
	public void method474(int i) {
		float k = Rasterizer.SINE_FLOAT[i];
		float l = Rasterizer.COSINE_FLOAT[i];
		for (int i1 = 0; i1 < vertexCount; i1++) {
			float j1 = getVertexY(i1) * l - getVertexZ(i1) * k;
			setVertexZ(i1, getVertexY(i1) * k + getVertexZ(i1) * l);
			setVertexY(i1, j1);
		}
	}
	
	public void translate(int x, int y, int z) {
		for (int i1 = 0; i1 < vertexCount; i1++) {
			setVertexX(i1, getVertexX(i1) + x);
			setVertexY(i1, getVertexY(i1) + y);
			setVertexZ(i1, getVertexZ(i1) + z);
		}
	}
	
	public void replaceHs1(int i, int j) {
		for (int k = 0; k < triangleCount; k++)
			if (colorValues[k] == i)
				colorValues[k] = j;
	}
	
	public void method477() {
		for (int j = 0; j < vertexCount; j++)
			setVertexZ(j, -getVertexZ(j));
		for (int k = 0; k < triangleCount; k++) {
			int l = triangleViewSpaceX[k];
			triangleViewSpaceX[k] = triangleViewSpaceZ[k];
			triangleViewSpaceZ[k] = l;
			
			if (uv != null) {
				final float u1 = uv[k][0];
				final float v1 = uv[k][1];
				final float u2 = uv[k][2];
				final float v2 = uv[k][3];
				final float u3 = uv[k][4];
				final float v3 = uv[k][5];
				
				uv[k][0] = u3;
				uv[k][1] = v3;
				uv[k][2] = u2;
				uv[k][3] = v2;
				uv[k][4] = u1;
				uv[k][5] = v1;
			}
		}
	}
	
	public void method478(int x, int y, int z) {
		for (int i1 = 0; i1 < vertexCount; i1++) {
			setVertexX(i1, ((getVertexX(i1) * x) / 128));
			setVertexY(i1, ((getVertexY(i1) * y) / 128));
			setVertexZ(i1, ((getVertexZ(i1) * z) / 128));
		}
	}

	public final void calculateLighting(int lightness, int contrast, int x, int y, int z, boolean flag) {
		int j1 = (int) Math.sqrt(x * x + y * y + z * z);
		int k1 = contrast * j1 >> 8;
		if (colorValues1 == null) {
			colorValues1 = new int[triangleCount];
			colorValues2 = new int[triangleCount];
			colorValues3 = new int[triangleCount];
		}
		if (super.aClass33Array1425 == null) {
			super.aClass33Array1425 = new Vertex[vertexCount];
			for (int l1 = 0; l1 < vertexCount; l1++)
				super.aClass33Array1425[l1] = new Vertex();
		}
		for (int i2 = 0; i2 < triangleCount; i2++) {
			if (colorValues != null && (colorValues[i2] == 65535/* || anIntArray1640[i2] == 0 || anIntArray1640[i2] == 16705*/) && triangleAlphaValues != null) {
				triangleAlphaValues[i2] = 255;
			}

			int k2 = triangleViewSpaceX[i2];
			int l2 = triangleViewSpaceY[i2];
			int i3 = triangleViewSpaceZ[i2];
			int j3 = (int) getVertexX(l2) - (int) getVertexX(k2);
			int k3 = (int) getVertexY(l2) - (int) getVertexY(k2);
			int l3 = (int) getVertexZ(l2) - (int) getVertexZ(k2);
			int i4 = (int) getVertexX(i3) - (int) getVertexX(k2);
			int j4 = (int) getVertexY(i3) - (int) getVertexY(k2);
			int k4 = (int) getVertexZ(i3) - (int) getVertexZ(k2);
			int l4 = k3 * k4 - j4 * l3;
			int i5 = l3 * i4 - k4 * j3;
			int j5;
			for (j5 = j3 * j4 - i4 * k3; l4 > 8192 || i5 > 8192 || j5 > 8192 || l4 < -8192 || i5 < -8192 || j5 < -8192; j5 >>= 1) {
				l4 >>= 1;
				i5 >>= 1;
			}
			
			int k5 = (int) Math.sqrt(l4 * l4 + i5 * i5 + j5 * j5);
			if (k5 <= 0)
				k5 = 1;
			l4 = (l4 * 256) / k5;
			i5 = (i5 * 256) / k5;
			j5 = (j5 * 256) / k5;
			if (drawTypes == null || (drawTypes[i2] & 1) == 0) {
				Vertex class33_2 = super.aClass33Array1425[k2];
				class33_2.x += l4;
				class33_2.y += i5;
				class33_2.z += j5;
				class33_2.length++;
				class33_2 = super.aClass33Array1425[l2];
				class33_2.x += l4;
				class33_2.y += i5;
				class33_2.z += j5;
				class33_2.length++;
				class33_2 = super.aClass33Array1425[i3];
				class33_2.x += l4;
				class33_2.y += i5;
				class33_2.z += j5;
				class33_2.length++;
			} else {
				int l5 = lightness + (x * l4 + y * i5 + z * j5) / (k1 + k1 / 2);
				colorValues1[i2] = method481(colorValues[i2], l5, drawTypes[i2]);
			}
		}
		
		if (flag) {
			method480(lightness, k1, x, y, z);
		} else {
			aClass33Array1660 = new Vertex[vertexCount];
			for (int j2 = 0; j2 < vertexCount; j2++) {
				Vertex class33 = super.aClass33Array1425[j2];
				Vertex class33_1 = aClass33Array1660[j2] = new Vertex();
				class33_1.x = class33.x;
				class33_1.y = class33.y;
				class33_1.z = class33.z;
				class33_1.length = class33.length;
			}
			
		}

		if (flag) {
			method466();
		} else {
			method468();
		}
	}
	
	public final void method480(int i, int j, int k, int l, int i1) {
		for (int j1 = 0; j1 < triangleCount; j1++) {
			int k1 = triangleViewSpaceX[j1];
			int i2 = triangleViewSpaceY[j1];
			int j2 = triangleViewSpaceZ[j1];
			if (drawTypes == null) {
				int i3 = colorValues[j1];
				Vertex class33 = super.aClass33Array1425[k1];
				int k2 = i
						+ (k * class33.x + l * class33.y + i1
						* class33.z) / (j * class33.length);
				colorValues1[j1] = method481(i3, k2, 0);
				class33 = super.aClass33Array1425[i2];
				k2 = i
						+ (k * class33.x + l * class33.y + i1
						* class33.z) / (j * class33.length);
				colorValues2[j1] = method481(i3, k2, 0);
				class33 = super.aClass33Array1425[j2];
				k2 = i
						+ (k * class33.x + l * class33.y + i1
						* class33.z) / (j * class33.length);
				colorValues3[j1] = method481(i3, k2, 0);
			} else if ((drawTypes[j1] & 1) == 0) {
				int j3 = colorValues[j1];
				int k3 = drawTypes[j1];
				Vertex class33_1 = super.aClass33Array1425[k1];
				int l2 = i
						+ (k * class33_1.x + l * class33_1.y + i1
						* class33_1.z)
						/ (j * class33_1.length);
				colorValues1[j1] = method481(j3, l2, k3);
				class33_1 = super.aClass33Array1425[i2];
				l2 = i
						+ (k * class33_1.x + l * class33_1.y + i1
						* class33_1.z)
						/ (j * class33_1.length);
				colorValues2[j1] = method481(j3, l2, k3);
				class33_1 = super.aClass33Array1425[j2];
				l2 = i
						+ (k * class33_1.x + l * class33_1.y + i1
						* class33_1.z)
						/ (j * class33_1.length);
				colorValues3[j1] = method481(j3, l2, k3);
			}
		}
		
		super.aClass33Array1425 = null;
		aClass33Array1660 = null;
		anIntArray1655 = null;
		anIntArray1656 = null;
		/*if (drawTypes != null) {
			for (int l1 = 0; l1 < triangleCount; l1++)
				if ((drawTypes[l1] & 2) == 2)
					return;
			
		}
		colorValues = null;*/
	}

	public void downscale() {
		for (int i = verticesData.length - 1; i >= 0; i--) {
			verticesData[i] = verticesData[i] / 4.0f;
		}
	}

	public static final int method481(int i, int j, int k) {
		if (i == 65535)
			return 0;
		if ((k & 2) == 2) {
			if (j < 0)
				j = 0;
			else if (j > 127)
				j = 127;
			j = 127 - j;
			return j;
		}
		
		j = j * (i & 0x7f) >> 7;
		if (j < 2)
			j = 2;
		else if (j > 126)
			j = 126;
		return (i & 0xff80) + j;
	}
	
	private void loadSpriteModel(int dx, int dy, boolean alwaysOnTop, long uid, boolean animated, boolean isInterfaceModel) {
		float depth = alwaysOnTop ? 2100f : GraphicsDisplay.getSpriteDepth();
		
		if (isInterfaceModel)
			dx = dy = 0;
		
		// depth -= 210;
		// those are subtracted before callign
		// the render method, we undo that to get
		// the real world coordinates!
		float x = dx;
		float y = depth;
		float z = (GraphicsDisplay.getHeight() - dy);
		
		final ModelVBO vbo = VBOManager.atlasModels.loadModel(new DrawableModel(this, uid, -1, animated, true, 0), isInterfaceModel);
		if (vbo == null) {
			return;
		}
		
		int translateX = 0, translateY = 0, translateZ = 0;
		float scaleX = 1f, scaleY = 1f, scaleZ = 1f;
		VBOManager.addModel(uid, x + translateX, y + translateY, z + translateZ, 0, 1024, 0, vbo, scaleX, scaleY, scaleZ, Raster.scissor);
	}
	
	public final void method482(int j, int k, int l, int i1, float j1, float k1, long uid, boolean isInterfaceModel, boolean enabled, boolean animated) {
		if (GraphicsDisplay.enabled && GraphicsDisplay.vboRendering && enabled) {
			if (ModelLoader.isModelLoaded(animated, uid)) {
				loadSpriteModel(Rasterizer.center_x, Rasterizer.center_y, false, uid, animated, isInterfaceModel);
				return;
			}
		}
		
		int i = 0;
		int l1 = Rasterizer.center_x;
		int i2 = Rasterizer.center_y;
		
		float j2 = Rasterizer.SINE_FLOAT[i];
		float k2 = Rasterizer.COSINE_FLOAT[i];
		float l2 = Rasterizer.SINE_FLOAT[j];
		
		float i3 = Rasterizer.COSINE_FLOAT[j];
		float j3 = Rasterizer.SINE_FLOAT[k];
		float k3 = Rasterizer.COSINE_FLOAT[k];
		
		float l3 = Rasterizer.SINE_FLOAT[l];
		float i4 = Rasterizer.COSINE_FLOAT[l];
		float j4 = j1 * l3 + k1 * i4;
		for (int k4 = 0; k4 < vertexCount; k4++) {
			float l4 = getVertexX(k4);
			float i5 = getVertexY(k4);
			float j5 = getVertexZ(k4);
			if (k != 0) {
				float k5 = i5 * j3 + l4 * k3;
				i5 = i5 * k3 - l4 * j3;
				l4 = k5;
			}
			if (i != 0) {
				float l5 = i5 * k2 - j5 * j2;
				j5 = i5 * j2 + j5 * k2;
				i5 = l5;
			}
			if (j != 0) {
				float i6 = j5 * l2 + l4 * i3;
				j5 = j5 * i3 - l4 * l2;
				l4 = i6;
			}
			l4 += i1;
			i5 += j1;
			j5 += k1;
			float j6 = i5 * i4 - j5 * l3;
			j5 = i5 * l3 + j5 * i4;
			i5 = j6;
			vertexPerspectiveZ[k4] = (int) (j5 - j4);
			vertexPerspectiveX[k4] = (int) (l1 + (l4 * 512.0f) / j5);
			vertexPerspectiveY[k4] = (int) (i2 + (i5 * 512.0f) / j5);
			vertexPerspectiveDepth[k4] = (int) j5;
			if (texTriangleCount > 0) {
				anIntArray1668[k4] = (int) l4;
				anIntArray1669[k4] = (int) i5;
				anIntArray1670[k4] = (int) j5;
			}
		}
		
		try {
			if (GraphicsDisplay.enabled && GraphicsDisplay.vboRendering && enabled) {
				loadSpriteModel(l1, i2, false, uid, animated, isInterfaceModel);
			} else {
				method483(false, false, 0);
			}
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}
	
	public static boolean enableClickBoxDebug = false;

	public final void method443(int i, float j, float k, float l, float i1, int j1, int k1, int l1, long i2, boolean animated, int level, long glUid) {
		float j2 = l1 * i1 - j1 * l;
		float k2 = k1 * j + j2 * k;
		float l2 = anInt1650 * k;
		float i3 = k2 + l2;
		if (i3 <= 50 || k2 >= 5500)
			return;
		int fov = Client.fov;
		if (GraphicsDisplay.enabled) {
			fov = VBORenderer.fov;
		}
		float j3 = l1 * l + j1 * i1;
		float k3 = (j3 - anInt1650) * fov;
		if (k3 / i3 >= Raster.centerX)
			return;
		float l3 = (j3 + anInt1650) * fov;
		if (l3 / i3 <= -Raster.centerX)
			return;
		float i4 = k1 * k - j2 * j;
		float j4 = anInt1650 * j;
			float k4 = (i4 + j4) * fov;
		if (k4 / i3 <= -Raster.centerY)
			return;
		float l4 = j4 + (super.modelHeight * k);
		float i5 = (i4 - l4) * fov;
		if (i5 / i3 >= Raster.centerY)
			return;
		float j5 = l2 + (super.modelHeight * j);
		boolean flag = false;
		if (k2 - j5 <= 50)
			flag = true;
		boolean flag1 = false;
		if (i2 > 0 && aBoolean1684) {
			float k5 = k2 - l2;
			if (k5 <= 50)
				k5 = 50;
			if (j3 > 0) {
				k3 /= i3;
				l3 /= k5;
			} else {
				l3 /= i3;
				k3 /= k5;
			}
			if (i4 > 0) {
				i5 /= i3;
				k4 /= k5;
			} else {
				k4 /= i3;
				i5 /= k5;
			}
			int i6 = anInt1685 - Rasterizer.center_x;
			int k6 = anInt1686 - Rasterizer.center_y;
			if (enableClickBoxDebug) {
				int x = Rasterizer.center_x + Raster.topX + (int) k3;
				int y = Rasterizer.center_y + Raster.topY + (int) i5;
				Raster.drawRect(x, y, (int) (l3 + Rasterizer.center_x) - x, (int) (k4 + Rasterizer.center_y) - y, 0xffffff);
			}

			if (i6 > k3 && i6 < l3 && k6 > i5 && k6 < k4) {
				if (aBoolean1659) {
					anIntArray1688[anInt1687++] = i2;
				} else
					flag1 = true;
			}
		}
		int l5 = Rasterizer.center_x;
		int j6 = Rasterizer.center_y;
		float l6 = 0;
		float i7 = 0;
		if (i != 0) {
			l6 = Rasterizer.SINE_FLOAT[i];
			i7 = Rasterizer.COSINE_FLOAT[i];
		}

		renderAtPointX = j1 + Client.xCameraPos;
		renderAtPointY = l1 + Client.yCameraPos;
		renderAtPointZ = k1 + Client.zCameraPos;
		lastRenderedRotation = i;

		if (GraphicsDisplay.enabled && GraphicsDisplay.vboRendering) {
			//if (!seen)
			//	return;
			
			//final boolean seen2 = seen;
			//seen = true;
			
			//if (triangleCount > 38)
			//	return;	
			final DrawableModel drawable = new DrawableModel(this, glUid, 0, animated, false, true, level, false);//TODO all params?
			final ModelVBO vbo = VBOManager.atlasModels.loadModel(drawable, false);
			
			//if (modelId == 0)
			//	return;
			
			int translateX = 0, translateY = 0, translateZ = 0;
			float scaleX = 1f, scaleY = 1f, scaleZ = 1f;
			int yRotation = 0;
			int zRotation = 0;
			if (vbo == null) {
				drawable.setCoords(renderAtPointX + translateX, renderAtPointY + translateY, renderAtPointZ + translateZ, i, yRotation, zRotation, scaleX, scaleY, scaleZ);
				return;
			}
			
			// add model to rendering list
			VBOManager.addModel(drawable.getModelUid(), renderAtPointX + translateX, renderAtPointY + translateY, renderAtPointZ + translateZ, i, yRotation, zRotation, vbo, scaleX, scaleY, scaleZ);
			// add model to shadow rendering list
			//if (newShadows && Client.useShadows && distance <= maxShadowCastDistance)//TODO shadows
			//	VBOManager.addModelShadow(drawable.getModelUid(), x+translateX, y+translateY, z+translateZ, i, yRotation, zRotation, vbo, scaleX, scaleY, scaleZ);
			
			/*resetScale();
			resetTranslate();
			
			if (selectionBufferCandidate) {
				//System.out.println("identifier: "+vbo.mapIdentifier);
				objIdToUid.put(vbo.mapIdentifier, i2);
				VBOManager.addSelectionBufferModel(i2, x, y, z, i, yRotation, zRotation, vbo, collisionColor);
				collisionColor += 1;
			}*/
		}
		
		for (int j7 = 0; j7 < vertexCount; j7++) {
			float k7 = getVertexX(j7);
			float l7 = getVertexY(j7);
			float i8 = getVertexZ(j7);
			if (i != 0) {
				float j8 = i8 * l6 + k7 * i7;
				i8 = i8 * i7 - k7 * l6;
				k7 = j8;
			}
			k7 += j1;
			l7 += k1;
			i8 += l1;
			float k8 = i8 * l + k7 * i1;
			i8 = i8 * i1 - k7 * l;
			k7 = k8;
			k8 = l7 * k - i8 * j;
			i8 = l7 * j + i8 * k;
			l7 = k8;
			vertexPerspectiveZ[j7] = (int) (i8 - k2);
			vertexPerspectiveDepth[j7] = (int) i8;
			if (i8 >= 50) {
				vertexPerspectiveX[j7] = (int) (l5 + k7 * fov / i8);
				vertexPerspectiveY[j7] = (int) (j6 + l7 * fov / i8);
			} else {
				vertexPerspectiveX[j7] = -5000;
				flag = true;
			}
			if (flag || texTriangleCount > 0) {
				anIntArray1668[j7] = (int) k7;
				anIntArray1669[j7] = (int) l7;
				anIntArray1670[j7] = (int) i8;
			}
		}
		
		try {
			if (GraphicsDisplay.enabled) {
				method483HD(flag, flag1, i2);
			} else {
				method483(flag, flag1, i2);
			}
		} catch (Exception _ex) {
			return;
		}
	}
	
	private final void method483HD(boolean projected, boolean hasInput, long bitset) {
		for (int t = 0; t < triangleCount; t++) {
			int a = triangleViewSpaceX[t];
			int b = triangleViewSpaceY[t];
			int c = triangleViewSpaceZ[t];
			
			int xA = vertexPerspectiveX[a];
			int xB = vertexPerspectiveX[b];
			int xC = vertexPerspectiveX[c];
			if (!projected || xA != -5000 && xB != -5000 && xC != -5000) {
				if (hasInput && method486(anInt1685, anInt1686, vertexPerspectiveY[a], vertexPerspectiveY[b], vertexPerspectiveY[c], xA, xB, xC)) {
					anIntArray1688[anInt1687++] = bitset;
					break;
				}
			}
		}
	}
	
	private final void method483(boolean projected, boolean hasInput, long bitset) {
		try {
		for (int j = 0; j < anInt1652; j++) {
			anIntArray1671[j] = 0;
		}
		
		for (int t = 0; t < triangleCount; t++) {
			if (drawTypes == null || drawTypes[t] != -1) {
				int a = triangleViewSpaceX[t];
				int b = triangleViewSpaceY[t];
				int c = triangleViewSpaceZ[t];
				
				int xA = vertexPerspectiveX[a];
				int xB = vertexPerspectiveX[b];
				int xC = vertexPerspectiveX[c];
				if (!projected || xA != -5000 && xB != -5000 && xC != -5000) {
					if (hasInput && method486(anInt1685, anInt1686, vertexPerspectiveY[a], vertexPerspectiveY[b], vertexPerspectiveY[c], xA, xB, xC)) {
						anIntArray1688[anInt1687++] = bitset;
						hasInput = false;
					}
					if ((xA - xB) * (vertexPerspectiveY[c] - vertexPerspectiveY[b]) - (vertexPerspectiveY[a] - vertexPerspectiveY[b]) * (xC - xB) > 0) {
						aBooleanArray1664[t] = false;
						aBooleanArray1663[t] = xA < 0 || xB < 0 || xC < 0 || xA > Raster.bottomX || xB > Raster.bottomX || xC > Raster.bottomX;
						int k5 = (vertexPerspectiveZ[a] + vertexPerspectiveZ[b] + vertexPerspectiveZ[c]) / 3 + anInt1653;
						anIntArrayArray1672[k5][anIntArray1671[k5]++] = t;
					}
				} else {
					aBooleanArray1664[t] = true;
					int k5 = (vertexPerspectiveZ[a] + vertexPerspectiveZ[b] + vertexPerspectiveZ[c]) / 3 + anInt1653;
					anIntArrayArray1672[k5][anIntArray1671[k5]++] = t;
				}
			}
		}
		
		if (trianglePriorities == null) {
			for (int i1 = anInt1652 - 1; i1 >= 0; i1--) {
				int l1 = anIntArray1671[i1];
				if (l1 > 0) {
					int ai[] = anIntArrayArray1672[i1];
					for (int j3 = 0; j3 < l1; j3++)
						method484(ai[j3]);
					
				}
			}
			
			return;
		}
		for (int j1 = 0; j1 < 12; j1++) {
			anIntArray1673[j1] = 0;
			anIntArray1677[j1] = 0;
		}
		
		for (int i2 = anInt1652 - 1; i2 >= 0; i2--) {
			int k2 = anIntArray1671[i2];
			if (k2 > 0) {
				int ai1[] = anIntArrayArray1672[i2];
				for (int i4 = 0; i4 < k2; i4++) {
					int l4 = ai1[i4];
					int l5 = trianglePriorities[l4];
					int j6 = anIntArray1673[l5]++;
					anIntArrayArray1674[l5][j6] = l4;
					if (l5 < 10)
						anIntArray1677[l5] += i2;
					else if (l5 == 10)
						anIntArray1675[j6] = i2;
					else
						anIntArray1676[j6] = i2;
				}
				
			}
		}
		
		int l2 = 0;
		if (anIntArray1673[1] > 0 || anIntArray1673[2] > 0)
			l2 = (anIntArray1677[1] + anIntArray1677[2])
					/ (anIntArray1673[1] + anIntArray1673[2]);
		int k3 = 0;
		if (anIntArray1673[3] > 0 || anIntArray1673[4] > 0)
			k3 = (anIntArray1677[3] + anIntArray1677[4])
					/ (anIntArray1673[3] + anIntArray1673[4]);
		int j4 = 0;
		if (anIntArray1673[6] > 0 || anIntArray1673[8] > 0)
			j4 = (anIntArray1677[6] + anIntArray1677[8])
					/ (anIntArray1673[6] + anIntArray1673[8]);
		int i6 = 0;
		int k6 = anIntArray1673[10];
		int ai2[] = anIntArrayArray1674[10];
		int ai3[] = anIntArray1675;
		if (i6 == k6) {
			i6 = 0;
			k6 = anIntArray1673[11];
			ai2 = anIntArrayArray1674[11];
			ai3 = anIntArray1676;
		}
		int i5;
		if (i6 < k6)
			i5 = ai3[i6];
		else
			i5 = -1000;
		for (int l6 = 0; l6 < 10; l6++) {
			while (l6 == 0 && i5 > l2) {
				method484(ai2[i6++]);
				if (i6 == k6 && ai2 != anIntArrayArray1674[11]) {
					i6 = 0;
					k6 = anIntArray1673[11];
					ai2 = anIntArrayArray1674[11];
					ai3 = anIntArray1676;
				}
				if (i6 < k6)
					i5 = ai3[i6];
				else
					i5 = -1000;
			}
			while (l6 == 3 && i5 > k3) {
				method484(ai2[i6++]);
				if (i6 == k6 && ai2 != anIntArrayArray1674[11]) {
					i6 = 0;
					k6 = anIntArray1673[11];
					ai2 = anIntArrayArray1674[11];
					ai3 = anIntArray1676;
				}
				if (i6 < k6)
					i5 = ai3[i6];
				else
					i5 = -1000;
			}
			while (l6 == 5 && i5 > j4) {
				method484(ai2[i6++]);
				if (i6 == k6 && ai2 != anIntArrayArray1674[11]) {
					i6 = 0;
					k6 = anIntArray1673[11];
					ai2 = anIntArrayArray1674[11];
					ai3 = anIntArray1676;
				}
				if (i6 < k6)
					i5 = ai3[i6];
				else
					i5 = -1000;
			}
			int i7 = anIntArray1673[l6];
			int ai4[] = anIntArrayArray1674[l6];
			for (int j7 = 0; j7 < i7; j7++)
				method484(ai4[j7]);
			
		}
		
		while (i5 != -1000) {
			method484(ai2[i6++]);
			if (i6 == k6 && ai2 != anIntArrayArray1674[11]) {
				i6 = 0;
				ai2 = anIntArrayArray1674[11];
				k6 = anIntArray1673[11];
				ai3 = anIntArray1676;
			}
			if (i6 < k6)
				i5 = ai3[i6];
			else
				i5 = -1000;
		}
		
		if (Client.instance.displayParticles && verticesParticle != null) {
			for (int vertex = 0; vertex < vertexCount; ++vertex) {
				int pid = verticesParticle[vertex] - 1;
				if (pid >= 0) {
					ParticleDefinition def = ParticleDefinition.cache[pid];
					int x = (int) getVertexX(vertex);
					int y = (int) getVertexY(vertex);
					int z = (int) getVertexZ(vertex);
					int depth = vertexPerspectiveDepth[vertex];
					if (lastRenderedRotation != 0) {
						int sine = Rasterizer.SINE[lastRenderedRotation];
						int cosine = Rasterizer.COSINE[lastRenderedRotation];
						int rotatedX = z * sine + x * cosine >> 16;
						z = z * cosine - x * sine >> 16;
						x = rotatedX;
					}
					x += renderAtPointX;
					z += renderAtPointY;
					
					ParticleVector pos = new ParticleVector(x, -y, z);
					
					for (int p = 0; p < def.getSpawnRate(); p++) {
						Particle particle = new Particle(def, pos, depth, pid);
						Client.instance.addParticle(particle);
					}
				}
			}
		}
		} catch(Exception e) {
			//e.printStackTrace();//TODO for now..
		}
	}
	
	private int lastRenderedRotation = 0;
	private int renderAtPointX;
	public int renderAtPointZ = 0;
	public int renderAtPointY = 0;
	public int[] verticesParticle;
	
	private final void method484(int i) {
		if (aBooleanArray1664[i]) {
			method485(i);
			return;
		}
		int j = triangleViewSpaceX[i];
		int k = triangleViewSpaceY[i];
		int l = triangleViewSpaceZ[i];
		Rasterizer.aBoolean1462 = aBooleanArray1663[i];
		if (triangleAlphaValues == null)
			Rasterizer.anInt1465 = 0;
		else
			Rasterizer.anInt1465 = triangleAlphaValues[i];
		int i1;
		if (drawTypes == null)
			i1 = 0;
		else
			i1 = drawTypes[i] & 3;
		if (i1 == 0) {
			Rasterizer.drawGouraudTriangle(vertexPerspectiveY[j], vertexPerspectiveY[k],
					vertexPerspectiveY[l], vertexPerspectiveX[j], vertexPerspectiveX[k],
					vertexPerspectiveX[l], colorValues1[i], colorValues2[i],
					colorValues3[i], vertexPerspectiveDepth[j],
					vertexPerspectiveDepth[k], vertexPerspectiveDepth[l]);
			return;
		}
		if (i1 == 1) {
			Rasterizer.drawFlatTriangle(vertexPerspectiveY[j], vertexPerspectiveY[k],
					vertexPerspectiveY[l], vertexPerspectiveX[j], vertexPerspectiveX[k],
					vertexPerspectiveX[l], modelIntArray3[colorValues1[i]], vertexPerspectiveDepth[j],
					vertexPerspectiveDepth[k], vertexPerspectiveDepth[l]);
			return;
		}
		if (i1 == 2) {
			int j1 = drawTypes[i] >> 2;
			int l1 = textureTrianglePIndex[j1];
			int j2 = textureTriangleMIndex[j1];
			int l2 = textureTriangleNIndex[j1];
			Rasterizer.drawTexturedTriangle(vertexPerspectiveY[j], vertexPerspectiveY[k],
					vertexPerspectiveY[l], vertexPerspectiveX[j], vertexPerspectiveX[k],
					vertexPerspectiveX[l], colorValues1[i], colorValues2[i],
					colorValues3[i], anIntArray1668[l1], anIntArray1668[j2],
					anIntArray1668[l2], anIntArray1669[l1], anIntArray1669[j2],
					anIntArray1669[l2], anIntArray1670[l1], anIntArray1670[j2],
					anIntArray1670[l2], colorValues[i], vertexPerspectiveDepth[j],
					vertexPerspectiveDepth[k], vertexPerspectiveDepth[l]);
			return;
		}
		if (i1 == 3) {
			int k1 = drawTypes[i] >> 2;
			int i2 = textureTrianglePIndex[k1];
			int k2 = textureTriangleMIndex[k1];
			int i3 = textureTriangleNIndex[k1];
			Rasterizer.drawTexturedTriangle(vertexPerspectiveY[j], vertexPerspectiveY[k],
					vertexPerspectiveY[l], vertexPerspectiveX[j], vertexPerspectiveX[k],
					vertexPerspectiveX[l], colorValues1[i], colorValues1[i],
					colorValues1[i], anIntArray1668[i2], anIntArray1668[k2],
					anIntArray1668[i3], anIntArray1669[i2], anIntArray1669[k2],
					anIntArray1669[i3], anIntArray1670[i2], anIntArray1670[k2],
					anIntArray1670[i3], colorValues[i], vertexPerspectiveDepth[j],
					vertexPerspectiveDepth[k], vertexPerspectiveDepth[l]);
		}
	}
	
	private final void method485(int i) {
		int j = Rasterizer.center_x;
		int k = Rasterizer.center_y;
		int l = 0;
		int i1 = triangleViewSpaceX[i];
		int j1 = triangleViewSpaceY[i];
		int k1 = triangleViewSpaceZ[i];
		
		int l1 = anIntArray1670[i1];
		int i2 = anIntArray1670[j1];
		int j2 = anIntArray1670[k1];
		
		if (l1 >= 50) {
			anIntArray1678[l] = vertexPerspectiveX[i1];
			anIntArray1679[l] = vertexPerspectiveY[i1];
			anIntArray1680[l++] = colorValues1[i];
		} else {
			int k2 = anIntArray1668[i1];
			int k3 = anIntArray1669[i1];
			int k4 = colorValues1[i];
			if (j2 >= 50) {
				int k5 = (50 - l1) * modelIntArray4[j2 - l1];
				anIntArray1678[l] = j + ((k2 + ((anIntArray1668[k1] - k2) * k5 >> 16)) * Client.fov) / 50;
				anIntArray1679[l] = k + ((k3 + ((anIntArray1669[k1] - k3) * k5 >> 16)) * Client.fov) / 50;
				anIntArray1680[l++] = k4 + ((colorValues3[i] - k4) * k5 >> 16);
			}
			if (i2 >= 50) {
				int l5 = (50 - l1) * modelIntArray4[i2 - l1];
				anIntArray1678[l] = j + ((k2 + ((anIntArray1668[j1] - k2) * l5 >> 16)) * Client.fov) / 50;
				anIntArray1679[l] = k + ((k3 + ((anIntArray1669[j1] - k3) * l5 >> 16)) * Client.fov) / 50;
				anIntArray1680[l++] = k4 + ((colorValues2[i] - k4) * l5 >> 16);
			}
		}
		if (i2 >= 50) {
			anIntArray1678[l] = vertexPerspectiveX[j1];
			anIntArray1679[l] = vertexPerspectiveY[j1];
			anIntArray1680[l++] = colorValues2[i];
		} else {
			int l2 = anIntArray1668[j1];
			int l3 = anIntArray1669[j1];
			int l4 = colorValues2[i];
			if (l1 >= 50) {
				int i6 = (50 - i2) * modelIntArray4[l1 - i2];
				anIntArray1678[l] = j + ((l2 + ((anIntArray1668[i1] - l2) * i6 >> 16)) * Client.fov) / 50;
				anIntArray1679[l] = k + ((l3 + ((anIntArray1669[i1] - l3) * i6 >> 16)) * Client.fov) / 50;
				anIntArray1680[l++] = l4 + ((colorValues1[i] - l4) * i6 >> 16);
			}
			if (j2 >= 50) {
				int j6 = (50 - i2) * modelIntArray4[j2 - i2];
				anIntArray1678[l] = j + ((l2 + ((anIntArray1668[k1] - l2) * j6 >> 16)) * Client.fov) / 50;
				anIntArray1679[l] = k + ((l3 + ((anIntArray1669[k1] - l3) * j6 >> 16)) * Client.fov) / 50;
				anIntArray1680[l++] = l4 + ((colorValues3[i] - l4) * j6 >> 16);
			}
		}
		if (j2 >= 50) {
			anIntArray1678[l] = vertexPerspectiveX[k1];
			anIntArray1679[l] = vertexPerspectiveY[k1];
			anIntArray1680[l++] = colorValues3[i];
		} else {
			int i3 = anIntArray1668[k1];
			int i4 = anIntArray1669[k1];
			int i5 = colorValues3[i];
			if (i2 >= 50) {
				int k6 = (50 - j2) * modelIntArray4[i2 - j2];
				anIntArray1678[l] = j + ((i3 + ((anIntArray1668[j1] - i3) * k6 >> 16)) * Client.fov) / 50;
				anIntArray1679[l] = k + ((i4 + ((anIntArray1669[j1] - i4) * k6 >> 16)) * Client.fov) / 50;
				anIntArray1680[l++] = i5 + ((colorValues2[i] - i5) * k6 >> 16);
			}
			if (l1 >= 50) {
				int l6 = (50 - j2) * modelIntArray4[l1 - j2];
				anIntArray1678[l] = j + ((i3 + ((anIntArray1668[i1] - i3) * l6 >> 16)) * Client.fov) / 50;
				anIntArray1679[l] = k + ((i4 + ((anIntArray1669[i1] - i4) * l6 >> 16)) * Client.fov) / 50;
				anIntArray1680[l++] = i5 + ((colorValues1[i] - i5) * l6 >> 16);
			}
		}
		int j3 = anIntArray1678[0];
		int j4 = anIntArray1678[1];
		int j5 = anIntArray1678[2];
		int i7 = anIntArray1679[0];
		int j7 = anIntArray1679[1];
		int k7 = anIntArray1679[2];
		if ((j3 - j4) * (k7 - j7) - (i7 - j7) * (j5 - j4) > 0) {
			Rasterizer.aBoolean1462 = false;
			if (l == 3) {
				if (j3 < 0 || j4 < 0 || j5 < 0 || j3 > Raster.bottomX
						|| j4 > Raster.bottomX || j5 > Raster.bottomX)
					Rasterizer.aBoolean1462 = true;
				int l7;
				if (drawTypes == null)
					l7 = 0;
				else
					l7 = drawTypes[i] & 3;
				if (l7 == 0)
					Rasterizer.drawGouraudTriangle(i7, j7, k7, j3, j4, j5,
							anIntArray1680[0], anIntArray1680[1],
							anIntArray1680[2], -1, -1, -1);
				else if (l7 == 1)
					Rasterizer.drawFlatTriangle(i7, j7, k7, j3, j4, j5,
							modelIntArray3[colorValues1[i]], -1, -1, -1);
				else if (l7 == 2) {
					int j8 = drawTypes[i] >> 2;
					int k9 = textureTrianglePIndex[j8];
					int k10 = textureTriangleMIndex[j8];
					int k11 = textureTriangleNIndex[j8];
					Rasterizer.drawTexturedTriangle(i7, j7, k7, j3, j4, j5,
							anIntArray1680[0], anIntArray1680[1],
							anIntArray1680[2], anIntArray1668[k9],
							anIntArray1668[k10], anIntArray1668[k11],
							anIntArray1669[k9], anIntArray1669[k10],
							anIntArray1669[k11], anIntArray1670[k9],
							anIntArray1670[k10], anIntArray1670[k11],
							colorValues[i], -1, -1, -1);
				} else if (l7 == 3) {
					int k8 = drawTypes[i] >> 2;
					int l9 = textureTrianglePIndex[k8];
					int l10 = textureTriangleMIndex[k8];
					int l11 = textureTriangleNIndex[k8];
					Rasterizer.drawTexturedTriangle(i7, j7, k7, j3, j4, j5,
							colorValues1[i], colorValues1[i],
							colorValues1[i], anIntArray1668[l9],
							anIntArray1668[l10], anIntArray1668[l11],
							anIntArray1669[l9], anIntArray1669[l10],
							anIntArray1669[l11], anIntArray1670[l9],
							anIntArray1670[l10], anIntArray1670[l11],
							colorValues[i], -1, -1, -1);
				}
			}
			if (l == 4) {
				if (j3 < 0 || j4 < 0 || j5 < 0 || j3 > Raster.bottomX
						|| j4 > Raster.bottomX || j5 > Raster.bottomX
						|| anIntArray1678[3] < 0
						|| anIntArray1678[3] > Raster.bottomX)
					Rasterizer.aBoolean1462 = true;
				int i8;
				if (drawTypes == null)
					i8 = 0;
				else
					i8 = drawTypes[i] & 3;
				if (i8 == 0) {
					Rasterizer.drawGouraudTriangle(i7, j7, k7, j3, j4, j5,
							anIntArray1680[0], anIntArray1680[1],
							anIntArray1680[2], -1, -1, -1);
					Rasterizer.drawGouraudTriangle(i7, k7, anIntArray1679[3], j3, j5,
							anIntArray1678[3], anIntArray1680[0],
							anIntArray1680[2], anIntArray1680[3], -1, -1, -1);
					return;
				}
				if (i8 == 1) {
					int l8 = modelIntArray3[colorValues1[i]];
					Rasterizer.drawFlatTriangle(i7, j7, k7, j3, j4, j5, l8, -1, -1, -1);
					Rasterizer.drawFlatTriangle(i7, k7, anIntArray1679[3], j3, j5,
							anIntArray1678[3], l8, -1, -1, -1);
					return;
				}
				if (i8 == 2) {
					int i9 = drawTypes[i] >> 2;
					int i10 = textureTrianglePIndex[i9];
					int i11 = textureTriangleMIndex[i9];
					int i12 = textureTriangleNIndex[i9];
					Rasterizer.drawTexturedTriangle(i7, j7, k7, j3, j4, j5,
							anIntArray1680[0], anIntArray1680[1],
							anIntArray1680[2], anIntArray1668[i10],
							anIntArray1668[i11], anIntArray1668[i12],
							anIntArray1669[i10], anIntArray1669[i11],
							anIntArray1669[i12], anIntArray1670[i10],
							anIntArray1670[i11], anIntArray1670[i12],
							colorValues[i], -1, -1, -1);
					Rasterizer.drawTexturedTriangle(i7, k7, anIntArray1679[3], j3, j5,
							anIntArray1678[3], anIntArray1680[0],
							anIntArray1680[2], anIntArray1680[3],
							anIntArray1668[i10], anIntArray1668[i11],
							anIntArray1668[i12], anIntArray1669[i10],
							anIntArray1669[i11], anIntArray1669[i12],
							anIntArray1670[i10], anIntArray1670[i11],
							anIntArray1670[i12], colorValues[i], -1, -1, -1);
					return;
				}
				if (i8 == 3) {
					int j9 = drawTypes[i] >> 2;
					int j10 = textureTrianglePIndex[j9];
					int j11 = textureTriangleMIndex[j9];
					int j12 = textureTriangleNIndex[j9];
					Rasterizer.drawTexturedTriangle(i7, j7, k7, j3, j4, j5,
							colorValues1[i], colorValues1[i],
							colorValues1[i], anIntArray1668[j10],
							anIntArray1668[j11], anIntArray1668[j12],
							anIntArray1669[j10], anIntArray1669[j11],
							anIntArray1669[j12], anIntArray1670[j10],
							anIntArray1670[j11], anIntArray1670[j12],
							colorValues[i], -1, -1, -1);
					Rasterizer.drawTexturedTriangle(i7, k7, anIntArray1679[3], j3, j5,
							anIntArray1678[3], colorValues1[i],
							colorValues1[i], colorValues1[i],
							anIntArray1668[j10], anIntArray1668[j11],
							anIntArray1668[j12], anIntArray1669[j10],
							anIntArray1669[j11], anIntArray1669[j12],
							anIntArray1670[j10], anIntArray1670[j11],
							anIntArray1670[j12], colorValues[i], -1, -1, -1);
				}
			}
		}
	}
	
	private final boolean method486(int i, int j, int k, int l, int i1, int j1,
	                                int k1, int l1) {
		if (j < k && j < l && j < i1)
			return false;
		if (j > k && j > l && j > i1)
			return false;
		if (i < j1 && i < k1 && i < l1)
			return false;
		return i <= j1 || i <= k1 || i <= l1;
	}

	public static Model entityModel = new Model();
	private static int entityAlphas[] = new int[1];

	public static Model spotAnimModel = new Model();
	private static int spotAnimAlphas[] = new int[1];

	public static Model locModel = new Model();
	private static int locAlphas[] = new int[1];

	public int vertexCount;
	public int triangleCount;
	public int triangleViewSpaceX[];
	public int triangleViewSpaceY[];
	public int triangleViewSpaceZ[];
	public int colorValues1[];
	public int colorValues2[];
	public int colorValues3[];
	public int drawTypes[];
	public int trianglePriorities[];
	public int triangleAlphaValues[];
	public int colorValues[];
	public int anInt1641;
	public int texTriangleCount;
	public int textureTrianglePIndex[];
	public int textureTriangleMIndex[];
	public int textureTriangleNIndex[];
	public int particleLifespanX[];
	public int particleLifespanZ[];
	public int particleLifespanY[];
	public int particleDirectionZ[];
	public int particleDirectionY[];
	public int particleDirectionX[];
	public int texturePrimaryColor[];
	public int textureSecondaryColor[];
	public int minX;
	public int maxX;
	public int maxZ;
	public int minZ;
	public int anInt1650;
	public int minY;
	public int anInt1652;
	public int anInt1653;
	public int anInt1654;
	public int anIntArray1655[];
	public int anIntArray1656[];
	public int anIntArrayArray1657[][];
	public int anIntArrayArray1658[][];
	public boolean aBoolean1659;
	public Vertex aClass33Array1660[];
	static ModelDef aClass21Array1661[];
	static ModelDef osrsModelDefCache[];
	static OnDemandFetcher aOnDemandFetcher;
	static boolean aBooleanArray1663[] = new boolean[10000];
	static boolean aBooleanArray1664[] = new boolean[10000];
	public static int vertexPerspectiveX[] = new int[10000];
	public static int vertexPerspectiveY[] = new int[10000];
	public static int vertexPerspectiveZ[] = new int[10000];
	static int anIntArray1668[] = new int[10000];
	static int anIntArray1669[] = new int[10000];
	static int anIntArray1670[] = new int[10000];
	static int vertexPerspectiveDepth[] = new int[10000];
	static int anIntArray1671[] = new int[3000];
	static int anIntArrayArray1672[][] = new int[2000][512];
	static int anIntArray1673[] = new int[12];
	static int anIntArrayArray1674[][] = new int[12][5000]; // polygons
	static int anIntArray1675[] = new int[5000];
	static int anIntArray1676[] = new int[5000];
	static int anIntArray1677[] = new int[12];
	static int anIntArray1678[] = new int[10];
	static int anIntArray1679[] = new int[10];
	static int anIntArray1680[] = new int[10];
	static float anInt1681;
	static float anInt1682;
	static float anInt1683;
	public static boolean aBoolean1684;
	public static int anInt1685;
	public static int anInt1686;
	public static int anInt1687;
	public static long anIntArray1688[] = new long[1000];
	static int modelIntArray3[];
	static int modelIntArray4[];
	
	static {
		modelIntArray3 = Rasterizer.anIntArray1482;
		modelIntArray4 = Rasterizer.anIntArray1469;
	}
	
	public void completelyRecolor(int j) {
		for (int k = 0; k < triangleCount; k++) {
			colorValues[k] = j;
		}
	}
	
	public void shadingRecolor(int j) {
		j += 100;
		int kcolor = 0;
		for (int k = 0; k < triangleCount; k++) {
			kcolor = colorValues[k];
			if (k + j >= 0) {
				colorValues[k] = (kcolor + j);
			}
		}
	}
	
	public void shadingRecolor2(int j) {
		
		for (int k = 0; k < triangleCount; k++) {
			if (k + j >= 0) {
				colorValues[k] = (k + j);
			}
		}
	}
	
	public void shadingRecolor4(int j) {
		
		for (int k = 0; k < triangleCount; k++) {
			if ((colorValues[k] != 65535) && (k + j >= 0)) {
				colorValues[k] += j;
			}
		}
	}
	
	public void shadingRecolor3(int j) {
		
		for (int k = 0; k < triangleCount; k++) {
			int lastcolor = 1;
			if ((colorValues[k] + j >= 10000)
					&& (colorValues[k] + j <= 90000)) {
				colorValues[k] = (k + j + lastcolor);
			}
			lastcolor++;
		}
	}
	
	public void modelRecoloring(int i, int j) {
		for (int k = 0; k < triangleCount; k++) {
			if (colorValues[k] == i) {
				colorValues[k] = j;
			}
		}
	}
	
	public void setTexture(int fromColor, int tex) {
		//printModelColours(this);
		int foundAmt = 0;
		int set2 = 0;
		for (int i = 0; i < colorValues.length; i++)
			if (fromColor == colorValues[i])
				foundAmt++;
		texTriangleCount = foundAmt;
		if (drawTypes == null)
			drawTypes = new int[foundAmt];
		colorValues = new int[foundAmt];
		textureTrianglePIndex = new int[foundAmt];
		textureTriangleMIndex = new int[foundAmt];
		textureTriangleNIndex = new int[foundAmt];
		int assigned = 0;
		for (int i = 0; i < triangleCount; i++) {
			if (fromColor == colorValues[i]) {
				colorValues[i] = tex;
				drawTypes[i] = 3 + set2;
				set2 += 4;
				textureTrianglePIndex[assigned] = triangleViewSpaceX[i];
				textureTriangleMIndex[assigned] = triangleViewSpaceY[i];
				textureTriangleNIndex[assigned] = triangleViewSpaceZ[i];
				assigned++;
			}
		}
	}
	
	public void setTexture(int tex) {
		texTriangleCount = triangleCount;
		int set2 = 0;
		if (drawTypes == null)
			drawTypes = new int[triangleCount];
		if (colorValues == null)
			colorValues = new int[triangleCount];
		textureTrianglePIndex = new int[triangleCount];
		textureTriangleMIndex = new int[triangleCount];
		textureTriangleNIndex = new int[triangleCount];
		
		for (int i = 0; i < triangleCount; i++) {
			colorValues[i] = tex;
			drawTypes[i] = 3 + set2;
			set2 += 4;
			textureTrianglePIndex[i] = triangleViewSpaceX[i];
			textureTriangleMIndex[i] = triangleViewSpaceY[i];
			textureTriangleNIndex[i] = triangleViewSpaceZ[i];
		}
	}
	
	public static void printModelColours(Model model) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i : model.colorValues) {
			list.add(i);
		}
		ArrayList<Integer> done = new ArrayList<Integer>();
		for (Integer i : list) {
			if (done.contains(i))
				continue;
			int amt = 0;
			for (Integer j : list) {
				if (j.intValue() == i.intValue())
					amt++;
			}
			System.out.println(i + " on " + amt + " faces");
			done.add(i);
		}
	}

	public static int pools = 1;
	
	private int verticesLen = 0;
	private float[] verticesData = null;
	
	private void initVertices(int len) {
		verticesData = new float[len * 3];
		verticesLen = len;
	}
	
	private void copyVertices(float[] verticesData) {
		copyVertices(verticesData, 0);
	}
	
	private void copyVertices(float[] verticesData, int offset) {
		final int arraySize = verticesData.length;
		
		// create new data array
		initVertices(arraySize / 3);
		
		// copy from srcBuffer
		System.arraycopy(verticesData, offset, this.verticesData, offset, arraySize);
	}
	
	public int getVertexLen() {
		return verticesLen;
	}
	
	public float getVertexX(int i) {
		return verticesData[i];
	}
	
	public void setVertexX(int idx, float value) {
		this.verticesData[idx] = value;
	}
	
	public float getVertexY(int idx) {
		return this.verticesData[verticesLen + idx];
	}
	
	public void setVertexY(int idx, float value) {
		this.verticesData[verticesLen + idx] = value;
	}
	
	public float getVertexZ(int idx) {
		return this.verticesData[verticesLen + verticesLen + idx];
	}
	
	public void setVertexZ(int idx, float value) {
		this.verticesData[verticesLen + verticesLen + idx] = value;
	}

	public void adjustToGround(int hXY, int hX1Y, int hXY1, int hX1Y1) {
		int avg = (hXY + hX1Y + hX1Y1 + hXY1) / 4;
		for (int v = 0; v < vertexCount; v++) {
			float x = getVertexX(v);
			float z = getVertexZ(v);
			float dx1 = hXY + ((hX1Y - hXY) * (x + 64)) / 128;
			float dx2 = hXY1 + ((hX1Y1 - hXY1) * (x + 64)) / 128;
			float dz = dx1 + ((dx2 - dx1) * (z + 64)) / 128;
			setVertexY(v, getVertexY(v) + (dz - avg));
		}
	}
	
	public float uv[][] = null;
	public int textureAnimationSpeed = 100;

	public int getTexture(int k) {
		if ((colorValues != null && k >= colorValues.length) || (texturePointers != null && k >= texturePointers.length)) {
			return -1;
		}
		
		int drawType = drawTypes != null && k < drawTypes.length ? drawTypes[k] & 0xFF : 0;
		int texture = textureBackgrounds != null ? textureBackgrounds[k] : -1;
		
		boolean clearColors = false;
		boolean alwaysDraw = false;
		
		if ((colorValues[k] < Rasterizer.textureAmount || texturePointers != null && k < texturePointers.length && texturePointers[k] != -1)) {
			if (texture == 0 && colorValues[k] < Rasterizer.textureAmount && textureBackgrounds == null) {
				texture = colorValues[k];
				clearColors = true;
			}
		}
		
		// texturing for old models, revision 317
		if (texture <= 0 && colorValues[k] < Rasterizer.textureAmount && drawType != 1 && drawType != 0) {
			texture = colorValues[k];
		}
		
		// default texture on everything
		if (texture <= 0) {
			//texture = DEFAULT_TEXTURE;
		}
		
		if (texture >= Texture.cache.length || texture == 42) {
			return -1;
		}
		
		// texture definition class
		Texture td = null;
		if (texture > 0)
			td = Texture.cache[texture];
		
		// always draw this texture?
		if (texture >= 0)
			alwaysDraw = ModelConfig.basicTexture[texture] || texture < Rasterizer.textureAmount;
		
		// always draw following textures
		// tree crowns, older textures, roofs, etc...
		if (td != null && td.transparentTexture && texture > 0) {
			alwaysDraw = true;
			if (texture != 922)
				clearColors = true;
		}
		
		// textures for every model enabled?
		if (texture >= 0 && !alwaysDraw)// && !Rasterizer.modelsTextured)
		{
			// no texturing
			if (!ModelConfig.basicTexture[texture]) {
				clearColors = false;
				texture = -1;
			}
		}
		
		if (clearColors && texture > 0)
			texture *= -1;
		
		return texture;
	}

	public void calculateUV(boolean animated, int mId, ModelDef mDef) {
		if (uv != null) {
			System.out.println("uv already calced");
			return;
		}
		
		int[] faces = null;
		com.soulplayps.client.opengl.gl15.particle.Particle particles = null;
		if (mDef.uv == null) {
			//System.out.println("new mDef uv: "+mId+", "+mDef.uv+", "+triangleCount);
			mDef.uv = uv = new float[triangleCount][6];
		} else {
			//System.out.println("returning mDef uvs");
			uv = mDef.uv;
			return;
		}
		
		//System.out.println("calc: "+mDef.uv);
		
		for (int k = 0; k < triangleCount; k++) {
			int drawType = drawTypes != null && k < drawTypes.length ? drawTypes[k] & 0xFF : 0;
			
			int pt0 = triangleViewSpaceX[k];
			int pt1 = triangleViewSpaceY[k];
			int pt2 = triangleViewSpaceZ[k];
			
			boolean flipUV = false;
			
			int texPtr = texturePointers == null ? 0 : texturePointers[k];
			if (texPtr < 0 && texPtr != -1) {
				texPtr &= 0xFF;
				//System.out.println("ptr: "+texPtr);
			}
			
			// abyssal vine whip white triangle fix
			if (mId == 10253 || mId == 10247) {
				int c = colorValues[k];
				if (c == 37798) {
					colorValues[k] = 64192;
					
					//System.out.println("tex: "+textureBackgrounds[k]);
					
					if (textureBackgrounds != null)
						textureBackgrounds[k] = 91;
					
					if (colorValues1 != null) {
						colorValues1[k] = 64192;
						colorValues2[k] = 64192;
						colorValues3[k] = 64192;
					}
					
					triangleAlphaValues[k] = 210;
				} else if (c == 64192 || c == 64178) {
					triangleAlphaValues[k] = 50;
				} else {
					textureBackgrounds[k] = 1389;
				}
			}
			
			// tormented demon upper triangle
			else if (mId == 44733) {
				if (k == 1973)
					triangleAlphaValues[k] = 255;
			}
			
			// elemental staff
			else if (mId == 56224) {
				if (colorValues[k] >= 43086)
					triangleAlphaValues[k] = 80;
			}
			
			// elemental bow
			else if (mId == 65558 || mId == 65560) {
				if (textureBackgrounds == null)
					textureBackgrounds = new short[triangleCount];
				textureBackgrounds[k] = 91;
			}
			
			// default texture for new models
			else if (mId >= 65518) {
				if (textureBackgrounds == null)
					textureBackgrounds = new short[triangleCount];
				textureBackgrounds[k] = 91;
			}
			
			int texType = textureTypes == null || texPtr >= textureTypes.length || texPtr == -1 ? 16 : textureTypes[texPtr];
			int texture = getTexture(k);
			if (texture == -1) continue;

			if (texture < -1)
				texture *= -1;

			if (texture >= Texture.COUNT)
				continue;

			if (texture == 59 && (mId != 33144 && mId != 33103 && mId != 33111)) {//Infernal cape
				textureBackgrounds[k] = -1;
				continue;
			}

			// TODO: new firecape idea
			/*if (mId == 9638)
			{
				if (texture == 40)
				{
					textureBackgrounds[k] = 61;
					colorValues[k] = ItemDef.COLOR_ICE;
				}
			}*/
			
			if (texture > 0 && ModelConfig.animatedTexture[texture]) {
				animated = true;
			}
			
			// old models, e.g. firecape
			if (drawType > 3) {
				texPtr = ((drawType & 0xFF) >> 2);
				//texPtr = 0;
				drawType = 0;
				
				if (texPtr == 2 && texture == 40 && animated) {
					flipUV = true;
				}
			}
			
			boolean uvPut = false;
			
			if (texPtr == 255 || texPtr == -1) {
				texPtr = 0;
				texType = 16;
			}
			
			float u1 = 0;
			float v1 = 1;
			float u2 = 1;
			float v2 = 1;
			float u3 = 0;
			float v3 = 0;
			
			
			// mostly human skins
			if (texPtr == -1 || texPtr == 255) {
				u1 = 0;
				v1 = 1;
				u2 = 1;
				v2 = 1;
				u3 = 0;
				v3 = 0;
				
				uvPut = true;
			}
			
			// unused in revision 667, probably new models
			/*else if (texPtr >= 32765 || anInt1234 != 0)//TODO this if doesnt wrok
			{
                int var72 = aByteArray1241[k] & 255;
                int var73 = aByteArray1266[k] & 255;
                int var23 = aByteArray1243[k] & 255;
                var72 += anIntArray1231[pt0];
                var73 += anIntArray1231[pt1];
                var23 += anIntArray1231[pt2];
                
                u1 = texCoordU[var72];
                v1 = texCoordV[var72];
                u2 = texCoordU[var73];
                v2 = texCoordV[var73];
                u3 = texCoordU[var23];
                v3 = texCoordV[var23];

				uvPut = true;
            }*/
			
			// example: normal trees and evergreens near barbarian
			else if (texType == 0 && textureTrianglePIndex != null && texPtr < textureTrianglePIndex.length) {
				int pI = textureTrianglePIndex[texPtr];
				int mI = textureTriangleMIndex[texPtr];
				int nI = textureTriangleNIndex[texPtr];
				
				if (pI >= getVertexLen())
					pI = pt0;
				if (mI >= getVertexLen())
					mI = pt1;
				if (nI >= getVertexLen())
					nI = pt2;
				
				final float px = getVertexX(pI);
				final float py = getVertexY(pI);
				final float pz = getVertexZ(pI);
				
				final float mx = getVertexX(mI) - px;
				final float my = getVertexY(mI) - py;
				final float mz = getVertexZ(mI) - pz;
				final float nx = getVertexX(nI) - px;
				final float ny = getVertexY(nI) - py;
				final float nz = getVertexZ(nI) - pz;
				
				final float xx = getVertexX(pt0) - px;
				final float yx = getVertexY(pt0) - py;
				final float zx = getVertexZ(pt0) - pz;
				final float xy = getVertexX(pt1) - px;
				final float yy = getVertexY(pt1) - py;
				final float zy = getVertexZ(pt1) - pz;
				final float xz = getVertexX(pt2) - px;
				final float yz = getVertexY(pt2) - py;
				final float zz = getVertexZ(pt2) - pz;
				
				final float mnyz = my * nz - mz * ny;
				final float mnxz = mz * nx - mx * nz;
				final float mnxy = mx * ny - my * nx;
				float mn1 = ny * mnxy - nz * mnxz;
				float mn2 = nz * mnyz - nx * mnxy;
				float mn3 = nx * mnxz - ny * mnyz;
				
				float inv = 1f / (mn1 * mx + mn2 * my + mn3 * mz);
				
				u1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
				u2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
				u3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
				
				mn1 = my * mnxy - mz * mnxz;
				mn2 = mz * mnyz - mx * mnxy;
				mn3 = mx * mnxz - my * mnyz;
				
				inv = 1.0F / (mn1 * nx + mn2 * ny + mn3 * nz);
				
				v1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
				v2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
				v3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
				
				if (texture > 0 && ModelConfig.animatedTexture[texture]) {
					if (texture == 17) {
					} else {
						float tu1 = u1;
						float tu2 = u2;
						float tu3 = u3;
						u1 = v1;
						u2 = v2;
						u3 = v3;
						v1 = tu1;
						v2 = tu2;
						v3 = tu3;
						
						// animated textures are saved 3x in atlas
						// can safely shift to the right
						if (u1 < 0 || u2 < 0 || u3 < 0) {
							u1++;
							u2++;
							u3++;
						}
						
						while (v1 < 0 || v2 < 0 || v3 < 0)
						{
							v1++;
							v2++;
							v3++;
						}
						
						if (v1 > 1) v1 = 1;
						if (v2 > 1) v2 = 1;
						if (v3 > 1) v3 = 1;
						if (v1 < 0) v1 = 0;
						if (v2 < 0) v2 = 0;
						if (v3 < 0) v3 = 0;
					}
				} else {
					if (u1 < 0) u1 = 0;
					if (u1 > 1) u1 = 1;
					if (u2 < 0) u2 = 0;
					if (u2 > 1) u2 = 1;
					if (u3 < 0) u3 = 0;
					if (u3 > 1) u3 = 1;
					
					if (v1 < 0) v1 = 0;
					if (v1 > 1) v1 = 1;
					if (v2 < 0) v2 = 0;
					if (v2 > 1) v2 = 1;
					if (v3 < 0) v3 = 0;
					if (v3 > 1) v3 = 1;
					
					//if (texture > 0 && ModelConfig.flipUVTexture[texture])
					{
						v1 = 1 - v1;
						v2 = 1 - v2;
						v3 = 1 - v3;
					}
				}
				
				uvPut = true;
			}
			
			
			// example: fire cape
			else if (texType >= 4 && textureTrianglePIndex != null && texture > 0) {
				texPtr &= 0xFF;
				
				if (texPtr < textureTrianglePIndex.length) {
					int pI = textureTrianglePIndex[texPtr];
					int mI = textureTriangleMIndex[texPtr];
					int nI = textureTriangleNIndex[texPtr];
					
					if (pI >= getVertexLen())
						pI = pt0;
					if (mI >= getVertexLen())
						mI = pt1;
					if (nI >= getVertexLen())
						nI = pt2;
					
					final float px = getVertexX(pI);
					final float py = getVertexY(pI);
					final float pz = getVertexZ(pI);
					
					final float mx = getVertexX(mI) - px;
					final float my = getVertexY(mI) - py;
					final float mz = getVertexZ(mI) - pz;
					final float nx = getVertexX(nI) - px;
					final float ny = getVertexY(nI) - py;
					final float nz = getVertexZ(nI) - pz;
					
					final float xx = getVertexX(pt0) - px;
					final float yx = getVertexY(pt0) - py;
					final float zx = getVertexZ(pt0) - pz;
					final float xy = getVertexX(pt1) - px;
					final float yy = getVertexY(pt1) - py;
					final float zy = getVertexZ(pt1) - pz;
					final float xz = getVertexX(pt2) - px;
					final float yz = getVertexY(pt2) - py;
					final float zz = getVertexZ(pt2) - pz;
					
					final float mnyz = my * nz - mz * ny;
					final float mnxz = mz * nx - mx * nz;
					final float mnxy = mx * ny - my * nx;
					float mn1 = ny * mnxy - nz * mnxz;
					float mn2 = nz * mnyz - nx * mnxy;
					float mn3 = nx * mnxz - ny * mnyz;
					
					float inv = 1f / (mn1 * mx + mn2 * my + mn3 * mz);
					
					u1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
					u2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
					u3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
					
					mn1 = my * mnxy - mz * mnxz;
					mn2 = mz * mnyz - mx * mnxy;
					mn3 = mx * mnxz - my * mnyz;
					
					inv = 1.0F / (mn1 * nx + mn2 * ny + mn3 * nz);
					
					v1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
					v2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
					v3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
					
					if (u1 < 0) u1 = 0;
					if (u1 > 1) u1 = 1;
					if (u2 < 0) u2 = 0;
					if (u2 > 1) u2 = 1;
					if (u3 < 0) u3 = 0;
					if (u3 > 1) u3 = 1;
					
					if (v1 < 0) v1 = 0;
					if (v1 > 1) v1 = 1;
					if (v2 < 0) v2 = 0;
					if (v2 > 1) v2 = 1;
					if (v3 < 0) v3 = 0;
					if (v3 > 1) v3 = 1;
					
					
					if (texture > 0 && ModelConfig.animatedTexture[texture]) {
						float tu1 = u1;
						float tu2 = u2;
						float tu3 = u3;
						
						u1 = v1;
						u2 = v2;
						u3 = v3;
						
						v1 = tu1;
						v2 = tu2;
						v3 = tu3;
						
					} else {
						v1 = 1 - v1;
						v2 = 1 - v2;
						v3 = 1 - v3;
					}
					
					uvPut = true;
				}
			} else {
				//System.out.println("unhandled texture: " + texture);
			}
			
			if (!uvPut) {
				if (textureTypes != null && textureTypes.length != texTriangleCount)
					texTriangleCount = textureTypes.length;
				
				// copied RS method
				if (texturePointers != null && texTriangleCount > 0) {
					if (faces == null) {
						
						Texture def = Texture.get(texture);
						int blendType = def != null ? def.anInt1226 : 0;
						/*if (blendType != 1 && blendType != 2)
							blendType = 0;*/
						
						/*if (animated)
							System.out.println(texture + " is animated");*/
						
						//int scale = 8192;
						int scale = animated ? 4096 : 8192;
						
						// done by Jire
						//TODO disable 64x64 size for now?
						if (texture >= 0 && Texture.tex64x64[texture]) {
							if (blendType == 0/* && def.aByte1214 == 1*/) {
								// obj: 3979
								//System.out.println("tex["+texture+"]: "+def.toString());
								scale = 4096;
							}
						}
						
						//if (!animated)
						//	scale = 8192;
						
						// done by Jire but left commented out
						/*if (texture >= 0 && texture < Texture.tex64x64.length && Texture.tex64x64[texture])
							scale = 4096;
						else
							scale = 8192;*/
						
						int count = triangleCount > 1 ? 1 : triangleCount;
						//count = triangleCount;
						
						faces = new int[count];
						for (int triangle = 0; triangle < count; triangle++)
							faces[triangle] = 0;
						
						
						particles = ParticleGenerator.method2485(this, count, faces, scale);
					}
					
					float[] fs = new float[2];
					float[] U = new float[3];// aFloatArrayArray5098[face] = new float[3];
					float[] V = new float[3];// aFloatArrayArray5071[face] = new float[3];
					
					if (texPtr >= particles.verticesX.length) {
						//System.out.println("err ptr: "+texPtr+" / "+particles.verticesX.length+"; "+textureTrianglePIndex.length+"; "+triangleCount+"; "+texTriangleCount);
						//texPtr = 0;
						
						//texPtr -= 128;
						//texPtr = 0;//(drawType & 0xFF) >> 2;
						//if (texPtr == particles.verticesX.length)
						texPtr = 0;
						
					}
					
					//if (texPtr <= 0)
					if (texPtr <= 0)
						texPtr &= 0xFF;
					
					
					int anLocalInt_904_ = pt0;
					int anLocalInt_905_ = pt1;
					int anLocalInt_906_ = pt2;
					int particleVertexX = particles.verticesX[texPtr];
					int particleVertexY = particles.verticesY[texPtr];
					int particleVertexZ = particles.verticesZ[texPtr];
					float[] particleCoordinates = particles.coordinates[texPtr];
					int anLocalInt_911_ = particleLifespanY == null ? 1 : particleLifespanY[texPtr];
					float f = particleLifespanZ == null ? 1 : ((float) (particleLifespanZ[texPtr]) / 256.0F);
					
					boolean loaded = false;
					
					//if (texType != 3)
					int oTexType = texType;
					texType = 2;
					
					if (particleCoordinates != null) {
						if (texType == 1) {
							float f_912_ = particleDirectionZ == null ? 1f : ((float) (particleDirectionZ[texPtr]) / 1024.0F);
							fs = ModelCalculations.method699(particleVertexX, particleCoordinates,
									anLocalInt_911_, f, particleVertexY,
									getVertexY(anLocalInt_904_), f_912_,
									particleVertexZ,
									getVertexZ(anLocalInt_904_), fs,
									getVertexX(anLocalInt_904_), -110);
							U[0] = fs[0];
							V[0] = fs[1];
							fs = ModelCalculations.method699(particleVertexX, particleCoordinates,
									anLocalInt_911_, f, particleVertexY,
									getVertexY(anLocalInt_905_), f_912_,
									particleVertexZ,
									getVertexZ(anLocalInt_905_), fs,
									getVertexX(anLocalInt_905_), -118);
							U[1] = fs[0];
							V[1] = fs[1];
							fs = ModelCalculations.method699(particleVertexX, particleCoordinates,
									anLocalInt_911_, f, particleVertexY,
									getVertexY(anLocalInt_906_), f_912_,
									particleVertexZ,
									getVertexZ(anLocalInt_906_), fs,
									getVertexX(anLocalInt_906_), -116);
							U[2] = fs[0];
							V[2] = fs[1];
							
							float f_913_ = f_912_ / 2.0F;
							if ((anLocalInt_911_ & 0x1) == 0) {
								if (U[1] - U[0] > f_913_)
									U[1] -= f_912_;
								else if (U[0] - U[1] > f_913_)
									U[1] += f_912_;
								if (U[2] - U[0] > f_913_)
									U[2] -= f_912_;
								else if (U[0] - U[2] > f_913_)
									U[2] += f_912_;
							} else {
								if (V[1] - V[0] > f_913_)
									V[1] -= f_912_;
								else if (V[0] - V[1] > f_913_)
									V[1] += f_912_;
								
								if (V[2] - V[0] > f_913_)
									V[2] -= f_912_;
								else if (V[0] - V[2] > f_913_)
									V[2] += f_912_;
							}
							
							loaded = true;
						} else if (texType == 2) {
							int textureCoord = texPtr;
							float f_914_ = 0f;
							float f_915_ = 0f;
							if (texturePrimaryColor != null) {
								f_914_ = ((float) (texturePrimaryColor[textureCoord]) / 256.0F);
								f_915_ = ((float) (textureSecondaryColor[textureCoord]) / 256.0F);
							}
							
							float anLocalInt_916_ = (getVertexX(anLocalInt_905_) - getVertexX(anLocalInt_904_));
							float anLocalInt_917_ = (getVertexY(anLocalInt_905_) - getVertexY(anLocalInt_904_));
							float anLocalInt_918_ = (getVertexZ(anLocalInt_905_) - getVertexZ(anLocalInt_904_));
							float anLocalInt_919_ = (getVertexX(anLocalInt_906_) - getVertexX(anLocalInt_904_));
							float anLocalInt_920_ = (getVertexY(anLocalInt_906_) - getVertexY(anLocalInt_904_));
							float anLocalInt_921_ = (getVertexZ(anLocalInt_906_) - getVertexZ(anLocalInt_904_));
							float anLocalInt_922_ = (anLocalInt_917_
									* anLocalInt_921_ - anLocalInt_920_
									* anLocalInt_918_);
							float anLocalInt_923_ = (anLocalInt_918_
									* anLocalInt_919_ - anLocalInt_921_
									* anLocalInt_916_);
							float anLocalInt_924_ = (anLocalInt_916_
									* anLocalInt_920_ - anLocalInt_919_
									* anLocalInt_917_);
							
							//float f_925_ = 64.0F / (float)(particleDirectionX[textureCoord]);
							//float f_926_ = 64.0F / (float)(particleDirectionY[textureCoord]);
							//float f_927_ = 64.0F / (float)(particleDirectionZ[textureCoord]);
							
							float f_925_ = -0.15625f;
							float f_926_ = -0.15625f;
							float f_927_ = -0.15625f;
							
		                    /*if (particleDirectionX[textureCoord] == 0)
		                    	f_925_ = -63.15625f;
		                    if (particleDirectionY[textureCoord] == 0)
		                    	f_926_ = -63.15625f;
		                    if (particleDirectionZ[textureCoord] == 0)
		                    	f_927_ = -63.15625f;*/
							
							float f_928_ = ((anLocalInt_922_
									* particleCoordinates[0] + anLocalInt_923_
									* particleCoordinates[1] + anLocalInt_924_
									* particleCoordinates[2]) / f_925_);
							float f_929_ = ((anLocalInt_922_
									* particleCoordinates[3] + anLocalInt_923_
									* particleCoordinates[4] + anLocalInt_924_
									* particleCoordinates[5]) / f_926_);
							float f_930_ = ((anLocalInt_922_
									* particleCoordinates[6] + anLocalInt_923_
									* particleCoordinates[7] + anLocalInt_924_
									* particleCoordinates[8]) / f_927_);
							int anLocalInt_931_ = ModelCalculations.method1337(f_928_, f_929_, f_930_, -12855);
							
							fs = ModelCalculations.method1689(anLocalInt_931_, fs,
									anLocalInt_911_, particleVertexY,
									particleVertexZ,
									getVertexX(anLocalInt_904_),
									particleVertexX, 0, particleCoordinates, f_915_,
									getVertexZ(anLocalInt_904_),
									getVertexY(anLocalInt_904_), f, f_914_);
							U[0] = fs[0];
							V[0] = fs[1];
							fs = ModelCalculations.method1689(anLocalInt_931_, fs,
									anLocalInt_911_, particleVertexY,
									particleVertexZ,
									getVertexX(anLocalInt_905_),
									particleVertexX, 0, particleCoordinates, f_915_,
									getVertexZ(anLocalInt_905_),
									getVertexY(anLocalInt_905_), f, f_914_);
							U[1] = fs[0];
							V[1] = fs[1];
							fs = ModelCalculations.method1689(anLocalInt_931_, fs,
									anLocalInt_911_, particleVertexY,
									particleVertexZ,
									getVertexX(anLocalInt_906_),
									particleVertexX, 0, particleCoordinates, f_915_,
									getVertexZ(anLocalInt_906_),
									getVertexY(anLocalInt_906_), f, f_914_);
							U[2] = fs[0];
							V[2] = fs[1];
							
							loaded = true;
						} else if (texType == 3) {
							fs = ModelCalculations.method226(fs, particleVertexX,
									getVertexX(anLocalInt_904_),
									particleVertexY,
									getVertexZ(anLocalInt_904_),
									getVertexY(anLocalInt_904_), particleCoordinates,
									particleVertexZ, true, anLocalInt_911_, f);
							U[0] = fs[0];
							V[0] = fs[1];
							fs = ModelCalculations.method226(fs, particleVertexX,
									getVertexX(anLocalInt_905_),
									particleVertexY,
									getVertexZ(anLocalInt_905_),
									getVertexY(anLocalInt_905_), particleCoordinates,
									particleVertexZ, true, anLocalInt_911_, f);
							U[1] = fs[0];
							V[1] = fs[1];
							fs = ModelCalculations.method226(fs, particleVertexX,
									getVertexX(anLocalInt_906_),
									particleVertexY,
									getVertexZ(anLocalInt_906_),
									getVertexY(anLocalInt_906_), particleCoordinates,
									particleVertexZ, true, anLocalInt_911_, f);
							U[2] = fs[0];
							V[2] = fs[1];
							
							if ((anLocalInt_911_ & 0x1) == 0) {
								if (U[1] - U[0] > 0.5F)
									U[1]--;
								else if (U[0] - U[1] > 0.5F)
									U[1]++;
								if (U[2] - U[0] > 0.5F)
									U[2]--;
								else if (U[0] - U[2] > 0.5F)
									U[2]++;
							} else {
								if (V[1] - V[0] > 0.5F)
									V[1]--;
								else if (V[0] - V[1] > 0.5F)
									V[1]++;
								if (V[2] - V[0] > 0.5F)
									V[2]--;
								else if (V[0] - V[2] > 0.5F)
									V[2]++;
							}
							
							loaded = true;
						}
					}
					
					
					if (loaded) {
						u1 = U[0];
						u2 = U[1];
						u3 = U[2];
						v1 = V[0];
						v2 = V[1];
						v3 = V[2];

						/*while (u1 < 0) u1++;
						while (u2 < 0) u2++;
						while (u3 < 0) u3++;
						
						while (v1 < 0) v1++;
						while (v2 < 0) v2++;
						while (v3 < 0) v3++;

						if (oTexType == 1 || true)
						{
							if (v1 > 1 || v2 > 1 || v3 > 1)
							{
								float vm = AtlasModelLoader.max(v1, v2, v3);
								v1 /= vm;
								v2 /= vm;
								v3 /= vm;
							}
							
							if (u1 > 1 || u2 > 1 || u3 > 1)
							{
								float um = AtlasModelLoader.max(u1, u2, u3);
								u1 /= um;
								u2 /= um;
								u3 /= um;
							}
							
							if (v1 < 0 || v2 < 0 || v3 < 0)
							{
								float vm = -AtlasModelLoader.max(v1, v2, v3);
								v1 *= vm;
								v2 *= vm;
								v3 *= vm;
							}
							
							if (u1 < 0 || u2 < 0 || u3 < 0)
							{
								float um = -AtlasModelLoader.max(u1, u2, u3);
								u1 *= um;
								u2 *= um;
								u3 *= um;
							}
						}*/
						
						u1 = ModelCalculations.roundUV(u1);
						u2 = ModelCalculations.roundUV(u2);
						u3 = ModelCalculations.roundUV(u3);
						v1 = ModelCalculations.roundUV(v1);
						v2 = ModelCalculations.roundUV(v2);
						v3 = ModelCalculations.roundUV(v3);
						
						//String extra = particleVertexX+" "+particleVertexY+" "+particleVertexZ;
						//System.out.println("UVs: "+u1+"/"+v1+" "+u2+"/"+v2+" "+u3+"/"+v3+" "+texType+", "+extra+", "+particleCoordinates[0]+", "+f_912_);
						
						
						uvPut = true;
					}
				}
			}
			
			uv[k][0] = u1;
			uv[k][1] = v1;
			uv[k][2] = u2;
			uv[k][3] = v2;
			uv[k][4] = u3;
			uv[k][5] = v3;
		}
		
		
		faces = null;
		particles = null;
	}
	
}