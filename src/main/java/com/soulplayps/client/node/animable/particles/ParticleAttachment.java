package com.soulplayps.client.node.animable.particles;

import java.util.*;

public class ParticleAttachment {

	private static final Map<Integer, int[][]> attachments = new HashMap<Integer, int[][]>();

	public static int[][] getAttachments(int model) {
        return attachments.get(model);
    }

	static {
		//Completionist cape
		attachments.put(65297, new int[][] { { 494, 0 }, { 488, 0 }, { 485, 0 }, { 476, 0 }, { 482, 0 }, { 479, 0 }, { 491, 0 } });
		attachments.put(65316, new int[][] { { 494, 0 }, { 488, 0 }, { 485, 0 }, { 476, 0 }, { 482, 0 }, { 479, 0 }, { 491, 0 } });

		//Trimmed Completionist Cape
		attachments.put(65295, new int[][] { { 494, 1 }, { 488, 1 }, { 485, 1 }, { 476, 1 }, { 482, 1 }, { 479, 1 }, { 491, 1 } });
		attachments.put(65328, new int[][] { { 494, 1 }, { 488, 1 }, { 485, 1 }, { 476, 1 }, { 482, 1 }, { 479, 1 }, { 491, 1 } });
		
		//Tokhaar-kal cape need to improve fire
		attachments.put(62575, new int[][] {{ 0, 2 }, { 1, 2 }, { 3, 2 }, { 131, 2 }, { 132, 2 }, { 133, 2 }, { 134, 2 }, { 135, 2 }, { 136, 2 }, { 137, 2 }, { 138, 2 }, { 139, 2 }, { 140, 2 }, { 141, 2 }, { 142, 2 }, { 145, 2 },});
		attachments.put(62582, new int[][] {{ 0, 2 }, { 1, 2 }, { 3, 2 }, { 131, 2 }, { 132, 2 }, { 133, 2 }, { 134, 2 }, { 135, 2 }, { 136, 2 }, { 137, 2 }, { 138, 2 }, { 139, 2 }, { 140, 2 }, { 141, 2 }, { 142, 2 }, { 145, 2 },});
			
		//warmonger magic gfx
		// flying projectile
		attachments.put(59586, new int[][] { { 0, 3 }, { 6, 3 }, { 5, 3 }, { 10, 3 } });
		attachments.put(59585, new int[][] { { 0, 3 }, { 30, 3 }, { 29, 3 }, { 57, 3 }, {21, 3}, {55, 3}, {35, 3}, {5, 3}, {8, 3}, {38, 3}, {11, 3}, {40, 3}, {12, 3}, {42, 3}, {15, 3}, {45, 3}, {18, 3}, {48, 3} });
		attachments.put(59584, new int[][] { { 0, 3 }, { 30, 3 }, { 29, 3 }, { 57, 3 }, {21, 3}, {55, 3}, {35, 3}, {5, 3}, {8, 3}, {38, 3}, {11, 3}, {40, 3}, {12, 3}, {42, 3}, {15, 3}, {45, 3}, {18, 3}, {48, 3} });
	}
}