package com.soulplayps.client.node.animable.item;

public class ItemDefOsrs {

	public static ItemDef loadOsrs(int i) {
		ItemDef itemDef = null;

		switch (i) {
			case 30329:
				itemDef = ItemDef.forIDOsrs(2425, i);
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;
			case 30018:
				itemDef = ItemDef.forIDOsrs(13263, i);
				return itemDef;
		case 2677:
			itemDef = ItemDef.forIDOsrs(22001, i);
			return itemDef;
		case 2715:
			itemDef = ItemDef.forIDOsrs(20546, i);
			return itemDef;

		case 2819:
			itemDef = ItemDef.forIDOsrs(23046, i);
			return itemDef;
		case 13054:
			itemDef = ItemDef.forIDOsrs(20545, i);
			return itemDef;

		case 2739:
			itemDef = ItemDef.forIDOsrs(21526, i);
			return itemDef;
		case 13037:
			itemDef = ItemDef.forIDOsrs(20544, i);
			return itemDef;

		case 19043:
			itemDef = ItemDef.forIDOsrs(21524, i);
			return itemDef;
		case 19039:
			itemDef = ItemDef.forIDOsrs(20543, i);
			return itemDef;

		case 19064:
			itemDef = ItemDef.forIDOsrs(19835, i);
			return itemDef;
		case 13077:
			itemDef = ItemDef.forIDOsrs(19836, i);
			return itemDef;

		case 30421: // skeleton lantern note
			return itemDef;
		
		case 30420: // skeleton lantern
			itemDef = ItemDef.forIDOsrs(24327, i);
			itemDef.womanwearyoff = -7;
			itemDef.womanwearxoff = 3;
			return itemDef;
		
		case 30419: // pumpkin lantern note
			return itemDef;
		
		case 30418: // pumpkin lantern
			itemDef = ItemDef.forIDOsrs(24325, i);
			itemDef.womanwearyoff = -7;
			itemDef.womanwearzoff = 3;
			return itemDef;
		
		case 30417: // spookier boots note
			return itemDef;
		
		case 30416: // spookier boots
			itemDef = ItemDef.forIDOsrs(24323, i);
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			itemDef.manwearyoff = -4;
			return itemDef;

		case 30415: // spookier gloves note
			return itemDef;
		
		case 30414: // spookier gloves
			itemDef = ItemDef.forIDOsrs(24321, i);
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			itemDef.manwearyoff = -4;
			return itemDef;

		case 30413: // spookier skirt note
			return itemDef;
			
		case 30412: // spookier skirt
			itemDef = ItemDef.forIDOsrs(24319, i);
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			return itemDef;

		case 30411: // spookier robe note
			return itemDef;
		
		case 30410: // spookier robe
			itemDef = ItemDef.forIDOsrs(24317, i);
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			return itemDef;

		case 30409: // spookier hood note
			return itemDef;
		
		case 30408: // spookier hood
			itemDef = ItemDef.forIDOsrs(24315, i);
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			itemDef.manwearyoff = -4;
			return itemDef;

		case 30407: // spooky boots note
			return itemDef;
		
		case 30406: // spooky boots
			itemDef = ItemDef.forIDOsrs(24313, i);
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			itemDef.manwearyoff = -4;
			return itemDef;

		case 30405: // spooky gloves note
			return itemDef;
		
		case 30404: // spooky gloves
			itemDef = ItemDef.forIDOsrs(24311, i);
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			itemDef.manwearyoff = -4;
			return itemDef;

		case 30403: // spooky skirt note
			return itemDef;
			
		case 30402: // spooky skirt
			itemDef = ItemDef.forIDOsrs(24309, i);
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			return itemDef;

		case 30401: // spooky robe note
			return itemDef;

		case 30400: // spooky robe
			itemDef = ItemDef.forIDOsrs(24307, i);
			itemDef.manwearyoff = -4;
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			return itemDef;
			
		case 30399: // spooky hood note
			return itemDef;

		case 30398: // spooky hood
			itemDef = ItemDef.forIDOsrs(24305, i);
			itemDef.womanwearyoff = -3;
			itemDef.womanwearzoff = 3;
			itemDef.manwearyoff = -4;
			return itemDef;
		
		case 30397: // infernal harpoon animation model
			itemDef = ItemDef.forIDOsrs(3971, i);
			itemDef.womanwearyoff = -10;
			itemDef.womanwearxoff = 5;
			itemDef.manwearyoff = 10;
			return itemDef;
			
		case 30396: // dragon harpoon animation model
			itemDef = ItemDef.forIDOsrs(3969, i);
			itemDef.womanwearyoff = -10;
			itemDef.womanwearxoff = 0;
			return itemDef;
		
		case 30395: // Thammaron's sceptre charged
			itemDef = ItemDef.forIDOsrs(22555, i);
			itemDef.manwearxoff = -3;
			itemDef.manwearyoff = 9;
			return itemDef;
			
		case 30394: // Thammaron's sceptre (u) noted
			itemDef = ItemDef.forIDOsrs(22553, i);
			itemDef.noteId = 30395;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
			
		case 30393: // Thammaron's sceptre (u)
			itemDef = ItemDef.forIDOsrs(22552, i);
			itemDef.manwearxoff = -3;
			itemDef.manwearyoff = 9;
			return itemDef;
		
		case 30392: // Viggora's chainmace charged
			itemDef = ItemDef.forIDOsrs(22545, i);
			itemDef.manwearxoff = -3;
			itemDef.manwearyoff = 9;
			return itemDef;
			
		case 30391: // Viggora's chainmace (u) noted
			itemDef = ItemDef.forIDOsrs(22544, i);
			itemDef.noteId = 30390;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
			
		case 30390: // Viggora's chainmace (u)
			itemDef = ItemDef.forIDOsrs(22542, i);
			itemDef.manwearxoff = -3;
			itemDef.manwearyoff = 9;
			return itemDef;
		
		case 30389: //larran's key
			return ItemDef.forIDOsrs(23494, i);
		case 30388: //larran's key
			return ItemDef.forIDOsrs(23493, i);
		case 30387: //larran's key
			return ItemDef.forIDOsrs(22329, i);
		case 30386: //larran's key
			return ItemDef.forIDOsrs(23492, i);
		case 30385: //larran's key
			itemDef = ItemDef.forIDOsrs(23490, i);
			itemDef.stackIDs[0] = 30386;
			itemDef.stackIDs[1] = 30387;
			itemDef.stackIDs[2] = 30388;
			itemDef.stackIDs[3] = 30389;
			return itemDef;
		
		case 30384: // craw's bow charged
			itemDef = ItemDef.forIDOsrs(22550, i);
			itemDef.manwearxoff = -3;
			itemDef.manwearyoff = 9;
			return itemDef;
			
		case 30383: // craw's bow (u) noted
			itemDef = ItemDef.forIDOsrs(22548, i);
			itemDef.noteId = 30382;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
			
			
		case 30382: // craw's bow (u)
			itemDef = ItemDef.forIDOsrs(22547, i);
			itemDef.manwearxoff = -3;
			itemDef.manwearyoff = 9;
			return itemDef;
		
		case 30381: // black and white phat
			itemDef = ItemDef.forIDOsrs(11863, i);
			itemDef.name = "Checkered Partyhat";
			itemDef.recolorModified = new int[] {127, 0, 127, 127, 127, 0, 127, 0, 127, 0, 0 };
			itemDef.recolorOriginal = new int[] { 6067, 947, 55217, 55196, 55186, 43955, 38835, 27571, 17331, 11187, 11185 };
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			itemDef.manwearyoff = -7;
			itemDef.manwearzoff = 4;
			return itemDef;
			
		case 30380:// Flash scroll
			itemDef = ItemDef.forIDOsrs(607, i);
			itemDef.recolorModified[0] = 374700;
			itemDef.recolorOriginal[0] = 10351;
			itemDef.name = "Flash scroll";
			itemDef.stackable = true;
			itemDef.inventoryOptions = new String[]{"Activate", null, null, null, "Drop"};
			return itemDef;
		
		case 30379:// speed scroll
			itemDef = ItemDef.forIDOsrs(607, i);
			itemDef.recolorModified[0] = 374770;
			itemDef.recolorOriginal[0] = 10351;
			itemDef.name = "Speed scroll";
			itemDef.stackable = true;
			itemDef.inventoryOptions = new String[]{"Activate", null, null, null, "Drop"};
			return itemDef;
		
		case 30378:// wild kill count reset placeholder
			itemDef = ItemDef.forIDOsrs(619, i);
			itemDef.name = "Reset Wild killcount";
			itemDef.inventoryOptions = new String[]{"Activate", null, null, null, "Drop"};
			return itemDef;
			
		case 30377: //zuk pet
			return ItemDef.forIDOsrs(22319, i);
		case 30376: //Great Olm pet Olmlet
			return ItemDef.forIDOsrs(20851, i);
		case 30375: //pet kraken
			return ItemDef.forIDOsrs(12655, i);
		case 30374: //scorpia's offspring
			return ItemDef.forIDOsrs(13181, i);
		case 30373: //callisto cub
			return ItemDef.forIDOsrs(13178, i);
		case 30372: //venenatis spiderling
			return ItemDef.forIDOsrs(13177, i);
		case 30371: //vet'ion jr recolor
			return ItemDef.forIDOsrs(13180, i);
		case 30370: //vet'ion jr
			return ItemDef.forIDOsrs(13179, i);
			
		case 30369: // vial of blood
			return ItemDef.forIDOsrs(22446, i);
			
		case 30265: // elder chaos robe bottom
			itemDef = ItemDef.forIDOsrs(20520, i);
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			return itemDef;
			
		case 30264: // elder chaos robe top
			itemDef = ItemDef.forIDOsrs(20517, i);
			itemDef.manwearyoff = -3;
			itemDef.womanwearyoff = -10;
			itemDef.womanwearzoff = 4;
			return itemDef;
			
			
			
		case 30003:
			itemDef = ItemDef.forIDOsrs(12931, i);
			itemDef.womanwearyoff = -4;
			return itemDef;
			
		case 30002: //serpentine helm uncharged
			itemDef = ItemDef.forIDOsrs(12929, i);
			return itemDef;
		}
		
		return itemDef;
	}
	
}
