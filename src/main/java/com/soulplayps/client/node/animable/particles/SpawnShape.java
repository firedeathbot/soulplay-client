package com.soulplayps.client.node.animable.particles;

import java.util.Random;

public interface SpawnShape {
	
	ParticleVector divide(Random random);
}
