package com.soulplayps.client.node.animable;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.unknown.Class36;

public final class Animation {

	public static int newAnimationsLength = 100;
	public static int anim667Length = 0;
	public static int animOsrsLength = 0;
	
	public static final int NEW_OSRS_ID_START = 20000;
	
	public static void unpackConfig(RSArchive streamLoader) {
		RSBuffer stream = new RSBuffer(streamLoader.readFile("seq.dat"));
//		Stream stream = new Stream(FileOperations.ReadFile(SignLink.findcachedir()+ "688seq.dat"));
		int length = anim667Length = stream.readUnsignedWord();
		System.out.println("667 Animation Amount: " + length);
		
		RSBuffer stream155 = new RSBuffer(streamLoader.readFile("seqosrs.dat"));
		int length155 = animOsrsLength = stream155.readUnsignedWord();
		System.out.println("OSRS 182 Animation Amount: " + length155);
		
		if (anims == null)
			anims = new Animation[NEW_OSRS_ID_START+length155/*length + newAnimationsLength*/];
//		test = new int[length+newAnimationsLength];
		int count = 0;
		for (int j = 0; j < length; j++) {
			if (anims[j] == null)
				anims[j] = new Animation();
			anims[j].readValues667(stream, j);
//			test[j] = anims[j].frameCount;
			
//			if (j >= 18 && j <= 21) {
//				System.out.println("index:"+j);
//				System.out.println("frameCount: "+ anims[j].frameCount);
//				System.out.print("frameIds2: ");
//				for (int kk = 0; kk < anims[j].frame2Ids.length; kk++)
//					System.out.print(" 0x"+Integer.toHexString(anims[j].frame2Ids[kk])+" ,");
//				System.out.println("");
//				System.out.print("frameLengths: ");
//				for (int kk = 0; kk < anims[j].frameLengths.length; kk++)
//					System.out.print(" "+anims[j].frameLengths[kk]+" ,");
//				System.out.println("");
//				System.out.println("frameStep: "+anims[j].frameStep);
//				System.out.println("flowControl: "+anims[j].flowControl);
//				System.out.println("aBoolean358: "+anims[j].aBoolean358);
//				System.out.println("anInt359: "+anims[j].anInt359);
//				System.out.println("anInt360: "+anims[j].anInt360);
//				System.out.println("anInt361: "+anims[j].anInt361);
//				System.out.println("anInt362: "+anims[j].anInt362);
//				System.out.println("anInt363: "+anims[j].anInt363);
//				System.out.println("anInt364: "+anims[j].anInt364);
//				System.out.println("anInt365: "+anims[j].anInt365);
//			}
			
			count++;
			
//			if(j == 5870) {
//				System.out.println("anInt359: "+anims[j].anInt359);
//				System.out.println("anInt360: "+anims[j].anInt360);
//				System.out.println("anInt361: "+anims[j].anInt361);
//				System.out.println("anInt362: "+anims[j].anInt362);
//				System.out.println("anInt363: "+anims[j].anInt363);
//				System.out.println("anInt364: "+anims[j].anInt364);
//			}
			
			//anims[j].id = 14790;
			//anims[j].fileId = 3353;
		//	if(j == 14790) {
		//	anims[j].anInt352 = 23;
		//	anims[j].anIntArray355 = new int[]{3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3};
		//	anims[j].anIntArray353 = new int[]{219742351, 219742340, 219742332, 219742326, 219742328, 219742345, 219742324, 219742352, 219742343, 219742333, 219742331, 219742342, 219742350, 219742327, 219742329, 219742321, 219742316, 219742319, 219742337, 219742344, 219742336, 219742353, 219742339};
		//	}

		}
		//System.out.println("Count:"+count);
		//System.out.println(anims[5070].toString());
		
//		dumpValues(anim667Length);
		
//		
//		try {
//			encodeValues();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		testConfig();
		loadOsrsAnims(stream155, length155);
		
		editOsrsAnims();
		hardCodedAnims();
	}

	public static final int OSRS_ANIM_FILE_START = 5000;
	public static void loadOsrsAnims(RSBuffer stream, int length) {
		
		int newIndex = NEW_OSRS_ID_START;
		for (int j = 0; j < length; j++) {
			int newId = newIndex+j;
			if (anims[newId] == null)
				anims[newId] = new Animation();
			anims[newId].readValuesOSRS(stream, newId);
			if (anims[newId].frame2Ids != null && anims[newId].frame2Ids.length > 0) {
				for (int i = 0; i < anims[newId].frame2Ids.length; i++) {
					int currentFile = anims[newId].frame2Ids[i];
					int frame = currentFile & 0xFFFF;
					int newfile = (currentFile>>16)+OSRS_ANIM_FILE_START;
					anims[newId].frame2Ids[i] = (newfile << 16) | frame;
				}
			}
		}
	}
	
	public static int getOsrsAnimId(int id) {
		return NEW_OSRS_ID_START + id;
	}
	
	public static void editOsrsAnims() {
		// dragon pickaxe (or) animation
		anims[getOsrsAnimId(7139)].shieldDisplayed = -1;
		anims[getOsrsAnimId(7139)].weaponDisplayed = 30566;
		
		//dragon harpoon
		anims[getOsrsAnimId(7401)].weaponDisplayed = 30908;
		anims[getOsrsAnimId(7401)].shieldDisplayed = -1;
		//infernal harpoon
		anims[getOsrsAnimId(7402)].weaponDisplayed = 30909;
		anims[getOsrsAnimId(7402)].shieldDisplayed = -1;

		boolean[] newData = anims[426].flowControl;

		anims[getOsrsAnimId(8145)].flowControl = newData;
		anims[getOsrsAnimId(1661)].flowControl = newData;
		anims[getOsrsAnimId(1660)].flowControl = newData;
		anims[getOsrsAnimId(808)].flowControl = newData;
		anims[getOsrsAnimId(426)].flowControl = newData;
		anims[getOsrsAnimId(5061)].flowControl = newData;
	}
	
//	public static int[] test;
//	
//	public static Animation anims2[];
//	
//	public static void testConfig() {
////		Stream stream = new Stream(streamLoader.getDataForName("seq.dat"));
//		Stream stream = new Stream(FileOperations.ReadFile(SignLink.findcachedir()+ "688seq.dat"));
//		int length = stream.readUnsignedWord();
//		System.out.println("667 Animation Amount: " + length);
//		if (anims2 == null)
//			anims2 = new Animation[length + newAnimationsLength/*+6000*/];
//		int count = 0;
//		for (int j = 0; j < length; j++) {
//			if (anims2[j] == null)
//				anims2[j] = new Animation();
//			anims2[j].animationIndex = j;
//			anims2[j].readValues667(stream);
//			
//			
//			if (j >= 18 && j <= 21) {
//				System.out.println("index:"+j);
//				System.out.println("frameCount: "+ anims2[j].frameCount);
//				System.out.print("frameIds2: ");
//				for (int kk = 0; kk < anims2[j].frame2Ids.length; kk++)
//					System.out.print(" 0x"+Integer.toHexString(anims2[j].frame2Ids[kk])+" ,");
//				System.out.println("");
//				System.out.print("frameLengths: ");
//				for (int kk = 0; kk < anims2[j].frameLengths.length; kk++)
//					System.out.print(" "+anims2[j].frameLengths[kk]+" ,");
//				System.out.println("");
//				System.out.println("frameStep: "+anims2[j].frameStep);
//				System.out.println("flowControl: "+anims2[j].flowControl);
//				System.out.println("aBoolean358: "+anims2[j].aBoolean358);
//				System.out.println("anInt359: "+anims2[j].anInt359);
//				System.out.println("anInt360: "+anims2[j].anInt360);
//				System.out.println("anInt361: "+anims2[j].anInt361);
//				System.out.println("anInt362: "+anims2[j].anInt362);
//				System.out.println("anInt363: "+anims2[j].anInt363);
//				System.out.println("anInt364: "+anims2[j].anInt364);
//				System.out.println("anInt365: "+anims2[j].anInt365);
//			}
//			
//			if (anims2[j].frameCount != test[j]) {
//				System.out.println("broke at index:"+j);
//				break;
//			}
//			
//			count++;
//			
//
//		}
//	}

	public static void hardCodedAnims() {
		anims[6648].anInt365 = 1;
		anims[13735].anInt365 = 1; //edimmu npc
		anims[13270].anInt365 = 2; //sagittare npc

		// Seasonal stuff
		sliceAnimation(17214, 14223, 17); // Fire spell
		sliceAnimation(17215, 14222, 20); // Earth spell
		sliceAnimation(17216, 13190, 0); // Transform
		anims[17216].loop = 1;
		System.out.println(anims[17216].resetCycle);
		sliceAnimation(17217, 1979, 4); // Necromancer summon
		anims[17217].anInt359 = 10;
		sliceAnimation(17218, getOsrsAnimId(5612), 30); // Tarn appear
		anims[17218].anInt359 = 10;
	}

	private static void sliceAnimation(int newId, int oldId, int offset) {
		Animation newAnimation = anims[newId] = new Animation();
		newAnimation.copy(anims[oldId]);
		newAnimation.frame1Ids = getSliceOfArray(newAnimation.frame1Ids, offset, newAnimation.frameCount);
		newAnimation.frame2Ids = getSliceOfArray(newAnimation.frame2Ids, offset, newAnimation.frameCount);
		newAnimation.frameLengths = getSliceOfArray(newAnimation.frameLengths, offset, newAnimation.frameCount);
		newAnimation.frameCount -= offset;
	}

	public static int[] getSliceOfArray(int[] arr, int start, int end) {
		int[] slice = new int[end - start];

		for (int i = 0; i < slice.length; i++) {
			slice[i] = arr[start + i];
		}

		return slice;
	}

	public int method258(int i) { // getFrameLength
		if (i >= frameCount) {
			return 1;
		}

		int j = frameLengths[i];
		if (j == 0) {
			Class36 class36 = Class36.method531(frame2Ids[i]);
			if (class36 != null)
				j = frameLengths[i] = class36.anInt636;
		}
		if (j == 0)
			j = 1;
		return j;
	}

	public void readValues(RSBuffer stream) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				break;
			if (i == 1) {
				frameCount = stream.readUnsignedWord();
				frame2Ids = new int[frameCount];
				frame1Ids = new int[frameCount];
				frameLengths = new int[frameCount];
				for (int j = 0; j < frameCount; j++) {
					frameLengths[j] = stream.readUnsignedWord();
					frame1Ids[j] = -1;
				}
				for (int j = 0; j < frameCount; j++)
					frame2Ids[j] = stream.readUnsignedWord();
				for (int i1 = 0; i1 < frameCount; i1++) {
					frame2Ids[i1] = (stream.readUnsignedWord() << 16)
							+ frame2Ids[i1];
				}
			} else if (i == 2)
				loop = stream.readUnsignedWord();
			else if (i == 3) {
				flowControl = new boolean[256];

				int k = stream.readUnsignedByte();
				for (int l = 0; l < k; l++) {
					flowControl[stream.readUnsignedByte()] = true;
				}
			} else if (i == 4)
				aBoolean358 = true;
			else if (i == 5)
				anInt359 = stream.readUnsignedByte();
			else if (i == 6)
				shieldDisplayed = stream.readUnsignedWord();
			else if (i == 7)
				weaponDisplayed = stream.readUnsignedWord();
			else if (i == 8)
				resetCycle = stream.readUnsignedByte();
			else if (i == 9)
				anInt363 = stream.readUnsignedByte();
			else if (i == 10)
				anInt364 = stream.readUnsignedByte();
			else if (i == 11)
				anInt365 = stream.readUnsignedByte();
			else if (i == 12)
				stream.readDWord();
			else
				System.out.println("Error unrecognised seq config code: " + i);
		} while (true);
		if (frameCount == 0) {
			frameCount = 1;
			frame2Ids = new int[1];
			frame2Ids[0] = -1;
			frame1Ids = new int[1];
			frame1Ids[0] = -1;
			frameLengths = new int[1];
			frameLengths[0] = -1;
		}
		if (anInt363 == -1)
			if (flowControl != null)
				anInt363 = 2;
			else
				anInt363 = 0;
		if (anInt364 == -1) {
			if (flowControl != null) {
				anInt364 = 2;
				return;
			}
			anInt364 = 0;
		}
	}
	
	public void readValues602(RSBuffer stream)
	{
		do {
			int i = stream.readUnsignedByte();
			if(i == 0)
				break;
			if(i == 1) {
				frameCount = stream.readUnsignedWord();
				frame2Ids = new int[frameCount];
				frame1Ids = new int[frameCount];
				frameLengths = new int[frameCount];
				for(int i_ = 0; i_ < frameCount; i_++){
					frame2Ids[i_] = stream.readDWord();
					frame1Ids[i_] = -1;
				}
				for(int i_ = 0; i_ < frameCount; i_++)
					frameLengths[i_] = stream.readUnsignedByte();
			}
			else if(i == 2)
				loop = stream.readUnsignedWord();
			else if(i == 3) {
				flowControl = new boolean[256];

				int k = stream.readUnsignedByte();
				for (int l = 0; l < k; l++) {
					flowControl[stream.readUnsignedByte()] = true;
				}
			}
			else if(i == 4)
				aBoolean358 = true;
			else if(i == 5)
				anInt359 = stream.readUnsignedByte();
			else if(i == 6)
				shieldDisplayed = stream.readUnsignedWord();
			else if(i == 7)
				weaponDisplayed = stream.readUnsignedWord();
			else if(i == 8)
				resetCycle = stream.readUnsignedByte();
			else if(i == 9)
				anInt363 = stream.readUnsignedByte();
			else if(i == 10)
				anInt364 = stream.readUnsignedByte();
			else if(i == 11)
				anInt365 = stream.readUnsignedByte();
			else 
				System.out.println("Unrecognized seq.dat config code: "+i);
		} while(true);
		if(frameCount == 0)
		{
			frameCount = 1;
			frame2Ids = new int[1];
			frame2Ids[0] = -1;
			frame1Ids = new int[1];
			frame1Ids[0] = -1;
			frameLengths = new int[1];
			frameLengths[0] = -1;
		}
		if(anInt363 == -1)
			if(flowControl != null)
				anInt363 = 2;
			else
				anInt363 = 0;
		if(anInt364 == -1)
		{
			if(flowControl != null)
			{
				anInt364 = 2;
				return;
			}
			anInt364 = 0;
		}
	}
	
	public void readValues634(RSBuffer stream)
    {
        do {
            int i = stream.readUnsignedByte();
            if(i == 0)
                break;
            if(i == 1) {
                frameCount = stream.readUnsignedWord();
                frame2Ids = new int[frameCount];
                frame1Ids = new int[frameCount];
                frameLengths = new int[frameCount];
                for(int i_ = 0; i_ < frameCount; i_++){
                    frame2Ids[i_] = stream.readDWord();
                    frame1Ids[i_] = -1;
                }
                for(int i_ = 0; i_ < frameCount; i_++)
                    frameLengths[i_] = stream.readUnsignedByte();
            }
            else if(i == 2)
                loop = stream.readUnsignedWord();
            else if(i == 3) {
				flowControl = new boolean[256];

				int k = stream.readUnsignedByte();
				for (int l = 0; l < k; l++) {
					flowControl[stream.readUnsignedByte()] = true;
				}
            }
            else if(i == 4)
                aBoolean358 = true;
            else if(i == 5)
                anInt359 = stream.readUnsignedByte();
            else if(i == 6)
                shieldDisplayed = stream.readUnsignedWord();
            else if(i == 7)
                weaponDisplayed = stream.readUnsignedWord();
            else if(i == 8)
                resetCycle = stream.readUnsignedByte();
            else if(i == 9)
                anInt363 = stream.readUnsignedByte();
            else if(i == 10)
                anInt364 = stream.readUnsignedByte();
            else if(i == 11)
                anInt365 = stream.readUnsignedByte();
            else
                System.out.println("Unrecognized seq.dat config code: "+i);
        } while(true);
        if(frameCount == 0)
        {
            frameCount = 1;
            frame2Ids = new int[1];
            frame2Ids[0] = -1;
            frame1Ids = new int[1];
            frame1Ids[0] = -1;
            frameLengths = new int[1];
            frameLengths[0] = -1;
        }
        if(anInt363 == -1)
            if(flowControl != null)
                anInt363 = 2;
            else
                anInt363 = 0;
        if(anInt364 == -1)
        {
            if(flowControl != null)
            {
                anInt364 = 2;
                return;
            }
            anInt364 = 0;
        }
    }
	
	private void readValues667(RSBuffer stream, int animationIndex) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				break;
			if (i == 1) {
				frameCount = stream.readUnsignedWord();
				frame2Ids = new int[frameCount];
				frame1Ids = new int[frameCount];
				frameLengths = new int[frameCount];
				for (int j = 0; j < frameCount; j++) {
					frame2Ids[j] = stream.readDWord();
					frame1Ids[j] = -1;
				}
					for (int j = 0; j < frameCount; j++)
						frameLengths[j] = stream.readUnsignedByte();
				
			} else if (i == 2)
				loop = stream.readUnsignedWord();
			else if (i == 3) {
				flowControl = new boolean[256];

				int k = stream.readUnsignedByte();
				for (int l = 0; l < k; l++) {
					flowControl[stream.readUnsignedByte()] = true;
				}
			} else if (i == 4)
				aBoolean358 = true;
			else if (i == 5)
				anInt359 = stream.readUnsignedByte();
			else if (i == 6)
				shieldDisplayed = stream.readUnsignedWord();
			else if (i == 7)
				weaponDisplayed = stream.readUnsignedWord();
			else if (i == 8)
				resetCycle = stream.readUnsignedByte();
			else if (i == 9)
				anInt363 = stream.readUnsignedByte();
			else if (i == 10)
				anInt364 = stream.readUnsignedByte();
			else if (i == 11)
				anInt365 = stream.readUnsignedByte();
			else if (i == 12)
				stream.readDWord();
			else
				if (animationIndex < 200)
				System.out.println("Error unrecognised seq config code: " + i + " anim:"+animationIndex);
		} while (true);
		if (frameCount == 0) {
			frameCount = 1;
			frame2Ids = new int[1];
			frame2Ids[0] = -1;
			frame1Ids = new int[1];
			frame1Ids[0] = -1;
			frameLengths = new int[1];
			frameLengths[0] = -1;
		}
		if (anInt363 == -1)
			if (flowControl != null)
				anInt363 = 2;
			else
				anInt363 = 0;
		if (anInt364 == -1) {
			if (flowControl != null) {
				anInt364 = 2;
				return;
			}
			anInt364 = 0;
		}
	}
	
	private void readValuesOSRS(RSBuffer stream, int animationIndex) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				break;
			if (i == 1) {
				frameCount = stream.readUnsignedWord();
				frame2Ids = new int[frameCount];
				frame1Ids = new int[frameCount];
				frameLengths = new int[frameCount];

				for (int j = 0; j < frameCount; j++)
					frameLengths[j] = stream.readUnsignedWord();
				
				for (int j = 0; j < frameCount; j++) {
					frame2Ids[j] = stream.readDWord();
				}
				
			} else if (i == 2)
				loop = stream.readUnsignedWord();
			else if (i == 3) {
				flowControl = new boolean[256];

				int k = stream.readUnsignedByte();
				for (int l = 0; l < k; l++) {
					flowControl[stream.readUnsignedByte()] = true;
				}
			} else if (i == 4)
				aBoolean358 = true;
			else if (i == 5)
				anInt359 = stream.readUnsignedByte();
			else if (i == 6)
				shieldDisplayed = stream.readUnsignedWord();
			else if (i == 7)
				weaponDisplayed = stream.readUnsignedWord();
			else if (i == 8)
				resetCycle = stream.readUnsignedByte();
			else if (i == 9)
				anInt363 = stream.readUnsignedByte();
			else if (i == 10)
				anInt364 = stream.readUnsignedByte();
			else if (i == 11)
				anInt365 = stream.readUnsignedByte();
			else if (i == 12) {
				int len = stream.readUnsignedByte();
				
				for (int k = 0; k < len; k++) {
					stream.readUnsignedWord();
				}
				for (int k = 0; k < len; k++) {
					stream.readUnsignedWord();
				}
			} else if (i == 13) {
				int len = stream.readUnsignedByte();
				
				for (int k = 0; k < len; k++) {
					stream.read3Bytes();
				}
				
			} else
				if (animationIndex < 200)
					System.out.println("Error unrecognised seq config code: " + i + " anim:"+animationIndex);
		} while (true);
		if (frameCount == 0) {
			frameCount = 1;
			frame2Ids = new int[1];
			frame2Ids[0] = -1;
			frame1Ids = new int[1];
			frame1Ids[0] = -1;
			frameLengths = new int[1];
			frameLengths[0] = -1;
		}
		if (anInt363 == -1)
			if (flowControl != null)
				anInt363 = 2;
			else
				anInt363 = 0;
		if (anInt364 == -1) {
			if (flowControl != null) {
				anInt364 = 2;
				return;
			}
			anInt364 = 0;
		}
	}
	
	private static void encodeValues() throws IOException {
		DataOutputStream data = new DataOutputStream(new FileOutputStream("688seq.dat"));
        data.writeShort(anims.length);
		for (int i = 0; i < anims.length; i++) {
			Animation anim = anims[i];
			if (anim == null) {
				anim = new Animation();
			}
//			int i = stream.readUnsignedByte();
			if (anim.frame2Ids != null) {
				data.writeByte(1);//if (i == 1) {
				data.writeShort(anim.frameCount); //frameCount = stream.readUnsignedWord();
				for (int j = 0; j < anim.frameCount; j++) {
					writeHex(data, anim.frame2Ids[j]);//frame2Ids[j] = stream.readDWord();
					//frame1Ids[j] = -1;
				}
				for (int j = 0; j < anim.frameCount; j++)
					data.writeByte(anim.frameLengths[j]);//frameLengths[j] = stream.readUnsignedByte();
				
			}
			
			if (anim.loop != -1) {
				data.writeByte(2);//} else if (i == 2)
				data.writeShort(anim.loop); //frameStep = stream.readUnsignedWord();
			}
			if (anim.flowControl != null) {
				data.writeByte(3);//else if (i == 3) {
				int length = anim.flowControl.length-1;
				data.writeByte(length);//int k = stream.readUnsignedByte();
				//flowControl = new int[k + 1];
				//for (int l = 0; l < length; l++)
				//	data.writeByte(anim.flowControl[l]);//flowControl[l] = stream.readUnsignedByte();
				//flowControl[k] = 0x98967f;
			}
			if (anim.aBoolean358)
				data.writeByte(4);//} else if (i == 4)
//				aBoolean358 = true;
			if (anim.anInt359 != 5) { // default is 5
				data.writeByte(5);//else if (i == 5)
				data.writeByte(anim.anInt359);//anInt359 = stream.readUnsignedByte();
			}
			if (anim.shieldDisplayed != -1) {
				data.writeByte(6);//else if (i == 6)
				data.writeShort(anim.shieldDisplayed);//anInt360 = stream.readUnsignedWord();
			}
			if (anim.weaponDisplayed != -1) {
				data.writeByte(7);//else if (i == 7)
				data.writeShort(anim.weaponDisplayed);//anInt361 = stream.readUnsignedWord();
			}
			if (anim.resetCycle != 99) {
				data.writeByte(8);//else if (i == 8)
				data.writeByte(anim.resetCycle);//anInt362 = stream.readUnsignedByte();
			}
			if (anim.anInt363 != -1) {
				data.writeByte(9);//else if (i == 9)
				data.writeByte(anim.anInt363);//anInt363 = stream.readUnsignedByte();
			}
			if (anim.anInt364 != -1) {
				data.writeByte(10);//else if (i == 10)
				data.writeByte(anim.anInt364);//anInt364 = stream.readUnsignedByte();
			}
			if (anim.anInt365 != 2) {
				data.writeByte(11);//else if (i == 11)
				data.writeByte(anim.anInt365); //anInt365 = stream.readUnsignedByte();
			}
			data.writeByte(0);
		}
		data.close();
	}
		
	private static void writeShort2(DataOutputStream dos, int input) throws IOException {
		dos.writeByte((input >> 8) + 1);
		dos.writeByte(input);
	}

	private static void writeHex(DataOutputStream dos, int input) throws IOException {
		dos.writeByte(input >> 24);
		dos.writeByte(input >> 16);
		dos.writeByte(input >> 8);
		dos.writeByte(input);
	}

	private static void writeString(DataOutputStream dos, String input) throws IOException {
		dos.write(input.getBytes());
		dos.writeByte(10);
	}

	public Animation() {
		loop = -1;
		aBoolean358 = false;
		anInt359 = 5;
		shieldDisplayed = -1;
		weaponDisplayed = -1;
		resetCycle = 99;
		anInt363 = -1;
		anInt364 = -1;
		anInt365 = 2;
	}
	
	public static void dumpValues(int length) {
			System.out.println("Dumping Animations..");
//			String[] variablesNames = new String[] { "frameCount", "frameIDs", "frameIDs2", "delays", "loopDelay",
//					"animationFlowControl", "oneSquareAnimation", "forcedPriority", "leftHandItem", "rightHandItem",
//					"frameStep", "resetWhenWalk", "priority", "delayType" };
			File f = new File("animDefs667.txt");
			try {
				f.createNewFile();
				BufferedWriter bf = new BufferedWriter(new FileWriter(f));
				for (int j = 0; j < length; j++) {
					String frameCount = anims[j].frameCount + "";
					StringBuilder build = new StringBuilder();
					if (anims[j].frame2Ids != null) {
						build.append("new int[] { ");
						for (int kk = 0; kk < anims[j].frame2Ids.length; kk++) {
							build.append("0x"+Integer.toHexString(anims[j].frame2Ids[kk]));
							if (kk != anims[j].frame2Ids.length-1)
								build.append(", ");
						}
						build.append(" }");
					}
					String frame2Ids = build.toString(); // anims[j].anIntArray353 != null ? "new int[] "+Arrays.toString(anims[j].anIntArray353).replace("[", "{").replace("]", "}") : null;
					String frame1Ids = anims[j].frame1Ids != null ? "new int[] "+Arrays.toString(anims[j].frame1Ids).replace("[", "{ ").replace("]", " }") : null;
					String frameLengths = anims[j].frameLengths != null ? "new int[] "+Arrays.toString(anims[j].frameLengths).replace("[", "{ ").replace("]", " }") : null;
					String frameStep = anims[j].loop + "";
					String flowControl = anims[j].flowControl != null ? "new int[] "+ Arrays.toString(anims[j].flowControl).replace("[", "{ ").replace("]", " }") : null;
					String aBoolean358 = anims[j].aBoolean358 + "";
					String anInt359 = anims[j].anInt359 + "";
					String anInt360 = anims[j].shieldDisplayed + "";
					String anInt361 = anims[j].weaponDisplayed + "";
					String anInt362 = anims[j].resetCycle + "";
					String anInt363 = anims[j].anInt363 + "";
					String anInt364 = anims[j].anInt364 + "";
					String anInt365 = anims[j].anInt365 + "";

					String[] variables = new String[] { frameCount, frame2Ids, frame1Ids, frameLengths, frameStep,
							flowControl, aBoolean358, anInt359, anInt360, anInt361,
							anInt362, anInt363, anInt364, anInt365 };
					String[] variableNames = new String[] { "frameCount", "frame2Ids", "frame1Ids", "frameLengths", "frameStep",
							"flowControl", "aBoolean358", "anInt359", "anInt360", "anInt361",
							"anInt362", "anInt363", "anInt364", "anInt365" };
					bf.write("if (j == " + j + ") {\n");
					for (int k = 0; k < variables.length; k++) {
						bf.write("anims[" + j + "]." + variableNames[k] + " = " + variables[k] + ";\n");
					}
					bf.write("}\n\n");
				}
				bf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Dumping Complete!");
	}

	@Override
	public String toString() {
		return "Animation [frameCount=" + frameCount + ", frame2Ids=" + Arrays.toString(frame2Ids) + ", frame1Ids=" + Arrays.toString(frame1Ids) + ", frameLengths=" + Arrays.toString(frameLengths) + ", frameStep=" + loop + ", flowControl=" + Arrays.toString(flowControl) + ", aBoolean358=" + aBoolean358 + ", anInt359=" + anInt359 + ", anInt360=" + shieldDisplayed + ", anInt361=" + weaponDisplayed
				+ ", anInt362=" + resetCycle + ", anInt363=" + anInt363 + ", anInt364=" + anInt364 + ", anInt365=" + anInt365 + "]";
	}

	public void copy(Animation animation) {
		frameCount = animation.frameCount;
		frame2Ids = new int[frameCount];
		frame1Ids = new int[frameCount];
		frameLengths = new int[frameCount];

		for (int j = 0; j < frameCount; j++) {
			frameLengths[j] = animation.frameLengths[j];
			frame2Ids[j] = animation.frame2Ids[j];
			frame1Ids[j] = animation.frame1Ids[j];
		}

		loop = animation.loop;

		if (animation.flowControl != null) {
			flowControl = new boolean[256];
	
			for (int l = 0; l < 256; l++) {
				flowControl[l] = animation.flowControl[l];
			}
		}

		aBoolean358 = animation.aBoolean358;
		anInt359 = animation.anInt359;
		shieldDisplayed = animation.shieldDisplayed;
		weaponDisplayed = animation.weaponDisplayed;
		resetCycle = animation.resetCycle;
		anInt363 = animation.anInt363;
		anInt364 = animation.anInt364;
		anInt365 = animation.anInt365;
	}

	public static Animation anims[];
	public int frameCount;			 //frameCount	//anInt352
	public int frame2Ids[];  //frame2Ids		//anIntArray353
	public int frame1Ids[];  //frame1Ids		//anIntArray354
	public int[] frameLengths;  // frameLengths	//anIntArray355
	public int loop;   // frameStep				//anInt356
	public boolean[] flowControl; // flow control?    //anIntArray357
	public boolean aBoolean358;
	public int anInt359;			// priority
	public int shieldDisplayed;			// rightHandEquip
	public int weaponDisplayed;			// leftHandEquip
	public int resetCycle;
	public int anInt363;
	public int anInt364;
	public int anInt365;
	
}
