package com.soulplayps.client.node.object;

import java.io.*;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

import com.soulplayps.client.Client;
import com.soulplayps.client.ConfigCodes;
import com.soulplayps.client.node.ObjectCache;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.animable.model.ModelTypeEnum;
import com.soulplayps.client.node.ondemand.OnDemandFetcher;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.unpack.VarBit;
import com.soulplayps.client.unknown.*;

public final class ObjectDef {

//	private static final int[] showBlack = {26346,26347,26348,26358,26359,26360,26361,26362,26363,26364};

	//public boolean displayModelID;
	
//	public static ArrayList<Integer> objects = new ArrayList<Integer>();
	
	@Override
	public String toString() {
		return "ObjectDef [aBoolean736=" + aBoolean736 + ", aByte737=" + aByte737 + ", anInt738=" + anInt738 + ", name=" + name + ", anInt740=" + anInt740 + ", aByte742=" + aByte742 + ", anInt744=" + anInt744 + ", anInt745=" + anInt745 + ", anInt746=" + anInt746 + ", originalModelColors=" + Arrays.toString(originalModelColors) + ", anInt748=" + anInt748 + ", configID=" + configID
				+ ", aBoolean751=" + aBoolean751 + ", myId=" + myId + ", aBoolean757=" + aBoolean757 + ", anInt758=" + anInt758 + ", childrenIDs=" + Arrays.toString(childrenIDs) + ", anInt760=" + anInt760 + ", anInt761=" + anInt761 + ", aBoolean762=" + aBoolean762 + ", aBoolean764=" + aBoolean764 + ", aBoolean766=" + aBoolean766 + ", aBoolean767=" + aBoolean767 + ", anInt768=" + anInt768
				+ ", aBoolean769=" + aBoolean769 + ", anInt772=" + anInt772 + ", anIntArray773=" + Arrays.toString(anIntArray773) + ", anInt774=" + anInt774 + ", anInt775=" + anInt775 + ", anIntArray776=" + Arrays.toString(anIntArray776) + ", description=" + Arrays.toString(description) + ", hasActions=" + hasActions + ", aBoolean779=" + aBoolean779 + ", anInt781=" + anInt781 + ", anInt783="
				+ anInt783 + ", modifiedModelColors=" + Arrays.toString(modifiedModelColors) + ", actions=" + Arrays.toString(actions) + ", newModels="+ twoDArrayToString(newModels)+"]  clipType="+clipType;
	}
	
	public static String twoDArrayToString(int array[][]) {
		String string = "[";
		if (array != null)
		for (int i = 0; i < array.length; i++) {
			string += ".";
			for (int j = 0; j < array[i].length; j++) {
				string += array[i][j]+",";
			}
		}
		return string+"]";
	}

	private final static int[] hotSpotIDs = new int[] {13373, 13374, 13375, 13376, 13377, 13378, 39260, 39261, 39262, 39263, 39264, 39265, 2715, 13366, 13367, 13368, 13369, 13370, 13371, 13372, 15361, 15362, 15363, 15366, 15367, 15364, 15365, 15410, 15412, 15411, 15414, 15415, 15413, 15416, 15416, 15418, 15419, 15419, 15419, 15419, 15419, 15419, 15419, 15419, 15402, 15405, 15401, 15398, 15404, 15403, 15400, 15400, 15399, 15302, 15302, 15302, 15302, 15302, 15302, 15304, 15303, 15303, 15301, 15300, 15300, 15300, 15300, 15299, 15299, 15299, 15299, 15298, 15443, 15445, 15447, 15446, 15444, 15441, 15439, 15448, 15450, 15266, 15265, 15264, 15263, 15263, 15263, 15263, 15263, 15263, 15263, 15263, 15267, 15262, 15260, 15261, 15268, 15379, 15378, 15377, 15386, 15383, 15382, 15384, 15385, 34255, 15380, 15381, 15346, 15344, 15345, 15343, 15342, 15296, 15297, 15297, 15294, 15293, 15292, 15291, 15290, 15289, 15288, 15287, 15286, 15282, 15281, 15280, 15279, 15278, 15277, 15397, 15396, 15395, 15393, 15392, 15394, 15390, 15389, 15388, 15387, 44909, 44910, 44911, 44908, 15423, 15423, 15423, 15423, 15420, 48662, 15422, 15421, 15425, 15425, 15424, 18813, 18814, 18812, 18815, 18811, 18810, 15275, 15275, 15271, 15271, 15276, 15270, 15269, 13733, 13733, 13733, 13733, 13733, 13733, 15270, 15274, 15273, 15406, 15407, 15408, 15409, 15368, 15375, 15375, 15375, 15375, 15376, 15376, 15376, 15376, 15373, 15373, 15374, 15374, 15370, 15371, 15372, 15369, 15426, 15426, 15435, 15438, 15434, 15434, 15431, 15431, 15431, 15431, 15436, 15436, 15436, 15436, 15436, 15436, 15437, 15437, 15437, 15437, 15437, 15437, 15350, 15348, 15347, 15351, 15349, 15353, 15352, 15354, 15356, 15331, 15331, 15331, 15331, 15355, 15355, 15355, 15355, 15330, 15330, 15330, 15330, 15331, 15331, 15323, 15325, 15325, 15324, 15324, 15329, 15328, 15326, 15327, 15325, 15325, 15324, 15324, 15330, 15330, 15330, 15330, 15331, 15331, 34138, 15330, 15330, 34138, 34138, 15330, 34138, 15330, 15331, 15331, 15337, 15336, 39230, 39231, 36692, 39229, 36676, 34138, 15330, 15330, 34138, 34138, 15330, 34138, 15330, 15331, 15331, 36675, 36672, 36672, 36675, 36672, 36675, 36675, 36672, 15331, 15331, 15330, 15330, 15257, 15256, 15259, 15259, 15327, 15326};

	
	public static ObjectDef forID667(int i) {
//		System.out.println("667 defs"+i);
//		for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
//			System.out.println(ste);
//		}
		if (i >= streamIndices667.length) {
			return forID830(i);
		}

		ObjectDef objectDef = (ObjectDef) recentUse.get(i);

		if (objectDef != null) {
			return objectDef;
		}

		objectDef = new ObjectDef();
		stream667.currentOffset = streamIndices667[i];
		objectDef.myId = i;
		objectDef.setDefaults();
		objectDef.modelType = ModelTypeEnum.NEWEST;
		try {
			objectDef.readValuesUSA(stream667, false);
			recentUse.put(objectDef, i);
		} catch (Exception e) {
			e.printStackTrace();
		}

		switch (i) {
			case 38660:
				objectDef.description = new String("This is a size-9 star. A Mining level of at least 90 is required to mine this layer.").getBytes();
				break;
			case 38661:
				objectDef.description = new String("This is a size-8 star. A Mining level of at least 80 is required to mine this layer.").getBytes();
				break;
			case 38662:
				objectDef.description = new String("This is a size-7 star. A Mining level of at least 70 is required to mine this layer.").getBytes();
				break;
			case 38663:
				objectDef.description = new String("This is a size-6 star. A Mining level of at least 60 is required to mine this layer.").getBytes();
				break;
			case 38664:
				objectDef.description = new String("This is a size-5 star. A Mining level of at least 50 is required to mine this layer.").getBytes();
				break;
			case 38665:
				objectDef.description = new String("This is a size-4 star. A Mining level of at least 40 is required to mine this layer.").getBytes();
				break;
			case 38666:
				objectDef.description = new String("This is a size-3 star. A Mining level of at least 30 is required to mine this layer.").getBytes();
				break;
			case 38667:
				objectDef.description = new String("This is a size-2 star. A Mining level of at least 20 is required to mine this layer.").getBytes();
				break;
			case 38668:
				objectDef.description = new String("This is a size-1 star. A Mining level of at least 10 is required to mine this layer.").getBytes();
				break;
			case 54529:
			case 54527:
			case 54525:
			case 54531:
			case 54506:
			case 54508:
			case 54510:
			case 54504:
				objectDef.aBoolean767 = false;
				break;
			case 55220: // equipment table
				objectDef.actions[0] = "Open";
				break;
			case 38699:
				objectDef.newModels = new int[][] {{ 15138, 11233 }};
				objectDef.modelType = ModelTypeEnum.OSRS;
				break;
			case 38698:
			case 28213:
				objectDef.anIntArray773 = new int[] { 42293, 42291};
				objectDef.anIntArray776 = null; // this fixed the portals showing the insides. setting this to null
				break;
			case 28204:
				objectDef.setDefaults();
				break;
			case 28214:
				objectDef.anIntArray773 = new int[] { 30177, 11233 };
				objectDef.anIntArray776 = null;
				break;
		}

		if (Client.debugModels) {
			if (objectDef.name == null || objectDef.name.equalsIgnoreCase("null")) {
				objectDef.name = "test";
			}

			objectDef.hasActions = true;
		}

		return objectDef;
	}
	
	public static ObjectDef forID525(int i) {
//		System.out.println("525 defs");
//		if (!objects.contains(i)) {
//			System.out.print(", "+i);
//			objects.add(i);
//		}
		
		if (i >= streamIndices525.length)
			return forID667(i);
		
		ObjectDef objectDef;
//		synchronized(recentUse) {
			objectDef = (ObjectDef) recentUse.get(i);
//		}
		if (objectDef != null) {
			return objectDef;
		}
		
		objectDef = new ObjectDef();
		stream525.currentOffset = streamIndices525[i];
		objectDef.myId = i;
		objectDef.setDefaults();
		try {
		objectDef.readValues525(stream525);
		
//		synchronized(recentUse) {
			recentUse.put(objectDef, i);
//		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		if (Client.debugModels) {

			if (objectDef.name == null || objectDef.name.equalsIgnoreCase("null"))
				objectDef.name = "test";

			objectDef.hasActions = true;
		}
		
		return objectDef;
	}
	
	public static ObjectDef forID830(int i) {
//		System.out.println("830 defs");
		ObjectDef objectDef;
//		synchronized(recentUse) {
			objectDef = (ObjectDef) recentUse.get(i);
//		}
		if (objectDef != null) {
			return objectDef;
		}
		
		objectDef = new ObjectDef();
		stream830.currentOffset = streamIndices830[i];
		objectDef.myId = i;
		objectDef.setDefaults();
		try {
		objectDef.readValuesUSA(stream830, true);
//		objectDef.anInt781 = -1;
		if (objectDef.anInt781 > 0)
			System.out.println("objID:" + objectDef.myId + "   animationId:"+objectDef.anInt781);
//		if (objectDef.newModels != null)
//			for (int kk = 0; kk < objectDef.newModels.length; kk++) {
//				for (int jj = 0; jj < objectDef.newModels[kk].length; jj++) {
//					int kkk = objectDef.newModels[kk][jj];
//					if (!Model.temp.contains(kkk))
//						Model.temp.add(kkk);
//				}
//			}
//		synchronized(recentUse) {
			recentUse.put(objectDef, i);
//		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (Client.debugModels) {

			if (objectDef.name == null || objectDef.name.equalsIgnoreCase("null"))
				objectDef.name = "test";

			objectDef.hasActions = true;
		}
		
		return objectDef;
	}
	
	public static ObjectDef forIDOSRS(int i) {
		ObjectDef objectDef;
//		synchronized(recentUse) {
			objectDef = (ObjectDef) recentUse.get(i);
//		}
		if (objectDef != null) {
			return objectDef;
		}
		
		boolean isIn317Map = i > OSRS_START;
		
		int realId = i;
		
		if (isIn317Map)
			i -= OSRS_START;

		if (i >= streamIndicesOSRS.length) {
			return forID525(i);
		}
		
		objectDef = new ObjectDef();
		streamOSRS.currentOffset = streamIndicesOSRS[i];
		objectDef.myId = realId;
		objectDef.setDefaults();
		objectDef.modelType = ModelTypeEnum.OSRS;
		try {
		objectDef.readValuesOSRS(streamOSRS);
//		synchronized(recentUse) {
			recentUse.put(objectDef, realId);
//		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (objectDef.anIntArray776 != null && objectDef.anIntArray776[0] == 22) // raise up ground decorations
			objectDef.anInt745 -= 1;
		
		switch(i) {
			//Lms stuff
			case 29073:
			case 29074:
			case 29077:
			case 29078:
				objectDef.actions = new String[5];
				objectDef.actions[0] = "Open";
				break;
			//Tob stuff
			case 32719:
			case 32721:
			case 32722:
			case 32723:
			case 32724:
			case 32725:
			case 32726:
			case 32727:
			case 32728:
			case 32729:
			case 32730:
			case 32731:
			case 32733:
				objectDef.anInt745 -= 3;
				break;
			case 32734:
				objectDef.anInt745 -= 7;
				break;
		case 32687:
		case 32688:
		case 32689:
		//Inferno stuff
		case 30339:
		case 30340:
		case 30341:
		case 30342:
		case 30345:
		case 30346:
			objectDef.aBoolean769 = false;
			break;
		case 30352:
			objectDef.setDefaults();
			objectDef.anIntArray773= new int[] {33022};
			objectDef.anInt744 = 6;
			objectDef.anInt761 = 6;
			objectDef.actions[0] = "Jump-in";
			objectDef.name = "The Inferno";
			objectDef.hasActions = true;
			objectDef.modelType = ModelTypeEnum.OSRS;
			break;
		//End of inferno stuff
		case 12: // pumpkin tree
			objectDef.aBoolean736=false;
			objectDef.aByte737=0;
			objectDef.anInt738=0;
			objectDef.name="Tree";
			objectDef.anInt740=128;
			objectDef.aByte742=0;
			objectDef.anInt744=2;
			objectDef.anInt745=0;
			objectDef.anInt746=-1;
			objectDef.originalModelColors=new int[] {4960};
			objectDef.anInt748=128;
			objectDef.configID=-1;
			objectDef.aBoolean751=false;
			objectDef.aBoolean757=true;
			objectDef.anInt758=1;
			objectDef.childrenIDs=null;
			objectDef.anInt760=1;
			objectDef.anInt761=2;
			objectDef.aBoolean762=false;
			objectDef.aBoolean764=false;
			objectDef.aBoolean766=false;
			objectDef.aBoolean767=true;
			objectDef.anInt768=0;
			objectDef.aBoolean769=false;
			objectDef.anInt772=128;
			objectDef.anIntArray773= new int[] {1637};
			objectDef.anInt774=-1;
			objectDef.anInt775=16;
			objectDef.anIntArray776=null;
			objectDef.description=null;
			objectDef.hasActions=true;
			objectDef.aBoolean779=true;
			objectDef.anInt781=-1;
			objectDef.anInt783=0;
			objectDef.modifiedModelColors=new int[] {3470};
			objectDef.actions= new String[] { "Shake", null, null, null, null } ;
			objectDef.clipType=2;
			break;
			
		case 13: // pumpkin tree small
			objectDef.aBoolean736=false;
			objectDef.aByte737=0;
			objectDef.anInt738=0;
			objectDef.name="Tree";
			objectDef.anInt740=128;
			objectDef.aByte742=0;
			objectDef.anInt744=2;
			objectDef.anInt745=0;
			objectDef.anInt746=-1;
			objectDef.originalModelColors=new int[] {4960};
			objectDef.anInt748=128;
			objectDef.configID=-1;
			objectDef.aBoolean751=false;
			objectDef.aBoolean757=true;
			objectDef.anInt758=1;
			objectDef.childrenIDs=null;
			objectDef.anInt760=1;
			objectDef.anInt761=2;
			objectDef.aBoolean762=false;
			objectDef.aBoolean764=false;
			objectDef.aBoolean766=false;
			objectDef.aBoolean767=true;
			objectDef.anInt768=0;
			objectDef.aBoolean769=false;
			objectDef.anInt772=128;
			objectDef.anIntArray773= new int[] {1570};
			objectDef.anInt774=-1;
			objectDef.anInt775=16;
			objectDef.anIntArray776=null;
			objectDef.description=null;
			objectDef.hasActions=true;
			objectDef.aBoolean779=true;
			objectDef.anInt781=-1;
			objectDef.anInt783=0;
			objectDef.modifiedModelColors=new int[] {3470};
			objectDef.actions= new String[] { "Shake", null, null, null, null } ;
			objectDef.clipType=2;
			break;

		case 14209: //FOE statue
			objectDef.name = "Geoff";
			break;
		case 26826: //Fatality statue
			objectDef.name = "Kevin";
			break;
		case 26825: //Ancient Fury statue
			objectDef.name = "Chad";
			break;
		case 26827: //Hexis statue
			objectDef.name = "N";
			break;
		case 26828: //ROT statue
			objectDef.name = "Melo";
			break;
		case 26829: //SU statue
			objectDef.name = "Janae";
			break;
			
		// raids crystals and other deco
		case 30018:
		case 29783:
		case 29784:
		case 29787:
		case 29822:
		case 29823:
		case 29824:
		case 29879:
		case 30036:
		case 30037:
		case 30038:
		case 30039:
		case 30040:
		case 30041:
		case 30042:
		case 30043:
		case 30044:
		case 30045:
		case 29880: // great olm object
		case 29881: // great olm object
		case 29882: // great olm object
		case 29883: // great olm object
		case 29884: // great olm object
		case 29885: // great olm object
		case 29886: // great olm object
		case 29887: // great olm object
		case 29888: // great olm object this one still flickers :(
			objectDef.anInt745 -= 1;
			break;

		// DMM skull
		case 31502:
		case 31503:
		case 31504:
		case 31505:
		case 31506:
		case 31507:
		case 31508:
		case 31509:
		case 31510:
		case 31511:
		case 31512:
		case 31513:
		case 31514:
		case 31515:
		case 31516:
		case 31517:
		case 31518:
		case 31519:
		case 31520:
		case 31521:
		case 31522:
		case 31523:
		case 31524:
		case 31525:
		case 31526:
		case 31527:
		case 31528:
		case 31529:
		case 31530:
		case 31531:
		case 31532:
		case 31533:
		case 31534:
		case 31535:
		case 31536:
		case 31537:
		case 31538:
		case 31539:
		case 31540:
		case 31541:
		case 31542:
		case 31543:
		case 31544:
		case 31545:
		case 31546:
		case 31547:
		case 31548:
		case 31549:
		case 31550:
		case 31551:
			objectDef.anInt745 -= 1;
			break;
			
		case 32000: // vorkath puddles
		case 30032: // great olm puddles
			objectDef.anInt745 -= 4;
			break;
			
		}
		
		if (Client.debugModels) {

			if (objectDef.name == null || objectDef.name.equalsIgnoreCase("null"))
				objectDef.name = "test";

			objectDef.hasActions = true;
		}
		
		return objectDef;
	}
	
	public static ObjectDef forID(int i) {
		if (Client.inOsrsRegion || i == 14918)
			return forIDOSRS(i);
		
		if (i >= OSRS_START) {
			return forIDOSRS(i);
		}
		
		if (i == 38150) {
			return forIDOSRS(26492);
		}
		
		if ((i >= 11426 && i <= 11433) || (i >= 11391 && i <= 11395) || i == 12713 || i == 12714 || i == 12715 || i == 11922 || i == 11919 || i == 11916 || i == 11444 || i == 11441 || i == 11437 || i == 11435) {
			return forID667(i);
		}

		switch (i) {
			case 26186:
			case 26575:
			case 26576:
			case 20001:
			case 20000:
			case 26185:
				return forIDOSRS(i);
		}

		if (i == 29881 || i == 29887 || i == 29884) {//raids olm boss
			return forIDOSRS(i);
		}
		
		if (i == 31555 || i == 31556) {//revenant caves
			return forIDOSRS(i);
		}
		
		if (Client.objectDefsRegion == 667) {
			return forID667(i);
		}
			
		if (i >= streamIndices474.length)
			return forID667(i);//forID525(i);
		
//		if (i > streamIndices474.length)
//			i = streamIndices474.length - 1;
//		for (int index = 0; index < object667.length; index++) {
//			if(object667[index] == i)
//				return forID667(i);
//		}

		//System.out.println("loading objectID:"+i);
//		if (i == 4446 || i == 4447) {
//			return forID667(i);
//		}
		
		
//		if (i == 8553) {
//			return forID667(i);
//		}
		
		
//		if (!objects.contains(i)) {
//			System.out.println("Loading object "+i);
//			objects.add(i);
//		}
		
		ObjectDef objectDef;
//		synchronized(recentUse) {
			objectDef = (ObjectDef) recentUse.get(i);
//		}
		if (objectDef != null) {
			return objectDef;
		}

		objectDef = new ObjectDef();
		stream.currentOffset = streamIndices474[i];
		objectDef.myId = i;
		objectDef.setDefaults();
		objectDef.readValues474(stream);
		
		if (objectDef.anInt781 > 0) {
			switch (i) {
			case 24313:
			case 26814:
				//do not change animations
				break;
				
				default:
					objectDef.anInt781 += Animation.NEW_OSRS_ID_START;
					break;
			}
		}
		
		if (objectDef.anIntArray776 != null && objectDef.anIntArray776[0] == 22) // raise up ground decorations
			objectDef.anInt745 -= 1;
		
//		synchronized(recentUse) {
			recentUse.put(objectDef, i);
//		}
//		if(objectDef.anIntArray773 != null) {
//			ArrayList<Integer> test = new ArrayList<Integer>();
//			for (int j = 0; j < objectDef.anIntArray773.length; j++) {
////				System.out.print(" , "+objectDef.anIntArray773[j]);
//				if (objectDef.anIntArray773[j] > 10000) {
//					if (!test.contains(objectDef.anIntArray773[j])) {
//						test.add(objectDef.anIntArray773[j]);
//					}
//				}
//			}
//			
//			for (int kk = 0; kk < test.size(); kk++) {
//				if (test.get(kk) < lowest) {
//					lowest = test.get(kk);
//				}
//				if (test.get(kk) > highest) {
//					highest = test.get(kk);
//				}
////				System.out.print(" , "+test.get(kk));
//			}
//			if (lowest != 50000 && highest != 0)
//				System.out.println("lowest:"+lowest+"   highest:"+highest);
//			test = null;
//		}
		

//		if (i == 2732) {
//			objectDef.anInt781 = 17248;//16704;//917;
//			return objectDef;
//		}
		
//		if (objectDef.modifiedModelColors == null) {
//			objectDef.modifiedModelColors = new int[1];
//			objectDef.originalModelColors = new int[1];
//			objectDef.modifiedModelColors[0] = 0;
//			objectDef.originalModelColors[0] = 1;
//		}
		
//		if (i == 11614) { // barb banners
//			objectDef.anInt781 = -1;
//			return objectDef;
//		}
//		
//		if (i == 11606) { // barb torches
//			objectDef.anInt781 = 3927;
//			return objectDef;
//		}

		if (i == 11699 || i == 11698 || i == 10658) {
			ObjectDef.setZulrahObjects(i, objectDef);
			return objectDef;
		}
		
//		if (i == 4446 || i == 4447 || i == 4482) {
////			System.out.println("model is maybe "+objectDef.hasActions);
//			objectDef.hasActions = true;
////			if(objectDef.anIntArray773 != null) {
////				for (int j = 0; j < objectDef.anIntArray773.length; j++) {
////					System.out.println("model is maybe "+objectDef.anIntArray773[j]);
////				}
////			}
//		}
		
		/*for(int mm = 0; mm < showBlack.length; mm++) {
			if(i == showBlack[mm]) {
			class46.modifiedModelColors = new int[1];
			class46.originalModelColors = new int[1];
			class46.modifiedModelColors[0] = 0;
			class46.originalModelColors[0] = 1;
			}
		}*/
		
		/*if(class46.anIntArray773 != null) {
			for (int j = 0; j < class46.anIntArray773.length; j++) {
				//for(int k = 0; k < client.modelCollection.size(); k++) {
					if(client.modelCollection.contains(class46.anIntArray773[j])) {
						//System.out.println("model "+class46.anIntArray773[j]);
					} else {
						client.modelCollection.add(class46.anIntArray773[j]);
						//System.out.println("model "+class46.anIntArray773[j]);
					}
				//}
			}
		}*/
		
		if (i == 26420 || i == 26421 || i == 26422) { // saradomin godwars waterfall cheapfix
			//objectDef.aBoolean769 = false;
			/*class46.displayModelID = true;
			if(class46.anIntArray773 != null) {
				for (int j = 0; j < class46.anIntArray773.length; j++)
					System.out.println("model "+class46.anIntArray773[j]);
			
				//System.out.println("model "+class46.anIntArray773[0]);
				//System.out.println("model1 "+class46.anIntArray773[1]);
				//System.out.println("model2 "+class46.
				//System.out.println("model3 "+class46.
				//System.out.println("model4 "+class46.
				//System.out.println("model5 "+class46.
				//System.out.println("model6 "+class46.
			}*/
		}
		
		if (i == 11700) {
			objectDef.name = "Venom";
			//objectDef.isUnwalkable = false;
			//objectDef.isSolidObject = false;
			objectDef.anIntArray773 = new int[] { 4086 };
//			objectDef.animationID = 1261;
			objectDef.modifiedModelColors = new int[] { 10543 };
			objectDef.originalModelColors = new int[] { 31636 };
			objectDef.anInt781 = 1261;
			objectDef.description = "It's a cloud of venomous smoke that is extremely toxic.".getBytes();


			objectDef.anInt744 = 3;
			objectDef.anInt761 = 3;
			objectDef.aBoolean767 = false;
			objectDef.aBoolean762 = true;
			
			objectDef.anInt748 = 160;
			objectDef.anInt772 = 160;
			objectDef.anInt740 = 160;
			
			return objectDef;
		}
		
		if (i == 15477) {
			objectDef.actions = new String[] {"Enter", "Home", "Build mode", "Friend's house", "Popular houses"};
		}
		
		if (i == 4458 || i == 4459 || i == 4460 || i == 4461 || i == 4462 || i == 4463 || i == 4464) { // bandage table
			objectDef.actions[0] = "Take-1";
			objectDef.actions[1] = "Take-5";
			objectDef.actions[2] = "Take-10";
			return objectDef;
		}
		
		if (i == 7149 || i == 7147) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Squeeze-Through";
			objectDef.name = "Gap";
			return objectDef;
		}
		if (i == 7152 || i == 7144) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Distract";
			objectDef.name = "Eyes";
			return objectDef;
		}
		if (i == 2164) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Fix";
			objectDef.actions[1] = null;
			objectDef.name = "Trawler Net";
			return objectDef;
		}
		if (i == 1293) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Teleport";
			objectDef.actions[1] = null;
			objectDef.name = "Spirit Tree";
			return objectDef;
		}
		if (i == 7152 || i == 7144) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Burn Down";
			objectDef.name = "Boil";
			return objectDef;
		}
		if (i == 7152 || i == 7144) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Chop";
			objectDef.name = "Tendrils";
			return objectDef;
		}
		if (i == 2452) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Go Through";
			objectDef.name = "Passage";
			return objectDef;
		}
		if (i == 7153) {
			objectDef.hasActions = true;
			objectDef.actions = new String[5];
			objectDef.actions[0] = "Mine";
			objectDef.name = "Rock";
			return objectDef;
		}
		if (i == 2452 || i == 2455 || i == 2456 || i == 2454 || i == 2453
				|| i == 2461 || i == 2457 || i == 2461 || i == 2459
				|| i == 2460) {
			objectDef.hasActions = true;
			objectDef.name = "Mysterious Ruins";
			return objectDef;
		}
		
		
		for(int i1 : hotSpotIDs)
		{
			if(i1 == i)
			{
				objectDef.configID = ConfigCodes.CONSTRUCTION_MODE_1;
				objectDef.childrenIDs = new int[] {i, 0-1};
				return objectDef;
			}
		}

//		if (i == 38698 || i == 38699 || i == 28213) { // clan wars portals portal
//			objectDef.setDefaults();//objectDef.anIntArray773 = new int[] { 11231, 42293 };
//			objectDef.anIntArray773 = new int[] { 11231 };
//			return objectDef;//forID667(i);
//		}
		
		switch (i) {
		
		case 26972://bank stall
			objectDef.actions[2] = "Bank All";
			objectDef.actions[3] = "Open presets";
			break;
		
		case 6434:
			objectDef.anInt745 -= 1;
			break;
		case 6435:
			objectDef.anInt745 -= 5;
			break;
		
		//construction door hotspots
		case 15432: // throne room floor space
		case 15295: // combat room floor
		case 15430: // throne room floor space fremmy style
		case 15429: // throne room floor space desert style
        case 15428: // throne room floor space stone style
		case 15427: // throne room floor space basic wood style
		case 15305:
		case 15306:
		case 15307:
		case 15308:
		case 15309:
		case 15310:
		case 15311:
		case 15312:
		case 15313:
		case 15314:
		case 15315:
		case 15316:
		case 15317: // dungeon door hotspots
			objectDef.configID = ConfigCodes.CONSTRUCTION_MODE_1;
			objectDef.childrenIDs = new int[] {i, 0 -1};
			return objectDef;
		
		case 10638:
			objectDef.hasActions = true;
			return objectDef;
			
		case 19192:
			objectDef.actions[0] = "Rebuild";
			objectDef.actions[1] = "Dismantle";
			return objectDef;
		}
		
		if (Client.debugModels) {

			if (objectDef.name == null || objectDef.name.equalsIgnoreCase("null"))
				objectDef.name = "test";

			objectDef.hasActions = true;
		}
		
		return objectDef;
	}
	
	public static void searchForModel(int modelId) {
		
		for (int i = 0; i < totalObjects830; i++) {
			ObjectDef def = ObjectDef.forID830(i);
			if (def != null) {
				if (def.anIntArray773 != null) {
					for (int j = 0; j < def.anIntArray773.length; j++) {
						int model = def.anIntArray773[j];
						if (model == modelId) {
							System.out.println("Found 830 Object ID:"+ def.myId);
						}
					}
				}
				if (def.newModels != null) {
					for (int j = 0; j < def.newModels.length; j++) {
						for (int j1 = 0; j1 < def.newModels[j].length; j1++) {
							if (def.newModels[j][j1] == modelId) {
								System.out.println("2Found 830 Object ID:"+ def.myId);
							}
						}
					}
				}
			}
		}
		
		for (int i = 0; i < totalObjects667; i++) {
			ObjectDef def = ObjectDef.forID667(i);
			if (def != null) {
				if (def.anIntArray773 != null) {
					for (int j = 0; j < def.anIntArray773.length; j++) {
						int model = def.anIntArray773[j];
						if (model == modelId) {
							System.out.println("Found 667 Object ID:"+ def.myId);
						}
					}
				}
				if (def.newModels != null) {
					for (int j = 0; j < def.newModels.length; j++) {
						for (int j1 = 0; j1 < def.newModels[j].length; j1++) {
							if (def.newModels[j][j1] == modelId) {
								System.out.println("2Found 667 Object ID:"+ def.myId);
							}
						}
					}
				}
			}
		}
		
		for (int i = 0; i < totalObjectsOSRS; i++) {
			ObjectDef def = ObjectDef.forIDOSRS(i);
			if (def != null) {
				if (def.anIntArray773 != null) {
					for (int j = 0; j < def.anIntArray773.length; j++) {
						int model = def.anIntArray773[j];
						if (model == modelId) {
							System.out.println("Found OSRS Object ID:"+ def.myId);
						}
					}
				}
				if (def.newModels != null) {
					for (int j = 0; j < def.newModels.length; j++) {
						for (int j1 = 0; j1 < def.newModels[j].length; j1++) {
							if (def.newModels[j][j1] == modelId) {
								System.out.println("2Found OSRS Object ID:"+ def.myId);
							}
						}
					}
				}
			}
		}
		
		for (int i = 0; i < totalObjects474; i++) {
			ObjectDef def = ObjectDef.forID(i);
			if (def != null) {
				if (def.anIntArray773 != null) {
					for (int j = 0; j < def.anIntArray773.length; j++) {
						int model = def.anIntArray773[j];
						if (model == modelId) {
							System.out.println("Found 474 Object ID:"+ def.myId);
						}
					}
				}
				if (def.newModels != null) {
					for (int j = 0; j < def.newModels.length; j++) {
						for (int j1 = 0; j1 < def.newModels[j].length; j1++) {
							if (def.newModels[j][j1] == modelId) {
								System.out.println("2Found 474 Object ID:"+ def.myId);
							}
						}
					}
				}
			}
		}
	}

	public void setDefaults() {
		anIntArray773 = null; //model id i think?
		anIntArray776 = null;
		name = null;
		description = null;
		modifiedModelColors = null;
		originalModelColors = null;
		anInt744 = 1;
		anInt761 = 1;
		aBoolean767 = true;
		aBoolean757 = true;
		hasActions = false;
		aBoolean762 = false;
		aBoolean769 = false;
		aBoolean764 = false;
		anInt781 = -1;
		anInt775 = 16;
		aByte737 = 0;
		aByte742 = 0;
		actions = null;
		anInt746 = -1;
		anInt758 = -1;
		aBoolean751 = false;
		aBoolean779 = true;
		anInt748 = 128;
		anInt772 = 128;
		anInt740 = 128;
		anInt768 = 0;
		anInt738 = 0;
		anInt745 = 0;
		anInt783 = 0;
		aBoolean736 = false;
		aBoolean766 = false;
		anInt760 = -1;
		anInt774 = -1;
		configID = -1;
		childrenIDs = null;
		clipType = 2;
		actions = new String[5];
	}

	public void method574(OnDemandFetcher class42_sub1) {
		if (anIntArray773 == null && newModels == null)
			return;
		if (newModels != null) {
			for (int j = 0; j < newModels.length; j++)
				for (int j1 = 0; j1 < newModels[j].length; j1++)
					class42_sub1.method560(newModels[j][j1] & 0x3ffff, 0);
		} else {
			for (int j = 0; j < anIntArray773.length; j++)
				class42_sub1.method560(anIntArray773[j] & 0x3ffff, 0);
		}
	}

	public static int totalObjects474;
	public static int totalObjects667;
	public static int totalObjects830;
	public static int totalObjects525;
	public static int totalObjectsOSRS;

	public static void unpackConfig(RSArchive streamLoader) {
		stream = new RSBuffer(streamLoader.readFile("loc.dat"));
		RSBuffer stream = new RSBuffer(streamLoader.readFile("loc.idx"));
		
		stream830 = new RSBuffer(streamLoader.readFile("830loc.dat"));
		RSBuffer idxBuffer830 = new RSBuffer(streamLoader.readFile("830loc.idx"));
		
		stream667 = new RSBuffer(streamLoader.readFile("667loc.dat"));
		RSBuffer idxBuffer667 = new RSBuffer(streamLoader.readFile("667loc.idx"));
		
		stream525 = new RSBuffer(streamLoader.readFile("525loc.dat"));
		RSBuffer idxBuffer525 = new RSBuffer(streamLoader.readFile("525loc.idx"));
		
		streamOSRS = new RSBuffer(streamLoader.readFile("osrsloc.dat"));
		RSBuffer idxBufferOSRS = new RSBuffer(streamLoader.readFile("osrsloc.idx"));
		
		totalObjects474 = stream.readUnsignedWord();
		totalObjects667 = idxBuffer667.readDWord();
		totalObjects830 = idxBuffer830.readDWord();
		totalObjects525 = idxBuffer525.readUnsignedWord();
		totalObjectsOSRS = idxBufferOSRS.readUnsignedWord();
		System.out.println("474 Object Amount: " + totalObjects474);
		System.out.println("525 Object Amount: " + totalObjects525);
		System.out.println("667 Object Amount: " + totalObjects667);
		System.out.println("OSRS Object Amount: " + totalObjectsOSRS);
		streamIndices474 = new int[totalObjects474];
		int i = 2;
		for (int j = 0; j < totalObjects474; j++) {
			streamIndices474[j] = i;
			i += stream.readUnsignedWord();
		}
		streamIndices667 = new int[totalObjects667];
		i = 4;
		for (int j = 0; j < totalObjects667; j++) {
			streamIndices667[j] = i;
			i += idxBuffer667.readDWord();
		}
		streamIndices830 = new int[totalObjects830];
		i = 4;
		for (int j = 0; j < totalObjects830; j++) {
			streamIndices830[j] = i;
			i += idxBuffer830.readDWord();
		}
		streamIndices525 = new int[totalObjects525];
		i = 2;
		for (int j = 0; j < totalObjects525; j++) {
			streamIndices525[j] = i;
			i += idxBuffer525.readUnsignedWord();
		}
		
		streamIndicesOSRS = new int[totalObjectsOSRS];
		i = 2;
		for (int j = 0; j < totalObjectsOSRS; j++) {
			streamIndicesOSRS[j] = i;
			i += idxBufferOSRS.readUnsignedWord();
		}

//		for (int count = 0; count < ObjectDef.totalObjects667; count++) {
//			ObjectDef def = ObjectDef.forID(count);
//			if (def == null || def.name == null) {
//				continue;
//			}
//
//			if (def.name.toLowerCase().contains("clock")) {
//				System.out.println(count + " " + def.name);
//			}
//		}
	}

	public boolean method577(int i) {
		if (anIntArray776 == null) {
			if (anIntArray773 == null && newModels == null)
				return true;
			if (i != 10)
				return true;
			boolean flag1 = true;
			if (newModels != null) {
				int count = newModels.length;
				for (int i1 = 0; i1 < count; i1++)
				{
					int childCount = newModels[i1].length;
					for (int i11 = 0; i11 < childCount; i11++)
						flag1 &= Model.method463(newModels[i1][i11] & 0x3ffff, modelType);

				}
			} else {
				for (int k = 0; k < anIntArray773.length; k++)
					flag1 &= Model.method463(anIntArray773[k] & 0x3ffff, modelType);
			}
	
			return flag1;
		}
		
		if (newModels != null) {
			boolean flag1 = true;
			for (int j = 0; j < anIntArray776.length; j++) {
				if (anIntArray776[j] == i) {
					int childCount = newModels[j].length;
					for (int i11 = 0; i11 < childCount; i11++) {
						flag1 &= Model.method463(newModels[j][i11] & 0x3ffff, modelType);
					}
	
					break;
				}
			}
			
			return flag1;
		} else {
			for (int j = 0; j < anIntArray776.length; j++)
				if (anIntArray776[j] == i)
					return Model.method463(anIntArray773[j] & 0x3ffff, modelType);
		}

		return true;
	}

	public Model method578(int i, int j, int hXY, int hX1Y, int hX1Y1, int hXY1, int k1) {
		long uid;
		if (anIntArray776 == null) {
			uid = (long) (myId << 10) + j;
		} else {
			uid = (long) (i << 3) + (myId << 10) + j;
		}

		Model model;
//		synchronized(mruNodes2) {
			model = (Model) mruNodes2.get(uid);
//		}
		if (model == null) {
			model = method581(i, true, j);
			if (model == null) {
				return null;
			}

//			synchronized(mruNodes2) {
				mruNodes2.put(model, uid);
//			}
		}

		if (k1 == -1 && !aBoolean762) {
			return model;
		}

		model = model.copyLoc(!Class36.hasAlpha(k1));

		if (k1 != -1) {
			switch (j) {
				case 1:
					model.rotate270();
					break;
				case 2:
					model.rotate180();
					break;
				case 3:
					model.rotate90();
					break;
			}

			model.animated = true;
			if (!model.applyAnimation(k1)) {
				return null;
			}

			switch (j) {
				case 1:
					model.rotate90();
					break;
				case 2:
					model.rotate180();
					break;
				case 3:
					model.rotate270();
					break;
			}
		}

		if (aBoolean762) {
			model.adjustToGround(hXY, hX1Y, hXY1, hX1Y1);
		}

		model.method466();

		return model;
	}

	public Model method578(int i, int j, int hXY, int hX1Y, int hX1Y1, int hXY1) {
		long uid;
		if (anIntArray776 == null) {
			uid = (long) (myId << 10) + j;
		} else {
			uid = (long) (i << 3) + (myId << 10) + j;
		}

		Model model;
//		synchronized(mruNodes2) {
			model = (Model) mruNodes2.get(uid);
//		}
		if (model == null) {
			model = method581(i, !aBoolean769, j);
			if (model == null) {
				return null;
			}

//			synchronized(mruNodes2) {
				mruNodes2.put(model, uid);
//			}
		}

		if (aBoolean762 || aBoolean769) {
			model = new Model(true, aBoolean769, model);
		}

		if (aBoolean762) {
			model.adjustToGround(hXY, hX1Y, hXY1, hX1Y1);
		}

		model.method466();

		return model;
	}

	public boolean method579() {
		if (anIntArray773 == null && newModels == null)
			return true;
		boolean flag1 = true;
		if (newModels != null) {
			for (int j = 0; j < newModels.length; j++) {
				int childCount = newModels[j].length;
				for (int i11 = 0; i11 < childCount; i11++) {
					flag1 &= Model.method463(newModels[j][i11] & 0x3ffff, modelType);
				}
			}
		} else {
			for (int i = 0; i < anIntArray773.length; i++) {
				flag1 &= Model.method463(anIntArray773[i] & 0x3ffff, modelType);
			}
		}
		return flag1;
	}

	public ObjectDef getTransformedObject() { //method580
		int i = -1;
		if (anInt774 != -1) {
			VarBit varBit = VarBit.cache[anInt774];
			int j = varBit.anInt648;
			int k = varBit.anInt649;
			int l = varBit.anInt650;
			int i1 = Client.anIntArray1232[l - k];
			i = Client.variousSettings[j] >> k & i1;
		} else if (configID != -1)
			i = Client.variousSettings[configID];
		if (i < 0 || i >= childrenIDs.length || childrenIDs[i] == -1)
			return null;
		else
			return forID(childrenIDs[i]);
	}

	public Model method581(int type, boolean flatShading, int rotation) {
		Model model = null;
		if (anIntArray776 == null) {
			if (type != 10)
				return null;
			if (anIntArray773 == null)
				return null;
			boolean flag1 = aBoolean751 ^ (rotation > 3);
			int k1 = anIntArray773.length;
			for (int i2 = 0; i2 < k1; i2++) {
				int l2 = anIntArray773[i2];
				if (flag1)
					l2 += 0x40000;
//				synchronized(mruNodes1) {
					model = (Model) mruNodes1.get(l2);
//				}
				if (model == null) {
					model = Model.loadDropModel(l2 & 0x3ffff, modelType);
					if (model == null)
						return null;
					if (flag1)
						model.method477();
//					synchronized(mruNodes1) {
						mruNodes1.put(model, l2);
//					}
				}
				if (k1 > 1)
					aModelArray741s[i2] = model;
			}

			if (k1 > 1)
				model = new Model(k1, aModelArray741s);
		} else {
			int i1 = -1;
			for (int j1 = 0; j1 < anIntArray776.length; j1++) {
				if (anIntArray776[j1] != type)
					continue;
				i1 = j1;
				break;
			}

			if (i1 == -1)
				return null;

			boolean flag3 = aBoolean751 ^ (rotation > 3);
			if (newModels != null) {
				int k1 = newModels[i1].length;
				for (int i2 = 0; i2 < k1; i2++) {
					int l2 = newModels[i1][i2];
					if (flag3)
						l2 += 0x40000;
//					synchronized(mruNodes1) {
						model = (Model) mruNodes1.get(l2);
//					}
					if (model == null) {
						model = Model.loadDropModel(l2 & 0x3ffff, modelType);
						if (model == null)
							return null;
						if (flag3)
							model.method477();
//						synchronized(mruNodes1) {
							mruNodes1.put(model, l2);
//						}
					}
					if (k1 > 1)
						aModelArray741s[i2] = model;
				}

				if (k1 > 1)
					model = new Model(k1, aModelArray741s);
			} else {
				int j2 = anIntArray773[i1];
				if (flag3)
					j2 += 0x40000;
//				synchronized(mruNodes1) {
					model = (Model) mruNodes1.get(j2);
//				}
				if (model == null) {
					model = Model.loadDropModel(j2 & 0x3ffff, modelType);
					if (model == null)
						return null;
					if (flag3)
						model.method477();
//					synchronized(mruNodes1) {
						mruNodes1.put(model, j2);
//					}
				}
			}
		}

		boolean needScale = anInt748 != 128 || anInt772 != 128 || anInt740 != 128;
		boolean needOffset = anInt738 != 0 || anInt745 != 0 || anInt783 != 0;
		Model model_3 = new Model(modifiedModelColors == null, true, rotation == 0 && !needScale && !needOffset, model);
		model_3.applyEffects();

		rotation &= 0x3;
		switch (rotation) {
			case 1:
				model_3.rotate90();
				break;
			case 2:
				model_3.rotate180();
				break;
			case 3:
				model_3.rotate270();
				break;
		}

		if (modifiedModelColors != null) {
			for (int k2 = 0, length = modifiedModelColors.length; k2 < length; k2++) {
				model_3.replaceHs1(modifiedModelColors[k2],
						originalModelColors[k2]);
			}
		}

		if (needScale)
			model_3.method478(anInt748, anInt772, anInt740);
		if (needOffset)
			model_3.translate(anInt738, anInt745, anInt783);
		model_3.calculateLighting(64, 768, -50, -10, -50, flatShading); // alk update rendering objects
		if (anInt760 == 1) {
			model_3.anInt1654 = model_3.modelHeight;
		}
		return model_3;
	}
	
	public void readValuesOSRS(RSBuffer stream) {
		int flag = -1;
		do {
			int type = stream.readUnsignedByte();
			if (type == 0)
				break;
			if (type == 1) {
				int len = stream.readUnsignedByte();
				if (len > 0) {
					if (anIntArray773 == null || lowMem) {
						anIntArray776 = new byte[len];
						anIntArray773 = new int[len];
						for (int k1 = 0; k1 < len; k1++) {
							anIntArray773[k1] = stream.readUnsignedWord();
							anIntArray776[k1] = (byte) stream.readUnsignedByte();
						}
					} else {
						stream.currentOffset += len * 3;
					}
				}
			} else if (type == 2)
				name = stream.readString();
			else if (type == 3)
				description = stream.readString().getBytes();
			else if (type == 5) {
				int len = stream.readUnsignedByte();
				if (len > 0) {
					if (anIntArray773 == null || lowMem) {
						anIntArray776 = null;
						anIntArray773 = new int[len];
						for (int l1 = 0; l1 < len; l1++)
							anIntArray773[l1] = stream.readUnsignedWord();
					} else {
						stream.currentOffset += len * 2;
					}
				}
			} else if (type == 14)
				anInt744 = stream.readUnsignedByte();
			else if (type == 15)
				anInt761 = stream.readUnsignedByte();
			else if (type == 17)
				aBoolean767 = false;
			else if (type == 18)
				aBoolean757 = false;
			else if (type == 19)
				hasActions = (stream.readUnsignedByte() == 1);
			else if (type == 21)
				aBoolean762 = true;
			else if (type == 22)
				aBoolean769 = true;
			else if (type == 23)
				aBoolean764 = true;
			else if (type == 24) {
				anInt781 = stream.readUnsignedWord();
				if (anInt781 == 65535) {
					anInt781 = -1;
				} else {
					anInt781 += Animation.NEW_OSRS_ID_START;
					if (anInt781 >= 65535) {
						System.out.println("error! animationId:"+anInt781);
					}
				}
			} else if (type == 28)
				anInt775 = stream.readUnsignedByte();
			else if (type == 29)
				aByte737 = stream.readSignedByte();
			else if (type == 39)
				aByte742 = stream.readSignedByte();
			else if (type >= 30 && type < 39) {
				if (actions == null)
					actions = new String[5];
				actions[type - 30] = stream.readString();
				if (actions[type - 30].equalsIgnoreCase("hidden"))
					actions[type - 30] = null;
			} else if (type == 40) {
				int i1 = stream.readUnsignedByte();
				modifiedModelColors = new int[i1];
				originalModelColors = new int[i1];
				for (int i2 = 0; i2 < i1; i2++) {
					modifiedModelColors[i2] = stream.readUnsignedWord();
					originalModelColors[i2] = stream.readUnsignedWord();
				}
			} else if (type == 41) {
				int j2 = stream.readUnsignedByte();
//				modifiedTexture = new short[j2];
//				originalTexture = new short[j2];
				for (int k = 0; k < j2; k++) {
					/*modifiedTexture[k] = (short)*/ stream.readUnsignedWord();
					/*originalTexture[k] = (short)*/ stream.readUnsignedWord();
				}

			} else if (type == 60)
				anInt746 = stream.readUnsignedWord();
			else if (type == 62)
				aBoolean751 = true;
			else if (type == 64)
				aBoolean779 = false;
			else if (type == 65)
				anInt748 = stream.readUnsignedWord();
			else if (type == 66)
				anInt772 = stream.readUnsignedWord();
			else if (type == 67)
				anInt740 = stream.readUnsignedWord();
			else if (type == 68)
				anInt758 = stream.readUnsignedWord();
			else if (type == 69)
				anInt768 = stream.readUnsignedByte();
			else if (type == 70)
				anInt738 = stream.readSignedWord();
			else if (type == 71)
				anInt745 = stream.readSignedWord();
			else if (type == 72)
				anInt783 = stream.readSignedWord();
			else if (type == 73)
				aBoolean736 = true;
			else if (type == 74)
				aBoolean766 = true;
			else if (type == 75)
				anInt760 = stream.readUnsignedByte();
			else if (type == 77 || type == 92) {
				anInt774 = stream.readUnsignedWord();
				if (anInt774 == 65535)
					anInt774 = -1;
				configID = stream.readUnsignedWord();
				if (configID == 65535)
					configID = -1;

				if (type == 92) {
					stream.readUnsignedWord();
				}

				int j1 = stream.readUnsignedByte();
				childrenIDs = new int[j1 + 1];
				for (int j2 = 0; j2 <= j1; j2++) {
					childrenIDs[j2] = stream.readUnsignedWord();
					if (childrenIDs[j2] == 65535)
						childrenIDs[j2] = -1;
				}
			}
		} while (true);
		if (flag == -1  && name != "null" && name != null) {
			hasActions = anIntArray773 != null
			&& (anIntArray776 == null || anIntArray776[0] == 10);
			if (actions != null)
				hasActions = true;
		}
		if (aBoolean766) {
			aBoolean767 = false;
			aBoolean757 = false;
		}
		if (anInt760 == -1)
			anInt760 = aBoolean767 ? 1 : 0;
	}
	
	public void readValuesUSA(RSBuffer stream, boolean def830) {
		do {
			int type = stream.readUnsignedByte();
			if (type == 0)
				break;
			if (type == 1) {
				int count = stream.readUnsignedByte();
				anIntArray776 = new byte[count];
				newModels = new int[count][];
				for (int i = 0; i != count; ++i)
				{
					anIntArray776[i] = (byte) stream.readUnsignedByte();
					int childCount = stream.readUnsignedByte();
					newModels[i] = new int[childCount];
					for (int i1 = 0; i1 != childCount; ++i1)
						newModels[i][i1] = def830 ? stream.readDWord() : stream.readUnsignedWord();

				}
			} else if (type == 2)
				name = stream.readString();
			else if (type == 3)
				description = stream.readString().getBytes();
			else if (type == 5) {
				int len = stream.readUnsignedByte();
				if (len > 0) {
					if (anIntArray773 == null || lowMem) {
						anIntArray776 = null;
						anIntArray773 = new int[len];
						for (int l1 = 0; l1 < len; l1++)
							anIntArray773[l1] = stream.readUnsignedWord();
					} else {
						stream.currentOffset += len * 2;
					}
				}
			} else if (type == 14)
				anInt744 = stream.readUnsignedByte();
			else if (type == 15)
				anInt761 = stream.readUnsignedByte();
			else if (type == 17) {
				aBoolean767 = false;
				clipType = 0;
			}
			else if (type == 18)
				aBoolean757 = false;
			else if (type == 19)
				hasActions = (stream.readUnsignedByte() == 1);
			else if (type == 21)
				aBoolean762 = true;
			else if (type == 22)
				aBoolean769 = true;
			else if (type == 23)
				aBoolean764 = true;
			else if (type == 24) {
				anInt781 = stream.readUnsignedWord();
				if (anInt781 == 65535)
					anInt781 = -1;
			} else if (type == 27) {
				clipType = 1;
			} else if (type == 28)
				anInt775 = stream.readUnsignedByte();
			else if (type == 29)
				aByte737 = stream.readSignedByte();
			else if (type == 39)
				aByte742 = stream.readSignedByte();
			else if (type >= 30 && type < 39) {
				if (actions == null)
					actions = new String[5];
				actions[type - 30] = stream.readString();
				if (actions[type - 30].equalsIgnoreCase("hidden"))
					actions[type - 30] = null;
			} else if (type == 40) {
				int i1 = stream.readUnsignedByte();
				modifiedModelColors = new int[i1];
				originalModelColors = new int[i1];
				for (int i2 = 0; i2 < i1; i2++) {
					modifiedModelColors[i2] = stream.readUnsignedWord();
					originalModelColors[i2] = stream.readUnsignedWord();
				}
			} else if (type == 41) {
				int j2 = stream.readUnsignedByte();
//				modifiedTexture = new short[j2];
//				originalTexture = new short[j2];
				for (int k = 0; k < j2; k++) {
					/*modifiedTexture[k] = (short)*/ stream.readUnsignedWord();
					/*originalTexture[k] = (short)*/ stream.readUnsignedWord();
				}

			} else if (type == 60)
				anInt746 = stream.readUnsignedWord();
			else if (type == 62)
				aBoolean751 = true;
			else if (type == 64)
				aBoolean779 = false;
			else if (type == 65)
				anInt748 = stream.readUnsignedWord();
			else if (type == 66)
				anInt772 = stream.readUnsignedWord();
			else if (type == 67)
				anInt740 = stream.readUnsignedWord();
			else if (type == 68)
				anInt758 = stream.readUnsignedWord();
			else if (type == 69)
				anInt768 = stream.readUnsignedByte();
			else if (type == 70)
				anInt738 = stream.readSignedWord();
			else if (type == 71)
				anInt745 = stream.readSignedWord();
			else if (type == 72)
				anInt783 = stream.readSignedWord();
			else if (type == 73)
				aBoolean736 = true;
			else if (type == 74)
				aBoolean766 = true;
			else if (type == 75)
				anInt760 = stream.readUnsignedByte();
			else if (type == 77) {
				anInt774 = stream.readUnsignedWord();
				if (anInt774 == 65535)
					anInt774 = -1;
				configID = stream.readUnsignedWord();
				if (configID == 65535)
					configID = -1;
				int j1 = stream.readUnsignedByte();
				childrenIDs = new int[j1 + 1];
				for (int j2 = 0; j2 <= j1; j2++) {
					childrenIDs[j2] = stream.readUnsignedWord();
					if (childrenIDs[j2] == 65535)
						childrenIDs[j2] = -1;
				}
			}
		} while (true);
//		if (flag == -1  && name != "null" && name != null) {
//			hasActions = newModels != null
//			&& (anIntArray776 == null || anIntArray776[0] == 10);
//			if (actions != null)
//				hasActions = true;
//		}
		
		if (!hasActions) {
			if (anIntArray776 != null && anIntArray776.length == 1 && anIntArray776[0] == 10)
				hasActions = true;
			for (int i_13_ = 0; i_13_ < 5; i_13_++) {
				if (actions[i_13_] != null) {
					hasActions = true;
					break;
				}
			}
		}
		
		if (aBoolean766) {
			aBoolean767 = false;
			aBoolean757 = false;
		}
		if (anInt760 == -1)
			anInt760 = clipType != 0 ? 1 : 0;
	}
	
	public void readValues525(RSBuffer stream) {
		int i = -1;
		do {
			int i_39_ = stream.readUnsignedByte();
			if (i_39_ == 0) {
				break;
			}
			if (i_39_ == 1) {
				int i_40_ = stream.readUnsignedByte();
				if (i_40_ > 0) {
					if (anIntArray773 == null || lowMem) {
						anIntArray776 = new byte[i_40_];
						anIntArray773 = new int[i_40_];
						for (int i_41_ = 0; i_41_ < i_40_; i_41_++) {
							anIntArray773[i_41_] = stream.readUnsignedWord();
							anIntArray776[i_41_] = (byte) stream.readUnsignedByte();
						}
					} else {
						stream.currentOffset += i_40_ * 3;
					}
				}
			} else if (i_39_ == 2) {
				name = stream.readString();
			} else if (i_39_ == 3) {
				description = stream.readBytes();
			} else if (i_39_ == 5) {
				int i_42_ = stream.readUnsignedByte();
				if (i_42_ > 0) {
					if (anIntArray773 == null || lowMem) {
						anIntArray776 = null;
						anIntArray773 = new int[i_42_];
						for (int i_43_ = 0; i_43_ < i_42_; i_43_++)
							anIntArray773[i_43_] = stream.readUnsignedWord();
					} else {
						stream.currentOffset += i_42_ * 2;
					}
				}
			} else if (i_39_ == 14) {
				anInt744 = stream.readUnsignedByte();
			} else if (i_39_ == 15) {
				anInt761 = stream.readUnsignedByte();
			} else if (i_39_ == 17) {
				aBoolean767 = false;
				aBoolean757 = false; // 474+
			} else if (i_39_ == 18) {
				aBoolean757 = false;
			} else if (i_39_ == 19) {
				i = stream.readUnsignedByte();
				if (i == 1) {
					hasActions = true;
				}
			} else if (i_39_ == 21) {
				aBoolean762 = true;
			} else if (i_39_ == 22) {
				aBoolean769 = true;
			} else if (i_39_ == 23) {
				aBoolean764 = true;
			} else if (i_39_ == 24) {
				anInt781 = stream.readUnsignedWord();
				if (anInt781 == 65535) {
					anInt781 = -1;
				}
			} else if (i_39_ == 28) {
				anInt775 = stream.readUnsignedByte();
			} else if (i_39_ == 29) {
				aByte737 = stream.readSignedByte();
			} else if (i_39_ == 39) {
				aByte742 = stream.readSignedByte();
			} else if (i_39_ >= 30 && i_39_ < 39) {
				if (actions == null) {
					actions = new String[10];
				}
				actions[i_39_ - 30] = stream.readString();
				if (actions[i_39_ - 30].equalsIgnoreCase("hidden")) {
					actions[i_39_ - 30] = null;
				}
			} else if (i_39_ == 40) {
				int i_44_ = stream.readUnsignedByte();
				modifiedModelColors = new int[i_44_];
				originalModelColors = new int[i_44_];
				for (int i_45_ = 0; i_45_ < i_44_; i_45_++) {
					modifiedModelColors[i_45_] = stream.readUnsignedWord();
					originalModelColors[i_45_] = stream.readUnsignedWord();
				}
			} else if (i_39_ == 60) {
				anInt746 = stream.readUnsignedWord();
			} else if (i_39_ == 62) {
				aBoolean751 = true;
			} else if (i_39_ == 64) {
				aBoolean779 = false;
			} else if (i_39_ == 65) {
				anInt748 = stream.readUnsignedWord();
			} else if (i_39_ == 66) {
				anInt772 = stream.readUnsignedWord();
			} else if (i_39_ == 67) {
				anInt740 = stream.readUnsignedWord();
			} else if (i_39_ == 68) {
				anInt758 = stream.readUnsignedWord();
			} else if (i_39_ == 69) {
				anInt768 = stream.readUnsignedByte();
			} else if (i_39_ == 70) {
				anInt738 = stream.readSignedWord();
			} else if (i_39_ == 71) {
				anInt745 = stream.readSignedWord();
			} else if (i_39_ == 72) {
				anInt783 = stream.readSignedWord();
			} else if (i_39_ == 73) {
				aBoolean736 = true;
			} else if (i_39_ == 74) {
				aBoolean766 = true;
			} else if (i_39_ == 75) {
				anInt760 = stream.readUnsignedByte();
			} else if (i_39_ == 77) {
				anInt774 = stream.readUnsignedWord();
				if (anInt774 == 65535) {
					anInt774 = -1;
				}
				anInt774 = stream.readUnsignedWord();
				if (anInt774 == 65535) {
					anInt774 = -1;
				}
				int i_46_ = stream.readUnsignedByte();
				childrenIDs = new int[i_46_ + 1];
				for (int i_47_ = 0; i_47_ <= i_46_; i_47_++) {
					childrenIDs[i_47_] = stream.readUnsignedWord();
					if (childrenIDs[i_47_] == 65535) {
						childrenIDs[i_47_] = -1;
					}
				}
			}
		} while (true);
		if (i == -1 && !name.equalsIgnoreCase("null")) {
			hasActions = false;
			if (anIntArray773 != null
					&& (anIntArray776 == null || anIntArray776[0] == 10)) {
				hasActions = true;
			}
			if (actions != null) {
				hasActions = true;
			}
		}
		if (aBoolean766) {
			aBoolean767 = false;
			aBoolean757 = false;
		}
		if (anInt760 == -1)
			anInt760 = aBoolean767 ? 1 : 0;
	}

	public void readValues474(RSBuffer byteBuffer) {
		int i = -1;
		label0: do {
			int j;
			do {
				j = byteBuffer.readUnsignedByte();
				if (j == 0)
					break label0;
				if (j == 1) {
					int k = byteBuffer.readUnsignedByte();
					if (k > 0)
						if (anIntArray773 == null || lowMem) {
							anIntArray776 = new byte[k];
							anIntArray773 = new int[k];
							for (int k1 = 0; k1 < k; k1++) {
								anIntArray773[k1] = byteBuffer
										.readUnsignedWord();
								anIntArray776[k1] = (byte) byteBuffer.readUnsignedByte();
							}

						} else {
							byteBuffer.currentOffset += k * 3;
						}
				} else if (j == 2)
					name = byteBuffer.readString();
				else if (j == 3) {
					try {
					description = byteBuffer.readBytes();
					} catch (Exception ex) {
						System.out.println("bugged objdef item:"+myId);
					}
				}
				else if (j == 5) {
					int l = byteBuffer.readUnsignedByte();
					if (l > 0)
						if (anIntArray773 == null || lowMem) {
							anIntArray776 = null;
							anIntArray773 = new int[l];
							for (int l1 = 0; l1 < l; l1++)
								anIntArray773[l1] = byteBuffer
								.readUnsignedWord();

						} else {
							byteBuffer.currentOffset += l * 2;
						}
				} else if (j == 14)
					anInt744 = byteBuffer.readUnsignedByte();
				else if (j == 15)
					anInt761 = byteBuffer.readUnsignedByte();
				else if (j == 17)
					aBoolean767 = false;
				else if (j == 18)
					aBoolean757 = false;
				else if (j == 19) {
					i = byteBuffer.readUnsignedByte();
					if (i == 1)
						hasActions = true;
				} else if (j == 21)
					aBoolean762 = true;
				else if (j == 22)
					aBoolean769 = true;
				else if (j == 23)
					aBoolean764 = true;
				else if (j == 24) {
					anInt781 = byteBuffer.readUnsignedWord();
					if (anInt781 == 65535)
						anInt781 = -1;
				} else if (j == 28)
					anInt775 = byteBuffer.readUnsignedByte();
				else if (j == 29)
					aByte737 = byteBuffer.readSignedByte();
				else if (j == 39)
					aByte742 = byteBuffer.readSignedByte();
				else if (j >= 30 && j < 39) {
					if (actions == null)
						actions = new String[5];
					actions[j - 30] = byteBuffer.readString();
					if (actions[j - 30].equalsIgnoreCase("hidden"))
						actions[j - 30] = null;
				} else if (j == 40) {
					int i1 = byteBuffer.readUnsignedByte();
					modifiedModelColors = new int[i1];
					originalModelColors = new int[i1];
					for (int i2 = 0; i2 < i1; i2++) {
						modifiedModelColors[i2] = byteBuffer.readUnsignedWord();
						originalModelColors[i2] = byteBuffer.readUnsignedWord();
					}

				} else if (j == 60)
					anInt746 = byteBuffer.readUnsignedWord();
				else if (j == 62)
					aBoolean751 = true;
				else if (j == 64)
					aBoolean779 = false;
				else if (j == 65)
					anInt748 = byteBuffer.readUnsignedWord();
				else if (j == 66)
					anInt772 = byteBuffer.readUnsignedWord();
				else if (j == 67)
					anInt740 = byteBuffer.readUnsignedWord();
				else if (j == 68)
					anInt758 = byteBuffer.readUnsignedWord();
				else if (j == 69)
					anInt768 = byteBuffer.readUnsignedByte();
				else if (j == 70)
					anInt738 = byteBuffer.readSignedWord();
				else if (j == 71)
					anInt745 = byteBuffer.readSignedWord();
				else if (j == 72)
					anInt783 = byteBuffer.readSignedWord();
				else if (j == 73)
					aBoolean736 = true;
				else if (j == 74) {
					aBoolean766 = true;
				} else {
					if (j != 75)
						continue;
					anInt760 = byteBuffer.readUnsignedByte();
				}
				continue label0;
			} while (j != 77);
			anInt774 = byteBuffer.readUnsignedWord();
			if (anInt774 == 65535)
				anInt774 = -1;
			configID = byteBuffer.readUnsignedWord();
			if (configID == 65535)
				configID = -1;
			int j1 = byteBuffer.readUnsignedByte();
			childrenIDs = new int[j1 + 1];
			for (int j2 = 0; j2 <= j1; j2++) {
				childrenIDs[j2] = byteBuffer.readUnsignedWord();
				if (childrenIDs[j2] == 65535)
					childrenIDs[j2] = -1;
			}
		} while (true);
		//if (i == -1) {
		if (i == -1  && name != "null" && name != null) {
			hasActions = anIntArray773 != null
					&& (anIntArray776 == null || anIntArray776[0] == 10);
			if (actions != null)
				hasActions = true;
		}
		if (aBoolean766) {
			aBoolean767 = false;
			aBoolean757 = false;
		}
		if (anInt760 == -1)
			anInt760 = aBoolean767 ? 1 : 0;

	}
	
	public void readValues474OLD(RSBuffer stream) {
		int flag = -1;
		do {
			int type = stream.readUnsignedByte();
			if (type == 0)
				break;
			if (type == 1) {
				int len = stream.readUnsignedByte();
				if (len > 0) {
					if (anIntArray773 == null || lowMem) {
						anIntArray776 = new byte[len];
						anIntArray773 = new int[len];
						for (int k1 = 0; k1 < len; k1++) {
							anIntArray773[k1] = stream.readUnsignedWord();
							anIntArray776[k1] = (byte) stream.readUnsignedByte();
						}
					} else {
						stream.currentOffset += len * 3;
					}
				}
			} else if (type == 2)
				name = stream.readNewString();
			else if (type == 3)
				description = stream.readBytes();
			else if (type == 5) {
				int len = stream.readUnsignedByte();
				if (len > 0) {
					if (anIntArray773 == null || lowMem) {
						anIntArray776 = null;
						anIntArray773 = new int[len];
						for (int l1 = 0; l1 < len; l1++)
							anIntArray773[l1] = stream.readUnsignedWord();
					} else {
						stream.currentOffset += len * 2;
					}
				}
			} else if (type == 14)
				anInt744 = stream.readUnsignedByte();
			else if (type == 15)
				anInt761 = stream.readUnsignedByte();
			else if (type == 17)
				aBoolean767 = false;
			else if (type == 18)
				aBoolean757 = false;
			else if (type == 19) {
				//hasActions = (stream.readUnsignedByte() == 1);
				flag = stream.readUnsignedByte();
				if (flag == 1)
					hasActions = true;
			}
			else if (type == 21)
				aBoolean762 = true;
			else if (type == 22)
				aBoolean769 = true;
			else if (type == 23)
				aBoolean764 = true;
			else if (type == 24) {
				anInt781 = stream.readUnsignedWord();
				if (anInt781 == 65535)
					anInt781 = -1;
			} else if (type == 28)
				anInt775 = stream.readUnsignedByte();
			else if (type == 29)
				aByte737 = stream.readSignedByte();
			else if (type == 39)
				aByte742 = stream.readSignedByte();
			else if (type >= 30 && type < 39) {
				if (actions == null)
					actions = new String[5];
				actions[type - 30] = stream.readNewString();
				if (actions[type - 30].equalsIgnoreCase("hidden"))
					actions[type - 30] = null;
			} else if (type == 40) {
				int i1 = stream.readUnsignedByte();
				modifiedModelColors = new int[i1];
				originalModelColors = new int[i1];
				for (int i2 = 0; i2 < i1; i2++) {
					modifiedModelColors[i2] = stream.readUnsignedWord();
					originalModelColors[i2] = stream.readUnsignedWord();
				}

			} else if (type == 60)
				anInt746 = stream.readUnsignedWord();
			else if (type == 62)
				aBoolean751 = true;
			else if (type == 64)
				aBoolean779 = false;
			else if (type == 65)
				anInt748 = stream.readUnsignedWord();
			else if (type == 66)
				anInt772 = stream.readUnsignedWord();
			else if (type == 67)
				anInt740 = stream.readUnsignedWord();
			else if (type == 68)
				anInt758 = stream.readUnsignedWord();
			else if (type == 69)
				anInt768 = stream.readUnsignedByte();
			else if (type == 70)
				anInt738 = stream.readSignedWord();
			else if (type == 71)
				anInt745 = stream.readSignedWord();
			else if (type == 72)
				anInt783 = stream.readSignedWord();
			else if (type == 73)
				aBoolean736 = true;
			else if (type == 74)
				aBoolean766 = true;
			else if (type == 75)
				anInt760 = stream.readUnsignedByte();
			else if (type == 77) {
				anInt774 = stream.readUnsignedWord();
				if (anInt774 == 65535)
					anInt774 = -1;
				configID = stream.readUnsignedWord();
				if (configID == 65535)
					configID = -1;
				int j1 = stream.readUnsignedByte();
				childrenIDs = new int[j1 + 1];
				for (int j2 = 0; j2 <= j1; j2++) {
					childrenIDs[j2] = stream.readUnsignedWord();
					if (childrenIDs[j2] == 65535)
						childrenIDs[j2] = -1;
				}
			}
		} while (true);
		if (flag == -1 && name != "null" && name != null) {
			hasActions = anIntArray773 != null
					&& (anIntArray776 == null || anIntArray776[0] == 10);
			if (actions != null)
				hasActions = true;
		}
		if (aBoolean766) {
			aBoolean767 = false;
			aBoolean757 = false;
		}
		if (anInt760 == -1)
			anInt760 = aBoolean767 ? 1 : 0;
	}
	
	 private void readValues667OLD(RSBuffer buffer) {
			int i = -1;
			label0: do {
				int opcode;
				do {
					opcode = buffer.readUnsignedByte();
					if (opcode == 0)
						break label0;
					if (opcode == 1) {
						int k = buffer.readUnsignedByte();
						if (k > 0)
							if (anIntArray773 == null || lowMem) {
								anIntArray776 = new byte[k];
								anIntArray773 = new int[k];
								for (int k1 = 0; k1 < k; k1++) {
									anIntArray773[k1] = buffer.readUnsignedWord();
									anIntArray776[k1] = (byte) buffer.readUnsignedByte();
								}
							} else {
								buffer.currentOffset += k * 3;
							}
					} else if (opcode == 2)
						name = buffer.readString();
					else if (opcode == 3)
						description = buffer.readBytes();
					else if (opcode == 5) {
						int l = buffer.readUnsignedByte();
						if (l > 0)
							if (anIntArray773 == null || lowMem) {
								anIntArray776 = null;
								anIntArray773 = new int[l];
								for (int l1 = 0; l1 < l; l1++)
									anIntArray773[l1] = buffer.readUnsignedWord();
							} else {
								buffer.currentOffset += l * 2;
							}
					} else if (opcode == 14)
						anInt744 = buffer.readUnsignedByte();
					else if (opcode == 15)
						anInt761 = buffer.readUnsignedByte();
					else if (opcode == 17)
						aBoolean767 = false;
					else if (opcode == 18)
						aBoolean757 = false;
					else if (opcode == 19) {
						i = buffer.readUnsignedByte();
						if (i == 1)
							hasActions = true;
					} else if (opcode == 21)
						aBoolean762 = true;
					else if (opcode == 22)
						aBoolean769 = true;
					else if (opcode == 23)
						aBoolean764 = true;
					else if (opcode == 24) {
						anInt781 = buffer.readUnsignedWord();
						if (anInt781 == 65535)
							anInt781 = -1;
					} else if (opcode == 28)
						anInt775 = buffer.readUnsignedByte();
					else if (opcode == 29)
						aByte737 = buffer.readSignedByte();
					else if (opcode == 39)
						aByte742 = buffer.readSignedByte();
					else if (opcode >= 30 && opcode < 39) {
						if (actions == null)
							actions = new String[10];
						actions[opcode - 30] = buffer.readString();
						if (actions[opcode - 30].equalsIgnoreCase("hidden"))
							actions[opcode - 30] = null;
					} else if (opcode == 40) {
						int i1 = buffer.readUnsignedByte();
						modifiedModelColors = new int[i1];
						originalModelColors = new int[i1];
						for (int i2 = 0; i2 < i1; i2++) {
							modifiedModelColors[i2] = buffer.readUnsignedWord();
							originalModelColors[i2] = buffer.readUnsignedWord();
						}
					} else if (opcode == 60)
						anInt746 = buffer.readUnsignedWord();
					else if (opcode == 62)
						aBoolean751 = true;
					else if (opcode == 64)
						aBoolean779 = false;
					else if (opcode == 65)
						anInt748 = buffer.readUnsignedWord();
					else if (opcode == 66)
						anInt772 = buffer.readUnsignedWord();
					else if (opcode == 67)
						anInt740 = buffer.readUnsignedWord();
					else if (opcode == 68)
						anInt758 = buffer.readUnsignedWord();
					else if (opcode == 69)
						anInt768 = buffer.readUnsignedByte();
					else if (opcode == 70)
						anInt738 = buffer.readSignedWord();
					else if (opcode == 71)
						anInt745 = buffer.readSignedWord();
					else if (opcode == 72)
						anInt783 = buffer.readSignedWord();
					else if (opcode == 73)
						aBoolean736 = true;
					else if (opcode == 74) {
						aBoolean766 = true;
					} else {
						if (opcode != 75)
							continue;
						anInt760 = buffer.readUnsignedByte();
					}
					continue label0;
				} while (opcode != 77);
					anInt774 = buffer.readUnsignedWord();
				if (anInt774 == 65535)
					anInt774 = -1;
					configID = buffer.readUnsignedWord();
				if (configID == 65535)
					configID = -1;
				int j1 = buffer.readUnsignedByte();
				childrenIDs = new int[j1 + 1];
				for (int j2 = 0; j2 <= j1; j2++) {
					childrenIDs[j2] = buffer.readUnsignedWord();
					if (childrenIDs[j2] == 65535)
						childrenIDs[j2] = -1;
				}

			} while (true);
			if (i == -1) {
				hasActions = anIntArray773 != null && (anIntArray776 == null || anIntArray776[0] == 10);
				if (actions != null)
					hasActions = true;
			}
			if (aBoolean766) {
				aBoolean767 = false;
				aBoolean757 = false;
			}
			if (anInt760 == -1)
				anInt760 = aBoolean767 ? 1 : 0;
	    }
	 
	 private void readValues667(RSBuffer stream)
		{
			int i = -1;
	label0:
			do
			{
				int j;
				do
				{
					j = stream.readUnsignedByte();
					if(j == 0)
						break label0;
					if(j == 1)
					{
						int k = stream.readUnsignedByte();
						if(k > 0)
							if(anIntArray773 == null || lowMem)
							{
								anIntArray776 = new byte[k];
								anIntArray773 = new int[k];
								for(int k1 = 0; k1 < k; k1++)
								{
									anIntArray773[k1] = stream.readUnsignedWord();
									anIntArray776[k1] = (byte) stream.readUnsignedByte();
								}

							} else
							{
								stream.currentOffset += k * 3;
							}
					} else
					if(j == 2)
						name = stream.readString();
					else
					if(j == 3)
						description = stream.readBytes();
					else
					if(j == 5)
					{
						int l = stream.readUnsignedByte();
						if(l > 0)
							if(anIntArray773 == null || lowMem)
							{
								anIntArray776 = null;
								anIntArray773 = new int[l];
								for(int l1 = 0; l1 < l; l1++)
									anIntArray773[l1] = stream.readUnsignedWord();

							} else
							{
								stream.currentOffset += l * 2;
							}
					} else
					if(j == 14)
						anInt744 = stream.readUnsignedByte();
					else
					if(j == 15)
						anInt761 = stream.readUnsignedByte();
					else
					if(j == 17)
						aBoolean767 = false;
					else
					if(j == 18)
						aBoolean757 = false;
					else
					if(j == 19)
					{
						i = stream.readUnsignedByte();
						if(i == 1)
							hasActions = true;
					} else
					if(j == 21)
						aBoolean762 = true;
					else
					if(j == 22)
						aBoolean769 = true;
					else
					if(j == 23)
						aBoolean764 = true;
					else
					if(j == 24)
					{
						anInt781 = stream.readUnsignedWord();
						if(anInt781 == 65535)
							anInt781 = -1;
					} else
					if(j == 28)
						anInt775 = stream.readUnsignedByte();
					else
					if(j == 29)
						aByte737 = stream.readSignedByte();
					else
					if(j == 39)
						aByte742 = stream.readSignedByte();
					else
					if(j >= 30 && j < 39)
					{
						if(actions == null)
							actions = new String[5];
						actions[j - 30] = stream.readString();
						if(actions[j - 30].equalsIgnoreCase("hidden"))
							actions[j - 30] = null;
					} else
					if(j == 40)
					{
						int i1 = stream.readUnsignedByte();
						modifiedModelColors = new int[i1];
						originalModelColors = new int[i1];
						for(int i2 = 0; i2 < i1; i2++)
						{
							modifiedModelColors[i2] = stream.readUnsignedWord();
							originalModelColors[i2] = stream.readUnsignedWord();
						}

					} else
					if(j == 60)
						anInt746 = stream.readUnsignedWord();
					else
					if(j == 62)
						aBoolean751 = true;
					else
					if(j == 64)
						aBoolean779 = false;
					else
					if(j == 65)
						anInt748 = stream.readUnsignedWord();
					else
					if(j == 66)
						anInt772 = stream.readUnsignedWord();
					else
					if(j == 67)
						anInt740 = stream.readUnsignedWord();
					else
					if(j == 68)
						anInt758 = stream.readUnsignedWord();
					else
					if(j == 69)
						anInt768 = stream.readUnsignedByte();
					else
					if(j == 70)
						anInt738 = stream.readSignedWord();
					else
					if(j == 71){
						anInt745 = stream.readSignedWord();
					}else
					if(j == 72)
						anInt783 = stream.readSignedWord();
					else
					if(j == 73)
						aBoolean736 = true;
					else
					if(j == 74)
					{
						aBoolean766 = true;
					} else
					{
						if(j != 75)
							continue;
						anInt760 = stream.readUnsignedByte();
					}
					continue label0;
				} while(j != 77);
				anInt774 = stream.readUnsignedWord();
				if(anInt774 == 65535)
					anInt774 = -1;
				configID = stream.readUnsignedWord();
				if(configID == 65535)
					configID = -1;
				int j1 = stream.readUnsignedByte();
				childrenIDs = new int[j1 + 1];
				for(int j2 = 0; j2 <= j1; j2++)
				{
					childrenIDs[j2] = stream.readUnsignedWord();
					if(childrenIDs[j2] == 65535)
						childrenIDs[j2] = -1;
				}

			} while(true);
			if(i == -1)
			{
				hasActions = anIntArray773 != null && (anIntArray776 == null || anIntArray776[0] == 10);
				if(actions != null)
					hasActions = true;
				if(name == null || name == "null")//some walls showing up red on minimap fix!!
					hasActions = false;	
			}
			if(aBoolean766)
			{
				aBoolean767 = false;
				aBoolean757 = false;
			}
			if(anInt760 == -1)
				anInt760 = aBoolean767 ? 1 : 0;
		}

	 private void readValues634(RSBuffer stream) {
			int i = -1;
			label0: do {
				int j;
				do {
					j = stream.readUnsignedByte();
					if (j == 0)
						break label0;
					if (j == 1) {
						int k = stream.readUnsignedByte();
						if (k > 0)
							if (anIntArray773 == null || lowMem) {
								anIntArray776 = new byte[k];
								anIntArray773 = new int[k];
								for (int k1 = 0; k1 < k; k1++) {
									anIntArray773[k1] = stream.readUnsignedWord();
									anIntArray776[k1] = (byte) stream.readUnsignedByte();
								}
							} else {
								stream.currentOffset += k * 3;
							}
					} else if (j == 2)
						name = stream.readString();
					else if (j == 3)
						description = stream.readBytes();
					else if (j == 5) {
						int l = stream.readUnsignedByte();
						if (l > 0)
							if (anIntArray773 == null || lowMem) {
								anIntArray776 = null;
								anIntArray773 = new int[l];
								for (int l1 = 0; l1 < l; l1++)
									anIntArray773[l1] = stream.readUnsignedWord();
							} else {
								stream.currentOffset += l * 2;
							}
					} else if (j == 14)
						anInt744 = stream.readUnsignedByte();
					else if (j == 15)
						anInt761 = stream.readUnsignedByte();
					else if (j == 17)
						aBoolean767 = false;
					else if (j == 18)
						aBoolean757 = false;
					else if (j == 19) {
						i = stream.readUnsignedByte();
						if (i == 1)
							hasActions = true;
					} else if (j == 21)
						aBoolean762 = true;
					else if (j == 22)
						aBoolean769 = true; // change to false to fix gowwars waterfalls??
					else if (j == 23)
						aBoolean764 = true;
					else if (j == 24) {
						anInt781 = stream.readUnsignedWord();
						if (anInt781 == 65535)
							anInt781 = -1;
					} else if (j == 28)
						anInt775 = stream.readUnsignedByte();
					else if (j == 29)
						aByte737 = stream.readSignedByte();
					else if (j == 39)
						aByte742 = stream.readSignedByte();
					else if (j >= 30 && j < 39) {
						if (actions == null)
							actions = new String[10];
						actions[j - 30] = stream.readString();
						if (actions[j - 30].equalsIgnoreCase("hidden"))
							actions[j - 30] = null;
					} else if (j == 40) {
						int i1 = stream.readUnsignedByte();
						modifiedModelColors = new int[i1];
						originalModelColors = new int[i1];
						for (int i2 = 0; i2 < i1; i2++) {
							modifiedModelColors[i2] = stream.readUnsignedWord();
							originalModelColors[i2] = stream.readUnsignedWord();
						}
					} else if (j == 60)
						anInt746 = stream.readUnsignedWord();
					else if (j == 62)
						aBoolean751 = true;
					else if (j == 64)
						aBoolean779 = false;
					else if (j == 65)
						anInt748 = stream.readUnsignedWord();
					else if (j == 66)
						anInt772 = stream.readUnsignedWord();
					else if (j == 67)
						anInt740 = stream.readUnsignedWord();
					else if (j == 68)
						anInt758 = stream.readUnsignedWord();
					else if (j == 69)
						anInt768 = stream.readUnsignedByte();
					else if (j == 70)
						anInt738 = stream.readSignedWord();
					else if (j == 71)
						anInt745 = stream.readSignedWord();
					else if (j == 72)
						anInt783 = stream.readSignedWord();
					else if (j == 73)
						aBoolean736 = true;
					else if (j == 74) {
						aBoolean766 = true;
					} else {
						if (j != 75)
							continue;
						anInt760 = stream.readUnsignedByte();
					}
					continue label0;
				} while (j != 77);
				anInt774 = stream.readUnsignedWord();
				if (anInt774 == 65535)
					anInt774 = -1;
				configID = stream.readUnsignedWord();
				if (configID == 65535)
					configID = -1;
				int j1 = stream.readUnsignedByte();
				childrenIDs = new int[j1 + 1];
				for (int j2 = 0; j2 <= j1; j2++) {
					childrenIDs[j2] = stream.readUnsignedWord();
					if (childrenIDs[j2] == 65535)
						childrenIDs[j2] = -1;
				}

			} while (true);
			if (i == -1) {
				hasActions = anIntArray773 != null && (anIntArray776 == null || anIntArray776[0] == 10);
				if (actions != null)
					hasActions = true;
			}
			if (aBoolean766) {
				aBoolean767 = false;
				aBoolean757 = false;
			}
			if (anInt760 == -1)
				anInt760 = aBoolean767 ? 1 : 0;
		}
	 
	public ObjectDef() {
		myId = -1;
	}
	
	public static void createWhiteberryBush(int id, ObjectDef objectDef) {
		
		if (id == 10) { // 7722 one white berry
		objectDef.aBoolean736=false;
		objectDef.aByte737=25;
		objectDef.anInt738=0;
		objectDef.name="Whiteberry bush";
		objectDef.anInt740=128;
		objectDef.aByte742=0;
		objectDef.anInt744=1;
		objectDef.anInt745=0;
		objectDef.anInt746=-1;
		objectDef.originalModelColors=null;
		objectDef.anInt748=128;
		objectDef.configID=-1;
		objectDef.aBoolean751=false;
		objectDef.myId=10;
		objectDef.aBoolean757=true;
		objectDef.anInt758=-1;
		objectDef.childrenIDs=null;
		objectDef.anInt760=1;
		objectDef.anInt761=1;
		objectDef.aBoolean762=false;
		objectDef.aBoolean764=false;
		objectDef.aBoolean766=false;
		objectDef.aBoolean767=true;
		objectDef.anInt768=0;
		objectDef.aBoolean769=false;
		objectDef.anInt772=128;
		objectDef.anIntArray773=null;
		objectDef.anInt774=-1;
		objectDef.anInt775=16;
		objectDef.anIntArray776=new byte[] {10};
		objectDef.description=null;
		objectDef.hasActions=true;
		objectDef.aBoolean779=true;
		objectDef.anInt781=-1;
		objectDef.anInt783=0;
		objectDef.modifiedModelColors=null;
		objectDef.actions= new String[] {"Pick-from", "Inspect", null, "Guide", null};
		objectDef.newModels=new int[][] {{7826,7816,7767}};
		objectDef.clipType=2;
		}
	}
	
	public static void setZulrahObjects(int id, ObjectDef objectDef) {
		if (id == 10658) {
			objectDef.anIntArray773 = new int[] { 2288 };
			objectDef.modifiedModelColors = new int[] { 8598};
			objectDef.originalModelColors = new int[] { 10650};
			objectDef.anInt781=7286;//17245;//473;
			objectDef.name = "popo";
		}
		if (id == 11699) {
			objectDef.aBoolean736=false;
			objectDef.aByte737=0;
			objectDef.anInt738=0;
			objectDef.name=null;
			objectDef.anInt740=128;
			objectDef.aByte742=-10;
			objectDef.anInt744=1;
			objectDef.anInt745=0;
			objectDef.anInt746=-1;
			objectDef.originalModelColors= new int[] { 21543, 21547 };
			objectDef.anInt748=128;
			objectDef.configID=-1;
			objectDef.aBoolean751=false;
			objectDef.myId=11699;
			objectDef.aBoolean757=false;
			objectDef.anInt758=-1;
			objectDef.childrenIDs=null;
			objectDef.anInt760=1;
			objectDef.anInt761=1;
			objectDef.aBoolean762=false;
			objectDef.aBoolean764=false;
			objectDef.aBoolean766=false;
			objectDef.aBoolean767=true;
			objectDef.anInt768=0;
			objectDef.aBoolean769=false;
			objectDef.anInt772=128;
			objectDef.anIntArray773= new int[] { 1424 };
			objectDef.anInt774=-1;
			objectDef.anInt775=16;
			objectDef.anIntArray776=null;
			objectDef.description=null;
			objectDef.hasActions=false;
			objectDef.aBoolean779=true;
			objectDef.anInt781=-1;
			objectDef.anInt783=0;
			objectDef.modifiedModelColors= new int[] { 74, 43117 };
			objectDef.actions=null;
		}
		if (id == 11698) {
			objectDef.aBoolean736=false;
			objectDef.aByte737=0;
			objectDef.anInt738=0;
			objectDef.name=null;
			objectDef.anInt740=128;
			objectDef.aByte742=0;
			objectDef.anInt744=1;
			objectDef.anInt745=0;
			objectDef.anInt746=-1;
			objectDef.originalModelColors=new int[] { 21543, 21547, 45, 7341};
			objectDef.anInt748=128;
			objectDef.configID=-1;
			objectDef.aBoolean751=false;
			objectDef.myId=11698;
			objectDef.aBoolean757=true;
			objectDef.anInt758=-1;
			objectDef.childrenIDs=null;
			objectDef.anInt760=1;
			objectDef.anInt761=1;
			objectDef.aBoolean762=false;
			objectDef.aBoolean764=false;
			objectDef.aBoolean766=false;
			objectDef.aBoolean767=true;
			objectDef.anInt768=0;
			objectDef.aBoolean769=false;
			objectDef.anInt772=128;
			objectDef.anIntArray773= new int[] { 5013};
			objectDef.anInt774=-1;
			objectDef.anInt775=16;
			objectDef.anIntArray776=null;
			objectDef.description=null;
			objectDef.hasActions=false;
			objectDef.aBoolean779=true;
			objectDef.anInt781=473;
			objectDef.anInt783=0;
			objectDef.modifiedModelColors= new int[] { 6817, 6697, 6693, 7580};
			objectDef.actions=null;
		}
	}
	
	
	private static void dumpConfig() {

		File dir = new File("./dump/");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		final DataOutputStream dat;
		try {
			dat = new DataOutputStream(new FileOutputStream(new File(dir, "667loc.dat")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		final DataOutputStream idx;
		try {
			idx = new DataOutputStream(new FileOutputStream(new File(dir, "667loc.idx")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		
		try {

			int size = totalObjects667;


			idx.writeInt(size);
			dat.writeInt(size);

			for (int index = 0; index < size; index++) {

				int start = dat.size();

				ObjectDef obj = forID(index);

				if (obj.newModels != null) {
					dat.writeByte(1);
					dat.writeByte(obj.anIntArray776.length);
					if (obj.newModels.length > 0) {

						for (int tIndex = 0; tIndex < obj.anIntArray776.length; tIndex++) {
							dat.writeByte(obj.anIntArray776[tIndex]);
							dat.writeByte(obj.newModels[tIndex].length);
							for (int i = 0; i < obj.newModels[tIndex].length; i++) {
								dat.writeShort(obj.newModels[tIndex][i]);
							}
						}

					}
				}

				if (obj.name != null && !obj.name.equalsIgnoreCase("null")) {
					dat.writeByte(2);
					dat.write(obj.name.getBytes());
					dat.writeByte(10);
				}

				if (obj.anInt744 != 1) {
					dat.writeByte(14);
					dat.writeByte(obj.anInt744);
				}

				if (obj.anInt761 != 1) {
					dat.writeByte(15);
					dat.writeByte(obj.anInt761);
				}

				if (!obj.aBoolean767) {
					dat.writeByte(17);
				}

				if (!obj.aBoolean757) {
					dat.writeByte(18);
				}

				if (obj.hasActions) {
					dat.writeByte(19);
					dat.writeByte(1);
				}

				if (obj.aBoolean762) {
					dat.writeByte(21);
				}

				if (!obj.aBoolean769) {
					dat.writeByte(22);
				}

				if (obj.aBoolean764) {
					dat.writeByte(23);
				}

				if (obj.anInt781 != -1) {
					dat.writeByte(24);
					dat.writeShort(obj.anInt781);
				}

				if (obj.anInt775 != 16) {
					dat.writeByte(28);
					dat.writeByte(obj.anInt775);
				}

				if (obj.aByte737 != 0) {
					dat.writeByte(29);
					dat.writeByte(obj.aByte737);
				}

				if (obj.aByte742 != 0) {
					dat.writeByte(39);
					dat.writeByte(obj.aByte742);
				}

				if (!ArrayUtils.isEmpty(obj.actions)) {
					for (int i = 0; i < obj.actions.length; i++) {
						if (obj.actions[i] != null) {
							dat.writeByte(i + 30);
							dat.write(obj.actions[i].getBytes());
							dat.writeByte(10);
						}
					}
				}

				if (obj.modifiedModelColors != null || obj.originalModelColors != null) {
					dat.writeByte(40);
					dat.writeByte(obj.modifiedModelColors.length);

					for (int i = 0; i < obj.modifiedModelColors.length; i++) {
						dat.writeShort(obj.modifiedModelColors[i]);
						dat.writeShort(obj.originalModelColors[i]);
					}

				}

				if (obj.modifiedTexture != null || obj.originalTexture != null) {
					dat.writeByte(41);
					dat.writeByte(obj.originalTexture.length);

					for (int i = 0; i < obj.originalTexture.length; i++) {
						dat.writeShort(obj.originalTexture[i]);
						dat.writeShort(obj.modifiedTexture[i]);
					}

				}
				
				if (obj.anInt746 != -1) { // mapfunctions id
					dat.writeByte(60);
					dat.writeShort(obj.anInt746);
				}

				if (obj.aBoolean751) {
					dat.writeByte(62);
				}

				if (!obj.aBoolean779) {
					dat.writeByte(64);
				}

				if (obj.anInt748 != 128) {
					dat.writeByte(65);
					dat.writeShort(obj.anInt748);
				}

				if (obj.anInt772 != 128) {
					dat.writeByte(66);
					dat.writeShort(obj.anInt772);
				}

				if (obj.anInt740 != 128) {
					dat.writeByte(67);
					dat.writeShort(obj.anInt740);
				}

				if (obj.anInt758 != -1) { // map scene id
					dat.writeByte(68);
					dat.writeShort(obj.anInt758);
				}

				if (obj.anInt768 != -1) {
					dat.writeByte(69);
					dat.writeByte(obj.anInt768);
				}

				if (obj.anInt738 != 0) {
					dat.writeByte(70);
					dat.writeShort(obj.anInt738);
				}

				if (obj.anInt745 != 0) {
					dat.writeByte(71);
					dat.writeShort(obj.anInt745);
				}

				if (obj.anInt783 != 0) {
					dat.writeByte(72);
					dat.writeShort(obj.anInt783);
				}

				if (obj.aBoolean736) {
					dat.writeByte(73);
				}

				if (obj.aBoolean766) {
					dat.writeByte(74);
				}

				if (obj.anInt760 == 1) {
					dat.writeByte(75);
					dat.writeByte(obj.anInt760);
				}

				if (obj.anInt774 != -1 || obj.configID != -1 || obj.childrenIDs != null) {
					dat.writeByte(77);
					dat.writeShort(obj.anInt774);
					dat.writeShort(obj.configID);
					dat.writeByte(obj.childrenIDs.length - 1);

					for (int i = 0; i < obj.childrenIDs.length; i++) {
						dat.writeShort(obj.childrenIDs[i]);
					}

				}

//				if (obj.getMapAreaId() != -1) {
//					dat.writeByte(82);
//					dat.writeShort(obj.getMapAreaId());
//				}

				dat.writeByte(0);

				int end = dat.size();

				idx.writeInt(end - start);


			}


			System.out.println("Dumped 667 Object Definitions into 317 format. Size:"+size);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				dat.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				idx.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }

	private static RSBuffer stream830;
	public static int[] streamIndices830;
	
	private static RSBuffer stream667;
	public static int[] streamIndices667;
	
	private static RSBuffer stream525;
	public static int[] streamIndices525;
	
	private static RSBuffer streamOSRS;
	public static int[] streamIndicesOSRS;

	public boolean aBoolean736;
	public byte aByte737;
	public int anInt738; //offset X
	public String name;
	public int anInt740; // model size y
	public static final Model[] aModelArray741s = new Model[4];
	public byte aByte742;
	public int anInt744;
	public int anInt745; //modeloffsetheight
	public int anInt746;
	public int[] originalModelColors;
	public int anInt748; // model size x
	public int configID; //anInt749
	public boolean aBoolean751;
	public static boolean lowMem;
	public static RSBuffer stream;
	public int myId;
	public static int[] streamIndices474;
	public boolean aBoolean757;
	public int anInt758; // mapSceneId
	public int childrenIDs[];
	public int anInt760;
	public int anInt761;
	public boolean aBoolean762;
	public boolean aBoolean764;
	public static Client clientInstance;
	public boolean aBoolean766;
	public boolean aBoolean767;
	public int anInt768; // use object from direction
	public boolean aBoolean769;
	public int anInt772; // model size height
	public int[] anIntArray773;
	public int[][] newModels;
	public int anInt774;
	public int anInt775;
	public byte[] anIntArray776;
	public byte description[];
	public boolean hasActions;
	public boolean aBoolean779;
	public static ObjectCache mruNodes2 = new ObjectCache(30);
	public static ObjectCache recentUse = new ObjectCache(64);
	public int anInt781; // animation?
	public int anInt783; // offsetY
	public int[] modifiedModelColors;
	public static ObjectCache mruNodes1 = new ObjectCache(500);
	public String actions[];
	
	public short[] originalTexture; // these are not inverted like the colors
	public short[] modifiedTexture; // not inverted like pi shit code

    public int clipType;
	
	public ModelTypeEnum modelType = ModelTypeEnum.REGULAR;
	
	public static final int OSRS_START = 100_000;

}
