package com.soulplayps.client.node.object;

import com.soulplayps.client.node.Node;

public final class GameObject extends Node {

	public GameObject() {
		removeTime = -1;
	}

	public int currentIDRequested; //anInt1291
	public int currentFaceRequested; //anInt1292
	public int currentTypeRequested; //anInt1293
	public int removeTime; //anInt1294
	public int plane; //anInt1295
	public int objectType; //anInt1296
	public int tileX; //anInt1297
	public int tileY; //anInt1298
	public int anInt1299;
	public int anInt1300;
	public int anInt1301;
	public int spawnTime; //anInt1302
	public int regionType;
}
