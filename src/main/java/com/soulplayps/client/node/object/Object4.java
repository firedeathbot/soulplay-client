package com.soulplayps.client.node.object;

import com.soulplayps.client.node.animable.Animable;

public final class Object4 extends SceneEntity {
	
	public Object4() {
	}
	
	public int anInt45;
	public int anInt46;
	public int anInt47;
	public Animable aClass30_Sub2_Sub4_48;
	public Animable aClass30_Sub2_Sub4_49;
	public Animable aClass30_Sub2_Sub4_50;
	public int anInt52;
	public int level;
}
