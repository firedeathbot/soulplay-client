package com.soulplayps.client.node.object;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.animable.Animable;
import com.soulplayps.client.node.animable.Animable_Sub5;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.ondemand.OnDemandFetcher;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.unknown.Class11;
import com.soulplayps.client.unknown.Class4;
import com.soulplayps.client.unpack.Flo;
import com.soulplayps.client.unpack.FloHigherRev;
import com.soulplayps.client.unpack.FloOsrs;

public final class ObjectManager {

	public ObjectManager(byte abyte0[][][], int ai[][][]) {
		anInt145 = 99;
		anInt146 = 104;
		anInt147 = 104;
		anIntArrayArrayArray129 = ai;
		aByteArrayArrayArray149 = abyte0;
		osrsTiles = new boolean[4][anInt146][anInt147];
		aByteArrayArrayArray142 = new byte[4][anInt146][anInt147];
		aByteArrayArrayArray130 = new byte[4][anInt146][anInt147];
		aByteArrayArrayArray136 = new byte[4][anInt146][anInt147];
		aByteArrayArrayArray148 = new byte[4][anInt146][anInt147];
		anIntArrayArrayArray135 = new int[4][anInt146 + 1][anInt147 + 1];
		aByteArrayArrayArray134 = new byte[4][anInt146 + 1][anInt147 + 1];
		anIntArrayArray139 = new int[anInt146 + 1][anInt147 + 1];
		anIntArray124 = new int[anInt147];
		anIntArray125 = new int[anInt147];
		anIntArray126 = new int[anInt147];
		anIntArray127 = new int[anInt147];
		anIntArray128 = new int[anInt147];
	}

	private static int method170(int i, int j) {
		int k = i + j * 57;
		k = k << 13 ^ k;
		int l = k * (k * k * 15731 + 0xc0ae5) + 0x5208dd0d & 0x7fffffff;
		return l >> 19 & 0xff;
	}

	public final void method171(Class11 aclass11[],
			WorldController worldController) {
		try {
			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < 104; k++) {
					for (int i1 = 0; i1 < 104; i1++)
						if ((aByteArrayArrayArray149[j][k][i1] & 1) == 1) {
							int k1 = j;
							if ((aByteArrayArrayArray149[1][k][i1] & 2) == 2)
								k1--;
							if (k1 >= 0)
								aclass11[k1].method213(i1, k);
						}

				}

			}
			for (int l = 0; l < 4; l++) {
				byte abyte0[][] = aByteArrayArrayArray134[l];
				byte byte0 = 96;
				char c = '\u0300';
				byte byte1 = -50;
				byte byte2 = -10;
				byte byte3 = -50;
				int j3 = (int) Math.sqrt(byte1 * byte1 + byte2 * byte2 + byte3
						* byte3);
				int l3 = c * j3 >> 8;
				for (int j4 = 1; j4 < anInt147 - 1; j4++) {
					for (int j5 = 1; j5 < anInt146 - 1; j5++) {
						int k6 = anIntArrayArrayArray129[l][j5 + 1][j4]
								- anIntArrayArrayArray129[l][j5 - 1][j4];
						int l7 = anIntArrayArrayArray129[l][j5][j4 + 1]
								- anIntArrayArrayArray129[l][j5][j4 - 1];
						int j9 = (int) Math.sqrt(k6 * k6 + 0x10000 + l7 * l7);
						int k12 = (k6 << 8) / j9;
						int l13 = 0x10000 / j9;
						int j15 = (l7 << 8) / j9;
						int j16 = byte0
								+ (byte1 * k12 + byte2 * l13 + byte3 * j15)
								/ l3;
						int j17 = (abyte0[j5 - 1][j4] >> 2)
								+ (abyte0[j5 + 1][j4] >> 3)
								+ (abyte0[j5][j4 - 1] >> 2)
								+ (abyte0[j5][j4 + 1] >> 3)
								+ (abyte0[j5][j4] >> 1);
						anIntArrayArray139[j5][j4] = j16 - j17;
					}

				}

				for (int k5 = 0; k5 < anInt147; k5++) {
					anIntArray124[k5] = 0;
					anIntArray125[k5] = 0;
					anIntArray126[k5] = 0;
					anIntArray127[k5] = 0;
					anIntArray128[k5] = 0;
				}

				for (int l6 = -5; l6 < anInt146 + 5; l6++) {
					for (int i8 = 0; i8 < anInt147; i8++) {
						int k9 = l6 + 5;
						if (k9 >= 0 && k9 < anInt146) {
							int l12 = aByteArrayArrayArray142[l][k9][i8] & 0xff;
							if (l12 > 0) {
								int int397;
								int int395;
								int int396;
								int int398;
								if (osrsTiles[l][k9][i8]) {
									FloOsrs flo = FloOsrs.cacheUnderlay[l12 - 1];
									int397 = flo.anInt397;
									int395 = flo.anInt395;
									int396 = flo.anInt396;
									int398 = flo.anInt398;
								} else {
									Flo flo = Flo.cache[l12 - 1];
									int397 = flo.anInt397;
									int395 = flo.anInt395;
									int396 = flo.anInt396;
									int398 = flo.anInt398;
								}

								anIntArray124[i8] += int397;
								anIntArray125[i8] += int395;
								anIntArray126[i8] += int396;
								anIntArray127[i8] += int398;
								anIntArray128[i8]++;
							}
						}
						int i13 = l6 - 5;
						if (i13 >= 0 && i13 < anInt146) {
							int i14 = aByteArrayArrayArray142[l][i13][i8] & 0xff;
							if (i14 > 0) {
								int int397;
								int int395;
								int int396;
								int int398;
								if (osrsTiles[l][i13][i8]) {
									FloOsrs flo = FloOsrs.cacheUnderlay[i14 - 1];
									int397 = flo.anInt397;
									int395 = flo.anInt395;
									int396 = flo.anInt396;
									int398 = flo.anInt398;
								} else {
									Flo flo = Flo.cache[i14 - 1];
									int397 = flo.anInt397;
									int395 = flo.anInt395;
									int396 = flo.anInt396;
									int398 = flo.anInt398;
								}

								anIntArray124[i8] -= int397;
								anIntArray125[i8] -= int395;
								anIntArray126[i8] -= int396;
								anIntArray127[i8] -= int398;
								anIntArray128[i8]--;
							}
						}
					}

					if (l6 >= 1 && l6 < anInt146 - 1) {
						int l9 = 0;
						int j13 = 0;
						int j14 = 0;
						int k15 = 0;
						int k16 = 0;
						for (int k17 = -5; k17 < anInt147 + 5; k17++) {
							int j18 = k17 + 5;
							if (j18 >= 0 && j18 < anInt147) {
								l9 += anIntArray124[j18];
								j13 += anIntArray125[j18];
								j14 += anIntArray126[j18];
								k15 += anIntArray127[j18];
								k16 += anIntArray128[j18];
							}
							int k18 = k17 - 5;
							if (k18 >= 0 && k18 < anInt147) {
								l9 -= anIntArray124[k18];
								j13 -= anIntArray125[k18];
								j14 -= anIntArray126[k18];
								k15 -= anIntArray127[k18];
								k16 -= anIntArray128[k18];
							}
							if (k17 >= 1
									&& k17 < anInt147 - 1
									&& (!lowMem
											|| (aByteArrayArrayArray149[0][l6][k17] & 2) != 0 || (aByteArrayArrayArray149[l][l6][k17] & 0x10) == 0)) {
								if (l < anInt145)
									anInt145 = l;
								int l18 = aByteArrayArrayArray142[l][l6][k17] & 0xff;
								int i19 = aByteArrayArrayArray130[l][l6][k17] & 0xff;
								if (l18 > 0 || i19 > 0) {
									int j19 = anIntArrayArrayArray129[l][l6][k17];
									int k19 = anIntArrayArrayArray129[l][l6 + 1][k17];
									int l19 = anIntArrayArrayArray129[l][l6 + 1][k17 + 1];
									int i20 = anIntArrayArrayArray129[l][l6][k17 + 1];
									int j20 = anIntArrayArray139[l6][k17];
									int k20 = anIntArrayArray139[l6 + 1][k17];
									int l20 = anIntArrayArray139[l6 + 1][k17 + 1];
									int i21 = anIntArrayArray139[l6][k17 + 1];
									int j21 = -1;
									int k21 = -1;
									if (l18 > 0) {
										int l21 = (l9 * 256) / k15;
										int j22 = j13 / k16;
										int l22 = j14 / k16;
										j21 = method177(l21, j22, l22);
										/*
										 * l21 = l21 + anInt123 & 0xff; l22 +=
										 * anInt133; if(l22 < 0) l22 = 0; else
										 * if(l22 > 255) l22 = 255;
										 */
										k21 = method177(l21, j22, l22);
									}
									if (l > 0) {
										boolean flag = true;
										if (l18 == 0
												&& aByteArrayArrayArray136[l][l6][k17] != 0)
											flag = false;
										if (i19 > 0) {
											int id = i19 - 1;
											if (osrsTiles[l][l6][k17]) {
												if (!FloOsrs.cacheOverlay[id].aBoolean393) {
													flag = false;
												}
											} else {
												if (id < Flo.cache.length && !Flo.cache[id].aBoolean393) {
													flag = false;
												}
											}
										}
										if (flag && j19 == k19 && j19 == l19
												&& j19 == i20)
											anIntArrayArrayArray135[l][l6][k17] |= 0x924;
									}
									int i22 = 0;
									if (j21 != -1)
										i22 = Rasterizer.anIntArray1482[method187(
												k21, 96)];
									if (i19 == 0) {
										worldController.method279(l, l6, k17, 0, 0,
												-1, 154, j19, k19, l19, i20,
												method187(j21, j20),
												method187(j21, k20),
												method187(j21, l20),
												method187(j21, i21), 0, 0, 0, 0,
												i22, 0, false, j21);
									} else {
										int k22 = aByteArrayArrayArray136[l][l6][k17] + 1;
										byte byte4 = aByteArrayArrayArray148[l][l6][k17];

										int floorTexture = -1;
										int j23;
										int color = -1;
										int mapTexture;
										if (osrsTiles[l][l6][k17]) {
											FloOsrs flo = FloOsrs.cacheOverlay[i19 - 1];
											j23 = method177(flo.anInt394, flo.anInt395, flo.anInt396);
											color = method185(j23, 96);
											mapTexture = Rasterizer.anIntArray1482[color];
											floorTexture = flo.anInt391;
											if (flo.anInt390 == 0xff00ff) {
												mapTexture = 0;
												j23 = -2;
												floorTexture = -1;
											} else if (floorTexture >= 0 && floorTexture <= 50) {
												mapTexture = Rasterizer.method369(floorTexture);
												j23 = -1;
											}
											
											if (i19 - 1 == 151 && flo.anInt390 == 0xff00ff) { // Lava
												mapTexture = 0xFBDC26;
											}
										} else {
											if (i19 - 1 > FloHigherRev.cache.length) {
												i19 = FloHigherRev.cache.length;
											}
											FloHigherRev flo_2 = FloHigherRev.cache[i19 - 1];
											floorTexture = flo_2.textureId;
											if (floorTexture >= 0 && floorTexture <= 50) {
												mapTexture = Rasterizer.method369(floorTexture);
												j23 = -1;
											} else if (flo_2.rgb == 0xff00ff) {
												mapTexture = 0;
												j23 = -2;
												floorTexture = -1;
											} else if (flo_2.rgb == 0x333333) {
												color = method185(flo_2.anInt399, 96);
												mapTexture = Rasterizer.anIntArray1482[color];
												j23 = -2;
												floorTexture = -1;
											} else {
												j23 = method177(flo_2.anInt394, flo_2.anInt395, flo_2.anInt396);
												color = method185(flo_2.anInt399, 96);
												mapTexture = Rasterizer.anIntArray1482[color];
											}
											
											if (i19 - 1 == 151 && flo_2.rgb == 0xff00ff) { // Lava
												mapTexture = 0xFBDC26;
											}
											if (i19 - 1 == 54) {
												mapTexture = flo_2.rgb = 0x8B8B83;
												j23 = -2;
												floorTexture = -1;
											} else if (i19 - 1 == 111) {
												mapTexture = Rasterizer.method369(1);
												j23 = -1;
												floorTexture = 1;
											} else if ((i19 - 1) == 63) {//edge bridge
												mapTexture = flo_2.rgb = 0x4F4F4F;
												j23 = -2;
												floorTexture = -1;
											}

											if (j23 == 111) { // Water
												mapTexture = Rasterizer.method369(1);
												j23 = -1;
												floorTexture = 1;
											} else if (j23 == 53) { // Blue at duel
												// arena
												mapTexture = flo_2.rgb = 0xAA9166;
												floorTexture = -1;
											} else if (j23 == 52) { // Green in duel
												// arena
												mapTexture = flo_2.rgb = 0x736836;
												floorTexture = -1;
											} else if (j23 == 125) { // Roofs, duel
												// arena
												mapTexture = flo_2.rgb = 0xAA9166;
												j23 = -1;
												floorTexture = 32;
											} else if (j23 == 135) { // Water at duel
												// arena
												mapTexture = Rasterizer.method369(1);
												j23 = -2;
												floorTexture = -1;
											} else if (j23 == 6041) { // Al kharid
												// floors
												mapTexture = flo_2.rgb = 0xAA9166;
												j23 = -1;
												floorTexture = 32;
											} else if (j23 == 63) { // Seer's court
												// stairs
												mapTexture = flo_2.rgb = 0x767676;
												j23 = -2;
												floorTexture = -1;
											} else if (j23 == 177) { // Castle Wars,
												// lobby floor
												mapTexture = flo_2.rgb = 0x4D4D4D;
												j23 = method177(0, 0, 55);
												floorTexture = -1;
											} else if (j23 == 72) { // Cliffside at
												// ogres
												mapTexture = flo_2.rgb = 0x483B21;
												j23 = method177(25, 146, 24);
											} else if (j23 == 6363 || j23 == 549) { // Dirt
												// banks,
												// etc
												mapTexture = 0x483B21;
												j23 = method177(25, 146, 24);
											} else if (j23 == 127) {
												mapTexture = Rasterizer.method369(25);
												j23 = -1;
												floorTexture = 25;
											} else if (j23 == 6813) {//Castle wars road
												mapTexture = flo_2.rgb = 7757611;
												floorTexture = 154;//154
											}
											if (floorTexture == 137) {
												mapTexture = Rasterizer.method369(25);
												floorTexture = 25;
												j23 = -1;
											}
										}

										worldController.method279(l, l6, k17, k22,
												byte4, floorTexture, 154, j19, k19,
												l19, i20, method187(j21, j20),
												method187(j21, k20),
												method187(j21, l20),
												method187(j21, i21),
												method185(j23, j20),
												method185(j23, k20),
												method185(j23, l20),
												method185(j23, i21), i22,
												mapTexture, floorTexture >= 0
														&& floorTexture <= 50, color);
		                                
										
										
									}
								}
							}
						}

					}
				}

				for (int j8 = 1; j8 < anInt147 - 1; j8++) {
					for (int i10 = 1; i10 < anInt146 - 1; i10++)
						worldController.method278(l, i10, j8,
								method182(j8, l, i10));

				}
			}

			worldController.method305(-10, -50, -50);
			for (int j1 = 0; j1 < anInt146; j1++) {
				for (int l1 = 0; l1 < anInt147; l1++)
					if ((aByteArrayArrayArray149[1][j1][l1] & 2) == 2)
						worldController.method276(l1, j1);

			}

			int i2 = 1;
			int j2 = 2;
			int k2 = 4;
			for (int l2 = 0; l2 < 4; l2++) {
				if (l2 > 0) {
					i2 <<= 3;
					j2 <<= 3;
					k2 <<= 3;
				}
				for (int i3 = 0; i3 <= l2; i3++) {
					for (int k3 = 0; k3 <= anInt147; k3++) {
						for (int i4 = 0; i4 <= anInt146; i4++) {
							if ((anIntArrayArrayArray135[i3][i4][k3] & i2) != 0) {
								int k4 = k3;
								int l5 = k3;
								int i7 = i3;
								int k8 = i3;
								for (; k4 > 0
										&& (anIntArrayArrayArray135[i3][i4][k4 - 1] & i2) != 0; k4--)
									;
								for (; l5 < anInt147
										&& (anIntArrayArrayArray135[i3][i4][l5 + 1] & i2) != 0; l5++)
									;
								label0: for (; i7 > 0; i7--) {
									for (int j10 = k4; j10 <= l5; j10++)
										if ((anIntArrayArrayArray135[i7 - 1][i4][j10] & i2) == 0)
											break label0;

								}

								label1: for (; k8 < l2; k8++) {
									for (int k10 = k4; k10 <= l5; k10++)
										if ((anIntArrayArrayArray135[k8 + 1][i4][k10] & i2) == 0)
											break label1;

								}

								int l10 = ((k8 + 1) - i7) * ((l5 - k4) + 1);
								if (l10 >= 8) {
									char c1 = '\360';
									int k14 = anIntArrayArrayArray129[k8][i4][k4]
											- c1;
									int l15 = anIntArrayArrayArray129[i7][i4][k4];
									WorldController.method277(l2, i4 * 128,
											l15, i4 * 128, l5 * 128 + 128, k14,
											k4 * 128, 1);
									for (int l16 = i7; l16 <= k8; l16++) {
										for (int l17 = k4; l17 <= l5; l17++)
											anIntArrayArrayArray135[l16][i4][l17] &= ~i2;

									}

								}
							}
							if ((anIntArrayArrayArray135[i3][i4][k3] & j2) != 0) {
								int l4 = i4;
								int i6 = i4;
								int j7 = i3;
								int l8 = i3;
								for (; l4 > 0
										&& (anIntArrayArrayArray135[i3][l4 - 1][k3] & j2) != 0; l4--)
									;
								for (; i6 < anInt146
										&& (anIntArrayArrayArray135[i3][i6 + 1][k3] & j2) != 0; i6++)
									;
								label2: for (; j7 > 0; j7--) {
									for (int i11 = l4; i11 <= i6; i11++)
										if ((anIntArrayArrayArray135[j7 - 1][i11][k3] & j2) == 0)
											break label2;

								}

								label3: for (; l8 < l2; l8++) {
									for (int j11 = l4; j11 <= i6; j11++)
										if ((anIntArrayArrayArray135[l8 + 1][j11][k3] & j2) == 0)
											break label3;

								}

								int k11 = ((l8 + 1) - j7) * ((i6 - l4) + 1);
								if (k11 >= 8) {
									char c2 = '\360';
									int l14 = anIntArrayArrayArray129[l8][l4][k3]
											- c2;
									int i16 = anIntArrayArrayArray129[j7][l4][k3];
									WorldController.method277(l2, l4 * 128,
											i16, i6 * 128 + 128, k3 * 128, l14,
											k3 * 128, 2);
									for (int i17 = j7; i17 <= l8; i17++) {
										for (int i18 = l4; i18 <= i6; i18++)
											anIntArrayArrayArray135[i17][i18][k3] &= ~j2;

									}

								}
							}
							if ((anIntArrayArrayArray135[i3][i4][k3] & k2) != 0) {
								int i5 = i4;
								int j6 = i4;
								int k7 = k3;
								int i9 = k3;
								for (; k7 > 0
										&& (anIntArrayArrayArray135[i3][i4][k7 - 1] & k2) != 0; k7--)
									;
								for (; i9 < anInt147
										&& (anIntArrayArrayArray135[i3][i4][i9 + 1] & k2) != 0; i9++)
									;
								label4: for (; i5 > 0; i5--) {
									for (int l11 = k7; l11 <= i9; l11++)
										if ((anIntArrayArrayArray135[i3][i5 - 1][l11] & k2) == 0)
											break label4;

								}

								label5: for (; j6 < anInt146; j6++) {
									for (int i12 = k7; i12 <= i9; i12++)
										if ((anIntArrayArrayArray135[i3][j6 + 1][i12] & k2) == 0)
											break label5;

								}

								if (((j6 - i5) + 1) * ((i9 - k7) + 1) >= 4) {
									int j12 = anIntArrayArrayArray129[i3][i5][k7];
									WorldController.method277(l2, i5 * 128,
											j12, j6 * 128 + 128,
											i9 * 128 + 128, j12, k7 * 128, 4);
									for (int k13 = i5; k13 <= j6; k13++) {
										for (int i15 = k7; i15 <= i9; i15++)
											anIntArrayArrayArray135[i3][k13][i15] &= ~k2;

									}

								}
							}
						}

					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static final int hslToRgb(int var0, int var1, int var2) {
		if (var2 > 179) {
			var1 /= 2;
		}


		if (var2 > 192) {
			var1 /= 2;
		}


		if (var2 > 217) {
			var1 /= 2;
		}


		if (var2 > 243) {
			var1 /= 2;
		}


		int var3 = var2 / 2 + (var0 / 4 << 10) + (var1 / 32 << 7);
		return var3;
	}




	static final int poesy(int var0, int var1) {
		if (var0 == -2) {
			return 12345678;
		} else if (var0 == -1) {
			if (var1 < 2) {
				var1 = 2;
			} else if (var1 > 126) {
				var1 = 126;
			}


			return var1;
		} else {
			var1 = var1 * (var0 & 127) / 128;
			if (var1 < 2) {
				var1 = 2;
			} else if (var1 > 126) {
				var1 = 126;
			}


			return var1 + (var0 & '\uff80');
		}
	}

	private static int method172(int i, int j) {
		int k = (method176(i + 45365, j + 0x16713, 4) - 128)
				+ (method176(i + 10294, j + 37821, 2) - 128 >> 1)
				+ (method176(i, j, 1) - 128 >> 2);
		k = (int) (k * 0.29999999999999999D) + 35;
		if (k < 10)
			k = 10;
		else if (k > 60)
			k = 60;
		return k;
	}

	public static void method173(RSBuffer stream, OnDemandFetcher class42_sub1) {
		label0: {
			int i = -1;
			do {
				int j = stream.readUSmart2();
				if (j == 0)
					break label0;
				i += j;
				ObjectDef class46 = ObjectDef.forID(i);
				class46.method574(class42_sub1);
				do {
					int k = stream.readUSmart();
					if (k == 0)
						break;
					stream.readUnsignedByte();
				} while (true);
			} while (true);
		}
	}

	public final void method174(int startZ, int lenZ, int lenX, int startX) {
		for (int z = startZ; z <= startZ + lenZ; z++) {
			for (int x = startX; x <= startX + lenX; x++)
				if (x >= 0 && x < anInt146 && z >= 0 && z < anInt147) {
					aByteArrayArrayArray134[0][x][z] = 127;
					if (x == startX && x > 0)
						anIntArrayArrayArray129[0][x][z] = anIntArrayArrayArray129[0][x - 1][z];
					if (x == startX + lenX && x < anInt146 - 1)
						anIntArrayArrayArray129[0][x][z] = anIntArrayArrayArray129[0][x + 1][z];
					if (z == startZ && z > 0)
						anIntArrayArrayArray129[0][x][z] = anIntArrayArrayArray129[0][x][z - 1];
					if (z == startZ + lenZ && z < anInt147 - 1)
						anIntArrayArrayArray129[0][x][z] = anIntArrayArrayArray129[0][x][z + 1];
				}

		}
	}

	private void method175(int obj_y, WorldController worldController,
			Class11 class11, int objType, int level, int obj_x, int objectId, int orientation, ObjectDef class46, int regionType) {
		try {
		if (lowMem && (aByteArrayArrayArray149[0][obj_x][obj_y] & 2) == 0) {
			if ((aByteArrayArrayArray149[level][obj_x][obj_y] & 0x10) != 0)
				return;
			//if (method182(obj_y, level, obj_x) != anInt131)
			//	return;
		}
		if (level < anInt145)
			anInt145 = level;
		
//		if (i1 == 12010) { // cheaphax lava fix
//			i1 = 33286;
//		}
		
		if (objectId == 733 && objType == 10) { // spider web cheaphax
			objType = 0;
		}

		int rot_size_x;
		int rot_size_y;
		if (orientation == 1 || orientation == 3) {
			rot_size_x = class46.anInt761;
			rot_size_y = class46.anInt744;
		} else {
			rot_size_x = class46.anInt744;
			rot_size_y = class46.anInt761;
		}
		
		int calc_x, calc_x_2;
		if (rot_size_x + obj_x <= 104) {
			calc_x = (rot_size_x >> 1) + obj_x;
			calc_x_2 = obj_x + (rot_size_x + 1 >> 1);
		} else {
			calc_x_2 = obj_x - -1;
			calc_x = obj_x;
		}

		int calc_z, calc_z_2;
		if (obj_y + rot_size_y <= 104) {
			calc_z = (1 + rot_size_y >> 1) + obj_y;
			calc_z_2 = (rot_size_y >> 1) + obj_y;
		} else {
			calc_z = 1 + obj_y;
			calc_z_2 = obj_y;
		}
		
		final int[][] y_map = anIntArrayArrayArray129[level];
		final int k2 = y_map[calc_x][calc_z] + y_map[calc_x_2][calc_z_2] + y_map[calc_x][calc_z_2] + y_map[calc_x_2][calc_z] >> 2;
		
		int k1 = y_map[obj_x][obj_y];
		int l1 = y_map[obj_x + 1][obj_y];
		int i2 = y_map[obj_x + 1][obj_y + 1];
		int j2 = y_map[obj_x][obj_y + 1];

		long l2 = obj_x | (obj_y << 7) | (objType << 14) | (orientation << 20) | 0x40000000;
		if (!class46.hasActions) {
			l2 |= ~0x7fffffffffffffffL;
		}
		/*if (class46.supportItems == 1) {//TODO
			i3 |= 0x400000L;
		}*/
		l2 |= (long) objectId << 32;
		if (objType == 22) {
			if (drawGroundDecor || class46.hasActions || class46.aBoolean736) {
				Object obj;
				if (class46.anInt781 == -1 && class46.childrenIDs == null)
					obj = class46.method578(22, orientation, k1, l1, i2, j2);
				else
					obj = new Animable_Sub5(objectId, orientation, 22, l1, i2, k1, j2,
							class46.anInt781, true, regionType);
				worldController.method280(level, k2, obj_y, ((Animable) (obj)), l2, obj_x);
				if (class46.aBoolean767 && class46.hasActions && class11 != null)
					class11.method213(obj_y, obj_x);
				return;
			}
		}
		if (objType == 10 || objType == 11) {
			Object obj1;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj1 = class46.method578(10, orientation, k1, l1, i2, j2);
			else
				obj1 = new Animable_Sub5(objectId, orientation, 10, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			if (obj1 != null) {
				int i5 = 0;
				if (objType == 11)
					i5 += 256;
				if (worldController.method284(l2, k2, rot_size_y, ((Animable) (obj1)), rot_size_x, level, i5, obj_y, obj_x) && class46.aBoolean779) {
					Model model;
					if (obj1 instanceof Model)
						model = (Model) obj1;
					else
						model = class46.method578(10, orientation, k1, l1, i2, j2);
					if (model != null) {
						for (int j5 = 0; j5 <= rot_size_x; j5++) {
							for (int k5 = 0; k5 <= rot_size_y; k5++) {
								int l5 = model.anInt1650 / 4;
								if (l5 > 30)
									l5 = 30;
								if (l5 > aByteArrayArrayArray134[level][obj_x + j5][obj_y
										+ k5])
									aByteArrayArrayArray134[level][obj_x + j5][obj_y + k5] = (byte) l5;
							}

						}

					}
				}
			}
			if (class46.aBoolean767 && class11 != null)
				class11.method212(class46.aBoolean757, class46.anInt744,
						class46.anInt761, obj_x, obj_y, orientation);
			return;
		}
		if (objType >= 12) {
			Object obj2;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj2 = class46.method578(objType, orientation, k1, l1, i2, j2);
			else
				obj2 = new Animable_Sub5(objectId, orientation, objType, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method284(l2, k2, 1, ((Animable) (obj2)), 1, level,
					0, obj_y, obj_x);
			if (objType >= 12 && objType <= 17 && objType != 13 && level > 0)
				anIntArrayArrayArray135[level][obj_x][obj_y] |= 0x924;
			if (class46.aBoolean767 && class11 != null)
				class11.method212(class46.aBoolean757, class46.anInt744,
						class46.anInt761, obj_x, obj_y, orientation);
			return;
		}
		if (objType == 0) {
			Object obj3;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj3 = class46.method578(0, orientation, k1, l1, i2, j2);
			else
				obj3 = new Animable_Sub5(objectId, orientation, 0, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method282(anIntArray152[orientation], ((Animable) (obj3)),
					l2, obj_y, obj_x, null, k2, 0, level);
			if (orientation == 0) {
				if (class46.aBoolean779) {
					aByteArrayArrayArray134[level][obj_x][obj_y] = 50;
					aByteArrayArrayArray134[level][obj_x][obj_y + 1] = 50;
				}
				if (class46.aBoolean764)
					anIntArrayArrayArray135[level][obj_x][obj_y] |= 0x249;
			} else if (orientation == 1) {
				if (class46.aBoolean779) {
					aByteArrayArrayArray134[level][obj_x][obj_y + 1] = 50;
					aByteArrayArrayArray134[level][obj_x + 1][obj_y + 1] = 50;
				}
				if (class46.aBoolean764)
					anIntArrayArrayArray135[level][obj_x][obj_y + 1] |= 0x492;
			} else if (orientation == 2) {
				if (class46.aBoolean779) {
					aByteArrayArrayArray134[level][obj_x + 1][obj_y] = 50;
					aByteArrayArrayArray134[level][obj_x + 1][obj_y + 1] = 50;
				}
				if (class46.aBoolean764)
					anIntArrayArrayArray135[level][obj_x + 1][obj_y] |= 0x249;
			} else if (orientation == 3) {
				if (class46.aBoolean779) {
					aByteArrayArrayArray134[level][obj_x][obj_y] = 50;
					aByteArrayArrayArray134[level][obj_x + 1][obj_y] = 50;
				}
				if (class46.aBoolean764)
					anIntArrayArrayArray135[level][obj_x][obj_y] |= 0x492;
			}
			if (class46.aBoolean767 && class11 != null)
				class11.method211(obj_y, orientation, obj_x, objType, class46.aBoolean757);
			if (class46.anInt775 != 16)
				worldController.method290(obj_y, class46.anInt775, obj_x, level);
			return;
		}
		if (objType == 1) {
			Object obj4;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj4 = class46.method578(1, orientation, k1, l1, i2, j2);
			else
				obj4 = new Animable_Sub5(objectId, orientation, 1, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method282(anIntArray140[orientation], ((Animable) (obj4)),
					l2, obj_y, obj_x, null, k2, 0, level);
			if (class46.aBoolean779)
				if (orientation == 0)
					aByteArrayArrayArray134[level][obj_x][obj_y + 1] = 50;
				else if (orientation == 1)
					aByteArrayArrayArray134[level][obj_x + 1][obj_y + 1] = 50;
				else if (orientation == 2)
					aByteArrayArrayArray134[level][obj_x + 1][obj_y] = 50;
				else if (orientation == 3)
					aByteArrayArrayArray134[level][obj_x][obj_y] = 50;
			if (class46.aBoolean767 && class11 != null)
				class11.method211(obj_y, orientation, obj_x, objType, class46.aBoolean757);
			return;
		}
		if (objType == 2) {
			int i3 = orientation + 1 & 3;
			Object obj11;
			Object obj12;
			if (class46.anInt781 == -1 && class46.childrenIDs == null) {
				obj11 = class46.method578(2, 4 + orientation, k1, l1, i2, j2);
				obj12 = class46.method578(2, i3, k1, l1, i2, j2);
			} else {
				obj11 = new Animable_Sub5(objectId, 4 + orientation, 2, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
				obj12 = new Animable_Sub5(objectId, i3, 2, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			}
			worldController.method282(anIntArray152[orientation], ((Animable) (obj11)),
					l2, obj_y, obj_x, ((Animable) (obj12)), k2, anIntArray152[i3],
					level);
			if (class46.aBoolean764)
				if (orientation == 0) {
					anIntArrayArrayArray135[level][obj_x][obj_y] |= 0x249;
					anIntArrayArrayArray135[level][obj_x][obj_y + 1] |= 0x492;
				} else if (orientation == 1) {
					anIntArrayArrayArray135[level][obj_x][obj_y + 1] |= 0x492;
					anIntArrayArrayArray135[level][obj_x + 1][obj_y] |= 0x249;
				} else if (orientation == 2) {
					anIntArrayArrayArray135[level][obj_x + 1][obj_y] |= 0x249;
					anIntArrayArrayArray135[level][obj_x][obj_y] |= 0x492;
				} else if (orientation == 3) {
					anIntArrayArrayArray135[level][obj_x][obj_y] |= 0x492;
					anIntArrayArrayArray135[level][obj_x][obj_y] |= 0x249;
				}
			if (class46.aBoolean767 && class11 != null)
				class11.method211(obj_y, orientation, obj_x, objType, class46.aBoolean757);
			if (class46.anInt775 != 16)
				worldController.method290(obj_y, class46.anInt775, obj_x, level);
			return;
		}
		if (objType == 3) {
			Object obj5;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj5 = class46.method578(3, orientation, k1, l1, i2, j2);
			else
				obj5 = new Animable_Sub5(objectId, orientation, 3, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method282(anIntArray140[orientation], ((Animable) (obj5)),
					l2, obj_y, obj_x, null, k2, 0, level);
			if (class46.aBoolean779)
				if (orientation == 0)
					aByteArrayArrayArray134[level][obj_x][obj_y + 1] = 50;
				else if (orientation == 1)
					aByteArrayArrayArray134[level][obj_x + 1][obj_y + 1] = 50;
				else if (orientation == 2)
					aByteArrayArrayArray134[level][obj_x + 1][obj_y] = 50;
				else if (orientation == 3)
					aByteArrayArrayArray134[level][obj_x][obj_y] = 50;
			if (class46.aBoolean767 && class11 != null)
				class11.method211(obj_y, orientation, obj_x, objType, class46.aBoolean757);
			return;
		}
		if (objType == 9) {
			Object obj6;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj6 = class46.method578(objType, orientation, k1, l1, i2, j2);
			else
				obj6 = new Animable_Sub5(objectId, orientation, objType, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method284(l2, k2, 1, ((Animable) (obj6)), 1, level,
					0, obj_y, obj_x);
			if (class46.aBoolean767 && class11 != null)
				class11.method212(class46.aBoolean757, class46.anInt744,
						class46.anInt761, obj_x, obj_y, orientation);
			return;
		}
		if (class46.aBoolean762)
			if (orientation == 1) {
				int j3 = j2;
				j2 = i2;
				i2 = l1;
				l1 = k1;
				k1 = j3;
			} else if (orientation == 2) {
				int k3 = j2;
				j2 = l1;
				l1 = k3;
				k3 = i2;
				i2 = k1;
				k1 = k3;
			} else if (orientation == 3) {
				int l3 = j2;
				j2 = k1;
				k1 = l1;
				l1 = i2;
				i2 = l3;
			}
		if (objType == 4) {
			Object obj7;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj7 = class46.method578(4, 0, k1, l1, i2, j2);
			else
				obj7 = new Animable_Sub5(objectId, 0, 4, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method283(l2, obj_y, orientation * 512, level, 0, k2,
					((Animable) (obj7)), obj_x, 0, anIntArray152[orientation]);
			return;
		}
		if (objType == 5) {
			int i4 = 16;
			long k4 = worldController.method300(level, obj_x, obj_y);
			if (k4 != 0L) {
				i4 = ObjectDef.forID((int) (k4 >>> 32) & 0x7fffffff).anInt775;
			}

			Object obj13;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj13 = class46.method578(4, 0, k1, l1, i2, j2);
			else
				obj13 = new Animable_Sub5(objectId, 0, 4, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method283(l2, obj_y, orientation * 512, level, anIntArray137[orientation]
					* i4, k2, ((Animable) (obj13)), obj_x, anIntArray144[orientation]
					* i4, anIntArray152[orientation]);
			return;
		}
		if (objType == 6) {
			Object obj8;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj8 = class46.method578(4, 0, k1, l1, i2, j2);
			else
				obj8 = new Animable_Sub5(objectId, 0, 4, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method283(l2, obj_y, orientation, level, 0, k2, ((Animable) (obj8)),
					obj_x, 0, 256);
			return;
		}
		if (objType == 7) {
			Object obj9;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj9 = class46.method578(4, 0, k1, l1, i2, j2);
			else
				obj9 = new Animable_Sub5(objectId, 0, 4, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method283(l2, obj_y, orientation, level, 0, k2, ((Animable) (obj9)),
					obj_x, 0, 512);
			return;
		}
		if (objType == 8) {
			Object obj10;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj10 = class46.method578(4, 0, k1, l1, i2, j2);
			else
				obj10 = new Animable_Sub5(objectId, 0, 4, l1, i2, k1, j2,
						class46.anInt781, true, regionType);
			worldController.method283(l2, obj_y, orientation, level, 0, k2,
					((Animable) (obj10)), obj_x, 0, 768);
		}
		} catch(Exception e) {
			
		}
	}

	private static int method176(int i, int j, int k) {
		int l = i / k;
		int i1 = i & k - 1;
		int j1 = j / k;
		int k1 = j & k - 1;
		int l1 = method186(l, j1);
		int i2 = method186(l + 1, j1);
		int j2 = method186(l, j1 + 1);
		int k2 = method186(l + 1, j1 + 1);
		int l2 = method184(l1, i2, i1, k);
		int i3 = method184(j2, k2, i1, k);
		return method184(l2, i3, k1, k);
	}

	private int method177(int i, int j, int k) {
		if (k > 179)
			j /= 2;
		if (k > 192)
			j /= 2;
		if (k > 217)
			j /= 2;
		if (k > 243)
			j /= 2;
		return (i / 4 << 10) + (j / 32 << 7) + k / 2;
	}

	public static boolean method178(int i, int j, int regionType) {
		ObjectDef class46 = getObjectDef(i, regionType);//ObjectDef.forID(i);
		if (j == 11)
			j = 10;
		if (j >= 5 && j <= 8)
			j = 4;
		return class46.method577(j);
	}

	public final void method179(int i, int j, Class11 aclass11[], int l,
			int i1, byte abyte0[], int j1, int k1, int l1) {
		for (int i2 = 0; i2 < 8; i2++) {
			for (int j2 = 0; j2 < 8; j2++)
				if (l + i2 > 0 && l + i2 < 103 && l1 + j2 > 0 && l1 + j2 < 103)
					aclass11[k1].anIntArrayArray294[l + i2][l1 + j2] &= 0xfeffffff;

		}
		RSBuffer stream = new RSBuffer(abyte0);
		for (int l2 = 0; l2 < 4; l2++) {
			for (int i3 = 0; i3 < 64; i3++) {
				for (int j3 = 0; j3 < 64; j3++)
					if (l2 == i && i3 >= i1 && i3 < i1 + 8 && j3 >= j1
							&& j3 < j1 + 8)
						method181(l1 + Class4.method156(j3 & 7, j, i3 & 7), 0,
								stream,
								l + Class4.method155(j, j3 & 7, i3 & 7), k1, j,
								0, false);
					else
						method181(-1, 0, stream, -1, 0, 0, 0, false);

			}

		}

	}

	public final void method180(byte abyte0[], int i, int j, int k, int l,
			Class11 aclass11[], boolean inOsrsRegion) {
		for (int i1 = 0; i1 < 4; i1++) {
			for (int j1 = 0; j1 < 64; j1++) {
				for (int k1 = 0; k1 < 64; k1++)
					if (j + j1 > 0 && j + j1 < 103 && i + k1 > 0
							&& i + k1 < 103)
						aclass11[i1].anIntArrayArray294[j + j1][i + k1] &= 0xfeffffff;

			}

		}

		RSBuffer stream = new RSBuffer(abyte0);
		for (int l1 = 0; l1 < 4; l1++) {
			for (int i2 = 0; i2 < 64; i2++) {
				for (int j2 = 0; j2 < 64; j2++)
					method181(j2 + i, l, stream, i2 + j, l1, 0, k, inOsrsRegion);

			}

		}
	}

	private void method181(int i, int j, RSBuffer stream, int k, int l, int i1,
                           int k1, boolean inOsrsRegion) {
		try {
			if (k >= 0 && k < 104 && i >= 0 && i < 104) {
				aByteArrayArrayArray149[l][k][i] = 0;
				osrsTiles[l][k][i] = inOsrsRegion;
				do {
					int l1 = stream.readUnsignedByte();
					if (l1 == 0)
						if (l == 0) {
							anIntArrayArrayArray129[0][k][i] = -method172(
									0xe3b7b + k + k1, 0x87cce + i + j) * 8;
							return;
						} else {
							anIntArrayArrayArray129[l][k][i] = anIntArrayArrayArray129[l - 1][k][i] - 240;
							return;
						}
					if (l1 == 1) {
						int j2 = stream.readUnsignedByte();
						if (j2 == 1)
							j2 = 0;
						if (l == 0) {
							anIntArrayArrayArray129[0][k][i] = -j2 * 8;
							return;
						} else {
							anIntArrayArrayArray129[l][k][i] = anIntArrayArrayArray129[l - 1][k][i]
									- j2 * 8;
							return;
						}
					}
					if (l1 <= 49) {
						aByteArrayArrayArray130[l][k][i] = stream
								.readSignedByte();
						aByteArrayArrayArray136[l][k][i] = (byte) ((l1 - 2) / 4);
						aByteArrayArrayArray148[l][k][i] = (byte) ((l1 - 2)
								+ i1 & 3);
					} else if (l1 <= 81)
						aByteArrayArrayArray149[l][k][i] = (byte) (l1 - 49);
					else
						aByteArrayArrayArray142[l][k][i] = (byte) (l1 - 81);
				} while (true);
			}
			do {
				int i2 = stream.readUnsignedByte();
				if (i2 == 0)
					break;
				if (i2 == 1) {
					stream.readUnsignedByte();
					return;
				}
				if (i2 <= 49)
					stream.readUnsignedByte();
			} while (true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int method182(int i, int j, int k) {
		if ((aByteArrayArrayArray149[j][k][i] & 8) != 0)
			return 0;
		if (j > 0 && (aByteArrayArrayArray149[1][k][i] & 2) != 0)
			return j - 1;
		else
			return j;
	}

	public final void method183(Class11 aclass11[],
			WorldController worldController, int i, int j, int k, int l,
			byte abyte0[], int i1, int j1, int k1, int regionType) {
		label0: {
			RSBuffer stream = new RSBuffer(abyte0);
			int l1 = -1;
			do {
				int i2 = stream.readUSmart2();
				if (i2 == 0)
					break label0;
				l1 += i2;
				int j2 = 0;
				do {
					int k2 = stream.readUSmart();
					if (k2 == 0)
						break;
					j2 += k2 - 1;
					int l2 = j2 & 0x3f;
					int i3 = j2 >> 6 & 0x3f;
					int j3 = j2 >> 12;
					int k3 = stream.readUnsignedByte();
					int l3 = k3 >> 2;
					int i4 = k3 & 3;
					if (j3 == i && i3 >= i1 && i3 < i1 + 8 && l2 >= k
							&& l2 < k + 8) {
						ObjectDef class46 = getObjectDef(l1, regionType);
						int j4 = j + Class4.method157(j1, ((i4 == 0 || i4 == 2) ? class46.anInt761 : class46.anInt744), i3 & 7, l2 & 7, ((i4 == 0 || i4 == 2) ? class46.anInt744 : class46.anInt761));//int j4 = j + com.soulplayps.client.Class4.method157(j1, class46.anInt761, i3 & 7, l2 & 7, class46.anInt744);
						int k4 = k1 + Class4.method158(l2 & 7, (i4 == 0 || i4 == 2) ? class46.anInt761 : class46.anInt744, j1, ((i4 == 0 || i4 == 2) ? class46.anInt744 : class46.anInt761), i3 & 7);//int k4 = k1 + com.soulplayps.client.Class4.method158(l2 & 7, class46.anInt761, j1, class46.anInt744, i3 & 7);
						if (j4 > 0 && k4 > 0 && j4 < 103 && k4 < 103) {
							int l4 = j3;
							if ((aByteArrayArrayArray149[1][j4][k4] & 2) == 2)
								l4--;
							Class11 class11 = null;
							if (l4 >= 0)
								class11 = aclass11[l4];
							method175(k4, worldController, class11, l3, l, j4, l1, i4 + j1 & 3, class46, regionType);
						}
					}
				} while (true);
			} while (true);
		}
	}

	private static int method184(int i, int j, int k, int l) {
		int i1 = 0x10000 - Rasterizer.COSINE[(k * 1024) / l] >> 1;
		return (i * (0x10000 - i1) >> 16) + (j * i1 >> 16);
	}

	private int method185(int i, int j) {
		if (i == -2)
			return 0xbc614e;
		if (i == -1) {
			if (j < 0)
				j = 0;
			else if (j > 127)
				j = 127;
			j = 127 - j;
			return j;
		}
		j = (j * (i & 0x7f)) / 128;
		if (j < 2)
			j = 2;
		else if (j > 126)
			j = 126;
		return (i & 0xff80) + j;
	}

	private static int method186(int i, int j) {
		int k = method170(i - 1, j - 1) + method170(i + 1, j - 1)
				+ method170(i - 1, j + 1) + method170(i + 1, j + 1);
		int l = method170(i - 1, j) + method170(i + 1, j) + method170(i, j - 1)
				+ method170(i, j + 1);
		int i1 = method170(i, j);
		return k / 16 + l / 8 + i1 / 4;
	}

	private static int method187(int i, int j) {
		if (i == -1)
			return 0xbc614e;
		j = (j * (i & 0x7f)) / 128;
		if (j < 2)
			j = 2;
		else if (j > 126)
			j = 126;
		return (i & 0xff80) + j;
	}

//	public static void method188(com.soulplayps.client.WorldController worldController, int i, int j,
//			int k, int l, com.soulplayps.client.Class11 class11, int ai[][][], int i1, int j1, int k1) {
//		int l1 = ai[l][i1][j];
//		int i2 = ai[l][i1 + 1][j];
//		int j2 = ai[l][i1 + 1][j + 1];
//		int k2 = ai[l][i1][j + 1];
//		int l2 = l1 + i2 + j2 + k2 >> 2;
//		com.soulplayps.client.ObjectDef class46 = com.soulplayps.client.ObjectDef.forID(j1);
////		int i3 = i1 + (j << 7) + (j1 << 14) + 0x40000000;
//		int i3 = i1 + (j << 7) + ((j1 << 14) & 32767) + 0x40000000;
//		if (!class46.hasActions)
//			i3 += 0x80000000;
//		byte byte1 = (byte) ((i << 6) + k);
//		if (k == 22) {
//			Object obj;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj = class46.method578(22, i, l1, i2, j2, k2, -1);
//			else
//				obj = new com.soulplayps.client.Animable_Sub5(j1, i, 22, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method280(k1, l2, j, ((com.soulplayps.client.Animable) (obj)), byte1, i3,
//					i1, j1);
//			if (class46.aBoolean767 && class46.hasActions)
//				class11.method213(j, i1);
//			return;
//		}
//		if (k == 10 || k == 11) {
//			Object obj1;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj1 = class46.method578(10, i, l1, i2, j2, k2, -1);
//			else
//				obj1 = new com.soulplayps.client.Animable_Sub5(j1, i, 10, i2, j2, l1, k2,
//						class46.anInt781, true);
//			if (obj1 != null) {
//				int j5 = 0;
//				if (k == 11)
//					j5 += 256;
//				int k4;
//				int i5;
//				if (i == 1 || i == 3) {
//					k4 = class46.anInt761;
//					i5 = class46.anInt744;
//				} else {
//					k4 = class46.anInt744;
//					i5 = class46.anInt761;
//				}
//				worldController.method284(i3, byte1, l2, i5,
//						((com.soulplayps.client.Animable) (obj1)), k4, k1, j5, j, i1, i1);
//			}
//			if (class46.aBoolean767)
//				class11.method212(class46.aBoolean757, class46.anInt744,
//						class46.anInt761, i1, j, i);
//			return;
//		}
//		if (k >= 12) {
//			Object obj2;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj2 = class46.method578(k, i, l1, i2, j2, k2, -1);
//			else
//				obj2 = new com.soulplayps.client.Animable_Sub5(j1, i, k, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method284(i3, byte1, l2, 1, ((com.soulplayps.client.Animable) (obj2)), 1,
//					k1, 0, j, i1, i1);
//			if (class46.aBoolean767)
//				class11.method212(class46.aBoolean757, class46.anInt744,
//						class46.anInt761, i1, j, i);
//			return;
//		}
//		if (k == 0) {
//			Object obj3;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj3 = class46.method578(0, i, l1, i2, j2, k2, -1);
//			else
//				obj3 = new com.soulplayps.client.Animable_Sub5(j1, i, 0, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method282(anIntArray152[i], ((com.soulplayps.client.Animable) (obj3)),
//					i3, j, byte1, i1, null, l2, 0, k1, i1);
//			if (class46.aBoolean767)
//				class11.method211(j, i, i1, k, class46.aBoolean757);
//			return;
//		}
//		if (k == 1) {
//			Object obj4;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj4 = class46.method578(1, i, l1, i2, j2, k2, -1);
//			else
//				obj4 = new com.soulplayps.client.Animable_Sub5(j1, i, 1, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method282(anIntArray140[i], ((com.soulplayps.client.Animable) (obj4)),
//					i3, j, byte1, i1, null, l2, 0, k1, i1);
//			if (class46.aBoolean767)
//				class11.method211(j, i, i1, k, class46.aBoolean757);
//			return;
//		}
//		if (k == 2) {
//			int j3 = i + 1 & 3;
//			Object obj11;
//			Object obj12;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null) {
//				obj11 = class46.method578(2, 4 + i, l1, i2, j2, k2, -1);
//				obj12 = class46.method578(2, j3, l1, i2, j2, k2, -1);
//			} else {
//				obj11 = new com.soulplayps.client.Animable_Sub5(j1, 4 + i, 2, i2, j2, l1, k2,
//						class46.anInt781, true);
//				obj12 = new com.soulplayps.client.Animable_Sub5(j1, j3, 2, i2, j2, l1, k2,
//						class46.anInt781, true);
//			}
//			worldController.method282(anIntArray152[i], ((com.soulplayps.client.Animable) (obj11)),
//					i3, j, byte1, i1, ((com.soulplayps.client.Animable) (obj12)), l2,
//					anIntArray152[j3], k1, i1);
//			if (class46.aBoolean767)
//				class11.method211(j, i, i1, k, class46.aBoolean757);
//			return;
//		}
//		if (k == 3) {
//			Object obj5;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj5 = class46.method578(3, i, l1, i2, j2, k2, -1);
//			else
//				obj5 = new com.soulplayps.client.Animable_Sub5(j1, i, 3, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method282(anIntArray140[i], ((com.soulplayps.client.Animable) (obj5)),
//					i3, j, byte1, i1, null, l2, 0, k1, i1);
//			if (class46.aBoolean767)
//				class11.method211(j, i, i1, k, class46.aBoolean757);
//			return;
//		}
//		if (k == 9) {
//			Object obj6;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj6 = class46.method578(k, i, l1, i2, j2, k2, -1);
//			else
//				obj6 = new com.soulplayps.client.Animable_Sub5(j1, i, k, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method284(i3, byte1, l2, 1, ((com.soulplayps.client.Animable) (obj6)), 1,
//					k1, 0, j, i1, i1);
//			if (class46.aBoolean767)
//				class11.method212(class46.aBoolean757, class46.anInt744,
//						class46.anInt761, i1, j, i);
//			return;
//		}
//		if (class46.aBoolean762)
//			if (i == 1) {
//				int k3 = k2;
//				k2 = j2;
//				j2 = i2;
//				i2 = l1;
//				l1 = k3;
//			} else if (i == 2) {
//				int l3 = k2;
//				k2 = i2;
//				i2 = l3;
//				l3 = j2;
//				j2 = l1;
//				l1 = l3;
//			} else if (i == 3) {
//				int i4 = k2;
//				k2 = l1;
//				l1 = i2;
//				i2 = j2;
//				j2 = i4;
//			}
//		if (k == 4) {
//			Object obj7;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj7 = class46.method578(4, 0, l1, i2, j2, k2, -1);
//			else
//				obj7 = new com.soulplayps.client.Animable_Sub5(j1, 0, 4, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method283(i3, j, i * 512, k1, 0, l2,
//					((com.soulplayps.client.Animable) (obj7)), i1, byte1, 0, anIntArray152[i], i1);
//			return;
//		}
//		if (k == 5) {
//			int j4 = 16;
//			int l4 = worldController.method300(k1, i1, j);
//			if (l4 > 0)
//				j4 = com.soulplayps.client.ObjectDef.forID(l4 >> 14 & 0x7fff).anInt775;
//			Object obj13;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj13 = class46.method578(4, 0, l1, i2, j2, k2, -1);
//			else
//				obj13 = new com.soulplayps.client.Animable_Sub5(j1, 0, 4, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method283(i3, j, i * 512, k1,
//					anIntArray137[i] * j4, l2, ((com.soulplayps.client.Animable) (obj13)), i1, byte1,
//					anIntArray144[i] * j4, anIntArray152[i], i1);
//			return;
//		}
//		if (k == 6) {
//			Object obj8;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj8 = class46.method578(4, 0, l1, i2, j2, k2, -1);
//			else
//				obj8 = new com.soulplayps.client.Animable_Sub5(j1, 0, 4, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method283(i3, j, i, k1, 0, l2, ((com.soulplayps.client.Animable) (obj8)),
//					i1, byte1, 0, 256, i1);
//			return;
//		}
//		if (k == 7) {
//			Object obj9;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj9 = class46.method578(4, 0, l1, i2, j2, k2, -1);
//			else
//				obj9 = new com.soulplayps.client.Animable_Sub5(j1, 0, 4, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method283(i3, j, i, k1, 0, l2, ((com.soulplayps.client.Animable) (obj9)),
//					i1, byte1, 0, 512, i1);
//			return;
//		}
//		if (k == 8) {
//			Object obj10;
//			if (class46.anInt781 == -1 && class46.childrenIDs == null)
//				obj10 = class46.method578(4, 0, l1, i2, j2, k2, -1);
//			else
//				obj10 = new com.soulplayps.client.Animable_Sub5(j1, 0, 4, i2, j2, l1, k2,
//						class46.anInt781, true);
//			worldController.method283(i3, j, i, k1, 0, l2,
//					((com.soulplayps.client.Animable) (obj10)), i1, byte1, 0, 768, i1);
//		}
//	}
	
	public static void method188(WorldController worldController, int i, int j, int k, int l, Class11 class11, int ai[][][], int i1, int j1, int k1, int regionType) {
		int l1 = ai[l][i1][j];
		int i2 = ai[l][i1 + 1][j];
		int j2 = ai[l][i1 + 1][j + 1];
		int k2 = ai[l][i1][j + 1];
		int l2 = l1 + i2 + j2 + k2 >> 2;
		ObjectDef class46 = getObjectDef(j1, regionType);//ObjectDef.forID(j1);
		long i3 = i1 | (j << 7) | (k << 14) | (i << 20) | 0x40000000;
		if (!class46.hasActions) {
			i3 |= ~0x7fffffffffffffffL;
		}
		/*if (class46.supportItems == 1) {//TODO
			i3 |= 0x400000L;
		}*/
		i3 |= (long) j1 << 32;
		if (k == 22) {
			Object obj;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj = class46.method578(22, i, l1, i2, j2, k2);
			else
				obj = new Animable_Sub5(j1, i, 22, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method280(k1, l2, j, ((Animable) (obj)), i3, i1);
			if (class46.aBoolean767 && class46.hasActions)
				class11.method213(j, i1);
			return;
		}
		if (k == 10 || k == 11) {
			Object obj1;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj1 = class46.method578(10, i, l1, i2, j2, k2);
			else
				obj1 = new Animable_Sub5(j1, i, 10, i2, j2, l1, k2, class46.anInt781, true, regionType);
			if (obj1 != null) {
				int j5 = 0;
				if (k == 11)
					j5 += 256;
				int k4;
				int i5;
				if (i == 1 || i == 3) {
					k4 = class46.anInt761;
					i5 = class46.anInt744;
				} else {
					k4 = class46.anInt744;
					i5 = class46.anInt761;
				}
				worldController.method284(i3, l2, i5, ((Animable) (obj1)), k4, k1, j5, j, i1);
			}
			if (class46.aBoolean767)
				class11.method212(class46.aBoolean757, class46.anInt744, class46.anInt761, i1, j, i);
			return;
		}
		if (k >= 12) {
			Object obj2;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj2 = class46.method578(k, i, l1, i2, j2, k2);
			else
				obj2 = new Animable_Sub5(j1, i, k, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method284(i3, l2, 1, ((Animable) (obj2)), 1, k1, 0, j, i1);
			if (class46.aBoolean767)
				class11.method212(class46.aBoolean757, class46.anInt744, class46.anInt761, i1, j, i);
			return;
		}
		if (k == 0) {
			Object obj3;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj3 = class46.method578(0, i, l1, i2, j2, k2);
			else
				obj3 = new Animable_Sub5(j1, i, 0, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method282(anIntArray152[i], ((Animable) (obj3)), i3, j, i1, null, l2, 0, k1);
			if (class46.aBoolean767)
				class11.method211(j, i, i1, k, class46.aBoolean757);
			return;
		}
		if (k == 1) {
			Object obj4;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj4 = class46.method578(1, i, l1, i2, j2, k2);
			else
				obj4 = new Animable_Sub5(j1, i, 1, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method282(anIntArray140[i], ((Animable) (obj4)), i3, j, i1, null, l2, 0, k1);
			if (class46.aBoolean767)
				class11.method211(j, i, i1, k, class46.aBoolean757);
			return;
		}
		if (k == 2) {
			int j3 = i + 1 & 3;
			Object obj11;
			Object obj12;
			if (class46.anInt781 == -1 && class46.childrenIDs == null) {
				obj11 = class46.method578(2, 4 + i, l1, i2, j2, k2);
				obj12 = class46.method578(2, j3, l1, i2, j2, k2);
			} else {
				obj11 = new Animable_Sub5(j1, 4 + i, 2, i2, j2, l1, k2, class46.anInt781, true, regionType);
				obj12 = new Animable_Sub5(j1, j3, 2, i2, j2, l1, k2, class46.anInt781, true, regionType);
			}
			worldController.method282(anIntArray152[i], ((Animable) (obj11)), i3, j, i1, ((Animable) (obj12)), l2, anIntArray152[j3], k1);
			if (class46.aBoolean767)
				class11.method211(j, i, i1, k, class46.aBoolean757);
			return;
		}
		if (k == 3) {
			Object obj5;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj5 = class46.method578(3, i, l1, i2, j2, k2);
			else
				obj5 = new Animable_Sub5(j1, i, 3, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method282(anIntArray140[i], ((Animable) (obj5)), i3, j, i1, null, l2, 0, k1);
			if (class46.aBoolean767)
				class11.method211(j, i, i1, k, class46.aBoolean757);
			return;
		}
		if (k == 9) {
			Object obj6;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj6 = class46.method578(k, i, l1, i2, j2, k2);
			else
				obj6 = new Animable_Sub5(j1, i, k, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method284(i3, l2, 1, ((Animable) (obj6)), 1, k1, 0, j, i1);
			if (class46.aBoolean767)
				class11.method212(class46.aBoolean757, class46.anInt744, class46.anInt761, i1, j, i);
			return;
		}
		if (class46.aBoolean762)
			if (i == 1) {
				int k3 = k2;
				k2 = j2;
				j2 = i2;
				i2 = l1;
				l1 = k3;
			} else if (i == 2) {
				int l3 = k2;
				k2 = i2;
				i2 = l3;
				l3 = j2;
				j2 = l1;
				l1 = l3;
			} else if (i == 3) {
				int i4 = k2;
				k2 = l1;
				l1 = i2;
				i2 = j2;
				j2 = i4;
			}
		if (k == 4) {
			Object obj7;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj7 = class46.method578(4, 0, l1, i2, j2, k2);
			else
				obj7 = new Animable_Sub5(j1, 0, 4, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method283(i3, j, i * 512, k1, 0, l2, ((Animable) (obj7)), i1, 0, anIntArray152[i]);
			return;
		}
		if (k == 5) {
			int j4 = 16;
			long l4 = worldController.method300(k1, i1, j);
			if (l4 != 0L) {
				j4 = getObjectDef((int) ((l4 >>> 32) & 0x7fffffff), regionType).anInt775;//ObjectDef.forID((int) (l4 >>> 32) & 0x7fffffff).anInt775;
			}

			Object obj13;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj13 = class46.method578(4, 0, l1, i2, j2, k2);
			else
				obj13 = new Animable_Sub5(j1, 0, 4, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method283(i3, j, i * 512, k1, anIntArray137[i] * j4, l2, ((Animable) (obj13)), i1, anIntArray144[i] * j4, anIntArray152[i]);
			return;
		}
		if (k == 6) {
			Object obj8;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj8 = class46.method578(4, 0, l1, i2, j2, k2);
			else
				obj8 = new Animable_Sub5(j1, 0, 4, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method283(i3, j, i, k1, 0, l2, ((Animable) (obj8)), i1, 0, 256);
			return;
		}
		if (k == 7) {
			Object obj9;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj9 = class46.method578(4, 0, l1, i2, j2, k2);
			else
				obj9 = new Animable_Sub5(j1, 0, 4, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method283(i3, j, i, k1, 0, l2, ((Animable) (obj9)), i1, 0, 512);
			return;
		}
		if (k == 8) {
			Object obj10;
			if (class46.anInt781 == -1 && class46.childrenIDs == null)
				obj10 = class46.method578(4, 0, l1, i2, j2, k2);
			else
				obj10 = new Animable_Sub5(j1, 0, 4, i2, j2, l1, k2, class46.anInt781, true, regionType);
			worldController.method283(i3, j, i, k1, 0, l2, ((Animable) (obj10)), i1, 0, 768);
		}
	}

	public static boolean method189(int i, byte[] is, int i_250_, int regionId) // xxx bad
																	// method,
																	// decompiled
																	// with JODE
	{
//		boolean use525 = regionId == 12598 || regionId == 12597 || regionId == 13113;
		int defVersion = getObjectDefVer(regionId);
		boolean bool = true;
		RSBuffer stream = new RSBuffer(is);
		int i_252_ = -1;
		for (;;) {
			int i_253_ = stream.readUSmart2();
			if (i_253_ == 0)
				break;
			i_252_ += i_253_;
			int objectId = i_252_;
			if (defVersion == 1337)
				objectId += ObjectDef.OSRS_START;
			int i_254_ = 0;
			boolean bool_255_ = false;
			for (;;) {
				if (bool_255_) {
					int i_256_ = stream.readUSmart();
					if (i_256_ == 0)
						break;
					stream.readUnsignedByte();
				} else {
					int i_257_ = stream.readUSmart();
					if (i_257_ == 0)
						break;
					i_254_ += i_257_ - 1;
					int i_258_ = i_254_ & 0x3f;
					int i_259_ = i_254_ >> 6 & 0x3f;
					int i_260_ = stream.readUnsignedByte() >> 2;
					int i_261_ = i_259_ + i;
					int i_262_ = i_258_ + i_250_;
					if (i_261_ > 0 && i_262_ > 0 && i_261_ < 103
							&& i_262_ < 103) {
						ObjectDef class46 = getObjectDef(objectId, defVersion);
						if (i_260_ != 22 || drawGroundDecor || class46.hasActions
								|| class46.aBoolean736) {
							bool &= class46.method579();
							bool_255_ = true;
						}
					}
				}
			}
		}
		return bool;
	}
	
	public static int getObjectDefVer(int regionId) {
		switch (regionId) {
		case 12598:
		case 12597:
			return 525;
			
		case 14645:
		case 13625:
		case 13626:
		case 13881:
		case 13882:
		case 14389:
			return 667;
			
		case 5731:
			return 830;
		}
		if (Client.osrsMapIn317(regionId)) {
			return 1337;
		}
		if (Client.osrsRegions(regionId))
			return 100; // osrs
		return 474;
	}

	public final void method190(int i, Class11 aclass11[], int j,
			WorldController worldController, byte abyte0[], int regionId) {
//		boolean use525 = regionId == 12598 || regionId == 12597 || regionId == 13113;

		int version = getObjectDefVer(regionId);

		label0: {
			RSBuffer stream = new RSBuffer(abyte0);
			int l = -1;
			do {
				int i1 = stream.readUSmart2();
				if (i1 == 0)
					break label0;
				l += i1;
				
				int objectId = l;
				
				if (version == 1337)
					objectId += ObjectDef.OSRS_START;
				
				int j1 = 0;
				do {
					int k1 = stream.readUSmart();
					if (k1 == 0)
						break;
					j1 += k1 - 1;
					int l1 = j1 & 0x3f;
					int i2 = j1 >> 6 & 0x3f;
					int j2 = j1 >> 12;
					int k2 = stream.readUnsignedByte();
					int l2 = k2 >> 2;
					int i3 = k2 & 3;
					int j3 = i2 + i;
					int k3 = l1 + j;
					if (j3 > 0 && k3 > 0 && j3 < 103 && k3 < 103 && j2 >= 0
							&& j2 < 4) {
						int l3 = j2;
						if ((aByteArrayArrayArray149[1][j3][k3] & 2) == 2)
							l3--;
						Class11 class11 = null;
						if (l3 >= 0)
							class11 = aclass11[l3];

						ObjectDef class46 = getObjectDef(objectId, version);

						method175(k3, worldController, class11, l2, j2, j3, objectId,
								i3, class46, version);
					}
				} while (true);
			} while (true);
		}
	}
	
	public static ObjectDef getObjectDef(int objectId, int regionType) {
		switch (regionType) {
		case 1337:
		case 100:
			return ObjectDef.forIDOSRS(objectId);
		case 525:
			return ObjectDef.forID525(objectId);
		case 667:
			return ObjectDef.forID667(objectId);
		case 830:
			return ObjectDef.forID830(objectId);
			
			default:
				return ObjectDef.forID(objectId);
				
		}
	}

	private final int[] anIntArray124;
	private final int[] anIntArray125;
	private final int[] anIntArray126;
	private final int[] anIntArray127;
	private final int[] anIntArray128;
	private final int[][][] anIntArrayArrayArray129;
	private final byte[][][] aByteArrayArrayArray130;
	public static int anInt131;
	private final byte[][][] aByteArrayArrayArray134;
	private final int[][][] anIntArrayArrayArray135;
	private final byte[][][] aByteArrayArrayArray136;
	private static final int anIntArray137[] = { 1, 0, -1, 0 };
	private final int[][] anIntArrayArray139;
	private static final int anIntArray140[] = { 16, 32, 64, 128 };
	private final byte[][][] aByteArrayArrayArray142;
	private final boolean[][][] osrsTiles;
	private static final int anIntArray144[] = { 0, -1, 0, 1 };
	public static int anInt145 = 99;
	private final int anInt146;
	private final int anInt147;
	private final byte[][][] aByteArrayArrayArray148;
	private final byte[][][] aByteArrayArrayArray149;
	public static boolean lowMem = true;
	public static boolean drawGroundDecor = true;
	private static final int anIntArray152[] = { 1, 2, 4, 8 };

}
