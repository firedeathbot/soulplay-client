package com.soulplayps.client.node;

public class NodeSub extends Node {

	public final void unlinkSub() {
		if (nextNodeSub != null) {
			nextNodeSub.prevNodeSub = prevNodeSub;
			prevNodeSub.nextNodeSub = nextNodeSub;
			prevNodeSub = null;
			nextNodeSub = null;
		}
	}

	public NodeSub() {
	}

	public NodeSub prevNodeSub;
	public NodeSub nextNodeSub;
}
