package com.soulplayps.client.node;

public class Deque {
	
    public Node head = new Node();
    private Node current;
    
    final Node getBack() {
    	Node node = head.prev;
    	if (node == head) {
    		current = null;
    		return null;
    	}
    	current = node.prev;
    	return node;
    }
    
    public final Node getFront() {
    	Node node = head.next;
    	if (node == head) {
    		current = null;
    		return null;
    	}
    	current = node.next;
    	return node;
    }
    
    final Node popLast() {
		Node node = head.prev;
		if (node == head)
		    return null;
		node.unlink();
		return node;
    }
    
    public final void insertFront(Node node) {
    	if (node.prev != null)
    		node.unlink();
    	node.next = head.next;
    	node.prev = head;
    	node.prev.next = node;
    	node.next.prev = node;
    }
    
    public final void insertBack(Node node) {
    	if (node.prev != null)
		    node.unlink();
    	node.next = head;
    	node.prev = head.prev;
	    node.prev.next = node;
	    node.next.prev = node;
    }
    
    public final void method894(Node class3, Node class3_27_) {
    	if (class3.prev != null)
    		class3.unlink();
    	class3.next = class3_27_;
    	class3.prev = class3_27_.prev;
    	class3.prev.next = class3;
		class3.next.prev = class3;
    }
    
    public final Node getNext() {
    	Node node = current;
    	if (head == node) {
    		current = null;
    		return null;
    	}
    	current = node.next;
    	return node;
    }
    
    final void clear() {
    	for (;;) {
    		Node node = head.next;
    		if (node == head)
    			break;
    		node.unlink();
    	}
    }
    
    public final Node popFront() {
    	Node node = head.next;
    	if (node == head)
    		return null;
    	node.unlink();
    	return node;
    }
    
    final Node getprev() {
    	Node node = current;
    	if (head == node) {
    		current = null;
    		return null;
    	}
    	current = node.prev;
    	return node;
    }
    
    public Deque() {
    	head.next = head;
    	head.prev = head;
    }
}
