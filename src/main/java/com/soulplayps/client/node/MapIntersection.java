package com.soulplayps.client.node;

import com.soulplayps.client.Client;

public class MapIntersection implements Comparable<MapIntersection>
{
	
	/**
	 * @return the x
	 */
	public int getX()
	{
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY()
	{
		return y;
	}

	/**
	 * @return the objectId
	 */
	public int getObjectId()
	{
		return objectId;
	}

	/**
	 * @return the uid
	 */
	public long getUid()
	{
		return uid;
	}

	private final int x, y;
	private final double distance;
	private final int objectId;
	private final long uid;
	private final boolean isSmallObject;
	
	public MapIntersection(int objectId, long uid){
		this(objectId, uid, false);
	}
	public MapIntersection(int objectId, long uid, boolean isSmallObject) {
		int cx = Client.xCameraPos >> 7;
		int cy = Client.yCameraPos >> 7;
		int ox = (int) (uid & 0x7f);
		int oy = (int) ((uid >> 7) & 0x7f);
		
		this.x = cx - ox;
		this.y = cy - oy;
		this.objectId = objectId;
		this.uid = uid;
		this.isSmallObject = isSmallObject;
		
		//System.out.println("intersection: "+objectId+", "+uid);
		//System.out.println("pos: "+cx+"/"+cy+"; "+ox+"/"+oy+"; "+x+"/"+y);
		
		this.distance = Math.sqrt(x*x + y*y);
	}

	@Override
	public int compareTo(MapIntersection o)
	{
		return (int) ((o.getDistance() - this.getDistance()) * 10000.0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + objectId;
		result = prime * result + (int) (uid ^ (uid >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapIntersection other = (MapIntersection) obj;
		if (objectId != other.objectId)
			return false;
		if (uid != other.uid)
			return false;
		return true;
	}

	public double getDistance()
	{
		return distance;
	}

	public boolean isSmallObject() {
		return isSmallObject;
	}

}
