package com.soulplayps.client.unpack;

import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.settings.Settings;
import com.soulplayps.client.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Flo {

	private static List<Integer> whitefloors = new ArrayList<Integer>();
	private static Random random = new Random();

	public static void unpackConfig(RSArchive streamLoader) {
		whitefloors.add(0xffffff);
		whitefloors.add(0xdedede);
		RSBuffer stream = new RSBuffer(streamLoader.readFile("flo.dat"));
		int cacheSize = stream.readUnsignedWord();
		if (cache == null)
			cache = new Flo[cacheSize];
		for (int j = 0; j < cacheSize; j++) {
			if (cache[j] == null)
				cache[j] = new Flo();
			cache[j].readValues(stream);
		}

		cache[41].aBoolean393 = false;
		cache[154].aBoolean393 = false;

		whitefloors.clear();
		whitefloors = null;
		random = null;
	}

	private void readValues(RSBuffer stream) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				return;
			else if (i == 1) {
				anInt390 = stream.read3Bytes();
				if ((Boolean) Settings.getSetting("display_snow") && Config.isWinter()) {
					int r = whitefloors.get(random.nextInt(whitefloors.size()));
					method262(r);
				} else {
					method262(anInt390);
				}
			} else if (i == 2)
				anInt391 = stream.readUnsignedByte();
			else if (i == 3) {
			} else if (i == 5)
				aBoolean393 = false;
			else if (i == 6)
				stream.readString();
			else if (i == 7) {
				int j = anInt394;
				int k = anInt395;
				int l = anInt396;
				int i1 = anInt397;
				int j1 = stream.read3Bytes();
				method262(j1);
				anInt394 = j;
				anInt395 = k;
				anInt396 = l;
				anInt397 = i1;
				anInt398 = i1;
			} else {
				System.out.println("Error unrecognised config code: " + i);
			}
		} while (true);
	}

	private void method262(int i) {
		double d = (i >> 16 & 0xff) / 256D;
		double d1 = (i >> 8 & 0xff) / 256D;
		double d2 = (i & 0xff) / 256D;
		double d3 = d;
		if (d1 < d3)
			d3 = d1;
		if (d2 < d3)
			d3 = d2;
		double d4 = d;
		if (d1 > d4)
			d4 = d1;
		if (d2 > d4)
			d4 = d2;
		double d5 = 0.0D;
		double d6 = 0.0D;
		double d7 = (d3 + d4) / 2D;
		if (d3 != d4) {
			if (d7 < 0.5D)
				d6 = (d4 - d3) / (d4 + d3);
			if (d7 >= 0.5D)
				d6 = (d4 - d3) / (2D - d4 - d3);
			if (d == d4)
				d5 = (d1 - d2) / (d4 - d3);
			else if (d1 == d4)
				d5 = 2D + (d2 - d) / (d4 - d3);
			else if (d2 == d4)
				d5 = 4D + (d - d1) / (d4 - d3);
		}
		d5 /= 6D;
		anInt394 = (int) (d5 * 256D);
		anInt395 = (int) (d6 * 256D);
		anInt396 = (int) (d7 * 256D);
		if (anInt395 < 0)
			anInt395 = 0;
		else if (anInt395 > 255)
			anInt395 = 255;
		if (anInt396 < 0)
			anInt396 = 0;
		else if (anInt396 > 255)
			anInt396 = 255;
		if (d7 > 0.5D)
			anInt398 = (int) ((1.0D - d7) * d6 * 512D);
		else
			anInt398 = (int) (d7 * d6 * 512D);
		if (anInt398 < 1)
			anInt398 = 1;
		anInt397 = (int) (d5 * anInt398);
	}

	private Flo() {
		anInt391 = -1;
		aBoolean393 = true;
	}

	public static Flo cache[];
	public int anInt390; //rgbColor?
	public int anInt391; //textureId?
	public boolean aBoolean393; // occlude ?
	public int anInt394;  //hue2?
	public int anInt395; //saturation?
	public int anInt396; //lightness?
	public int anInt397; //hue?
	public int anInt398; //hueDivisor?
}
