package com.soulplayps.client.unpack;

import com.soulplayps.client.node.ObjectCache;
import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.ondemand.OnDemandFetcher;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.animable.model.ModelTypeEnum;
import com.soulplayps.client.Client;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.unknown.Class36;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public final class SpotAnim {
	
	public static int gfx667Length = 0;
	
	public static int gfx155Length = 0;
	
	public static int gfx727Length = 0;
	
	public static int extraGfx = 100;
	
	public static final int OSRS_GFX_START = 4000;
	
	public static final int GFX_START_727 = 6000;

	public static void unpackConfig(RSArchive streamLoader) {
		RSBuffer stream = new RSBuffer(streamLoader.readFile("spotanim.dat"));
		int length = gfx667Length = stream.readUnsignedWord();
		
		RSBuffer stream155 = new RSBuffer(streamLoader.readFile("spotanimosrs.dat"));
		int length155 = gfx155Length = stream155.readUnsignedWord();
		
//		RSBuffer stream727 = new RSBuffer(streamLoader.readFile("spotanim727.dat"));
//		int length727 = gfx727Length = stream727.readUnsignedWord();
		
		System.out.println("667 gfx count:"+gfx667Length+"  extra support count:"+extraGfx);
		
		System.out.println("182 gfx coutn:"+ gfx155Length);
		
//		System.out.println("727 gfx coutn:"+ gfx727Length);
		
		if (cache == null)
			cache = new SpotAnim[GFX_START_727 + gfx727Length/*length+extraGfx*/];
		
		for (int j = 0; j < length; j++) {
			if (cache[j] == null)
				cache[j] = new SpotAnim();
			cache[j].gfxId = j;
			cache[j].readValues(stream, false);
			
		}

		//dumpValues(length);
		
		hardCodedGFX();
		for (int j = length; j < length+extraGfx; j++) {
			if (cache[j] == null)
				continue;
			cache[j].gfxId = j;
		}

		loadOsrsGfx(stream155, length155);

		//load727Gfx(stream727, length727); // no need for these yet - When enabled, it causes 32 bit java to be out of heap memory
	}

	public static void loadOsrsGfx(RSBuffer stream, int length) {
		for (int j = 0; j < length; j++) {
			int newId = j + OSRS_GFX_START;
			SpotAnim spotAnim = cache[newId];
			if (spotAnim == null) {
				spotAnim = cache[newId] = new SpotAnim();
			}

			spotAnim.gfxId = newId;
			spotAnim.readValuesOSRS(stream);
			spotAnim.modelType = ModelTypeEnum.OSRS;
		}

		cache[1595 + OSRS_GFX_START].offsetY = -6;
	}

	public static void load727Gfx(RSBuffer stream, int length) {
		for (int j = 0; j < length; j++) {
			int newId = j + GFX_START_727;
			if (cache[newId] == null)
				cache[newId] = new SpotAnim();
			cache[newId].gfxId = newId;
			cache[newId].readValues727(stream);
		}
	}

	public void preloadAnimation(OnDemandFetcher onDemandFetcher) {
		if (animation == null) {
			return;
		}

		int framesFile = animation.frame2Ids[0] >> 16;
		if (Class36.animationlist[framesFile].length == 0) {
			onDemandFetcher.loadData(1, framesFile);
		}
	}

	public static void hardCodedGFX() {
		
		
	}

	private void readValues(RSBuffer stream, boolean loadingOsrs) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				return;
			if (i == 1)
				modelId = stream.readUnsignedWord();
			else if (i == 2) {
				animationId = stream.readUnsignedWord();
				if (loadingOsrs) {
					animationId += Animation.NEW_OSRS_ID_START;
				}
				if (Animation.anims != null)
					animation = Animation.anims[animationId];
			} else if (i == 4)
				anInt410 = stream.readUnsignedWord();
			else if (i == 5)
				anInt411 = stream.readUnsignedWord();
			else if (i == 6)
				anInt412 = stream.readUnsignedWord();
			else if (i == 7)
				anInt413 = stream.readUnsignedWord();
			else if (i == 8)
				anInt414 = stream.readUnsignedWord();
			else if (i == 40) {
				int j = stream.readUnsignedByte();
				anIntArray408 = new int[j];
				anIntArray409 = new int[j];
				for (int k = 0; k < j; k++) {
					anIntArray408[k] = stream.readUnsignedWord();
					anIntArray409[k] = stream.readUnsignedWord();
				}
			} else if (i == 41) {
				int length = stream.readUnsignedByte();
				originalTexture = new short[length];
				modifiedTexture = new short[length];
				for (int k = 0; k < length; k++) {
					originalTexture[k] = (short) stream.readUnsignedWord();
					modifiedTexture[k] = (short) stream.readUnsignedWord();
				}
			} else
				System.out.println("Error unrecognised spotanim config code: "
						+ i);
		} while (true);
	}
	
	private void readValuesOSRS(RSBuffer stream) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0)
				return;
			if (i == 1)
				modelId = stream.readUnsignedWord();
			else if (i == 2) {
				animationId = stream.readUnsignedWord();
				animationId += Animation.NEW_OSRS_ID_START;
				if (Animation.anims != null)
					animation = Animation.anims[animationId];
			} else if (i == 4)
				anInt410 = stream.readUnsignedWord();
			else if (i == 5)
				anInt411 = stream.readUnsignedWord();
			else if (i == 6)
				anInt412 = stream.readUnsignedWord();
			else if (i == 7)
				anInt413 = stream.readUnsignedWord();
			else if (i == 8)
				anInt414 = stream.readUnsignedWord();
			else if (i == 40) {
				int j = stream.readUnsignedByte();
				anIntArray408 = new int[j];
				anIntArray409 = new int[j];
				for (int k = 0; k < j; k++) {
					anIntArray408[k] = stream.readUnsignedWord();
					anIntArray409[k] = stream.readUnsignedWord();
				}
			} else if (i == 41) {
				int length = stream.readUnsignedByte();
				originalTexture = new short[length];
				modifiedTexture = new short[length];
				for (int k = 0; k < length; k++) {
					originalTexture[k] = (short) stream.readUnsignedWord();
					modifiedTexture[k] = (short) stream.readUnsignedWord();
				}
			} else
				System.out.println("Error unrecognised spotanim osrs config code: "
						+ i);
		} while (true);
	}
	
	private void readValues727(RSBuffer stream) {
	do {
		int i = stream.readUnsignedByte();
		if (i == 0)
			return;
		if (i == 1)
			modelId = stream.readDWord();
		else if (i == 2) {
			animationId = stream.readDWord();
			if (Animation.anims != null)
				animation = Animation.anims[animationId];
		} else if (i == 4)
			anInt410 = stream.readUnsignedWord();
		else if (i == 5)
			anInt411 = stream.readUnsignedWord();
		else if (i == 6)
			anInt412 = stream.readUnsignedWord();
		else if (i == 7)
			anInt413 = stream.readUnsignedByte();
		else if (i == 8)
			anInt414 = stream.readUnsignedByte();
		else if (i == 40) {
			int j = stream.readUnsignedByte();
			anIntArray408 = new int[j];
			anIntArray409 = new int[j];
			for (int k = 0; k < j; k++) {
				anIntArray408[k] = stream.readUnsignedWord();
				anIntArray409[k] = stream.readUnsignedWord();
			}
		} else
			System.out.println("Error unrecognised spotanim config code: "
					+ i);
	} while (true);
}

	public Model getModel(int frameId, int nextFrameId, int delay) {
		Model model = (Model) aMRUNodes_415.get(gfxId);
		if (model == null) {
			model = Model.loadDropModel(modelId, modelType);
			if (model == null) {
				return null;
			}

			if (anIntArray408 != null) {
				for (int i = 0, length = anIntArray408.length; i < length; i++) {
					model.replaceHs1(anIntArray408[i], anIntArray409[i]);
				}
			}

			model.applyEffects();
			model.calculateLighting(95 + anInt413, 9282 + anInt414, -30, -50, -30, true);

			aMRUNodes_415.put(model, gfxId);
		}
		
		int frame = -1;
		int nextFrame = -1;
		int length = 0;
		if (animation != null) {
			frame = animation.frame2Ids[frameId];
			if (Client.tweening && nextFrameId != -1) {
				nextFrame = animation.frame2Ids[nextFrameId];
				length = animation.frameLengths[frameId];
			}
		}

		Model finalModel;

		if (frame != -1 && nextFrame != -1) {
			finalModel = model.copySpotAnim(!Class36.hasAlpha(frame) & !Class36.hasAlpha(nextFrame));
			if (!finalModel.applyAnimationTween(frame, nextFrame, delay - 1, length, null, false)) {
				return null;
			}
		} else {
			finalModel = model.copySpotAnim(!Class36.hasAlpha(frame));
			if (frame != -1 && !finalModel.applyAnimation(frame)) {
				return null;
			}
		}

		if (anInt410 != 128 || anInt411 != 128) {
			finalModel.method478(anInt410, anInt410, anInt411);
		}

		if (offsetX != 0 || offsetY != 0 || offsetZ != 0)
			finalModel.translate(offsetX, offsetY, offsetZ);

		switch (anInt412) {
			case 90:
				finalModel.rotate90();
				break;

			case 180:
				finalModel.rotate180();
				break;

			case 270:
				finalModel.rotate270();
				break;
		}

		return finalModel;
	}

	public SpotAnim() {
		animationId = -1;
		anInt410 = 128;
		anInt411 = 128;
	}

	public static void dumpValues(int length) {
		System.out.println("Dumping Graphics..");
		File f = new File("gfx.txt");
		String[] variableNames = new String[] { "modelId", "animationId", "anInt410", "anInt411", "anInt412", "anInt413",
				"anInt414", "anIntArray408", "anIntArray409" };
		try {
			f.createNewFile();
			BufferedWriter bf = new BufferedWriter(new FileWriter(f));
			for (int j = 0; j < length; j++) {
				StringBuilder builder = new StringBuilder();
				String modelId = cache[j].modelId + "";
				String animationId = cache[j].animationId + "";
				String sizeXY = cache[j].anInt410 + "";
				String sizeZ = cache[j].anInt411 + "";
				String rotation = cache[j].anInt412 + "";
				String shadow = cache[j].anInt413 + "";
				String lightness = cache[j].anInt414 + "";
				if (cache[j].anIntArray408 != null) {
					builder.append("new int[] { ");
					for (int kk = 0; kk < cache[j].anIntArray408.length; kk++) {
						builder.append(cache[j].anIntArray408[kk]);
						if (kk != cache[j].anIntArray408.length-1)
							builder.append(", ");
					}
					builder.append(" }");
				}
				String originalColours = builder.toString(); //Arrays.toString(cache[j].anIntArray408);
				builder = new StringBuilder();
				if (cache[j].anIntArray409 != null) {
					builder.append("new int[] { ");
					for (int kk = 0; kk < cache[j].anIntArray409.length; kk++) {
						builder.append(cache[j].anIntArray409[kk]);
						if (kk != cache[j].anIntArray409.length-1)
							builder.append(", ");
					}
					builder.append(" }");
				}
				String destColours = builder.toString(); // Arrays.toString(cache[j].anIntArray409);
				String[] variables = new String[] { modelId, animationId, sizeXY, sizeZ, rotation, shadow, lightness,
						originalColours, destColours };
				bf.write("SpotAnim spotAnim"+j+" = cache["+j+"] = new SpotAnim();\n");
				for (int i = 0; i < variables.length; i++) {
					bf.write("spotAnim" + j + "." + variableNames[i] + " = " + variables[i] + ";");
					bf.newLine();
				}
				bf.write("spotAnim" + j + "." + "animation" + " = " + "Animation.anims[" + variables[1] + "];");
				bf.newLine();
				bf.newLine();

			}
			bf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Dumping Complete..");

	}

	public static SpotAnim cache[];
	public int gfxId;
	public int modelId;
	public int animationId;
	public Animation animation;
	public int[] anIntArray408;
	public int[] anIntArray409;
	public int anInt410;
	public int anInt411;
	public int anInt412;
	public int anInt413;
	public int anInt414;
	public int offsetX;
	public int offsetY;
	public int offsetZ;
	public static ObjectCache aMRUNodes_415 = new ObjectCache(30);
	public ModelTypeEnum modelType = ModelTypeEnum.REGULAR;
	public short[] originalTexture;
	public short[] modifiedTexture;

}