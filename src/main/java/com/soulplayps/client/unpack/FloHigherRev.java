package com.soulplayps.client.unpack;

import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.node.io.RSBuffer;

public class FloHigherRev {

    public static FloHigherRev[] cache;
    public int textureId = -1;
    public int rgb;
    public int anInt394;
    public int anInt395;
    public int anInt396;
    public int anInt398;
    public int anInt399;

    public static void load(RSArchive arg1) {
    	RSBuffer bb = new RSBuffer(arg1.readFile("flo2.dat"));
        int count = bb.readUnsignedWord();
        cache = new FloHigherRev[count];
        for (int i = 0; i < count ; i++) {
            if (cache[i] == null)
                cache[i] = new FloHigherRev();
            cache[i].parse(bb);
            cache[i].generateHsl();
        }
    }
    
    public void generateHsl() {
        this.method262(this.rgb);
    }
    
    private void parse(RSBuffer byteBuffer) {
        for (;;) {
            int attributeId = byteBuffer.readUnsignedByte();
            if (attributeId == 0)
                break;
            else if (attributeId == 1) {
                rgb = byteBuffer.read3Bytes();
                method262(rgb);
            } else if (attributeId == 2) {
                textureId = byteBuffer.readUnsignedByte();
            } else if (attributeId == 3) {
                textureId = byteBuffer.readUnsignedWord();
                if (textureId == 65535) {
                    textureId = -1;
                }
            } else if (attributeId == 7) {
            	byteBuffer.read3Bytes();
            } else if (attributeId == 9) {
                byteBuffer.readUnsignedWord();
            } else if (attributeId == 11) {
                byteBuffer.readUnsignedByte();
            } else if (attributeId == 13) {
            	byteBuffer.read3Bytes();
            } else if (attributeId == 14) {
            	byteBuffer.readUnsignedByte();
            } else if (attributeId == 15) {
            	byteBuffer.readUnsignedWord();
            } else if (attributeId == 16) {
            	byteBuffer.readUnsignedByte();
            } else {
                System.err.println("[OverlayFloor] Missing AttributeId: "+attributeId);
            }
        }
    }

    private void method262(int arg0) {
        double d = (double) (arg0 >> 16 & 0xff) / 256.0;
        double d_5_ = (double) (arg0 >> 8 & 0xff) / 256.0;
        double d_6_ = (double) (arg0 & 0xff) / 256.0;
        double d_7_ = d;
        if (d_5_ < d_7_)
            d_7_ = d_5_;
        if (d_6_ < d_7_)
            d_7_ = d_6_;
        double d_8_ = d;
        if (d_5_ > d_8_)
            d_8_ = d_5_;
        if (d_6_ > d_8_)
            d_8_ = d_6_;
        double d_9_ = 0.0;
        double d_10_ = 0.0;
        double d_11_ = (d_7_ + d_8_) / 2.0;
        if (d_7_ != d_8_) {
            if (d_11_ < 0.5)
                d_10_ = (d_8_ - d_7_) / (d_8_ + d_7_);
            if (d_11_ >= 0.5)
                d_10_ = (d_8_ - d_7_) / (2.0 - d_8_ - d_7_);
            if (d == d_8_)
                d_9_ = (d_5_ - d_6_) / (d_8_ - d_7_);
            else if (d_5_ == d_8_)
                d_9_ = 2.0 + (d_6_ - d) / (d_8_ - d_7_);
            else if (d_6_ == d_8_)
                d_9_ = 4.0 + (d - d_5_) / (d_8_ - d_7_);
        }
        d_9_ /= 6.0;
        anInt394 = (int) (d_9_ * 256.0);
        anInt395 = (int) (d_10_ * 256.0);
        anInt396 = (int) (d_11_ * 256.0);
        if (anInt395 < 0)
            anInt395 = 0;
        else if (anInt395 > 255)
            anInt395 = 255;
        if (anInt396 < 0)
            anInt396 = 0;
        else if (anInt396 > 255)
            anInt396 = 255;
        if (d_11_ > 0.5)
            anInt398 = (int) ((1.0 - d_11_) * d_10_ * 512.0);
        else
            anInt398 = (int) (d_11_ * d_10_ * 512.0);
        if (anInt398 < 1)
            anInt398 = 1;
        anInt399 = method263(anInt394, anInt395, anInt396);
    }

    public static int method263(int arg0, int arg1, int arg2) {
        if (arg2 > 179)
            arg1 /= 2;
        if (arg2 > 192)
            arg1 /= 2;
        if (arg2 > 217)
            arg1 /= 2;
        if (arg2 > 243)
            arg1 /= 2;
        int i = (arg0 / 4 << 10) + (arg1 / 32 << 7) + arg2 / 2;
        return i;
    }

}