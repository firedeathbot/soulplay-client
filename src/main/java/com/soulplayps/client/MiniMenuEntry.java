package com.soulplayps.client;

import com.soulplayps.client.minimenu.MenuManager;

public class MiniMenuEntry {

	public int id, cmd2, cmd3;
	public long cmd1;
	public String text;

	public static MiniMenuEntry entry;

	public static void putEntry(int index) {
		entry = new MiniMenuEntry();
		entry.id = MenuManager.menuActionID[index];
		entry.cmd1 = MenuManager.menuActionCmd1[index];
		entry.cmd2 = MenuManager.menuActionCmd2[index];
		entry.cmd3 = MenuManager.menuActionCmd3[index];
		entry.text = MenuManager.menuActionName[index];
	}

	public static void executeEntry() {
		if (entry == null) {
			return;
		}

		Client.instance.doAction(entry.id, entry.cmd1, entry.cmd2, entry.cmd3, entry.text);
		entry = null;
	}

}
