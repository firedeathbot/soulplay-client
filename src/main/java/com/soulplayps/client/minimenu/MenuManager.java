package com.soulplayps.client.minimenu;

public class MenuManager {

	public static final int MENU_SIZE = 500;

	public static int[] menuActionCmd2 = new int[MENU_SIZE];
	public static int[] menuActionCmd3 = new int[MENU_SIZE];
	public static int[] menuActionID = new int[MENU_SIZE];
	public static long[] menuActionCmd1 = new long[MENU_SIZE];
	public static String[] menuActionName = new String[MENU_SIZE];
	public static String[] menuActionBase = new String[MENU_SIZE];
	public static int menuActionRow;
	public static boolean menuOpen;

	public static void addOption(String option, int id) {
		addOption(option, id, 0, 0, 0);
	}

	public static void addOption(String option, int id, long data1) {
		addOption(option, id, data1, 0, 0);
	}

	public static void addOption(String option, int id, int data2, int data3) {
		addOption(option, id, 0, data2, data3);
	}

	public static void addOption(String option, int id, long data1, int data2, int data3) {
		addOption(option, "", id, data1, data2, data3);
	}

	public static void addOption(String option, String base, int id, long data1, int data2, int data3) {
		if (menuOpen || menuActionRow >= 500) {
			return;
		}

		menuActionName[menuActionRow] = option;
		menuActionBase[menuActionRow] = base;
		menuActionID[menuActionRow] = id;
		menuActionCmd1[menuActionRow] = data1;
		menuActionCmd2[menuActionRow] = data2;
		menuActionCmd3[menuActionRow] = data3;
		menuActionRow++;
	}

	public static boolean optionLowPriority(int index) {
		if (index < 0) {
			return false;
		}
	
		int id = menuActionID[index];
		if (id >= 2000) {
			id -= 2000;
		}
	
		return id == 337;
	}

	public static void sort() {
		boolean stopSort = false;
		while (!stopSort) {
			stopSort = true;
			for (int j = 0; j < menuActionRow - 1; j++) {
				if (menuActionID[j] < 1000 && menuActionID[j + 1] > 1000) {
					String s = menuActionName[j];
					menuActionName[j] = menuActionName[j + 1];
					menuActionName[j + 1] = s;
					String s1 = menuActionBase[j];
					menuActionBase[j] = menuActionBase[j + 1];
					menuActionBase[j + 1] = s1;
					int k = menuActionID[j];
					menuActionID[j] = menuActionID[j + 1];
					menuActionID[j + 1] = k;
					k = menuActionCmd2[j];
					menuActionCmd2[j] = menuActionCmd2[j + 1];
					menuActionCmd2[j + 1] = k;
					k = menuActionCmd3[j];
					menuActionCmd3[j] = menuActionCmd3[j + 1];
					menuActionCmd3[j + 1] = k;
					long var = menuActionCmd1[j];
					menuActionCmd1[j] = menuActionCmd1[j + 1];
					menuActionCmd1[j + 1] = var;
					stopSort = false;
				}
			}
		}
	}

	public static int getLeftClick() {
		if (MenuManager.dropIndex != -1) {
			return MenuManager.dropIndex;
		}
	
		return menuActionRow - 1;
	}

	public static int dropIndex = -1;

	public static void clearMiniMenu() {
		menuActionRow = 0;
		addOption("Cancel", 1107);
		dropIndex = -1;
	}

}

