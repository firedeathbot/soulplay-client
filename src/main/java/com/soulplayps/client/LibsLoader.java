package com.soulplayps.client;

import org.lwjgl.LWJGLUtil;

import java.io.*;

import static java.io.File.separatorChar;

public class LibsLoader {
	
	public static void loadNativeLibs(int time) {
		try {
			String nativeLibFolder;
			String[] resources;
			switch (LWJGLUtil.getPlatform()) {
				case LWJGLUtil.PLATFORM_LINUX:
					nativeLibFolder = "linux";
					resources = new String[] {"libjinput-linux.so", "libjinput-linux64.so", "liblwjgl.so", "liblwjgl64.so", "libopenal.so", "libopenal64.so"};
					break;
				case LWJGLUtil.PLATFORM_MACOSX:
					nativeLibFolder = "macosx";
					resources = new String[] {"libjinput-osx.jnilib", "liblwjgl.dylib", "liblwjgl.jnilib", "openal.dylib"};
					break;
				default:
					nativeLibFolder = "windows";
					resources = new String[] {"jinput-dx8.dll", "jinput-dx8_64.dll", "jinput-raw.dll", "jinput-raw_64.dll", "lwjgl.dll", "lwjgl64.dll", "OpenAL32.dll", "OpenAL64.dll"};
					break;
			}

			String addon = time == 0 ? "" : Integer.toString(time);
			File homeFolder = new File(System.getProperty("user.home") + separatorChar + Config.folderName
					+ separatorChar + "resources" + separatorChar + "native" + addon + separatorChar + nativeLibFolder + separatorChar);
			if (!homeFolder.exists()) {
				homeFolder.mkdirs();

				for (String resource : resources) {
					InputStream inputStream = ClassLoader.getSystemResourceAsStream("resources/native/" + nativeLibFolder + "/" + resource);
					byte[] bytes = fileToBytes(inputStream);
					if (bytes != null) {
						bytesToFile(bytes, new File(homeFolder, resource));
					}
				}
			}

			System.setProperty("org.lwjgl.librarypath", homeFolder.getAbsolutePath());
		} catch (FileNotFoundException e) {
			loadNativeLibs(time + 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static byte[] fileToBytes(InputStream ios) {
		ByteArrayOutputStream ous = null;
		try {
			byte[] buffer = new byte[4096];
			ous = new ByteArrayOutputStream();
			int read;
			while ((read = ios.read(buffer)) != -1) {
				ous.write(buffer, 0, read);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ous != null)
					ous.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				if (ios != null)
					ios.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ous.toByteArray();
	}
	
	private static void bytesToFile(byte[] bytes, File file) throws FileNotFoundException {
		FileOutputStream fos = new FileOutputStream(file);
		try {
			fos.write(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
