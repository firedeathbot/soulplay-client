package com.soulplayps.client.hwid;

import com.soulplayps.client.util.SystemUtils;

public class Hardware {

	/**
	 * Return computer serial number.
	 * 
	 * @return Computer's SN
	 */
	public static final String getSerialNumber() {
		if (SystemUtils.IS_OS_WINDOWS) {
			System.out.println("win");
			return Hardware4Win.getSerialNumber();
		}
		if (SystemUtils.IS_OS_LINUX) {
			System.out.println("linux");
			return Hardware4Nix.getSerialNumber();
		}
		if (SystemUtils.IS_OS_MAC_OSX) {
			System.out.println("mac");
			return Hardware4Mac.getSerialNumber();
		}
		return null;
	}

}