package com.soulplayps.client.hwid;

import com.soulplayps.client.util.SystemUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class CreateUID {

	public static String getWMIValue(String wmiQueryStr, String wmiCommaSeparatedFieldName) throws Exception
	{
		String vbScript = getVBScript(wmiQueryStr, wmiCommaSeparatedFieldName);
		String tmpDirName = getEnvVar("TEMP").trim();
		String tmpFileName = tmpDirName + File.separator + "jwmi.vbs";
		writeStrToFile(tmpFileName, vbScript);
		String output = execute(new String[] {"cmd.exe", "/C", "cscript.exe", tmpFileName});
		new File(tmpFileName).delete();

		if (output.toLowerCase().contains("error")) {
			throw new Exception();
		}

		return output.trim();
	}
	private static final String CRLF = "\r\n";
	
	private static String getVBScript(String wmiQueryStr, String wmiCommaSeparatedFieldName)
	{
		String vbs = "Dim oWMI : Set oWMI = GetObject(\"winmgmts:\")"+CRLF;
		vbs += "Dim classComponent : Set classComponent = oWMI.ExecQuery(\""+wmiQueryStr+"\")"+CRLF;
		vbs += "Dim obj, strData"+CRLF;
		vbs += "For Each obj in classComponent"+CRLF;
		String[] wmiFieldNameArray = wmiCommaSeparatedFieldName.split(",");
		for(int i = 0; i < wmiFieldNameArray.length; i++)
		{
			vbs += "  strData = strData & obj."+wmiFieldNameArray[i]+" & VBCrLf"+CRLF;
		}
		vbs += "Next"+CRLF;
		vbs += "wscript.echo strData"+CRLF;
		return vbs;
	}
	
	private static String getEnvVar(String envVarName) throws Exception
	{
		String varName = "%"+envVarName+"%";
		String envVarValue = execute(new String[] {"cmd.exe", "/C", "echo "+varName});
		if(envVarValue.equals(varName))
		{
			throw new Exception("Environment variable '"+envVarName+"' does not exist!");
		}
		return envVarValue;
	}
	
	private static void writeStrToFile(String filename, String data) throws Exception
	{
		FileWriter output = new FileWriter(filename);
		output.write(data);
		output.flush();
		output.close();
		output = null;
	}
	
	private static String execute(String[] cmdArray) throws Exception
	{
		Process process = Runtime.getRuntime().exec(cmdArray);
		BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String output = "";
		String line = "";
		while((line = input.readLine()) != null)
		{
			//need to filter out lines that don't contain our desired output
			if(!line.contains("Microsoft") && !line.equals(""))
			{
				output += line +CRLF;
			}
		}
		process.destroy();
		process = null;
		return output.trim();
	}

	public static String getUIDOther2(){
		String s = "NOTWINDOWS";
		long diskSize = new File("/").getTotalSpace();
		String userName = System.getProperty("user.name");
		long maxMemory = Runtime.getRuntime().maxMemory();

		return s+userName+diskSize+maxMemory;
	}


	public static String getUIDOther() throws java.io.IOException {
		try {
			Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			line = in.readLine();
			String[] result = line.split(",");
			String mac = result[0].replace('"', ' ').trim();
			
			if (mac == null || mac.isEmpty())
				return getAdapterUUID();
			return mac;
		} catch (Exception e) {
			//e.printStackTrace();
			return getAdapterUUID();
		}
	}

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("win") >= 0);
	}

	public static String generateUID() throws Exception {

//		if(!isWindows())
//			return "LOF"+getUIDOther()+"LOF";
		
		if (SystemUtils.IS_OS_LINUX) {
			System.out.println("Linux");

			
			String macc = null;
			try {

		        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
		        while(networkInterfaces.hasMoreElements())
		        {
		            NetworkInterface network = networkInterfaces.nextElement();
//		            System.out.println("network : " + network);
		            byte[] mac = network.getHardwareAddress();
		            if(mac == null)
		            {
//		                System.out.println("null mac");             
		            }
		            else
		            {
//		                System.out.print("MAC address : ");

		                StringBuilder sb = new StringBuilder();
		                for (int i = 0; i < mac.length; i++)
		                {
		                    sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));        
		                }
//		                System.out.println(sb.toString());  
		                macc = sb.toString();
		                break;
		            }
		        }
		    } catch (SocketException e){

		        e.printStackTrace();

		    }
			
			if (macc != null)
				return "LOF"+macc+"LOF";
			
			//TODO: WE GOTTA FIX THIS SHIT AYE 
			return "LOFLINUXREPLACEMENTLOF"; // test out com.soulplayps.client.Hardware.getSerialNumber();
//			if (com.soulplayps.client.Hardware4Nix.getSerialNumber() == null)
//				return "LOF"+getUIDOther()+"LOF";
			//return "LOF"+com.soulplayps.client.Hardware4Nix.getSerialNumber()+"LOF";
		}
		if (SystemUtils.IS_OS_MAC_OSX) {
			System.out.println("MacOSX");
			if (Hardware4Mac.getSerialNumber() == null)
				return "LOF"+getUIDOther()+"LOF";
			return "LOF"+Hardware4Mac.getSerialNumber()+"LOF";
		}

		
		//windows
		
//		if (com.soulplayps.client.SystemUtils.IS_OS_WINDOWS) {
//			InetAddress ip;
//			try {
//
//				ip = InetAddress.getLocalHost();
//				//						System.out.println("Current IP address : " + ip.getHostAddress());
//				NetworkInterface network = NetworkInterface.getByInetAddress(ip);
//
//				byte[] mac = network.getHardwareAddress();
//
//				//						System.out.print("Current MAC address : ");
//
//				StringBuilder sb = new StringBuilder();
//				for (int i = 0; i < mac.length; i++) {
//					sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));        
//				}
//				System.out.println(sb.toString());
//
//				String macc = null;
//				macc = sb.toString();
//
//				if (!macc.isEmpty() && macc != null) {
//					return "LOF"+macc+"LOF";
//				}
//
//			} catch (Exception e) {
//
//				e.printStackTrace();
//
//			}
//
//		}
		
		
		String serial = "";
		
//		if (com.soulplayps.client.SystemUtils.IS_OS_WINDOWS) {
//			System.out.println("win");
//			serial = com.soulplayps.client.Hardware4Win.getSerialNumber();
//			//return "LOF"+com.soulplayps.client.Hardware4Win.getSerialNumber()+"LOF";
//		}
		
//		String serial = getWMIValue("SELECT SerialNumber FROM Win32_BIOS", "SerialNumber");
		if (serial.equals("")) {
			try {
				serial = getWMIValue("SELECT SerialNumber FROM Win32_BIOS", "SerialNumber");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			String lowerUUID = serial.toLowerCase();
			if (serial.equals("") || lowerUUID.equals("to be filled by o.e.m.") || lowerUUID.equals("system serial number") || lowerUUID.equals("default string") || lowerUUID.startsWith("cscripterror:")) {
				try {
					serial = getWMIValue("SELECT SerialNumber FROM Win32_DiskDrive", "SerialNumber");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
//		//serial = serial.replaceAll("[^\\d]", "");
//		String idate = getWMIValue("Select InstallDate from Win32_OperatingSystem", "InstallDate"); // this is old method and can be changed in regedit too easy
//		//idate = idate.replaceAll("[^\\d]", "");
//		String uuid = serial.concat(idate);
		
		String uuid = "";
		try {
			uuid = serial.replaceAll("_", "").replaceAll(" ", "").replaceAll(System.getProperty("line.separator"), "");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try {
			if (uuid.equals("")) {
				/*return*/uuid = Hardware4Win.getSerialNumber();
			}
		} catch (Exception ex) {
			if (uuid.equals("")) {
				String backupUUID = getUIDOther();
				if (backupUUID != null && !backupUUID.isEmpty())
					uuid = backupUUID;
			}
		}
		if (uuid.length() > 150 || uuid.toLowerCase().startsWith("can't") || uuid.toLowerCase().startsWith("error")) {
			String backupUUID = getUIDOther();
			if (backupUUID != null && !backupUUID.isEmpty()) {
				uuid = backupUUID;
			} else {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						JOptionPane.showMessageDialog(null, "Windows is not configured properly. Please run cleaning tools. In cmd.exe try command (sfc /scannow)");
					}
				});
			}
		}

		return "LOF"+(uuid)+"LOF";
	}
	
	public static String getAdapterUUID() {
		InetAddress ip;
		try {

			ip = InetAddress.getLocalHost();
			//						System.out.println("Current IP address : " + ip.getHostAddress());
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);

			byte[] mac = network.getHardwareAddress();

			//						System.out.print("Current MAC address : ");

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));        
			}
//			System.out.println(sb.toString());

			String macc = null;
			macc = sb.toString();

			return macc;

		} catch (Exception e) {

			e.printStackTrace();
			return getUIDOther2();
		}
	}

	public static final String CLASS_Win32_BIOS = "Win32_BIOS";
	public static final String CLASS_Win32_OperatingSystem = "Win32_OperatingSystem";
	
}