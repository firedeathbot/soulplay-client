package com.soulplayps.client;

import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.opengl.GraphicsDisplay;

public class Camera {

	public static CamType camType = CamType.PLAYER;
	public static boolean freeCamNeedPositioning;
	public static int freeCamShiftSpeed = 6;
	public static int freeCamNormalSpeed = 6;
	public static int freeCamThisAngle;
	public static int freeCamThisYStep;
	public static int freeCamAngleStep;

	public static int cameraX;
	public static int cameraY;
	public static int cameraZ;
	public static int cameraOffsetX;
	public static int cameraOffsetY;

	public enum CamType {
		PLAYER,
		FREE
	}

	public static void fixCamera() {
		int l = cameraX >> 7;
		int i1 = cameraZ >> 7;
		int j1 = Client.method42(Client.plane, cameraZ, cameraX);
		int k1 = 0;
		if (l > 3 && i1 > 3 && l < 100 && i1 < 100) {
			for (int l1 = l - 4; l1 <= l + 4; l1++) {
				for (int k2 = i1 - 4; k2 <= i1 + 4; k2++) {
					int l2 = Client.plane;
					if (l2 < 3 && (Client.byteGroundArray[1][l1][k2] & 2) == 2)
						l2++;
					int i3 = j1 - Client.intGroundArray[l2][l1][k2];
					if (i3 > k1)
						k1 = i3;
				}

			}

		}
		int j2 = k1 * 192;
		if (j2 > 0x17f00)
			j2 = 0x17f00;
		if (j2 < 32768)
			j2 = 32768;
		if (j2 > Client.anInt984) {
			Client.anInt984 += (j2 - Client.anInt984) / 24;
			return;
		}
		if (j2 < Client.anInt984) {
			Client.anInt984 += (j2 - Client.anInt984) / 80;
		}
	}

	private static void transmitPosition(int level) {
		/*int localCamX = Class3_Sub13_Sub13.cameraX >> 7;
		int localCamZ = Class62.cameraZ >> 7;
		if ((TextureManager.player.localX >> 7) != localCamX || (TextureManager.player.localY >> 7) != localCamZ || WorldListCountry.localPlane != level) {
			Class3_Sub13_Sub1.outgoingBuffer.putOpcode(5);
			Class3_Sub13_Sub1.outgoingBuffer.putByte(level);
			Class3_Sub13_Sub1.outgoingBuffer.putShort(localCamX + WorldMapDetails.anInt1716);
			Class3_Sub13_Sub1.outgoingBuffer.putShort(localCamZ + Class82.anInt1152);
		}*/
	}

	public static void method108() {
		if (camType == CamType.PLAYER) {
			int playerX = Client.myPlayer.x + Camera.cameraOffsetX;
			int playerZ = Client.myPlayer.z + Camera.cameraOffsetY;
			if (cameraX - playerX < -500 || cameraX - playerX > 500 || cameraZ - playerZ < -500 || cameraZ - playerZ > 500) {
				cameraX = playerX;
				cameraZ = playerZ;
			}

			if (cameraX != playerX)
				cameraX += (playerX - cameraX) / 16;
			if (cameraZ != playerZ)
				cameraZ += (playerZ - cameraZ) / 16;

			cameraY = Client.method42(Client.plane, Client.myPlayer.z, Client.myPlayer.x) - 50;
		} else if (camType == CamType.FREE) {
			checkFreeCamPosition();

			do {
				int j = Client.instance.readChar();
				if (j == -1) {
					break;
				}

				
				switch (j) {
					case 32: // Space
						freeCamNeedPositioning = true;
						break;
					case 1002: // Page up
						if (freeCamNormalSpeed < 25) {
							freeCamNormalSpeed = Math.min(25, freeCamNormalSpeed + 1);
						}
						break;
					case 1003: // Page down
						if (freeCamNormalSpeed > 3) {
							freeCamNormalSpeed = Math.max(3, freeCamNormalSpeed - 1);
						}
						break;
					case 49: // 1
						transmitPosition(0);
						break;
					case 50: // 2
						transmitPosition(1);
						break;
					case 51: // 3
						transmitPosition(2);
						break;
					case 52: // 3
						transmitPosition(3);
						break;
				}
				System.out.println("j=" + j);
			} while (true);

            int angle = -1;
            if(Client.instance.keyArray[119] == 1) {
                angle = 0;
            } else if(Client.instance.keyArray[115] == 1) {
                angle = 1024;
            }

            if(Client.instance.keyArray[97] == 1) {
                if(angle == 0) {
                    angle = 1792;
                } else if(angle == 1024) {
                    angle = 1280;
                } else {
                    angle = 1536;
                }
            } else if(Client.instance.keyArray[100] == 1) {
                if(angle == 0) {
                    angle = 256;
                } else if(angle == 1024) {
                    angle = 768;
                } else {
                    angle = 512;
                }
            }

            int yStep = 0;
            if(Client.instance.keyArray[101] == 1) {
                yStep = -1;
            } else if(Client.instance.keyArray[102] == 1) {
                yStep = 1;
            }

            int movePixels = 0;
            if(angle >= 0 || yStep != 0) {
                movePixels = Client.holdingShiftKey ? freeCamShiftSpeed : freeCamNormalSpeed;
                movePixels *= 16;
                freeCamThisAngle = angle;
                freeCamThisYStep = yStep;
            }

            if (freeCamAngleStep < movePixels) {
            	freeCamAngleStep += movePixels / 8;
            	if (freeCamAngleStep > movePixels) {
            		freeCamAngleStep = movePixels;
            	}
            } else if (freeCamAngleStep > movePixels) {
            	freeCamAngleStep = freeCamAngleStep * 9 / 10;
            }

            if (freeCamAngleStep > 0) {
            	int step = freeCamAngleStep / 16;

            	if (freeCamThisAngle >= 0) {
            		int diff = freeCamThisAngle - Client.viewRotation & 2047;
            		int sin = Rasterizer.SINE[diff];
            		int cos = Rasterizer.COSINE[diff];
            		cameraX += sin * step / 65536;
            		cameraZ += cos * step / 65536;
            	}

            	if (freeCamThisYStep != 0) {
            		cameraY += step * freeCamThisYStep;
            		if (cameraY > 0) {
            			cameraY = 0;
            		}
            	}
            } else {
            	freeCamThisAngle = -1;
                freeCamThisYStep = -1;
            }

            if(Client.instance.keyArray[0] == 1) { // Esc
            	camType = CamType.PLAYER;
            }

            /*if(Client.instance.keyArray[80]) { // Tab
	            Class3_Sub13_Sub1.outgoingBuffer.putOpcode(5);
				Class3_Sub13_Sub1.outgoingBuffer.putByte(-1);
				Class3_Sub13_Sub1.outgoingBuffer.putShort(0);
				Class3_Sub13_Sub1.outgoingBuffer.putShort(0);
            }*/
		}

		if (Client.instance.clickMode2 == 3 && Client.variousSettings[ConfigCodes.MIDDLE_MOUSE_MOVE] == 0) {
			int deltaY;
			if (GraphicsDisplay.enabled) {
				deltaY = Client.instance.deltaY * 2;
				Client.instance.deltaY = 0;
			} else {
				deltaY = Client.instance.mouseY - Client.savedMouseY;
			}
			Client.anInt1187 = deltaY * 2;
			Client.savedMouseY = deltaY != -1 && deltaY != 1 ? (Client.savedMouseY + Client.instance.mouseY) / 2 : Client.instance.mouseY;
	
			int deltaX;
			if (GraphicsDisplay.enabled) {
				deltaX = Client.instance.deltaX * 2;
				Client.instance.deltaX = 0;
			} else {
				deltaX = Client.savedMouseX - Client.instance.mouseX;
			}
			Client.anInt1186 = deltaX * 2;
			Client.savedMouseX = deltaX != -1 && deltaX != 1 ? (Client.savedMouseX + Client.instance.mouseX) / 2 : Client.instance.mouseX;
		} else {
			if (Client.instance.keyArray[1] == 1)
				Client.anInt1186 += (-24 - Client.anInt1186) / 2;
			else if (Client.instance.keyArray[2] == 1)
				Client.anInt1186 += (24 - Client.anInt1186) / 2;
			else
				Client.anInt1186 /= 2;
			if (Client.instance.keyArray[3] == 1)
				Client.anInt1187 += (12 - Client.anInt1187) / 2;
			else if (Client.instance.keyArray[4] == 1)
				Client.anInt1187 += (-12 - Client.anInt1187) / 2;
			else
				Client.anInt1187 /= 2;
	
			Client.savedMouseX = Client.instance.mouseX;
			Client.savedMouseY = Client.instance.mouseY;
		}
	
		int lastViewRotation = Client.viewRotation;
		int lastAnInt1184 = Client.anInt1184;
		Client.viewRotation = Client.viewRotation + Client.anInt1186 / 2 & 0x7ff;
		Client.anInt1184 += Client.anInt1187 / 2;
		if (Client.anInt1184 < 128)
			Client.anInt1184 = 128;
		if (Client.anInt1184 > 383)
			Client.anInt1184 = 383;
	
		if (lastViewRotation != Client.viewRotation || lastAnInt1184 != Client.anInt1184) {
			Client.refreshGLMinimap = true;
		}
	
		fixCamera();
	}

	private static void checkFreeCamPosition() {
		if (freeCamNeedPositioning && Client.myPlayer != null) {
			int x = Client.myPlayer.smallX[0];
			int z = Client.myPlayer.smallY[0];
			if (x < 0 || z < 0 || x >= 104 || z >= 104) {
				return;
			}

			cameraX = Client.myPlayer.x;
			int tileHeight = Client.method42(Client.plane, Client.myPlayer.z, Client.myPlayer.x) - 50;
			if (tileHeight < cameraY) {
				cameraY = tileHeight;
			}

			cameraZ = Client.myPlayer.z;
			freeCamNeedPositioning = false;
		}
	}

}
