package com.soulplayps.client;

import java.util.Calendar;

public final class Config {
	
	public static boolean connectToLive = false;

	public static /*final*/ String SERVER_NAME = "SoulPlay";
	
	public static final String WEBSITE_URL = "http://www.soulplayps.com";
	
	public static final int CLIENT_VERSION = 31;
	
	public static final int CLIENT_VERSION_SUB = 1;
	
	public static String SERVER_IP =  connectToLive ? "8.26.94.80" : "127.0.0.1"; // "server1.soulplayps.com";

	//public static final String UPDATE_SERVER_IP = "127.0.0.1";
	public static final String UPDATE_SERVER_IP = "8.26.94.80";
	public static final int UPDATE_SERVER_PORT = 43595;

	public static final int JAGGRAB_PORT = 43595;
	public static final int ONDEMAND_PORT = 43594;
	
	public static final int SERVER_PORT = 43599; //43597
	
	public static final String SERVER_IP_EU = "europe1.soulplayps.com";
	
	public static final int SERVER_PORT_EU = 43592; //43597
	
	public static final String folderName = "soulplaycache";
	
	public static final int SERVER_PORT_PVP = 43591; //43597
	
	public static final String SERVER_IP_PVP = "pvp1.soulplayps.com";
	
	public static final int USERNAME_LENGTH = 12;
	
	public static final int PASSWORD_LENGTH = 20;
	
	public static final boolean DEBUG = false;
	
	public static final boolean USE_UPDATE_SERVER = false;

	public static boolean enableIds;
	
	/*
	 * Only things you need to change
	 *
	 */
	public static final int CACHE_VERSION = 25; // Version of cache
	
	public static final String cacheLink = "https://soulplayps.com/excludecf/cache182.zip"; // Link to cache
	public static final String cacheLink2 = "http://downloads.soulplayps.com/cache182.zip"; // Link to cache
	
	public static final String SPRITES_DATA = "https://www.soulplayps.com/excludecf/main_file_sprites.dat"; // Link to our packed sprites
	public static final String SPRITES_META = "https://www.soulplayps.com/excludecf/main_file_sprites.idx"; // Link to our packed sprites
	
	public static final int BACKGROUND_SIZE = 1;
	
	public static final String BACKGROUND_URL = "https://www.soulplayps.com/excludecf/background.png"; // Link to bg
	public static final String BACKGROUND2_URL = "https://www.soulplayps.com/excludecf/background2.png"; // Link to bg2

	public static Boolean isWinter() {
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		return month == Calendar.DECEMBER || month == Calendar.JANUARY;
	}

}
