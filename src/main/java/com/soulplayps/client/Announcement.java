package com.soulplayps.client;

public class Announcement {

	public final int addHour;
	public final String message;

	public Announcement(String message) {
		this.message = message;
		this.addHour = Client.calendar.get24Hour();
	}

}
