package com.soulplayps.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import com.soulplayps.client.data.SignLink;
import com.soulplayps.client.util.TextClass;

public class ErrorHandler {

	public static String errorOwner;

	public static final void submitError(String message, Throwable throwable) {
		try {
			String stackTrace = "";
			if (throwable != null) {
				stackTrace = formatStackTrace(throwable);
			}

			if (message != null) {
				if (throwable != null) {
					stackTrace = stackTrace + " | ";
				}

				stackTrace = stackTrace + message;
			}

			System.out.println("Error: " + TextClass.replaceAll(stackTrace, "%0a", "\n"));

			if (!Config.connectToLive) {
				return;
			}

			stackTrace = TextClass.replaceAll(stackTrace, ":", "%3a");
			stackTrace = TextClass.replaceAll(stackTrace, "@", "%40");
			stackTrace = TextClass.replaceAll(stackTrace, "&", "%26");
			stackTrace = TextClass.replaceAll(stackTrace, "#", "%23");
			DataInputStream dis = new DataInputStream(new URL(new URL("https://www.soulplayps.com/excludecf/"),
					"error.php?c1=" + Config.CLIENT_VERSION + "&c2=" + Config.CLIENT_VERSION_SUB + "&u=" + errorOwner + "&v1="
							+ SignLink.javaVendor + "&v2=" + SignLink.javaVersion + "&e=" + stackTrace).openStream());
			dis.read();
			dis.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	static final String formatStackTrace(Throwable throwable) throws IOException {
		String message = "";

		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		throwable.printStackTrace(printWriter);
		printWriter.close();

		String var5 = stringWriter.toString();
		BufferedReader var6 = new BufferedReader(new StringReader(var5));
		String errorLine = var6.readLine();

		while (true) {
			String stackTraceLine = var6.readLine();

			if (stackTraceLine == null) {
				message = message + "| " + errorLine;
				return message;
			}

			int openingBracket = stackTraceLine.indexOf('(');
			int closingBracket = stackTraceLine.indexOf(')', openingBracket + 1);
			String var11;
			if (openingBracket == -1) {
				var11 = stackTraceLine;
			} else {
				var11 = stackTraceLine.substring(0, openingBracket);
			}

			var11 = var11.trim();
			var11 = var11.substring(1 + var11.lastIndexOf(32));
			var11 = var11.substring(var11.lastIndexOf(9) + 1);
			message = message + var11;
			if (openingBracket != -1 && closingBracket != -1) {
				int var12 = stackTraceLine.indexOf(".java:", openingBracket);
				if (var12 >= 0) {
					message = message + stackTraceLine.substring(5 + var12, closingBracket);
				} else {
					var12 = stackTraceLine.indexOf(":", openingBracket);
					if (var12 >= 0) {
						message = message + '(' + stackTraceLine.substring(openingBracket + 1, closingBracket) + ')';
					}
				}
			}

			message = message + ' ';
		}
	}

}