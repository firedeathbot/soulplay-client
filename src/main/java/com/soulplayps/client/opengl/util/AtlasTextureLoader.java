package com.soulplayps.client.opengl.util;

import java.util.LinkedList;
import java.util.List;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.atlas.AtlasTexture;

public class AtlasTextureLoader
{

	// actions that can happen when a texture has loaded
	// e.g. loading it onto the GPU
	private static List<AtlasTexture> texturesToLoad;
	private static List<AtlasTexture> texturesToLoadDeletes;
	private static List<Integer> texturesLoading;

	static {
		if (GraphicsDisplay.enabled) {
			init();
		}
	}

	public static void init() {
		texturesToLoad = new LinkedList<AtlasTexture>();
		texturesToLoadDeletes = new LinkedList<AtlasTexture>();
		texturesLoading = new LinkedList<Integer>();
	}

	public static void cleanup() {
		texturesToLoad = null;
		texturesToLoadDeletes = null;
		texturesLoading = null;
	}

	private static int hash(AtlasTexture at)
	{
		return (at.getTextureID() << 16) + (at.getAtlas().getAtlasListId());
	}
	
	public static boolean addTexture(AtlasTexture at)
	{
		if (texturesLoading.indexOf(hash(at)) != -1)
			return false;
		
		synchronized (texturesToLoad)
		{
			texturesToLoad.add(at);
		}
		
		return true;
	}
	
	public static void clear()
	{
		synchronized(texturesToLoad)
		{
			texturesToLoadDeletes.clear();
			texturesToLoad.clear();
			texturesLoading.clear();
		}
	}
	
	private static long lastCheck = 0;
	public static void process()
	{
		if (System.currentTimeMillis() - lastCheck < 100)
			return;
		lastCheck = System.currentTimeMillis();
		
		// don't load textures while the game world is loading
		if (Client.loadingStage != 2)
			return;
		
		int c = 0;
		int max = 3;
		synchronized (texturesToLoad)
		{
			for (AtlasTexture at : texturesToLoad)
			{
				Texture temp = Texture.get(at.getTextureID());
				if (temp != null)
				{
					at.setWidth(128);
					at.setHeight(128);
					
					if (c++ > max)
						break;
	
					if (Cache.textureForAtlas(at))
					{
						texturesToLoadDeletes.add(at);
						//System.out.println("post loading: "+at.getTextureID()+", "+temp.width);
					}
	
				}
			}
	
			// delete successfully loaded textures
			for (AtlasTexture texture : texturesToLoadDeletes)
				texturesToLoad.remove(texture);
			texturesToLoadDeletes.clear();
		}
	}
	
}
