package com.soulplayps.client.opengl.util;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.raster.RSFont;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl14.Quad;
import com.soulplayps.client.opengl.gl14.Triangle;
import com.soulplayps.client.opengl.gl15.model.ModelVBO;
import com.soulplayps.client.opengl.gl15.model.Scissor;
import com.soulplayps.client.opengl.gl15.model.TextureAtlas;
import com.soulplayps.client.opengl.gl15.model.TextureAtlasList;
import com.soulplayps.client.opengl.gl15.model.atlas.AtlasPosition;
import com.soulplayps.client.opengl.gl15.model.atlas.AtlasTexture;
import com.soulplayps.client.opengl.gl15.model.simple.ModelPosition;
import com.soulplayps.client.util.BitwiseHelper;
import com.soulplayps.client.util.Long2ObjectEntry;
import it.unimi.dsi.fastutil.ints.Int2ObjectRBTreeMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.longs.*;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import it.unimi.dsi.fastutil.objects.ObjectSet;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Cache {
	
	public static boolean removeOldVBOModels = false;
	public static boolean clearVBOModels = false;
	
	// a list containing buffers that need to be deleted
	private static final IntList buffersToClean = new IntArrayList();
	
	public static void clearMap(final Long2IntMap data, final int type) {
		buffersToClean.addAll(data.values());
		GLBufferManager.deleteTextureBuffer(type, buffersToClean);
		buffersToClean.clear();
		data.clear();
	}
	
	
	// the usage of final variables in this class are speeding up rendering
	// some methods are called multiple thousands of times per drawing cycle
	// that means that tiny adjustments have impact on rendering speed
	// we're gaining about 2-3fps with final variables
	
	
	// textured triangle/quad caching
	// it's more efficient to draw textured triangles with the same texture id
	// at once, because we need 1 glBegin() and glEnd() block PER texture
	// that way we minimize the amount of data transfer between GPU and CPU
	
	// we're using a simple one-directional linked list mechanic for storage
	public static Triangle[][] triangles = null;
	public static Triangle[][] trianglesAlpha = null;
	public static Int2ObjectRBTreeMap<Triangle> trianglesAlphaMap = new Int2ObjectRBTreeMap<Triangle>();
	public static Triangle[] floorTriangles = null;
	public static Triangle[] floorTrianglesAlpha = null;
	public static Quad[] quads = null;
	public static Quad[] quadsAlpha = null;
	
	private static int ATLAS_SIZE = 0;
	
	static {
		ATLAS_SIZE = GraphicsDisplay.MAX_TEXTURE_SIZE / 2;
		
		// GPU memory too small for atlas
		if (ATLAS_SIZE < 512)
			ATLAS_SIZE = 32;
			
			// atlas should be no bigger than that
		else if (ATLAS_SIZE > 4096)
			ATLAS_SIZE = 4096;
		
		initStreamingArrays();
	}
	
	// TODO: unused
	public static void updateMapDecorationOffsets(int dx, int dy) {
		VBOManager.atlasModels.updateMapDecorationOffsets(dx, dy);
	}
	
	/*public static void clearMapDecorations(int ex, int ey)
	{
		AtlasModelLoader.clearMapDecorations(ex, ey);
	}*/
	
	public static void nullLoader() {
		triangles = null;
		trianglesAlpha = null;
		floorTriangles = null;
		floorTrianglesAlpha = null;
		quads = null;
		quadsAlpha = null;
	}
	
	public static void initStreamingArrays() {
		if (!GraphicsDisplay.enabled || GraphicsDisplay.vboRendering)
			nullLoader();
		else {
			triangles = new Triangle[Model.pools][Texture.COUNT];
			trianglesAlpha = new Triangle[Model.pools][Texture.COUNT];
			floorTriangles = new Triangle[Texture.COUNT];
			floorTrianglesAlpha = new Triangle[Texture.COUNT];
			quads = new Quad[Texture.COUNT];
			quadsAlpha = new Quad[Texture.COUNT];
			
			for (int pool = 0; pool < Model.pools; pool++) {
				triangles[pool] = new Triangle[Texture.COUNT];
				trianglesAlpha[pool] = new Triangle[Texture.COUNT];
			}
		}
		
	}
	
	public static void softClear() {
		//glBindTexture(GL_TEXTURE_2D, 0);
		
		clearSpriteCache();
		//clearFloorVBOs();
		clearAtlases(false);
		
	}
	
	public static void hardClear() {
		//glBindTexture(GL_TEXTURE_2D, 0);
		
		clearBackgroundCache();
		clearLetterCache();
		
		// clearing textures isn't really necessary, done automatically
		clearTextureCache();
		
		softClear();
	}
	
	public static void releaseAtlases() {
		clearAtlases(true);
	}
	
	private static void clearAtlases(boolean hard) {
		if (hard) {
			sprites32.delete(hard);
			sprites64.delete(hard);
			letterAtlas.delete(hard);
			letters.clear();
			
			if (groundTextureAtlas128 != null)
				groundTextureAtlas128.delete(hard);
			if (modelTextureAtlas128 != null)
				modelTextureAtlas128.delete(hard);
			if (modelTextureAtlas64 != null)
				modelTextureAtlas64.delete(hard);
		}
	}
	
	public static final Long2IntMap spriteCache = new Long2IntOpenHashMap();
	
	public static long imageHash(final int offX, final int offY, final int width, final int height, final long uid) {
		if (uid > 0)
			return uid;
		else
			return (offX << 48) + (offY << 32) + (width << 16) + (height << 0);
	}
	
	public static void clearSpriteCache() {
		clearMap(spriteCache, GLBufferManager.TEXTURE_SPRITE);
	}
	
	public static void clearSprites(LongList uids) {
		for (int i = 0; i < uids.size(); i++) {
			long uid = uids.getLong(i);
			int value = spriteCache.get(uid);
			if (value != 0) {
				//System.out.println("removed: "+uid);
				spriteCache.remove(uid);
				buffersToClean.add(value);
			}
		}
		
		GLBufferManager.deleteTextureBuffer(GLBufferManager.TEXTURE_SPRITE, buffersToClean);
		buffersToClean.clear();
	}
	
	// atlases for sprites that are <= 48x48 pixels. we also store
	// them in different atlases to save some space.
	// <= 16x16 pixels
	// <= 32x32 pixels
	// <= 48x48 pixels
	// <= 64x64 pixels
	// any larger sprites aren't put into an atlas
	// number of slots: (512x512) / (32x32x)
	public static TextureAtlasList sprites32 = new TextureAtlasList(512, 32);
	public static TextureAtlasList sprites64 = new TextureAtlasList(512, 64);
	public static TextureAtlasList sprites128 = new TextureAtlasList(512, 128);
	public static ObjectList<AtlasPosition> sprites = new ObjectArrayList<AtlasPosition>();
	
	// initialized in GraphicsDisplay, depending on the GPUs maximum texture
	// size. can also remain null, if the GPU has a too small texture storage.
	// in that case we allocate 1 buffer per texture
	public static TextureAtlasList groundTextureAtlas128 = null;
	public static TextureAtlasList modelTextureAtlas128 = null;
	public static TextureAtlasList modelTextureAtlas64 = null;
	
	public static boolean addAtlasSprite(long uid, int x, int y, int alpha, int width, int height, Sprite sprite) {
		// interface underlay sprites
		// putting them in the atlas screws z-buffering of cut sprites
		if (width == 88 && height == 60 && uid == 8992214652205303814L) {
			//System.out.println("uid: "+uid);
			return false;
		}
		
		TextureAtlasList atlas = null;
		
		if (width <= 32 && height <= 32)
			atlas = sprites32;
		else if (width <= 64 && height <= 64)
			atlas = sprites64;
		else if (width <= 128 && height <= 128)
			atlas = sprites128;
		
		if (atlas == null) {
			//System.out.println("no atlas found: "+width+"/"+height);
			return false;
		}
		
		return atlas.addSprite(uid, x, y, alpha, width, height, sprite);
	}
	
	// returns dynamic atlas sprite numbers
	private static long dynamicSpriteId = 1 << 20;
	
	public static long getDynamicAtlasSpriteId() {
		return dynamicSpriteId++;
	}
	
	public static void resetDynamicAtlasSpriteIds() {
		dynamicSpriteId = 1 << 20;
	}
	
	// letter atlas: 512x512 bytes
	public static TextureAtlas letterAtlas = new TextureAtlas(512, 16);
	public static ObjectList<AtlasPosition> letters = new ObjectArrayList<AtlasPosition>();
	
	public static void addAtlasLetter(long uid, int x, int y, int color, int alpha, byte[] data, int width, int height, RSFont tda, boolean update, Scissor scissor) {
		long offsets = letterAtlas.addLetter(uid, data, width, height, update);
		if (offsets != -1)
			letters.add(new AtlasPosition(color, alpha, x, y, uid,
					BitwiseHelper.long2IntA(offsets), BitwiseHelper.long2IntB(offsets), width, height, scissor));
	}
	
	public static void addAtlasBox(long uid, int x, int y, int color, int width, int height, int alpha, Scissor scissor) {
		long offsets = letterAtlas.addBox(uid);
		if (offsets != -1)
			letters.add(new AtlasPosition(color, alpha, x, y, uid,
					BitwiseHelper.long2IntA(offsets), BitwiseHelper.long2IntB(offsets), width, height, scissor));
	}

	/*public static void updateModelVBOs(int ex, int ey){
		System.out.println("updating: "+ex+"/"+ey);
		
		if (true)
			return;
		
		// nothing needs to be done
		//if (ex == 0 && ey == 0)
		//	return;
		
		final List<Long[]> updates = new LinkedList<Long[]>();
		int deleted = 0;
		int updated = 0;
		int ignored = 0;
		
		for (Entry<Long, ModelVBO> e : modelVBOs.entrySet()) {
			final long uid = e.getKey();
			final ModelVBO vbo = e.getValue();
			
			// ignore these vbos. they are already cleared or deleted
			if (vbo.handlesReleased || vbo.delete || vbo.clear) {
				deleted++;
				continue;
			}
			
			//if (true) {
			//	vbo.clear = true;
			//}
			
			// kept anyway
			if (!vbo.hasPositionHash()) {
				ignored++;
				continue;
			}

			// no position hash updates necessary
			if (ex == 0 && ey == 0)
				continue;

			// add new position
			long newUid = uid >> 14L;
			newUid <<= 14L;
			final long pos = (vbo.getRegionX()+ex) + ((vbo.getRegionY()+ey) << 7L);
			newUid |= pos;
			updated++;
			
			if (Math.random() < 0.001)
				System.out.println(Long.toBinaryString(newUid)+" - "+Long.toBinaryString(uid)+": "+vbo.getRegionX()+"/"+vbo.getRegionY()+"; "+Long.toBinaryString(pos));


			// add new uid
			if (newUid != uid) {
			//	updates.add(new Long[]{uid, newUid});
			}
				
		}
		
		// update vbos
		int kept = 0;
		for (Long[] uids : updates) {
			final ModelVBO vbo = modelVBOs.get(uids[0]);
			
			if (!modelVBOs.containsKey(uids[1])) {
				modelVBOs.put(uids[1], vbo);
				modelVBOs.remove(uids[0]);
				vbo.updateRegionCoords(ex, ey);
				kept++;
			}
		}
		
		removeOldVBOModels = true;
		
		if (Client.showDeveloperInfos)
			System.out.println("[Cache]: keeping ModelVBOs: ["+updates.size()+","+kept+"] / "+modelVBOs.size()+", del: "+deleted+", upd: "+updated+", ign: "+ignored);
	}*/
	
	private static Long2ObjectMap<ModelVBO> modelVBOs = new Long2ObjectOpenHashMap<ModelVBO>();
	
	public static void addModelVBO(long uid, ModelVBO vbo) {
		ModelVBO put = modelVBOs.put(uid, vbo);
		if (put != null)
			System.err.println("[Cache]: duplicate model vbo: " + vbo.getObjectId() + ", " + vbo.getTriangles() + "");
		/*if (modelVBOs.get(uid) == null)
			modelVBOs.put(uid, vbo);
			*/
	}
	
	public static Long2ObjectMap<ModelVBO> getModelVBOs() {
		return modelVBOs;
	}
	
	private static Long2ObjectMap<ModelVBO> floorVBOs = new Long2ObjectOpenHashMap<ModelVBO>();
	
	public static void addFloorVBO(final long uid, final ModelVBO floor) {
		if (floorVBOs.get(uid) == null)
			floorVBOs.put(uid, floor);
		else
			System.err.println("[Cache]: duplicate floor vbo: " + floor.getObjectId() + ", " + floor.getTriangles() + "");
	}
	
	public static Long2ObjectMap<ModelVBO> getFloorVBOs() {
		return floorVBOs;
	}
	
	public static Long2ObjectMap<ObjectList<ModelPosition>> modelPositions = new Long2ObjectOpenHashMap<ObjectList<ModelPosition>>();
	public static Long2ObjectMap<ObjectList<ModelPosition>> modelPositionsAlpha = new Long2ObjectOpenHashMap<ObjectList<ModelPosition>>();
	public static Long2ObjectMap<ObjectList<ModelPosition>> modelPositionsShadow = new Long2ObjectOpenHashMap<ObjectList<ModelPosition>>();
	public static Long2ObjectMap<ObjectList<ModelPosition>> floorPositions = new Long2ObjectOpenHashMap<ObjectList<ModelPosition>>();
	
	private static void addVBO(final long uid, final ModelVBO vbo, final Long2ObjectMap<ObjectList<ModelPosition>> map) {
		addVBO(uid, 0, 0, 0, 0, 0, 0, vbo, map);
	}
	
	private static void addVBO(final long uid, final int x, final int y, final int z, final int xRot, final int yRot, final int zRot, final ModelVBO vbo, final Long2ObjectMap<ObjectList<ModelPosition>> map) {
		addVBO(uid, x, y, z, xRot, yRot, zRot, vbo, map, 0, 1f, 1f, 1f, null);
	}
	
	private static void addVBO(final long uid, final float x, final float y, final float z, final int xRot, final int yRot, final int zRot, final ModelVBO vbo, final Long2ObjectMap<ObjectList<ModelPosition>> map, final int collisionColor, final float scaleX, final float scaleY, final float scaleZ, Scissor scissor) {
		final ModelPosition pos = new ModelPosition(x, y, z, xRot, yRot, zRot, vbo, collisionColor, scaleX, scaleY, scaleZ, scissor);
		ObjectList<ModelPosition> value = map.get(uid);
		if (value == null) {
			value = new ObjectArrayList<ModelPosition>();
			map.put(uid, value);
		}

		value.add(pos);
	}
	
	public static void addModel(final long uid, final float x, final float y, final float z, final int xRot, final int yRot, final int zRot, final ModelVBO vbo, final float scaleX, final float scaleY, final float scaleZ) {
		addVBO(uid, x, y, z, xRot, yRot, zRot, vbo, vbo.isHasAlpha() ? modelPositionsAlpha : modelPositions, 0, scaleX, scaleY, scaleZ, null);
	}
	
	public static void addModel(final long uid, final float x, final float y, final float z, final int xRot, final int yRot, final int zRot, final ModelVBO vbo, final float scaleX, final float scaleY, final float scaleZ, Scissor scissor) {
		addVBO(uid, x, y, z, xRot, yRot, zRot, vbo, vbo.isHasAlpha() ? modelPositionsAlpha : modelPositions, 0, scaleX, scaleY, scaleZ, scissor);
	}
	
	public static void addModelShadow(final long uid, final int x, final int y, final int z, final int xRot, final int yRot, final int zRot, final ModelVBO vbo, final float scaleX, final float scaleY, final float scaleZ) {
		addVBO(uid, x, y, z, xRot, yRot, zRot, vbo, modelPositionsShadow, 0, scaleX, scaleY, scaleZ, null);
	}
	
	public static void clearModel(long uid) {
		final ModelVBO vbo = modelVBOs.get(uid);
		if (vbo != null)
			vbo.clear = true;
		/*else
		{
			System.err.println("Cannot clear ModelVBO: "+uid);
			new Throwable().printStackTrace();
		}*/
	}
	
	public static void addFloor(final long uid, final ModelVBO vbo) {
		addVBO(uid, vbo, floorPositions);
	}
	
	public static void flushFloorVBOs() {
		if (!clearFloorVBOs)
			return;
		
		clearFloorVBOs = false;
		VBOManager.models.clearColors();
		
		for (ModelVBO vbo : floorVBOs.values()) {
			if (vbo.handlesReleased)
				continue;
			
			vbo.handlesReleased = true;
			
			if (vbo.getVertexHandle() != 0)
				buffersToClean.add(vbo.getVertexHandle());
			if (vbo.getColorHandle() != 0)
				buffersToClean.add(vbo.getColorHandle());
			if (vbo.getUvHandle() != 0)
				buffersToClean.add(vbo.getUvHandle());
		}
		
		GLBufferManager.deleteGenBuffers(buffersToClean);
		buffersToClean.clear();
		floorVBOs.clear();
	}
	
	private static boolean clearFloorVBOs = false;
	
	public static void clearFloorVBOs() {
		clearFloorVBOs = true;
	}
	
	public static long lastVBODeletionCheck = 0;
	private static long ACTIVE_TIMER = 20000;
	
	private static Queue<Long2ObjectEntry<ModelVBO>> deleteQueue = new ConcurrentLinkedQueue<Long2ObjectEntry<ModelVBO>>();
	
	public static void clearModelVBOs() {
		Long2ObjectEntry<ModelVBO> entry;
		while ((entry = deleteQueue.poll()) != null) {
			// need to try catch, later maybe can get the entries synced
			try {
				final ModelVBO vbo = entry.getValue();
				if (vbo != null && !vbo.handlesReleased) {
					vbo.handlesReleased = true;
					
					if (vbo.getVertexHandle() != 0)
						buffersToClean.add(vbo.getVertexHandle());
					if (vbo.getColorHandle() != 0)
						buffersToClean.add(vbo.getColorHandle());
					if (vbo.getUvHandle() != 0)
						buffersToClean.add(vbo.getUvHandle());
				}
				
				final long uid = entry.getKey();
				modelVBOs.remove(uid);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		GLBufferManager.deleteGenBuffers(buffersToClean);
		buffersToClean.clear();
	}
	
	private static void clearMap(final long now, final ObjectSet<Long2ObjectMap.Entry<ModelVBO>> entries) {
		int deleted = 0;
		int maxDel = 1000;
		//if (true)
		//	return;
		
		
		// System.out.println("check");
		for (final Long2ObjectMap.Entry<ModelVBO> entry : entries) {
			final long uid = entry.getLongKey();
			final ModelVBO vbo = entry.getValue();
			
			// already marked for deletion
			//if (vbo.isAreaBuffer && !vbo.clear)
			//	continue;
			
			//System.out.println("check: "+vbo.getWorldX()+" / "+vbo.getWorldY());
			if (clearVBOModels || vbo.clear || now - vbo.lastUse > ACTIVE_TIMER || Client.distanceToMe(vbo.getWorldX(), vbo.getWorldY()) > 60) {
				vbo.delete = true;
				deleteQueue.add(new Long2ObjectEntry<ModelVBO>(uid, vbo));
				
				if (!clearVBOModels && deleted++ >= maxDel)
					break;
			}
		}
	}
	
	public static void manageVBOs() {
		if (Client.loadingStage != 2)
			return;
		
		final long now = System.currentTimeMillis();
		
		// position takes care of VBO removal. this timer only ensures
		// that no VBO is left over
		ACTIVE_TIMER = 60000;//Client.fullyLoggedIn || !Client.drawAnimatedWorldBackground ? 300000 : 10000;
		
		if (now - lastVBODeletionCheck > 5000 || removeOldVBOModels) {
			// clear floors
			if (removeOldVBOModels)
				clearFloorVBOs();
			
			ObjectSet<Long2ObjectMap.Entry<ModelVBO>> entries = modelVBOs.long2ObjectEntrySet();
			clearMap(now, entries);
			
			lastVBODeletionCheck = now;
			removeOldVBOModels = false;
			clearVBOModels = false;
		}
	}
	
	
	public static Long2IntMap backgroundCache = new Long2IntOpenHashMap();
	
	public static void clearBackgroundCache() {
		clearMap(backgroundCache, GLBufferManager.TEXTURE_BACKGROUND);
	}
	
	public static Long2IntMap letterCache = new Long2IntOpenHashMap();
	
	public static long letterHash(final int c, final int tdaID, final int update) {
		//return (tdaID << 60) + (c << 32);
		return (update << 65) + (tdaID << 60) + (c << 32);
	}
	
	public static void clearLetterCache() {
		clearMap(letterCache, GLBufferManager.TEXTURE_LETTER);
	}
	
	
	// key is the texture id, value is the texture buffer id on the GPU
	public static int[] textureCache = new int[Texture.COUNT];
	public static int[] textureUses = new int[Texture.COUNT];
	
	public static void clearTextureCache() {
		for (int i = 0; i < textureCache.length; i++) {
			if (textureCache[i] != 0)
				buffersToClean.add(textureCache[i]);
			textureCache[i] = 0;
			textureUses[i] = 0;
		}
		
		GLBufferManager.deleteTextureBuffer(GLBufferManager.TEXTURE_TEXTURE, buffersToClean);
		buffersToClean.clear();
		
	}
	
	private static long lastTextureCleanup = System.currentTimeMillis();
	
	public static void cleanupTextures() {
		if (System.currentTimeMillis() - lastTextureCleanup < 1000)
			return;
		lastTextureCleanup = System.currentTimeMillis();
		
		for (int i = 0; i < textureCache.length; i++) {
			// texture not being used anymore for 30 seconds
			// we can free up that buffer
			if (textureCache[i] != 0 && textureUses[i] <= -30) {
				//System.out.println("cleanup: "+i);
				buffersToClean.add(textureCache[i]);
				textureCache[i] = 0;
			}
			
			if (textureUses[i] > 0)
				textureUses[i] = 0;
			else
				textureUses[i]--;
			
		}
		
		GLBufferManager.deleteTextureBuffer(GLBufferManager.TEXTURE_TEXTURE, buffersToClean);
		buffersToClean.clear();
	}
	
	public static int loadTexture(int id, boolean isFloor) {
		if (id >= textureCache.length) {
			return 0;
		}
		
		int texID = textureCache[id];
		boolean cached = true;
		
		// images are textures --> load image texture
		if (texID == 0 || VBOManager.isAnimated(id)) {
			cached = false;
			
			//System.out.println("miss!");
			Texture texture = Texture.get(id);
			final int[] pixels = Rasterizer.method371(id);
			if (pixels == null || texture == null)
				return 0;
			
			int alpha = texture.hasAlpha ? 0 : 255; // 0 means load from RGBA-A value
			
			// no transparent textures, we don't need them
			// because openGL is taking color values for
			// textures as well!
			alpha = 255;
			
			// animated textures are reloaded here
			// this isn't the proper way of doing it
			if (texID != 0 && VBOManager.isAnimated(id))
				texID = VBOManager.loadTextureRGBA(true, texID, pixels, texture.width, texture.height, alpha, true, id);
			else
				texID = VBOManager.loadTextureRGBA(pixels, texture.width, texture.height, alpha, true, id);
			textureCache[id] = texID;
		}
		
		if (textureUses[id] < 0)
			textureUses[id] = 1;
		else
			textureUses[id]++;
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in loadTexture(" + texID + ", " + id + ", " + cached + ")");
		}
		
		return texID;
	}
	
	
	public static boolean textureForAtlas(AtlasTexture at) {
		int[] pixels = Rasterizer.method371(at.getTextureID());
		if (pixels == null)
			return false;
		
		at.getAtlas().addTexture(at.getTextureID(), pixels, 255, at.getWidth(), at.getHeight(), true, true, at.getTextureID(), at.isFloor());
		return true;
	}
	
	public static TextureAtlas loadAtlasTextures(boolean isFloor, IntList textures) {
		int width = 128;
		int height = 128;
			
		/*TextureTemplate texture = TextureTemplate.get(id);
		if (texture != null)
		{
			width = texture.width;
			height = texture.height;
		}*/
		
		if (isFloor)
			return groundTextureAtlas128.addTextures(width, height, isFloor, textures);
		else
			return modelTextureAtlas128.addTextures(width, height, isFloor, textures);
	}
	
	private static int[] textureCount = new int[Texture.COUNT];
	private static int[] lastTextureCount = new int[Texture.COUNT];
	
	public static void inc(int textureId) {
		textureCount[textureId]++;
	}
	
	public static void clearTextureCount() {
		for (int i = 0; i < textureCount.length; i++) {
			lastTextureCount[i] = textureCount[i];
			textureCount[i] = 0;
		}
	}
	
	public final static int THRESHOLD = 256;
	
	public static boolean useVbo(int textureId) {
		int value = lastTextureCount[textureId];
		return value > THRESHOLD; // threshold
	}
	
	
	// we only need to clear the first list entries
	// the renderer is destroying the linked lists
	// when reading them
	public static void clearTexturedObjects() {
		for (int texture = 0; texture < Texture.COUNT; texture++) {
			for (int pool = 0; pool < 1; pool++)//Model.pools; pool++)
			{
				triangles[pool][texture] = null;
				trianglesAlpha[pool][texture] = null;
			}
			
			floorTriangles[texture] = null;
			floorTrianglesAlpha[texture] = null;
			quads[texture] = null;
			quadsAlpha[texture] = null;
		}
	}
	
	public static void addQuad(final int texture,
	                           final float x1, final float x2, final float x3, final float x4,
	                           final float y1, final float y2, final float y3, final float y4,
	                           final float z1, final float z2, final float z3, final float z4,
	                           final int c1, final int c2, final int c3, final int c4, final int alpha,
	                           final int pool) {
		Quad tq = new Quad(texture, x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4, c1, c2, c3, c4, alpha);
		
		if (tq.alpha >= 255) {
			Quad last = quads[texture];
			tq.next = last;
			quads[texture] = tq;
		} else {
			Quad last = quadsAlpha[texture];
			tq.next = last;
			quadsAlpha[texture] = tq;
		}
	}
	
	
	public static void addTriangle(
			final float x1, final float x2, final float x3,
			final float y1, final float y2, final float y3,
			final float z1, final float z2, final float z3,
			final int c1, final int c2, final int c3, final int alpha,
			final int pool) {
		addTriangle(0, x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha, pool);
	}
	
	public static void addTriangle(final int texture,
	                               final float x1, final float x2, final float x3,
	                               final float y1, final float y2, final float y3,
	                               final float z1, final float z2, final float z3,
	                               final int c1, final int c2, final int c3, final int alpha,
	                               final int pool) {
		addTriangle(texture, x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha, -1000, 0, 0, 0, 0, 0, pool);
	}
	
	public static void addTriangle(final int texture,
	                               final float x1, final float x2, final float x3,
	                               final float y1, final float y2, final float y3,
	                               final float z1, final float z2, final float z3,
	                               final int c1, final int c2, final int c3, final int alpha,
	                               float u1, float u2, float u3,
	                               float v1, float v2, float v3,
	                               int pool) {
		
		boolean hasAlpha = alpha < 255;
		Texture def = Texture.get(texture);
		hasAlpha |= (texture >= Rasterizer.textureAmount && def != null);// && def.hasAlpha);//TODO
		hasAlpha |= texture < Rasterizer.textureAmount && texture > 0;
		
		// alpha values, no pooling for those
		//if (alpha < 255)
		//	pool = Model.DEFAULT_POOL;
		
		// animated textures, e.g. magic tree
		// the animation used here is a pulsation
		// all others just looked weird :(
		if ((texture == 34) && def != null) {
			int speed = 50;
			int height = def.height;
			
			float progress = (float) ((System.currentTimeMillis() / speed) % def.height);
			progress /= (float) height;
			
			if (progress > 0.5f) {
				progress = 1 - progress;
			}
			
			v1 *= 1 - progress;
			v2 *= 1 - progress;
			v3 *= 1 - progress;
		}
		
		Triangle tt;
		if (u1 != -1000)
			tt = new Triangle(texture, x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha, u1, u2, u3, v1, v2, v3);
		else
			tt = new Triangle(texture, x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha);
		
		if (!hasAlpha) {
			Triangle last = triangles[pool][texture];
			tt.next = last;
			triangles[pool][texture] = tt;
		} else {
			/*if (Model.usingPooledRendering())
			{
				synchronized(trianglesAlphaMap)
				{
					int key = ((int) (z2/GraphicsDisplay.SF)) + (texture << 16);
					Triangle last = trianglesAlphaMap.get(key);
					tt.next = last;
					
					trianglesAlphaMap.put(key, tt);
				}
			}
			else
			{*///TODO not using pooling
			Triangle last = trianglesAlpha[pool][texture];
			tt.next = last;
			trianglesAlpha[pool][texture] = tt;
			//}
		}
	}
	
	private static float max(Float... values) {
		float max = 0f;
		
		for (float f : values)
			if (f > max)
				max = f;
		
		return max;
	}
	
	public static void addFloorTriangle(final int texture,
	                                    final float x1, final float x2, final float x3,
	                                    final float y1, final float y2, final float y3,
	                                    final float z1, final float z2, final float z3,
	                                    final int c1, final int c2, final int c3, int alpha,
	                                    final float u1, final float u2, final float u3,
	                                    final float v1, final float v2, final float v3,
	                                    final int pool
	) {
		/*if (true)
		{
			addTriangle(texture, x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha, u1, u2, u3, v1, v2, v3, 1);
			return;
		}*/
		
		Triangle tt;
		if (u1 != -1000)
			tt = new Triangle(texture, x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha, u1, u2, u3, v1, v2, v3);
		else
			tt = new Triangle(texture, x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha);
		
		if (tt.alpha >= 255) {
			Triangle last = floorTriangles[texture];
			tt.next = last;
			floorTriangles[texture] = tt;
		} else {
			Triangle last = floorTrianglesAlpha[texture];
			tt.next = last;
			floorTrianglesAlpha[texture] = tt;
		}
	}
	
	public static void addFloorTriangle(final int texture,
	                                    final float x1, final float x2, final float x3,
	                                    final float y1, final float y2, final float y3,
	                                    final float z1, final float z2, final float z3,
	                                    final int c1, final int c2, final int c3, final int alpha,
	                                    final int pool
	) {
		addFloorTriangle(texture, x1, x2, x3, y1, y1, y3, z1, z2, z3, c1, c2, c3, alpha, -1000, 0, 0, 0, 0, 0, pool);
	}
}
