package com.soulplayps.client.opengl.util;

import static org.lwjgl.opengl.GL11.*;

import java.nio.ByteBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import com.soulplayps.client.Client;
import com.soulplayps.client.node.object.WorldController;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.ModelVBO;
import com.soulplayps.client.opengl.gl15.model.Scissor;
import com.soulplayps.client.opengl.gl15.model.atlas.AtlasModelLoader;
import com.soulplayps.client.opengl.gl15.model.simple.ModelLoader;

public class VBOManager {

	public static AtlasModelLoader atlasModels;
	public static ModelLoader models;
	public static ByteBuffer buffer;

	public static void init() {
		atlasModels = new AtlasModelLoader();
		models = new ModelLoader();
		
		// one buffer that is large enough for all possible textures and background images
		buffer = BufferUtils.createByteBuffer(1920*1080*4);
	}

	public static void cleanup() {
		buffer = null;
		atlasModels = null;
		models = null;
	}
	
	static {
		if (GraphicsDisplay.enabled) {
			init();
		}
	}

	// loading texture from byte array --> fonts
	public static int loadLetterTexture(final byte[] letter, int width, int height, int color)
	{
		//if (letter.length < 30)
		//	return 0;

		//System.out.println ("len: "+letter.length+", size: "+width+"/"+height);
		
		for (int y = height-1; y >= 0; y--)
		{
			for (int x = 0; x < width; x++)
			{
				byte pixel = letter[y * width + x];
				// we can set the color here
				
				buffer.put((byte) ((color >> 16) & 0xFF)); // R
				buffer.put((byte) ((color >> 8) & 0xFF)); // G
				buffer.put((byte) ((color >> 0) & 0xFF)); // B
				
				// ignore black pixels around letters
				if (pixel <= 0 || pixel >= 0xFFFFFF)
				{
					buffer.put((byte) 0);
				}
				else
				{
					// this is the letter data
					buffer.put((byte) 250);
				}
			}
		}
		
		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS

		// You now have a ByteBuffer filled with the color data of each pixel.
		// Now just create a texture ID and bind it. Then you can load it using
		// whatever OpenGL method you want, for example:

		int texID = GLBufferManager.genTextureBuffer(GLBufferManager.TEXTURE_LETTER); // Generate texture ID
		GraphicsDisplay.setTexture(texID);

		// Setup wrap mode
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

		// Setup texture scaling filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		// Send texel data to OpenGL
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		buffer.clear();
		
		// Return the texture ID so we can bind it later again
		return texID;
	}
	
	public static void addModelShadow(long uid, int x, int y, int z, int xRot, int yRot, int zRot, ModelVBO vbo, float scaleX, float scaleY, float scaleZ)
	{
		Cache.addModelShadow(uid, x, y, z, xRot, yRot, zRot, vbo, scaleX, scaleY, scaleZ);
	}
	
	public static void addModel(long uid, float x, float y, float z, int xRot, int yRot, int zRot, ModelVBO vbo, float scaleX, float scaleY, float scaleZ)
	{
		Cache.addModel(uid, x, y, z, xRot, yRot, zRot, vbo, scaleX, scaleY, scaleZ);
	}
	
	public static void addModel(long uid, float x, float y, float z, int xRot, int yRot, int zRot, ModelVBO vbo, float scaleX, float scaleY, float scaleZ, Scissor scissor)
	{
		Cache.addModel(uid, x, y, z, xRot, yRot, zRot, vbo, scaleX, scaleY, scaleZ, scissor);
	}
	
	public static void addFloor(final int height, final WorldController wc, final int startX, final int startY, final int tileSize, final boolean colorsOnly, final boolean isVisible)
	{
		final ModelVBO vbo = models.loadFloor(height, wc, startX, startY, tileSize, colorsOnly);
		if (vbo != null) {
			vbo.isVisible = isVisible;
			if (!colorsOnly)
				Cache.addFloor(vbo.getUid(), vbo);
		}
		
		//System.out.println("floor: "+vbo);
	}
	
	public static void organizeMapDecorations()
	{
		atlasModels.organizeMapDecorations();
	}
	
	// loading background as textures
	public static int loadTextureRGBA_Direct(final byte[] data, int[] pixels, int width, int height, int alpha)
	{
		//width /= 2;
		//height /= 2;
		for (int i = data.length-1; i >= 0; i--)
		{
			int pixel = pixels[data[i]];
			buffer.put((byte) ((pixel >> 16) & 0xff)); // R
			buffer.put((byte) ((pixel >> 8) & 0xff)); // G
			buffer.put((byte) (pixel & 0xff)); // B
			
			if (pixel == 0)
			{
				buffer.put((byte) 0);
			}
			else
			{
				buffer.put((byte) 150);
			}
		}
		
		/*for (int y = height-1; y >= 0; y--)
		{
			for (int x = 0; x < width; x++)
			{
				int pixel = pixels[y * width + x];
				buffer.put((byte) (pixel & 0xFF));
			}
		}*/

		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS

		// You now have a ByteBuffer filled with the color data of each pixel.
		// Now just create a texture ID and bind it. Then you can load it using
		// whatever OpenGL method you want, for example:

		int texID = GLBufferManager.genTextureBuffer(GLBufferManager.TEXTURE_BACKGROUND); // Generate texture ID
		GraphicsDisplay.setTexture(texID);

		// Setup wrap mode
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

		// Setup texture scaling filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		// Send texel data to OpenGL
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		buffer.clear();
		
		// Return the texture ID so we can bind it later again
		return texID;
	}
	
	public static int loadTextureRGBA(final int[] pixels, int width, int height, int alpha, boolean isTexture, int id)
	{
		final int texID = GLBufferManager.genTextureBuffer(isTexture ? GLBufferManager.TEXTURE_TEXTURE : GLBufferManager.TEXTURE_SPRITE); // Generate texture ID
		
		return loadTextureRGBA(false, texID, pixels, width, height, alpha, isTexture, id);
	}

	public static boolean isAnimated(int id) {
		return false; //(id == 34/* || id == 40*/);
	}

	public static boolean isFlipped(int id) {
		return false;//(id == 34);
	}

	// loading sprites as textures
	public static int loadTextureRGBA(boolean update, int texID, final int[] pixels, int width, int height, int alpha, boolean isTexture, int id)
	{
		// we have to check if the image has any alpha values first
		// if it doesn't, we shouldn't set any alpha values!!
		// else it just becomes invisible
		boolean hasAlpha = false;
		loop: for (int y = height-1; y >= 0; y--)
		{
			int offset = y * width;
			for (int x = 0; x < width; x++)
			{
				int pixel = pixels[x + offset];
				int a = (pixel >> 24) & 0xFF;
				if (a > 0)
				{
					hasAlpha = true;
					break loop;
				}
			}
		}

		// textures don't need that mode
		if (isTexture)
			hasAlpha = false;
		
		// animated textures
		for (int y = height-1; y >= 0; y--)
		{
			int offset = y * width;
			for (int x = 0; x < width; x++)
			{
				int pixel = pixels[x + offset];
				int alphaBack = alpha;
				
				// fixing white and black pixels that
				// are supposed to be transparent!
				if (pixel == 0 || pixel == 0xffffffff)
				{
					alpha = 256;
				}
				
				buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
				buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
				buffer.put((byte) (pixel & 0xFF)); // Blue component
				if (alpha == 0 || (alpha == 255 && hasAlpha))
					buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component. Only for RGBA
				else
					buffer.put((byte) (alpha & 0xFF));
				
				alpha = alphaBack;
			}
		}

		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS

		// You now have a ByteBuffer filled with the color data of each pixel.
		// Now just create a texture ID and bind it. Then you can load it using
		// whatever OpenGL method you want, for example:

		GraphicsDisplay.setTexture(texID);
		
		// updating is much faster than recreating the data structure
		// we're definitely winning fps this way!
		if (update)
		{
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		}
		else
		{
			if (GraphicsDisplay.useMipMapping && isTexture)
			{
				// Setup wrap mode
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		
				// Setup texture scaling filtering
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				// use mipmapping
				glTexParameteri(GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL_TRUE);
				
				if (!Client.disableOpenGlErrors) {
					GraphicsDisplay.getInstance().exitOnGLError("error in loadTextureRGBA1(mipmap, "+texID+", "+id+", "+width+", "+height+")");
				}
			}
			else
			{
				// Setup wrap mode
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
				// high quality
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				// no mipmapping
				glTexParameteri(GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL_FALSE);
				
				if (!Client.disableOpenGlErrors) {
					GraphicsDisplay.getInstance().exitOnGLError("error in loadTextureRGBA2(no_mipmap, "+texID+", "+id+", "+width+", "+height+")");
				}
			}

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
			
		}

		buffer.clear();
		
		// couldn't read texture
		/*if (glGetError() != 0)
		{
			Client.drawAnimatedWorldBackground = false;
			return 0;
		}*/
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[GraphicsDisplay]: loadTextureRGBA("+texID+", "+id+", "+update+", "+width+", "+height+", "+GraphicsDisplay.MAX_TEXTURE_SIZE+")");
		}
		
		// Return the texture ID so we can bind it later again
		return texID;
	}

	
	
	/*public static long floorHash()
	{
		
	}*/
	
	public static void getFloorVBO(
			float x1, float x2, float x3,
			float y1, float y2, float y3,
			float z1, float z2, float z3,
			int color, int texture,
			int tileX, int tileY, int part)
	{
		
	}
	
	/*public static void vertexBufferData(int id, FloatBuffer buffer) { //Not restricted to FloatBuffer
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, id); //Bind buffer (also specifies type of buffer)
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STREAM_DRAW); //Send up the data and specify usage hint.
	}*/
	
}
