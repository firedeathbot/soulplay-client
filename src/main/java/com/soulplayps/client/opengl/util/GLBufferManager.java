package com.soulplayps.client.opengl.util;

import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL15.glGenBuffers;

import java.util.ArrayList;
import java.util.List;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

import com.soulplayps.client.Client;
import com.soulplayps.client.opengl.GraphicsDisplay;

public class GLBufferManager {
	
	public static int TEXTURE_ATLAS = 0;
	public static int TEXTURE_LETTER = 1;
	public static int TEXTURE_BACKGROUND = 2;
	public static int TEXTURE_SPRITE = 3;
	public static int TEXTURE_TEXTURE = 4;
	public static String[] TYPES = {"Texture Atlas", "Letter", "Background", "Sprite", "Texture"};
	
	private static int[] totalTextureBuffers = new int[TYPES.length];
	private static int totalGenBuffers = 0;
	public static IntArrayList buffers = new IntArrayList();
	public static List<BufferToDelete> textBuffers = new ArrayList<BufferToDelete>();

	static {
		if (GraphicsDisplay.enabled) {
			init();
		}
	}

	public static void init() {
		totalTextureBuffers = new int[TYPES.length];
		buffers = new IntArrayList();
		textBuffers = new ArrayList<BufferToDelete>();
	}

	public static void cleanup() {
		totalTextureBuffers = null;
		buffers = null;
		textBuffers = null;
	}

	public static class BufferToDelete {
		public int type;
		public int id;
		
		public BufferToDelete(int type, int id) {
			this.type = type;
			this.id = id;
		}
	}

	public static int getTotalGenBuffers() {
		return totalGenBuffers;
	}
	
	public static int[] getTotalTextureBuffers() {
		return totalTextureBuffers;
	}
	
	public static int genBuffer() {
		return genBuffers(1)[0];
	}
	
	public static int[] genBuffers(int amount) {
		int[] buffers = new int[amount];
		
		for (int i = 0; i < amount; i++) {
			buffers[i] = glGenBuffers();
			totalGenBuffers++;
			GLBufferManager.buffers.add(buffers[i]);
		}
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[GLBufferManager]: genBuffers(" + amount + ")");
		}
		
		return buffers;
	}
	
	public static void deleteGenBuffers(IntList buffers) {
		for (int i = 0; i < buffers.size(); i++) {
			int buf = buffers.getInt(i);
			totalGenBuffers--;
			GL15.glDeleteBuffers(buf);
			GLBufferManager.buffers.rem(buf);
		}
		
		buffers.clear();
	}
	
	public static void deleteGenBuffer(int buf) {
		totalGenBuffers--;
		GL15.glDeleteBuffers(buf);
	}
	
	public static int genTextureBuffer(int type) {
		int tex = GL11.glGenTextures();
		totalTextureBuffers[type]++;
		textBuffers.add(new BufferToDelete(type, tex));
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[GLBufferManager]: genTextureBuffers(int type)");
		}
		
		//if (tex == -1)
		//	System.err.println("[GLBufferManager]: couldn't create texture: "+tex);
		
		return tex;
	}
	
	public static int[] genTextureBuffers(int amount, int type) {
		int[] buffers = new int[amount];
		for (int i = 0; i < amount; i++) {
			buffers[i] = GL11.glGenTextures();
			totalTextureBuffers[type]++;
			textBuffers.add(new BufferToDelete(type, buffers[i]));
		}
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[GLBufferManager]: genTextureBuffers(" + amount + ")");
		}
		
		return buffers;
	}
	
	
	public static void deleteTextureBuffer(int type, IntList buffers) {
		for (int i = 0; i < buffers.size(); i++) {
			int value = buffers.getInt(i);
			glDeleteTextures(value);
			totalTextureBuffers[type]--;
		}
		
		buffers.clear();
	}
	
	public static void deleteTextureBuffer(int type, int... buffers) {
		for (int i : buffers) {
			glDeleteTextures(i);
			totalTextureBuffers[type]--;
		}
	}
	
	public static void clearAllBuffers() {
		for (int i = 0, length = buffers.size(); i < length; i++) {
			deleteGenBuffer(buffers.elements()[i]);
		}
		buffers.clear();

		for (int i = 0, length = textBuffers.size(); i < length; i++) {
			BufferToDelete del = textBuffers.get(i);
			deleteTextureBuffer(del.type, del.id);
		}
		textBuffers.clear();
	}

}
