package com.soulplayps.client.opengl.model;

import com.soulplayps.client.Client;
import it.unimi.dsi.fastutil.longs.Long2BooleanMap;
import it.unimi.dsi.fastutil.longs.Long2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;

public class DrawableArea {
	
	private int height;
	
	public int getHeight() {
		return height;
	}
	
	private long uid;
	private final ObjectList<DrawableModel> models;
	private final Long2BooleanMap modelUids;
	private boolean oneSeen = true;
	private boolean allSeen = true;
	
	private long lastUpdate = 0;
	private int lastAmount = 0;
	private boolean update = false;
	private long lastSeen = 0;
	
	private boolean mustClear = false;
	
	public boolean mustClear() {
		return mustClear;
	}
	
	public int getWorldX() {
		return worldX;
	}
	
	public int getWorldY() {
		return worldY;
	}
	
	private int worldX = 0;
	private int worldY = 0;
	private int localX = 0;
	private int localY = 0;
	private int dx = 0;
	private int dy = 0;
	
	public void resetOffset() {
		dx = 0;
		dy = 0;
	}
	
	public void offsetMap(int x, int y) {
		//int AREA_RANGE = 3;
		//if (localX >= 104-AREA_RANGE || localY >= 104-AREA_RANGE || localX <= AREA_RANGE || localY <= AREA_RANGE) {
		//	this.mustClear = true;
		//	//System.out.println("offs: "+localX+"/"+localY+"; "+dx+"/"+dy+", "+worldX+"/"+worldY+", "+Client.baseX+"/"+Client.baseY);
		//	return;
		//}
		
		dx += x * 128;
		dy += y * 128;
	}
	
	public int getDX() {
		return dx;
	}
	
	public int getDY() {
		return dy;
	}
	
	public long getLastSeen() {
		return lastSeen;
	}
	
	public void setLastSeen(long now) {
		this.lastSeen = now;
	}
	
	public boolean requiresUpdate() {
		return this.update;
	}
	
	public void updated(long now) {
		this.update = false;
		this.lastUpdate = now;
	}
	
	public long getLastUpdate() {
		return lastUpdate;
	}
	
	public void update(long now) {
		this.update = true;
		//this.lastUpdate = now;
	}
	
	public int getLastAmount() {
		return this.lastAmount;
	}
	
	public void drawn() {
		this.lastAmount = this.models.size();
		this.oneSeen = false;
		this.allSeen = true;
	}
	
	public int getX() {
		return localX;
	}
	
	public int getY() {
		return localY;
	}
	
	public void update(long uid, int x, int y, int height, int range) {
		this.uid = uid;
		this.height = height;
		
		final int cx = Client.baseDX != 0 ? Client.baseDX : Client.baseOffsetX;
		final int cy = Client.baseDY != 0 ? Client.baseDY : Client.baseOffsetY;
		this.worldX = ((Client.regionBaseX + x + cx) / range) * range;
		this.worldY = ((Client.regionBaseY + y + cy) / range) * range;
		
		this.localX = (x / range) * range;
		this.localY = (y / range) * range;
	}
	
	public DrawableArea(long uid, int x, int y, int height, int range) {
		this.models = new ObjectArrayList<DrawableModel>();
		this.modelUids = new Long2BooleanOpenHashMap();
		this.lastSeen = System.currentTimeMillis();
		
		this.update(uid, x, y, height, range);
		
		//System.out.println("xy: "+getX()+"/"+getY()+"; "+this.getWorldX()+"/"+this.getWorldY());
	}
	
	public static long hashModel(DrawableModel drawable) {
		final int cx = Client.baseDX != 0 ? Client.baseDX : Client.baseOffsetX;
		final int cy = Client.baseDY != 0 ? Client.baseDY : Client.baseOffsetY;
		
		final long x = Client.regionBaseX + drawable.getX() + cx;
		final long y = Client.regionBaseY + drawable.getY() + cy;
		
		// remove position values
		final long uid = ((drawable.getModelUid() >> 28L) << 28L);
		final long pos = (x & 0x3FFF) + ((y & 0xFFF) << 14L)/* + ((long)((drawable.getObjectType() & 0x1F)) << 28L)*/;
		
		//System.out.println("xy: "+x+"/"+y+"/"+z);
		
		//return (z << 32L) + (x << 16L) + y;
		return uid + pos;
	}
	
	public boolean containsModel(DrawableModel model) {
		final long uid = hashModel(model);
		return this.modelUids.containsKey(uid);
	}
	
	public boolean addModel(DrawableModel model) {
		final long uid = hashModel(model);
		
		if (!this.modelUids.containsKey(uid)) {
			if (model.getZ() != this.getHeight())
				System.err.println("[DrawableArea]: height issue warning: " + model.getZ() + "/" + this.getHeight());
			
			this.modelUids.put(uid, true);
			this.models.add(model);
			
			// let the model know the current offsets of the area in local coordinates
			model.setOffsets(this.dx, this.dy);
			
			return true;
		}
		
		return false;
	}
	
	public ObjectList<DrawableModel> getModels() {
		return models;
	}
	
	public void drawableSeen(boolean seen) {
		if (!seen)
			this.allSeen = false;
		else
			this.oneSeen = true;
	}
	
	public boolean isOneSeen() {
		return this.oneSeen;
	}
	
	public boolean areAllSeen() {
		return this.allSeen;
	}
	
	public void clear() {
		this.models.clear();
		this.modelUids.clear();
	}
}
