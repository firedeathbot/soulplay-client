package com.soulplayps.client.opengl.model;

public class DrawableModelCoordinates
{

	/**
	 * @return the translateX
	 */
	public int getTranslateX()
	{
		return translateX;
	}

	/**
	 * @return the translateY
	 */
	public int getTranslateY()
	{
		return translateY;
	}

	/**
	 * @return the translateZ
	 */
	public int getTranslateZ()
	{
		return translateZ;
	}

	/**
	 * @return the rotationX
	 */
	public int getRotationX()
	{
		return rotationX;
	}

	/**
	 * @return the rotationY
	 */
	public int getRotationY()
	{
		return rotationY;
	}

	/**
	 * @return the rotationZ
	 */
	public int getRotationZ()
	{
		return rotationZ;
	}

	/**
	 * @return the scaleX
	 */
	public float getScaleX()
	{
		return scaleX;
	}

	/**
	 * @return the scaleY
	 */
	public float getScaleY()
	{
		return scaleY;
	}

	/**
	 * @return the scaleZ
	 */
	public float getScaleZ()
	{
		return scaleZ;
	}

	private final int translateX;
	private final int translateY;
	private final int translateZ;

	private final int rotationX;
	private final int rotationY;
	private final int rotationZ;

	private final float scaleX;
	private final float scaleY;
	private final float scaleZ;
	
	public DrawableModelCoordinates(int translateX, int translateY, int translateZ, int xRotation, int yRotation, int zRotation, float scaleX, float scaleY, float scaleZ)
	{
		this.translateX = translateX;
		this.translateY = translateY;
		this.translateZ = translateZ;
		this.rotationX = xRotation;
		this.rotationY = yRotation;
		this.rotationZ = zRotation;
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		this.scaleZ = scaleZ;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "DrawableModelCoordinates [translateX=" + translateX + ", translateY=" + translateY + ", translateZ=" + translateZ + "]";
	}
	
}
