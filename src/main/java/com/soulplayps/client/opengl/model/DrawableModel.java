package com.soulplayps.client.opengl.model;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.opengl.GraphicsDisplay;

public class DrawableModel
{


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (modelUid ^ (modelUid >>> 32));
		result = prime * result + objectId;
		result = prime * result + (positionHash ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DrawableModel other = (DrawableModel) obj;
		if (modelUid != other.modelUid)
			return false;
		if (objectId != other.objectId)
			return false;
		if (positionHash != other.positionHash)
			return false;
		return true;
	}

	public int distanceToPlayer()
	{
		return Client.distanceToCameraLocal(getX(), getY()); 
		//return Client.distanceToMeLocal(getX(), getY());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "DrawableModel [uid=" + objUid + ", objectId=" + objectId + ", coords=" + coords + "]";
	}

	/**
	 * @return the model
	 */
	public Model getModel()
	{
		return model;
	}
	
	/**
	 * @return the uid for the vbo model
	 */
	public long getObjUid() {
		/*// ground items
		if (this.getEntityType() == 3)
			return objUid;
		
		// forget about the position if we're not adjusting the object
		// to the ground or if it's an entity (npc or player)
		long uid2 = objUid;
		if (!adjustGround || model.aBoolean1659) {
			//uid2 >>= 14L;
			//uid2 <<= 14L;
			//positionHash = false;
		}
		*/
		return this.objUid;
	}
	
	public long getModelUid() {
		return this.modelUid;
	}

	/**
	 * @return the objectId
	 */
	public int getObjectId()
	{
		return objectId;
	}

	/**
	 * @return the animated
	 */
	public boolean isAnimated()
	{
		return animated;
	}

	/**
	 * @return the adjustGround
	 */
	public boolean isAdjustGround()
	{
		return adjustGround;
	}
	
	public int getX()
	{
		return (int) ((objUid) & 0x7f);
	}
	
	public int getY()
	{
		return (int) ((objUid >> 7) & 0x7f);
	}
	
	public int getZ()
	{
		return z;
	}
	
	public int getEntityType()
	{
		return (int) (objUid >> 29 & 3); // player, npc, object?
	}

	private boolean positionHash = true;
	public boolean hasPositionHash() {
		return positionHash;
	}
	
	private int offsetX = 0;
	private int offsetY = 0;
	public void setOffsets(int dx, int dy) {
		this.offsetX = dx;
		this.offsetY = dy;
	}
	public int getOffsetX() {
		return this.offsetX;
	}
	public int getOffsetY() {
		return this.offsetY;
	}

	private boolean shadowModel = false;
	private Model model;
	private final long objUid;
	private final long modelUid;
	private final int objectId;
	static float SF = GraphicsDisplay.SF;
	private final boolean animated;
	private final boolean adjustGround;
	private final boolean seen;
	private final int z;
	private DrawableModelCoordinates coords = null;
	
	public void clear() {
		this.coords = null;
		this.model = null;
	}
	
	public boolean isShadowModel() {
		return shadowModel;
	}
	
	public void setShadowModel(boolean isShadow) {
		this.shadowModel = isShadow;
	}
	
	public DrawableModel(Model model, long objUid, int objectId, boolean animated, boolean adjustGround, int z) {
		this(model, objUid, objectId, animated, adjustGround, true, z, true);
	}
	
	public DrawableModel(Model model, long objUid, int objectId, boolean animated, boolean adjustGround, boolean seen, int z, boolean isInterfaceModel) {
		this.model = model;
		this.objUid = objUid;
		this.objectId = objectId;
		this.animated = animated;
		this.adjustGround = adjustGround;
		this.seen = seen;
		this.z = z;
		int x = getX() + Client.regionBaseX;
		int y = getY() + Client.regionBaseY;
		int type = getEntityType();
		int id = (int) (objUid >>> 32) & 0x7fffffff;
		int locType = 0;
		int locRotation = 0;
		if (type == 2) {
			locType = (int) (objUid >> 14) & 0x1f;
			locRotation = (int) (objUid >> 20) & 0x3;
		}

		this.modelUid = x | (y << 14) | (z << 28) | ((long) type << 31) | ((long) locType << 34) | ((long) locRotation << 40) | ((long) id << 42);
		if (!adjustGround || model.aBoolean1659)
			positionHash = false;
	}
	
	public void setCoords(int translateX, int translateY, int translateZ, int xRotation, int yRotation, int zRotation, float scaleX, float scaleY, float scaleZ) {
		this.coords = new DrawableModelCoordinates(translateX, translateY, translateZ, xRotation, yRotation, zRotation, scaleX, scaleY, scaleZ);
	}

	public DrawableModelCoordinates getCoords() {
		return coords;
	}

	public boolean isSeen() {
		return seen;
	}
	
}
