package com.soulplayps.client.opengl.gl30;

//import static org.lwjgl.opengl.ARBFramebufferObject.*;
//import static org.lwjgl.opengl.ARBShadowAmbient.GL_TEXTURE_COMPARE_FAIL_VALUE_ARB;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.util.glu.GLU.*;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

import com.soulplayps.client.Client;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.VBORenderer;
import com.soulplayps.client.opengl.util.BufferTools;

public class ShadowMapperGL30 implements ShadowMapper {
	// see https://www.youtube.com/watch?v=IiqiCVs3R4Q

	private boolean lightPositionLoaded = false;
	private int shadowMapSize = 2048;
	//private final Matrix4f depthModelViewProjection = new Matrix4f();

	// Moving from unit cube [-1,1] to [0,1]  
	private FloatBuffer bias = BufferTools.asFlippedFloatBuffer(
		0.5f, 0.0f, 0.0f, 0.0f, 
		0.0f, 0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.5f, 1.0f);
	

	// light view matrix
	private final FloatBuffer lightModelView = BufferUtils.createFloatBuffer(16);

	// light projection matrix
	private final FloatBuffer lightProjection = BufferUtils.createFloatBuffer(16);
	
	public ShadowMapperGL30(int size) {
		setSize(size);
	}

	public void setSize(int size) {
		bias = BufferTools.asFlippedFloatBuffer(
				0.5f, 0.0f, 0.0f, 0.0f, 
				0.0f, 0.5f, 0.0f, 0.0f,
				0.0f, 0.0f, 0.5f, 0.0f,
				0.5f, 0.5f, 0.5f, 1.0f);
		
		lightPositionLoaded = false;
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		this.shadowMapSize = size;
		cleanupBuffers();
		setUpFrameBufferObject();
		glActiveTexture(GL_TEXTURE0);
	}

	public int getSize() {
		return this.shadowMapSize;
	}

	private int frameBuffer = 0;
	private int textureHandle = 0;

	public void cleanupBuffers() {
		if (frameBuffer != 0) {
			glDeleteFramebuffers(frameBuffer);
			frameBuffer = 0;
		}
		
		if (textureHandle != 0) {
			glDeleteTextures(textureHandle);
			textureHandle = 0;
		}
	}

	/** Sets up the OpenGL states. */
	private void setUpFrameBufferObject() {
		final int MAX_RENDERBUFFER_SIZE = glGetInteger(GL_MAX_RENDERBUFFER_SIZE) / 4;
		final int MAX_TEXTURE_SIZE = glGetInteger(GL_MAX_TEXTURE_SIZE) / 4;

		/**
		 * Cap the maximum shadow map size at 1024x1024 pixels or at the maximum
		 * render buffer size. If you have a good graphics card, feel free to
		 * increase this value. The program will lag if I record and run the
		 * program at the same time with higher values.
		 */
		if (shadowMapSize > MAX_TEXTURE_SIZE || shadowMapSize > MAX_RENDERBUFFER_SIZE) {
			if (MAX_RENDERBUFFER_SIZE < MAX_TEXTURE_SIZE) {
				shadowMapSize = MAX_RENDERBUFFER_SIZE;
			} else {
				shadowMapSize = MAX_TEXTURE_SIZE;
			}
		}

		// System.out.println("wh: "+shadowMapWidth);

		// Generate and bind a frame buffer.
		frameBuffer = glGenFramebuffers();
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

		// generate and bind texture buffer
		textureHandle = glGenTextures();
		GraphicsDisplay.setTexture(textureHandle);

		// smoothening
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // [x,y,z,w] -> [s,t,r,q]
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


		// initialize texture
		final FloatBuffer fb = null;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapSize, shadowMapSize, 0, GL_DEPTH_COMPONENT, GL_FLOAT, fb);
	
		// bind texture to framebuffer
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureHandle, 0);
		
		// OpenGL shall make no amendment to the colour or multisample buffer.
		glDrawBuffer(GL_NONE);

		// Disable the colour buffer for pixel read operations (such as
		// glReadPixels or glCopyTexImage2D).
		glReadBuffer(GL_NONE);

		// Check for frame buffer errors.
		final int FBOStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (FBOStatus != GL_FRAMEBUFFER_COMPLETE) {
			System.err.println("Framebuffer error: " + gluErrorString(glGetError()));
		}

		// Bind the default frame buffer, which is used for ordinary drawing.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

	}

	/** Generate the shadow map. */
	public void drawShadowMap(boolean clearModels, int resolution) {

		// update shadow map quality if required
		if (resolution != this.shadowMapSize)
			setSize(resolution);
		
		// buffers not properly setup --> return
		if (frameBuffer == 0 || textureHandle == 0 || Client.myPlayer == null)
			return;
		
		
		/*// setup transformation matrix for shadow to eye plane
		lightProjectionTemp.load(lightProjection);
		lightModelViewTemp.load(lightModelView);
		lightProjection.flip();
		lightModelView.flip();
		depthModelViewProjection.setIdentity();

		// [-1,1] -> [-0.5,0.5] -> [0,1]
		depthModelViewProjection.translate(new Vector3f(0.5f, 0.5f, 0.5f));
		depthModelViewProjection.scale(new Vector3f(0.5f, 0.5f, 0.5f));

		// Multiply the texture matrix by the projection and model-view matrices
		// of the light.
		Matrix4f.mul(depthModelViewProjection, lightProjectionTemp, depthModelViewProjection);
		Matrix4f.mul(depthModelViewProjection, lightModelViewTemp, depthModelViewProjection);

		// Transpose the texture matrix.
		Matrix4f.transpose(depthModelViewProjection, depthModelViewProjection);
		
		depthModelViewProjection.store(depthModelView);
		depthModelView.flip();*/


		VBORenderer.setPerspective(true, true);
		glGetFloat(GL_PROJECTION_MATRIX, lightProjection);
		glGetFloat(GL_MODELVIEW_MATRIX, lightModelView);

		// Bind the extra frame buffer in which to store the shadow map in the
		// form a depth texture.
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		glClear(GL_DEPTH_BUFFER_BIT);
		//glColor4f(0.3f, 0.3f, 0.0f, 1f);

		// Set the view port to the shadow map dimensions so no part of the
		// shadow is cut off.
		glViewport(0, 0, shadowMapSize, shadowMapSize);
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[ShadowMapper]: glBindFramebuffer(" + frameBuffer + ")");
		}
		
		// Store the current attribute state.
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		{
			//glDepthFunc(GL_LEQUAL);
			//glDepthFunc(GL_GEQUAL);
		   
			// Disable smooth shading, because the shading in a shadow map is
			// irrelevant. It only matters where the
			// shape vertices are positioned, and not what colour they have.
			glShadeModel(GL_FLAT);

			// Disable the writing of the red, green, blue, and alpha colour
			// components, because we only need the depth component.
			glColorMask(false, false, false, false);
			glCullFace(GL_BACK);

			// An offset is given to every depth value of every polygon fragment
			// to prevent a visual quirk called
			// 'shadow acne'.
			glEnable(GL_POLYGON_OFFSET_FILL);
			glPolygonOffset(2.5f, 0f);

			//TODO: shadows seem to be inverted, they are rendered from somewhere inside the ground to the light
			// I should fix that process, because shadow casting is simply wrong that way. Idk how exactly to
			// do that though. probably inverting the projection matrices? we'll see...
			VBORenderer.renderShadowScene(clearModels);
			
			// crashed?
			if (!Client.disableOpenGlErrors) {
				GraphicsDisplay.getInstance().exitOnGLError("[ShadowMapper]: renderShadowScene(" + shadowMapSize + ")");
			}

		}
		// Restore the previous attribute state.
		// glCullFace(GL_FRONT);
		
		// setup the shadow projections for depth comparison in the shader
		// this is done on texture channel 1
		glActiveTexture(GL_TEXTURE1);
		GraphicsDisplay.setTexture(textureHandle);

		glMatrixMode(GL_TEXTURE);
		glLoadMatrix(bias);
		glMultMatrix(lightProjection);
		glMultMatrix(lightModelView);
		
		glActiveTexture(GL_TEXTURE0);
		
		
		// pop old settings
		glPopAttrib();
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// reset viewport
		GraphicsDisplay.getInstance().glOrthoReset();

		// exit on error
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[ShadowMapper]: error");
		}
		
	}

	public void unbindFrameBuffer() {
		
		// reset and clear the framebuffer, so that it cannot interfere
		// with anything that is drawn later on, e.g. sprites
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		glClear(GL_DEPTH_BUFFER_BIT);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
}
