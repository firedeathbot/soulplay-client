package com.soulplayps.client.opengl.gl30;

public interface ShadowMapper {

	public void drawShadowMap(boolean clearBuffers, int shadowMapSize);
	public void unbindFrameBuffer();
	public void cleanupBuffers();
	
}
