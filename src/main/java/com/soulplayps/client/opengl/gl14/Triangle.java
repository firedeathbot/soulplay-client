package com.soulplayps.client.opengl.gl14;

public class Triangle
{

	public final float x1, x2, x3;
	public final float y1, y2, y3;
	public final float z1, z2, z3;

	public final int texture;
	public final int c1, c2, c3, alpha;

	public final float u1, u2, u3, v1, v2, v3;
	public final boolean uv;

	public Triangle next = null;

	public Triangle(int texture, float x1, float x2, float x3, float y1, float y2, float y3, float z1, float z2, float z3, int c1, int c2, int c3, int alpha)
	{
		// moving texture definitely behind the corresponding
		// colored triangle. this is just a little depth
		// buffering work around for textures
		z1 -= 0.00001f;
		z2 -= 0.00001f;
		z3 -= 0.00001f;	
		
		// proper old textures
		if ((c1 < 50 || c2 < 50 || c3 < 50) && texture < 50)
		{
			//c1 = c2 = c3 = 0xFFFF;
		}
		
		//if (alpha == 0)
		//	alpha = 256;
		
		this.texture = texture;
		this.x1 = x1;
		this.x2 = x2;
		this.x3 = x3;
		this.y1 = y1;
		this.y2 = y2;
		this.y3 = y3;
		this.z1 = z1;
		this.z2 = z2;
		this.z3 = z3;
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.alpha = alpha;
		this.u1 = 0;
		this.u2 = 0;
		this.u3 = 0;
		this.v1 = 0;
		this.v2 = 0;
		this.v3 = 0;
		this.uv = false;
	}

	public Triangle(int texture, float x1, float x2, float x3, float y1, float y2, float y3, float z1, float z2, float z3, int c1, int c2, int c3,
			int alpha, float u1, float u2, float u3, float v1, float v2, float v3)
	{
		// moving texture definitely behind the corresponding
		// colored triangle. this is just a little depth
		// buffering work around for textures
		z1 -= 0.00001f;
		z2 -= 0.00001f;
		z3 -= 0.00001f;
		
		// proper old textures
		if ((c1 < 50 || c2 < 50 || c3 < 50) && texture < 50 && texture != 0)
		{
			c1 = c2 = c3 = 0xFFFF;
		}
		
		this.texture = texture;
		this.x1 = x1;
		this.x2 = x2;
		this.x3 = x3;
		this.y1 = y1;
		this.y2 = y2;
		this.y3 = y3;
		this.z1 = z1;
		this.z2 = z2;
		this.z3 = z3;
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.alpha = alpha;
		this.u1 = u1;
		this.u2 = u2;
		this.u3 = u3;
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
		this.uv = true;
	}

}
