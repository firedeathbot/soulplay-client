package com.soulplayps.client.opengl.gl14;

public class Quad {
	
	public final float x1, x2, x3, x4;
	public final float y1, y2, y3, y4;
	public final float z1, z2, z3, z4;

	public final int texture;
	public final int c1, c2, c3, c4, alpha;
	
	public Quad next = null;
	
	public Quad(int texture, float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4, float z1, float z2, float z3, float z4, int c1, int c2, int c3, int c4, int alpha)
	{
		// moving texture definitely behind the corresponding
		// colored triangle. this is just a little depth
		// buffering work around for textures
		z1 -= 0.00001f;
		z2 -= 0.00001f;
		z3 -= 0.00001f;
		z4 -= 0.00001f;
		
		// proper old textures
		if ((c1 < 50 || c2 < 50 || c3 < 50) && texture < 50)
		{
			//c1 = c2 = c3 = 0xFFFF;
		}
		
		this.texture = texture;
		this.x1 = x1;
		this.x2 = x2;
		this.x3 = x3;
		this.x4 = x4;
		this.y1 = y1;
		this.y2 = y2;
		this.y3 = y3;
		this.y4 = y4;
		this.z1 = z1;
		this.z2 = z2;
		this.z3 = z3;
		this.z4 = z4;
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.c4 = c4;
		this.alpha = alpha;
	}
}
