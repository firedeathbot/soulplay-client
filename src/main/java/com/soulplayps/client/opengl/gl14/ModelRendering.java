package com.soulplayps.client.opengl.gl14;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.opengl.gl15.model.ModelConfig;
import com.soulplayps.client.opengl.util.Cache;

import static org.lwjgl.opengl.GL11.glColor4f;

public class ModelRendering {

	public static void addModelTriangle_MODERN(
			final float[][] uv,
			final float[] vertexPerspectiveX, final float[] vertexPerspectiveY, final float[] vertexPerspectiveZ,
			final int[] triangleVertexPoint1, final int[] triangleVertexPoint2, final int[] triangleVertexPoint3, final int[] colorValues,
			final int[] textureBackgrounds, final int vertexCount, final int[] anIntArray1671,
			final int[][] anIntArrayArray1672, final int[] colorValues1,
			final int[] colorValues2, final int[] colorValues3, final int[] triangleAlphaValues,
			final int[] drawTypes, int[] texturePointers, byte[] textureTypes,
			final int[] textureP, final int[] textureM, final int[] textureN,
			final int[] texA, final int[] texB, final int[] texC,
			final int[] particleDirectionX, final int[] particleDirectionY, final int[] particleDirectionZ,
			final int[] particleLifespanX, final int[] particleLifespanY, final int[] particleLifespanZ,
			final int[] texturePrimaryColor, final int[] textureSecondaryColor,
			final int vertex, final int l1, final int height, final float SF,
			final boolean renderingWorld, final boolean modelsTextured, final int pool) {
		int ai[] = anIntArrayArray1672[vertex];
		for (int j3 = 0; j3 < l1 && j3 < ai.length; j3++) {
			final int i = ai[j3];
			
			int alpha = (triangleAlphaValues != null ? 256 - triangleAlphaValues[i] : 256);
			int drawType = drawTypes == null ? 0 : drawTypes[i] & 3;
			
			//if (!renderingWorld)
			//	alpha = 70;
			
			//alpha = 1;
			//if (alpha != 0 && alpha != 256)
			//	System.out.println("alpha: "+alpha);
			
			final float x1 = vertexPerspectiveX[triangleVertexPoint1[i]] * SF;
			final float y1 = (height - vertexPerspectiveY[triangleVertexPoint1[i]]) * SF;
			final float z1 = vertexPerspectiveZ[triangleVertexPoint1[i]] * SF;
			final float x2 = vertexPerspectiveX[triangleVertexPoint2[i]] * SF;
			final float y2 = (height - vertexPerspectiveY[triangleVertexPoint2[i]]) * SF;
			final float z2 = vertexPerspectiveZ[triangleVertexPoint2[i]] * SF;
			final float x3 = vertexPerspectiveX[triangleVertexPoint3[i]] * SF;
			final float y3 = (height - vertexPerspectiveY[triangleVertexPoint3[i]]) * SF;
			final float z3 = vertexPerspectiveZ[triangleVertexPoint3[i]] * SF;
			
			//System.out.println(x1+", "+y1+", "+z1+"; "+x2+", "+y2+", "+z2+"; "+x3+", "+y3+", "+z3);
			
			int c1 = 0;
			int c2 = 0;
			int c3 = 0;
			if (drawType == 1)
				c1 = c2 = c3 = colorValues[i] & 0xFFFF;
			else {
				c1 = colorValues1[i] & 0xFFFF;
				c2 = colorValues2[i] & 0xFFFF;
				c3 = colorValues3[i] & 0xFFFF;
			}
			
			// ragefire, steadfast and glaiven boots fix
			// that isn't a beautiful way of removing vertices
			// but it works nonetheless
			if (alpha == 131 && c1 >= 32816 && c1 <= 32855) {
				c2 = c1;
				c3 = c1;
				
				//System.out.println("color: "+c1);
				if (c1 == 32845)
					continue;
				else if (c1 == 32833)
					continue;
				else if (c1 == 32816)
					continue;
				else if (c1 == 32820)
					continue;
				else if (c1 == 32848)
					continue;
				else if (c1 == 32855)
					continue;
				else if (c1 == 32819)
					continue;
			}
			
			
			boolean alwaysDraw = false;
			int texture = textureBackgrounds != null ? textureBackgrounds[i] : -1;
			boolean clearColors = false;
			
			if ((colorValues[i] <= 50 || texturePointers != null && i < texturePointers.length && texturePointers[i] != -1)) {
				if (texture == 0 && colorValues[i] <= 50 && textureBackgrounds == null) {
					texture = colorValues[i];
					clearColors = true;
				}
			}
			
			// texturing for old models, revision 317
			if (texture <= 0 && colorValues[i] <= 50 && drawType != 0) {
				texture = colorValues[i];
			}
			
			// default texture on everything
			if (texture <= 0) {
				//texture = DEFAULT_TEXTURE;
			}
			
			// done by Jire
			
			// texture definition class
			Texture td = Texture.get(texture); // TODO big one?
			if (td == null)
				texture = -1;
			
			// always draw this texture?
			if (texture >= 0)
				alwaysDraw = ModelConfig.basicTexture[texture] || texture <= 50;
			
			// always draw following textures
			// tree crowns, older textures, roofs, etc...
			if (td != null && td.transparentTexture && texture > 0) {
				alwaysDraw = true;
				if (texture != 922)
					clearColors = true;
			}
			
			// textures for every model enabled?
			if (texture >= 0 && !alwaysDraw && !Client.enableHDTextures) {
				// no texturing
				if (!ModelConfig.basicTexture[texture]) {
					clearColors = false;
					texture = -1;
				}
			}
			
			// tree stump texture, it's just looking odd
			// so we simply remove it
			if (texture == 922) {
				alpha = 220;
			}
			
			
			if (uv != null && texture > 0) {
				float u1 = uv[i][0];
				float v1 = uv[i][1];
				float u2 = uv[i][2];
				float v2 = uv[i][3];
				float u3 = uv[i][4];
				float v3 = uv[i][5];
				
				Cache.addTriangle(texture,
						x1, x2, x3,
						y1, y2, y3,
						z1, z2, z3,
						c1, c2, c3,
						alpha,
						u1, u2, u3,
						v1, v2, v3,
						pool);
			} else {
				Cache.addTriangle(
						x1, x2, x3,
						y1, y2, y3,
						z1, z2, z3,
						c1, c2, c3,
						alpha,
						pool);
			}
			
			/*if (renderingWorld || true)
			{
				if (drawType == 1) {
					Cache.addTriangle(x1, x2, x3, y1, y2, y3, z1, z2, z3,
							colorValues[i], colorValues[i], colorValues[i], alpha,
							pool);
				} else {
					Cache.addTriangle(x1, x2, x3, y1, y2, y3, z1, z2, z3,
							colorValues1[i], colorValues2[i], colorValues3[i],
							alpha,
							pool);
				}
			}*/
			
			// this.exitOnGLError("Error in addModel(draw: " + i + ")");
		}
	}
	
	public static boolean addModelTriangle_CLASSIC(
			final float[] vertexX, final float[] vertexY, final float[] vertexZ,
			final int[] triangleVertexPoint1, final int[] triangleVertexPoint2, final int[] triangleVertexPoint3, final int[] colorValues,
			final int[] textureBackgrounds, final int[] anIntArray1671, final int[][] anIntArrayArray1672,
			final int[] colorValues1, final int[] colorValues2, final int[] colorValues3,
			final int[] triangleAlphaValues, final int[] drawTypes, final int[] texturePointers,
			final byte[] textureTypes,
			final int[] textureP, final int[] textureM, final int[] textureN,
			final int[] texA, final int[] texB, final int[] texC, final int vertex,
			final int height, final float SF, final int pool
	) {
		if (vertex >= colorValues.length)
			return true;
		
		//if (aBooleanArray1664[i])
		//	return method485(i, pool);
		
		final int pI = triangleVertexPoint1[vertex];
		final int mI = triangleVertexPoint2[vertex];
		final int nI = triangleVertexPoint3[vertex];
		//Rasterizer.restrictEdges = triangleEdgeRestriction[i];
		if (colorValues[vertex] == 65536) // ?
			return true;
		
		int alpha = triangleAlphaValues != null ? 256 - triangleAlphaValues[vertex] : 256;
		
		//if (forcedAlpha != -1)
		//	alpha = forcedAlpha;
		
		final int type = drawTypes == null ? 0 : drawTypes[vertex] & 3;
		
		int c1 = 0;
		int c2 = 0;
		int c3 = 0;
		if (type == 1)
			c1 = c2 = c3 = colorValues[vertex];
		else {
			c1 = colorValues1[vertex];
			c2 = colorValues2[vertex];
			c3 = colorValues3[vertex];
		}
		
		
		if (textureBackgrounds != null) {
			// fire cape fix
			if (textureBackgrounds[vertex] == 0 && colorValues[vertex] == 40) {
				textureBackgrounds[vertex] = 40;
			}
			
			if (textureBackgrounds[vertex] == 50 || textureBackgrounds[vertex] == 45) {
				textureBackgrounds[vertex] = 50;
				colorValues[vertex] = 50;
				c1 = c2 = c3 = 50;
			}
		}
		
		//if (texture != null)
		//	System.out.println("t: "+texture[i]);
		
		if (textureBackgrounds != null && textureBackgrounds[vertex] != -1) {
			// tree stump texture, it's just looking odd
			// so we simply remove it
			if (textureBackgrounds[vertex] >= 1321 && textureBackgrounds[vertex] <= 1326) // evergreen
				c1 = c2 = c3 = 0xFFFF;
			else if (textureBackgrounds[vertex] >= 937 && textureBackgrounds[vertex] <= 945) // willow
			{
				c1 = c2 = c3 = 0xFFFF;
				alpha = 240;
			}
			
			if (texturePointers != null && texturePointers[vertex] != -1) {
				final int texPtr = texturePointers[vertex] & 0xff;
				byte texType = textureTypes == null || texPtr >= textureTypes.length ? 0 : textureTypes[texPtr];
				
				if (textureP == null || texPtr >= textureP.length)
					texType = 16;
				
				if (texType == 0) {
					final int pointX = textureP[texPtr];
					final int pointY = textureM[texPtr];
					final int pointZ = textureN[texPtr];
					
					drawTexturedTriangle(
							texA[pI], texA[mI], texA[nI],
							texB[pI], texB[mI], texB[nI],
							texC[pI], texC[mI], texC[nI],
							c1, c2, c3,
							texA[pointX], texA[pointY], texA[pointZ],
							texB[pointX], texB[pointY], texB[pointZ],
							texC[pointX], texC[pointY], texC[pointZ],
							textureBackgrounds[vertex],
							colorValues[vertex], false,
							alpha, vertexX, vertexY, vertexZ, pI, mI, nI,
							height, SF, pool);
					return true;
				} else if (texType >= 15) {
					// this is mainly drawing players/npcs
					// also the function that draws textured models
					final int pointX = pI;
					final int pointY = mI;
					final int pointZ = nI;
					
					// default texture drawing method, all models textured!!
					drawTexturedTriangle(
							texA[pI], texA[mI], texA[nI],
							texB[pI], texB[mI], texB[nI],
							texC[pI], texC[mI], texC[nI],
							c1, c2, c3,
							texA[pointX], texA[pointY], texA[pointZ],
							texB[pointX], texB[pointY], texB[pointZ],
							texC[pointX], texC[pointY], texC[pointZ],
							textureBackgrounds[vertex],
							colorValues[vertex], false,
							alpha, vertexX, vertexY, vertexZ, pI, mI, nI,
							height, SF, pool);
					return true;
				}
			} else {
				final int pointX = pI;
				final int pointY = mI;
				final int pointZ = nI;
				
				drawTexturedTriangle(
						texA[pI], texA[mI], texA[nI],
						texB[pI], texB[mI], texB[nI],
						texC[pointX], texC[pointY], texC[pointZ],
						c1, c2, c3,
						texA[pointX], texA[pointY], texA[pointZ],
						texB[pointX], texB[pointY], texB[pointZ],
						texC[pointX], texC[pointY], texC[pointZ],
						textureBackgrounds[vertex],
						colorValues[vertex], false,
						alpha, vertexX, vertexY, vertexZ, pI, mI, nI,
						height, SF, pool);
				return true;
			}
		}
		
		if (type == 0 || type == 1) {
			final float x1 = vertexY[pI] * SF;
			final float y1 = (height - vertexY[pI]) * SF;
			final float z1 = vertexZ[pI] * SF;
			final float x2 = vertexY[mI] * SF;
			final float y2 = (height - vertexY[mI]) * SF;
			final float z2 = vertexZ[mI] * SF;
			final float x3 = vertexY[nI] * SF;
			final float y3 = (height - vertexY[nI]) * SF;
			final float z3 = vertexZ[nI] * SF;
			
			Cache.addTriangle(x1, x2, x3, y1, y2, y3, z1, z2, z3, c1, c2, c3, alpha, pool);
			
		} else if (type == 2) {
			// mostly trees
			final int j1 = (drawTypes[vertex] & 0xFF) >> 2;
			final int pointX = textureP[j1];
			final int pointY = textureM[j1];
			final int pointZ = textureN[j1];
			
			if (colorValues[vertex] == 50) {
				c1 = c2 = c3 = 50;
			}
			
			// color values are the texture ids here
			drawTexturedTriangle(texA[pI], texA[mI], texA[nI], texB[pI],
					texB[mI], texB[nI], texC[pI], texC[mI], texC[nI], c1, c2,
					c3, texA[pointX], texA[pointY], texA[pointZ], texB[pointX],
					texB[pointY], texB[pointZ], texC[pointX], texC[pointY],
					texC[pointZ], colorValues[vertex], -1, false, alpha, vertexX,
					vertexY, vertexZ, pI, mI, nI, height, SF, pool);
			return true;
		} else if (type == 3) {
			// tree alpha weird stuff??
			final int k1 = (drawTypes[vertex] & 0xFF) >> 2;
			final int pointX = textureP[k1];
			final int pointY = textureM[k1];
			final int pointZ = textureN[k1];
			
			drawTexturedTriangle(texA[pI], texA[mI], texA[nI], texB[pI],
					texB[mI], texB[nI], texC[pI], texC[mI], texC[nI], c1, c2,
					c3, texA[pointX], texA[pointY], texA[pointZ], texB[pointX],
					texB[pointY], texB[pointZ], texC[pointX], texC[pointY],
					texC[pointZ], colorValues[vertex], -1, false, alpha, vertexX,
					vertexY, vertexZ, pI, mI, nI, height, SF, pool);
			return true;
		}
		
		return false;
	}
	
	private static void drawTexturedTriangle(
			float ax, float ay, float az,
			float bx, float by, float bz,
			float cx, float cy, float cz,
			int grad_a, int grad_b, int grad_c,
			float px, float mx, float nx,
			float py, float my, float ny,
			float pz, float mz, float nz,
			int t_id, int color,
			boolean force,
			int alpha, float[] verticesX, float verticesY[], float[] verticesZ, int pX, int pY, int pZ,
			int height, float SF,
			int pool
	) {
		final float x1 = verticesX[pX] * SF;
		final float y1 = (height - verticesY[pX]) * SF;
		final float z1 = verticesZ[pX] * SF;
		final float x2 = verticesX[pY] * SF;
		final float y2 = (height - verticesY[pY]) * SF;
		final float z2 = verticesZ[pY] * SF;
		final float x3 = verticesX[pZ] * SF;
		final float y3 = (height - verticesY[pZ]) * SF;
		final float z3 = verticesZ[pZ] * SF;
		
		mx -= px;
		my -= py;
		mz -= pz;
		nx -= px;
		ny -= py;
		nz -= pz;
		
		ax -= px;
		bx -= py;
		cx -= pz;
		ay -= px;
		by -= py;
		cy -= pz;
		az -= px;
		bz -= py;
		cz -= pz;
		
		final float mnyz = my * nz - mz * ny;
		final float mnxz = mz * nx - mx * nz;
		final float mnxy = mx * ny - my * nx;
		float mn1 = ny * mnxy - nz * mnxz;
		float mn2 = nz * mnyz - nx * mnxy;
		float mn3 = nx * mnxz - ny * mnyz;
		
		float inv = 1f / (mn1 * mx + mn2 * my + mn3 * mz);
		
		float u1 = (mn1 * ax + mn2 * bx + mn3 * cx) * inv;
		float u2 = (mn1 * ay + mn2 * by + mn3 * cy) * inv;
		float u3 = (mn1 * az + mn2 * bz + mn3 * cz) * inv;
		
		mn1 = my * mnxy - mz * mnxz;
		mn2 = mz * mnyz - mx * mnxy;
		mn3 = mx * mnxz - my * mnyz;
		
		inv = 1.0F / (mn1 * nx + mn2 * ny + mn3 * nz);
		
		float v1 = (mn1 * ax + mn2 * bx + mn3 * cx) * inv;
		float v2 = (mn1 * ay + mn2 * by + mn3 * cy) * inv;
		float v3 = (mn1 * az + mn2 * bz + mn3 * cz) * inv;

			/*if (Float.isNaN(u1) || Float.isInfinite(u1)) u1 = 0;
			if (Float.isNaN(u2) || Float.isInfinite(u2)) u2 = 0;
			if (Float.isNaN(u3) || Float.isInfinite(u3)) u3 = 0;
			if (Float.isNaN(v1) || Float.isInfinite(v1)) v1 = 0;
			if (Float.isNaN(v2) || Float.isInfinite(v2)) v2 = 0;
			if (Float.isNaN(v3) || Float.isInfinite(v3)) v3 = 0;*/
		
		if (u1 < 0) u1 = 0;
		if (u1 > 1) u1 = 1;
		if (u2 < 0) u2 = 0;
		if (u2 > 1) u2 = 1;
		if (u3 < 0) u3 = 0;
		if (u3 > 1) u3 = 1;
		
		if (v1 < 0) v1 = 0;
		if (v1 > 1) v1 = 1;
		if (v2 < 0) v2 = 0;
		if (v2 > 1) v2 = 1;
		if (v3 < 0) v3 = 0;
		if (v3 > 1) v3 = 1;
		
		//doneUV = true;
		//if (drawType == 1)
		//	Cache.addTexturedTriangleUV(texID, x1, x2, x3,
		//			y1, y2, y3, z1, z2, z3, color[i],
		//			color[i], color[i], alpha, 1f-u1, 1f-u2, 1f-u3, 1f-v1, 1f-v2, 1f-v3);
		//else
		Cache.addTriangle(t_id, x1, x2, x3,
				y1, y2, y3, z1, z2, z3, grad_a,
				grad_b, grad_c, alpha, u1, u2, u3, v1, v2, v3, pool);
	}
	
	private static void setColor(int col, int a) {
		final float r = (float) ((col >> 16) & 0xFF);
		final float g = (float) ((col >> 8) & 0xFF);
		final float b = (float) ((col >> 0) & 0xFF);
		final float alpha = a / 256f;
		glColor4f(r / 256f, g / 256f, b / 256f, alpha);
		// glColor3f(r/256f, g/256f, b/256f);
	}
	
	
	private static float[] method699(int argument, float[] argument_1_, int lifespanY, float argument_3_,
	                                 int argument_4_, int vertexY, float argument_6_, int argument_7_,
	                                 int vertexZ, float[] argument_9_, int vertexX,
	                                 int argument_11_) {
		vertexY -= argument_4_;
		vertexZ -= argument_7_;
		vertexX -= argument;
		float f = (argument_1_[0] * (float) vertexX
				+ argument_1_[1] * (float) vertexY
				+ argument_1_[2] * (float) vertexZ);
		float f_12_ = (argument_1_[5] * (float) vertexZ
				+ ((float) vertexX * argument_1_[3]
				+ argument_1_[4] * (float) vertexY));
		float f_13_ = ((float) vertexZ * argument_1_[8]
				+ ((float) vertexX * argument_1_[6]
				+ (float) vertexY * argument_1_[7]));
		float f_14_
				= ((float) Math.atan2((double) f, (double) f_13_) / 6.2831855F
				+ 0.5F);
		if (argument_6_ != 1.0F)
			f_14_ *= argument_6_;
		float f_15_ = f_12_ + 0.5F + argument_3_;
		if (lifespanY != 1) {
			if (lifespanY == 2) {
				f_15_ = -f_15_;
				f_14_ = -f_14_;
			} else if (lifespanY == 3) {
				float f_16_ = f_14_;
				f_14_ = f_15_;
				f_15_ = -f_16_;
			}
		} else {
			float f_17_ = f_14_;
			f_14_ = -f_15_;
			f_15_ = f_17_;
		}
		argument_9_[0] = f_14_;
		argument_9_[1] = f_15_;
		
		return argument_9_;
	}
	
	private static float[] method226(float[] argument, int argument_34_,
	                                 int argument_35_, int argument_36_, int argument_37_,
	                                 int argument_38_, float[] argument_39_, int argument_40_,
	                                 boolean argument_41_, int argument_42_, float argument_43_) {
		argument_38_ -= argument_36_;
		argument_37_ -= argument_40_;
		argument_35_ -= argument_34_;
		float f = ((float) argument_37_ * argument_39_[2] + ((float) argument_35_
				* argument_39_[0] + argument_39_[1] * (float) argument_38_));
		float f_44_ = (argument_39_[5] * (float) argument_37_ + ((float) argument_35_
				* argument_39_[3] + argument_39_[4] * (float) argument_38_));
		float f_45_ = (argument_39_[8] * (float) argument_37_ + ((float) argument_38_
				* argument_39_[7] + argument_39_[6] * (float) argument_35_));
		float f_46_ = (float) Math.sqrt((double) (f_44_ * f_44_ + f * f + f_45_
				* f_45_));
		float f_47_ = ((float) Math.atan2((double) f, (double) f_45_) / 6.2831855F + 0.5F);
		float f_48_ = (argument_43_ + ((float) Math
				.asin((double) (f_44_ / f_46_)) / 3.1415927F + 0.5F));
		if (argument_42_ == 1) {
			float f_49_ = f_47_;
			f_47_ = -f_48_;
			f_48_ = f_49_;
		} else if (argument_42_ != 2) {
			if (argument_42_ == 3) {
				float f_50_ = f_47_;
				f_47_ = f_48_;
				f_48_ = -f_50_;
			}
		} else {
			f_48_ = -f_48_;
			f_47_ = -f_47_;
		}
		argument[0] = f_47_;
		argument[1] = f_48_;
		return argument;
	}
	
	private static float[] method1689(int argument, float[] argument_0_,
	                                  int argument_1_, int argument_2_, int argument_3_, int argument_4_,
	                                  int argument_5_, int argument_6_, int[] fs_910_,
	                                  float argument_8_, int argument_9_, int argument_10_,
	                                  float argument_11_, float argument_12_) {
		argument_10_ -= argument_2_;
		argument_9_ -= argument_3_;
		argument_4_ -= argument_5_;
		float f = (fs_910_[2] * (float) argument_9_ + (fs_910_[0]
				* (float) argument_4_ + fs_910_[1] * (float) argument_10_));
		float f_13_ = (fs_910_[5] * (float) argument_9_ + (fs_910_[4]
				* (float) argument_10_ + fs_910_[3] * (float) argument_4_));
		float f_14_ = (fs_910_[7] * (float) argument_10_
				+ (float) argument_4_ * fs_910_[6] + (float) argument_9_
				* fs_910_[8]);
		float f_15_;
		float f_16_;
		if (argument != 0) {
			if (argument != 1) {
				if (argument == 2) {
					f_16_ = argument_11_ + -f + 0.5F;
					f_15_ = argument_12_ + -f_13_ + 0.5F;
				} else if (argument == 3) {
					f_15_ = argument_12_ + -f_13_ + 0.5F;
					f_16_ = f + argument_11_ + 0.5F;
				} else if (argument == 4) {
					f_16_ = argument_8_ + f_14_ + 0.5F;
					f_15_ = -f_13_ + argument_12_ + 0.5F;
				} else {
					f_16_ = argument_8_ + -f_14_ + 0.5F;
					f_15_ = argument_12_ + -f_13_ + 0.5F;
				}
			} else {
				f_16_ = f + argument_11_ + 0.5F;
				f_15_ = argument_8_ + f_14_ + 0.5F;
			}
		} else {
			f_15_ = argument_8_ + -f_14_ + 0.5F;
			f_16_ = argument_11_ + f + 0.5F;
		}
		if (argument_1_ == 1) {
			float f_17_ = f_16_;
			f_16_ = -f_15_;
			f_15_ = f_17_;
		} else if (argument_1_ == 2) {
			f_15_ = -f_15_;
			f_16_ = -f_16_;
		} else if (argument_1_ == 3) {
			float f_18_ = f_16_;
			f_16_ = f_15_;
			f_15_ = -f_18_;
		}
		argument_0_[1] = f_15_;
		argument_0_[argument_6_] = f_16_;
		return argument_0_;
	}
	
	private static int method1337(float argument, float argument_2_,
	                              float argument_3_, int argument_4_) {
		float f = argument < 0.0F ? -argument : argument;
		float f_5_ = argument_2_ < 0.0F ? -argument_2_ : argument_2_;
		float f_6_ = !(argument_3_ < 0.0F) ? argument_3_ : -argument_3_;
		if (!(f < f_5_) || !(f_5_ > f_6_)) {
			if (f_6_ > f && f_5_ < f_6_) {
				if (argument_3_ > 0.0F)
					return 2;
				return 3;
			}
			if (!(argument > 0.0F))
				return 5;
			return 4;
		}
		if (argument_2_ > 0.0F)
			return 0;
		return 1;
	}
}
