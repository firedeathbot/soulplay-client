package com.soulplayps.client.opengl.gl14;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDepthRange;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.util.Cache;

public class StreamRenderer {
	public static void renderScene(final boolean inGame) {
		if (inGame) {
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);
			
			// normal depth range for in-game models
			glDepthRange(0, 1);
		} else {
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);
			glClear(GL_DEPTH_BUFFER_BIT);
			
			// models in interfaces appear to have inverted
			// depth buffers. that's no problem though, we can
			// just invert the calculation for these models!
			if (GraphicsDisplay.vboRendering)
				glDepthRange(1, 0);
		}
		
		// rendering non-alpha polygons before alpha polygons
		renderPolygons(false);
		
		// render alpha only
		renderPolygons(true);
		
		// glDisable(GL_TEXTURE_2D);
		Cache.clearTextureCount();
		
		// load identity matrix to reset all perspectives
		// for further openGL calls (sprites, etc..)
		glLoadIdentity();
		GraphicsDisplay.ortho();
	}
	
	private static void renderPolygons(final boolean alphaPass) {
		if (GraphicsDisplay.vboRendering)
			return;
		
		Triangle[] floorTriangles = alphaPass ? Cache.floorTrianglesAlpha : Cache.floorTriangles;
		Triangle[][] triangles = alphaPass ? Cache.trianglesAlpha : Cache.triangles;
		Quad[] quads = alphaPass ? Cache.quadsAlpha : Cache.quads;
		
		boolean switched = false;
		
		// drawing residual textured triangles now
		for (int texture = 0; texture < Texture.COUNT; texture++) {
			// new textures have proper alpha filters
			if (switched && texture > 50) {
				glAlphaFunc(GL_GREATER, 0.25f);
				switched = false;
			}
			
			// old textures don't have alpha; they look much
			// better with that filter
			if (!switched && texture > 0 && texture <= 50) {
				glAlphaFunc(GL_GREATER, 0.65f);
				switched = true;
			}
			
			// render floor triangles
			Triangle tt = floorTriangles[texture];
			renderTriangle(tt);
			floorTriangles[texture] = null;
			
			Quad tq = quads[texture];
			renderQuad(tq);
			quads[texture] = null;
			
			
			if (alphaPass && Cache.trianglesAlphaMap.size() > 0) {
				for (final Triangle tt2 : Cache.trianglesAlphaMap.values()) {
					renderTriangle(tt2);
				}
				Cache.trianglesAlphaMap.clear();
			} else {
				// render non-floor triangles
				for (int pool = 0; pool < Model.pools; pool++) {
					if (triangles[pool] == null)
						continue;
					
					Triangle tt2 = triangles[pool][texture];
					renderTriangle(tt2);
					triangles[pool][texture] = null;
				}
			}
		}
		
		glAlphaFunc(GL_GREATER, 0.25f);
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in renderPolygons(" + alphaPass + ")");
		}
	}
	
	
	private static void renderQuad(Quad tq) {
		// drawing all triangles with the same texture id
		// in only ONE glBegin() and glEnd() block per texture!
		if (tq != null) {
			int textureInternalId = tq.texture;
			int textureID = Cache.loadTexture(tq.texture, false);
			
			GraphicsDisplay.setTexture(textureID);
			
			// mipmapping (better near/far textures)
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			
			glBegin(GL_QUADS);
			
			while (tq != null) {
				if (textureID > 0) // 0 means an error has occured
				{
					// 0,0; 1,1; 0,1
					// 1,1; 0,0; 1,0
					
					glTexCoord2f(0, 1);
					setColor(Rasterizer.anIntArray1482[tq.c1], tq.alpha);
					glVertex3f(tq.x1, tq.y1, tq.z1);
					
					glTexCoord2f(1, 1);
					setColor(Rasterizer.anIntArray1482[tq.c2], tq.alpha);
					glVertex3f(tq.x2, tq.y2, tq.z2);
					
					glTexCoord2f(1, 0);
					setColor(Rasterizer.anIntArray1482[tq.c3], tq.alpha);
					glVertex3f(tq.x3, tq.y3, tq.z3);
					
					glTexCoord2f(0, 0);
					setColor(Rasterizer.anIntArray1482[tq.c4], tq.alpha);
					glVertex3f(tq.x4, tq.y4, tq.z4);
				} else {
					setColor(Rasterizer.anIntArray1482[tq.c1], tq.alpha);
					glVertex3f(tq.x1, tq.y1, tq.z1);
					
					setColor(Rasterizer.anIntArray1482[tq.c2], tq.alpha);
					glVertex3f(tq.x2, tq.y2, tq.z2);
					
					setColor(Rasterizer.anIntArray1482[tq.c3], tq.alpha);
					glVertex3f(tq.x3, tq.y3, tq.z3);
					
					setColor(Rasterizer.anIntArray1482[tq.c4], tq.alpha);
					glVertex3f(tq.x4, tq.y4, tq.z4);
				}
				
				// moving list pointer and clearing
				// old object list pointer
				Quad current = tq;
				tq = tq.next;
				current.next = null;
			}
			
			glEnd();
			
			if (!Client.disableOpenGlErrors) {
				GraphicsDisplay.getInstance().exitOnGLError("error in renderQuad(" + textureInternalId + ")");
			}
		}
	}
	
	private static void renderTriangle(Triangle tt) {
		// drawing all triangles with the same texture id
		// in only ONE glBegin() and glEnd() block per texture!
		if (tt != null) {
			int textureInternalId = tt.texture;
			int textureID = Cache.loadTexture(tt.texture, false);
			
			GraphicsDisplay.setTexture(textureID);
			
			// mipmapping (better near/far textures)
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			
			glBegin(GL_TRIANGLES);
			
			while (tt != null) {
				if (textureID > 0) // 0 means an error has occured
				{
					if (tt.uv) {
						//System.out.println(tt.u1 + ", " + tt.v1 + "; " + tt.u2 + ", " + tt.v2 + "; " + tt.u3 + ", " + tt.v3);
						glTexCoord2f(tt.u1, tt.v1);
						setColor(Rasterizer.anIntArray1482[tt.c1], tt.alpha);
						glVertex3f(tt.x1, tt.y1, tt.z1);
						
						glTexCoord2f(tt.u2, tt.v2);
						setColor(Rasterizer.anIntArray1482[tt.c2], tt.alpha);
						glVertex3f(tt.x2, tt.y2, tt.z2);
						
						glTexCoord2f(tt.u3, tt.v3);
						setColor(Rasterizer.anIntArray1482[tt.c3], tt.alpha);
						glVertex3f(tt.x3, tt.y3, tt.z3);
					} else {
						glTexCoord2f(0, 0);
						setColor(Rasterizer.anIntArray1482[tt.c1], tt.alpha);
						glVertex3f(tt.x1, tt.y1, tt.z1);
						
						glTexCoord2f(1, 1);
						setColor(Rasterizer.anIntArray1482[tt.c2], tt.alpha);
						glVertex3f(tt.x2, tt.y2, tt.z2);
						
						glTexCoord2f(0, 1);
						setColor(Rasterizer.anIntArray1482[tt.c3], tt.alpha);
						glVertex3f(tt.x3, tt.y3, tt.z3);
					}
				}
				
				// non-textured triangles here
				else {
					setColor(Rasterizer.anIntArray1482[tt.c1], tt.alpha);
					glVertex3f(tt.x1, tt.y1, tt.z1);
					
					setColor(Rasterizer.anIntArray1482[tt.c2], tt.alpha);
					glVertex3f(tt.x2, tt.y2, tt.z2);
					
					setColor(Rasterizer.anIntArray1482[tt.c3], tt.alpha);
					glVertex3f(tt.x3, tt.y3, tt.z3);
				}
				
				// moving list pointer and clearing
				// old object list pointer
				Triangle current = tt;
				tt = tt.next;
				current.next = null;
			}
			
			glEnd();
			
			if (!Client.disableOpenGlErrors) {
				GraphicsDisplay.getInstance().exitOnGLError("error in renderTriangle(" + textureInternalId + ", " + textureID + ")");
			}
		}
	}
	
	private static void setColor(int col, int a) {
		int argb = (col & ~0xff000000) | (a & 0xff) << 24;
		
		if (argb == GraphicsDisplay.currentColor) {
			return;
		}
		
		final float r = (float) ((col >> 16) & 0xFF);
		final float g = (float) ((col >> 8) & 0xFF);
		final float b = (float) ((col >> 0) & 0xFF);
		final float alpha = a / 256f;
		
		glColor4f(r / 256f, g / 256f, b / 256f, alpha);
		//glColor3f(r/256f, g/256f, b/256f);
		GraphicsDisplay.currentColor = argb;
	}
}
