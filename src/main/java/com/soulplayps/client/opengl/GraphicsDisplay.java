package com.soulplayps.client.opengl;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.*;
import org.lwjgl.util.glu.Sphere;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.applet.RSApplet;
import com.soulplayps.client.data.SignLink;
import com.soulplayps.client.node.object.WorldController;
import com.soulplayps.client.node.raster.RSFont;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.opengl.gl14.ModelRendering;
import com.soulplayps.client.opengl.gl14.StreamRenderer;
import com.soulplayps.client.opengl.gl15.VBORenderer;
import com.soulplayps.client.opengl.gl15.model.Scissor;
import com.soulplayps.client.opengl.gl15.model.TextureAtlas;
import com.soulplayps.client.opengl.gl15.model.TextureAtlasList;
import com.soulplayps.client.opengl.util.Cache;
import com.soulplayps.client.opengl.util.GLBufferManager;
import com.soulplayps.client.opengl.util.VBOManager;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Date;
import java.util.Deque;

import static org.lwjgl.opengl.GL11.*;
//import org.newdawn.slick.Color;
//import org.newdawn.slick.TrueTypeFont;

//import org.newdawn.slick.Color;
//import org.newdawn.slick.TrueTypeFont;

public class GraphicsDisplay {
	
	public static boolean canSwapModes = true;
	public static int currentColor;
	public static int currentTexture;

	// keeps track of the execution stack. we'll show that when an error appears
	private Deque<String> executionStack = new ArrayDeque<String>();
	
	// what is supported in our engine?
	public static boolean vboRenderingSupported = false;
	public static boolean FBOsSupported = false;
	public static boolean FBOsSupportedARB = false;
	public static boolean shadersSupported = false;
	
	// friendly colors enhance the RGB channels by multiplying them by a factor.
	// it gives the game an overall nicer look
	public static boolean friendlyColors = false;
	
	// use vbos to render the scene?
	// TODO: vboRendering is enabled by default now
	// see if people have problems with it
	public static boolean vboRendering = true;
	
	// use one buffer for multiple models when rendering?
	public static boolean multiModelBuffers = true;
	
	//public static boolean gpuRotate = false;
	public static boolean switchVboRendering = false;
	
	// flag to disable all rendering, basically for testing purpose
	private static boolean disableAll = false;

	// enables debugging, e.g. glGetError() flags
	public static boolean debug = true;
	
	// mipmapping? smoother far/near textures
	public static boolean useMipMapping = true;
	
	// maximum size of textures: width*height
	public static int MAX_TEXTURE_SIZE = 0;
	public static String GPU_VENDOR = "unknown";
	
	// using openGL mode?
	// default value is true, if something goes wrong we'll
	// automatically pick the software renderer!!
	public static boolean enabled = false;
	
	// use VBOs to render the scene?
	// TODO: rotating scene isn't implemented yet!
	// public static boolean useVBO = false;
	
	// setting variables
	public static boolean groundTextured = false;
	
	public static boolean modelsTextured = false;
	
	public static boolean antiAliasing = true;
	public static boolean lastAntiAliasing = true;
	public static boolean antiAliasingSupported = false;
	
	public static boolean resizeRequired = false;
	
	// do we need a fog refresh? upon logins!
	public boolean refreshFog = false;
	
	// graphics display only has 1 instance
	private static GraphicsDisplay instance = new GraphicsDisplay();
	
	public static GraphicsDisplay getInstance() {
		return instance;
	}
	
	// scaling factor for transforming game coordinates
	// into openGL coordinates
	// better just don't touch it ;)
	public static float SF = 0.001f;
	
	// a boolean to change GL states (images don't
	// (need any depth buffering when drawn)
	private volatile boolean useDepthBuffer = false;
	
	// width and height of this frame
	public volatile int width = 0;
	public volatile int height = 0;
	
	// no fog by default
	public volatile boolean useFog = false;
	
	// already initialized?
	public volatile boolean initialized = false;
	
	private volatile boolean showVersion = false;
	
	public void showVersion() {
		showVersion = true;
	}
	
	public void showVersionInfo() {
		final String info = getDetails();
		
		Thread t = new Thread(new Runnable() {
			public void run() {
				showShowMessageDialog("LWJGL Information", info, JOptionPane.INFORMATION_MESSAGE);
			}
		});
		t.start();
	}
	
	private void showShowMessageDialog(String title, String report, int type) {
		JOptionPane op = new JOptionPane(report, type);
		JDialog dialog = op.createDialog(Config.SERVER_NAME + " " + title);
		dialog.setAlwaysOnTop(true); //<-- this line
		dialog.setModal(true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}
	
	public boolean isIntel() {
		return GPU_VENDOR.toLowerCase().contains("intel");
	}
	
	private String getDetails() {
		final int modelBufs = Cache.getModelVBOs().size();
		final int floorBufs = Cache.getFloorVBOs().size();
		float fac = (modelBufs + floorBufs) > 0 ? ((float) GLBufferManager.getTotalGenBuffers() / (modelBufs + floorBufs)) : (GLBufferManager.getTotalGenBuffers() > 0 ? 10f : 3f);
		final String bufs = "glBufs: " + GLBufferManager.getTotalGenBuffers() + " [" + (Math.round(fac * 100f) / 100f) + "]";
		
		return ""
				+ "OS name: " + System.getProperty("os.name") + "\n"
				+ "OS version: " + System.getProperty("os.version") + "\n"
				+ "Java version: " + System.getProperty("java.version") + "\n"
				+ "Vendor: " + GPU_VENDOR + "\n"
				+ "OpenGL version: " + glGetString(GL_VERSION) + "\n"
				+ "Display: " + Display.getVersion() + "\n"
				+ "Adapter: " + Display.getAdapter() + "\n"
				+ "Max Texture: " + MAX_TEXTURE_SIZE + "\n"
				+ "Buffers: " + bufs + "\n"
				+ "Atlasses: " + TextureAtlas.ATLAS_COUNT + "\n"
				+ "TexBuffers: " + Arrays.toString(GLBufferManager.getTotalTextureBuffers()).replaceAll(", ", ", ") + "\n"
				;
	}
	
	private String getEngineCounters() {
		final int modelBufs = Cache.getModelVBOs().size();
		final int floorBufs = Cache.getFloorVBOs().size();
		float fac = (modelBufs + floorBufs) > 0 ? ((float) GLBufferManager.getTotalGenBuffers() / (modelBufs + floorBufs)) : (GLBufferManager.getTotalGenBuffers() > 0 ? 10f : 3f);
		
		final String glBuffers = ""
				+ "glTextures: " + Arrays.toString(GLBufferManager.getTotalTextureBuffers()).replaceAll(", ", ", ") + "\n"
				+ "glBuffers: " + GLBufferManager.getTotalGenBuffers() + " [" + (Math.round(fac * 100f) / 100f) + "]\n"
				+ "VBOs: " + (modelBufs + floorBufs) + " [m: " + modelBufs + ", f: " + floorBufs + "]\n"
				+ "glAtlasses: " + TextureAtlas.ATLAS_COUNT + "\n"
				+ "ver: " + Config.CLIENT_VERSION + "\n"
				+ "";
		
		return glBuffers;
	}
	
	private String writeExecutionStack() throws IOException {
		final String date = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss").format(new Date());
		final String fDir = SignLink.cacheDir + "/crashes/";
		final String dir = fDir + "crash_log_" + (date.replaceAll(":", "-")) + ".txt";
		
		final File file = new File(dir);
		final File folder = new File(fDir);
		if (!folder.exists())
			folder.mkdirs();
		
		final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		
		writer.write(Config.SERVER_NAME + " crash on: " + date);
		writer.newLine();
		writer.newLine();
		
		// write openGL info
		writer.write("Execution Environment Details:");
		writer.newLine();
		writer.write(getDetails());
		
		// write internal counters
		writer.write(getEngineCounters());
		writer.newLine();
		
		// write stack trace
		writer.newLine();
		writer.write("Stack trace (" + executionStack.size() + " items):");
		writer.newLine();
		
		for (String msg : executionStack) {
			writer.write(formatErrorMessage(msg));
			writer.newLine();
		}
		
		writer.write("END");
		writer.close();
		
		return dir;
	}
	
	private String formatErrorMessage(String msg) {
		return msg.replace("error in ", "");
	}
	
	public void showError(String message, boolean exit) {
		String stackFile = null;
		try {
			stackFile = writeExecutionStack();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final String errorReport = ""
				+ getDetails() + "\n"
				+ "Crashed function: " + "\n"
				+ message + "\n\n"
				+ (stackFile == null ? "No crash report written" : "Crash report written: " + stackFile) + "\n\n"
				+ "Press ok to exit." + "\n";
		
		if (exit) {
			showShowMessageDialog("Crash Report - " + Config.CLIENT_VERSION, errorReport, JOptionPane.ERROR_MESSAGE);
			
			if (Display.isCreated())
				Display.destroy();
			System.exit(-1);
		} else {
			/*Thread t = new Thread(new Runnable()
			{
				public void run()
				{
					showShowMessageDialog("Crash Report", errorReport, JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
			});
			t.start();*/
		}
	}
	
	// get maximum amount of anti aliasing samples
	private int getMaxSamples() {
		int samples = 0;
		
		try {
			PixelFormat format = new PixelFormat();
			Pbuffer pb = new Pbuffer(width, height, format, null);
			pb.makeCurrent();
			boolean supported = GLContext.getCapabilities().GL_ARB_multisample;
			
			// anti aliasing supported?
			if (supported) {
				antiAliasingSupported = true;
				samples = GL11.glGetInteger(GL30.GL_MAX_SAMPLES);
				if (samples > 4)
					samples = 4;
			} else {
				antiAliasingSupported = false;
				antiAliasing = lastAntiAliasing = false;
			}
			
			pb.destroy();
		} catch (LWJGLException e) {
			e.printStackTrace();
			antiAliasingSupported = false;
			antiAliasing = lastAntiAliasing = false;
		}
		
		return samples;
	}
	
	public int pixelFormatAttempts = 0;

	private PixelFormat getFormat() {
		if (!initialized)
			pixelFormatAttempts++;
		
		switch (pixelFormatAttempts) {
			// preferred pixel format
			case 1:
				// new PixelFormat(bpp, alpha, depth, stencil, samples)*/
				int maxSamples = getMaxSamples();
				if (!antiAliasing)
					maxSamples = 0;
				return new PixelFormat(32, 8, 24, 8, maxSamples);
			
			// preferred with alpha bits
			case 2:
				return new PixelFormat().withAlphaBits(8);
			
			// if nothing else works, we just have
			// to use the default format
			case 3:
				return new PixelFormat();
			
			// no PixelFormat available --> no openGL support!
			default:
				return null;
		}
	}

	public static boolean usingTextureAtlas = false;
	
	public void initialize(Client c) {
		if (!enabled) {
			Cache.clearSpriteCache();
			Cache.clearLetterCache();
			Cache.clearBackgroundCache();
			Cache.clearTextureCache();
			Display.destroy();
			return;
		}
		
		lastAntiAliasing = antiAliasing;
		this.width = Client.clientWidth;
		this.height = Client.clientHeight;
		
		try {
			Client.instance.frame.setResizable(true);
			Display.setResizable(true);
			
			// automatically set max FPS to highest refresh rate
			/*DisplayMode[] modes = Display.getAvailableDisplayModes();
			if (modes.length > 0) {
				DisplayMode highestFreq = modes[0];
				if (modes.length > 1) for (int i = 1; i < modes.length; i++) {
					DisplayMode mode = modes[i];
					if (mode.getFrequency() > highestFreq.getFrequency())
						highestFreq = mode;
				}
				int fps = highestFreq.getFrequency();
				fps = 1000;
				Client.myMaxFpsRate = maxFPS = fps;
				System.out.println("Set max FPS to " + fps);
			}*/
			
			Display.setDisplayModeAndFullscreen(new DisplayMode(width, height));
			Display.setVSyncEnabled(false); // v-sync will automatically cap FPS for each individual screen
			Display.setTitle(Config.SERVER_NAME);
			
			// find working PixelFormat
			while (true) {
				PixelFormat format = getFormat();
				
				// no openGL support
				if (format == null) {
					throw new LWJGLException("PixelFormat not found.");
				}
				
				// try creating display; if it doesn't work, we
				// grab the next pixel format in our list
				try {
					Client.instance.frame.setResizable(false);
					Display.setResizable(false);
					Display.setParent(Client.instance.canvas);
					Display.create(format);
					break;
				} catch (LWJGLException e) {
					e.printStackTrace();
					continue;
				}
			}
			
			if (initialized) {
				return;
			}
			
			MAX_TEXTURE_SIZE = glGetInteger(GL_MAX_TEXTURE_SIZE);
			GPU_VENDOR = glGetString(GL_VENDOR);
			System.out.println(GPU_VENDOR);
			
			int gt = 0; // ground textures
			int mt = 0; // model textures
			
			// 32+ textures per atlas
			if (MAX_TEXTURE_SIZE >= 2048) {
				// no more than 32 ground textures per atlas
				// there is no region that has more than that!
				gt = 1024; // ground textures
				
				// unlimited amount of model texture atlases
				// as much as the GPU gives us.
				mt = MAX_TEXTURE_SIZE / 2; // model textures
				// mt = 512;
				
				if (mt >= 4096)
					mt = 4096;
			}
			
			// not enough space on the GPU, don't use atlasses
			else {
				gt = 0; // ground textures
				mt = 0; // model textures
			}
			
			// initialize gigantic texture atlases
			if (gt != 0) {
				int maxAtlasses = 2;
				
				// System.out.println("size: "+mt);
				
				// TODO: finish atlasses properly
				// note: atlasses don't really make rendering faster!
				//Cache.groundTextureAtlas128 = new TextureAtlasList(gt, 128, 1, false, true);
				Cache.modelTextureAtlas128 = new TextureAtlasList(mt, 128, maxAtlasses, false, true);
				
				usingTextureAtlas = true;
			}
			
			Mouse.create();
			Keyboard.create();
			Keyboard.enableRepeatEvents(true);
			
			// smooth shading isn't really needed
			// at least I see no difference
			glShadeModel(GL_SMOOTH);
			
			// culling is bugging our ground
			// we should enable culling! for now it makes no difference
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK); // Doesn't draw back faces
			
			// alpha blending
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			
			// fix for trees with alpha values
			// basically this ignores special alpha values
			// in the z-buffer. so we can't have invisible textures!
			glAlphaFunc(GL_GREATER, 0.2f);
			glEnable(GL_ALPHA_TEST);
			
			// fixes invisible loading text on some GPUs
			glPointSize(1.5f);
		} catch (LWJGLException e) {
			canSwapModes = false;

			try {
				Display.destroy();
			} catch (Exception e1) {
				e.printStackTrace();
			}
			
			String glVersion = "";
			try {
				PixelFormat format = new PixelFormat();
				Pbuffer pb = new Pbuffer(width, height, format, null);
				pb.makeCurrent();
				glVersion = glGetString(GL_VERSION);
			} catch (LWJGLException e1) {
				e1.printStackTrace();
			}

			e.printStackTrace();
			final String errorReport = "An error occurred while initializing OpenGL.\nBelow is debug information that you can post on forums.\n\n"
					+ "Details: " + e.getMessage() + "\n"
					+ "OS name: " + System.getProperty("os.name") + "\n"
					+ "OS version: " + System.getProperty("os.version") + "\n"
					+ "OpenGL version: " + glVersion + "\n"
					+ "Display: " + Display.getVersion() + "\n"
					+ "Adapter: " + Display.getAdapter() + "\n"
					//+ "\n"
					//+ "We recommend reading a guide how to update GPU drivers:\n"
					//+ "http://www.positech.co.uk/support/videocards.html\n"
					+ "\n"
					+ "Click Ok to continue playing without OpenGL.\n"
					+ "\n";
			
			showShowMessageDialog("OpenGL Error", errorReport, JOptionPane.WARNING_MESSAGE);
			
			//JOptionPane.showMessageDialog(null, errorReport, "LWJGL Error", JOptionPane.WARNING_MESSAGE);
			
			// software renderer fallback
			enabled = false;
		}
		
		// check what's supported on this machine
		if (enabled) {
			// Vertex Buffer Objects supported
			if (GLContext.getCapabilities().OpenGL15)
				vboRenderingSupported = true;
			
			if (false) { // make sure these features are disabled always since we don't use them
				// FBO support?
				if (GLContext.getCapabilities().OpenGL30)
					FBOsSupported = true; // FBO support with ARB extension?
				else if (GLContext.getCapabilities().GL_ARB_framebuffer_object)
					FBOsSupported = FBOsSupportedARB = true;
				
				if (GLContext.getCapabilities().OpenGL20)
					shadersSupported = true;
			}
			
			// no vbo rendering in classic client, or it's not supported (openGL version < 1.5)
			if (!GraphicsDisplay.enabled || !GraphicsDisplay.vboRenderingSupported) {
				GraphicsDisplay.vboRendering = true;
				GraphicsDisplay.switchVboRendering = true;
			}
			
			/*// vbo rendering supported --> prepare the switch
			if (GraphicsDisplay.vboRendering)
			{
				System.out.println("yes");
				GraphicsDisplay.vboRendering = false;
				GraphicsDisplay.switchVboRendering = true;
			}*/
			
			System.out.println("vboRenderingSupported=" + vboRenderingSupported
					+ ", FBOsSupported=" + FBOsSupported
					+ ", shadersSupported=" + shadersSupported);
		}
		
		initialized = true;
	}
	
	public void addScrollbarBox2(int h, int y, int color, int x, int w) {
		addBox(x, y, w, h, color);
	}
	
	public void addScrollbarBox(int h, int y, int x, int color, int w) {
		addBox(x, y, w, h, color);
	}
	
	public void addBox(int x, int y, int width, int height, int color) {
		addBox(x, y, width, height, color, 255, false, null);
	}
	
	public void addBox(int x, int y, int width, int height, int color, int alpha, boolean ignoreAtlas, Scissor scissor) {
		// use letter atlas if we have more than 1024x1024 bytes available
		// boxes also go into the letter atlas, because text is drawn
		// above these boxes and we want a rendering order here.
		if (!ignoreAtlas && GraphicsDisplay.vboRendering && GraphicsDisplay.MAX_TEXTURE_SIZE >= 1024 && Client.loggedIn) {
			y = this.height - y - height;
			Cache.addAtlasBox(0, x, y, color, width, height, alpha, scissor);
			return;
		}
		
		if (useDepthBuffer) {
			useDepthBuffer = false;
			glDisable(GL_DEPTH_TEST);
		}
		
		glDisable(GL_TEXTURE_2D);
		
		y = (Client.clientHeight - y);
		setColor(color, alpha);
		
		glBegin(GL_QUADS);
		
		glVertex2f(x * SF, y * SF);
		glVertex2f(x * SF, (y - height) * SF);
		glVertex2f((x + width) * SF, (y - height) * SF);
		glVertex2f((x + width) * SF, y * SF);
		
		glEnd();
		
		glEnable(GL_TEXTURE_2D);
	}
	
	public void drawRect(int x, int y, int w, int h, int color, int alpha) {
		if (useDepthBuffer) {
			useDepthBuffer = false;
			glDisable(GL_DEPTH_TEST);
		}
		
		glDisable(GL_TEXTURE_2D);
		setColor(color, alpha);
		
		float x2 = x + (w - 1);
		float y1 = Client.clientHeight - y;
		float y2 = y1 - (h - 1);
		glBegin(GL_LINE_LOOP);
		glVertex2f(x * SF, y1 * SF);
		glVertex2f(x * SF, y2 * SF);
		glVertex2f(x2 * SF, y2 * SF);
		glVertex2f(x2 * SF, y1 * SF);
		glEnd();
		
		glEnable(GL_TEXTURE_2D);
	}

	private ByteBuffer gradientBuffer;
	public void drawGradient(int x, int y, int width, int height, int color1, int color2, int trans1, int trans2, Scissor scissor) {
		if (width <= 0 || height <= 0) {
			return;
		}

		int size = width * height * 4;
		if (gradientBuffer == null || gradientBuffer.capacity() != size) {
			gradientBuffer = ByteBuffer.allocateDirect(size).order(ByteOrder.nativeOrder());
		} else {
			gradientBuffer.clear();
		}

		int posY = 0;
		int stepY = 65536 / height;

		for (int yPos = -height; yPos < 0; yPos++) {
			int xPos = 65536 - posY >> 8;
			int pixel = posY >> 8;
   			int var15 = (xPos * trans2 + pixel * trans1 & '\uff00') >>> 8;
   			if (var15 == 0) {
				for (xPos = -width; xPos < 0; xPos++) {
					gradientBuffer.put((byte) 0);
					gradientBuffer.put((byte) 0);
					gradientBuffer.put((byte) 0);
					gradientBuffer.put((byte) 0);
				}
	   		} else {
	   			int var16 = (pixel * (color2 & 16711935) + xPos * (color1 & 16711935) & -16711936) + (pixel * (color2 & '\uff00') + xPos * (color1 & '\uff00') & 16711680) >>> 8;
	   			int var18 = ((var16 & 16711935) * var15 >> 8 & 16711935) + (var15 * (var16 & '\uff00') >> 8 & '\uff00');
				for (xPos = -width; xPos < 0; xPos++) {
					gradientBuffer.put((byte) (var18 >> 16));
					gradientBuffer.put((byte) (var18 >> 8));
					gradientBuffer.put((byte) (var18 & 0xff));
					gradientBuffer.put((byte) var15);
				}
	   		}

			posY += stepY;
		}

		glBlendFunc(GL_ONE, GL_ONE);
		glRasterPos2f(x * SF, (Client.clientHeight - y - height) * SF);
		try {
			gradientBuffer.flip();
			glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, gradientBuffer);
			if (!Client.disableOpenGlErrors) {
				exitOnGLError("error in gradient()");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public void drawVerticalLne(int x, int y, int height, int color, int alpha, Client cl, Scissor scissor) {
		// use letter atlas if we have more than 1024x1024 bytes available
		// boxes also go into the letter atlas, because text is drawn
		// above these boxes and we want a rendering order here.
		if (GraphicsDisplay.vboRendering && GraphicsDisplay.MAX_TEXTURE_SIZE >= 1024 && Client.loggedIn) {
			y = this.height - y - height;
			Cache.addAtlasBox(0, x, y, color, 1, height, alpha, scissor);
			return;
		}
		
		if (useDepthBuffer) {
			useDepthBuffer = false;
			glDisable(GL_DEPTH_TEST);
		}
		
		glDisable(GL_TEXTURE_2D);
		
		y = (Client.clientHeight - y);
		setColor(color, alpha);
		
		glBegin(GL_LINES);
		
		glVertex2f(x * SF, y * SF);
		glVertex2f(x * SF, (y - height) * SF);
		
		glEnd();
		
		glEnable(GL_TEXTURE_2D);
	}
	
	public void addFloorQuad(float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4, float z1, float z2, float z3, float z4,
	                         int color1, int color2, int color3, int color4, int texture, int tileX, int tileY, int part, int pool) {
		if (disableAll)
			return;
		
		int alpha = 256;
		
		if (!groundTextured)
			texture = 0;
		else if (texture <= 0)
			texture = Rasterizer.DEFAULT_GROUND_TEXTURE;
		
		if (color1 <= 0 || color2 <= 0 || color3 <= 0 || color4 <= 0 || color1 > 0xFFFF || color2 > 0xFFFF || color3 > 0xFFFF || color4 > 0xFFFF)
			return;
		
		if (!groundTextured)
			texture = 0;
		
		Cache.addQuad(texture, x1 * SF, x2 * SF, x3 * SF, x4 * SF, (height - y1) * SF, (height - y2) * SF, (height - y3) * SF, (height - y4) * SF, z1
				* SF, z2 * SF, z3 * SF, z4 * SF, color1, color2, color3, color4, alpha, pool);
		
	}
	
	public void addFloorVertex(float x1, float x2, float x3, float y1, float y2, float y3, float z1, float z2, float z3, int color1, int color2, int color3,
	                           int texture, int tileX, int tileY, int part, int pool) {
		addFloorVertex(x1, x2, x3, y1, y2, y3, z1, z2, z3, color1, color2, color3, texture, tileX, tileY, part, null, null, null, 0, 0, 0, false, pool);
	}
	
	public void addFloorVertex(float x1, float x2, float x3, float y1, float y2, float y3, float z1, float z2, float z3, int color1, int color2, int color3,
	                           int texture, int tileX, int tileY, int part, int[] texU, int[] texV, int[] texDepths, int pointX, int pointY, int pointZ, boolean unused, int pool) {
		
		if (disableAll)
			return;
		
		int alpha = 256;
		
		if (!groundTextured)
			texture = 0;
		else if (texture <= 0)
			texture = Rasterizer.DEFAULT_GROUND_TEXTURE;
		
		if (color1 <= 0 || color2 <= 0 || color3 <= 0 || color1 > 0xFFFF || color2 > 0xFFFF || color3 > 0xFFFF)
			return;
		
		if (texU != null) {
			int pI = 0;
			int mI = 1;
			int nI = 3;
			
			float px = (float) texU[pI];
			float py = (float) 0;
			float pz = (float) texDepths[pI];
			
			float mx = (float) texU[mI] - px;
			float my = (float) 0 - py;
			float mz = (float) texDepths[mI] - pz;
			float nx = (float) texU[nI] - px;
			float ny = (float) 0 - py;
			float nz = (float) texDepths[nI] - pz;
			
			float xx = (float) texU[pointX] - px;
			float yx = (float) 0 - py;
			float zx = (float) texDepths[pointX] - pz;
			float xy = (float) texU[pointY] - px;
			float yy = (float) 0 - py;
			float zy = (float) texDepths[pointY] - pz;
			float xz = (float) texU[pointZ] - px;
			float yz = (float) 0 - py;
			float zz = (float) texDepths[pointZ] - pz;
			
			float mnyz = my * nz - mz * ny;
			float mnxz = mz * nx - mx * nz;
			float mnxy = mx * ny - my * nx;
			float mn1 = ny * mnxy - nz * mnxz;
			float mn2 = nz * mnyz - nx * mnxy;
			float mn3 = nx * mnxz - ny * mnyz;
			
			float inv = 1f / (mn1 * mx + mn2 * my + mn3 * mz);
			
			float u1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
			float u2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
			float u3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
			
			mn1 = my * mnxy - mz * mnxz;
			mn2 = mz * mnyz - mx * mnxy;
			mn3 = mx * mnxz - my * mnyz;
			
			inv = 1.0F / (mn1 * nx + mn2 * ny + mn3 * nz);
			
			float v1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
			float v2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
			float v3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
			
			if (u1 < 0) u1 = 0;
			if (u1 > 1) u1 = 1;
			if (u2 < 0) u2 = 0;
			if (u2 > 1) u2 = 1;
			if (u3 < 0) u3 = 0;
			if (u3 > 1) u3 = 1;
			
			if (v1 < 0) v1 = 0;
			if (v1 > 1) v1 = 1;
			if (v2 < 0) v2 = 0;
			if (v2 > 1) v2 = 1;
			if (v3 < 0) v3 = 0;
			if (v3 > 1) v3 = 1;
			
			Cache.addFloorTriangle(texture, x1 * SF, x2 * SF, x3 * SF, (height - y1) * SF, (height - y2) * SF, (height - y3) * SF, z1 * SF, z2 * SF, z3 * SF,
					color1, color2, color3, alpha, 1f - u1, 1f - u2, 1f - u3, v1, v2, v3, pool);
		} else {
			Cache.addFloorTriangle(texture, x1 * SF, x2 * SF, x3 * SF, (height - y1) * SF, (height - y2) * SF, (height - y3) * SF, z1 * SF, z2 * SF, z3 * SF,
					color1, color2, color3, alpha, pool);
		}
		
	}
	
	public static int backgroundColor = 0;
	
	public void setBackground(double advancement) {
		setBackground(0, true, advancement);//white color
	}
	
	private void applyFog() {
		rgbaBuffer.clear();
		rgbaBuffer.put((float) ((backgroundColor >> 16) & 0xFF) / 256f); // R
		rgbaBuffer.put((float) ((backgroundColor >> 8) & 0xFF) / 256f); // G
		rgbaBuffer.put((float) ((backgroundColor >> 0) & 0xFF) / 256f); // B
		rgbaBuffer.put(1f); // A
		rgbaBuffer.flip();
		
		glEnable(GL_FOG);
		glFog(GL_FOG_COLOR, rgbaBuffer);
		glFogi(GL_FOG_MODE, GL_LINEAR);
		glFogf(GL_FOG_START, fogStart);
		glFogf(GL_FOG_END, fogEnd);
	}
	
	private final FloatBuffer rgbaBuffer = BufferUtils.createFloatBuffer(4);
	private double toRed = 0;
	private double toGreen = 0;
	private double toBlue = 0;
	
	public void applyBackgroundColor(int to, double advancement) {
		double r2 = (double) ((to >> 16) & 0xFF);
		double g2 = (double) ((to >> 8) & 0xFF);
		double b2 = (double) ((to >> 0) & 0xFF);
		
		if (!Client.drawAnimatedWorldBackground && !Client.loggedIn) {
			toRed = r2;
			toGreen = g2;
			toBlue = b2;
			
			backgroundColor = (((int) toRed & 0xFF) << 16) + (((int) toGreen & 0xFF) << 8) + (((int) toBlue & 0xFF) << 0);
			glClearColor((float) toRed / 256f, (float) toGreen / 256f, (float) toBlue / 256f, 0f);
			
			return;
		}
		
		double round = 0.01;
		double speed = Client.fps / 0.3;
		if (speed == 0)
			speed = 1;
		double rd = (r2 - toRed) / speed;
		double gd = (g2 - toGreen) / speed;
		double bd = (b2 - toBlue) / speed;
		
		//System.out.println("d: "+rd+", "+gd+", "+bd);
		
		boolean finish = closeTo(rd, round) || closeTo(gd, round) || closeTo(bd, round);
		
		if (!finish) {
			toRed += rd;
			toGreen += gd;
			toBlue += bd;
		} else {
			toRed = r2;
			toGreen = g2;
			toBlue = b2;
		}
		
		backgroundColor = (((int) toRed & 0xFF) << 16) + (((int) toGreen & 0xFF) << 8) + (((int) toBlue & 0xFF) << 0);
		glClearColor((float) toRed / 256f, (float) toGreen / 256f, (float) toBlue / 256f, 0f);
	}
	
	private boolean closeTo(double d, double m) {
		if (d < 0)
			d *= -1;
		
		return d >= -m && d <= m;
	}
	
	public void setFog() {
		float rd = VBORenderer.getRD() + 0.05f;
		//float maxZoom = Client.getMaxZoom();
		//rd -= ((maxZoom - Client.cameraZoom) / maxZoom) * 1;
		
		float fd = rd / 2f;
		
		if (WorldController.viewDistance <= 40)
			fd = 0.85f;
		if (WorldController.viewDistance <= 50)
			fd = 1f;
		else if (WorldController.viewDistance <= 60)
			fd = 1.15f;
		else if (WorldController.viewDistance <= 80)
			fd = 1.5f;
		else
			fd = 2f;
		
		if (fd > 3)
			fd = 3;
		
		// move fog back a little bit when shadows
		// are enabled to get the same result
		if (Client.useShadows)
			rd += 0.25;
		
		fogEnd = rd - 1;
		fogStart = fogEnd - fd;
		
		
		//float plus = Client.cameraZoom / 1100f;
		
		//fogStart += plus;
		//fogEnd += plus;
		
		applyFog();
		
	}
	
	public void setBackground(int col, boolean isFrustum, double advancement) {
		applyBackgroundColor(col, advancement);
		
		// new background color, set fog too!
		if (col != backgroundColor || Client.displayFog != this.useFog || refreshFog || vboRendering) {
			useFog = Client.displayFog;
			refreshFog = false;
			
			fogStart = ((float) WorldController.viewDistance) / 25f;
			
			if (WorldController.viewDistance <= 30)
				fogEnd = fogStart + 0.5f;
			else if (WorldController.viewDistance <= 40)
				fogEnd = fogStart + 0.75f;
			else if (WorldController.viewDistance <= 50)
				fogEnd = fogStart + 1f;
			else if (WorldController.viewDistance <= 60)
				fogEnd = fogStart + 1.25f;
			else
				fogEnd = fogStart + 1.5f;
			
			
			if (useFog) {
				if (!vboRendering) {
					fogStart -= 0.15f;
					fogEnd -= 0.15f;
					
					glMatrixMode(GL_PROJECTION);
					glLoadIdentity();
					
					// width from 0 to width
					// height from 0 to height
					// z-buffer from -3f to 3f
					glOrtho(0f, width * SF, 0f, height * SF, fogStart, -fogEnd);
					
					glMatrixMode(GL_MODELVIEW);
					glLoadIdentity();
					
					applyFog();
				} else {
					setFog();
				}
			} else {
				fogStart -= 0.05f;
				fogEnd -= 0.05f;
				glDisable(GL_FOG);
			}
			
			if (!Client.disableOpenGlErrors) {
				exitOnGLError("error in setBackground(" + col + ")");
			}
		}
		
		if (!Client.loggedIn)
			fogEnd = 2.75f;
	}

	public static void setTexture(int texture) {
		if (texture == GraphicsDisplay.currentTexture) {
			return;
		}

		glBindTexture(GL_TEXTURE_2D, texture);
		GraphicsDisplay.currentTexture = texture;
	}

	public static void setColor(int col, int a) {
		int argb = (col & ~0xff000000) + ((a & 0xff) << 24);

		if (argb == GraphicsDisplay.currentColor) {
			return;
		}

		byte r = (byte) (col >> 16);
		byte g = (byte) (col >> 8);
		byte b = (byte) col;
		byte alpha = a > 255 ? (byte) -1 : (byte) a;

		glColor4ub(r, g, b, alpha);

		GraphicsDisplay.currentColor = argb;
	}

	public void addLetter(byte[] letter, int offX, int offY, int width, int height, int color, char ch, RSFont tda) {
		addLetter(letter, offX, offY, width, height, color, ch, tda, true, null);
	}
	
	public void addLetter(byte[] letter, int offX, int offY, int width, int height, int color, char ch, RSFont tda, boolean cache, Scissor scissor) {
		int alpha = 255 - (color >> 24) & 0xFF;
		//int alpha = 255;
		addLetter(letter, offX, offY, width, height, color, ch, tda, cache, alpha, scissor);
	}
	
	public void addLetter(byte[] letter, int offX, int offY, int width, int height, int color, char ch, RSFont tda, boolean cache, int alpha, Scissor scissor) {
		if (!initialized) {
			return;
		}

		// fixing offY value for openGL
		offY = this.height - offY - height;
		int chInt = (int) ch;
		long hash = Cache.letterHash(chInt, tda.textDrawingAreaID, cache ? 1 : 0);
		
		// use letter atlas if we have more than 1024x1024 bytes available
		// and if the letter is supposed to be cached
		if (GraphicsDisplay.vboRendering && GraphicsDisplay.MAX_TEXTURE_SIZE >= 1024)
		{
			Cache.addAtlasLetter(hash, offX, offY, color, alpha, letter, width, height, tda, !cache, scissor);
			return;
		}
		
		//if (true)
		//	return;
		
		if (useDepthBuffer) {
			useDepthBuffer = false;
			glDisable(GL_DEPTH_TEST);
			glEnable(GL_TEXTURE_2D);
		}
		
		/*useDepthBuffer = true;
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);*/
		
		// that only displays fps
		//if (offY > 20)
		//	return;
		
		if (true) {
			// Cache.clearLetterCache();
			// return;
		}
		// Cache.clearLetterCache();
		// if (chInt <= 0 || true)
		// System.out.println("chint: "+chInt+", "+tda.textDrawingAreaID);
		
		
		int textureID;
		if (cache) {
			int value = Cache.letterCache.get(hash);
			if (value != 0) {
				// images are textures --> load image texture
				textureID = value;
			} else {
				// use white color here, so we can change it with setColor()
				textureID = VBOManager.loadLetterTexture(letter, width, height, 0xFFFFFF);
				Cache.letterCache.put(hash, textureID);
			}
		} else {
			// use white color here, so we can change it with setColor()
			textureID = VBOManager.loadLetterTexture(letter, width, height, 0xFFFFFF);
		}
		// int textureID = loadTextureBytes(letter, width, height, color);
		
		// set color of letter
		setColor(color, 256);
		
		// bind texture --> we're now using it
		GraphicsDisplay.setTexture(textureID);
		
		// begin drawing texture quad
		glBegin(GL_QUADS);
		
		glTexCoord2f(0, 0);
		glVertex2f(offX * SF, offY * SF);
		
		glTexCoord2f(1, 0);
		glVertex2f(width * SF + offX * SF, offY * SF);
		
		glTexCoord2f(1, 1);
		glVertex2f(width * SF + offX * SF, height * SF + offY * SF);
		
		glTexCoord2f(0, 1);
		glVertex2f(offX * SF, height * SF + offY * SF);
		
		// finished drawing quad
		glEnd();
		
		if (!cache) {
			GLBufferManager.deleteTextureBuffer(GLBufferManager.TEXTURE_LETTER, textureID);
		}
		
		if (!Client.disableOpenGlErrors) {
			exitOnGLError("error in addText(" + ch + ", " + cache + ")");
		}
	}
	
	public void addBackground(byte[] image, int[] pixels, int offX, int offY, int width, int height, int alpha) {
		if (!initialized || disableAll)
			return;
		
		System.out.println("add: " + image.length);
		
		if (useDepthBuffer) {
			useDepthBuffer = false;
			glDisable(GL_DEPTH_TEST);
			glEnable(GL_TEXTURE_2D);
		}
		
		// fixing offY value for openGL
		offY = this.height - offY - height;
		
		// set background color, basically this avoids annoying
		// interactions with ground pixels and color flickering
		setColor(0xFFFFFFFF, 256);
		
		// images are textures --> load image texture
		long hash = Cache.imageHash(offX, offY, width, height, 0);
		int textureID = Cache.backgroundCache.get(hash);
		if (textureID == 0) {
			// System.out.println("miss!");
			textureID = VBOManager.loadTextureRGBA_Direct(image, pixels, width, height, alpha);
			Cache.backgroundCache.put(hash, textureID);
		}
		
		// bind texture --> we're now using it
		GraphicsDisplay.setTexture(textureID);
		
		// begin drawing texture quad
		glBegin(GL_QUADS);
		
		glTexCoord2f(0, 0);
		glVertex2f(offX * SF, offY * SF);
		
		glTexCoord2f(1, 0);
		glVertex2f(width * SF + offX * SF, offY * SF);
		
		glTexCoord2f(1, 1);
		glVertex2f(width * SF + offX * SF, height * SF + offY * SF);
		
		glTexCoord2f(0, 1);
		glVertex2f(offX * SF, height * SF + offY * SF);
		
		// finished drawing quad
		glEnd();
		
		if (!Client.disableOpenGlErrors) {
			exitOnGLError("error in addBackground()");
		}
		// glBindTexture(GL_TEXTURE_2D, 0);
		
		// immediately delete texture
		// glDeleteTextures(textureID);
	}
	
	// sometimes, we manually want to enable or
	// disable the depth buffer. especially
	// outside the actual game rendering
	public void setDepthBuffer(boolean depth) {
		useDepthBuffer = depth;
	}
	
	public boolean isSpriteCached(int offX, int offY, int width, int height, long uid) {
		if (uid < 0) return false;
		
		long hash = Cache.imageHash(offX, offY, width, height, uid);
		return Cache.spriteCache.containsKey(hash);
	}
	
	public void addSphere(int pixels[], int offX, int offY, float radius, int alpha, long uid, float rotation, int height, int width, int transX, int transY) {
		if (!initialized || disableAll)
			return;
		
		// fixing offY value for openGL
		offY = this.height - offY - height;
		
		
		if (useDepthBuffer) {
			useDepthBuffer = false;
			glDisable(GL_DEPTH_TEST);
			glEnable(GL_TEXTURE_2D);
		}
		
		// set background color, basically this avoids annoying
		// interactions with ground pixels and color flickering
		setColor(0xFFFFFFFF, 256);
		int textureID;
		
		// disable caching --> debugging
		if (uid == -1)
			textureID = VBOManager.loadTextureRGBA(pixels, width, height, alpha, false, -1);
			
			// this type is the caching type, whenever a negative uid arrives
			// we need to update the texture, e.g. minimap or compass
			// basically everything that moves
		else if (uid < 0) {
			long hash = Cache.imageHash(offX, offY, width, height, -uid);
			int value = Cache.spriteCache.get(hash);
			if (value != 0) {
				textureID = value;
				// update the texture
				VBOManager.loadTextureRGBA(true, textureID, pixels, width, height, alpha, false, -1);
			} else {
				// System.out.println("miss!");
				textureID = VBOManager.loadTextureRGBA(pixels, width, height, alpha, false, -1);
				Cache.spriteCache.put(hash, textureID);
			}
		} else {
			// images are textures --> load image texture
			long hash = Cache.imageHash(offX, offY, width, height, uid);
			int value = Cache.spriteCache.get(hash);
			if (value != 0) {
				textureID = value;
			} else {
				// System.out.println("miss!");
				textureID = VBOManager.loadTextureRGBA(pixels, width, height, alpha, false, -1);
				Cache.spriteCache.put(hash, textureID);
			}
		}
		
		
		// rotate sprites
		if (rotation > 0f) {
			glTranslatef((offX + width / 2) * SF, (offY + height / 2) * SF, 0);
			glRotatef(rotation, 0f, 0f, 1f);
			glTranslatef(-(offX + width / 2) * SF, -(offY + height / 2) * SF, 0);
		}
		
		//glTranslatef(-(offX + width / 2), -(offY2 + height / 2), 0);
		
		// bind texture --> we're now using it
		GraphicsDisplay.setTexture(textureID);
		
		Sphere s = new Sphere();
		s.draw(rotation, (int) radius, (int) radius);
		
		// scene was rotated --> reset that
		glLoadIdentity();
		
		// immediately delete texture
		if (uid == -1) {
			GLBufferManager.deleteTextureBuffer(GLBufferManager.TEXTURE_SPRITE, textureID);
		}
		
		if (!Client.disableOpenGlErrors) {
			exitOnGLError("error in addSphere(" + uid + ")");
		}
	}
	
	public void addSprite(Sprite sprite, int offX, int offY, int width, int height, int alpha, long uid) {
		addSprite(sprite, offX, offY, width, height, alpha, uid, 0f);
	}
	
	public static float getSpriteDepth() {
		return VBORenderer.atlasDepth += VBORenderer.ATLAS_HEIGHT_MODIFIER;
	}
	
	public void addSprite(Sprite sprite, int offX, int offY, int width, int height, int alpha, long uid, float rotation) {
		if (!initialized || disableAll) {
			return;
		}
		
		// fixing offY value for openGL
		int offY2 = offY;
		offY = this.height - offY - height;
		
		width = sprite.maxWidth;
		height = sprite.maxHeight;
		
		// we may use a texture atlas for rendering
		if (GraphicsDisplay.vboRendering && GraphicsDisplay.MAX_TEXTURE_SIZE >= 1024 && Client.loggedIn && uid != -1) {
			// render the scene after drawing a large sprite,
			// which is an indicator for an interface.
			// we don't want text or sprites to be on top of
			// certain interfaces, although in reality they aren't!
			//if (width > 240 || height > 240)
			//	GraphicsDisplay.getInstance().renderScene();
			
			// priority problems in interfaces.
			// can't use atlasses yet for sprites
			// TODO: write some kind of priority list, or add z
			// values to every sprite/model that is drawn in interfaces
			
			// note: atlasses don't really make rendering faster!
			// is this sprite storable in an atlas?
			if (Cache.addAtlasSprite(uid, offX, offY, alpha, width, height, sprite)) {
				return;
			}
		}
		
		// do we have to load the sprite pixels
		// onto the GPU?
		long imageHash = Cache.imageHash(offX, offY, width, height, uid);
		boolean cached = Cache.spriteCache.containsKey(imageHash);
		boolean loadSpritePixels = uid < 0 || !cached;
		
		//if (loadSpritePixels)//TODO sprite reloading unloading?
		//	sprite.reload();

		if (sprite.myPixels == null && loadSpritePixels) {
			return;
		}
		addSpritePixels(sprite.myPixels, offX, offY2, width, height, alpha, uid);
		
		//if (loadSpritePixels && uid != -1)
		//	sprite.clear();
		
		if (!Client.disableOpenGlErrors) {
			exitOnGLError("error in addSprite(" + uid + ")");
		}
	}

	public void addSpritePixels(int[] pixels, int offX, int offY, int width, int height, int alpha, long uid) {
		addSpritePixels(pixels, offX, offY, width, height, alpha, uid, 0.0f);
	}

	public void addSpritePixels(int[] pixels, int offX, int offY, int width, int height, int alpha, long uid, double rotation) {
		offY = this.height - offY - height;
		
		// item sprites
		boolean requiresDepth = false;//vboRendering && (width == 32 || width == 33);
		
		if (requiresDepth) {
			if (!useDepthBuffer) {
				useDepthBuffer = true;
				glEnable(GL_DEPTH_TEST);
				glEnable(GL_TEXTURE_2D);
			}
		} else {
			if (useDepthBuffer) {
				useDepthBuffer = false;
				glDisable(GL_DEPTH_TEST);
				glEnable(GL_TEXTURE_2D);
			}
		}
		
		//glEnable(GL_DEPTH_TEST);
		
		// set background color, basically this avoids annoying
		// interactions with ground pixels and color flickering
		setColor(0xFFFFFFFF, alpha);
		
		// alpha is set in color, the actual sprite contains no alpha values now
		alpha = 255;
		
		// images are textures --> load image texture
		// int textureID = loadTextureRGBA(pixels, width, height, alpha);
		
		int textureID;
		// type -1, see if old entry exists and delete
		// it if necessary
		// disable caching --> debugging
		if (uid == -1) {
			textureID = VBOManager.loadTextureRGBA(pixels, width, height, alpha, false, -1);
			// this type is the caching type, whenever a negative uid arrives
			// we need to update the texture, e.g. minimap or compass
			// basically everything that moves
		} else if (uid < 0) {
			long hash = Cache.imageHash(offX, offY, width, height, -uid);
			int value = Cache.spriteCache.get(hash);
			if (value != 0) {
				textureID = value;
				// update the texture
				VBOManager.loadTextureRGBA(true, textureID, pixels, width, height, alpha, false, -1);
				Cache.spriteCache.put(hash, textureID);
			} else {
				// System.out.println("miss!");
				textureID = VBOManager.loadTextureRGBA(pixels, width, height, alpha, false, -1);
				Cache.spriteCache.put(hash, textureID);
			}
		} else {
			// images are textures --> load image texture
			long hash = Cache.imageHash(offX, offY, width, height, uid);
			int value = Cache.spriteCache.get(hash);
			if (value != 0) {
				textureID = value;
			} else {
				// System.out.println("miss!");
				textureID = VBOManager.loadTextureRGBA(pixels, width, height, alpha, false, -1);
				Cache.spriteCache.put(hash, textureID);
			}
		}
		
		//glTranslatef(-(offX + width / 2), -(offY2 + height / 2), 0);
		
		// bind texture --> we're now using it
		GraphicsDisplay.setTexture(textureID);

		if (rotation != 0.0f) {
			glLoadIdentity();

			float dx = offX * SF + (width * SF) / 2;
			float dy = offY * SF + (height * SF) / 2;
			glTranslatef(dx, dy, 0);
			glRotatef((float) (-rotation * 0.005493164F), 0.0F, 0.0F, 1.0F);
			glTranslatef(-dx, -dy, 0);
		}

		// begin drawing texture quad
		glBegin(GL_QUADS);
		
		if (requiresDepth) {
			float depth = VBORenderer.atlasDepth += VBORenderer.ATLAS_HEIGHT_MODIFIER;
			
			glTexCoord2f(0, 0);
			glVertex3f(offX * SF, offY * SF, depth);
			
			glTexCoord2f(1, 0);
			glVertex3f(width * SF + offX * SF, offY * SF, depth);
			
			glTexCoord2f(1, 1);
			glVertex3f(width * SF + offX * SF, height * SF + offY * SF, depth);
			
			glTexCoord2f(0, 1);
			glVertex3f(offX * SF, height * SF + offY * SF, depth);
		} else {
			glTexCoord2f(0, 0);
			glVertex2f(offX * SF, offY * SF);
			
			glTexCoord2f(1, 0);
			glVertex2f(width * SF + offX * SF, offY * SF);
			
			glTexCoord2f(1, 1);
			glVertex2f(width * SF + offX * SF, height * SF + offY * SF);
			
			glTexCoord2f(0, 1);
			glVertex2f(offX * SF, height * SF + offY * SF);
		}
		
		// finished drawing quad
		glEnd();

		if (rotation != 0.0f) {
			glLoadIdentity();
		}

		// scene was rotated --> reset that
		//if (rotation > 0f)
		//	glLoadIdentity();
		
		// immediately delete texture
		if (uid == -1) {
			GLBufferManager.deleteTextureBuffer(GLBufferManager.TEXTURE_SPRITE, textureID);
		}
		
		if (!Client.disableOpenGlErrors) {
			exitOnGLError("error in addSpritePixels(" + uid + ")");
		}
	}
	
	public boolean mayRender(Client c) {
		// anti aliasing setting was changed
		// pause for 1 render cycle
		if ((antiAliasingSupported && lastAntiAliasing != antiAliasing)) {
			return false;
		}
		
		return true;
	}
	
	private boolean renderingWorld = false;
	
	public void beginRenderingWorld(Client c) {
		//Model.startCycle();//TODO
		renderingWorld = true;
	}
	
	public void renderScene() {
		if (vboRendering)
			VBORenderer.renderScene(false);
		else
			StreamRenderer.renderScene(false);
		useDepthBuffer = true;
	}
	
	public void endRenderingWorld(Client c, double advancement) {
		//Model.awaitTermination();//TODO wtf
		
		// done drawing non-textured triangles
		//glEnd();
		
		// render textured triangles/quads
		if (vboRendering)
			VBORenderer.renderScene(true);
		else
			StreamRenderer.renderScene(true);
		useDepthBuffer = true;
		
		resetViewport();
		
		
		// we're not rendering the world anymore
		// only images/text at this point (textures)
		renderingWorld = false;
		
		//if (!vboRendering)
		setBackground(advancement);
		
		// setting new glOrtho here, because everything that
		// follow from this point are interfaces or text
		// basically 2D textures, which are overlay and don't
		// require a depth buffer, nor fog!
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0f, width * SF, 0f, height * SF, fogEnd, -fogEnd); // znear,
		// zfar
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		if (showVersion) {
			showVersion = false;
			showVersionInfo();
		}
		
		Cache.resetDynamicAtlasSpriteIds();
		
		if (!Client.disableOpenGlErrors) {
			exitOnGLError("error in endRenderingWorld()");
		}
	}
	
	public void addModel(
			float[][] uv,
			float[] vertexPerspectiveX, float[] vertexPerspectiveY, float[] vertexPerspectiveZ,
			int[] triangleVertexPoint1, int[] triangleVertexPoint2, int[] triangleVertexPoint3,
			int[] colorValues, int[] textureBackgrounds,
			int vertexCount, int triangleCount,
			int[] anIntArray1671, int[][] anIntArrayArray1672,
			int[] colorValues1, int[] colorValues2, int[] colorValues3,
			int[] triangleAlphaValues, int[] drawTypes, int[] texturePointers, byte[] textureTypes,
			int[] textureP, int[] textureM, int[] textureN, int[] texA, int[] texB, int[] texC, int[] particleDirectionX, int[] particleDirectionY,
			int[] particleDirectionZ, int[] particleLifespanX, int[] particleLifespanY, int[] particleLifespanZ, int[] texturePrimaryColor,
			int[] textureSecondaryColor, int mousePosX, int mousePosY, int id1, long uid, boolean flag, boolean flag1, int pool) {
		
		if (disableAll)
			return;
		
		boolean newMode = true;
		for (int vertex = vertexCount - 1; vertex >= 0; vertex--) {
			// if (vertex % 2 == 0)
			// continue;
			
			if (newMode) {
				int l1 = anIntArray1671[vertex];
				if (l1 > 0) {
					ModelRendering.addModelTriangle_MODERN(uv, vertexPerspectiveX, vertexPerspectiveY, vertexPerspectiveZ, triangleVertexPoint1, triangleVertexPoint2, triangleVertexPoint3, colorValues, textureBackgrounds,
							vertexCount, anIntArray1671, anIntArrayArray1672, colorValues1, colorValues2, colorValues3, triangleAlphaValues, drawTypes,
							texturePointers, textureTypes, textureP, textureM, textureN, texA, texB, texC, particleDirectionX, particleDirectionY,
							particleDirectionZ, particleLifespanX, particleLifespanY, particleLifespanZ, texturePrimaryColor, textureSecondaryColor, vertex,
							l1, height, SF, renderingWorld, modelsTextured, pool);
				}
			} else {
				int l1 = anIntArray1671[vertex];
				if (l1 > 0) {
					ModelRendering.addModelTriangle_CLASSIC(vertexPerspectiveX, vertexPerspectiveY, vertexPerspectiveZ, triangleVertexPoint1, triangleVertexPoint2, triangleVertexPoint3, colorValues, textureBackgrounds,
							anIntArray1671, anIntArrayArray1672, colorValues1, colorValues2, colorValues3, triangleAlphaValues, drawTypes, texturePointers, textureTypes,
							textureP, textureM, textureN, texA, texB, texC, l1, height, SF, pool);
				}
			}
		}
		
		/*if (pool == Model.DEFAULT_POOL)
		{
			if (!renderingWorld)
			{
				glEnd();
	
				// useDepthBuffer = false;
				glDisable(GL_DEPTH_TEST);
				glEnable(GL_TEXTURE_2D);
			}
	
			if (renderingWorld)
				this.exitOnGLError("Error in addModel(end2)");
		}*/
	}

	// called every loop
	public void drawStart() {
		if (!initialized) {
			return;
		}

		resetPerspective();
	}
	
	public boolean displayVisible() {
		return Display.isVisible();
	}
	
	public void drawEnd(Client c) {
		Cache.cleanupTextures();
		
		// render textured triangles/quads
		if (vboRendering)
			VBORenderer.renderScene(false);
		else
			StreamRenderer.renderScene(false);
		useDepthBuffer = true;

		/*float lightAmbient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float lightDiffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
		float lightPosition[] = { 0.0f, 1.0f, 1.0f, 0.0f };

		FloatBuffer ambient = BufferUtils.createFloatBuffer(4);
		ambient.put(lightAmbient);
		ambient.flip();
		GL11.glLight(GL11.GL_LIGHT1, GL11.GL_AMBIENT, ambient);

		FloatBuffer diffuse = BufferUtils.createFloatBuffer(4);
		diffuse.put(lightDiffuse);
		diffuse.flip();
		GL11.glLight(GL11.GL_LIGHT1, GL11.GL_DIFFUSE, diffuse);

		FloatBuffer position = BufferUtils.createFloatBuffer(4);
		position.put(lightPosition);
		position.flip();
		GL11.glLight(GL11.GL_LIGHT1, GL11.GL_POSITION, position);
		
		GL11.glEnable(GL11.GL_LIGHT1);
		GL11.glEnable(GL11.GL_LIGHTING);*/
		
		
		// else
		// maxFPS = 30;
		
		// window is minimized, there is no way
		// that the window is seen by the user
		// so we're completely killing the fps
		// rate to release resources for the computer
		if (!Display.isVisible()) {
			disableAll = true;
		} else {
			disableAll = false;
		}

		// debug mode, fps divided by 2 here
		// if (debug)
		// maxFPS /= 2;
		Display.update();
		
		if ((antiAliasingSupported && lastAntiAliasing != antiAliasing)) {
			// TODO: this crashes??
			lastAntiAliasing = antiAliasing;
			//Cache.hardClear();
			//Cache.releaseAtlases();
			
			//System.out.println("destroy: "+enabled+"; "+initialized);
			//Display.destroy();
			//initialize(c);
			//System.out.println("init: "+enabled+"; "+initialized);
			
			//Cache.clearVBOModels = true;
			//c.loadMap(100);
		}
		
		
		// Rasterizer.DEFAULT_GROUND_TEXTURE = 598; // 598
	}
	
	public final void handleEvents(RSApplet c) {
		// handle key input
		while (Keyboard.isCreated() && Keyboard.next()) {
			boolean pressed = Keyboard.getEventKeyState();
			int key = Keyboard.getEventKey();
			char ch = Keyboard.getEventCharacter();

			switch (key) {
				case Keyboard.KEY_RETURN:
					key = KeyEvent.VK_ENTER;
					break;
				case Keyboard.KEY_TAB:
					key = KeyEvent.VK_TAB;
					break;
				case Keyboard.KEY_BACK:
					key = KeyEvent.VK_BACK_SPACE;
					break;
				case Keyboard.KEY_RCONTROL:
				case Keyboard.KEY_LCONTROL:
					key = KeyEvent.VK_CONTROL;
					break;
				case Keyboard.KEY_DOWN:
					key = KeyEvent.VK_DOWN;
					ch = 1;
					break;
				case Keyboard.KEY_UP:
					key = KeyEvent.VK_UP;
					ch = 1;
					break;
				case Keyboard.KEY_LEFT:
					key = KeyEvent.VK_LEFT;
					ch = 1;
					break;
				case Keyboard.KEY_RIGHT:
					key = KeyEvent.VK_RIGHT;
					ch = 1;
					break;
				case Keyboard.KEY_F1:
					key = KeyEvent.VK_F1;
					break;
				case Keyboard.KEY_F2:
					key = KeyEvent.VK_F2;
					break;
				case Keyboard.KEY_F3:
					key = KeyEvent.VK_F3;
					break;
				case Keyboard.KEY_F4:
					key = KeyEvent.VK_F4;
					break;
				case Keyboard.KEY_F5:
					key = KeyEvent.VK_F5;
					break;
				case Keyboard.KEY_ESCAPE:
					key = KeyEvent.VK_ESCAPE;
					break;
				case Keyboard.KEY_PRIOR:
					key = KeyEvent.VK_PAGE_UP;
					break;
				case Keyboard.KEY_NEXT:
					key = KeyEvent.VK_PAGE_DOWN;
					break;
				case Keyboard.KEY_LSHIFT:
				case Keyboard.KEY_RSHIFT:
					key = KeyEvent.VK_SHIFT;
					break;
				case Keyboard.KEY_NUMPADENTER:
					key = KeyEvent.VK_ENTER;
					break;
				default:
					if (Character.isDefined(ch))
						key = 0;
					break;
			}
			
			//System.out.println("char: "+ch+", key: "+key+", "+pressed+" ;;; "+KeyEvent.VK_PAGE_DOWN);

			if (pressed)
				c.keyPressed(key, ch);
			else
				c.keyReleased(key, ch);
		}
		
		while (Mouse.isCreated() && Mouse.next()) {
			int wheel = Mouse.getDWheel();
			if (wheel != 0) {
				int notches;
				if (wheel > 0) {
					notches = -1;
				} else {
					notches = 1;
				}
				
				c.mouseWheelMoved(notches);
				continue;
			}

			int x = Mouse.getEventX();
			int y = height - Mouse.getEventY();
			int button = Mouse.getEventButton();

			if (button == -1) {
				c.mouseMoved(x, y, -Mouse.getEventDX(), -Mouse.getEventDY());
				continue;
			}

			boolean pressed = Mouse.getEventButtonState();
			if (pressed) {
				c.mousePressed(x, y, button + 1);
			} else {
				c.mouseReleased();
			}
		}
	}
	
	private static float fogStart = 1f;
	private static float fogEnd = 2f;
	
	public static void ortho() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		// width from 0 to width
		// height from 0 to height
		// z-buffer from -3f to 3f
		glOrtho(0f, Display.getWidth() * SF, 0f, Display.getHeight() * SF, fogEnd, -fogEnd); // znear, zfar
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
	
	private void resetViewport() {
		glViewport(0, 0, width, height);
	}
	
	public void glOrthoReset() {
		width = Display.getWidth();
		height = Display.getHeight();
		
		// set viewport
		resetViewport();
		
		// load projection mode
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		// width from 0 to width
		// height from 0 to height
		// z-buffer from -3f to 3f
		glOrtho(0f, width * SF, 0f, height * SF, fogEnd, -fogEnd); // znear, zfar
		
		// load model view
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
	
	private void resetPerspective() {
		if (!vboRendering) {
			setBackground(0);
		}
		
		// glDisable(GL_SCISSOR_TEST);
		/*if (Client.regionLoadingScreenRequired) {
			//if (!scissorTestEnabled) {
				glEnable(GL_SCISSOR_TEST);
				scissorTestEnabled = true;
				glScissor(50, getHeight() - 50 - 14, Client.loadingBoxWidth + 2, 30);
			//}
		} else {
			//if (scissorTestEnabled) {
				glDisable(GL_SCISSOR_TEST);
				scissorTestEnabled = false;
			//}
		}*/
		
		//if (!Client.regionLoadingScreenRequired)
		if (Client.loadingStage == 2 || !Client.doneLoading) {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		}
		
		//if (!vboRendering)
		
		// force small screen if client isn't
		/*if (!Client.isResizeable() && Display.getWidth() != 765 && Display.getHeight() != 503) {
			resizeRequired = true;
			try {
				Display.setDisplayMode(new DisplayMode(765, 503));
			} catch (LWJGLException e) {
				e.printStackTrace();
			}
			return;
		} else if (Client.isResizeable() && (Display.getWidth() < 800 || Display.getHeight() < 550)) {
			resizeRequired = true;

			try {
				Display.setDisplayMode(new DisplayMode(800, 550));
			} catch (LWJGLException e) {
				e.printStackTrace();
			}
			return;
		}*/
		
		// was our window resized?
		if (Display.wasResized() || resizeRequired) {
			resizeRequired = false;
			
			glOrthoReset();
			
			/*c.needResize = true;
			Client.extraWidth = width - 765;//TODO?
			Client.extraHeight = height - 503;*/
			Client.clientWidth = width;
			Client.clientHeight = height;
			if (Client.doneLoading) {
				Client.updateGame.set(true);
			}
		}
		
		if (GraphicsDisplay.enabled && !GraphicsDisplay.vboRendering) {
			float camX = 0;// -c.xCameraCurve*SF;
			float camY = 0;// -c.yCameraCurve*SF;
			float camZ = -Client.cameraZoom * SF * 0.8f; // to keep background at same
			// position
			// camX = c.xCameraPos;
			// camY = c.yCameraPos;
			camZ = -Client.cameraZoom * SF * 0.9f; // to keep background at same position
			glTranslatef(camX, camY, camZ);
		}
		
		// glLoadIdentity();
		// angle, x, y, z
		// glRotatef(10, 0, 1, 0);
		
		// float y = WorldController.camCurveY/360f;
		// glTranslatef(0, 0.1f, 0);
		/*
		 * glRotatef(rotX, 1.0f, 0.0f, 0.0f); glRotatef(rotY, 0.0f, 1.0f, 0.0f);
		 * glRotatef(rotZ, 0.0f, 0.0f, 1.0f);
		 */
		
		groundTextured = Client.enableHDTextures;
		modelsTextured = Client.enableHDTextures;
		// Cache.clearAll();
		
		// Cache.useVBOperTexture = false;
	}

	public boolean exitOnGLError(String errorMessage) {
		final int errorValue = glGetError();
		final boolean error = errorValue != GL_NO_ERROR;

		while (executionStack.size() > 1000)
			executionStack.removeFirst();
		
		executionStack.addLast(errorMessage);
		
		//if (!debug)
		//	return;
		
		if (error) {
			new Throwable().printStackTrace();
			boolean exit = true;
			
			// we don't have to close the application at these errors
			// a warning is enough!
			if (errorValue == GL_INVALID_VALUE || errorValue == GL_INVALID_ENUM || errorValue == GL_INVALID_OPERATION)
				exit = false;
			
			System.err.println("ERROR - " + errorMessage + ": " + errorValue);
			showError(errorMessage + ": " + errorValue, exit);
			
			return true;
		}
		
		return false;
	}
	
	public void exitError(String errorMessage) {
		showError(errorMessage, true);
		System.exit(-1);
	}
	
	public static int getWidth() {
		return Display.getWidth();
	}
	
	public static int getHeight() {
		return Display.getHeight();
	}
	
}
