package com.soulplayps.client.opengl.gl15.model.simple;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.soulplayps.client.util.BitwiseHelper;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongList;
import org.lwjgl.opengl.GL15;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.Ground;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.object.WorldController;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.AssetLoader;
import com.soulplayps.client.opengl.gl15.model.ModelConfig;
import com.soulplayps.client.opengl.gl15.model.ModelVBO;
import com.soulplayps.client.opengl.gl15.particle.Particle;
import com.soulplayps.client.opengl.model.DrawableModel;
import com.soulplayps.client.opengl.util.Cache;
import com.soulplayps.client.opengl.util.GLBufferManager;
import com.soulplayps.client.unknown.Class40;
import com.soulplayps.client.unknown.Class43;

public class ModelLoader extends AssetLoader
{

	private static int floorHash(int x, int y, int height)
	{
		return (height << 24) + (x << 16) + (y << 8);
	}
	
	private long hashPos(int x, int y, int h)
	{
		return (h << 50L) + (y << 16L) + x;
	}
	
	private boolean smoothenTexture(int texture)
	{
		// water
		if (texture == Rasterizer.DEFAULT_GROUND_TEXTURE || texture == -1/* || texture != 669*/)
			return true;
		
		return false;
	}
	
	private void putColor(int x, int y, int h, int color, int texture)
	{
		// no ground smothing
		if (!Rasterizer.smoothGround)
			return;
		
		// smoothen this texture?
		else if (!smoothenTexture(texture))
			return;
		
		// awkward white color that bugs spots at the GE
		else if (color == 12448719 || color == 11660227 || color == 10806456 || color == 10017965)
			return;
		
		final long hash = hashPos(x, y, h);
		
		List<Integer> cols = colorIntersections.get(hash);
		if (cols == null)
		{
			cols = new LinkedList<Integer>();
			colorIntersections.put(hash, cols);
		}
		
		//for (int i = 0; i < weight; i++)
		cols.add(color);
	}
	
	public int getSmoothedColor(float x, float y, int h, int color)
	{
		final long hash = hashPos((int)x, (int)y, h);
		List<Integer> cols = colorIntersections.get(hash);
		if (cols == null)
		{
			//System.out.println("no color found: "+x+"/"+y+"/"+h+": "+color);
			return color;
		}
		else
		{
			boolean print = false;//Math.random() < 0.00001;
			float red = 0;
			float green = 0;
			float blue = 0;
			float len = cols.size();
			
			for (Integer col : cols)
			{
				red += ((col >> 16) & 0xFF);
				green += ((col >> 8) & 0xFF);
				blue += ((col) & 0xFF);
				
				if (print)
					System.out.println("col: "+col+": "+red+"/"+blue+"/"+green);
			}
			
			red = red / len;
			green = green / len;
			blue = blue / len;
			
			int newColor = (Math.round(red) << 16) + (Math.round(green) << 8) + Math.round(blue);
			
			if (print)
				System.out.println(color+" --> "+newColor+"\n");
			
			return newColor;
		}
	}
	
	public void clearColors()
	{
		for (Entry<Long, List<Integer>> entry : colorIntersections.entrySet())
			entry.getValue().clear();
		
		colorIntersections.clear();
		colorsLoaded = false;
		
	}
	
	public boolean colorsLoaded = false;
	private static Map<Long, List<Integer>> colorIntersections;
	
	public ModelVBO loadFloor(int height, WorldController wc, int startX, int startY, int tileSize, boolean colorsOnly)
	{
		long uid = floorHash(height, startX, startY);
		ModelVBO vbo = Cache.getFloorVBOs().get(uid);
		
		if (vbo != null) {
			return vbo;
		}

		if (wc.anIntArrayArrayArray440 == null)
		{
			System.err.println("[ModelLoader]: heights null");
			return null;
		}
		
		textureLoaded.clear();
		clearBuffers();

		colorsLoaded = true;

		//if (VBOManager.atlasModels.floorBuffersCreated++ >= VBOManager.atlasModels.maxFloorBuffersPerFrame)
		//	return null;
		
		final Ground groundCoordinates[][] = wc.groundArray[height];
		//System.out.println("calculating floor: "+uid+", "+vbo+", "+Cache.floorVBOs.size());
		
		// iterate once to count triangles to prepare buffers
		
		int endX = startX+tileSize;
		if (endX > 104)
			endX = 104;
		
		int endY = startY+tileSize;
		if (endY > 104)
			endY = 104;
		
		int triangleCount = 0;
		for (int x = startX; x < endX; x++)
			for (int y = startY; y < endY; y++)
			{
				for (int i = 0; i < 2; i++)
				{
					Ground g = groundCoordinates[x][y];
					if (g == null)
						break;
					else if (i == 1)
						g = g.aClass30_Sub3_1329;
					
					if (g == null)
						break;
					
					if (g.aClass43_1311 != null)
					{
						Class43 tile = g.aClass43_1311;
	
						if (tile.colorTL > 0xFFFF || tile.colorBL > 0xFFFF || tile.colorTR > 0xFFFF || tile.colorBR > 0xFFFF)
							continue;
						else if (tile.colorTL < 0 || tile.colorBL < 0 || tile.colorTR < 0 || tile.colorBR < 0)
							continue;
						
						triangleCount += 2;

						int gx = x << 7;
						int gy = y << 7;
						int size = 128;
						
						int x1 = gx;
						int x2 = gx + size;
						int y1 = gy;
						int y2 = gy + size;
						
						putColor(x1, y1, height, Rasterizer.anIntArray1482[tile.colorTR & 0xFFFF], tile.groundTexture); // top right
						putColor(x1, y2, height, Rasterizer.anIntArray1482[tile.colorTL & 0xFFFF], tile.groundTexture); // top left
						putColor(x2, y1, height, Rasterizer.anIntArray1482[tile.colorBR & 0xFFFF], tile.groundTexture); // bottom right
						putColor(x2, y2, height, Rasterizer.anIntArray1482[tile.colorBL & 0xFFFF], tile.groundTexture); // bottom left
						
					}
					
					if (g.aClass40_1312 != null)
					{
						Class40 tile = g.aClass40_1312;
						for (int v = 0; v < tile.anIntArray679.length; v++)
						{
							if (tile.color1[v] > 0xFFFF || tile.color2[v] > 0xFFFF || tile.color3[v] > 0xFFFF)
								triangleCount--;		
							else if (tile.color1[v] < 0 || tile.color2[v] < 0 || tile.color3[v] < 0)
								triangleCount--;

							int newTexture = tile.groundTextures == null ? -1 : tile.groundTextures[v];
							int rgb0 = scaleRGB(Rasterizer.anIntArray1482[tile.color1[v] & 0xFFFF], false, true);
							int rgb1 = scaleRGB(Rasterizer.anIntArray1482[tile.color2[v] & 0xFFFF], false, true);
							int rgb2 = scaleRGB(Rasterizer.anIntArray1482[tile.color3[v] & 0xFFFF], false, true);

							int pt0 = tile.anIntArray679[v];
							int pt1 = tile.anIntArray680[v];
							int pt2 = tile.anIntArray681[v];
							
							int px0 = tile.anIntArray673[pt0];
							int py0 = tile.anIntArray674[pt0];
							int pz0 = tile.anIntArray675[pt0];
							
							int px1 = tile.anIntArray673[pt1];
							int py1 = tile.anIntArray674[pt1];
							int pz1 = tile.anIntArray675[pt1];
							
							int py2 = tile.anIntArray674[pt2];
							int px2 = tile.anIntArray673[pt2];
							int pz2 = tile.anIntArray675[pt2];
							

							if (tile.color != 0)
							{
								putColor(px0, pz0, height, rgb0, newTexture);
								putColor(px1, pz1, height, rgb1, newTexture);
								putColor(px2, pz2, height, rgb2, newTexture);
							}
						}
						
						triangleCount += tile.anIntArray673.length;
					}
				}
			}
		
		
		if (colorsOnly)
			return null;
		
		//System.out.println("triangles: "+triangleCount+", "+startX+"/"+startY+" --> "+endX+"/"+endY);


		//if (triangleCount > 0)
		//System.out.println("loading floor1: "+startX+"/"+startY+": "+triangleCount);
		
		if (triangleCount <= 0)
			return null;

		//System.out.println("loading2: "+startX+"/"+startY+": "+triangleCount);
		
		//final FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(totalSizeVertices);
		//final ByteBuffer colorBuffer = BufferUtils.createByteBuffer(totalSizeColors);

		// iterate another time to construct the VBO
		
		boolean hasUV = false;
		
		// still need new textures to load?
		boolean loading = true;

		int texture = -1;
		int trianglesCounted = 0;
		int iterations = 0;
		
		// a maximum of 10 textures per model
		LongList textureMappings = new LongArrayList();
		
		while (loading)
		{
			iterations++;
			
			// all textures of the same kind are finished
			// we now put their count into our texture map
			// that way we know how many triangles in the VBO
			// need to be drawn with this texture id
			if (iterations != 1)
			{
				textureMappings.add(BitwiseHelper.ints2Long(texture, trianglesCounted));
				textureLoaded.add(texture);
				trianglesCounted = 0;
			}
			
			loading = false;
			boolean countingTexture = false;
			
			for (int x = startX; x < endX; x++)
			{
				for (int y = startY; y < endY; y++)
				{
					boolean groundLoop = true;
					for (int i = 0; i < 2 && groundLoop; i++)
					{
						Ground g = groundCoordinates[x][y];
						if (g == null)
							break;
						else if (i == 1)
							g = g.aClass30_Sub3_1329;
						
						if (g == null)
							break;
						
						// to allow transparancy on the textures!
						int alpha = 255;
						Class43 sTile = g.aClass43_1311;
						Class40 cTile2 = null;
						
						boolean sHeightPlus = false;
						boolean cHeightPlus = false;
						
						// TODO: does fix the GE floor data, but bugs other floors
						// probably need some smarter technique here.
						/*if (height == 0) {
							if ((sTile == null && g.cutTile == null) && wc.groundArray[height+1][x][y] != null) {
								sTile = wc.groundArray[height+1][x][y].squareTile;
								sHeightPlus = true;
							} if (wc.groundArray[height+1][x][y] != null) {
								cTile2 = wc.groundArray[height+1][x][y].cutTile;
								cHeightPlus = true;
							}
						} else if (height == 1) {
							if (wc.groundArray[height-1][x][y] == null)
								continue;
						}*/
						
						if (sTile != null) {
							if (sTile.colorTL > 0xFFFF || sTile.colorBL > 0xFFFF || sTile.colorTR > 0xFFFF || sTile.colorBR > 0xFFFF)
								continue;
							else if (sTile.colorTL < 0 || sTile.colorBL < 0 || sTile.colorTR < 0 || sTile.colorBR < 0)
								continue;
							
							int newTexture = sTile.groundTexture;
							if (newTexture == -1 && Client.enableHDTextures)
								newTexture = Rasterizer.DEFAULT_GROUND_TEXTURE;
							
							// load all triangles with the same texture id, so that we
							// have them after each other in our VBO. that means, that
							// we can draw all 'same' textures in 1 call per VBO
							if (countingTexture && newTexture != texture)
							{
								loading = true;
								continue;
							}
							
							// texture was already loaded
							if (textureLoaded.indexOf(newTexture) != -1)
							{
								continue;
							}
							
							int gx = x << 7;
							int gy = y << 7;
							int size = 128;
							
							float px0 = gx;
							float px1 = gx + size;
							float pz0 = gy;
							float pz1 = gy + size;
							
							final int rgb0, rgb1, rgb2, rgb3;
							if (Rasterizer.smoothGround && smoothenTexture(sTile.groundTexture))
							{
								rgb1 = getSmoothedColor(px0, pz0, height, Rasterizer.anIntArray1482[sTile.colorTL & 0xFFFF]); // bottom left
								rgb0 = getSmoothedColor(px0, pz1, height, Rasterizer.anIntArray1482[sTile.colorBL & 0xFFFF]); // top left
								rgb3 = getSmoothedColor(px1, pz0, height, Rasterizer.anIntArray1482[sTile.colorTR & 0xFFFF]); // bottom right
								rgb2 = getSmoothedColor(px1, pz1, height, Rasterizer.anIntArray1482[sTile.colorBR & 0xFFFF]); // top right
							}
							else
							{	
								rgb0 = scaleRGB(Rasterizer.anIntArray1482[sTile.colorTL & 0xFFFF], false, true); // top left
								rgb1 = scaleRGB(Rasterizer.anIntArray1482[sTile.colorBL & 0xFFFF], false, true); // bottom left
								rgb2 = scaleRGB(Rasterizer.anIntArray1482[sTile.colorTR & 0xFFFF], false, true); // top right
								rgb3 = scaleRGB(Rasterizer.anIntArray1482[sTile.colorBR & 0xFFFF], false, true); // bottom right
							}
							
							int h = height;
							if (i == 0 && g.aClass30_Sub3_1329 != null && h < 3)
								h++;
							
							// ground has its own height value
							h = g.groundHeight;

							// use height from upper floor
							if (sHeightPlus)
								h++;
							
							float py0 = wc.anIntArrayArrayArray440[h][x][y+1]; // top left
							float py1 = wc.anIntArrayArrayArray440[h][x][y]; // bottom left
							float py2 = wc.anIntArrayArrayArray440[h][x+1][y+1]; // top right
							float py3 = wc.anIntArrayArrayArray440[h][x+1][y]; // bottom right 

							texture = newTexture;
							countingTexture = true;
							trianglesCounted += 2;
							
							int r0 = ((rgb0 >> 16) & 0xFF);
							int g0 = ((rgb0 >> 8) & 0xFF);
							int b0 = ((rgb0 >> 0) & 0xFF);
							int a0 = (alpha);
		
							int r1 = ((rgb1 >> 16) & 0xFF);
							int g1 = ((rgb1 >> 8) & 0xFF);
							int b1 = ((rgb1 >> 0) & 0xFF);
							int a1 = (alpha);
		
							int r2 = ((rgb2 >> 16) & 0xFF);
							int g2 = ((rgb2 >> 8) & 0xFF);
							int b2 = ((rgb2 >> 0) & 0xFF);
							int a2 = (alpha);
		
							int r3 = ((rgb3 >> 16) & 0xFF);
							int g3 = ((rgb3 >> 8) & 0xFF);
							int b3 = ((rgb3 >> 0) & 0xFF);
							int a3 = (alpha);
							
							// vertex 1
							vertexBuffer.put(px1);
							vertexBuffer.put(py2);
							vertexBuffer.put(pz1);
							vertexBuffer.put(px0);
							vertexBuffer.put(py0);
							vertexBuffer.put(pz1);
							vertexBuffer.put(px0);
							vertexBuffer.put(py1);
							vertexBuffer.put(pz0);
							
							// vertex 2
							vertexBuffer.put(px1);
							vertexBuffer.put(py2);
							vertexBuffer.put(pz1);
							vertexBuffer.put(px0);
							vertexBuffer.put(py1);
							vertexBuffer.put(pz0);
							vertexBuffer.put(px1);
							vertexBuffer.put(py3);
							vertexBuffer.put(pz0);
							
							// color1
							colorBuffer.put((byte) r2);
							colorBuffer.put((byte) g2);
							colorBuffer.put((byte) b2);
							colorBuffer.put((byte) a2);
							colorBuffer.put((byte) r0);
							colorBuffer.put((byte) g0);
							colorBuffer.put((byte) b0);
							colorBuffer.put((byte) a0);
							colorBuffer.put((byte) r1);
							colorBuffer.put((byte) g1);
							colorBuffer.put((byte) b1);
							colorBuffer.put((byte) a1);
							
							// color 2
							colorBuffer.put((byte) r2);
							colorBuffer.put((byte) g2);
							colorBuffer.put((byte) b2);
							colorBuffer.put((byte) a2);
							colorBuffer.put((byte) r1);
							colorBuffer.put((byte) g1);
							colorBuffer.put((byte) b1);
							colorBuffer.put((byte) a1);
							colorBuffer.put((byte) r3);
							colorBuffer.put((byte) g3);
							colorBuffer.put((byte) b3);
							colorBuffer.put((byte) a3);
							
							uvBuffer.put(1);
							uvBuffer.put(0);
							uvBuffer.put(0);
							uvBuffer.put(0);
							uvBuffer.put(0);
							uvBuffer.put(1);
							
							uvBuffer.put(1);
							uvBuffer.put(0);
							uvBuffer.put(0);
							uvBuffer.put(1);
							uvBuffer.put(1);
							uvBuffer.put(1);
							
							hasUV = true;
							
							//System.out.println("st v: "+px0+", "+py0+", "+pz0);
						}

						final Class40[] cTiles = new Class40[]{g.aClass40_1312, cTile2};
						for (Class40 cTile : cTiles) {
							if (cTile == null)
								continue;
							
							for (int v = 0; v < cTile.anIntArray679.length; v++) {
								if (cTile.color1[v] > 0xFFFF || cTile.color2[v] > 0xFFFF || cTile.color3[v] > 0xFFFF)
									continue;		
								else if (cTile.color1[v] < 0 || cTile.color2[v] < 0 || cTile.color3[v] < 0)
									continue;
	
								
								int newTexture = cTile.groundTextures == null ? -1 : cTile.groundTextures[v];
								if (newTexture == -1 && Client.enableHDTextures)
									newTexture = Rasterizer.DEFAULT_GROUND_TEXTURE;
								
								// load all triangles with the same texture id, so that we
								// have them after each other in our VBO. that means, that
								// we can draw all 'same' textures in 1 call per VBO
								if (countingTexture && newTexture != texture)
								{
									loading = true;
									continue;
								}
								
								// texture was already loaded
								if (textureLoaded.indexOf(newTexture) != -1)
								{
									continue;
								}
								
								int pt0 = cTile.anIntArray679[v];
								int pt1 = cTile.anIntArray680[v];
								int pt2 = cTile.anIntArray681[v];
								
								float px0 = cTile.anIntArray673[pt0];
								float py0 = cTile.anIntArray674[pt0];
								float pz0 = cTile.anIntArray675[pt0];
								
								float px1 = cTile.anIntArray673[pt1];
								float py1 = cTile.anIntArray674[pt1];
								float pz1 = cTile.anIntArray675[pt1];
								
								float py2 = cTile.anIntArray674[pt2];
								float px2 = cTile.anIntArray673[pt2];
								float pz2 = cTile.anIntArray675[pt2];

								final int rgb0, rgb1, rgb2;
								if (Rasterizer.smoothGround && smoothenTexture(cTile.groundTextures == null ? -1 : cTile.groundTextures[v]))
								{
									rgb0 = getSmoothedColor(px0, pz0, height, Rasterizer.anIntArray1482[cTile.color1[v] & 0xFFFF]);
									rgb1 = getSmoothedColor(px1, pz1, height, Rasterizer.anIntArray1482[cTile.color2[v] & 0xFFFF]);
									rgb2 = getSmoothedColor(px2, pz2, height, Rasterizer.anIntArray1482[cTile.color3[v] & 0xFFFF]);
								}
								else
								{
									rgb0 = scaleRGB(Rasterizer.anIntArray1482[cTile.color1[v] & 0xFFFF], false, true);
									rgb1 = scaleRGB(Rasterizer.anIntArray1482[cTile.color2[v] & 0xFFFF], false, true);
									rgb2 = scaleRGB(Rasterizer.anIntArray1482[cTile.color3[v] & 0xFFFF], false, true);
								}
								

								texture = newTexture;
								countingTexture = true;
								trianglesCounted++;
								
								
								int r0 = ((rgb0 >> 16) & 0xFF);
								int g0 = ((rgb0 >> 8) & 0xFF);
								int b0 = ((rgb0 >> 0) & 0xFF);
								int a0 = (alpha);
		
								int r1 = ((rgb1 >> 16) & 0xFF);
								int g1 = ((rgb1 >> 8) & 0xFF);
								int b1 = ((rgb1 >> 0) & 0xFF);
								int a1 = (alpha);
		
								int r2 = ((rgb2 >> 16) & 0xFF);
								int g2 = ((rgb2 >> 8) & 0xFF);
								int b2 = ((rgb2 >> 0) & 0xFF);
								int a2 = (alpha);
		
								// vertex
								vertexBuffer.put(px0);
								vertexBuffer.put(py0);
								vertexBuffer.put(pz0);
								vertexBuffer.put(px1);
								vertexBuffer.put(py1);
								vertexBuffer.put(pz1);
								vertexBuffer.put(px2);
								vertexBuffer.put(py2);
								vertexBuffer.put(pz2);
								
								// color
								colorBuffer.put((byte) r0);
								colorBuffer.put((byte) g0);
								colorBuffer.put((byte) b0);
								colorBuffer.put((byte) a0);
								colorBuffer.put((byte) r1);
								colorBuffer.put((byte) g1);
								colorBuffer.put((byte) b1);
								colorBuffer.put((byte) a1);
								colorBuffer.put((byte) r2);
								colorBuffer.put((byte) g2);
								colorBuffer.put((byte) b2);
								colorBuffer.put((byte) a2);
								
								final int pI = 0;
								final int mI = 1;
								final int nI = 3;
								
								// y = 0: proper normal calculation for floor
	
								final float px = (float) cTile.anIntArray673[pI];
								final float py = (float) 0;
								final float pz = (float) cTile.anIntArray675[pI];
	
								final float mx = (float) cTile.anIntArray673[mI] - px;
								final float my = (float) 0 - py;
								final float mz = (float) cTile.anIntArray675[mI] - pz;
								final float nx = (float) cTile.anIntArray673[nI] - px;
								final float ny = (float) 0 - py;
								final float nz = (float) cTile.anIntArray675[nI] - pz;
	
								final float xx = (float) cTile.anIntArray673[pt0] - px;
								final float yx = (float) 0 - py;
								final float zx = (float) cTile.anIntArray675[pt0] - pz;
								final float xy = (float) cTile.anIntArray673[pt1] - px;
								final float yy = (float) 0 - py;
								final float zy = (float) cTile.anIntArray675[pt1] - pz;
								final float xz = (float) cTile.anIntArray673[pt2] - px;
								final float yz = (float) 0 - py;
								final float zz = (float) cTile.anIntArray675[pt2] - pz;
	
								final float mnyz = my * nz - mz * ny;
								final float mnxz = mz * nx - mx * nz;
								final float mnxy = mx * ny - my * nx;
								float mn1 = ny * mnxy - nz * mnxz;
								float mn2 = nz * mnyz - nx * mnxy;
								float mn3 = nx * mnxz - ny * mnyz;
	
								float inv = 1f / (mn1 * mx + mn2 * my + mn3 * mz);
	
								float u1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
								float u2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
								float u3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
	
								mn1 = my * mnxy - mz * mnxz;
								mn2 = mz * mnyz - mx * mnxy;
								mn3 = mx * mnxz - my * mnyz;
	
								inv = 1.0F / (mn1 * nx + mn2 * ny + mn3 * nz);
	
								float v1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
								float v2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
								float v3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
	
								if (u1 < 0) u1 = 0;
								if (u1 > 1) u1 = 1;
								if (u2 < 0) u2 = 0;
								if (u2 > 1) u2 = 1;
								if (u3 < 0) u3 = 0;
								if (u3 > 1) u3 = 1;
								
								if (v1 < 0) v1 = 0;
								if (v1 > 1) v1 = 1;
								if (v2 < 0) v2 = 0;
								if (v2 > 1) v2 = 1;
								if (v3 < 0) v3 = 0;
								if (v3 > 1) v3 = 1;
	
								uvBuffer.put(u1);
								uvBuffer.put(1-v1);
								uvBuffer.put(u2);
								uvBuffer.put(1-v2);
								uvBuffer.put(u3);
								uvBuffer.put(1-v3);
								
								hasUV = true;
							}
						}
					}
				}
			}
			
			// all textures of the same kind are finished
			// we now put their count into our texture map
			// that way we know how many triangles in the VBO
			// need to be drawn with this texture id
			if (trianglesCounted > 0)
			{
				textureMappings.add(BitwiseHelper.ints2Long(texture, trianglesCounted));
				textureLoaded.add(texture);
				trianglesCounted = 0;
			}
		}
		
		vertexBuffer.flip();
		colorBuffer.flip();
		uvBuffer.flip();

		int BUFFER_TYPE = GL15.GL_STATIC_DRAW;

		int size = 2;
		if (hasUV) {
			size = 3;
		}

		final int[] bufs = GLBufferManager.genBuffers(size);
		
		int vertexHandle = bufs[0];
		int colorHandle = bufs[1];
		int uvHandle = hasUV ? bufs[2] : 0;
		
		writeHandles(vertexHandle, colorHandle, uvHandle, hasUV, BUFFER_TYPE);
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in loadFloor("+uid+", "+vertexHandle+", "+colorHandle+")");
		}

		//if (!Client.enableHDTextures)
		//	textureMappings = null;
		
		// register VBO
		vbo = new ModelVBO(startX, startY, true);
		Cache.addFloorVBO(uid, vbo);
		//System.out.println("null vbo: "+uid+", "+vbo+", "+Cache.getFloorVBOs().size());
		vbo.update(vertexHandle, colorHandle, uvHandle, triangleCount, uid, false, -1, false, textureMappings);
		
		//System.out.println("put: "+uid+", "+vbo);
		
		return vbo;
	}

	public static int scaleRGB(int rgb, boolean animated, boolean isFloor)
	{
		if (/*!GraphicsDisplay.friendlyColors || */animated || !isFloor)
			return rgb;
		
		float r = (rgb >> 16) & 0xFF;
		float g = (rgb >> 8) & 0xFF;
		float b = (rgb >> 0) & 0xFF;
		
		float s = 1.15f;
		r *= s;
		g *= s;
		b *= s;
		
		if (r > 255)
			r = 255;
		if (g > 255)
			g = 255;
		if (b > 255)
			b = 255;
		
		return ((int)r << 16) + ((int)g << 8) + ((int)b << 0);
	}
	
	public static boolean isModelLoaded(boolean animated, long uid)
	{
		if (animated)
			return false;
		
		final ModelVBO vbo = Cache.getModelVBOs().get(uid);
		return vbo != null;
		
	}

	static {
		if (GraphicsDisplay.enabled) {
			init();
		}
	}

	public static void init() {
		colorIntersections = new HashMap<Long, List<Integer>>();
		textureLoaded = new LinkedList<Integer>();
	}

	public static void cleanup() {
		colorIntersections = null;
		textureLoaded = null;
	}

	private static List<Integer> textureLoaded;
	public ModelVBO loadModel(DrawableModel drawable, boolean interfaceModel)
	{
		// use atlases
		/*if (Cache.modelTextureAtlas128 != null)
		{
			return AtlasModelLoader.loadModel(drawable, interfaceModel);
			
			// loading model via an atlas worked
			// --> return it
			//if (v != null)
			//	return v;
		}*/
		final Model model = drawable.getModel();
		long uid = drawable.getModelUid();
		final boolean animated = drawable.isAnimated();
		final boolean adjustGround = drawable.isAdjustGround();
		
		//Model model, long uid, int objectId, float SF, boolean animated, boolean interfaceModel, boolean adjustGround, boolean noActions
		if (adjustGround)
		{
			uid >>= 14L;
			uid <<= 14L;
		}
		
		// atlas loading didn't work, load it the old and classic way
		
		clearBuffers();
		
		ModelVBO vbo = Cache.getModelVBOs().get(uid);
		
		// model already loaded
		if (vbo != null && !animated)
			return vbo;

        int[] faces = null;
        Particle particles = null;
		
		// the isUpdate boolean gives a lot of performance!
		// UV coordinates don't have to be calculated another
		// time for animated models --> +10-20% fps
		boolean isUpdate = vbo != null;
		
		// textures require UV coordinate updates
		//if (Client.enableHDTextures)
		//	isUpdate = false;

		// model has UV coordinates?
		boolean hasUV = true;

		// still need new textures to load?
		boolean loading = true;

		int texture = 0;
		int trianglesCounted = 0;
		int iterations = 0;
		boolean colorUpdate = true;
		boolean hasAlpha = false;
		
		float depth = 0f;
		//if (interfaceModel)
		//	depth = (VBORenderer.atlasDepth += VBORenderer.ATLAS_HEIGHT_MODIFIER);
		
		// a maximum of 10 textures per model
		LongList textureMappings = new LongArrayList();
		
		while (loading)
		{
			iterations++;
			//System.out.println("iteration "+iterations+": "+texture+", "+counted);
			
			// all textures of the same kind are finished
			// we now put their count into our texture map
			// that way we know how many triangles in the VBO
			// need to be drawn with this texture id
			if (iterations != 1)
			{
				textureMappings.add(BitwiseHelper.ints2Long(texture, trianglesCounted));
				textureLoaded.add(texture);
				trianglesCounted = 0;
			}
			
			loading = false;
			boolean countingTexture = false;
			
			for (int k = 0; k < model.triangleCount; k++)
			{
				int drawType = model.drawTypes != null && k < model.drawTypes.length ? model.drawTypes[k] : 0;
				int rgb0 = 0;
				int rgb1 = 0;
				int rgb2 = 0;
				boolean alwaysDraw = false;
				
				int alpha = model.triangleAlphaValues != null && k < model.triangleAlphaValues.length ? 255 - model.triangleAlphaValues[k] : 255;
				if (alpha > 255)
					alpha = 255;
				
				if (alpha < 255)
					hasAlpha = true;
					
				int pt0 = model.triangleViewSpaceX[k];
				int pt1 = model.triangleViewSpaceY[k];
				int pt2 = model.triangleViewSpaceZ[k];
				
				float px0 = (float) (interfaceModel ? Model.vertexPerspectiveX[pt0] : model.getVertexX(pt0));
				float py0 = (float) (interfaceModel ? Model.vertexPerspectiveY[pt0] : model.getVertexY(pt0));
				float pz0 = (float) (interfaceModel ? Model.vertexPerspectiveZ[pt0] : model.getVertexZ(pt0));
				
				float px1 = (float) (interfaceModel ? Model.vertexPerspectiveX[pt0] : model.getVertexX(pt1));
				float py1 = (float) (interfaceModel ? Model.vertexPerspectiveY[pt0] : model.getVertexY(pt1));
				float pz1 = (float) (interfaceModel ? Model.vertexPerspectiveZ[pt0] : model.getVertexZ(pt1));
	
				float px2 = (float) (interfaceModel ? Model.vertexPerspectiveX[pt0] : model.getVertexX(pt2));
				float py2 = (float) (interfaceModel ? Model.vertexPerspectiveY[pt0] : model.getVertexY(pt2));
				float pz2 = (float) (interfaceModel ? Model.vertexPerspectiveZ[pt0] : model.getVertexZ(pt2));
				
				if (interfaceModel)
				{
					pz0 += depth;
					pz1 += depth;
					pz2 += depth;
				}
				
				int hsl0 = 0;
				if (drawType != 1 || model.colorValues1 != null)
				{
					rgb0 = scaleRGB(Rasterizer.anIntArray1482[model.colorValues1[k]], animated, false);
					rgb1 = scaleRGB(Rasterizer.anIntArray1482[model.colorValues2[k]], animated, false);
					rgb2 = scaleRGB(Rasterizer.anIntArray1482[model.colorValues3[k]], animated, false);
					hsl0 = model.colorValues1[k];
					
					if (rgb1 <= 1)
						rgb1 = rgb0;
					if (rgb2 <= 1)
						rgb2 = rgb0;
				}
				else
				{
					rgb0 = rgb1 = rgb2 = scaleRGB(Rasterizer.anIntArray1482[model.colorValues[k]], animated, false);
					hsl0 = model.colorValues[k];
				}
				
				
				// ragefire, steadfast and glaiven boots fix
				// that isn't a beautiful way of removing vertices
				// but it works nonetheless
				if (alpha == 130 && hsl0 >= 32798 && hsl0 <= 32870)
				{
					//System.out.println("color: "+hsl0+", "+rgb0+", "+rgb1+", "+rgb2+", "+model.colorValues1);
					
					if (hsl0 == 32863)
						alpha = 0;
					else if (hsl0 == 32829)
						alpha = 0;
					else if (hsl0 == 32817)
						alpha = 0;
					else if (hsl0 == 32864)
						alpha = 0;
					else if (hsl0 == 32800)
						alpha = 0;
					else if (hsl0 == 32858)
						alpha = 0;
					else if (hsl0 == 32822)
						alpha = 0;
					else if (hsl0 == 32798)
						alpha = 0;
					
				}
				
				boolean clearColors = false;
				
				int texPtr = model.texturePointers == null ? 0 : model.texturePointers[k];
				int texType = 5;
				if (texPtr >= 0)
					texType = model.textureTypes == null || texPtr >= model.textureTypes.length ? 15 : model.textureTypes[texPtr];
				if(model.textureTrianglePIndex == null || texPtr >= model.textureTrianglePIndex.length)
					texType = 16;
				int newTexture = model.textureBackgrounds != null ? model.textureBackgrounds[k] : -1;

				if ((model.colorValues[k] <= 50 || model.texturePointers != null && k < model.texturePointers.length && model.texturePointers[k] != -1))
				{
					if (newTexture == 0 && model.colorValues[k] <= 50 && model.textureBackgrounds == null)
					{
						newTexture = model.colorValues[k];
						clearColors = true;
					}
				}
				
				// texturing for old models, revision 317
				if (newTexture <= 0 && model.colorValues[k] <= 50 && drawType != 0)
				{
					newTexture = model.colorValues[k];
				}
				
				// fire cape
				if (newTexture == 0 && model.colorValues[k] == 40)
				{
					//System.out.println("model: "+uid);
					//newTexture = 40;
				}
				
				// texture definition class
				Texture td = null;
				if (newTexture >= 0)
					td = Texture.get(newTexture);
				
				// always draw this texture?
				if (newTexture >= 0)
					alwaysDraw = ModelConfig.basicTexture[newTexture] || newTexture <= 50;
				
				// always draw following textures
				// tree crowns, older textures, roofs, etc...
				if (td != null && td.transparentTexture && newTexture > 0)
				{
					alwaysDraw = true;
					if (newTexture != 922)
						clearColors = true;
				}
				
				// textures for every model enabled?
				if (!alwaysDraw && !Client.enableHDTextures)
				{
					// no texturing
					if (newTexture >= 0 && !ModelConfig.basicTexture[newTexture])
					{
						clearColors = false;
						newTexture = -1;
					}
				}
				
				// animated models get no textures for now
				// it'd be a huge performance loss if we gave
				// animated models textures. that'd slow down
				// the UV calculation (& sorting!!) process
				if (animated && !alwaysDraw)
				{
					newTexture = -1;
				}
				
				// default texture id for models without one
				//if (Client.enableHDTextures && newTexture <= 0)
				//	newTexture = 225;
				
				// tree stump texture, it's just looking odd
				// so we simply remove it
				if (newTexture == 922)
				{
					alpha = Client.enableHDTextures ? 150 : 180;
					//continue;
				}
				
				// texture on grass, that's not properly working
				// we just ignore it at this point
				if (newTexture == 970)
				{
					newTexture = -1;
				}
				
				// load all triangles with the same texture id, so that we
				// have them after each other in our VBO. that means, that
				// we can draw all 'same' textures in 1 call per VBO
				if (countingTexture && newTexture != texture)
				{
					loading = true;
					continue;
				}
				
				// texture was already loaded
				if (textureLoaded.indexOf(newTexture) != -1)
				{
					continue;
				}

				// colors need to be cleared? some textures don't require
				// a color, so we just set it to white: 0xFFFFFF
				if (clearColors && newTexture != -1)
				{
					rgb0 = rgb1 = rgb2 = 0xFFFFFF;
				}
				
				//if (!animated)
				//	System.out.println("new texture: "+newTexture);
				texture = newTexture;
				countingTexture = true;
				trianglesCounted++;

				
				// fixing triangle priorities. only needed, when there's
				// no texture, because roofs with textures are properly adjusted.
				// TODO: bugs other objects
				if (model.trianglePriorities != null)
				{
					int p = model.trianglePriorities[k];
					float c = 0;
					
					// offsets
					if (p == 0) // falador roofs
						c = -0.05f;
					
					py0 -= c;
					py1 -= c;
					py2 -= c;
					
					px0 += c;
					px1 += c;
					px2 += c;
					
					pz0 -= c;
					pz1 -= c;
					pz2 -= c;
				}

				int r0 = ((rgb0 >> 16) & 0xFF);
				int g0 = ((rgb0 >> 8) & 0xFF);
				int b0 = ((rgb0 >> 0) & 0xFF);
				int a0 = (alpha);
	
				int r1 = ((rgb1 >> 16) & 0xFF);
				int g1 = ((rgb1 >> 8) & 0xFF);
				int b1 = ((rgb1 >> 0) & 0xFF);
				int a1 = (alpha);
	
				int r2 = ((rgb2 >> 16) & 0xFF);
				int g2 = ((rgb2 >> 8) & 0xFF);
				int b2 = ((rgb2 >> 0) & 0xFF);
				int a2 = (alpha);

				// vertex
				vertexBuffer.put(px0);
				vertexBuffer.put(py0);
				vertexBuffer.put(pz0);
				vertexBuffer.put(px1);
				vertexBuffer.put(py1);
				vertexBuffer.put(pz1);
				vertexBuffer.put(px2);
				vertexBuffer.put(py2);
				vertexBuffer.put(pz2);
				
				// color
				colorBuffer.put((byte) r0);
				colorBuffer.put((byte) g0);
				colorBuffer.put((byte) b0);
				colorBuffer.put((byte) a0);
				colorBuffer.put((byte) r1);
				colorBuffer.put((byte) g1);
				colorBuffer.put((byte) b1);
				colorBuffer.put((byte) a1);
				colorBuffer.put((byte) r2);
				colorBuffer.put((byte) g2);
				colorBuffer.put((byte) b2);
				colorBuffer.put((byte) a2);
				
				// load all triangles with the same texture id, so that we
				// have them after each other in our VBO. that means, that
				// we can draw all 'same' textures in 1 call per VBO
				if (countingTexture)// && newTexture != texture)//TODO
				{
					loading = true;
					continue;
				}
				
				// don't load UVs another time
				//if (vbo != null)
				//	continue;

				
				hasUV = true;
				if (model.uv != null && texture > 0)
				{
					float u1 = model.uv[k][0];
					float v1 = model.uv[k][1];
					float u2 = model.uv[k][2];
					float v2 = model.uv[k][3];
					float u3 = model.uv[k][4];
					float v3 = model.uv[k][5];
	
					addUV(u1, v1, u2, v2, u3, v3);
				}
				else
				{
					float u1 = 0;
					float v1 = 1;
					float u2 = 1;
					float v2 = 1;
					float u3 = 0;
					float v3 = 0;
	
					addUV(u1, v1, u2, v2, u3, v3);
				}
				
			}

			// all textures of the same kind are finished
			// we now put their count into our texture map
			// that way we know how many triangles in the VBO
			// need to be drawn with this texture id
			if (trianglesCounted > 0)
			{
				textureMappings.add(BitwiseHelper.ints2Long(texture, trianglesCounted));
				textureLoaded.add(texture);
				trianglesCounted = 0;
			}
		}

		vertexBuffer.flip();
		colorBuffer.flip();
		uvBuffer.flip();

		int BUFFER_TYPE = GL15.GL_STATIC_DRAW;
		if (animated)
			BUFFER_TYPE = GL15.GL_DYNAMIC_DRAW;
	
		int vertexHandle = 0;
		int colorHandle = 0;
		int uvHandle = 0;
		
		if (vbo == null)
		{
			int size = 2;
			if (hasUV)
				size = 3;

			final int[] bufs = GLBufferManager.genBuffers(size);
			vertexHandle = bufs[0];
			colorHandle = bufs[1];
			if (hasUV)
				uvHandle = bufs[2];
		}
		else
		{
			vertexHandle = vbo.getVertexHandle();
			colorHandle = vbo.getColorHandle();
			uvHandle = vbo.getUvHandle();
		}
		
		
		writeHandles(vertexHandle, colorHandle, uvHandle, hasUV, BUFFER_TYPE);
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in loadModel(" + uid + ", " + vertexHandle + ", " + colorHandle + ", " + uvHandle + ", " + hasUV + ")");
		}
		
		// register VBO
		if (vbo == null)
		{
			vbo = new ModelVBO(drawable.getX(), drawable.getY(), drawable.hasPositionHash());
			Cache.addModelVBO(uid, vbo);
		}
		vbo.update(vertexHandle, colorHandle, uvHandle, model.triangleCount, uid, animated, -1, hasAlpha, textureMappings);
		
		return vbo;
	}

	
	private void addUV(float u1, float v1, float u2, float v2, float u3, float v3)
	{
		uvBuffer.put(u1);
		uvBuffer.put(v1);
		uvBuffer.put(u2);
		uvBuffer.put(v2);
		uvBuffer.put(u3);
		uvBuffer.put(v3);
	}
	
}
