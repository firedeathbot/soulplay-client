package com.soulplayps.client.opengl.gl15.model;

import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;

import com.soulplayps.client.Client;
import com.soulplayps.client.opengl.GraphicsDisplay;

public class AssetLoader {
	
	public FloatBuffer vertexBuffer = null;
	public ByteBuffer colorBuffer = null;
	public FloatBuffer uvBuffer = null;
	public static final int BUFFER_SIZE = (65536 + 32768) * 4;
	
	public void clearBuffers() {
		if (vertexBuffer == null) {
			vertexBuffer = BufferUtils.createFloatBuffer(BUFFER_SIZE);
		} else {
			vertexBuffer.clear();
		}
		
		if (colorBuffer == null) {
			colorBuffer = BufferUtils.createByteBuffer(BUFFER_SIZE * 4 / 3);
		} else {
			colorBuffer.clear();
		}
		
		if (uvBuffer == null) {
			uvBuffer = BufferUtils.createFloatBuffer(BUFFER_SIZE * 2 / 3);
		} else {
			uvBuffer.clear();
		}
	}
	
	public void writeHandles(int vertexHandle, int colorHandle, int uvHandle, boolean hasUV, int BUFFER_TYPE) {
		writeHandles(vertexHandle, colorHandle, uvHandle, true, hasUV, false, BUFFER_TYPE);
	}

	public void writeHandles(int vertexHandle, int colorHandle, int uvHandle, boolean isUpdate, boolean hasUV, boolean colorUpdate, int BUFFER_TYPE) {
		/*final boolean useGlBufferSubData = false;
		if (useGlBufferSubData) {
			int vLen = vertexBuffer.limit();
			int cLen = colorBuffer.limit();
			int uvLen = uvBuffer.limit();
			
			
			// bind vertex data
			if (vbo == null || vLen != vbo.vertices) {
				glBindBuffer(GL_ARRAY_BUFFER, vertexHandle);
				glBufferData(GL_ARRAY_BUFFER, vertexBuffer, BUFFER_TYPE);
			} else {
				glBindBuffer(GL_ARRAY_BUFFER, vertexHandle);
				glBufferSubData(GL_ARRAY_BUFFER, 0, vertexBuffer);
			}
			
			
			// bind color data
			if (isUpdate) {
				if (vbo == null || cLen != vbo.colors) {
					glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
					glBufferData(GL_ARRAY_BUFFER, colorBuffer, BUFFER_TYPE);
				} else {
					glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
					glBufferSubData(GL_ARRAY_BUFFER, 0, colorBuffer);
				}
				
				// bind uv data
				if (hasUV) {
					if (vbo == null || uvLen != vbo.uvs) {
						glBindBuffer(GL_ARRAY_BUFFER, uvHandle);
						glBufferData(GL_ARRAY_BUFFER, uvBuffer, GL15.GL_STATIC_DRAW);
					} else {
						glBindBuffer(GL_ARRAY_BUFFER, uvHandle);
						glBufferSubData(GL_ARRAY_BUFFER, 0, uvBuffer);
					}
					
				}
			}
			
			// only color updated
			else if (colorUpdate) {
				if (vbo == null || cLen != vbo.colors) {
					glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
					glBufferData(GL_ARRAY_BUFFER, colorBuffer, BUFFER_TYPE);
				} else {
					glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
					glBufferSubData(GL_ARRAY_BUFFER, 0, colorBuffer);
				}
			}
			
			if (vbo != null) {
				vbo.vertices = vLen;
				vbo.colors = cLen;
				vbo.uvs = uvLen;
			}
		} else {*/
			// bind vertex data
			//System.out.println("set data = " + vertexHandle+":"+vertexBuffer.limit() + " - " + colorHandle+":"+colorBuffer.limit()+ " - "+uvHandle+":"+uvBuffer.limit());
			glBindBuffer(GL_ARRAY_BUFFER, vertexHandle);
			glBufferData(GL_ARRAY_BUFFER, vertexBuffer, BUFFER_TYPE);
			
			if (isUpdate) {
				glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
				glBufferData(GL_ARRAY_BUFFER, colorBuffer, BUFFER_TYPE);
				
				if (hasUV) {
					glBindBuffer(GL_ARRAY_BUFFER, uvHandle);
					glBufferData(GL_ARRAY_BUFFER, uvBuffer, GL15.GL_STATIC_DRAW);
				}
			} else if (colorUpdate) {
				glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
				glBufferData(GL_ARRAY_BUFFER, colorBuffer, BUFFER_TYPE);
			}
		//}
		
		/*
		
		// buffers don't exist yet
		if (allocate || true)
		{
			// bind vertex data
			glBindBuffer(GL_ARRAY_BUFFER, vertexHandle);
			glBufferData(GL_ARRAY_BUFFER, vertexBuffer, BUFFER_TYPE);
			
			if (isUpdate)
			{
				glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
				glBufferData(GL_ARRAY_BUFFER, colorBuffer, BUFFER_TYPE);
				
				if (hasUV)
				{
					glBindBuffer(GL_ARRAY_BUFFER, uvHandle);
					glBufferData(GL_ARRAY_BUFFER, uvBuffer, GL15.GL_STATIC_DRAW);
				}
			}
			else if (colorUpdate)
			{
				glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
				glBufferData(GL_ARRAY_BUFFER, colorBuffer, BUFFER_TYPE);
			}
		}
		
		// TODO: that doesn't seem to work correctly
		// update buffer data
		else
		{
			// bind vertex data
			glBindBuffer(GL_ARRAY_BUFFER, vertexHandle);
			glBufferSubData(GL_ARRAY_BUFFER, 0, vertexBuffer);
			
			if (isUpdate)
			{
				glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
				glBufferSubData(GL_ARRAY_BUFFER, 0, colorBuffer);
				
				if (hasUV)
				{
					glBindBuffer(GL_ARRAY_BUFFER, uvHandle);
					glBufferSubData(GL_ARRAY_BUFFER, 0, uvBuffer);
				}
			}
			else if (colorUpdate)
			{
				glBindBuffer(GL_ARRAY_BUFFER, colorHandle);
				glBufferSubData(GL_ARRAY_BUFFER, 0, colorBuffer);
			}
		}*/
			
			if (!Client.disableOpenGlErrors) {
				GraphicsDisplay.getInstance().exitOnGLError("[AssetLoader]: writeHandles(" + vertexHandle + ", " + colorHandle + ", " + uvHandle + ", " + isUpdate + ", " + hasUV + ")");
			}
	}
	
	public AssetLoader() {
		super();
	}
	
}
