package com.soulplayps.client.opengl.gl15.model;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import com.soulplayps.client.util.BitwiseHelper;
import it.unimi.dsi.fastutil.ints.IntList;
import org.lwjgl.BufferUtils;

import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.opengl.gl15.model.atlas.AtlasPosition;

public class TextureAtlasList {
	
	public List<TextureAtlas> getAtlases() {
		return atlases;
	}
	
	public List<List<AtlasPosition>> getPositions() {
		return positions;
	}
	
	public void delete(boolean clear) {
		for (TextureAtlas atlas : atlases)
			atlas.delete(clear);
		
		//atlases.clear();
		//positions.clear();
	}
	
	private List<TextureAtlas> atlases;
	private List<List<AtlasPosition>> positions;
	private ByteBuffer initBuffer;
	
	private final int size;
	private final int elements;
	private final int maxAtlasses;
	private final boolean texturesOnly;
	
	public TextureAtlasList(int size, int elements, int maxAtlasses, boolean restricted, boolean texturesOnly) {
		this.maxAtlasses = restricted ? maxAtlasses : 0;
		this.size = size;
		this.elements = elements;
		this.texturesOnly = texturesOnly;
		atlases = new ArrayList<TextureAtlas>();
		positions = new ArrayList<List<AtlasPosition>>();
		
		if (!restricted) {
			this.initBuffer = BufferUtils.createByteBuffer(size * size * 4);
			for (int i = 0; i < size * size; i++) {
				initBuffer.put((byte) 255); // r
				initBuffer.put((byte) 255); // g
				initBuffer.put((byte) 255); // b
				initBuffer.put((byte) 255); // a
			}
			initBuffer.flip();
		}
		
		// fixed amount of atlasses
		if (maxAtlasses != 0) {
			for (int i = 0; i < maxAtlasses; i++)
				increase();
			
			if (restricted)
				this.initBuffer = null;
		}
	}
	
	public TextureAtlasList(int size, int elements) {
		this(size, elements, 0, false, false);
	}
	
	private int count = 0;
	
	private void increase() {
		count++;
		atlases.add(new TextureAtlas(size, elements, count, this, texturesOnly));
		positions.add(new ArrayList<AtlasPosition>());
	}
	
	// finds the next free atlas, or the
	// atlas that contains the uid
	public int findAtlas(long uid) {
		// check if there exists an atlas, which
		// contains the given uid.
		for (int i = 0; i < atlases.size(); i++) {
			// uid found, return that atlas
			if (atlases.get(i).contains(uid))
				return i;
		}
		
		// find an atlas, which can load the new texture
		for (int i = 0; i < atlases.size(); i++) {
			if (atlases.get(i).isFull())
				continue;
			
			return i;
		}
		
		if (maxAtlasses != 0) {
			System.err.println("[TextureAtlasList]: no free atlas found!!");
			return -1;
		}
		
		// no atlas found --> create a new one
		increase();
		return findAtlas(uid);
	}
	
	// TODO: finish that monster algorithm!
	// loads a model with a given list of textures into
	// an atlas. we will search if an atlas exists with
	// the given combination of textures. if that's the case,
	// we return that atlas, else we create a new one.
	// we might store textures 2-3 times, but we end up having
	// a very performant solution! moreover, this process only
	// takes place once, when the model is initially loaded.
	
	public TextureAtlas addTextures(int width, int height, boolean isFloor, IntList textures) {
		return addTextures(width, height, isFloor, textures, 0);
	}
	
	public TextureAtlas addTexture(boolean isFloor, int texture) {
		int width = 128;
		int height = 128;
		
		// search for an atlas that contains the texture
		for (int i = 0; i < atlases.size(); i++) {
			if (atlases.get(i).contains(texture))
				return atlases.get(i);
		}
		
		// find an atlas to add the texture
		for (int i = 0; i < atlases.size(); i++) {
			if (!atlases.get(i).isFull()) {
				atlases.get(i).addTexture(texture, null, 255, width, height, false, true, texture, isFloor);
				return atlases.get(i);
			}
		}
		
		
		if (maxAtlasses != 0) {
			System.err.println("[TextureAtlasList]: no free atlas found!!");
			return null;
		}
		
		// no atlas found --> create a new one
		increase();
		return addTexture(isFloor, texture);
	}
	
	public TextureAtlas addTextures(int width, int height, boolean isFloor, IntList textures, int tries) {
		
		// search for an atlas that contains the given
		// list of textures.
		for (int i = 0; i < atlases.size(); i++) {
			if (atlases.get(i).hasTextures(textures))
				return atlases.get(i);
			
			// amount of textures won't fit into any atlas
			// due to GPU memory restrictions
			//if (textures.size() > atlases.get(i).getMaxCapacity())
			//	return null;
			
		}
		
		// search for an atlas that could contain the
		// new set of given textures
		for (int i = 0; i < atlases.size(); i++) {
			if (atlases.get(i).canAddTextures(textures)) {
				// add atlas elements
				for (int id : textures)
					atlases.get(i).addTexture(id, null, 255, width, height, false, true, id, isFloor);
				
				return atlases.get(i);
			}
		}
		
		// no atlas was found. check if any atlas
		// has enough free space to load the
		// new set of textures
		for (int i = 0; i < atlases.size(); i++) {
			if (atlases.get(i).hasCapacity(textures.size())) {
				// add atlas elements
				for (int id : textures)
					atlases.get(i).addTexture(id, null, 255, width, height, false, true, id, isFloor);
				
				return atlases.get(i);
			}
		}
		
		// gpu too small for this atlas
		if (tries != 0)
			return null;
		
		if (maxAtlasses != 0) {
			System.err.println("[TextureAtlasList]: no free atlas found!!");
			return null;
		}
		
		// no atlas found --> create a new one
		increase();
		return addTextures(width, height, isFloor, textures, 1);
	}
	
	public TextureAtlas getAtlas(long uid) {
		int idx = findAtlas(uid);
		if (idx == -1)
			return null;
		
		return atlases.get(idx);
	}
	
	public boolean addSprite(long uid, int x, int y, int alpha, int width, int height, Sprite sprite) {
		// negative uids mean that the sprite needs to be updated
		long uid2 = uid;
		if (uid2 < 0)
			uid2 *= -1;
		
		int idx = findAtlas(uid2);
		if (idx == -1) {
			//System.out.println("no atlas found: "+uid2);
			return false;
		}
		
		TextureAtlas atlas = atlases.get(idx);
		List<AtlasPosition> list = positions.get(idx);
		
		// add atlas element
		long offsets = atlas.addSprite(uid2, sprite, 255, width, height, uid < 0);
		if (offsets != -1) {
			list.add(new AtlasPosition(0xFFFFFF, alpha, x, y, uid, BitwiseHelper.long2IntA(offsets),
					BitwiseHelper.long2IntB(offsets), width, height, 0.0f, null));
			return true;
		}
		
		//System.out.println("offsets null: "+uid2);
		
		return true;
	}
	
	public ByteBuffer getInitBuffer() {
		return initBuffer;
	}
	
}
