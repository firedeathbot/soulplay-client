package com.soulplayps.client.opengl.gl15.particle;

import com.soulplayps.client.node.animable.model.Model;

public class ParticleGenerator
{
	public static Particle method2485(Model model, int numTriangles, int[] faces, int scale)
	{
		int[] anLocalInts = null;
		int[] anLocalInts_22_ = null;
		int[] anLocalInts_23_ = null;
		float[][] particles = null;
		if (model.texturePointers != null)
		{
			int numTextureTriangles = model.texTriangleCount;
			int[] anLocalInts_24_ = new int[numTextureTriangles];
			int[] anLocalInts_25_ = new int[numTextureTriangles];
			int[] anLocalInts_26_ = new int[numTextureTriangles];
			int[] anLocalInts_27_ = new int[numTextureTriangles];
			int[] anLocalInts_28_ = new int[numTextureTriangles];
			int[] anLocalInts_29_ = new int[numTextureTriangles];
			for (int triangle = 0; numTextureTriangles > triangle; triangle++)
			{
				anLocalInts_24_[triangle] = 2147483647;
				anLocalInts_25_[triangle] = -2147483647;
				anLocalInts_26_[triangle] = 2147483647;
				anLocalInts_27_[triangle] = -2147483647;
				anLocalInts_28_[triangle] = 2147483647;
				anLocalInts_29_[triangle] = -2147483647;
			}
			anLocalInts_22_ = new int[numTextureTriangles];
			anLocalInts_23_ = new int[numTextureTriangles];
			particles = new float[numTextureTriangles][];
			for (int triangle = 0; triangle < numTriangles; triangle++)
			{
				int face = faces[triangle];
				if (model.texturePointers[face] != -1)
				{
					int coordinate = model.texturePointers[face];
					
					if (coordinate < 0 && coordinate != -1)
						coordinate &= 0xFF;
					
					// int coordinate = model.texturePointers[face] & 0xff;
					//if (true)
					//	coordinate &= 0xff;
					
					if (coordinate > -1 && coordinate < numTextureTriangles)// new 800+
					{
						for (int anLocalInt_34_ = 0; anLocalInt_34_ < 3; anLocalInt_34_++)
						{
							int anLocalInt_35_;
							if (anLocalInt_34_ != 0)
							{
								if (anLocalInt_34_ == 1)
									anLocalInt_35_ = (model.triangleViewSpaceY[face]);
								else
									anLocalInt_35_ = (model.triangleViewSpaceZ[face]);
							}
							else
								anLocalInt_35_ = (model.triangleViewSpaceX[face]);

							int anLocalInt_36_ = (int) (model.getVertexX(anLocalInt_35_));
							int anLocalInt_37_ = (int) (model.getVertexY(anLocalInt_35_));
							int anLocalInt_38_ = (int) (model.getVertexZ(anLocalInt_35_));
							if (anLocalInt_36_ < anLocalInts_24_[coordinate])
								anLocalInts_24_[coordinate] = anLocalInt_36_;
							if (anLocalInts_25_[coordinate] < anLocalInt_36_)
								anLocalInts_25_[coordinate] = anLocalInt_36_;
							if (anLocalInt_37_ < anLocalInts_26_[coordinate])
								anLocalInts_26_[coordinate] = anLocalInt_37_;
							if (anLocalInt_37_ > anLocalInts_27_[coordinate])
								anLocalInts_27_[coordinate] = anLocalInt_37_;
							if (anLocalInt_38_ < anLocalInts_28_[coordinate])
								anLocalInts_28_[coordinate] = anLocalInt_38_;
							if (anLocalInts_29_[coordinate] < anLocalInt_38_)
								anLocalInts_29_[coordinate] = anLocalInt_38_;
						}
					}
				}
			}
			
			anLocalInts = new int[numTextureTriangles];
			for (int triangle = 0; triangle < numTextureTriangles; triangle++)
			{
				int type = model.textureTypes == null ? 0 : model.textureTypes[triangle] & 0xFF;
				
				//if (type <= 0)
				//	type = 2;
				
				//if (type <= 0)
				//type = 2;
				
				if (type > 0)
				{
					anLocalInts[triangle] = (anLocalInts_24_[triangle] + anLocalInts_25_[triangle]) / 2;
					anLocalInts_22_[triangle] = (anLocalInts_26_[triangle] + anLocalInts_27_[triangle]) / 2;
					anLocalInts_23_[triangle] = (anLocalInts_29_[triangle] + anLocalInts_28_[triangle]) / 2;
					float directionZ = 0f;
					float directionY = 0f;
					float directionX = 0f;
					
					//System.out.println("tri: "+model.particleDirectionX[triangle]+", "+model.particleDirectionY[triangle]+", "+model.particleDirectionZ[triangle]);

					//float x1 = model.particleDirectionX == null ? scale : model.particleDirectionX[triangle];
					//float y1 = model.particleDirectionY == null ? scale : model.particleDirectionY[triangle];
					//float z1 = model.particleDirectionZ == null ? scale : model.particleDirectionZ[triangle];
					
					float x1 = scale;
					float y1 = scale;
					float z1 = scale;
					type = 2;
					
					if (type == 1)
					{
						//model.particleDirectionX[triangle] = f;
						//model.particleDirectionY[triangle] = f;
						//model.particleDirectionZ[triangle] = f;
						
						int anLocalInt_43_ = (int) x1;
						if (anLocalInt_43_ != 0)
						{
							if (anLocalInt_43_ <= 0)
							{
								directionX = (float) -anLocalInt_43_ / 1024.0F;
								directionZ = 1.0F;
							}
							else
							{
								directionZ = (float) anLocalInt_43_ / 1024.0F;
								directionX = 1.0F;
							}
						}
						else
						{
							directionX = 1.0F;
							directionZ = 1.0F;
						}

						directionY = 64.0F / y1;
					}
					else if (type == 2)
					{
						
						directionX = 64.0F / (float) x1;
						directionY = 64.0F / (float) y1;
						directionZ = 64.0F / (float) z1;
					}
					else
					{
						//model.particleDirectionX[triangle] = -f;
						//model.particleDirectionY[triangle] = -f;
						//model.particleDirectionZ[triangle] = -f;
						directionX = x1 / 1024.0F;
						directionY = y1 / 1024.0F;
						directionZ = z1 / 1024.0F;
					}
					

					//System.out.println(type+": "+directionX+", "+directionY+", "+directionZ+"; "+model.particleDirectionX[triangle]);
					
					
					particles[triangle] = generateParticles(directionY, model.textureTrianglePIndex[triangle], model.textureTriangleMIndex[triangle],
							model.particleLifespanX == null ? 1 : model.particleLifespanX[triangle] & 255, model.textureTriangleNIndex[triangle], directionX, directionZ);
				}
			}
		}
		return new Particle(anLocalInts, anLocalInts_22_, anLocalInts_23_, particles);
	}

	public static float[] generateParticles(float directionY, int textureTrianglePointX, int textureTrianglePointY, int lifespan, int textureTrianglePointZ,
			float directionX, float directionZ)
	{
		float[] startingParticles = new float[9];
		float[] translatedParticles = new float[9];
		float rotationOffsetX = (float) Math.cos(lifespan * 0.024543693f);
		float rotationOffsetY = (float) Math.sin(lifespan * 0.024543693f);
		
		//if (rotationOffsetX > 1)
		//	rotationOffsetX = 1;
		//else if (rotationOffsetY > 1)
		//	rotationOffsetY = 1;
		
		float pointY = 1.0F - rotationOffsetX;
		startingParticles[0] = rotationOffsetX;
		startingParticles[1] = 0.0F;
		startingParticles[2] = rotationOffsetY;
		startingParticles[3] = 0.0F;
		startingParticles[4] = 1.0F;
		startingParticles[5] = 0.0F;
		startingParticles[6] = -rotationOffsetY;
		startingParticles[7] = 0.0F;
		startingParticles[8] = rotationOffsetX;
		float[] rotatedParticles = new float[9];
		float pointZ = 1.0F;
		float pointX = 0.0F;
		rotationOffsetX = (float) textureTrianglePointY / 32767.0F;
		pointY = 1.0F - rotationOffsetX;
		
		double ar = -(rotationOffsetX * rotationOffsetX);
		double a1 = (double) (ar + 1.0);
		double a2 = textureTrianglePointX * textureTrianglePointX + textureTrianglePointZ * textureTrianglePointZ;

		//System.out.println("a1: "+a1+", "+a2+"; "+textureTrianglePointX+", "+textureTrianglePointZ+", "+rotationOffsetX);
		
		if (a1 < 0)
			a1 = 0;
		if (a2 < 0)
			a2 = 0;
		
		rotationOffsetY = -(float) Math.sqrt(a1);
		float speed = (float) Math.sqrt(a2);
		
		
		if (speed != 0.0F || rotationOffsetX != 0.0F)
		{
			if (speed != 0.0F)
			{
				pointX = (float) textureTrianglePointX / speed;
				pointZ = (float) -textureTrianglePointZ / speed;
			}
			rotatedParticles[5] = rotationOffsetY * pointZ;
			rotatedParticles[6] = pointY * (pointX * pointZ);
			rotatedParticles[3] = -pointX * rotationOffsetY;
			rotatedParticles[4] = rotationOffsetX;
			rotatedParticles[0] = rotationOffsetX + pointZ * pointZ * pointY;
			rotatedParticles[7] = -pointZ * rotationOffsetY;
			rotatedParticles[1] = pointX * rotationOffsetY;
			rotatedParticles[8] = pointY * (pointX * pointX) + rotationOffsetX;
			rotatedParticles[2] = pointY * (pointZ * pointX);
			translatedParticles[0] = rotatedParticles[6] * startingParticles[2]
					+ (rotatedParticles[3] * startingParticles[1] + rotatedParticles[0] * startingParticles[0]);
			translatedParticles[1] = rotatedParticles[1] * startingParticles[0] + startingParticles[1] * rotatedParticles[4] + startingParticles[2]
					* rotatedParticles[7];
			translatedParticles[3] = rotatedParticles[6] * startingParticles[5]
					+ (rotatedParticles[3] * startingParticles[4] + startingParticles[3] * rotatedParticles[0]);
			translatedParticles[2] = rotatedParticles[8] * startingParticles[2]
					+ (rotatedParticles[5] * startingParticles[1] + startingParticles[0] * rotatedParticles[2]);
			translatedParticles[4] = startingParticles[5] * rotatedParticles[7]
					+ (startingParticles[3] * rotatedParticles[1] + rotatedParticles[4] * startingParticles[4]);
			translatedParticles[5] = rotatedParticles[8] * startingParticles[5]
					+ (rotatedParticles[2] * startingParticles[3] + rotatedParticles[5] * startingParticles[4]);
			translatedParticles[6] = startingParticles[6] * rotatedParticles[0] + startingParticles[7] * rotatedParticles[3] + rotatedParticles[6]
					* startingParticles[8];
			translatedParticles[7] = startingParticles[6] * rotatedParticles[1] + rotatedParticles[4] * startingParticles[7] + rotatedParticles[7]
					* startingParticles[8];
			translatedParticles[8] = startingParticles[7] * rotatedParticles[5] + rotatedParticles[2] * startingParticles[6] + startingParticles[8]
					* rotatedParticles[8];
		}
		else
			translatedParticles = startingParticles;
		translatedParticles[0] *= directionX;
		translatedParticles[1] *= directionX;
		translatedParticles[2] *= directionX;
		translatedParticles[3] *= directionY;
		translatedParticles[4] *= directionY;
		translatedParticles[5] *= directionY;
		translatedParticles[6] *= directionZ;
		translatedParticles[7] *= directionZ;
		translatedParticles[8] *= directionZ;
		
		//for (int i = 0; i < translatedParticles.length; i++)
		//	System.out.println("tr: "+translatedParticles[i]);
		
		return translatedParticles;
	}

}
