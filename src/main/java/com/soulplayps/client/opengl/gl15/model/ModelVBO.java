package com.soulplayps.client.opengl.gl15.model;

import com.soulplayps.client.Client;
import it.unimi.dsi.fastutil.longs.LongList;

public class ModelVBO {

	/**
	 * @return the vertexHandle
	 */
	public int getVertexHandle() {
		return vertexHandle;
	}
	
	/**
	 * @return the colorHandle
	 */
	public int getColorHandle() {
		return colorHandle;
	}
	
	/**
	 * @return the triangles
	 */
	public int getTriangles() {
		return triangles;
	}
	
	/**
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}
	
	/**
	 * @return the animated
	 */
	public boolean isAnimated() {
		return animated;
	}
	
	public int getUvHandle() {
		return uvHandle;
	}

	public void update(int vertexHandle, int colorHandle, int uvHandle, int triangles, long uid, boolean animated,
	                   int objectId, boolean hasAlpha, LongList textures) {
		if (this.vertexHandle != 0 && vertexHandle != this.vertexHandle)
			System.err.println("[ModelVBO]: vertexHandle memory leak: " + this.vertexHandle + " --> " + vertexHandle);
		
		if (this.colorHandle != 0 && colorHandle != this.colorHandle)
			System.err.println("[ModelVBO]: colorHandle memory leak: " + this.colorHandle + " --> " + colorHandle);
		
		if (this.uvHandle != 0 && uvHandle != this.uvHandle)
			System.err.println("[ModelVBO]: uvHandle memory leak: " + this.uvHandle + " --> " + uvHandle);
		
		this.vertexHandle = vertexHandle;
		this.colorHandle = colorHandle;
		this.uvHandle = uvHandle;
		this.triangles = triangles;
		this.uid = uid;
		this.animated = animated;
		this.objectId = objectId;
		this.hasAlpha = hasAlpha;
		if (textures != null) {
			this.textures = textures;
		}
	}
	
	public int getObjectId() {
		return objectId;
	}
	
	public boolean isHasAlpha() {
		return hasAlpha;
	}
	
	public LongList getTextures() {
		return textures;
	}
	
	public void setHandles(int vertex, int color, int uv) {
		this.vertexHandle = vertex;
		this.colorHandle = color;
		this.uvHandle = uv;
	}
	
	public void clear() {
		this.textures = null;
	}
	
	public float lastAnimatedTextureProgress = 0f;
	public int vertices = 0;
	public int colors = 0;
	public int uvs = 0;
	
	// optional corresponding texture atlas
	//public TextureAtlas atlas = null;
	
	// do we have a texture ID or texture handle available?
	public boolean isTextureID = true;
	
	public boolean hasAnimatedTextures = false;
	public boolean clear = false;
	public boolean delete = false;
	public boolean handlesReleased = false;
	
	public boolean isAreaBuffer = false;
	
	public boolean isVisible = true;
	private int objectId;
	private boolean hasAlpha;
	private LongList textures = null;// = new LongArrayList(); // optimization to conserve RAM
	public long lastUse = System.currentTimeMillis();
	private int vertexHandle;
	private int colorHandle;
	private int uvHandle;
	private int triangles;
	private long uid;
	private boolean animated;
	private final boolean positionHash;
	
	public boolean hasPositionHash() {
		return positionHash;
	}
	
	private int worldX = 0, worldY = 0;
	
	public int getWorldX() {
		return worldX;
	}
	
	public int getWorldY() {
		return worldY;
	}
	
	public ModelVBO(int wx, int wy, boolean hasPositionHash) {
		this.worldX = Client.regionBaseX + wx;
		this.worldY = Client.regionBaseY + wy;
		this.positionHash = hasPositionHash;
	}
}
