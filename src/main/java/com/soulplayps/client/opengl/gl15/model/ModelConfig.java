package com.soulplayps.client.opengl.gl15.model;

import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.opengl.GraphicsDisplay;

public class ModelConfig {
	
	public static boolean[] oldTexture;
	public static boolean[] basicTexture;
	public static boolean[] flipUVTexture;
	public static boolean[] animatedTexture;

	static {
		if (GraphicsDisplay.enabled) {
			init();
		}
	}

	public static void cleanup() {
		oldTexture = null;
		basicTexture = null;
		flipUVTexture = null;
		animatedTexture = null;
	}

	public static void init() {
		// old textures use different drawing method
		oldTexture = new boolean[Texture.COUNT];
		
		// basic textures are always drawn
		basicTexture = new boolean[Texture.COUNT];
		
		// textures that need uv coordinates flipped
		flipUVTexture = new boolean[Texture.COUNT];
		
		// animated textures
		animatedTexture = new boolean[Texture.COUNT];
		
		animatedTexture[40] = true; // fire cape
		animatedTexture[34] = true;
		animatedTexture[59] = true;
		//animatedTexture[61] = true; // bugs with terrain! (wildy agility course)
		//animatedTexture[61] = true; // remove
		//animatedTexture[111] = true;
		
		for (int texID = 0; texID < Texture.COUNT; texID++) {
			if ((texID >= 1321 && texID <= 1326) // evergreen
					|| (texID >= 937 && texID <= 945) // willow
					|| texID == 50
					|| texID == 45) {
				oldTexture[texID] = true;
			}
			
			boolean alwaysDraw = false;
			boolean flipUV = false;
			
			// TODO Jire: We need to convert all these IDs so the textures show up correctly
			
			if (texID <= 59)
				alwaysDraw = true;
				
				// oak tree
			else if (texID >= 962 && texID <= 964)
				alwaysDraw = true;
				
				// willow tree
			else if (texID >= 937 && texID <= 945)
				alwaysDraw = true;
				
				// grand exchange small plants
			else if (texID >= 933 && texID <= 935)
				alwaysDraw = true;
				
				// yew tree
			else if (texID >= 1348 && texID <= 1351)
				alwaysDraw = true;
				
				// spirit tree
			else if (texID >= 1314 && texID <= 1316)
				alwaysDraw = true;
				
				// evergreen
			else if (texID >= 1321 && texID <= 1326) {
				alwaysDraw = true;
				flipUV = true;
			} else if (texID == 922) // moss on stones
				alwaysDraw = true;
			
			basicTexture[texID] = alwaysDraw;
			flipUVTexture[texID] = flipUV;
			
			basicTexture[texID] = false;
			oldTexture[texID] = false;
			flipUVTexture[texID] = false;
		}
	}
	
}
