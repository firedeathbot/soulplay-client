package com.soulplayps.client.opengl.gl15.model.atlas;

import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.Scissor;

public class AtlasPosition {
	
	/**
	 * @return the color
	 */
	public int getColor() {
		return color;
	}
	
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}
	
	public AtlasPosition(int color, int alpha, int x, int y, long uid, int ox, int oy, int width, int height, float rotation, Scissor scissor) {
		this.color = color;
		this.alpha = alpha;
		this.x = x;
		this.y = y;
		this.ox = ox;
		this.oy = oy;
		this.uid = uid;
		this.width = width;
		this.height = height;
		this.depth = GraphicsDisplay.getSpriteDepth();
		this.rotation = rotation;
		if (scissor != null && scissor.width >= 0 && scissor.height >= 0) {
			this.scissor = scissor;
		} else {
			this.scissor = null;
		}
	}

	public AtlasPosition(int color, int alpha, int x, int y, long uid, int ox, int oy, int width, int height, Scissor scissor) {
		this(color, alpha, x, y, uid, ox, oy, width, height, 0f, scissor);
	}
	
	public int getOy() {
		return oy;
	}
	
	public int getOx() {
		return ox;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	private final int color;
	private final int alpha;
	
	/**
	 * @return the alpha
	 */
	public int getAlpha() {
		return alpha;
	}
	
	public float getDepth() {
		return depth;
	}
	
	public float getRotation() {
		return rotation;
	}
	
	public Scissor getScissor() {
		return scissor;
	}
	
	private final float rotation;
	private final float depth;
	private final int x;
	private final int y;
	private final int ox;
	private final int oy;
	private final long uid;
	private final int width;
	private final int height;
	private final Scissor scissor;
	
}
