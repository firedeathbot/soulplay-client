package com.soulplayps.client.opengl.gl15.model;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.atlas.AtlasTexture;
import com.soulplayps.client.opengl.util.AtlasTextureLoader;
import com.soulplayps.client.opengl.util.GLBufferManager;
import com.soulplayps.client.util.BitwiseHelper;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.longs.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;

import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;

public class TextureAtlas {
	
	public static int ATLAS_COUNT = 0;
	
	private boolean isFull = false;
	
	/**
	 * @return the isFull
	 */
	public boolean isFull() {
		return isFull;
	}
	
	/**
	 * returns the amount of remaining elements
	 *
	 * @return
	 */
	public int remaining() {
		return space;
	}
	
	public int getMaxCapacity() {
		return maxSpace;
	}
	
	public boolean hasCapacity(int capacity) {
		return remaining() - capacity >= 0;
	}
	
	// check if the given collection of textures
	// is present in this atlas
	public boolean hasTextures(IntList textures) {
		boolean containsAll = true;
		LongSet texs = pointers.keySet();
		for (int i = 0; i < textures.size(); i++) {
			int tex = textures.getInt(i);
			if (!texs.contains(tex)) {
				containsAll = false;
				break;
			}
		}
		return containsAll;
	}
	
	public boolean canAddTextures(IntList textures) {
		int sp = textures.size();
		LongSet texs = pointers.keySet();
		for (int i = 0; i < textures.size(); i++) {
			int tex = textures.getInt(i);
			if (texs.contains(tex))
				sp--;
		}
		
		return hasCapacity(sp);
	}
	
	private final boolean texturesOnly;
	private final TextureAtlasList parent;
	private final int atlasListId;
	private final int maxSpace;
	private int space = 0;
	private final int size;
	private final int elementSize;
	private final int totalSize;
	private static ByteBuffer buffer;
	private Long2LongMap pointers = null;
	private int textureID = 0;

	static {
		if (GraphicsDisplay.enabled) {
			init();
		}
	}

	public static void init() {
		buffer = BufferUtils.createByteBuffer(512 * 512);
	}

	public static void cleanup() {
		buffer = null;
	}

	/**
	 * @return the textureID
	 */
	public int getTextureID() {
		return textureID;
	}
	
	public int getTextureID2() {
		if (textureID == 0)
			bindBuffer(true);
		
		return textureID;
	}
	
	public long getOffsets(long uid) {
		long value = pointers.get(uid);
		return value != 0 ? value : -1L;
	}
	
	public void test() {
		for (long l : pointers.keySet())
			System.out.println("uid: " + l);
	}
	
	private int offsetX = 0;
	private int offsetY = 0;
	
	// size of the texture atlas in integers!
	// the total size is size*size*4 for bytes
	public TextureAtlas(int size, int elementSize) {
		this(size, elementSize, 0);
	}
	
	
	public TextureAtlas(int size, int elementSize, int id) {
		this(size, elementSize, id, null);
	}
	
	
	public TextureAtlas(int size, int elementSize, int id, TextureAtlasList parent) {
		this(size, elementSize, id, parent, false);
	}
	
	public TextureAtlas(int size, int elementSize, int id, TextureAtlasList parent, boolean texturesOnly) {
		if (ATLAS_COUNT >= 1000) {
			String msg = "More than 1.000 texture atlases created. Program exited to avoid GPU memory leaks. Try disabling VBO Mode if this issue persists.";
			GraphicsDisplay.getInstance().exitError(msg);
			throw new RuntimeException(msg);
		}
		
		//this.space = (size*size) / (elementSize*elementSize*4);
		this.space = (size / elementSize) * 2;
		this.maxSpace = space;
		this.size = size;
		this.elementSize = elementSize;
		this.totalSize = size * size;
		this.pointers = new Long2LongOpenHashMap();
		ATLAS_COUNT++;
		this.atlasListId = id;
		this.parent = parent;
		this.texturesOnly = texturesOnly;
		
		//System.out.println("atlas: "+texturesOnly+", "+atlasListId);
		if (texturesOnly && atlasListId == 1)
			this.initTextures();
		
		// initialize buffer
		//bindBuffer(true);
	}
	
	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * @return the elementSize
	 */
	public int getElementSize() {
		return elementSize;
	}
	
	private void bindBuffer(boolean mipmap) {
		buffer.clear();
		//if (GraphicsDisplay.getInstance().isIntel())
		//	mipmap = false;
		//mipmap = false; // no performance loss with/without on laptop
		
		if (textureID == 0) {
			int texID = GLBufferManager.genTextureBuffer(GLBufferManager.TEXTURE_ATLAS); // Generate texture ID
			GraphicsDisplay.setTexture(texID);
			
			// TODO: clamp to edge looks ugly!
			
			if (!mipmap) {
				// Setup wrap mode
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				
				// high quality
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				
				// no mipmapping
				glTexParameteri(GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL_FALSE);
			} else {
				// intel chips do horrible mipmapping for atlasses!!
				if (GraphicsDisplay.getInstance().isIntel())
					glTexParameteri(GL_TEXTURE_2D, GL12.GL_TEXTURE_MAX_LEVEL, 0);
				
				// max mipmap level
				//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
				//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 5);
				
				// Setup wrap mode
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				
				// Setup texture scaling filtering
				// TODO: mipmapping in texture atlas?
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				
				// use mipmapping
				glTexParameteri(GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL_TRUE);
			}
			
			// Return the texture ID so we can bind it later again
			textureID = texID;
			ByteBuffer initBuffer = parent != null ? parent.getInitBuffer() : BufferUtils.createByteBuffer(size * size * 4);
			
			
			//System.out.println("initbuffer1: "+initBuffer.remaining()+", "+initBuffer.limit()+", "+initBuffer.capacity()+", "+initBuffer.position());
			
			// setup initial atlas size
			if (parent == null) {
				for (int i = 0; i < size * size; i++) {
					initBuffer.put((byte) 255); // r
					initBuffer.put((byte) 0); // g
					initBuffer.put((byte) 0); // b
					initBuffer.put((byte) 255); // a
				}
				initBuffer.flip();
			}
			
			//System.out.println("initbuffer2: "+initBuffer.remaining()+", "+initBuffer.limit()+", "+initBuffer.capacity()+", "+initBuffer.position());
			
			//glTexImage2D(target, level, internalFormat, width, height, border, format, type, buffer);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, initBuffer);
			
			if (parent == null)
				initBuffer.clear();
		} else {
			GraphicsDisplay.setTexture(textureID);
		}
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.bindLetterBuffer(" + mipmap + ", " + size + ", " + elementSize + ")");
		}
	}
	
	private boolean increment(boolean isTextureMipmapped, int pixelBorder) {
		int width = elementSize;
		int height = elementSize;
		
		int dx = 0;
		int dy = 0;
		
		if (pixelBorder != 0)
			dx = dy = elementSize;
		
		if (offsetX + width + dx < size) {
			//System.out.println("offset: "+offsetX+", "+width+", "+size);
			offsetX += width + pixelBorder;
			space--;
			return true;
		}
		
		if (offsetY + height + dy < size) {
			offsetY += height + pixelBorder;
			offsetX = 0;
			space--;
			return true;
		}
		
		space = 0;
		isFull = true;
		return false;
	}
	
	public long addSprite(long hash, Sprite sprite, int alpha, int width, int height, boolean update) {
		if (sprite == null)
			return -1;
		
		if (!update) {
			long value = pointers.get(hash);
			if (value != 0) {
				return value;
			}
		}
		
		// dynamic sprite loading and deloading
		// conserves a lot of RAM.
		//sprite.reload();//TODO reload?
		long pos = addTexture(hash, sprite.myPixels, alpha, width, height, update, false, 0, false);
		//sprite.clear();
		
		return pos;
	}
	
	public boolean contains(long hash) {
		return pointers.containsKey(hash);
	}
	
	public void addPixel() {
		byte c = (byte) 255;
		byte a = (byte) 255;
		
		buffer.put(c); // Red component
		buffer.put(c); // Green component
		buffer.put(c); // Blue component
		buffer.put(a); // Alpha component
	}
	
	// all textures are 128x128 --> 7 mipmap levels
	private ByteBuffer[] data = null;
	
	public ByteBuffer[] genMipmap(int width, int height, int[] pixels, boolean hasAlpha, int alpha, int blendType) {
		if (data == null) {
			// 2^7 = 128
			data = new ByteBuffer[7];
			int idx = 0;
			int w = width;
			int h = height;
			
			while (w > 1 && h > 1) {
				w /= 2;
				h /= 2;
				data[idx] = BufferUtils.createByteBuffer(w * h * 4);
			}
		} else {
			int idx = 0;
			int w = width;
			int h = height;
			
			while (w > 1 && h > 1) {
				w /= 2;
				h /= 2;
				
				data[idx].clear();
			}
		}
		
		int w = width;
		int h = height;
		int idx = 0;
		
		while (w > 1 && h > 1) {
			w = width / 2;
			h = height / 2;
			int level = idx + 1;
			
			for (int y = height - 1; y >= 0; y -= level * 2) {
				for (int x = 0; x < w; x += level * 2) {
					int pixel = pixels[(y % height) * width + x];
					int alphaBack = alpha;
					
					// fixing white and black pixels that
					// are supposed to be transparent!
					if (pixel == 0 || pixel == -1) {
						alpha = 256;
					}
					
					
					if (blendType != 1 && blendType != 2)
						blendType = 0;
					
					//if (id == 823)
					//	System.out.println("blend type: "+blendType);
					
					if (blendType == 0) {
						pixel = 0x888888;
						alpha = 255;
					}
					
					buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
					buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
					buffer.put((byte) ((pixel >> 0) & 0xFF)); // Blue component
					
					if (alpha == 0 || (alpha == 255 && hasAlpha))
						buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component. Only for RGBA
					else
						buffer.put((byte) (alpha & 0xFF));
					
					alpha = alphaBack;
				}
			}
			
			idx++;
		}
		
		return data;
	}
	
	private boolean requiresData(boolean update, long hash) {
		return !pointers.containsKey(hash);
	}
	
	public long addTexture(long hash, int[] pixels, int alpha, int width, int height, boolean update, boolean isTexture, int id, boolean isFloor) {
		if (isTexture)
			hash = id;
		
		// texture already in atlas?
		long value = pointers.get(hash);
		long offsets = value != 0 ? value : -1;
		if (!update && offsets != -1)
			return offsets;
		
		if (isFull && !update) {
			System.err.println("[TextureAtlas]: too large texture object: " + id + ", " + (pixels == null ? 0 : pixels.length) + "/" + totalSize);
			return -1;
		}
		
		// invisible texture place holder?
		boolean invisible = false;
		
		// load texture pixels here
		if (pixels == null && isTexture) {
			// load texture data
			if (id > 0) {
				pixels = Rasterizer.method371(id);
			}
			
			// texture not yet loaded? no problem, we still 
			// reserve the space on the GPU. we do that, to
			// be able to set the offset values for the model
			// loader. textures will pop up a second later or so.
			if (pixels == null) {
				pixels = new int[width * height];
				invisible = true;
				
				// pixels null, we have to load it afterwards
				if (id > 0) {
					AtlasTextureLoader.addTexture(new AtlasTexture(this, width, height, id, isFloor));
				}
			}
		}
		
		// an update that has null offsets --> return
		if (isTexture && update && offsets == -1)
			return -1;
		
		// bind the texture buffer, or create it if it doesn't exist yet
		bindBuffer(true);
		
		// we have to check if the image has any alpha values first
		// if it doesn't, we shouldn't set any alpha values!!
		// else it just becomes invisible
		boolean hasAlpha = false;
		
		//if (isTexture)
		//	hasAlpha = false;
		
		if (!isTexture) {
			for (int y = height - 1; y >= 0; y--) {
				for (int x = 0; x < width; x++) {
					int pixel = pixels[y * width + x];
					int a = (pixel >> 24) & 0xFF;
					if (a > 0) {
						hasAlpha = true;
						break;
					}
				}
			}
		}
		
		for (int y = height - 1; y >= 0; y--) {
			for (int x = 0; x < width; x++) {
				int pixel = pixels[y * width + x];
				int alphaBack = alpha;
				
				// blank texture
				if (invisible) {
					hasAlpha = false;
					pixel = 0xFFFFFF;
					alpha = 255;
					
					//if (id <= 0)
					//	alpha = 255;
					
					//System.out.println("invisible tex: "+id);
				}
				
				// fixing white and black pixels that
				// are supposed to be transparent!
				else if (pixel == 0 || pixel == -1) {
					alpha = 256;
				}
				
				// done by Jire
				/*if (pixel == 0 && isTexture) {//TODO texture
					Texture def = Texture.get(id);
					if (def != null) {
						int blendType = def.anInt1226;
						if (blendType != 1 && blendType != 2)
							blendType = 0;
						
						//if (id == 823)
						//	System.out.println("blend type: "+blendType);
						
						if (blendType == 0) {
							pixel = 0xFFFFFF;
							alpha = 0;
						}
					}
				}*/
				
				buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
				buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
				buffer.put((byte) ((pixel >> 0) & 0xFF)); // Blue component
				
				if (alpha == 0 || (alpha == 255 && hasAlpha))
					buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component. Only for RGBA
				else
					buffer.put((byte) (alpha & 0xFF));
				
				alpha = alphaBack;
			}
		}
		
		
		//System.out.println("updating: "+id+", "+pixels+", "+offsetX+", "+offsetY+", "+invisible+", "+width+", "+height);
		
		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS
		
		//if (isTexture)
		//	System.out.println("texture: "+id+"/"+atlasListId+": "+width+" / "+height+"; "+buffer.remaining());
		
		//if (isTexture)
		//	System.out.println("adding: "+hash+": "+pixels);
		
		int x = offsetX;
		int y = offsetY;
		
		if (update && offsets != -1) {
			x = BitwiseHelper.long2IntA(offsets);
			y = BitwiseHelper.long2IntB(offsets);
		} else {
			//x += (offsetX / 128) * pixelBorder;
			//y += (offsetY / 128) * pixelBorder;
			
			//System.out.println("plus: "+offsetX+", "+x+", "+((offsetX / 128)));
		}
		
		//width += pixelBorder*2;
		//height += pixelBorder*2;
		//pixelBorder = 0;
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addTexture1(pos: " + x + "/" + y + ", bounds:" + width + "/" + height + ", hash: " + hash + ", full: " + isFull + ", offsets: " + offsetX + "/" + offsetY + ", buffer: " + (buffer.remaining()) + ", update: " + update + ", pixels: " + pixels + ", invisible: " + invisible + ")");
		}
		
		// Send texel data to OpenGL
		glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, initBuffer);
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addTexture2(pos: " + x + "/" + y + ", bounds:" + width + "/" + height + ", hash: " + hash + ", full: " + isFull + ", offsets: " + offsetX + "/" + offsetY + ", buffer: " + (buffer.remaining()) + ", update: " + update + ", pixels: " + pixels + ", invisible: " + invisible + ")");
		}
		
		if (isTexture && id > 0 && ModelConfig.animatedTexture[id]) {
			// TODO: fix a bug here!
			//System.out.println("adding: "+id+": "+x+"/"+y+"; "+offsets);
			
			// only 1 spot left in this row. just jump to the next
			// row and add the 2 textures there.
			while (offsetX + elementSize * 2 >= size) {
				//System.out.println("incrementing");
				increment(isTexture, 0);
			}
			
			if (update) {
				glTexSubImage2D(GL_TEXTURE_2D, 0, x - elementSize, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
				if (!Client.disableOpenGlErrors) {
					GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addAnimatedTexture1_1(pos: " + x + "/" + y + ", bounds:" + width + "/" + height + ", hash: " + hash + ", full: " + isFull + ", offsets: " + offsetX + "/" + offsetY + ", buffer: " + (buffer.remaining()) + ", update: " + update + ", pixels: " + pixels + ", invisible: " + invisible + ")");
				}
				glTexSubImage2D(GL_TEXTURE_2D, 0, x + elementSize, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
				if (!Client.disableOpenGlErrors) {
					GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addAnimatedTexture1_2(pos: " + x + "/" + y + ", bounds:" + width + "/" + height + ", hash: " + hash + ", full: " + isFull + ", offsets: " + offsetX + "/" + offsetY + ", buffer: " + (buffer.remaining()) + ", update: " + update + ", pixels: " + pixels + ", invisible: " + invisible + ")");
				}
			} else {
				glTexSubImage2D(GL_TEXTURE_2D, 0, x + elementSize, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
				if (!Client.disableOpenGlErrors) {
					GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addAnimatedTexture2_1(pos: " + x + "/" + y + ", bounds:" + width + "/" + height + ", hash: " + hash + ", full: " + isFull + ", offsets: " + offsetX + "/" + offsetY + ", buffer: " + (buffer.remaining()) + ", update: " + update + ", pixels: " + pixels + ", invisible: " + invisible + ")");
				}
				glTexSubImage2D(GL_TEXTURE_2D, 0, x + elementSize * 2, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
				if (!Client.disableOpenGlErrors) {
					GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addAnimatedTexture2_2(pos: " + x + "/" + y + ", bounds:" + width + "/" + height + ", hash: " + hash + ", full: " + isFull + ", offsets: " + offsetX + "/" + offsetY + ", buffer: " + (buffer.remaining()) + ", update: " + update + ", pixels: " + pixels + ", invisible: " + invisible + ")");
				}
	
				pointers.put(hash, BitwiseHelper.ints2Long(x + elementSize, y));
				hash = -hash;
				offsetX += elementSize * 2;
			}
			
		}
		
		// add texture to pointers map
		if (offsets == -1) {
			offsets = BitwiseHelper.ints2Long(x, y);
			pointers.put(hash, offsets);
			
			//System.out.println("putting texture: "+hash);
			// increment offsets
			// smaller or unequal letter bounds throw an 1281 error! I assume that
			// it causes fragmentation, if the texture sizes are not equal
			
			increment(isTexture, 0);
		}
		
		// everything fine?
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addTexture3(pos: " + x + "/" + y + ", bounds:" + width + "/" + height + ", hash: " + hash + ", full: " + isFull + ", offsets: " + offsetX + "/" + offsetY + ", buffer: " + (buffer.remaining()) + ", update: " + update + ", pixels: " + pixels + ", invisible: " + invisible + ")");
		}

		return offsets;
		
	}

	public long addLetter(long hash, byte[] letter, int width, int height, boolean update) {
		// letter already in atlas?
		long value = pointers.get(hash);
		long offsets = value != 0 ? value : -1;
		if (!update && offsets != -1)
			return offsets;
		
		if (isFull) {
			System.err.println("[TextureAtlas]: too large letter object: " + hash + ", " + letter.length + "/" + totalSize);
			return -1;
		}
		
		// bind the letter buffer, or create it if it doesn't exist yet
		bindBuffer(false);
		
		for (int y = height - 1; y >= 0; y--) {
			int offY = y * width;
			for (int x = 0; x < width; x++) {
				byte pixel = letter[x + offY];
				// we can set the color here
				
				// ignore black pixels around letters
				if (pixel == 0) {
					buffer.put((byte) 0); // R
					buffer.put((byte) 0); // G
					buffer.put((byte) 0); // B
					buffer.put((byte) 0);
				}
				
				// this is the letter data
				else {
					buffer.put((byte) 255); // R
					buffer.put((byte) 255); // G
					buffer.put((byte) 255); // B
					buffer.put((byte) 255); // A
				}
			}
		}
		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS
		
		int x = offsetX;
		int y = offsetY;
		
		if (update && offsets != -1) {
			x = BitwiseHelper.long2IntA(offsets);
			y = BitwiseHelper.long2IntB(offsets);
		}
		
		// Send texel data to OpenGL
		glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		
		// add texture to pointers map
		if (offsets == -1) {
			offsets = BitwiseHelper.ints2Long(x, y);
			pointers.put(hash, offsets);
			
			// increment offsets
			// smaller or unequal letter bounds throw an 1281 error! I assume that
			// it causes fragmentation, if the texture sizes are not equal
			increment(false, 0);
		}
		
		// everything fine?
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addLetter(" + width + ", " + height + ")");
		}
		
		return offsets;
	}
	
	// some textures must be the first in the atlas
	// this method ensures that they are added first
	private void initTextures() {
		//System.out.println("init textures");
		
		for (int id = 1; id < ModelConfig.animatedTexture.length; id++)
			if (ModelConfig.animatedTexture[id]) {
				//System.out.println("adding animated texture: "+id);
				addTexture(id, null, 255, 128, 128, false, true, id, false);
				// id, pixels, alpha, width, height, update, texture, id, floor
			}
	}
	
	public void delete(boolean clear) {
		// complete reset, atlas is being removed
		// from GPU entirely.
		if (clear) {
			if (textureID != 0) {
				GLBufferManager.deleteTextureBuffer(GLBufferManager.TEXTURE_ATLAS, textureID);
				ATLAS_COUNT--;
				textureID = 0;
			}
		}
		
		// soft reset, atlas is re-used to write data
		offsetX = 0;
		offsetY = 0;
		pointers.clear();
		space = maxSpace;
		isFull = false;
		
		//if (texturesOnly && atlasListId == 0)
		//	this.initTextures();
	}
	
	public long addBox(long hash) {
		// letter already in atlas?
		long value = pointers.get(hash);
		long offsets = value != 0 ? value : -1;
		if (offsets != -1)
			return offsets;

		int width = elementSize;
		int height = elementSize;

		if (isFull) {
			System.err.println("[TextureAtlas]: too large box object: " + hash + ": " + width + ", " + height);
			return -1;
		}

		// bind the letter buffer, or create it if it doesn't exist yet
		bindBuffer(false);
		
		for (int y = height - 1; y >= 0; y--) {
			for (int x = 0; x < width; x++) {
				buffer.put((byte) 255); // R
				buffer.put((byte) 255); // G
				buffer.put((byte) 255); // B
			}
		}
		buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS
		
		// Send texel data to OpenGL
		glTexSubImage2D(GL_TEXTURE_2D, 0, offsetX, offsetY, width, height, GL_RGB, GL_UNSIGNED_BYTE, buffer);
		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		
		// add texture to pointers map
		if (offsets == -1) {
			offsets = BitwiseHelper.ints2Long(offsetX, offsetY);
			pointers.put(hash, offsets);
			
			// increment offsets
			// smaller or unequal letter bounds throw an 1281 error! I assume that
			// it causes fragmentation, if the texture sizes are not equal
			increment(false, 0);
		}
		
		// everything fine?
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in TextureAtlas.addBox(" + width + ", " + height + ")");
		}
		
		return offsets;
	}

	public int getAtlasListId() {
		return atlasListId;
	}
	
}
