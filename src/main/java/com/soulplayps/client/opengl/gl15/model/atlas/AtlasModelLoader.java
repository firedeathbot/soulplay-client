package com.soulplayps.client.opengl.gl15.model.atlas;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.Ground;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.object.WorldController;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.AssetLoader;
import com.soulplayps.client.opengl.gl15.model.ModelConfig;
import com.soulplayps.client.opengl.gl15.model.ModelVBO;
import com.soulplayps.client.opengl.gl15.model.TextureAtlas;
import com.soulplayps.client.opengl.model.DrawableArea;
import com.soulplayps.client.opengl.model.DrawableModel;
import com.soulplayps.client.opengl.util.Cache;
import com.soulplayps.client.opengl.util.GLBufferManager;
import com.soulplayps.client.opengl.util.VBOManager;
import com.soulplayps.client.unknown.Class40;
import com.soulplayps.client.unknown.Class43;
import com.soulplayps.client.util.BitwiseHelper;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongList;
import org.lwjgl.opengl.GL15;

public class AtlasModelLoader extends AssetLoader {
	
	// clamps texture to work at texture atlasses.
	// the problem is precision of UV calculation, so we
	// increase/decrease the coordinates to avoid bleeding.
	private static double clamp(double uv, int texture, boolean sClamp, boolean isFloor) {
		/*if (isFloor)
		{
			if (uv > 0.99)
				uv = 0.99;
			else if (uv < 0.01)
				uv = 0.01;
			
			return uv;
		}*/
		
		if (isFloor)
			return uv;
		
		double fac = 0.005;
		
		if (texture > 0 && ModelConfig.flipUVTexture[texture])
			fac = 0.01;
			
			// old texture on roofs
		else if (texture > 0 && texture < Rasterizer.textureAmount && !ModelConfig.animatedTexture[texture])
			fac = 0.05;
			
			// default texture for non-UV models
		else if (texture == 91)
			fac = 0.05;
		
		if (sClamp || texture == DEFAULT_TEXTURE || texture <= 0)
			fac = 0.25;
		
		if (uv >= 0.5)
			uv -= fac;
		else
			uv += fac;
		
		/*if (texture == 40) {
			System.out.println(uv + ", " + fac);
		}*/
		
		return uv;
	}
	
	
	private void addUV(TextureAtlas atlas, int texture, float u1, float v1, float u2, float v2, float u3, float v3, boolean isFloor) {
		addUV(atlas, texture, u1, v1, u2, v2, u3, v3, isFloor, false);
	}
	
	private void addUV(TextureAtlas atlas, int texture, double u1, double v1, double u2, double v2, double u3, double v3, boolean isFloor, boolean sClamp) {
		// DON'T CHANGE TEXTURES HERE!!!!
		//if (texture <= 0)
		//	texture = DEFAULT_TEXTURE;
		
		if (atlas != null) {
			// textures have equal width and height, we don't have
			// to destinguish for that when dividing the u and v coords
			final double width = 128f;
			final double height = 128f;
			
			// length and width are the same size, so
			// the size must be divided by 2
			final double dsize = atlas.getMaxCapacity() / 2;
			
			double x = 0;
			double y = 0;
			
			final long offsets = atlas.getOffsets(texture);
			
			//offsets = new int[]{256, 128};
			if (offsets != -1) {
				// position in atlas grid of the texture
				x = (BitwiseHelper.long2IntA(offsets) + 0.5) / dsize / width;
				y = (BitwiseHelper.long2IntB(offsets) + 0.5) / dsize / height;
			}/* else {
				if (Math.random() < 0.01)
					System.err.println("ERROR - offsets null for "+(!isFloor ? "model" : "floor")+" texture: "+texture);
			}*/
			
			
			//size /= 4;
			
			// scale to atlas space
			u1 = clamp(u1, texture, sClamp, isFloor) / dsize;
			u2 = clamp(u2, texture, sClamp, isFloor) / dsize;
			u3 = clamp(u3, texture, sClamp, isFloor) / dsize;
			v1 = clamp(v1, texture, sClamp, isFloor) / dsize;
			v2 = clamp(v2, texture, sClamp, isFloor) / dsize;
			v3 = clamp(v3, texture, sClamp, isFloor) / dsize;
			
			/*if (false && x != 0 && y != 0 && Math.random() < 0.001) {
				System.out.println("xy: " + x + "/" + y + "; " + BitwiseHelper.long2IntA(offsets)
						+ ", " + BitwiseHelper.long2IntB(offsets) + ", " + dsize);
				System.out.println("uv: " + u1 + "/" + v1 + ", " + u2 + "/" + v2 + ", " + u3 + "/" + v3);
				System.out.println("");
			}*/
			
			// add atlas position
			u1 += x;
			u2 += x;
			u3 += x;
			v1 += y;
			v2 += y;
			v3 += y;
			
			uvBuffer.put((float) u1);
			uvBuffer.put((float) v1);
			uvBuffer.put((float) u2);
			uvBuffer.put((float) v2);
			uvBuffer.put((float) u3);
			uvBuffer.put((float) v3);
		} else {
			new Throwable("[AtlasModelLoader]: atlas null for " + (isFloor ? "floor" : "model") + " texture: " + texture).printStackTrace();
			uvBuffer.put(0);
			uvBuffer.put(0);
			uvBuffer.put(0);
			uvBuffer.put(0);
			uvBuffer.put(0);
			uvBuffer.put(0);
		}
	}
	
	private static int floorHash(int x, int y, int height) {
		return (height << 24) + (x << 16) + (y << 8);
	}
	
	
	private TextureAtlas atlas = null;
	private int lastTexture = 0;
	private int texture = 0;
	private int trianglesCounted = 0;
	private int totalTrianglesCounted = 0;
	private int lastAtlas = 0;
	private LongList textureMappings = null;
	
	/*private void checkAtlas(boolean isFloor)
	{
		if (lastTexture != texture)
		{
			TextureAtlas newAtlas = Cache.getTextureAtlas(texture, isFloor);
			
			// new or different atlas
			if (lastAtlas != 0 && lastAtlas != newAtlas.getTextureID())
			{
				textureMappings.add(new int[]{texture > 0 ? atlas.getTextureID() : 0, trianglesCounted});
				trianglesCounted = 0;
			}
			lastAtlas = newAtlas.getTextureID();
			atlas = newAtlas;
			
			lastTexture = texture;
		}
	}*/
	
	private void clearValues(boolean isFloor, IntList textures) {
		if (textures != null)
			atlas = Cache.loadAtlasTextures(isFloor, textures);
		else
			atlas = null;
		
		lastTexture = -1;
		texture = -1;
		trianglesCounted = 0;
		lastAtlas = 0;
		
		// we require a new list object per model
		//textureMappings = new LinkedList<int[]>();
	}
	
	public ModelVBO loadFloor(int height, WorldController wc, int startX, int startY, int tileSize) {
		if (wc.anIntArrayArrayArray440 == null) {
			System.err.println("[AtlasModelLoader]: heights null");
			return null;
		}
		/*if (true)
		{
			// TODO: bleeding issue for texture atlas
			return null;
		}*/
		
		clearBuffers();
		
		long uid = floorHash(height, startX, startY);
		
		ModelVBO vbo = Cache.getFloorVBOs().get(uid);
		
		if (vbo != null)
			return vbo;
		
		Ground groundCoordinates[][] = wc.groundArray[height];
		
		// iterate once to count triangles to prepare buffers
		
		int endX = startX + tileSize;
		if (endX > 104)
			endX = 104;
		
		int endY = startY + tileSize;
		if (endY > 104)
			endY = 104;
		
		
		boolean updateMappings = true;
		clearValues(true, null);
		textureMappings = new LongArrayList(Texture.COUNT);
		
		boolean hasUV = false;
		
		for (int x = startX; x < endX; x++) {
			for (int y = startY; y < endY; y++) {
				boolean groundLoop = true;
				for (int i = 0; i < 2 && groundLoop; i++) {
					Ground g = groundCoordinates[x][y];
					if (g == null)
						break;
					else if (i == 1)
						g = g.aClass30_Sub3_1329;
					
					if (g == null)
						break;
					
					// to allow transparancy on the textures!
					int alpha = 255;
					
					if (g.aClass43_1311 != null) {
						Class43 tile = g.aClass43_1311;
						
						int rgb0 = Rasterizer.anIntArray1482[tile.colorTL & 0xFFFF]; // top left
						int rgb1 = Rasterizer.anIntArray1482[tile.colorBL & 0xFFFF]; // bottom left
						int rgb2 = Rasterizer.anIntArray1482[tile.colorTR & 0xFFFF]; // top right
						int rgb3 = Rasterizer.anIntArray1482[tile.colorBR & 0xFFFF]; // bottom right
						
						if (tile.colorTL > 0xFFFF || tile.colorBL > 0xFFFF || tile.colorTR > 0xFFFF || tile.colorBR > 0xFFFF)
							continue;
						else if (tile.colorTL < 0 || tile.colorBL < 0 || tile.colorTR < 0 || tile.colorBR < 0)
							continue;
						
						int gx = x << 7;
						int gy = y << 7;
						int size = 128;
						texture = tile.groundTexture;
						if (texture == -1)
							texture = Rasterizer.DEFAULT_GROUND_TEXTURE;
						if (!Client.enableHDTextures)
							texture = -1;
						
						// atlasses have to be loaded, because it's the first
						// time we're using this model
						if (updateMappings) {
							// texture has changed, may also change atlas
							// or we're here the first time and have to
							// set the initial atlas
							if (texture != lastTexture || atlas == null) {
								// no atlas needed for texture -1
								//boolean update = texture <= 0;
								
								//System.out.println("loading texture: "+texture);
								
								// atlas is null, first time loading
								if (atlas == null)
									atlas = Cache.groundTextureAtlas128.addTexture(true, texture);
								
								// new texture arrived, check if it's in the same or in a new atlas
								TextureAtlas newAtlas = Cache.groundTextureAtlas128.addTexture(true, texture);
								
								//System.out.println("new texture: "+lastAtlas+", "+atlas.getAtlasListId()+", "+newAtlas.getAtlasListId());
								
								// add triangles in a bulk, only if the atlas changes
								if (newAtlas.getAtlasListId() != atlas.getAtlasListId()) {
									int tex = texture <= 0 ? 0 : atlas.getTextureID2();
									textureMappings.add(BitwiseHelper.ints2Long(tex, trianglesCounted));
									trianglesCounted = 0;
									
									// new atlas for UV calculations
									atlas = newAtlas;
								}
								
								lastTexture = texture;
							}
						}
						
						float px0 = gx;
						float px1 = gx + size;
						
						float pz0 = gy;
						float pz1 = gy + size;
						
						int h = height;
						if (i == 0 && g.aClass30_Sub3_1329 != null && h < 3)
							h++;
						
						// ground has its own height value
						h = g.groundHeight;
						
						float py0 = wc.anIntArrayArrayArray440[h][x][y + 1]; // top left
						float py1 = wc.anIntArrayArrayArray440[h][x][y]; // bottom left
						float py2 = wc.anIntArrayArrayArray440[h][x + 1][y + 1]; // top right
						float py3 = wc.anIntArrayArrayArray440[h][x + 1][y]; // bottom right
						
						// load all triangles with the same texture id, so that we
						// have them after each other in our VBO. that means, that
						// we can draw all 'same' textures in 1 call per VBO
							/*if (countingTexture && newTexture != texture)
							{
								loading = true;
								continue;
							}
							
							// texture was already loaded
							if (textureLoaded.indexOf(newTexture) != -1)
							{
								continue;
							}*/
						
						trianglesCounted += 2;
						totalTrianglesCounted += 2;
						
						int r0 = ((rgb0 >> 16) & 0xFF);
						int g0 = ((rgb0 >> 8) & 0xFF);
						int b0 = ((rgb0 >> 0) & 0xFF);
						int a0 = (alpha);
						
						int r1 = ((rgb1 >> 16) & 0xFF);
						int g1 = ((rgb1 >> 8) & 0xFF);
						int b1 = ((rgb1 >> 0) & 0xFF);
						int a1 = (alpha);
						
						int r2 = ((rgb2 >> 16) & 0xFF);
						int g2 = ((rgb2 >> 8) & 0xFF);
						int b2 = ((rgb2 >> 0) & 0xFF);
						int a2 = (alpha);
						
						int r3 = ((rgb3 >> 16) & 0xFF);
						int g3 = ((rgb3 >> 8) & 0xFF);
						int b3 = ((rgb3 >> 0) & 0xFF);
						int a3 = (alpha);
						
						// vertex 1
						vertexBuffer.put(px1);
						vertexBuffer.put(py2);
						vertexBuffer.put(pz1);
						vertexBuffer.put(px0);
						vertexBuffer.put(py0);
						vertexBuffer.put(pz1);
						vertexBuffer.put(px0);
						vertexBuffer.put(py1);
						vertexBuffer.put(pz0);
						
						// vertex 2
						vertexBuffer.put(px1);
						vertexBuffer.put(py2);
						vertexBuffer.put(pz1);
						vertexBuffer.put(px0);
						vertexBuffer.put(py1);
						vertexBuffer.put(pz0);
						vertexBuffer.put(px1);
						vertexBuffer.put(py3);
						vertexBuffer.put(pz0);
						
						// color1
						colorBuffer.put((byte) r2);
						colorBuffer.put((byte) g2);
						colorBuffer.put((byte) b2);
						colorBuffer.put((byte) a2);
						colorBuffer.put((byte) r0);
						colorBuffer.put((byte) g0);
						colorBuffer.put((byte) b0);
						colorBuffer.put((byte) a0);
						colorBuffer.put((byte) r1);
						colorBuffer.put((byte) g1);
						colorBuffer.put((byte) b1);
						colorBuffer.put((byte) a1);
						
						// color 2
						colorBuffer.put((byte) r2);
						colorBuffer.put((byte) g2);
						colorBuffer.put((byte) b2);
						colorBuffer.put((byte) a2);
						colorBuffer.put((byte) r1);
						colorBuffer.put((byte) g1);
						colorBuffer.put((byte) b1);
						colorBuffer.put((byte) a1);
						colorBuffer.put((byte) r3);
						colorBuffer.put((byte) g3);
						colorBuffer.put((byte) b3);
						colorBuffer.put((byte) a3);
						
						addUV(atlas, texture, 1, 0, 0, 0, 0, 1, true);
						addUV(atlas, texture, 1, 0, 0, 1, 1, 1, true);
						
						hasUV = true;
						
						//System.out.println("st v: "+px0+", "+py0+", "+pz0);
					}
					
					if (g.aClass40_1312 != null) {
						Class40 tile = g.aClass40_1312;
						for (int v = 0; v < tile.anIntArray679.length; v++) {
							if (tile.color1[v] > 0xFFFF || tile.color2[v] > 0xFFFF || tile.color3[v] > 0xFFFF)
								continue;
							else if (tile.color1[v] < 0 || tile.color2[v] < 0 || tile.color3[v] < 0)
								continue;
							
							int rgb0 = Rasterizer.anIntArray1482[tile.color1[v] & 0xFFFF];
							int rgb1 = Rasterizer.anIntArray1482[tile.color2[v] & 0xFFFF];
							int rgb2 = Rasterizer.anIntArray1482[tile.color3[v] & 0xFFFF];
							
							texture = tile.groundTextures == null ? -1 : tile.groundTextures[v];
							if (texture == -1)
								texture = Rasterizer.DEFAULT_GROUND_TEXTURE;
							if (!Client.enableHDTextures)
								texture = -1;
							
							
							// atlasses have to be loaded, because it's the first
							// time we're using this model
							if (updateMappings) {
								// texture has changed, may also change atlas
								// or we're here the first time and have to
								// set the initial atlas
								if (texture != lastTexture || atlas == null) {
									// no atlas needed for texture -1
									//boolean update = texture <= 0;
									
									//System.out.println("loading texture: "+texture);
									
									// atlas is null, first time loading
									if (atlas == null)
										atlas = Cache.groundTextureAtlas128.addTexture(true, texture);
									
									// new texture arrived, check if it's in the same or in a new atlas
									TextureAtlas newAtlas = Cache.groundTextureAtlas128.addTexture(true, texture);
									
									//System.out.println("new texture: "+lastAtlas+", "+atlas.getAtlasListId()+", "+newAtlas.getAtlasListId());
									
									// add triangles in a bulk, only if the atlas changes
									if (newAtlas.getAtlasListId() != atlas.getAtlasListId()) {
										int tex = texture <= 0 ? 0 : atlas.getTextureID2();
										textureMappings.add(BitwiseHelper.ints2Long(tex, trianglesCounted));
										trianglesCounted = 0;
										
										// new atlas for UV calculations
										atlas = newAtlas;
									}
									
									lastTexture = texture;
								}
							}
							
							int pt0 = tile.anIntArray679[v];
							int pt1 = tile.anIntArray680[v];
							int pt2 = tile.anIntArray681[v];
							
							float px0 = tile.anIntArray673[pt0];
							float py0 = tile.anIntArray674[pt0];
							float pz0 = tile.anIntArray675[pt0];
							
							float px1 = tile.anIntArray673[pt1];
							float py1 = tile.anIntArray674[pt1];
							float pz1 = tile.anIntArray675[pt1];
							
							float py2 = tile.anIntArray674[pt2];
							float px2 = tile.anIntArray673[pt2];
							float pz2 = tile.anIntArray675[pt2];
							
							// load all triangles with the same texture id, so that we
							// have them after each other in our VBO. that means, that
							// we can draw all 'same' textures in 1 call per VBO
								/*if (countingTexture && newTexture != texture)
								{
									loading = true;
									continue;
								}
								
								// texture was already loaded
								if (textureLoaded.indexOf(newTexture) != -1)
								{
									continue;
								}*/
							
							trianglesCounted++;
							totalTrianglesCounted++;
							
							int r0 = ((rgb0 >> 16) & 0xFF);
							int g0 = ((rgb0 >> 8) & 0xFF);
							int b0 = ((rgb0 >> 0) & 0xFF);
							int a0 = (alpha);
							
							int r1 = ((rgb1 >> 16) & 0xFF);
							int g1 = ((rgb1 >> 8) & 0xFF);
							int b1 = ((rgb1 >> 0) & 0xFF);
							int a1 = (alpha);
							
							int r2 = ((rgb2 >> 16) & 0xFF);
							int g2 = ((rgb2 >> 8) & 0xFF);
							int b2 = ((rgb2 >> 0) & 0xFF);
							int a2 = (alpha);
							
							// vertex
							vertexBuffer.put(px0);
							vertexBuffer.put(py0);
							vertexBuffer.put(pz0);
							vertexBuffer.put(px1);
							vertexBuffer.put(py1);
							vertexBuffer.put(pz1);
							vertexBuffer.put(px2);
							vertexBuffer.put(py2);
							vertexBuffer.put(pz2);
							
							// color
							colorBuffer.put((byte) r0);
							colorBuffer.put((byte) g0);
							colorBuffer.put((byte) b0);
							colorBuffer.put((byte) a0);
							colorBuffer.put((byte) r1);
							colorBuffer.put((byte) g1);
							colorBuffer.put((byte) b1);
							colorBuffer.put((byte) a1);
							colorBuffer.put((byte) r2);
							colorBuffer.put((byte) g2);
							colorBuffer.put((byte) b2);
							colorBuffer.put((byte) a2);
							
							final int pI = 0;
							final int mI = 1;
							final int nI = 3;
							
							// y = 0: proper normal calculation for floor
							
							final float px = tile.anIntArray673[pI];
							final float py = 0;
							final float pz = tile.anIntArray675[pI];
							
							final float mx = tile.anIntArray673[mI] - px;
							final float my = 0 - py;
							final float mz = tile.anIntArray675[mI] - pz;
							final float nx = tile.anIntArray673[nI] - px;
							final float ny = 0 - py;
							final float nz = tile.anIntArray675[nI] - pz;
							
							final float xx = tile.anIntArray673[pt0] - px;
							final float yx = 0 - py;
							final float zx = tile.anIntArray675[pt0] - pz;
							final float xy = tile.anIntArray673[pt1] - px;
							final float yy = 0 - py;
							final float zy = tile.anIntArray675[pt1] - pz;
							final float xz = tile.anIntArray673[pt2] - px;
							final float yz = 0 - py;
							final float zz = tile.anIntArray675[pt2] - pz;
							
							final float mnyz = my * nz - mz * ny;
							final float mnxz = mz * nx - mx * nz;
							final float mnxy = mx * ny - my * nx;
							float mn1 = ny * mnxy - nz * mnxz;
							float mn2 = nz * mnyz - nx * mnxy;
							float mn3 = nx * mnxz - ny * mnyz;
							
							float inv = 1f / (mn1 * mx + mn2 * my + mn3 * mz);
							
							float u1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
							float u2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
							float u3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
							
							mn1 = my * mnxy - mz * mnxz;
							mn2 = mz * mnyz - mx * mnxy;
							mn3 = mx * mnxz - my * mnyz;
							
							inv = 1.0F / (mn1 * nx + mn2 * ny + mn3 * nz);
							
							float v1 = (mn1 * xx + mn2 * yx + mn3 * zx) * inv;
							float v2 = (mn1 * xy + mn2 * yy + mn3 * zy) * inv;
							float v3 = (mn1 * xz + mn2 * yz + mn3 * zz) * inv;
							
							if (u1 < 0) u1 = 0;
							if (u1 > 1) u1 = 1;
							if (u2 < 0) u2 = 0;
							if (u2 > 1) u2 = 1;
							if (u3 < 0) u3 = 0;
							if (u3 > 1) u3 = 1;
							
							if (v1 < 0) v1 = 0;
							if (v1 > 1) v1 = 1;
							if (v2 < 0) v2 = 0;
							if (v2 > 1) v2 = 1;
							if (v3 < 0) v3 = 0;
							if (v3 > 1) v3 = 1;
							
							addUV(atlas, texture, u1, 1 - v1, u2, 1 - v2, u3, 1 - v3, true);
							
							hasUV = true;
						}
					}
				}
			}
		}
		
		
		vertexBuffer.flip();
		colorBuffer.flip();
		uvBuffer.flip();
		
		int BUFFER_TYPE = GL15.GL_STATIC_DRAW;
		
		int vertexHandle = 0;
		int colorHandle = 0;
		int uvHandle = 0;
		
		if (vbo == null) {
			int size = 2;
			if (hasUV)
				size = 3;
			
			final int[] bufs = GLBufferManager.genBuffers(size);
			vertexHandle = bufs[0];
			colorHandle = bufs[1];
			if (hasUV)
				uvHandle = bufs[2];
		} else {
			vertexHandle = vbo.getVertexHandle();
			colorHandle = vbo.getColorHandle();
			uvHandle = vbo.getUvHandle();
		}
		
		
		writeHandles(vertexHandle, colorHandle, uvHandle, hasUV, BUFFER_TYPE);
		
		
		// atlas not null? add last mapping
		//atlas = Cache.getTextureAtlas(lastTexture, true);
		
		
		if (updateMappings) {
			int tex = texture <= 0 ? 0 : atlas.getTextureID2();
			textureMappings.add(BitwiseHelper.ints2Long(tex, trianglesCounted));
			trianglesCounted = 0;
		}
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in loadAtlasFloor(" + uid + ", " + vertexHandle + ", " + colorHandle + ")");
		}

		// register VBO
		if (vbo == null) {
			vbo = new ModelVBO(startX, startY, true);
			vbo.isTextureID = false;
			Cache.addFloorVBO(uid, vbo);
		}
		vbo.update(vertexHandle, colorHandle, uvHandle, totalTrianglesCounted, uid, false, -1, false, textureMappings);
		
		totalTrianglesCounted = 0;
		
		return vbo;
	}
	
	private static int DEFAULT_TEXTURE = 225;
	
	// we render map decorations in large buffers
	// that increases the overall performance
	private Long2ObjectMap<DrawableArea> mapDecoration = new Long2ObjectOpenHashMap<DrawableArea>();
	private Long2ObjectMap<DrawableArea> mapDecorationRemovals = new Long2ObjectOpenHashMap<DrawableArea>();
	
	public void clearMapDecorations() {
		for (Long2ObjectMap.Entry<DrawableArea> entry : mapDecoration.long2ObjectEntrySet())
			clearArea(entry.getLongKey(), entry.getValue());
		
		mapDecoration.clear();
		mapDecorationRemovals.clear();
	}
	
	private void clearArea(long uid, DrawableArea area) {
		Cache.clearModel(uid);
		for (DrawableModel model : area.getModels()) {
			Cache.clearModel(model.getModelUid());
			//model.getModel().clearArrays();
			model.clear();
		}
		
		area.clear();
	}
	
	public void organizeMapDecorations() {
		/*if (Math.random() < 0.01) {
			System.out.println("decos: "+mapDecoration.size());
			clearMapDecorations();
		}*/
		
		modelBuffersCreated = 0;
		floorBuffersCreated = 0;
		multiModelBuffersCreated = 0;
		
		final boolean isLoading = Client.loadingStage != 2;
		maxModelBuffersPerFrame = isLoading ? 21 : 42;
		maxFloorBuffersPerFrame = isLoading ? 10 : 20;
		maxMultiModelBuffersPerFrame = GraphicsDisplay.multiModelBuffers ? 3 : 6;
		
		final long now = System.currentTimeMillis();
		
		// TODO: find a good value for that
		int updated = 0;
		int maxUpdated = 5000;
		
		for (final Long2ObjectMap.Entry<DrawableArea> entry : mapDecoration.long2ObjectEntrySet()) {
			final long uid = entry.getLongKey();
			final DrawableArea area = entry.getValue();
			boolean render = true;
			
			ModelVBO vbo = Cache.getModelVBOs().get(uid);
			final int total = area.getModels().size();
			
			// draw roofs that are casting shadows. (correct shadows inside houses)
			if (Client.useShadows && !area.isOneSeen()) {
				final int distance = Client.distanceToMeLocal(area.getX() + 1 + area.getDX() / 128, area.getY() + 1 + area.getDY() / 128);
				if (vbo != null && distance <= 20)
					VBOManager.addModelShadow(uid, area.getDX(), area.getDY(), 0, 0, 0, 0, vbo, 0, 0, 0);
			}
			
			// skip not seen parts
			if (!area.isOneSeen() || area.getModels().size() <= 0)
				render = false;
			
			// buffer requires an update
			if (vbo != null && area.getModels().size() != area.getLastAmount())
				area.update(now);
			
			// TODO: handled in Model class and WorldController!!
			// roofs removal
			//else if (!area.areAllSeen() && area.getHeight() != Client.plane)
			//	render = false;
			
			
			// area must be refreshed
			if (area.mustClear())
				render = false;
			
			if (!render) {
				// buffer is dirty, clear required
				if (area.mustClear())
					mapDecorationRemovals.put(uid, area);
				
				// don't do area checks when loading
				if (Client.loadingStage != 2)
					continue;
				
				// no longer in local area (104x104 tile)
				if (!Client.inLocalArea(area.getWorldX(), area.getWorldY()))
					mapDecorationRemovals.put(uid, area);
				
				// buffer too far away
				final int d = Client.distanceToMe(area.getWorldX(), area.getWorldY());
				final int md = (WorldController.viewDistance / 2) + 8;
				if (d > md)
					mapDecorationRemovals.put(uid, area);
				
				continue;
			}
			
			//if (area.getHeight() == 0)
			//	continue;
			
			clearBuffers();
			clearValues(false, null);
			totalTrianglesCounted = 0;
			int triangles = 0;
			
			//textureMappings = new LinkedList<int[]>();
			
			boolean last = false;
			int counted = 0;
			
			//area.update(now);
			area.setLastSeen(now);
			
			// TODO: is this area.getLastUpdate() check required for performance?
			// it definitely keeps the fps rate high, at the cost of fps inbreaks
			//final long timer = area.getHeight() > Client.clientHeight ? 100 : 0;
			final boolean isUpdate = now - area.getLastUpdate() > 2000 && (vbo == null || area.requiresUpdate());
			//isUpdate = true;
			
			if (isUpdate) {
				if (updated++ >= maxUpdated)
					continue;
				
				textureMappings.clear();
				//textureMappings = new LongArrayList(Texture.COUNT);
				
				//System.out.println("models["+uid+"]: "+area.getModels().size());
				for (DrawableModel model : area.getModels()) {
					if (model == null) continue;
					
					Model vboModel = model.getModel();
					if (vboModel == null/* || vboModel.triangleCount < 5*/) continue;
					
					try {
						counted++;
						if (counted == total)
							last = true;
						
						// simulate hardware rotation
						int face = 0;//odel.getModel().getFace();
						if (face != 0) {
							face &= 0x3;
							if (face == 1) {
								model.getModel().rotate90();
							} else if (face == 2) {
								model.getModel().rotate180();
							} else if (face == 3) {
								model.getModel().rotate270();
							}
							//model.getModel().setFace(0);
						}
						
						if (totalTrianglesCounted != 0)
							triangles = totalTrianglesCounted;
						
						//System.out.println("Loading...");
						vbo = loadModel(model, false, uid, last, false);
						
						if (!Client.disableOpenGlErrors && GraphicsDisplay.getInstance().exitOnGLError("error in organizeMapDecorations(" + uid + ", " + area.getModels().size() + ")")) {
							clearArea(uid, area);
							if (vbo != null)
								vbo.clear = true;
							
							break;
						}
						
					} catch (Exception e) {
						System.err.println("triangles[" + area + "]: " + triangles);
						System.err.println("update[" + area + "]: " + area.getModels().size() + ": " + area.getModels().toString().replaceAll("]], D", "]]\nD"));
						area.getModels().clear();
						e.printStackTrace();
						//break;
					}
				}
				
				area.updated(now);
			}
			
			// tell the area it was drawn (clears seen booleans)
			area.drawn();
			
			//System.out.println("vbo["+uid+"]: "+vbo.getTextures().size()+", "+vbo.getTriangles());
			
			if (vbo != null) {
				vbo.isAreaBuffer = true;
				VBOManager.addModel(uid, area.getDX(), area.getDY(), 0, 0, 0, 0, vbo, 0, 0, 0);
				
				if (Client.useShadows)
					VBOManager.addModelShadow(uid, area.getDX(), area.getDY(), 0, 0, 0, 0, vbo, 0, 0, 0);
			}
		}
		
		//System.out.println("rem: "+mapDecorationRemovals.size());
		// areas that need to be removed (too far away or timeout)
		for (Long2ObjectMap.Entry<DrawableArea> entry : mapDecorationRemovals.long2ObjectEntrySet()) {
			//System.out.println("removing: "+entry.getValue());
			long key = entry.getLongKey();
			clearArea(key, entry.getValue());
			mapDecoration.remove(key);
		}
		mapDecorationRemovals.clear();
		
		// reset buffers
		clearBuffers();
	}
	
	public void updateMapDecorationOffsets(final int dx, final int dy) {
		if (dx == 0 && dy == 0)
			return;
		
		for (final DrawableArea area : mapDecoration.values()) {
			area.offsetMap(-dx, -dy);
		}
	}
	
	// I assume that any even divisor here is bugging the system
	// when taking 4 as divisor it is possible to include map parts out
	// of the current 104x104 tile. when a new map part loads, this tile
	// will be part of the new map area and of the old one. BUT it will still
	// be offsetted back to the old area. that results in the new objects of
	// this buffer appearing at wrong map positions. it's only an assumption,
	// but the issue doesn't seem to occur with values 3 and 5.
	//
	// PROBLEM SOLVED!! it had to do with offsetting map pieces that weren't fully
	// loaded. resulting in new parts not receiving the offset. :)
	private static int AREA_RANGE = 4;
	
	private static long hashPosition(final DrawableModel drawable) {
		final int cx = Client.baseDX != 0 ? Client.baseDX : Client.baseOffsetX;
		final int cy = Client.baseDY != 0 ? Client.baseDY : Client.baseOffsetY;
		
		final int x = ((Client.regionBaseX + (drawable.getX() + 1) - cx) / AREA_RANGE) * AREA_RANGE;
		final int y = ((Client.regionBaseY + (drawable.getY() + 1) - cy) / AREA_RANGE) * AREA_RANGE;
		final int z = drawable.getZ();
		
		//System.out.println("xy: "+drawable.getX()+"/"+drawable.getY()+"/"+drawable.getZ());
		
		return (AREA_RANGE << 54L) + ((z & 0xff) << 40L) + ((x & 0xffff) << 20L) + ((y & 0xffff) << 1L);
	}
	
	// TODO: objects in the stripe of the new region being loaded is dispatched
	// it appears on a wrong position. Idk, maybe position error during map stage loading?
	// REASON: overlapping of areas and map coordinates
	private void addMapDecoration(final DrawableModel drawable) {
		AREA_RANGE = GraphicsDisplay.multiModelBuffers ? 8 : 4;
		
		final long hash = hashPosition(drawable);
		DrawableArea area = mapDecoration.get(hash);
		
		// don't add new models while map is loading
		// why? heights may not be 100% correct for roofs and bridges!!
		final boolean mapLoading = Client.loadingStage != 2;
		//final boolean offsetting = Client.baseOffsetX != 0 || Client.baseOffsetY != 0 || Client.baseDX != 0 || Client.baseDY != 0;
		if ((/*offsetting || */mapLoading) && (area == null || !area.containsModel(drawable))) {
			if (area != null)
				area.drawableSeen(drawable.isSeen());
			return;
		}
		
		if (area == null) {
			if (multiModelBuffersCreated++ >= maxMultiModelBuffersPerFrame)
				return;
			area = new DrawableArea(hash, drawable.getX() + 1, drawable.getY() + 1, drawable.getZ(), AREA_RANGE);
			mapDecoration.put(hash, area);
		}
		
		//int ox = Client.baseX + drawable.getX();
		//int oy = Client.baseY + drawable.getY();
		
		area.drawableSeen(drawable.isSeen());
		area.addModel(drawable);
	}
	
	public ModelVBO loadModel(DrawableModel drawable, boolean interfaceModel) {
		return loadModel(drawable, true, 0, true, interfaceModel);
	}
	
	public int modelBuffersCreated = 0;
	public int floorBuffersCreated = 0;
	public int multiModelBuffersCreated = 0;
	public int maxModelBuffersPerFrame = 84;
	public int maxFloorBuffersPerFrame = 4;
	public int maxMultiModelBuffersPerFrame = 16;
	
	public ModelVBO loadModel(DrawableModel drawable, boolean resetBuffers, long overwriteUid, boolean isLast, boolean interfaceModel) {
		if (drawable == null) {
			return null;
		}

		final Model model = drawable.getModel();

		// model has no triangles?
		if (model == null || model.triangleCount < 1) {
			return null;
		}
		
		//final boolean adjustGround = drawable.isAdjustGround();
		final boolean isMultiBuffer = overwriteUid != 0;
		boolean animated = drawable.isAnimated();
		
		/*final int entityCount = Client.playerCount + Client.npcCount;
		final int distance = drawable.distanceToPlayer();
		final boolean isRotated = drawable.getCoords().getRotationX() == 0;*/
		
		
		// no click options and uniquely adjusted buffers
		// we can throw all those objects into localized
		// VBOs. e.g. 4x4 squares or so.
		
		// done by Jire
		/*if (canBeMultiModelBuffered(drawable, model.triangleCount, drawable.getZ()) && false
				&& resetBuffers && !animated && model.bufferUpdatePossible) {
			// looks like this is working out really well!
			// we have to test it on slower systems still tho
			addMapDecoration(drawable);
			return null;
		}*/
		
		final long uid = overwriteUid == 0 ? drawable.getModelUid() : overwriteUid;
		
		ModelVBO vbo = null;
		int animatedTriangles = 0;
		int animatedTexture = 0;
		
		// clear buffers
		if (resetBuffers) {
			// clear buffers
			clearBuffers();
			
			// fetch model from uid cache
			Long2ObjectMap<ModelVBO> modelVBOs = Cache.getModelVBOs();
			ModelVBO value = modelVBOs.get(uid);
			if (value != null) {
				vbo = value;
			}
		}
		
		// the isUpdate boolean gives a lot of performance!
		// UV coordinates don't have to be calculated another
		// time for animated models --> +10-20% fps
		boolean isUpdate = vbo == null;
		
		// textures require UV coordinate updates
		//if (Client.enableHDTextures)
		//	isUpdate = false;
		
		// model has UV coordinates?
		boolean hasUV = false;
		boolean updateAnimatedTexture = false;
		
		// model triangle count changed --> update it
		// e.g. equipping items at animated models
		if (vbo != null && vbo.getTriangles() != model.triangleCount && resetBuffers)
			isUpdate = true;
		
		//TODO this piece
		if (Client.useAnimatedTextures && vbo != null && vbo.hasAnimatedTextures && !isUpdate) // too big fps killer
		{
			animated = true;
			
			float progress = (Client.loopCycle % model.textureAnimationSpeed) / (float) model.textureAnimationSpeed;
			if (progress != vbo.lastAnimatedTextureProgress) {
				isUpdate = true;
				updateAnimatedTexture = true;
				vbo.lastAnimatedTextureProgress = progress;
			}
		}
		
		// model already loaded
		if (vbo != null && !animated && isLast)
			return vbo;
		
		// maximum load per frame
		if (isLast && !animated && !resetBuffers)
			if (modelBuffersCreated++ >= maxModelBuffersPerFrame)
				return null;
		
		boolean updateMappings = true;
		if (!isMultiBuffer)
			clearValues(false, null);
		
		if (vbo != null) {
			textureMappings = vbo.getTextures();
			if (isUpdate)
				textureMappings.clear();
			else
				updateMappings = false;
			
			//if (isUpdate && textureMappings.size() > 0)
			//	System.out.println("re-use: "+textureMappings.size());
		} else if (!isMultiBuffer) {
			//System.out.println("new list");
			textureMappings = new LongArrayList(Texture.COUNT);
		}
		
		
		if (!resetBuffers) {
			vbo = Cache.getModelVBOs().get(uid);
			isUpdate = true;
		}
		
		//if (!animated)
		//{
		//System.out.println("update: "+isUpdate+", "+animated);
		//}
		//if (!isUpdate)
		//	return null;
		//System.out.println("update: "+isUpdate);
		
		boolean colorUpdate = true;
		boolean hasAlpha = false;
		
		//if (vbo != null && animated && drawable.getEntityType() <= 1)
		//	colorUpdate = false;
		
		totalTrianglesCounted += model.triangleCount;
		
		for (int k = 0; k < model.triangleCount; k++) {
			//int drawType = model.drawTypes != null && k < model.drawTypes.length ? model.drawTypes[k] & 0xFF : 0;
			int rgb0 = 0;
			int rgb1 = 0;
			int rgb2 = 0;
			
			int alpha = model.triangleAlphaValues != null && k < model.triangleAlphaValues.length ? 255 - model.triangleAlphaValues[k] : 255;
			if (alpha > 255)
				alpha = 255;
			
			if (alpha < 255)
				hasAlpha = true;
			
			int pt0 = model.triangleViewSpaceX[k];
			int pt1 = model.triangleViewSpaceY[k];
			int pt2 = model.triangleViewSpaceZ[k];
			
			float px0, py0, pz0, px1, py1, pz1, px2, py2, pz2;
			if (interfaceModel) {
				px0 = Model.vertexPerspectiveX[pt0];
				py0 = Model.vertexPerspectiveY[pt0];
				pz0 = Model.vertexPerspectiveZ[pt0];

				px1 = Model.vertexPerspectiveX[pt1];
				py1 = Model.vertexPerspectiveY[pt1];
				pz1 = Model.vertexPerspectiveZ[pt1];

				px2 = Model.vertexPerspectiveX[pt2];
				py2 = Model.vertexPerspectiveY[pt2];
				pz2 = Model.vertexPerspectiveZ[pt2];
			} else {
				px0 = model.getVertexX(pt0);
				py0 = model.getVertexY(pt0);
				pz0 = model.getVertexZ(pt0);

				px1 = model.getVertexX(pt1);
				py1 = model.getVertexY(pt1);
				pz1 = model.getVertexZ(pt1);

				px2 = model.getVertexX(pt2);
				py2 = model.getVertexY(pt2);
				pz2 = model.getVertexZ(pt2);
			}
			
			if (drawable.getCoords() != null) {
				px0 += drawable.getCoords().getTranslateX();
				px1 += drawable.getCoords().getTranslateX();
				px2 += drawable.getCoords().getTranslateX();
				
				py0 += drawable.getCoords().getTranslateZ();
				py1 += drawable.getCoords().getTranslateZ();
				py2 += drawable.getCoords().getTranslateZ();
				
				pz0 += drawable.getCoords().getTranslateY();
				pz1 += drawable.getCoords().getTranslateY();
				pz2 += drawable.getCoords().getTranslateY();
			}
			
			// DrawableArea lives in another coordinate system
			// we have to add these offsets to the model in order
			// for it not to appear anywhere in the world!!
			final int ox = drawable.getOffsetX();
			final int oy = drawable.getOffsetY();
			if (ox != 0 || oy != 0) {
				px0 -= ox;
				px1 -= ox;
				px2 -= ox;
				
				pz0 -= oy;
				pz1 -= oy;
				pz2 -= oy;
			}
			
			int hsl0 = 0;
			if (/*drawType != 1 || */model.colorValues1 != null) {
				rgb0 = Rasterizer.anIntArray1482[model.colorValues1[k]];
				rgb1 = Rasterizer.anIntArray1482[model.colorValues2[k]];
				rgb2 = Rasterizer.anIntArray1482[model.colorValues3[k]];
				hsl0 = model.colorValues1[k];
				
				if (rgb1 <= 1)
					rgb1 = rgb0;
				if (rgb2 <= 1)
					rgb2 = rgb0;
			} else {
				rgb0 = rgb1 = rgb2 = Rasterizer.anIntArray1482[model.colorValues[k]];
				hsl0 = model.colorValues[k];
			}
			
			// ragefire, steadfast and glaiven boots fix
			// that isn't a beautiful way of removing vertices
			// but it works nonetheless
			/*if (alpha == 130 && hsl0 >= 32798 && hsl0 <= 32870) {
				//System.out.println("color: "+hsl0+", "+rgb0+", "+rgb1+", "+rgb2+", "+model.colorValues1);
				
				if (hsl0 == 32863)
					alpha = 0;
				else if (hsl0 == 32829)
					alpha = 0;
				else if (hsl0 == 32817)
					alpha = 0;
				else if (hsl0 == 32864)
					alpha = 0;
				else if (hsl0 == 32800)
					alpha = 0;
				else if (hsl0 == 32858)
					alpha = 0;
				else if (hsl0 == 32822)
					alpha = 0;
				else if (hsl0 == 32798)
					alpha = 0;
				
			}*/
			
			// fixing triangle priorities. only needed, when there's
			// no texture, because roofs with textures are properly adjusted.
			if (model.trianglePriorities != null) {
				final int p = model.trianglePriorities[k];
				float c = 0;
				
				// offsets
				if (p == 0) // falador roofs
					c = -0.05f;
				
				py0 -= c;
				py1 -= c;
				py2 -= c;
				
				px0 += c;
				px1 += c;
				px2 += c;
				
				pz0 -= c;
				pz1 -= c;
				pz2 -= c;
			}
			
			boolean clearColors = false;
			texture = model.getTexture(k);
			if (texture < -1) {
				clearColors = true;
				texture *= -1;
			}
			
			// texture on grass, that's not properly working
			// we just ignore it at this point
			if (texture == 970)
				texture = -1;
				
				// texture on floating stones or rivers
			else if (texture == 65)
				texture = -1;
			
			// atlasses have to be loaded, because it's the first
			// time we're using this model
			if (updateMappings) {
				// texture has changed, may also change atlas
				// or we're here the first time and have to
				// set the initial atlas
				if (texture != lastTexture || atlas == null) {
					// no atlas needed for texture -1
					//boolean update = texture <= 0;
					
					//System.out.println("loading texture: "+texture);
					
					// atlas is null, first time loading
					if (atlas == null)
						atlas = Cache.modelTextureAtlas128.addTexture(false, texture);
					
					// new texture arrived, check if it's in the same or in a new atlas
					TextureAtlas newAtlas = Cache.modelTextureAtlas128.addTexture(false, texture);
					
					//System.out.println("new texture: "+lastAtlas+", "+atlas.getAtlasListId()+", "+newAtlas.getAtlasListId()+", "+texture);
					
					// add triangles in a bulk, only if the atlas changes
					if (newAtlas.getAtlasListId() != atlas.getAtlasListId()) {
						int tex = atlas.getTextureID2();
						
						//System.out.println("add mapping: "+tex+"; "+trianglesCounted);
						
						textureMappings.add(BitwiseHelper.ints2Long(tex, trianglesCounted));
						trianglesCounted = 0;
						
						// new atlas for UV calculations
						atlas = newAtlas;
					}
					
					lastTexture = texture;
				}
			}
			
			//checkAtlas(false);
			trianglesCounted++;
			
			// colors need to be cleared? some textures don't require
			// a color, so we just set it to white: 0xFFFFFF
			if (clearColors && texture != -1) {
				double br = Rasterizer.getBrightness();
				int col = 0xFFFFFF;
				
				if (br >= 0.9)
					col = 0x999999;
				else if (br >= 0.8)
					col = 0xBBBBBB;
				else if (br >= 0.7)
					col = 0xDDDDDD;
				else if (br >= 0.6)
					col = 0xFFFFFF;
				
				rgb0 = rgb1 = rgb2 = col;
			}
				
				/*if (model.textures.indexOf(texture) == -1)
				{
					System.err.println("texture problem: "+texture);
				}*/
			
			// fixing triangle priorities. only needed, when there's
			// no texture, because roofs with textures are properly adjusted.
			// TODO: bugs other objects
			if (model.trianglePriorities != null/* && newTexture == -1*/) {
				int p = model.trianglePriorities[k];
				int c = 0;
				
				// offsets
				if (p == 0) // falador roofs
					c = -1;
				
				//px0 += c;
				//px1 += c;
				//px2 += c;
			}
			
			int r0 = ((rgb0 >> 16) & 0xFF);
			int g0 = ((rgb0 >> 8) & 0xFF);
			int b0 = ((rgb0 >> 0) & 0xFF);
			int a0 = (alpha);
			
			int r1 = ((rgb1 >> 16) & 0xFF);
			int g1 = ((rgb1 >> 8) & 0xFF);
			int b1 = ((rgb1 >> 0) & 0xFF);
			int a1 = (alpha);
			
			int r2 = ((rgb2 >> 16) & 0xFF);
			int g2 = ((rgb2 >> 8) & 0xFF);
			int b2 = ((rgb2 >> 0) & 0xFF);
			int a2 = (alpha);
			
			// vertex
			vertexBuffer.put(px0);
			vertexBuffer.put(py0);
			vertexBuffer.put(pz0);
			vertexBuffer.put(px1);
			vertexBuffer.put(py1);
			vertexBuffer.put(pz1);
			vertexBuffer.put(px2);
			vertexBuffer.put(py2);
			vertexBuffer.put(pz2);
			
			// color
			colorBuffer.put((byte) r0);
			colorBuffer.put((byte) g0);
			colorBuffer.put((byte) b0);
			colorBuffer.put((byte) a0);
			colorBuffer.put((byte) r1);
			colorBuffer.put((byte) g1);
			colorBuffer.put((byte) b1);
			colorBuffer.put((byte) a1);
			colorBuffer.put((byte) r2);
			colorBuffer.put((byte) g2);
			colorBuffer.put((byte) b2);
			colorBuffer.put((byte) a2);
			
			// load all triangles with the same texture id, so that we
			// have them after each other in our VBO. that means, that
			// we can draw all 'same' textures in 1 call per VBO
				/*if (countingTexture && newTexture != texture)
				{
					loading = true;
					continue;
				}*/
			
			if (texture > 0 && ModelConfig.animatedTexture[texture]) {
				//System.out.println("tex["+uid+"]: "+texture);
				animatedTriangles++;
				animatedTexture = texture;
			}
			
			// no need to calc UVs again
			if (!isUpdate)
				continue;
			
			//System.out.println("uv: "+model.uv);
			//if (model.uv == null || model.uv.length != model.triangleCount)
			//model.calculateUV();
			
			hasUV = true;
			
			// done by Jire
			if (texture > 0 && model != null && model.uv != null) {
				float u1 = model.uv[k][0];
				float v1 = model.uv[k][1];
				float u2 = model.uv[k][2];
				float v2 = model.uv[k][3];
				float u3 = model.uv[k][4];
				float v3 = model.uv[k][5];
				
				if (ModelConfig.animatedTexture[texture] && updateAnimatedTexture) {
					//model.hasAnimatedTextures = true;
					
					float progress = (Client.loopCycle % model.textureAnimationSpeed) / (float) model.textureAnimationSpeed;
					
					if (texture == 17) {
						v1 += progress;
						v2 += progress;
						v3 += progress;
						
						//if (v1 > 1 || v2 > 1 || v3 > 1)
						{
							float m = max(v1, v2, v3);
							v1 /= m;
							v2 /= m;
							v3 /= m;
						}
						
					} else {
						u1 -= progress;
						u2 -= progress;
						u3 -= progress;
					}
					
					//System.out.println("prog: "+progress);
				}
				
				addUV(atlas, texture, u1, v1, u2, v2, u3, v3, false);
			} else {
				float u1 = 0;
				float v1 = 1;
				float u2 = 1;
				float v2 = 1;
				float u3 = 0;
				float v3 = 0;
				
				addUV(atlas, texture, u1, v1, u2, v2, u3, v3, false);
			}
			
		}
		
		
		if (isLast) {
			vertexBuffer.flip();
			colorBuffer.flip();
			uvBuffer.flip();
			
			int BUFFER_TYPE = GL15.GL_STATIC_DRAW;
			if (animated)
				BUFFER_TYPE = GL15.GL_DYNAMIC_DRAW;
			
			int vertexHandle = 0;
			int colorHandle = 0;
			int uvHandle = 0;
			
			//if (hasUV && textureLoaded.size() <= 2)
			//	hasUV = false;
			
			if (vbo == null) {
				int size = 2;
				if (hasUV)
					size = 3;
				
				final int[] bufs = GLBufferManager.genBuffers(size);
				vertexHandle = bufs[0];
				colorHandle = bufs[1];
				if (hasUV)
					uvHandle = bufs[2];
			} else {
				vertexHandle = vbo.getVertexHandle();
				colorHandle = vbo.getColorHandle();
				uvHandle = vbo.getUvHandle();
			}
			
			
			//System.out.println("updating: "+isUpdate+", "+hasUV);
			if (isLast)
				writeHandles(vertexHandle, colorHandle, uvHandle, isUpdate, hasUV, colorUpdate, BUFFER_TYPE);
			
			// atlas not null? add last mapping
			//atlas = Cache.getTextureAtlas(lastTexture, false);
			
			if (resetBuffers && (!Client.disableOpenGlErrors && GraphicsDisplay.getInstance().exitOnGLError("error in loadAtlasModel(" + uid + ", " + vertexHandle + ", " + colorHandle + ", " + uvHandle + ", " + hasUV + ")")))
				return null;
			
			if (updateMappings) {
				int tex = atlas.getTextureID2();
				
				//System.out.println("add mapping: "+tex+"; "+trianglesCounted);
				textureMappings.add(BitwiseHelper.ints2Long(tex, trianglesCounted));
				trianglesCounted = 0;
			}
			
			//if (model.triangleCount != totalTrianglesCounted)
			//	System.out.println("vbo: "+model.triangleCount+"/"+totalTrianglesCounted+"; "+textureMappings.size()+"; "+animated+", "+uid);
			
			// register a new VBO
			if (vbo == null) {
				vbo = new ModelVBO(drawable.getX(), drawable.getY(), drawable.hasPositionHash());
				Cache.addModelVBO(uid, vbo);
			}
			vbo.update(vertexHandle, colorHandle, uvHandle, totalTrianglesCounted, uid, animated, -1, hasAlpha, (updateMappings && hasUV) ? textureMappings : null);
			vbo.isTextureID = false;
			
			//model.hasAnimatedTextures = false;
			
			totalTrianglesCounted = 0;
			
			//if (!animated)
			//	System.out.println("reg: "+vbo+": "+vertexHandle);
		}
		
		if (animatedTriangles > 5 && vbo != null) {
			vbo.hasAnimatedTextures = true;
			
			if (animatedTexture == 17)
				model.textureAnimationSpeed = 100;
			else
				model.textureAnimationSpeed = 400;
		}
		
		return vbo;
	}
	
	public static float max(float... vals) {
		float max = 0f;
		for (float f : vals)
			if (f > max)
				max = f;
		
		return max;
	}
	
	private static float roundUV(float uv) {
		if (uv >= 0f && uv <= 1f)
			return uv;
		
		if (uv < 0)
			uv *= -1;
		
		float uv2 = (int) uv;
		
		return (uv - uv2);
	}
	
	private static float[] normals(float x1, float y1, float z1, float x2, float y2, float z2) {
		float nx = y1 * z2 - z1 * y2;
		float ny = z1 * x2 - x1 * z2;
		float nz = x1 * y2 - y1 * x2;
		float len = (float) Math.sqrt(nx * nx + ny * ny + nz * nz);
		
		/*if (len != 0)
		{
			nx /= len;
			ny /= len;
			nz /= len;
		}*/
		
		return new float[]{nx, ny, nz, len};
	}
	
}
