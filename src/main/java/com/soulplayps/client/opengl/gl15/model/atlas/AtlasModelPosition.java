package com.soulplayps.client.opengl.gl15.model.atlas;

import com.soulplayps.client.opengl.gl15.model.ModelVBO;

public class AtlasModelPosition
{
	/**
	 * @return the x
	 */
	public int getX()
	{
		return x;
	}
	/**
	 * @return the y
	 */
	public int getY()
	{
		return y;
	}
	/**
	 * @return the z
	 */
	public int getZ()
	{
		return z;
	}
	

	public AtlasModelPosition(int x, int y, int z, int xRot, int yRot, ModelVBO vbo)
	{
		this(x, y, z, xRot, yRot, vbo, 0);
	}
	
	public AtlasModelPosition(int x, int y, int z, int xRot, int yRot, ModelVBO vbo, int collisionColor)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.xRot = xRot;
		this.yRot = yRot;
		this.vbo = vbo;
		this.collisionColor = collisionColor;
	}
	private final int collisionColor;
	private final int x;
	private final int y;
	private final int z;
	private final int xRot;
	private final int yRot;
	private final ModelVBO vbo;
	/**
	 * @return the vbo
	 */
	public ModelVBO getVbo()
	{
		return vbo;
	}
	public int getXRot()
	{
		return xRot;
	}
	public int getYRot()
	{
		return yRot;
	}
	public int getCollisionColor()
	{
		return collisionColor;
	}
	
}
