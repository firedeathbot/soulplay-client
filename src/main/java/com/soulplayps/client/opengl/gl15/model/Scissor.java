package com.soulplayps.client.opengl.gl15.model;

import com.soulplayps.client.node.raster.Raster;

public class Scissor {
	
	public final int x;
	public final int y;
	public final int width;
	public final int height;

	public Scissor(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public static Scissor create() {
		return new Scissor(Raster.topX, Raster.height - Raster.bottomY, Raster.bottomX - Raster.topX, Raster.bottomY - Raster.topY);
	}

}
