package com.soulplayps.client.opengl.gl15.model.atlas;

import com.soulplayps.client.opengl.gl15.model.TextureAtlas;

public class AtlasTexture
{
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atlas == null) ? 0 : atlas.hashCode());
		result = prime * result + height;
		result = prime * result + (isFloor ? 1231 : 1237);
		result = prime * result + textureID;
		result = prime * result + width;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtlasTexture other = (AtlasTexture) obj;
		if (atlas == null)
		{
			if (other.atlas != null)
				return false;
		}
		else if (!atlas.equals(other.atlas))
			return false;
		if (height != other.height)
			return false;
		if (isFloor != other.isFloor)
			return false;
		if (textureID != other.textureID)
			return false;
		if (width != other.width)
			return false;
		return true;
	}
	public AtlasTexture(TextureAtlas atlas, int width, int height, int textureID, boolean isFloor)
	{
		this.atlas = atlas;
		this.width = width;
		this.height = height;
		this.textureID = textureID;
		this.isFloor = isFloor;
	}
	
	private final TextureAtlas atlas;
	/**
	 * @param width the width to set
	 */
	public void setWidth(int width)
	{
		this.width = width;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(int height)
	{
		this.height = height;
	}

	private int width;
	private int height;
	private final int textureID;
	private final boolean isFloor;
	/**
	 * @return the atlas
	 */
	public TextureAtlas getAtlas()
	{
		return atlas;
	}
	/**
	 * @return the width
	 */
	public int getWidth()
	{
		return width;
	}
	/**
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}
	/**
	 * @return the textureID
	 */
	public int getTextureID()
	{
		return textureID;
	}
	/**
	 * @return the isFloor
	 */
	public boolean isFloor()
	{
		return isFloor;
	}
	
}
