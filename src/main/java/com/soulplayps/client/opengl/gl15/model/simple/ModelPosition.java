package com.soulplayps.client.opengl.gl15.model.simple;

import com.soulplayps.client.opengl.gl15.model.ModelVBO;
import com.soulplayps.client.opengl.gl15.model.Scissor;

public class ModelPosition
{
	/**
	 * @return the x
	 */
	public float getX()
	{
		return x;
	}
	/**
	 * @return the y
	 */
	public float getY()
	{
		return y;
	}
	/**
	 * @return the z
	 */
	public float getZ()
	{
		return z;
	}
	

	public ModelPosition(float x, float y, float z, int xRot, int yRot, int zRot, ModelVBO vbo)
	{
		this(x, y, z, xRot, yRot, zRot, vbo, 0, 1f, 1f, 1f, null);
	}
	
	public ModelPosition(float x, float y, float z, int xRot, int yRot, int zRot, ModelVBO vbo, int collisionColor, float scaleX, float scaleY, float scaleZ, Scissor scissor)
	{
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		this.scaleZ = scaleZ;
		this.x = x;
		this.y = y;
		this.z = z;
		this.xRot = xRot;
		this.yRot = yRot;
		this.zRot = zRot;
		this.vbo = vbo;
		this.collisionColor = collisionColor;
		this.scissor = scissor;
	}
	private final float scaleX;
	private final float scaleY;
	private final float scaleZ;
	private final int collisionColor;
	private final float x;
	private final float y;
	private final float z;
	private final int xRot;
	private final int yRot;
	private final int zRot;
	private final ModelVBO vbo;
	private final Scissor scissor;
	/**
	 * @return the vbo
	 */
	public ModelVBO getVbo()
	{
		return vbo;
	}
	public int getXRot()
	{
		return xRot;
	}
	public int getYRot()
	{
		return yRot;
	}
	public int getZRot()
	{
		return zRot;
	}
	public int getCollisionColor()
	{
		return collisionColor;
	}
	public float getScaleY()
	{
		return scaleY;
	}
	public float getScaleZ()
	{
		return scaleZ;
	}
	public float getScaleX()
	{
		return scaleX;
	}
	
	public Scissor getScissor() {
		return scissor;
	}
	
}
