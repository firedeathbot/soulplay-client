package com.soulplayps.client.opengl.gl15;


import com.soulplayps.client.Client;
import com.soulplayps.client.node.object.WorldController;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.model.ModelVBO;
import com.soulplayps.client.opengl.gl15.model.Scissor;
import com.soulplayps.client.opengl.gl15.model.TextureAtlas;
import com.soulplayps.client.opengl.gl15.model.TextureAtlasList;
import com.soulplayps.client.opengl.gl15.model.atlas.AtlasPosition;
import com.soulplayps.client.opengl.gl15.model.simple.ModelPosition;
import com.soulplayps.client.opengl.gl20.ShaderUtils;
import com.soulplayps.client.opengl.gl30.ShadowMapper;
import com.soulplayps.client.opengl.gl30.ShadowMapperARB;
import com.soulplayps.client.opengl.gl30.ShadowMapperGL30;
import com.soulplayps.client.opengl.util.Cache;
import com.soulplayps.client.util.BitwiseHelper;
import it.unimi.dsi.fastutil.longs.LongList;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectList;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

import java.nio.FloatBuffer;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;

public class VBORenderer {
	
	public static int fov = 0;
	public static boolean applyFog = false;
	private static boolean texCoordsEnabled = false;
	
	public static float START_ATLAS_HEIGHT = 1.0f;
	public static float ATLAS_HEIGHT_MODIFIER = 0.00001f;
	private static final float ROTATION_CONSTANT = 1.0f / 2048.0f * 360.0f;
	public static float atlasDepth = 0;
	
	public static boolean reloadShaders = false;
	
	private static void renderAtlasList(TextureAtlasList atlas) {
		List<TextureAtlas> atlases = atlas.getAtlases();
		List<List<AtlasPosition>> positions = atlas.getPositions();
		
		for (int i = 0, length = atlases.size(); i < length; i++) {
			renderAtlas(atlases.get(i), positions.get(i), false);
		}
	}
	
	private static void renderAtlas(TextureAtlas atlas, List<AtlasPosition> positions, boolean isLetters) {
		float SF = GraphicsDisplay.SF;
		float size = atlas.getSize();
		float eSize = atlas.getElementSize();
		
		float offX, offY, width, height;
		float u1, v1, u2, v2, depth;
		
		// reverse order to preserve shadows!
		int max = positions.size();
		for (int i = 0; i < max; i++)
		//for (int i = max-1; i >= 0; i--)
		{
			AtlasPosition pos = positions.get(i);
			Scissor scissor = pos.getScissor();
			if (scissor != null) {
				glPushAttrib(GL_SCISSOR_BIT);
				glEnable(GL_SCISSOR_TEST);
				glScissor(scissor.x, scissor.y, scissor.width, scissor.height);
				if (!Client.disableOpenGlErrors) {
					GraphicsDisplay.getInstance().exitOnGLError("error in renderScene2(" + scissor.x + ":"+scissor.y+":"+scissor.width+":"+scissor.height+")");
				}
			}
			
			// bind texture --> we're now using it
			GraphicsDisplay.setTexture(atlas.getTextureID());

			// set color
			GraphicsDisplay.setColor(pos.getColor(), pos.getAlpha());

			// begin drawing texture quad
			glBegin(GL_QUADS);
			
			boolean rotate = false;
			float rotation = 0f;
			
			offX = pos.getX();
			offY = pos.getY();
			width = pos.getWidth();
			height = pos.getHeight();
			depth = pos.getDepth();
			
			// colored boxes
			if (isLetters) {
				if (width >= eSize)
					width = eSize;
				if (height >= eSize)
					height = eSize;
			}
			
			// calculate uvs in atlas
			u1 = pos.getOx() / size;
			v1 = pos.getOy() / size;
			u2 = u1 + (width / size);
			v2 = v1 + (height / size);
			
			// colored boxes
			if (isLetters) {
				width = pos.getWidth();
				height = pos.getHeight();
			}

			if (pos.getRotation() != 0f) {
				rotation = pos.getRotation();
				//rotation = 30f;
				
				glEnd();
				
				glLoadIdentity();
				
				float dx = offX * SF + (width * SF) / 2;
				float dy = offY * SF + (height * SF) / 2;
				GL11.glTranslatef(dx, dy, 0);
				GL11.glRotatef(rotation, 0, 0, 1);
				GL11.glTranslatef(-dx, -dy, 0);
				
				glBegin(GL_QUADS);
				rotate = true;
			}
			
			glTexCoord2f(u1, v1);
			glVertex3f(offX * SF, offY * SF, depth);
			
			glTexCoord2f(u2, v1);
			glVertex3f(width * SF + offX * SF, offY * SF, depth);
			
			glTexCoord2f(u2, v2);
			glVertex3f(width * SF + offX * SF, height * SF + offY * SF, depth);
			
			glTexCoord2f(u1, v2);
			glVertex3f(offX * SF, height * SF + offY * SF, depth);
			
			if (rotate) {
				glEnd();
				
				glLoadIdentity();
				//glRotatef(-rotation, 0, 0, 0);
				
				glBegin(GL_QUADS);
				rotate = true;
			}

			glEnd();
			
			if (scissor != null) {
				glDisable(GL_SCISSOR_TEST);
				glPopAttrib();
			}
		}
		
		positions.clear();
	}
	
	private static FloatBuffer matSpecular;
	private static FloatBuffer lightPosition;
	private static FloatBuffer whiteLight;
	private static FloatBuffer lModelAmbient;
	
	private static void initLightArrays() {
		matSpecular = BufferUtils.createFloatBuffer(4);
		matSpecular.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
		
		
		float SF = GraphicsDisplay.SF;
		float x = 0f;//Client.xCameraPos * SF;
		float y = -10f;//Client.zCameraPos * SF;
		float z = 0f;//Client.yCameraPos * SF;
		
		lightPosition = BufferUtils.createFloatBuffer(4);
		lightPosition.put(x).put(y).put(z).put(0.0f).flip();
		
		whiteLight = BufferUtils.createFloatBuffer(4);
		whiteLight.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
		
		lModelAmbient = BufferUtils.createFloatBuffer(4);
		lModelAmbient.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
	}
	
	
	public static void renderShadowScene(boolean clearModels) {
		renderAllVBOModels(true, true, clearModels);
	}
	
	public static void renderScene() {
		renderScene(true);
	}
	
	
	private static int shadowProgram = 0;
	
	private static boolean setupGameShader() {
		if (true) return false;
		
		// load and compile shaders
		if (shadowProgram == 0) {
			final int vert = ShaderUtils.loadAndGet("shadow.vert");
			final int frag = ShaderUtils.loadAndGet("shadow.frag");
			if (vert == 0 || frag == 0)
				return false;
			
			shadowProgram = ShaderUtils.makeProgram(vert, frag);
		}
		
		//float fac = Client.shadowMapResolution >= 2048f ? 2f : 2f;
		float shadowMap = Client.useShadows ? (float) Client.shadowMapResolution * 2f : 0f;
		float fog = Client.displayFog ? 1f : 0f;
		
		// step of the loop, each incrementation
		float step = 1f;
		
		// goes in +/- direction. so actual amount of loops: (loop * 2) * (loop * 2)
		float loop = 1f;
		
		// settings per shadow quality
		switch (Client.shadowMapResolution) {
			case 1024: // very low
				loop = 0.5f;
				step = 0.25f;
				break;
			
			case 2048: // low
				loop = 1.5f;
				step = 0.75f;
				break;
			
			case 3072: // medium
				loop = 2f;
				step = 1f;
				break;
			
			case 4096: // high
				loop = 3f;
				step = 1f;
				break;
		}
		
		// use shadow shader
		ShaderUtils.useProgram(shadowProgram);
		ShaderUtils.setUniformVar(shadowProgram, "ShadowMap", 1);
		ShaderUtils.setUniformVar(shadowProgram, "ShadowResolution", shadowMap);
		ShaderUtils.setUniformVar(shadowProgram, "ShadowDensity", 0.25f);
		ShaderUtils.setUniformVar(shadowProgram, "FogEnabled", fog);
		ShaderUtils.setUniformVar(shadowProgram, "ShadowQualityStep", step);
		ShaderUtils.setUniformVar(shadowProgram, "ShadowQualityLoop", loop);
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[ShadowMapper]: useShader(" + shadowProgram + ")");
		}
		return true;
	}
	
	private static void cleanGameShader() {
		if (shadowProgram != 0) {
			ShaderUtils.deleteProgram(shadowProgram);
			ShaderUtils.clear();
			shadowProgram = 0;
		}
	}
	
	
	private static ShadowMapper shadows = null;
	
	public static void renderScene(boolean inGame) {
		if (inGame) {
			START_ATLAS_HEIGHT = 1.0f;
			ATLAS_HEIGHT_MODIFIER = 0.0001f;
			atlasDepth = START_ATLAS_HEIGHT;
			
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);
			
			// normal depth range for in-game models
			glDepthRange(0, 1);
		} else {
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);
			if (GraphicsDisplay.getInstance().useFog) {
				glDisable(GL_FOG);
			}
			
			// models in interfaces appear to have inverted
			// depth buffers. that's no problem though, we can
			// just invert the calculation for these models!
			if (GraphicsDisplay.vboRendering)
				glDepthRange(1, 0);
		}
		
		if (reloadShaders) {
			cleanGameShader();
			cleanColorShaders();
			reloadShaders = false;
			
			// dangerous code!! we know what we're doing though.
			// users cannot reload shaders. :)
			GraphicsDisplay.shadersSupported = true;
			GraphicsDisplay.FBOsSupported = true;
		}
		
		//glDisable(GL_BLEND);
		if (Client.doneLoading) {
			// rendering with shadows (AND supported!)
			if (inGame && GraphicsDisplay.shadersSupported) {
				if (Client.useShadows && GraphicsDisplay.FBOsSupported) {
					// setup shadows
					if (shadows == null)
						if (GraphicsDisplay.FBOsSupportedARB)
							shadows = new ShadowMapperARB(Client.shadowMapResolution);
						else
							shadows = new ShadowMapperGL30(Client.shadowMapResolution);
					
					// render shadow map
					shadows.drawShadowMap(false, Client.shadowMapResolution);
					
					setupGameShader();
					
					// and render the normal world
					GraphicsDisplay.getInstance().setFog();
					renderAllVBOModels(inGame, false, true);
					
					// generate shadow coordinates
					shadows.unbindFrameBuffer();
					
					// TODO: rendering with shaders is slower, why?
				} else {
					setupGameShader();
					GraphicsDisplay.getInstance().setFog();
					renderAllVBOModels(inGame, false, true);
				}
				
				ShaderUtils.useFixedFunctions();
				
				// rendering without shadows
			} else {
				
				// clear existing shadow buffers
				if (inGame) {
					if (shadows != null) {
						glActiveTexture(GL_TEXTURE1);
						shadows.cleanupBuffers();
						shadows = null;
						glActiveTexture(GL_TEXTURE0);
					}
					cleanGameShader();
				}
				
				renderAllVBOModels(inGame, false, true);
			}
			
			//handleSelectionBuffer();
			//System.out.println("render: "+Cache.modelPositions.size());
			
		}
		
		//glDisable(GL_TEXTURE_2D);
		setPerspective(inGame, false);
		
		// load identity matrix to reset all perspectives
		// for further openGL calls (sprites, etc..)
		glLoadIdentity();
		GraphicsDisplay.ortho();
		
		if (!inGame) {
			glClear(GL_DEPTH_BUFFER_BIT);
			
			if (GraphicsDisplay.vboRendering) {
				glAlphaFunc(GL_GREATER, 0.0f);

				renderAtlasList(Cache.sprites32);
				renderAtlasList(Cache.sprites64);
				renderAtlasList(Cache.sprites128);
				renderAtlas(Cache.letterAtlas, Cache.letters, true);
			}

			if (GraphicsDisplay.getInstance().useFog) {
				glEnable(GL_FOG);
			}
		}
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in renderScene(" + inGame + ")");
		}
		//if (inGame && GraphicsDisplay.vboRendering)
		//	GraphicsDisplay.getInstance().setBackground();
	}
	
	public static float getRD() {
		// actual rendering distance
		float rd = 6.0f;
		
		if (WorldController.viewDistance <= 30)
			rd = 4.0f;
		else if (WorldController.viewDistance <= 40) // setting
			rd = 4.35f;
		else if (WorldController.viewDistance <= 50) // setting
			rd = 5.05f;
		else if (WorldController.viewDistance <= 60) // setting
			rd = 5.65f;
		else if (WorldController.viewDistance <= 70)
			rd = 5.95f;
		else if (WorldController.viewDistance <= 80) // setting
			rd = 6.80f;
		else if (WorldController.viewDistance <= 90)
			rd = 7.05f;
		else if (WorldController.viewDistance <= 100) // setting
			rd = 7.80f;
		else if (WorldController.viewDistance <= 110)
			rd = 8.15f;
		else if (WorldController.viewDistance <= 120)
			rd = 8.55f;
		else if (WorldController.viewDistance <= 200)
			rd = 15f;
		
		if (!Client.loggedIn)
			rd = 5.25f;
		
		//rd -= 0.65f;
		
		// subtract zooming to keep the rendering
		// distance almost constant when zooming in
		float maxZoom = 1100F;
		rd -= ((maxZoom - Client.cameraZoom) / maxZoom) * 2f;
		//rd -= 0.5f;
		
		return rd;
	}
	
	public static void setFrustum(boolean isShadow) {
		//isShadow = false;
		
		final int ss = shadows == null ? 0 : 1024;
		int x = 0;
		int y = 0;
		int width = GraphicsDisplay.getWidth();
		int height = GraphicsDisplay.getHeight();
		float f = 200.0f;
		if (Client.isFixed() && Client.loggedIn) {
			x = 4;
			y = 4;
			width = 512;
			height = 334;
		}
		int centerX = x + width / 2;
		int centerY = y + height / 2;
		
		int heightDelta = height - 334;
		if (heightDelta < 0) {
			heightDelta = 0;
		} else if (heightDelta > 100) {
			heightDelta = 100;
		}
		int i_7_ = 256 + (163 - 256) * heightDelta / 100;
		int fov = height * i_7_ / 334;
		VBORenderer.fov = fov * 512 >> 8;
		
		final float left = (((x - centerX) * 256.0f) / fov) * GraphicsDisplay.SF;
		final float right = (((x + width - centerX) * 256.0f) / fov) * GraphicsDisplay.SF;
		final float top = (((y - centerY) * 256.0f) / fov) * GraphicsDisplay.SF;
		final float bottom = (((y + height - centerY) * 256.0f) / fov) * GraphicsDisplay.SF;
		
		// how far are we moving the frustum back?
		// the smaller, the further it goes back
		// towards the screen
		float sc = 0.05f;
		if (Rasterizer.firstPerson)
			sc = 0.005f;
		else if (isShadow)
			sc = 0.35f;
		
		// actual rendering distance
		final float rd = getRD() + (isShadow ? 25f : 0f);
		
		// aspect ratio
		glFrustum(left * GraphicsDisplay.SF * f, right * GraphicsDisplay.SF * f, -bottom * GraphicsDisplay.SF * f, -top * GraphicsDisplay.SF * f, 2.0f * sc, rd);
		glViewport(x, GraphicsDisplay.getHeight() - y - height, width, height);
	}
	
	private static int shadowLightX = 0;
	private static int shadowLightY = 0;
	private static boolean positionSet = false;
	
	private static void smoothShadowTransition(int x, int y) {
		if (shadowLightX != x || shadowLightY != y) {
			shadowLightX += trans(x - shadowLightX);
			shadowLightY += trans(y - shadowLightY);
		} else if (!positionSet) {
			//System.out.println("true!");
			positionSet = true;
		}
	}
	
	private static int trans(final int t) {
		final int deadzone = 1024;
		int maxjump = (int) (8.0 * Client.anInt945);
		if (maxjump < 1)
			maxjump = 1;
		else if (maxjump > 6)
			maxjump = 6;
		
		final int jumpborder = 2048;
		
		//if (!positionSet)
		//System.out.println("t: "+t+", "+positionSet);
		
		// jump (teleports)
		if (t > jumpborder || t < -jumpborder)
			return t;
		
		// deadzone
		if (t <= deadzone && t >= -deadzone && positionSet)
			return 0;
		
		positionSet = false;
		
		// maximum increase
		if (t > maxjump)
			return maxjump;
		else if (t < -maxjump)
			return -maxjump;
		
		//System.out.println("trans1: "+t);
		return t;
	}
	
	public static void cameraTransforms(final boolean isShadow) {
		final float SF = GraphicsDisplay.SF;
		
		// rendering camera position
		if (!isShadow) {
			// camera curves / rotations
			glRotatef(180f, 1.0f, 0.0f, 0.0f);
			glRotatef(Client.yCameraCurve * ROTATION_CONSTANT, 1.0f, 0.0f, 0.0f);
			glRotatef(Client.xCameraCurve * ROTATION_CONSTANT, 0.0f, 1.0f, 0.0f);
			
			// global down-scaling from model space to our frustum space
			glScalef(SF, SF, SF);
			
			// camera position
			glTranslatef(-Client.xCameraPos, -Client.zCameraPos, -Client.yCameraPos);
			
			// shadow camera position
		} else {
			// camera curves / rotations
			glRotatef(180f + (150) * ROTATION_CONSTANT, 1, 0, 0);
			glRotatef((-250) * ROTATION_CONSTANT, 0, 1, 0);
			//glRotatef(180f + (Client.yCameraCurve) / 2048f * 360f, 1, 0, 0);
			//glRotatef((Client.xCameraCurve) / 2048f * 360f, 0, 1, 0);
			
			// global down-scaling from model space to our frustum space
			glScalef(SF, SF, SF);
			
			final int div = 1;
			final int cx = Client.baseDX != 0 ? Client.baseDX : Client.baseOffsetX;
			final int cy = Client.baseDY != 0 ? Client.baseDY : Client.baseOffsetY;
			final int px = ((Client.myPlayer.x + cx) / div) * div;
			final int py = ((Client.myPlayer.z + cy) / div) * div;
			smoothShadowTransition(px, py);
			
			// TODO: finetuning with positions
			// TODO: shadows flicker
			final int sx = (shadowLightX - 5500);
			final int sy = (shadowLightY - 5500);
			final int sz = 6750;
			
			//final int sx = (Client.myPlayer.x / 100) * 100 - 1000;
			//final int sy = (Client.myPlayer.y / 100) * 100 - 1000;
			
			//System.out.println("plr: "+sx+", "+sy+", "+Client.myPlayer.height+"; "+dx+", "+dy+"; "+min+", "+d);
			//System.out.println("cam: "+Client.xCameraPos+", "+Client.yCameraPos+", "+Client.zCameraPos);
			
			// TODO: find shadow position
			//glTranslatef(-Client.xCameraPos + 2048, -Client.zCameraPos, -Client.yCameraPos);
			//glTranslatef(-sx, 3500, -sy);
			glTranslatef(-sx, sz, -sy);
			//glTranslatef(-Client.xCameraPos + 1, -Client.zCameraPos + 1, -Client.yCameraPos);
			//System.out.println("sh pos: "+(sx >> 6)+", "+(sy >> 6)+" "+sz);
		}
		
		//System.out.println("plr: "+Client.myPlayer.x+", "+Client.myPlayer.y+"; "+shadows.getSize());
		//System.out.println("cur: "+Client.xCameraCurve+", "+Client.yCameraCurve);
		
	}
	
	public static void setPerspective(boolean gameModels, boolean isShadow) {
		//glDisable(GL_FOG);
		
		// camera curves/rotation
		if (gameModels) {
			// setup projection matrix
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			
			// set view frustum
			setFrustum(isShadow);
			
			if (!Client.displayFog || !gameModels || shadows != null)
				cameraTransforms(isShadow);
			
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
		}
		
		// setup fog and background color
		//GraphicsDisplay.getInstance().setBackground();
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[VBORenderer:] setPerspective(" + gameModels + ", " + isShadow + ")");
		}
	}
	
	// gluPickMatrix
	private static void pickMatrix(float x, float y, float deltax, float deltay, int[] viewport) {
		if (deltax <= 0 || deltay <= 0) {
			return;
		}
	 
	   /* Translate and scale the picked region to the entire window */
		glTranslatef((viewport[2] - 2 * (x - viewport[0])) / deltax, (viewport[3] - 2 * (y - viewport[1])) / deltay, 0);
		glScalef(viewport[2] / deltax, viewport[3] / deltay, 1.0f);
	}
	
	private static int textureHandle = 0;
	
	// render all vbos.
	// gameModels true: rendering in-game models, else atlasses (images, letters, etc..)
	// sceneMode true: rendering in-game models, else shadow mapping
	public static void renderAllVBOModels(final boolean gameModels, final boolean isShadow, boolean clearBuffers) {
		//if (isShadow)
		//	return;
		
		//System.out.println("size: ");
		
		textureHandle = 0;
		GraphicsDisplay.setTexture(0);
		
		final long now = System.currentTimeMillis();
		
		// set perspective/rendering field
		//if (!isShadow)
		setPerspective(gameModels, isShadow);
		
		// don't clear buffers when rendering shadows
		// we have to render the scene twice!!
		//final boolean clearBuffers = !isShadow;
		
		//if (gameModels)
		{
			// bind texture
			//glBindTexture(GL_TEXTURE_2D, 0);
			
			final float minA = shadows == null ? 0.3f : 0f;
			
			glAlphaFunc(GL_GREATER, minA);
			
			GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			if (!isShadow)
				GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
			
			
			if (isShadow) {
				renderVBOList(Cache.modelPositionsShadow.values(), gameModels, false, now, clearBuffers, isShadow);
				Cache.modelPositionsShadow.clear();
				
				// render floors (floor is not casting shadows!)
				renderVBOList(Cache.floorPositions.values(), gameModels, true, now, clearBuffers, isShadow);
				
			} else {
				
				// render floors (floor is not casting shadows!)
				if (gameModels)
					renderVBOList(Cache.floorPositions.values(), gameModels, true, now, clearBuffers, isShadow);
				
				clearBuffers = true;
				
				// render models without alpha
				renderVBOList(Cache.modelPositions.values(), gameModels, false, now, clearBuffers, isShadow);
				
				// render with fixed alpha values? it's a bit slower!
				final boolean alphaFix = !isShadow;
				
				// proper alpha handling, if a model contains alpha and non-alpha values
				// we must make sure that non-alpha values are drawn first and alpha
				// values afterwards. else alpha values screw up the depth buffer, although
				// they shouldn't!
				if (alphaFix) {
					float alphaPass = 0.95f;
					
					// set alpha filter to 0.95, so all non-alpha parts of alpha models are drawn
					glAlphaFunc(GL_GREATER, alphaPass);
					renderVBOList(Cache.modelPositionsAlpha.values(), gameModels, false, now, false, isShadow);
					
					// revert alpha function, so that all alpha parts of alpha models are drawn
					glAlphaFunc(GL_LESS, alphaPass);
					renderVBOList(Cache.modelPositionsAlpha.values(), gameModels, false, now, clearBuffers, isShadow);
					
					// reset alpha function
					glAlphaFunc(GL_GREATER, minA);
				} else {
					renderVBOList(Cache.modelPositionsAlpha.values(), gameModels, false, now, clearBuffers, isShadow);
				}
			}
			
			
			if (!isShadow)
				GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
			GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
			GL15.glBindBuffer(GL_ARRAY_BUFFER, 0);
			
			// disable texture coordinates
			if (texCoordsEnabled) {
				GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
				texCoordsEnabled = false;
			}
			
			// clear model positions
			if (clearBuffers) {
				clearBuffers();
			}
			
			if (!Client.disableOpenGlErrors) {
				GraphicsDisplay.getInstance().exitOnGLError("error in renderAllVBOs(" + gameModels + ", " + isShadow + ", " + clearBuffers + ")");
			}
		}
	}
	
	public static void clearBuffers() {
		Cache.floorPositions.clear();
		
		Cache.modelPositions.clear();
		Cache.modelPositionsAlpha.clear();
		
		// TODO: multi-thread vbo deletion detection
		Cache.manageVBOs();
		Cache.clearModelVBOs();
	}
	
	private static int colorProgram = 0;
	
	private static boolean setupColorShaders() {
		if (colorProgram == 0) {
			final int vert = ShaderUtils.loadAndGet("picking.vert");
			final int frag = ShaderUtils.loadAndGet("picking.frag");
			if (vert == 0 || frag == 0)
				return false;
			
			colorProgram = ShaderUtils.makeProgram(vert, frag);
		}
		
		ShaderUtils.useProgram(colorProgram);
		return true;
	}
	
	private static void cleanColorShaders() {
		if (colorProgram != 0) {
			ShaderUtils.deleteProgram(colorProgram);
			ShaderUtils.clear();
			colorProgram = 0;
		}
	}
	
	private static void renderSimpleVBOList(Set<Entry<Long, List<ModelPosition>>> entries, boolean clear, boolean colorPicking) {
		for (Entry<Long, List<ModelPosition>> entry : entries) {
			final List<ModelPosition> positions = entry.getValue();
			
			for (ModelPosition mp : positions)
				renderSimpleVBO(mp, true, colorPicking);
			
			// collision detection code clears it
			if (clear)
				positions.clear();
		}
	}
	
	// only renders vertices of the VBO. we use that for our selection buffer
	public static void renderSimpleVBO(ModelPosition position, boolean inGame, boolean colorPicking) {
		final ModelVBO vbo = position.getVbo();
		final int vertexHandle = vbo.getVertexHandle();
		
		// invalid buffer, we're not trying to draw that!
		if (vertexHandle == 0 || vbo.getTriangles() <= 0)
			return;
		
		// vy and vz are voluntarily changed here
		// some variable naming from before
		// appears to be wrong, the change fixes it!!
		final float vx = position.getX();
		final float vz = position.getY();
		final float vy = position.getZ();
		
		// model rotation
		final float xRot = position.getXRot() * ROTATION_CONSTANT;
		final float yRot = position.getYRot() * ROTATION_CONSTANT;
		
		if (textureHandle != 0) {
			textureHandle = 0;
			GraphicsDisplay.setTexture(0);
		}

		/*if (!colorPicking) {
			glPushName(vbo.mapIdentifier);
		} else {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			final float pickColor = position.getCollisionColor() / 255f;
			ShaderUtils.setUniformVar(colorProgram, "PickColor", pickColor);
		}*/
		
		// camera translations for in-game space
		// scale model to rendering plane
		glLoadIdentity();
		
		// camera transformations
		//if (Client.displayFog && inGame)
		//	cameraTransforms();
		
		// model world position
		glTranslatef(vx, vy, vz);
		
		// rotate model individually (facings)
		glRotatef(xRot, 0, 1, 0);
		glRotatef(yRot, 1, 0, 0);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexHandle);
		GL11.glVertexPointer(3, GL_FLOAT, 0, 0);
		
		// render triangles
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vbo.getTriangles() * 3);
		
		glPopName();
	}
	
	private static void renderVBOList(ObjectCollection<ObjectList<ModelPosition>> values,
	                                  final boolean inGame, final boolean isFloor, final long now,
	                                  final boolean clear, final boolean isShadow) {
		for (ObjectList<ModelPosition> positions : values) {
			for (int i = 0, length = positions.size(); i < length; i++) {
				ModelPosition mp = positions.get(i);
				// else the application crashes, Idk why ^^
				if (isFloor && mp.getVbo().getUvHandle() == 0)
					continue;
						
						// don't render buffers marked for delete
				else if (mp.getVbo().delete || mp.getVbo().clear || mp.getVbo().handlesReleased)
					continue;
						
						// vbo is not visible
				else if (!mp.getVbo().isVisible && !isShadow)
					continue;
					//if (isFloor || true)
					//	continue;
				
				renderVBO(mp, inGame, isFloor, now, isShadow);
			}
		}

		if (clear) {
			values.clear();
		}
	}
	
	private static int combinerHandle = 0;
	
	private static void setupCombiner(int handle) {
		
		if (true)
			return;
		//System.out.println("setup handle: "+combinerHandle);
		
		// Tell OpenGL to use texture unit 0's color values for Arg0
		
		glActiveTexture(GL_TEXTURE0);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
		
		// Sample RGB, multiply by previous texunit result
		glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE); // Modulate RGB with RGB
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PREVIOUS);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_TEXTURE);
		glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
		glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR);
		
		// Sample ALPHA, multiply by previous texunit result
		glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_MODULATE); // Modulate ALPHA with ALPHA
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA, GL_PREVIOUS);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_ALPHA, GL_TEXTURE);
		glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);
		glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA, GL_SRC_ALPHA);
		
		if (combinerHandle % 3 == 0) {
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE0);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
		} else if (combinerHandle % 3 == 1) {
			//glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_TEXTURE0 + combinerHandle);
			//glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR);
		} else {
			//glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE2_RGB, GL_TEXTURE0 + combinerHandle);
			//glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_COLOR);
		}
		
		combinerHandle++;
		
		/*Integer id = textureCombiners.get(handle);
		if (id == null)
		{
			id = ++textureCombinerCount;
			textureCombiners.put(handle, id);
		}
		
		//glBindTexture(GL_TEXTURE_2D, handle);
		
		if (id == 1)
		{
			glActiveTexture(GL_TEXTURE1+id);
			glBindTexture(GL_TEXTURE_2D, handle);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		}
		else
		{
			glActiveTexture(GL_TEXTURE1+id);
			glBindTexture(GL_TEXTURE_2D, handle);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE);  
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE2_RGB, GL_PREVIOUS);
			glTexEnvi(GL_TEXTURE_ENV, GL_SRC1_RGB, GL_TEXTURE);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR);

			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_MODULATE);
			glTexEnvi(GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_PREVIOUS);
			glTexEnvi(GL_TEXTURE_ENV, GL_SRC1_ALPHA, GL_TEXTURE);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA, GL_SRC_ALPHA);
		}
		
		
		// brightens up stuff, looks really beautiful
		glTexEnvf(GL_TEXTURE_ENV, GL_RGB_SCALE, 2.0f);*/
	}
	
	public static void renderVBO(final ModelPosition position, final boolean inGame, final boolean isFloor, final long now, final boolean isShadow) {
		//if (inGame)
		//	return;

		final ModelVBO vbo = position.getVbo();

		vbo.lastUse = now;
		
		int triangles = vbo.getTriangles() * 3;
		if (triangles <= 0)
			return;
		
		// TODO: segemenation fault when handles / triangle count are invalid
		// some drivers catch this problem, others just crash. find out, if it's
		// possible somehow, that the triangle count is wrong
		// setting high triangles value doesn't crash the intel driver

		final int vertexHandle = vbo.getVertexHandle();
		final int colorHandle = vbo.getColorHandle();
		final int uvHandle = vbo.getUvHandle();

		// invalid buffer, we're not trying to draw that!
		if (vertexHandle <= 0 || colorHandle <= 0) {
			System.err.println("[VBORenderer]: invalid handles: " + vertexHandle + ", " + colorHandle + ", " + uvHandle + "");
			return;
		}
		
		/*if (vertexHandle > AssetLoader.maxBufferId || colorHandle > AssetLoader.maxBufferId || uvHandle > AssetLoader.maxBufferId) {
			System.out.println("Stopped crash");
			return;
		}*/
		
		// vy and vz are voluntarily changed here
		// some variable naming from before
		// appears to be wrong, the change fixes it!!
		final float vx = position.getX();
		final float vz = position.getY();
		final float vy = position.getZ();
		
		// model rotation
		final float xRot = position.getXRot() * ROTATION_CONSTANT;
		final float yRot = position.getYRot() * ROTATION_CONSTANT;
		
		//System.out.println("pos: "+vx+"/"+vy+"/"+vz);
		
		// camera translations for in-game space
		// scale model to rendering plane
		glLoadIdentity();
		
		// camera transformations
		if (Client.displayFog && inGame && shadows == null)
			cameraTransforms(isShadow);
		
		// rendering model in interface --> no real perspective
		// we center vx and vy and set the camera position to 0
		if (!inGame) {
			// center model on screen
			//vx = Rasterizer.centerX;
			//vy = (GraphicsDisplay.getHeight() - Rasterizer.centerY);
			//vz = 100f;
			
			// invert x rotation
			//xRot = 360 - xRot;
			
			// scale
			glScalef(GraphicsDisplay.SF, GraphicsDisplay.SF, GraphicsDisplay.SF);
			
			// rotate model upwards
		}
		
		// model world position
		glTranslatef(vx, vy, vz);
		
		// individual model scaling
		if (position.getScaleX() != 0f)
			glScalef(position.getScaleX(), position.getScaleY(), position.getScaleZ());
		
		
		// TODO: find a solution for non-stacking rotation matrices
		
		// rotate model individually (facings)
		glRotatef(xRot, 0, 1, 0);
		glRotatef(yRot, 1, 0, 0);
		//glRotatef(zRot, 0, 0, 1);
		
		// stride = distance(x1, x2) * 3
		
		if (position.getScissor() != null) {
			glPushAttrib(GL_SCISSOR_BIT);
			glEnable(GL_SCISSOR_TEST);
			glScissor(position.getScissor().x, position.getScissor().y, position.getScissor().width, position.getScissor().height);
		}
		
		combinerHandle = 0;
		{
			// no textures
			if ((isShadow && Client.shadowSetting == 1) || vbo.getTextures() == null || vbo.getTextures().size() <= 0 || vbo.getUvHandle() == 0) {
				if (textureHandle != 0) {
					//textureHandle = 0;
					//glBindTexture(GL_TEXTURE_2D, 0);
					//GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
					//texCoordsEnabled = false;
				}
				
				GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexHandle);
				GL11.glVertexPointer(3, GL_FLOAT, 0, 0);
				
				if (!isShadow) {
					GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, colorHandle);
					GL11.glColorPointer(4, GL_UNSIGNED_BYTE, 0, 0);
				}
				
				// render triangles
				GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, triangles);
			}
			
			// textures: model triangles are sorted
			// by their texture and the vbo.
			// textures map gives us the pointers to
			// the texture changes inside the model :^)
			else {
				int texture;
				int offset = 0;
				int totalTriangles = triangles;
				
				LongList textures = vbo.getTextures();
				for (int i = 0; i < textures.size(); i++) {
					long entry = textures.getLong(i);
					texture = BitwiseHelper.long2IntA(entry);
					triangles = BitwiseHelper.long2IntB(entry) * 3;
					
					if (inGame && !vbo.isTextureID) {
						//System.out.println("texture: "+texture+", "+vbo.getTextures().size()+", "+textureHandle+", "+triangles);
						//continue;
					}
					
					//if ()
					
					/*if (texture <= 0)
					{

						offset += triangles;
						continue;
					}*/
					
					/*if ((vbo.vertexRemaining != 0 && vbo.vertexRemaining < triangles * 3) || (vbo.colorRemaining != 0 && vbo.colorRemaining < triangles * 4) || (vbo.uvRemaining != 0 && vbo.uvRemaining < triangles * 2)) {
						System.out.println("stopped render");
						continue;
					}*/
					
					setupCombiner(0);
					
					GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexHandle);
					GL11.glVertexPointer(3, GL_FLOAT, 12, offset * 12);
					
					GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, colorHandle);
					GL11.glColorPointer(4, GL_UNSIGNED_BYTE, 4, offset * 4);
					//glActiveTexture(GL_TEXTURE0);
					//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
					//glTexEnvf(GL_TEXTURE_ENV, GL_RGB_SCALE, 1.0f);
					
					//if (texture > 0 && !isFloor)
					//	System.out.println("ddd "+uvHandle+", "+texture+", "+triangles);
					
					// has texture coordinates
					if (uvHandle != 0 && texture > 0) {
						// texture handle available (from atlas)
						if (!vbo.isTextureID) {
							// texture is changing
							if (texture != textureHandle) {
								//System.out.println("handle: "+textureHandle+", "+texture+", "+vbo.getTextures().size());
								textureHandle = texture;
							}
							
							GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, uvHandle);
							GL11.glTexCoordPointer(2, GL_FLOAT, 8, offset * 8);
							
						}
						
						// no atlas, list of textures given
						else {
							textureHandle = Cache.loadTexture(texture, isFloor);
							
							// System.out.println("handle: "+textureHandle);
							if (textureHandle != 0) {
								GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, uvHandle);
								GL11.glTexCoordPointer(2, GL_FLOAT, 8, offset * 8);
							}
						}
					} else if (textureHandle != 0) {
						textureHandle = 0;
					}
					
					if (textureHandle != 0 && !texCoordsEnabled) {
						texCoordsEnabled = true;
						GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
					} else if (textureHandle == 0 && texCoordsEnabled) {
						texCoordsEnabled = false;
						GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
					}
					
					//System.out.println(vbo.vertexRemaining+" - "+(triangles * 3)+" : "+vbo.colorRemaining+" - "+(triangles * 4)+" : "+vbo.uvRemaining+" - "+(triangles * 2));
					//System.out.println(vertexHandle+":"+colorHandle+":"+uvHandle+ " - " + AssetLoader.maxBufferId+","+vbo.getUid()+":"+vbo.getObjectId());
					GraphicsDisplay.setTexture(textureHandle);

					// draw triangles
					GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, triangles);
					
					// done drawing 1 texture, hop to next texture offset
					offset += triangles;
				}
				
				if (offset != totalTriangles) {
					//System.err.println("[VBORenderer]: error, model triangles invalid with textures: "+offset+"/"+totalTriangles);
				}
			}
		}
		
		if (position.getScissor() != null) {
			glDisable(GL_SCISSOR_TEST);
			glPopAttrib();
		}
		
		// exit on any error
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("error in renderVBO(" + vbo.getUid() + ", " + vbo.getVertexHandle() + ", " + vbo.getColorHandle() + ")");
		}
	}

}
