package com.soulplayps.client.opengl.gl20;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import com.soulplayps.client.Client;
import com.soulplayps.client.opengl.GraphicsDisplay;

public class ShaderUtils {
    public static int VERTEX = GL20.GL_VERTEX_SHADER;
    public static int FRAGMENT = GL20.GL_FRAGMENT_SHADER;
    
    private static Map<String, Integer> shaders = new HashMap<String, Integer>();
    

    public static int loadAndGet(String source) {
    	if (source.endsWith(".vert"))
    		return loadAndGet(source, VERTEX);
    	else if (source.endsWith(".frag"))
    		return loadAndGet(source, FRAGMENT);
    	else
    		return 0;
    }
    
    public static int loadAndGet(String source, int type) {
    	Integer shader = shaders.get(source);
    	if (shader == null) {
    		//System.out.println("loading shader... "+source);
    		final String fileContents;
	    	if (GraphicsDisplay.debug)
	    		fileContents = loadText(source);
	    	else
	    		fileContents = ShaderLoader.getShaderCode(source);

    		// couldn't compile shaders --> disable them
    		if (fileContents == null) {
    			GraphicsDisplay.shadersSupported = false;
    			return 0;
    		}
    		
    		shader = makeShader(fileContents, type);
    		shaders.put(source, shader);
    	}
    	
    	return shader;
    }
    
    
    /**
     * Deletes and releases all shaders
     */
    public static void clear() {
    	//System.out.println("clearing shaders");
    	
    	for (Integer shader : shaders.values()) {
    		GL20.glDeleteShader(shader);
    		if (!Client.disableOpenGlErrors) {
    			GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: deleteShader("+shader+")");
    		}
    	}
    	
    	shaders.clear();
    }
    
    /**
     * Sets opengl to use the specified program.
     * @param id The program to use.
     */
    public static void useProgram(int id) {
        GL20.glUseProgram(id);
        
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: useProgram("+id+")");
        }
    }
    /**
     * Sets opengl to use the default shaders.
     */
    public static void useFixedFunctions() {
        GL20.glUseProgram(0);
        
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: useFixedFunctions()");
        }
    }
    /**
     * Creates a program containing the specified shaders.
     * @param shaders The shaders to be used
     * @return The id of the program.
     */
	public static int makeProgram(int... shaders) {
		for (int shader : shaders) {
			if (shader <= 0) {
				GraphicsDisplay.shadersSupported = false;
				throw new RuntimeException("Shader Compilation Error");
			}
		}
		
		final int id = GL20.glCreateProgram();
		for (int shader : shaders) {
			GL20.glAttachShader(id, shader);
			
			if (!Client.disableOpenGlErrors) {
				GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: makeProgram() -> attachShader(" + id + ", " + shader + ")");
			}
		}
		GL20.glLinkProgram(id);
		
		if (!Client.disableOpenGlErrors) {
			GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: makeProgram() -> linkProgram(" + id + ")");
		}
		return id;
	}
    
    /**
     * Deletes a program with a given identifier
     * @param program the program to be deleted
     */
    public static void deleteProgram(int program) {
    	GL20.glDeleteProgram(program);
    	if (!Client.disableOpenGlErrors) {
    		GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: deleteProgram("+program+")");
    	}
    }
    
    /**
     * Returns the position of the specified uniform variable.
     * @param program The program containing the variable.
     * @param var The name of the variable.
     * @return  The position of the variable.
     */
    public static int getUniformVarPos(int program, String var) {
        int i = GL20.glGetUniformLocation(program, var);
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: getUniformVarPos("+program+", "+var+")");
        }
        return i;
    }
    /**
     * Sets the value of the specified uniform variable to that given.
     * @param program The id of the program containing the variable. 
     * @param name The name of the variable.
     * @param values The value or values to be set.
     */
    public static void setUniformVar(int program, String name, float... values) {
        int loc = GL20.glGetUniformLocation(program, name);
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: setUniformVar() -> getUniformLocation("+program+", "+name+")");
        }

        switch(values.length) {
            case 1: {GL20.glUniform1f(loc, values[0]); break;}
            case 2: {GL20.glUniform2f(loc, values[0], values[1]); break;}
            case 3: {GL20.glUniform3f(loc, values[0], values[1], values[2]); break;}
            case 4: {GL20.glUniform4f(loc, values[0], values[1], values[2], values[3]); break;}
            default: {
                FloatBuffer buff = BufferUtils.createFloatBuffer(values.length);
                buff.put(values);
                buff.rewind();
                GL20.glUniform1(loc, buff);
            }
        }
        
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: setUniformVar() -> Uniform"+values.length+"f("+program+", "+name+")");
        }
    }
    /**
     * Sets the value of the specified uniform variable to that given.
     * @param program The id of the program containing the variable. 
     * @param name The name of the variable.
     * @param values The value or values to be set.
     */
    public static void setUniformVar(int program, String name, int... values) {
        int loc = GL20.glGetUniformLocation(program, name);
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: setUniformVar() -> getUniformLocation("+program+", "+name+")");
        }

        switch(values.length) {
            case 1: {GL20.glUniform1i(loc, values[0]); break;}
            case 2: {GL20.glUniform2i(loc, values[0], values[1]); break;}
            case 3: {GL20.glUniform3i(loc, values[0], values[1], values[2]); break;}
            case 4: {GL20.glUniform4i(loc, values[0], values[1], values[2], values[3]); break;}
            default: {
                IntBuffer buff = BufferUtils.createIntBuffer(values.length);
                buff.put(values);
                buff.rewind();
                GL20.glUniform1(loc, buff);
            }
        }
        
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: setUniformVar() -> Uniform"+values.length+"i("+program+", "+name+")");
        }
    }
    /**
     * Creates and compiles a shader of the given type with the given source.
     * @param source A string containing the shader source.
     * @param type The type of the shader, either ShaderUtils.VERTEX or ShaderUtils.FRAGMENT (copies of GL20.GL_VERTEX_SHADER etc.)
     * @return The id of the shader.
     */
    public static int makeShader(String source, int type) {
        int id = GL20.glCreateShader(type);
        
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: makeShader() -> createShader("+type+")");
        }
        
        GL20.glShaderSource(id, source);
        
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: makeShader() -> shaderSource("+id+", "+source+")");
        }
        
        GL20.glCompileShader(id);
        String s = GL20.glGetShaderInfoLog(id, 1000);
        if(!s.isEmpty()) {
            System.err.println("Error compiling " + source);
            System.err.println(s);
            
            if (!Client.disableOpenGlErrors) {
            	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: compileShaderError() -> \n"+s);
            }
            return -1;
        }
	
        if (!Client.disableOpenGlErrors) {
        	GraphicsDisplay.getInstance().exitOnGLError("[ShaderUtils]: makeShader() -> compiledShader("+id+", "+source+", "+type+")");
        }
        return id;
    }
    /**
     * Allows GLSL fragment shaders to use different colours for front and back facing colours. 
     * Without this, gl_BackColor in the vertex shader is ignored. It is be disabled by default.
     */
    public static void enableTwoSide() {
        GL11.glEnable(GL20.GL_VERTEX_PROGRAM_TWO_SIDE);
    }
    /**
     * Disallows GLSL fragment shaders to use different colours for front and back facing colours. 
     * With this, gl_BackColor in the vertex shader is ignored. It is be disabled by default.
     */
    public static void disableTwoSide() {
        GL11.glDisable(GL20.GL_VERTEX_PROGRAM_TWO_SIDE);
    }
    /**
     * Allows GLSL vertex shaders to set the size of the points draw.
     */
    public static void enablePointSize() {
        GL11.glEnable(GL20.GL_VERTEX_PROGRAM_POINT_SIZE);
    }
    /**
     * Disallows GLSL vertex shaders to set the size of the points draw.
     */
    public static void disablePointSize() {
        GL11.glDisable(GL20.GL_VERTEX_PROGRAM_POINT_SIZE);
    }
    /**
     * Loads a file located in resoures/shaders and returns the context in a string.
     * @param name The name of the file.
     * @return A string containing the context of the file.
     */
    public static String loadText(String name) {
		// load shader out of program resources
		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		return loadText(classLoader.getResourceAsStream(name));
    }
    
    public static String loadText(InputStream in) {
        
    	if (in == null)
    		return null;
    	
    	// it's faster to append to a StringBuilder than to a String
		final StringBuilder sb = new StringBuilder();
		try {
			
			final BufferedReader br = new BufferedReader(new InputStreamReader(in));

			// read shader line by line
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append('\n');
			}
			
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// return loaded shader
		//System.out.println("return: "+sb.toString());
        return sb.toString();
    }
    /**
     * @return A string representing the maximum possible version of GLSL obtainable in the current gl context.
     */
    public static String getMaxGLSLVersion() {
        return GL11.glGetString(GL20.GL_SHADING_LANGUAGE_VERSION);
    }
}