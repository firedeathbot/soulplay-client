package com.soulplayps.client.opengl.gl20;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.soulplayps.client.data.SignLink;

public class ShaderLoader {


	private static ZipFile zipFile = null;
	private static Map<String, String> shaders = new HashMap<String, String>();
	
	public static final boolean USE_JAR = false;
	
	public static void loadShaders() throws IOException {
		if (USE_JAR) {
			String clientFile = SignLink.cacheDir + "client.jar";
			if (zipFile == null)
				zipFile = new ZipFile(clientFile);
			
			final String[] shaderFiles = new String[]{"shadow.vert", "shadow.frag", "picking.vert", "picking.frag"};
			for (String shader : shaderFiles)
				shaders.put(shader, ShaderUtils.loadText(readFromFile(shader)));
			
			zipFile.close();
			zipFile = null;
		} else {
			final String[] shaderFiles = new String[]{"shadow.vert", "shadow.frag", "picking.vert", "picking.frag"};
			for (String shader : shaderFiles) {
				InputStream inputStream = ClassLoader.getSystemResourceAsStream("resources/shaders/" + shader);
				shaders.put(shader, ShaderUtils.loadText(inputStream));
			}
		}
	}
	
	public static String getShaderCode(String file) {
		try {
			if (shaders.size() <= 0)
				try {
					loadShaders();
				} catch (IOException e) {
					e.printStackTrace();
				}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return shaders.get(file);
	}

	public static InputStream readFromFile(String resource) {
		try {
			final Enumeration<? extends ZipEntry> e = zipFile.entries();

			while (e.hasMoreElements()) {
				final ZipEntry entry = (ZipEntry) e.nextElement();
				if (entry == null)
					continue;

				// if the entry is not directory and matches relative file
				if (!entry.isDirectory() && entry.getName().equals(resource)) {
					return zipFile.getInputStream(entry);
				} else {
					continue;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("[ShaderLoader]: not found: " + resource);
		return null;
	}
	
}
