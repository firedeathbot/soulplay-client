package com.soulplayps.client.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;

public class CompressionUtils {

    public static byte[] degzip(ByteBuffer compressed) throws IOException {
        try (InputStream is = new GZIPInputStream(new ByteArrayInputStream(compressed.array()));
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];

            while (true) {
                int read = is.read(buffer, 0, buffer.length);
                if (read == -1) {
                    break;
                }

                out.write(buffer, 0, read);
            }

            return out.toByteArray();
        }
    }

    private CompressionUtils() {

    }

}
