package com.soulplayps.client.util;

public final class BitwiseHelper {

	public static long ints2Long(int a, int b) {
		return (((long) a) << 32) | (b & 0xffffffffL);
	}

	public static int long2IntA(long l) {
		return (int) (l >> 32);
	}

	public static int long2IntB(long l) {
		return (int) l;
	}

	public static int setBit(int value, int bit) {
		return value | 1 << bit;
	}

	public static int clearBit(int value, int bit) {
		return value & ~(1 << bit);
	}

	public static boolean testBit(int value, int bit) {
		return (value & 1 << bit) != 0;
	}

}
