package com.soulplayps.client.util;

public final class Long2ObjectEntry<T> {
	private final long key;
	private final T value;
	
	public Long2ObjectEntry(long key, T value) {
		this.key = key;
		this.value = value;
	}
	
	public long getKey() {
		return key;
	}
	
	public T getValue() {
		return value;
	}
}
