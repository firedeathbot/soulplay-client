package com.soulplayps.client.util;

public final class Skills {

	public static final int skillsCount = 25;
	public static final String[] skillNames = { "attack", "defence",
			"strength", "hitpoints", "ranged", "prayer", "magic", "cooking",
			"woodcutting", "fletching", "fishing", "firemaking", "crafting",
			"smithing", "mining", "herblore", "agility", "thieving", "slayer",
			"farming", "runecraft", "construction", "hunter", "summoning",
			"dungeoneering", "-unused-", "-unused-" };
	public static final boolean[] skillEnabled = { true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, false, false };

	/**
	 * The skill ids.
	 */
	public static final int ATTACK = 0, DEFENCE = 1, STRENGTH = 2, CONSTITUTION = 3, RANGE = 4, PRAYER = 5, MAGIC = 6, COOKING = 7, 
	WOODCUTTING = 8, FLETCHING = 9, FISHING = 10, FIREMAKING = 11, CRAFTING = 12, SMITHING = 13, MINING = 14, HERBLORE = 15,
	AGILITY = 16, THIEVING = 17, SLAYER = 18, FARMING = 19,RUNECRAFTING = 20, CONSTRUCTION = 21, HUNTER = 22, SUMMONING = 23, DUNGEONEERING = 24;
	
	/**
	 * The skill names.
	 */
	public static final String[] SKILL_NAMES = {
		"Attack", "Defence", "Strength", "Constitution", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching", 
		"Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer", "Farming", 
		"Runecraft", "Construction", "Hunter", "Summoning", "Dungeoneering"
	};
	
}
