package com.soulplayps.client.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JOptionPane;

import com.soulplayps.client.Client;
import com.soulplayps.client.data.SignLink;

public class HttpUpdate {
	
	public static void updateCacheFile(String downloadUrl, String fileName) {
		
		URL url = null;
		
		File file = new File(SignLink.cacheDir + fileName);
		
		try {
			url = new URL(downloadUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		if (url == null) {
			JOptionPane.showMessageDialog(null, "URL not found. " + downloadUrl);
			return;
		}
		
		try {
			if (!file.exists()) {
				File dir = new File(SignLink.cacheDir);
				if (!dir.exists()) {
					if (!dir.mkdir()) {
						JOptionPane.showMessageDialog(null, "Failed to create directory! Report this error to the Staff please.");
					}
				}
			} else {
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.addRequestProperty("User-Agent", "Mozilla/4.76");
				connection.connect();
				long time = connection.getLastModified();
				long length = connection.getContentLengthLong();
				long fileLength = file.length();
				if (time > file.lastModified() || fileLength == 0 || (length != -1 && fileLength != length)) { // not updated
				} else { // updated so do nothing
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return; //failed connection to website, so do nothing
		}
		
		String newFileName = removeExtension(fileName);
		
		OutputStream dest = null;
		HttpURLConnection download;
		InputStream readFileToDownload = null;
		try {
			dest = new BufferedOutputStream(new FileOutputStream(file));
			download = (HttpURLConnection) url.openConnection();
			download.addRequestProperty("User-Agent", "Mozilla/4.76");
			readFileToDownload = download.getInputStream();
			byte[] data = new byte[1024];
			int numRead;
			long numWritten = 0;
			int length = download.getContentLength();
			while ((numRead = readFileToDownload.read(data)) != -1) {
				dest.write(data, 0, numRead);
				numWritten += numRead;
				int percent = (int) (((double) numWritten / (double) length) * 100D);
				//TODO: display in loading bar?
				Client.instance.drawLoadingBar(percent, "Updating " + newFileName);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			try {
				if (readFileToDownload != null)
					readFileToDownload.close();
				if (dest != null)
					dest.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static String removeExtension(String fileName) {
		int charLocation = fileName.lastIndexOf('.');
		if (charLocation <= 0) {
			return fileName;
		}

		return fileName.substring(0, charLocation);
	}
}