package com.soulplayps.client.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;

import static org.apache.commons.codec.binary.Hex.decodeHex;
import static org.apache.commons.codec.binary.Hex.encodeHex;
import static org.apache.commons.io.FileUtils.readFileToByteArray;
import static org.apache.commons.io.FileUtils.writeStringToFile;

import com.soulplayps.client.data.SignLink;

public class Encryption {
	
	public static String encrypt(String password) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnrecoverableKeyException, KeyStoreException, CertificateException, FileNotFoundException, IOException {
		
		SecretKey secKey = grabkey();
		
		Cipher AesCipher = getCipher();
		
		AesCipher.init(Cipher.ENCRYPT_MODE, secKey);
		byte[] encryptedBytes = AesCipher.doFinal(password.getBytes());
		byte[] encodedBytes = new Base64().encode(encryptedBytes);
		
		return new String(encodedBytes);
	}
	
	public static String decrypt(String cipherText) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnrecoverableKeyException, KeyStoreException, CertificateException, FileNotFoundException, IOException {
		
		SecretKey SecKey = grabkey();
		
		Cipher AesCipher = getCipher();
		
		AesCipher.init(Cipher.DECRYPT_MODE, SecKey);
		byte[] decodedValue = new Base64().decode(cipherText.getBytes());
		byte[] decryptedVal = AesCipher.doFinal(decodedValue);
		return new String(decryptedVal);
		
	}
	
	public static Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException {
		return Cipher.getInstance("AES");
	}
	
	
	public static SecretKey grabkey() throws NoSuchAlgorithmException, IOException {
		File file = new File(SignLink.cacheDir + "settings2");
		if (file.exists()) {
			try {
				SecretKey key = loadKey(file);
				return key;
			} catch (Exception ex) {
			}
		} else {
			
			
			SecretKey key = generateKey();
			
			saveKey(key, file);
			
			return key;
		}
		return generateKey();
		
	}
	
	
	public static SecretKey generateKey() throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128); // 128 default; 192 and 256 also possible
		return keyGenerator.generateKey();
	}
	
	public static void saveKey(SecretKey key, File file) throws IOException {
		char[] hex = encodeHex(key.getEncoded());
		writeStringToFile(file, String.valueOf(hex), Charset.defaultCharset());
	}
	
	public static SecretKey loadKey(File file) throws IOException {
		String data = new String(readFileToByteArray(file));
		byte[] encoded;
		try {
			encoded = decodeHex(data.toCharArray());
		} catch (DecoderException e) {
			e.printStackTrace();
			return null;
		}
		return new SecretKeySpec(encoded, "AES");
	}
	
}
