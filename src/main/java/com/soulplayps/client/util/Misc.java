package com.soulplayps.client.util;

import com.soulplayps.client.node.io.RSBuffer;

public final class Misc {
	
	private Misc() {
		throw new UnsupportedOperationException();
	}

	public static boolean isPointInCircle(double x, double y, double centerX, double centerY, double radius) {
		return Math.pow(x - centerX, 2) + Math.pow(y - centerY, 2) < Math.pow(radius, 2);
	}

	public static String getFormattedTime(int time) {
		StringBuilder builder = new StringBuilder();
		
		int seconds = time / 50;
		int minutes = seconds / 60;
		seconds %= 60;

		builder.append(minutes).append(":");

		if (seconds < 10) {
			builder.append("0");
		}

		builder.append(seconds);

		return builder.toString();
	}

	public static void createFrameProxyToHere(RSBuffer stream, int i) {
		stream.buffer[stream.currentOffset++] = (byte) (i + stream.encryption.getNextKey());
	}
	
	public static void processProxyKeys(RSBuffer stream) {
		byte[] abyte1 = ProcessKeys.processBigInts(stream);
		stream.writeByte(abyte1.length);
		stream.writeBytes(abyte1, abyte1.length, 0);
	}

	public static int interpolate(int outputMin, int outputMax, int inputMin, int inputMax, int input) {
		return outputMin + (outputMax - outputMin) * (input - inputMin) / (inputMax - inputMin);
	}

	public static String optimizeText(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (i == 0) {
				s = String.format("%s%s", Character.toUpperCase(s.charAt(0)),
						s.substring(1));
			}
			if (!Character.isLetterOrDigit(s.charAt(i))) {
				if (i + 1 < s.length()) {
					s = String.format("%s%s%s", s.subSequence(0, i + 1),
							Character.toUpperCase(s.charAt(i + 1)),
							s.substring(i + 2));
				}
			}
		}
		return s;
	}
	
}
