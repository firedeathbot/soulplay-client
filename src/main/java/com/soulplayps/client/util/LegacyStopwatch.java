package com.soulplayps.client.util;

public class LegacyStopwatch {

	private long time;

	public LegacyStopwatch reset(long i) {
		time = i;
		return this;
	}

	public LegacyStopwatch reset() {
		time = System.currentTimeMillis();
		return this;
	}

	public long elapsed() {
		return System.currentTimeMillis() - time;
	}

	public boolean elapsed(long time) {
		return elapsed() >= time;
	}
	
	public long getTime() {
		return time;
	}
	
	public LegacyStopwatch() {

	}
}