package com.soulplayps.client.util;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.settings.Settings;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class HotKeys extends JDialog {
	
	public static boolean ESC_CLOSE = (Boolean) Settings.getSettingOr("escapeClose", false);
	
	private static boolean STANDALONE = false;
	
	public static void main(String[] args) {
		STANDALONE = true;
		new HotKeys(null).setVisible(true);
	}
	
	public static void saveAll() {
		for (HotkeyIcon i : ICONS) {
			Settings.changeSetting("" + i.spriteId, i.key);
		}
		Settings.changeSetting("escapeClose", ESC_CLOSE);
	}
	
	public static void resetIcons() {
		for (HotkeyIcon i : ICONS) {
			i.reset();
		}
		saveAll();
	}
	
	public void center() {
		setLocation(Client.instance.frame.getX()
				+ (Client.isFixed() ? 17 : (Client.clientWidth / 2) - 356),
				Client.instance.getYStart()
				+ (Client.isFixed() ? 15 : (Client.clientHeight / 2) - 267));
	}
	
	public static void initIcons() {
		ICONS = new LinkedList<HotkeyIcon>() {{
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 881, KeyEvent.VK_F1), KeyEvent.VK_F1, 881, 3));//INV Tab
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 898, KeyEvent.VK_F2), KeyEvent.VK_F2, 898, 4));//Equip
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 896, KeyEvent.VK_F3), KeyEvent.VK_F3, 896, 5));//prayer
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 906, KeyEvent.VK_F4), KeyEvent.VK_F4, 906, 6));//MAGIC
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 878, KeyEvent.VK_F5), KeyEvent.VK_F5, 878, 0));//combat tab
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 908, KeyEvent.VK_F6), KeyEvent.VK_F6, 908, 1));//skill tab
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 883, KeyEvent.VK_F7), KeyEvent.VK_F7, 883, 7));//Clan Tabz
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 876, KeyEvent.VK_F8), KeyEvent.VK_F8, 876, 8));//Friends Tab
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 900, KeyEvent.VK_F9), KeyEvent.VK_F9, 900, 2));//Quest Tab
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 904, KeyEvent.VK_F10), KeyEvent.VK_F10, 904, 11));//Settings Tab
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 891, -1), -1, 891, 9));//Ignores Tab
			
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 990, -1), -1, 990, 15));//Special hotkey
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 1310, -1), -1, 1310, 16));//Special hotkey
			add(new HotkeyIcon((Integer) Settings.getSettingOr("" + 994, -1), -1, 994, 17));//Special hotkey
		}};
	}
	
	public static LinkedList<HotkeyIcon> ICONS;
	
	public HotKeys(Frame parent) {
		super(parent);
		setLocationRelativeTo(parent);
		
		if (STANDALONE) {
			SpriteCache.init();
		} else {
			Client.instance.addHotKeyInfo();
		}
		initIcons();
		
		setType(Type.UTILITY);
		setUndecorated(true);
		setResizable(false);
		setSize(502, 300);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		setContentPane(new JLabel(new ImageIcon(SpriteCache.get(880).toImage())));
		getContentPane().setLayout(null);
		
		JButton close = new JButton();
		close.setFocusPainted(false);
		close.setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
		close.setContentAreaFilled(false);
		close.setIcon(new ImageIcon(SpriteCache.get(996).toImage()));
		close.setBounds(474, 7, 21, 21);
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						dispose();
					}
				});
			}
		});
		getContentPane().add(close);
		
		int x = 10;
		int y = 35;
		
		int x2 = 54;
		int y2 = y + 12;
		int i = 0;
		for (HotkeyIcon icon : ICONS) {
			JLabel l = new JLabel(new ImageIcon(SpriteCache.get(icon.spriteId).toImage()));
			icon.init();
			
			if (i == 5) {
				x += 160;
				y = 35;
				
				x2 += 160;
				y2 = y + 12;
				
				i = 0;
			}
			
			l.setBounds(x, y, 44, 43);
			getContentPane().add(l);
			
			icon.setBounds(x2, y2, 100, 20);
			getContentPane().add(icon);
			
			y += 44;
			y2 += 44;
			
			i++;
		}
		
		JButton setDefaults = new JButton(new ImageIcon(SpriteCache.get(902).toImage()));
		setDefaults.setFocusPainted(false);
		setDefaults.setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
		setDefaults.setContentAreaFilled(false);
		setDefaults.setPressedIcon(new ImageIcon(SpriteCache.get(903).toImage()));
		setDefaults.setBounds(335, 255, 150, 30);
		setDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						resetIcons();
					}
				});
			}
		});
		getContentPane().add(setDefaults);
		
		final JCheckBox escClose = new JCheckBox("   Esc closes current interface", new ImageIcon(SpriteCache.get(980).toImage()));
		escClose.setFocusPainted(false);
		escClose.setSelected(ESC_CLOSE);
		escClose.setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
		escClose.setContentAreaFilled(false);
		escClose.setForeground(new Color(255, 138, 31));
		escClose.setSelectedIcon(new ImageIcon(SpriteCache.get(981).toImage()));
		escClose.setBounds(15, 260, 300, 30);
		escClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						ESC_CLOSE = !ESC_CLOSE;
						saveAll();
					}
				});
			}
		});
		getContentPane().add(escClose);
	}
	
}
