package com.soulplayps.client.util;

import com.soulplayps.client.music.Class51;
import com.soulplayps.client.node.io.RSBuffer;

import java.math.BigInteger;

public class ProcessKeys {
	
	public static byte[] processBigInts(RSBuffer stream) {
		int i = stream.currentOffset;
		stream.currentOffset = 0;
		byte abyte0[] = new byte[i];
		stream.readBytes(i, 0, abyte0);
		BigInteger biginteger2 = new BigInteger(abyte0);
		//BigInteger biginteger3 = biginteger2/* .modPow(biginteger, biginteger1) */;
		BigInteger biginteger3 = biginteger2.modPow(Class51.RSA_EXPONENT, SizeConstants.getRsaModulus());
		byte abyte1[] = biginteger3.toByteArray();
		stream.currentOffset = 0;
		return abyte1;
	}

}
