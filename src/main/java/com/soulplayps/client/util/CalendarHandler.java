package com.soulplayps.client.util;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class CalendarHandler {
	
	/**
	 * Creates a new instance of the GregorianCalendar
	 */
	String[] ids;
	SimpleTimeZone pdt;
	GregorianCalendar cal;
	
	/**
	 * Set the GMT offset
	 */
	private final int GMT = -5;
	
	public CalendarHandler() {
		ids = TimeZone.getAvailableIDs(GMT * 60 * 60 * 1000);
		pdt = new SimpleTimeZone(GMT * 60 * 60 * 1000, ids[0]);
		pdt.setStartRule(Calendar.JANUARY, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
		pdt.setEndRule(Calendar.DECEMBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
		cal = new GregorianCalendar(pdt);
		refreshCalendar();
	}
	
	/**
	 * Holds the names of the days of the week
	 */
	public static final String[] DAYS_NA = {
			"Sunday","Monday","Tuesday",
			"Wednesday","Thursday","Friday",
			"Saturday"
	};
	
	public static final String[] DAYS_SHORT = {
			"Sun","Mon","Tues",
			"Wed","Thurs","Fri",
			"Sat"
	};
	
	/**
	 * Holds the names of the months of the year
	 */
	private static final String[] MONTHS = {
			"january","febuary","march","april",
			"may","june","july","august",
			"september","october","november","december"
	};
	
	/**
	 * Sets all of the proper calendar variables
	 */
	public void refreshCalendar() {
		cal.setTimeInMillis(System.currentTimeMillis());
	}

	/**
	 * Gets the name of the current day of the week
	 */
	private String getDayName(int day) {
		return Misc.optimizeText(DAYS_NA[day - 1]);
	}
	
	/**
	 * Gets the name of the current month of the year
	 */
	private String getMonthName(int month) {
		return Misc.optimizeText(MONTHS[month]);
	}
	
	/**
	 * Returns true if it is the given day
	 */
	public boolean isDay(int day) {
		return (cal.get(GregorianCalendar.DAY_OF_WEEK) == cal.get(day));
	}
	
	/**
	 * Returns true if it is the given month
	 */
	public boolean isMonth(int month) {
		return (cal.get(GregorianCalendar.MONTH) == cal.get(month));
	}
	
	/**
	 * Gets the current day of the week
	 */
	public String getDayOfWeek() {
		return getDayName(currentDayOfWeek());
	}
	
	/**
	 * Gets the current month of the year
	 */
	public String getMonth() {
		return getMonthName(currentMonth());
	}
	
	/**
	 * Gets the current day of the month with appropriate addons
	 */
	public String getDayOfMonth(int day) {
		String addOn = "th";
		if(day == 1)
			addOn = "st";
		else if (day == 2)
			addOn = "nd";
		else if (day == 3)
			addOn = "rd";
		return Integer.toString(day) + addOn;
	}
	
	/**
	 * Gets the current hour of the day
	 */
	public int getHour() {
		return cal.get(GregorianCalendar.HOUR);
	}
	
	public int get24Hour() {
		return cal.get(GregorianCalendar.HOUR_OF_DAY);
	}
	
	/**
	 * Gets AM or PM
	 */
	public String getHourType() {
		if(cal.get(GregorianCalendar.AM_PM) == 0)
			return "AM";
		else
			return "PM";
	}
	
	/**
	 * Gets the current minutes of the day
	 */
	public int getMinutes() {
		return cal.get(GregorianCalendar.MINUTE);
	}
	
	/**
	 * Returns the whole date
	 * Example: Today is Monday, April 29.
	 */
	public String getLongVersion() {
		return "Today is "+getDayName(currentDayOfWeek())+", "+getMonthName(currentMonth())+" "+getDayOfMonth(currentDayOfMonth())+".";
	}
	
	/**
	 * Returns the current time
	 */
	public String getTime() {
		return getHour() + ":" + getMinutes() + getHourType();
	}
	
	public String get24HourTime() {
		return get24Hour() + ":" + (getMinutes() < 10 ? "0"+getMinutes() : getMinutes());
	}
	
	/**
	 * Returns the current day id
	 */
	public int currentDayOfWeek() {
		return cal.get(GregorianCalendar.DAY_OF_WEEK);
	}
	
	/**
	 * Returns the current month id
	 */
	private int currentMonth() {
		return cal.get(GregorianCalendar.MONTH);
	}
	
	/**
	 * Returns the current day of the month
	 */
	public int currentDayOfMonth() {
		return cal.get(GregorianCalendar.DAY_OF_MONTH);
	}
	
	public int getYear() {
		return cal.get(GregorianCalendar.YEAR);
	}
	
	public int getWeekOfYear() {
		return cal.get(GregorianCalendar.WEEK_OF_YEAR);
	}
	
	public int getDayOfYear() {
		return cal.get(GregorianCalendar.DAY_OF_YEAR);
	}
}