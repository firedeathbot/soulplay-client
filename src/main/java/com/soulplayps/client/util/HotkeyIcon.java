package com.soulplayps.client.util;

import com.soulplayps.client.node.raster.sprite.SpriteCache;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public final class HotkeyIcon extends JButton {
	
	public int key;
	public final int defaultKey;
	public final int spriteId;
	public final int tabId;
	
	boolean listen = false;
	
	public HotkeyIcon(int key, int defaultKey, int spriteId, int tabId) {
		this.key = key;
		this.defaultKey = defaultKey;
		this.spriteId = spriteId;
		this.tabId = tabId;
	}
	
	public void reset() {
		key = defaultKey;
		setText(toString());
	}
	
	public void init() {
		setText(HotkeyIcon.this.toString());
		setFocusPainted(false);
		setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
		setContentAreaFilled(false);
		setIcon(new ImageIcon(SpriteCache.get(988).toImage()));
		setSelectedIcon(new ImageIcon(SpriteCache.get(283).toImage()));
		setForeground(new Color(255, 138, 31));
		setFont(new Font("Dialog", Font.PLAIN, 11));
		setHorizontalTextPosition(SwingConstants.CENTER);
		
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						setText("Select a hotkey...");
						listen = true;
					}
				});
			}
			
		});
		
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (!listen) return;
				
				HotkeyIcon.this.key = e.getKeyCode();
				if (e.getKeyCode() >= KeyEvent.VK_F1 && e.getKeyCode() <= KeyEvent.VK_F12 || e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					for (HotkeyIcon i : HotKeys.ICONS) {
						if (i != HotkeyIcon.this && i.key == HotkeyIcon.this.key) {
							i.key = -1;
							i.setText(i.toString());
							break;
						}
					}
				} else {
					HotkeyIcon.this.key = -1;
				}
				setText(HotkeyIcon.this.toString());
				listen = false;
				HotKeys.saveAll();
			}
		});
		
		addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				
			}
			
			@Override
			public void focusLost(FocusEvent e) {
				setText(HotkeyIcon.this.toString());
			}
		});
	}
	
	@Override
	public String toString() {
		return key == -1 ? "None" : KeyEvent.getKeyText(key);
	}
	
}
