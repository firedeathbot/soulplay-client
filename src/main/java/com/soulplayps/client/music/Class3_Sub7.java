package com.soulplayps.client.music;

import com.soulplayps.client.node.Node;

public abstract class Class3_Sub7 extends Node
{
    Class3_Sub9 aClass3_Sub9_1220;
    boolean aBoolean1221;
    
    public abstract int method378(int[] is, int i, int i_0_);
    
    int method379() {
	return 255;
    }
    
    public Class3_Sub7() {
	/* empty */
    }
    
    public abstract void method380(int i);
}
