package com.soulplayps.client.unknown;

public final class Vertex {

	public Vertex() {
	}

	public int x;
	public int y;
	public int z;
	public int length;
}
