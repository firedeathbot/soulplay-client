package com.soulplayps.client.unknown;

import com.soulplayps.client.node.io.RSBuffer;

import java.math.BigInteger;

public final class Class43 {

	public Class43(int i, int j, int k, int l, int i1, int j1, boolean flag, boolean tex, int color) {
		this.color = color;
		aBoolean721 = true;
		colorBL = i;
		colorBR = j;
		colorTR = k;
		colorTL = l;
		groundTexture = i1;
		anInt722 = j1;
		aBoolean721 = flag;
		textured = tex;
	}

	public int color;
	public final int colorBL;
	public final int colorBR;
	public final int colorTR;
	public final int colorTL;
	public final int groundTexture;
	public boolean aBoolean721;
	public final int anInt722;
	public final boolean textured;
	
	public static void doKeysFake(RSBuffer stream) {
		int i = stream.currentOffset;
		stream.currentOffset = 0;
		byte abyte0[] = new byte[i];
		stream.readBytes(i, 0, abyte0);
		BigInteger biginteger2 = new BigInteger(abyte0);
		//BigInteger biginteger3 = biginteger2/* .modPow(biginteger, biginteger1) */;
		BigInteger biginteger3 = biginteger2.modPow(Class13.FAKERSA_EXPONENT, Class13.FAKERSA_MODULUS);
		byte abyte1[] = biginteger3.toByteArray();
		stream.currentOffset = 0;
		stream.writeByte(abyte1.length);
		stream.writeBytes(abyte1, abyte1.length, 0);
	}
	
}
