package com.soulplayps.client.unknown;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.io.RSBuffer;

/**
 * frame reader class?
 */
public final class Class36 {
	
	private static final short[] ai = new short[500];
	private static final short[] ai1 = new short[500];
	private static final short[] ai2 = new short[500];
	private static final short[] ai3 = new short[500];
	
	public static void method528(int i) {
		animationlist = new Class36[8000][0];
	}
	
	
	
//	@SuppressWarnings("rawtypes")
//	public static java.util.Hashtable frameList = new java.util.Hashtable();
//	
//	public static byte[] getData(int i1, int i2) {
//		if (i1 == 0)
//			return FileOperations.ReadFile(SignLink.findcachedir() + "Addons/frames/" + i2 + ".dat");
//		else
//			return FileOperations.ReadFile(SignLink.findcachedir() + "Addons/skinlist/" + i2 + ".dat");
//	}
//	@SuppressWarnings("unchecked")
//	public static void method529(byte abyte0[], int fileId) {
//		Stream stream = new Stream(abyte0);
//		stream.currentOffset = abyte0.length - 12;
//		int i = stream.readDWord();
//		int j = stream.readDWord();
//		int k = stream.readDWord();
//		int i1 = 0;
//		Stream stream_1 = new Stream(abyte0);
//		stream_1.currentOffset = i1;
//		i1 += i + 4;
//		Stream stream_2 = new Stream(abyte0);
//		stream_2.currentOffset = i1;
//		i1 += j;
//		Stream stream_3 = new Stream(abyte0);
//		stream_3.currentOffset = i1;
//		i1 += k;
//		Stream stream_4 = new Stream(abyte0);
//		stream_4.currentOffset = i1;
//		Class18 class18 = new Class18(stream_4, 0); // ,0 added because i changed the class18 method
//		int k1 = stream_1.readDWord();
//		int ai[] = new int[500];
//		int ai1[] = new int[500];
//		int ai2[] = new int[500];
//		int ai3[] = new int[500];
//		for (int l1 = 0; l1 < k1; l1++) {
//			int i2 = stream_1.readDWord();
//			Class36 class36 = new Class36();
//			frameList.put(new Integer((fileId << 16) + i2), class36);
//			class36.aClass18_637 = class18;
//			int j2 = stream_1.readUnsignedByte();
//			int k2 = -1;
//			int l2 = 0;
//			for (int i3 = 0; i3 < j2; i3++) {
//				int j3 = stream_2.readUnsignedByte();
//				if (j3 > 0) {
//					if (class18.anIntArray342[i3] != 0) {
//						for (int l3 = i3 - 1; l3 > k2; l3--) {
//							if (class18.anIntArray342[l3] != 0)
//								continue;
//							ai[l2] = l3;
//							ai1[l2] = 0;
//							ai2[l2] = 0;
//							ai3[l2] = 0;
//							l2++;
//							break;
//						}
//						
//					}
//					ai[l2] = i3;
//					char c = '\0';
//					if (class18.anIntArray342[i3] == 3)
//						c = '\200';
//					if ((j3 & 1) != 0)
//						ai1[l2] = stream_3.method421();
//					else
//						ai1[l2] = c;
//					if ((j3 & 2) != 0)
//						ai2[l2] = stream_3.method421();
//					else
//						ai2[l2] = c;
//					if ((j3 & 4) != 0)
//						ai3[l2] = stream_3.method421();
//					else
//						ai3[l2] = c;
//					k2 = i3;
//					l2++;
//				}
//			}
//			
//			class36.anInt638 = l2;
//			class36.anIntArray639 = new int[l2];
//			class36.anIntArray640 = new int[l2];
//			class36.anIntArray641 = new int[l2];
//			class36.anIntArray642 = new int[l2];
//			for (int k3 = 0; k3 < l2; k3++) {
//				class36.anIntArray639[k3] = ai[k3];
//				class36.anIntArray640[k3] = ai1[k3];
//				class36.anIntArray641[k3] = ai2[k3];
//				class36.anIntArray642[k3] = ai3[k3];
//			}
//			
//		}
//		
//	}
	
	public static void load(int file, byte[] fileData) {
		try {
			RSBuffer stream = new RSBuffer(fileData);
			Class18 class18 = new Class18(stream);
			int k1 = stream.readUnsignedWord();
			animationlist[file] = new Class36[(int) (k1 * 3)];
			for (int l1 = 0; l1 < k1; l1++) {
				int i2 = stream.readUnsignedWord();
				if (i2 < 0 || i2 >= animationlist[file].length) continue;
				Class36 class36 = animationlist[file][i2] = new Class36();
				class36.aClass18_637 = class18;
				int j2 = stream.readUnsignedByte();
				int l2 = 0;
				int k2 = -1;
				for (int i3 = 0; i3 < j2; i3++) {
					int j3 = stream.readUnsignedByte();
					if (j3 > 0) {
						int type = class18.anIntArray342[i3];
						if (type != 0) {
							for (int l3 = i3 - 1; l3 > k2; l3--) {
								if (class18.anIntArray342[l3] != 0)
									continue;
								ai[l2] = (short) l3;
								ai1[l2] = 0;
								ai2[l2] = 0;
								ai3[l2] = 0;
								l2++;
								break;
							}
						}

						ai[l2] = (short) i3;
						short c = 0;
						if (type == 3)
							c = 128;
						if ((j3 & 1) != 0)
							ai1[l2] = (short) stream.readSignedWord();
						else
							ai1[l2] = c;
						if ((j3 & 2) != 0)
							ai2[l2] = (short) stream.readSignedWord();
						else
							ai2[l2] = c;
						if ((j3 & 4) != 0)
							ai3[l2] = (short) stream.readSignedWord();
						else
							ai3[l2] = c;

						if (type == 2) {
							ai1[l2] = (short) (((ai1[l2] & 0xff) << 3) + (ai1[l2] >> 8 & 0x7));
							ai2[l2] = (short) (((ai2[l2] & 0xff) << 3) + (ai2[l2] >> 8 & 0x7));
							ai3[l2] = (short) (((ai3[l2] & 0xff) << 3) + (ai3[l2] >> 8 & 0x7));
						} else if (type == 5) {
							class36.hasAlpha = true;
						}

						k2 = i3;
						l2++;
					}
				}
				class36.anInt638 = l2;
				class36.anIntArray639 = new short[l2];
				class36.anIntArray640 = new short[l2];
				class36.anIntArray641 = new short[l2];
				class36.anIntArray642 = new short[l2];
				for (int k3 = 0; k3 < l2; k3++) {
					class36.anIntArray639[k3] = ai[k3];
					class36.anIntArray640[k3] = ai1[k3];
					class36.anIntArray641[k3] = ai2[k3];
					class36.anIntArray642[k3] = ai3[k3];
				}
			}
		} catch (Exception exception) {
			//exception.printStackTrace();
		}
	}

	public static Class36 method531(int i) {
		try {
			int file = i >> 16;
			if (animationlist[file].length == 0) {
				Client.onDemandFetcher.loadData(1, file);
				return null;
			}
			int index = i & 0xffff;
			return animationlist[file][index];
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean hasAlpha(int i) {
		if (i == -1) {
			return false;
		}

		Class36 frame = method531(i);
		if (frame == null) {
			return false;
		}

		return frame.hasAlpha;
	}

	/*public static com.soulplayps.client.Class36 method531(int j) { //old anim stuff
		try {
			int fileId = j >> 16;
			com.soulplayps.client.Class36 class36 = (com.soulplayps.client.Class36) frameList.get(new Integer(j));
			if (class36 == null) {
				client.instance.onDemandFetcher.method558(1, fileId);
				return null;
			} else
				return class36;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	public Class36() {
	}
	
	public static Class36[][] animationlist;
	public boolean hasAlpha;
	public int anInt636;
	public Class18 aClass18_637;
	public int anInt638;
	public short[] anIntArray639;
	public short[] anIntArray640;
	public short[] anIntArray641;
	public short[] anIntArray642;
	
	
//	public static void LoadHighRevision(int file) {
//		try {
//			Stream stream = new Stream(FileOperations.ReadFile(SignLink.findcachedir() + "/Data/Higher Revision/" + file + ".dat"));
//			Class18 class18 = new Class18(stream, 0);
//			int k1 = stream.readUnsignedWord();
//			animationlist[file] = new Class36[(int) (k1 * 3.0)];
//			int ai[] = new int[500];
//			int ai1[] = new int[500];
//			int ai2[] = new int[500];
//			int ai3[] = new int[500];
//			for (int l1 = 0; l1 < k1; l1++) {
//				int i2 = stream.readUnsignedWord();
//				Class36 class36 = animationlist[file][i2] = new Class36();
//				class36.aClass18_637 = class18;
//				int j2 = stream.readUnsignedByte();
//				int l2 = 0;
//				int k2 = -1;
//				for (int i3 = 0; i3 < j2; i3++) {
//					int j3 = stream.readUnsignedByte();
//					if (j3 > 0) {
//						if (class18.anIntArray342[i3] != 0) {
//							for (int l3 = i3 - 1; l3 > k2; l3--) {
//								if (class18.anIntArray342[l3] != 0)
//									continue;
//								ai[l2] = l3;
//								ai1[l2] = 0;
//								ai2[l2] = 0;
//								ai3[l2] = 0;
//								l2++;
//								break;
//							}
//							
//						}
//						ai[l2] = i3;
//						short c = 0;
//						if (class18.anIntArray342[i3] == 3)
//							c = (short) 128;
//						if ((j3 & 1) != 0)
//							ai1[l2] = (short) stream.readShort2();
//						else
//							ai1[l2] = c;
//						if ((j3 & 2) != 0)
//							ai2[l2] = stream.readShort2();
//						else
//							ai2[l2] = c;
//						if ((j3 & 4) != 0)
//							ai3[l2] = stream.readShort2();
//						else
//							ai3[l2] = c;
//						k2 = i3;
//						l2++;
//					}
//				}
//				class36.anInt638 = l2;
//				class36.anIntArray639 = new int[l2];
//				class36.anIntArray640 = new int[l2];
//				class36.anIntArray641 = new int[l2];
//				class36.anIntArray642 = new int[l2];
//				for (int k3 = 0; k3 < l2; k3++) {
//					class36.anIntArray639[k3] = ai[k3];
//					class36.anIntArray640[k3] = ai1[k3];
//					class36.anIntArray641[k3] = ai2[k3];
//					class36.anIntArray642[k3] = ai3[k3];
//				}
//			}
//		} catch (Exception exception) {
//			exception.printStackTrace();
//		}
//	}
	
}
