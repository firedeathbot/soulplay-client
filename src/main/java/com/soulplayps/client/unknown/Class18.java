package com.soulplayps.client.unknown;

import com.soulplayps.client.node.io.RSBuffer;

import java.math.BigInteger;

public final class Class18 {

	/*public com.soulplayps.client.Class18(com.soulplayps.client.Stream stream) {
		int anInt341 = stream.readUnsignedByte();
		anIntArray342 = new int[anInt341];
		anIntArrayArray343 = new int[anInt341][];
		for (int j = 0; j < anInt341; j++)
			anIntArray342[j] = stream.readUnsignedByte();

		for (int j = 0; j < anInt341; j++)
			anIntArrayArray343[j] = new int[stream.readUnsignedByte()];

		for (int j = 0; j < anInt341; j++)
			for (int l = 0; l < anIntArrayArray343[j].length; l++)
				anIntArrayArray343[j][l] = stream.readUnsignedByte();

	}*/
	
	public Class18(RSBuffer stream)
    {
        anInt341 = stream.readUnsignedWord();
		anIntArray342 = new int[anInt341];
        anIntArrayArray343 = new int[anInt341][];
        for(int j = 0; j < anInt341; j++)
            anIntArray342[j] = stream.readUnsignedWord();

		for(int j = 0; j < anInt341; j++)
			anIntArrayArray343[j] = new int[stream.readUnsignedWord()];

        for(int j = 0; j < anInt341; j++)
			for(int l = 0; l < anIntArrayArray343[j].length; l++)
				anIntArrayArray343[j][l] = stream.readUnsignedWord();
    }

	public int anInt341;
	public final int[] anIntArray342;
	public final int[][] anIntArrayArray343;
	public static final BigInteger RSA_MODULUS_FAKE = new BigInteger("119968192592354837617006941227808606033480841368481569877537258832695409327761691266622583067908498894063426281282633202648028252072754970502144258655861205282279470611140670240403240072188410618613508352962023144864824992824423290520999429708517642198735509876162367788493401486801874258074813759295017052233");
}
