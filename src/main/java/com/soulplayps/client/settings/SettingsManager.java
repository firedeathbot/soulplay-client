package com.soulplayps.client.settings;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class SettingsManager extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7269932974643360247L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
//	public static void init() {
////		EventQueue.invokeLater(new Runnable() {
////			public void run() {
//				try {
//					SettingsManager frame = new SettingsManager();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
////			}
////		});
//	}

	/**
	 * Create the frame.
	 */
	public SettingsManager() {
		setTitle("Settings");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 250, 225);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBounds(new Rectangle(100, 100, 100, 100));
		contentPane.setAlignmentY(Component.TOP_ALIGNMENT);
		contentPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 1, 1));
		
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10, 0, 0, 0));
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWeights = new double[]{0.0, 0.0};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		panel.setLayout(gbl_panel);
		
		setResizable(false);
		
		/*Image img = Toolkit.getDefaultToolkit().getImage(SignLink.findcachedir() + "Sprites/favicon.png");
		if (img == null) {
			System.out.println("Image is null");
			return;
		}
		setIconImage(img);*/
		
		JButton btnOpenCache = new JButton("Open Cache");
		btnOpenCache.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Client.instance.openCacheFolder();
					}
				});
			}
		});
		
		JLabel lblClientVersion = new JLabel("Client Version: "+Config.CLIENT_VERSION+"."+Config.CLIENT_VERSION_SUB);
		GridBagConstraints gbc_lblClientVersion = new GridBagConstraints();
		gbc_lblClientVersion.insets = new Insets(0, 0, 5, 5);
		gbc_lblClientVersion.gridx = 0;
		gbc_lblClientVersion.gridy = 0;
		panel.add(lblClientVersion, gbc_lblClientVersion);
		
		JCheckBox chckbxDisplaySnow = new JCheckBox("Display Snow");
		GridBagConstraints gbc_chckbxDisplaySnow = new GridBagConstraints();
		gbc_chckbxDisplaySnow.anchor = GridBagConstraints.WEST;
		gbc_chckbxDisplaySnow.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxDisplaySnow.gridx = 0;
		gbc_chckbxDisplaySnow.gridy = 1;
		panel.add(chckbxDisplaySnow, gbc_chckbxDisplaySnow);
		
		chckbxDisplaySnow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Settings.toggleSetting("display_snow");
						Client.instance.setSettings();
					}
				});
			}
		});
		chckbxDisplaySnow.setSelected((Boolean)Settings.getSetting("display_snow"));
		GridBagConstraints gbc_btnOpenCache = new GridBagConstraints();
		gbc_btnOpenCache.insets = new Insets(0, 0, 5, 0);
		gbc_btnOpenCache.gridx = 1;
		gbc_btnOpenCache.gridy = 1;
		panel.add(btnOpenCache, gbc_btnOpenCache);
		
		JButton btnClearCache = new JButton("Clear Cache");
		btnClearCache.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
//				String prompt = JOptionPane.showInputDialog("To clear "+com.soulplayps.client.Config.SERVER_NAME+" Cache type in: YES");
//				if(prompt != null && prompt.toLowerCase().equals("yes")) {
//					client.deleteDir(new File(signlink.findcachedir()));
//				} else {
//					JOptionPane.showMessageDialog(null, "You decided not to clear Cache.");
//				}
				Client.instance.sendCacheFix();
					}
				});
			}
		});
		
		JCheckBox chckbxReply = new JCheckBox("Auto-add Reply");
		GridBagConstraints gbc_chckbxReply = new GridBagConstraints();
		gbc_chckbxReply.anchor = GridBagConstraints.WEST;
		gbc_chckbxReply.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxReply.gridx = 0;
		gbc_chckbxReply.gridy = 2;
		panel.add(chckbxReply, gbc_chckbxReply);
		
		chckbxReply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Settings.toggleSetting("auto_add_reply");
						Client.instance.setSettings();
					}
				});
			}
		});
		chckbxReply.setSelected((Boolean)Settings.getSetting("auto_add_reply"));
		GridBagConstraints gbc_btnClearCache = new GridBagConstraints();
		gbc_btnClearCache.insets = new Insets(0, 0, 5, 0);
		gbc_btnClearCache.gridx = 1;
		gbc_btnClearCache.gridy = 2;
		panel.add(btnClearCache, gbc_btnClearCache);
		
		JButton btnScreenshotDir = new JButton("ScreenShot Dir");
		btnScreenshotDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Client.instance.setScreenShotDir();
					}
				});
			}
		});
		
		JCheckBox chckbxOpengl = new JCheckBox("OpenGL");
		GridBagConstraints gbc_chckbxOpengl = new GridBagConstraints();
		gbc_chckbxOpengl.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxOpengl.gridx = 0;
		gbc_chckbxOpengl.gridy = 3;
		panel.add(chckbxOpengl, gbc_chckbxOpengl);
		GridBagConstraints gbc_btnScreenshotDir = new GridBagConstraints();
		gbc_btnScreenshotDir.insets = new Insets(0, 0, 5, 0);
		gbc_btnScreenshotDir.gridx = 1;
		gbc_btnScreenshotDir.gridy = 3;
		panel.add(btnScreenshotDir, gbc_btnScreenshotDir);
		chckbxOpengl.setSelected((Boolean) Settings.getSetting("opengl_enabled"));
		chckbxOpengl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Settings.toggleSetting("opengl_enabled");//just in case
						Client.toggleGl.set(true);
					}
				});
			}
		});
		
		JButton btnClosesave = new JButton("Close/Save");
		btnClosesave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						dispose();
					}
				});
			}
		});
		GridBagConstraints gbc_btnClosesave = new GridBagConstraints();
		gbc_btnClosesave.gridx = 1;
		gbc_btnClosesave.gridy = 5;
		panel.add(btnClosesave, gbc_btnClosesave);
		
	}

}
