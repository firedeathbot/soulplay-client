package com.soulplayps.client.settings;

import com.soulplayps.client.data.SignLink;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Hashtable;

/**
 * The settings class will manage all settings stored by the client. The client
 * will refer to, change, and remove settings as it needs to. Adding a new client
 * setting is extremely easy and changing them is even easier due to the fact that
 * the com.soulplayps.client.Settings class will remove old or nulled settings still stored in the settings
 * file, and it verifies each setting to make sure it's a legitimate setting (to check
 * if you still require the usage of that setting).
 * @author Galkon
 *
 */
public class Settings {

    /**
     * The settings stored created and stored by the client.
     */
    public static Hashtable<String, Object> settings = new Hashtable<String, Object>();

    /**
     * The values of the settings.
     */
    public static Object[][] settingValues = {
            //{ "display_world_background", false },
            //{ "play_client_sounds", false },
            //{ "quick_login", false },
    		{ "username" , "" },
            { "new_hp", true },
            { "new_hitmarks", true },
            { "hpModifier", 1 },
            { "roofs_off", false },
            { "left_click_attack", false },
            { "split_pm", 0 },
            { "display_ping", false },
            { "display_fps", false },
            { "display_snow", false },
            { "display_timeplayed", true},
            { "auto_add_reply", false },
            { "screenshot_dir", "" },
            { "fog", false },
            { "particles", false },
            { "prev_x_amount", 100 },
            { "hd_textures", false },
            { "display_config", 900 },
            { "display_orbs", true },
            { "ground_decor", true },
            { "hd_water", true },
            { "animate_bg", false },
            { "remember_profile", 0 },
            { "shift_drop", false },
            { "combat_box", false },
            { "881", KeyEvent.VK_F1 },
            { "898", KeyEvent.VK_F2 },
            { "896", KeyEvent.VK_F3 },
            { "906", KeyEvent.VK_F4 },
            { "878", KeyEvent.VK_F5 },
            { "908", KeyEvent.VK_F6 },
            { "883", KeyEvent.VK_F7 },
            { "876", KeyEvent.VK_F8 },
            { "900", KeyEvent.VK_F9 },
            { "904", KeyEvent.VK_F10 },
            { "891", -1 },
            { "990", KeyEvent.VK_F11 },
            { "992", -1 },
            { "994", -1 },
            { "escapeClose", false },
            { "music_volume", 4 },
            { "opengl_enabled", false },
            { "sound_effect_volume", 0},
            { "display_timers", true},
            { "disable_render_floors", true },
            { "tweening", true },
            { "player_attack_option", 0 },
            { "npc_attack_option", 0 }
    };

    /**
     * Creates a setting for the specified name and object.
     * @param name
     * @param object
     */
    public static void createSetting(String name, Object object) {
        if (!verify(name)) {
            return;
        }
        settings.put(name, object);
    }

    /**
     * Returns the object for the specified name.
     * @param name
     * @return
     */
    public static Object getSetting(String name) {
        if (!verify(name)) {
            return null;
        }
        return settings.get(name);
    }
    
    /**
     * Returns the object for the specified name.
     * @param name
     * @return
     */
    public static Object getSettingOr(String name, Object def) {
        if (!verify(name)) {
            return def;
        }
        Object o = settings.get(name);
        if (o == null) return def;
        
        return o;
    }

    /**
     * Changes the specified setting by removing it then re-adding the new values.
     * @param name
     * @param object
     */
    public static void changeSetting(String name, Object object) {
        if (!verify(name)) {
            return;
        }
        settings.remove(name);
        settings.put(name, object);
        save();
    }

    /**
     * Toggles the specified setting (only works for booleans).
     * @param name
     */
    public static void toggleSetting(String name) {
        if (settings.get(name) instanceof Boolean) {
            changeSetting(name, !(Boolean) settings.get(name));
        }
    }

    public static <T> T getOrDefault(String name, T defaultValue) {
        @SuppressWarnings("unchecked")
        T value = (T)settings.get(name);
        return value == null ? defaultValue : value;
    }


    /**
     * Verifies that the specified name is a defined setting.
     * @param name
     * @return
     */
    public static boolean verify(String name) {
        for (int index = 0; index < settingValues.length; index++) {
            String nameValue = (String) settingValues[index][0];
            if (nameValue.equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Creates the default settings.
     */
    public static void createDefaults() {
    	System.out.println("Creating default settings.");
        for (int index = 0; index < settingValues.length; index++) {
            createSetting((String) settingValues[index][0], settingValues[index][1]);
        }
        save();
    }

    /**
     * Loads and creates the settings from the settings file.
     */
    public static void load() {
        try {
        	File file = new File(DIRECTORY);
            if (!file.exists()) {
                createDefaults();
                return;
            }

            RandomAccessFile in = new RandomAccessFile(file, "rw");
            int size = in.readShort();
            if (size != settingValues.length) {
                createDefaults();
            }
            for (int index = 0; index < size; index++) {
                String name = in.readUTF();
                Object object = null;
                int type = in.readByte();
                if (type == 0) {
                    object = in.readBoolean();
                } else if (type == 1) {
                    object = in.readInt();
                } else if (type == 2) {
                    object = in.readUTF();
                }
                if (size == settingValues.length) {
                    createSetting(name, object);
                } else {
                    changeSetting(name, object);
                }
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Saves the settings to the settings file.
     */
    public static void save() {
        try {
            RandomAccessFile out = new RandomAccessFile(DIRECTORY, "rw");
            out.writeShort(settings.size());
            for (int index = 0; index < settingValues.length; index++) {
                String name = (String) settingValues[index][0];
                Object object = settings.get(name);
                if (object == null) {
                    object = settingValues[index][1];
                }
                out.writeUTF(name);
                if (object instanceof Boolean) {
                    out.writeByte(0);
                    out.writeBoolean((Boolean) object);
                } else if (object instanceof Integer) {
                    out.writeByte(1);
                    out.writeInt((Integer) object);
                } else if (object instanceof String) {
                    out.writeByte(2);
                    out.writeUTF((String) object);
                }
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * The settings file name.
     */
    public final static String DIRECTORY = SignLink.cacheDir + "settings";


}