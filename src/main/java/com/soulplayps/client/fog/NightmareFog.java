package com.soulplayps.client.fog;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.raster.Raster;

public class NightmareFog extends AbstractFog {

	private int currentRgb = 14684240;
	private int trans = 255;
	private int startCycle;
	private int fadeInCycle;
	private int fadeOutCycle;

	public NightmareFog(int id) {
		super(id);
		startCycle = Client.loopCycle;
		fadeInCycle = Client.loopCycle + 60;
		fadeOutCycle = Client.loopCycle + 120;
	}

	@Override
	public void decode(RSBuffer buffer) {
		/* empty */
	}

	@Override
	public void draw(int x, int y, int width, int height) {
		Raster.draw2SideGradient(x, y, width, height, currentRgb, currentRgb, 255 - trans, 0);
	}

	@Override
	public void process() {
		if (Client.loopCycle < startCycle) {
			return;
		}

		if (Client.loopCycle >= fadeOutCycle) {
			Client.fog = null;
			return;
		}

		if (Client.loopCycle <= fadeInCycle) {
			trans = interpolate(255, 0, startCycle, fadeInCycle, Client.loopCycle);
		} else {
			trans = interpolate(0, 255, fadeInCycle, fadeOutCycle, Client.loopCycle);
		}
	}

}
