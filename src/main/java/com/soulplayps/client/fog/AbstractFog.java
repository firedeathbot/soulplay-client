package com.soulplayps.client.fog;

import com.soulplayps.client.node.io.RSBuffer;

public abstract class AbstractFog {

	public static AbstractFog createFog(int type, RSBuffer buffer) {
		switch (type) {
			case 0:
				return new DeadmanFog(type);
			case 1:
				return new LastManStandingFog(type);
			case 2:
				return new NightmareFog(type);
			default:
				return null;
		}
	}

	private final int id;

	public AbstractFog(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public abstract void decode(RSBuffer buffer);

	public abstract void draw(int x, int y, int width, int height);

	public abstract void process();

	public static int interpolate(int outputMin, int outputMax, int inputMin, int inputMax, int input) {
		return outputMin + (outputMax - outputMin) * (input - inputMin) / (inputMax - inputMin);
	}

}
