package com.soulplayps.client.fog;

import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.raster.Raster;

public class DeadmanFog extends AbstractFog {

	private int currentRgb = 16742195;
	private int type = -1;
	private int trans = 255;

	public DeadmanFog(int id) {
		super(id);
	}

	@Override
	public void decode(RSBuffer buffer) {
		type = buffer.readUnsignedByte();
		if (type == 255) {
			type = -1;
		}
	}

	@Override
	public void draw(int x, int y, int width, int height) {
		Raster.draw2SideGradient(x, y, width, height, currentRgb, currentRgb, 255 - trans, 0);
	}

	@Override
	public void process() {
		int $int1 = trans;
		int $int5 = 0;
		int $int6 = 0;
		int $int7 = 0;
		int $int8 = 16742195 >> 16 & 0xff;
		int $int9 = 16742195 >> 8 & 0xff;
		int $int10 = 16742195 & 0xff;
		int $int11 = 52479 >> 16 & 0xff;
		int $int12 = 52479 >> 8 & 0xff;
		int $int13 = 52479 & 0xff;
		int $int17 = 100;
		int $int18 = 255;
		int $int19 = 0;
		int $int20 = currentRgb >> 16 & 0xff;
		int $int21 = currentRgb >> 8 & 0xff;
		int $int22 = currentRgb & 0xff;
		if (type == 2) {
			$int19 = $int17;
			$int7 = $int13;
			$int6 = $int12;
			$int5 = $int11;
		} else if (type == 1) {
			$int19 = $int17;
			$int7 = $int10;
			$int6 = $int9;
			$int5 = $int8;
		} else {
			$int19 = $int18;
			$int7 = $int22;
			$int6 = $int21;
			$int5 = $int20;
		}
		if ($int20 < $int5) {
			$int20++;
		} else if ($int20 > $int5) {
			$int20--;
		}
		if ($int21 < $int6) {
			$int21++;
		} else if ($int21 > $int6) {
			$int21--;
		}
		if ($int22 < $int7) {
			$int22++;
		} else if ($int22 > $int7) {
			$int22--;
		}

		if ($int1 < $int19) {
			$int1++;
		} else if ($int1 > $int19) {
			$int1--;
		}

		currentRgb = (($int20 & 0xff) << 16) | (($int21 & 0xff) << 8) | ($int22 & 0xff);
		trans = $int1;
	}

}
