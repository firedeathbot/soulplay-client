package com.soulplayps.client.fog;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.raster.Raster;

public class LastManStandingFog extends AbstractFog {

	private int currentRgb;
	private int baseX = -1;
	private int baseZ = -1;
	private int radius;
	private int trans = 255;

	public LastManStandingFog(int id) {
		super(id);
	}

	@Override
	public void decode(RSBuffer buffer) {
		baseX = buffer.readUnsignedWord();
		if (baseX == 65535) {
			baseX = -1;
		}

		baseZ = buffer.readUnsignedWord();
		if (baseZ == 65535) {
			baseZ = -1;
		}

		radius = buffer.readUnsignedByte();
	}

	@Override
	public void draw(int x, int y, int width, int height) {
		Raster.draw2SideGradient(x, y, width, height, currentRgb, currentRgb, 255 - trans, 0);
	}

	@Override
	public void process() {
		int $int1 = trans;
		int $int2 = 0;
		int $int3 = 0;
		int $int4 = 0;
		int $int5 = 0;
		int $int6 = 0;
		int $int7 = 0;
		int $int8 = 0;
		int $int9 = 0;
		int $int10 = 8921088 >> 16 & 0xff;
		int $int11 = 8921088 >> 8 & 0xff;
		int $int12 = 8921088 & 0xff;
		int $int13 = 100;
		int $int14 = 255;
		int $int15 = 0;
		int $int16 = currentRgb >> 16 & 0xff;
		int $int17 = currentRgb >> 8 & 0xff;
		int $int18 = currentRgb & 0xff;
		int playerX = Client.regionBaseX + (Client.myPlayer.x - 6 >> 7);
		int playerZ = Client.regionBaseY + (Client.myPlayer.x - 6 >> 7);
		int deltaX = baseX - playerX;
		int deltaZ = baseZ - playerZ;
		$int3 = (int) Math.pow(Math.pow(deltaX, 2), 1.0 / 2);
		$int4 = (int) Math.pow(Math.pow(deltaZ, 2), 1.0 / 2);
		if ($int3 > $int4) {
			$int2 = $int3;
		} else {
			$int2 = $int4;
		}
		$int5 = radius - 8;
		$int6 = radius + 8;
		if ($int2 <= $int5) {
			$int7 = $int16;
			$int8 = $int17;
			$int9 = $int18;
			$int15 = $int14;
		} else if ($int2 >= $int6) {
			$int7 = $int10;
			$int8 = $int11;
			$int9 = $int12;
			$int15 = $int13;
		} else {
			$int2 -= $int5;
			$int7 = interpolate(0, $int2, 0, 16, $int10);
			$int8 = interpolate(0, $int2, 0, 16, $int11);
			$int9 = interpolate(0, $int2, 0, 16, $int12);
			$int15 = $int13;
		}
		if ($int16 < $int7) {
			$int16++;
		} else if ($int16 > $int7) {
			$int16--;
		}
		if ($int17 < $int8) {
			$int17++;
		} else if ($int17 > $int8) {
			$int17--;
		}
		if ($int18 < $int9) {
			$int18++;
		} else if ($int18 > $int9) {
			$int18--;
		}
		currentRgb = (($int16 & 0xff) << 16) | (($int17 & 0xff) << 8) | ($int18 & 0xff);
		if ($int1 < $int15) {
			$int1++;
		} else if ($int1 > $int15) {
			$int1--;
		}
		trans = $int1;
	}

}
