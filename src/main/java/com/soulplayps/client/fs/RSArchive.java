package com.soulplayps.client.fs;

import com.soulplayps.client.data.LookupTable;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.unknown.Class13;

import java.io.*;
import java.util.zip.GZIPInputStream;

public class RSArchive {

	public RSArchive(byte[] abyte0) {
		RSBuffer stream = new RSBuffer(abyte0);
		int i = stream.read3Bytes();
		int j = stream.read3Bytes();
		if (j == 0) {
			byte[] abyte1 = new byte[i];
			byte[] abyte3 = new byte[i];
			System.arraycopy(abyte0, 6, abyte1, 0, abyte0.length - 6);
			try {
				DataInputStream datainputstream = new DataInputStream(
						new GZIPInputStream(new ByteArrayInputStream(abyte1)));
				datainputstream.readFully(abyte3, 0, abyte3.length);
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			aByteArray726 = abyte3;
			stream = new RSBuffer(aByteArray726);
			aBoolean732 = true;
		} else if (j != i) {
			byte abyte1[] = new byte[i];
			Class13.method225(abyte1, i, abyte0, j, 6);
			aByteArray726 = abyte1;
			stream = new RSBuffer(aByteArray726);
			aBoolean732 = true;
		} else {
			aByteArray726 = abyte0;
			aBoolean732 = false;
		}

		int dataSize = stream.readUnsignedWord();
		int[] anIntArray728 = new int[dataSize];
		anIntArray729 = new int[dataSize];
		anIntArray730 = new int[dataSize];
		anIntArray731 = new int[dataSize];
		int k = stream.currentOffset + dataSize * 10;
		for (int l = 0; l < dataSize; l++) {
			anIntArray728[l] = stream.readDWord();
			anIntArray729[l] = stream.read3Bytes();
			anIntArray730[l] = stream.read3Bytes();
			anIntArray731[l] = k;
			k += anIntArray730[l];
		}

		lookupTable = new LookupTable(anIntArray728);
	}

	public byte[] readFile(String name) {
		name = name.toUpperCase();

		int hash = 0;
		for (int i = 0, length = name.length(); i < length; i++) {
			hash = (hash * 61 + name.charAt(i))  - 32;
		}

		int fileId = lookupTable.lookupIdentifier(hash);

		if (fileId != -1) {
			byte[] outBuffer = new byte[anIntArray729[fileId]];
			if (!aBoolean732) {
				Class13.method225(outBuffer, anIntArray729[fileId], aByteArray726, anIntArray730[fileId], anIntArray731[fileId]);
			} else {
				System.arraycopy(aByteArray726, anIntArray731[fileId], outBuffer, 0, anIntArray729[fileId]);
			}

			return outBuffer;
		}

		return null;
	}

	private LookupTable lookupTable;
	public byte[] aByteArray726;
	public int[] anIntArray729;
	public int[] anIntArray730;
	public int[] anIntArray731;
	public boolean aBoolean732;
}
