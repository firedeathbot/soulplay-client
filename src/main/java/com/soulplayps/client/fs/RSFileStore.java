package com.soulplayps.client.fs;

import java.io.IOException;
import java.io.RandomAccessFile;

public final class RSFileStore {

	public RSFileStore(RandomAccessFile randomaccessfile,
					   RandomAccessFile randomaccessfile1, int j) {
		anInt311 = j;
		dataFile = randomaccessfile;
		indexFile = randomaccessfile1;
	}

	public byte[] readFile(int fileId) {
		synchronized (dataFile) {
			byte[] is;
			try {
				if (indexFile.length() < fileId * 6 + 6) {
					return null;
				}
				indexFile.seek(fileId * 6);
				indexFile.read(buffer, 0, 6);
				int i_4_ = (buffer[2] & 0xff) + ((buffer[1] & 0xff) << 8) + ((buffer[0] & 0xff) << 16);
				int i_5_ = (buffer[5] & 0xff) + ((buffer[4] & 0xff) << 8) + ((buffer[3] & 0xff) << 16);
				if (i_4_ < 0) {
					return null;
				}
				if (i_5_ <= 0 || i_5_ > dataFile.length() / 520L) {
					return null;
				}
				byte[] is_10_ = new byte[i_4_];
				int i_11_ = 0;
				int i_12_ = 0;
				while (i_11_ < i_4_) {
					if (i_5_ == 0) {
						return null;
					}
					dataFile.seek(i_5_ * 520L);
					int i_15_ = i_4_ - i_11_;
					int i_16_;
					int i_17_;
					int i_18_;
					int i_19_;
					int i_20_;
					if (fileId > 65535) {
						if (i_15_ > 510)
							i_15_ = 510;
						i_16_ = 10;
						dataFile.read(buffer, 0, i_16_ + i_15_);
						i_17_ = (buffer[3] & 0xff) + ((buffer[2] & 0xff) << 8) + ((buffer[0] & 0xff) << 24) + ((buffer[1] & 0xff) << 16);
						i_18_ = (buffer[5] & 0xff) + ((buffer[4] & 0xff) << 8);
						i_19_ = ((buffer[7] & 0xff) << 8) + ((buffer[6] & 0xff) << 16) + (buffer[8] & 0xff);
						i_20_ = buffer[9] & 0xff;
					}
					else {
						if (i_15_ > 512)
							i_15_ = 512;
						i_16_ = 8;
						dataFile.read(buffer, 0, i_16_ + i_15_);
						i_17_ = (buffer[1] & 0xff) + ((buffer[0] & 0xff) << 8);
						i_18_ = ((buffer[2] & 0xff) << 8) + (buffer[3] & 0xff);
						i_19_ = ((buffer[5] & 0xff) << 8) + ((buffer[4] & 0xff) << 16) + (buffer[6] & 0xff);
						i_20_ = buffer[7] & 0xff;
					}
					if (i_17_ != fileId || i_18_ != i_12_ || i_20_ != anInt311) {
						return null;
					}
					if (i_19_ < 0 || i_19_ > dataFile.length() / 520L) {
						return null;
					}
					int i_25_ = i_16_ + i_15_;
					for (int i_26_ = i_16_; i_26_ < i_25_; i_26_++)
						is_10_[i_11_++] = buffer[i_26_];
					i_5_ = i_19_;
					i_12_++;
				}
				is = is_10_;
			} catch (IOException ioexception) {
				return null;
			}
			return is;
		}
	}
	public synchronized boolean writeFile(int length, byte data[], int fileId) {
		boolean exists = writeFile(true, fileId, length, data);
		if (!exists) {
			exists = writeFile(false, fileId, length, data);
		}
		return exists;
	}

	private boolean writeFile(boolean exists, int fileId, int length, byte[] data) {
		synchronized (dataFile) {
			boolean bool_34_;
			try {
				int i_35_;
				if (exists) {
					if (indexFile.length() < fileId * 6 + 6) {
						return false;
					}
					indexFile.seek(fileId * 6);
					indexFile.read(buffer, 0, 6);
					i_35_ = ((buffer[4] & 0xff) << 8) + ((buffer[3] & 0xff) << 16) + (buffer[5] & 0xff);
					if (i_35_ <= 0 || i_35_ > dataFile.length() / 520L) {
						return false;
					}
				}
				else {
					i_35_ = (int) ((dataFile.length() + 519L) / 520L);
					if (i_35_ == 0)
						i_35_ = 1;
				}
				buffer[0] = (byte) (length >> 16);
				buffer[1] = (byte) (length >> 8);
				buffer[2] = (byte) length;
				buffer[3] = (byte) (i_35_ >> 16);
				buffer[4] = (byte) (i_35_ >> 8);
				buffer[5] = (byte) i_35_;
				indexFile.seek(fileId * 6);
				indexFile.write(buffer, 0, 6);
				int i_40_ = 0;
				int i_41_ = 0;
				while (i_40_ < length) {
					int i_42_ = 0;
					if (exists) {
						dataFile.seek(i_35_ * 520L);
						int i_43_;
						int i_44_;
						int i_45_;
						if (fileId > 65535) {
							try {
								dataFile.read(buffer, 0, 10);
							}
							catch (java.io.EOFException eofexception) {
								break;
							}
							i_43_ = ((buffer[2] & 0xff) << 8) + ((buffer[0] & 0xff) << 24) + ((buffer[1] & 0xff) << 16) + (buffer[3] & 0xff);
							i_44_ = (buffer[5] & 0xff) + ((buffer[4] & 0xff) << 8);
							i_42_ = (buffer[8] & 0xff) + ((buffer[6] & 0xff) << 16) + ((buffer[7] & 0xff) << 8);
							i_45_ = buffer[9] & 0xff;
						}
						else {
							try {
								dataFile.read(buffer, 0, 8);
							}
							catch (java.io.EOFException eofexception) {
								break;
							}
							i_43_ = (buffer[1] & 0xff) + ((buffer[0] & 0xff) << 8);
							i_44_ = ((buffer[2] & 0xff) << 8) + (buffer[3] & 0xff);
							i_42_ = (buffer[6] & 0xff) + ((buffer[5] & 0xff) << 8) + ((buffer[4] & 0xff) << 16);
							i_45_ = buffer[7] & 0xff;
						}
						if (fileId != i_43_ || i_41_ != i_44_ || anInt311 != i_45_) {
							return false;
						}
						if (i_42_ < 0 || i_42_ > dataFile.length() / 520L) {
							return false;
						}
					}
					if (i_42_ == 0) {
						exists = false;
						i_42_ = (int) ((dataFile.length() + 519L) / 520L);
						if (i_42_ == 0)
							i_42_++;
						if (i_42_ == i_35_)
							i_42_++;
					}
					if (fileId > 65535) {
						if (length - i_40_ <= 510)
							i_42_ = 0;
						buffer[0] = (byte) (fileId >> 24);
						buffer[1] = (byte) (fileId >> 16);
						buffer[2] = (byte) (fileId >> 8);
						buffer[3] = (byte) fileId;
						buffer[4] = (byte) (i_41_ >> 8);
						buffer[5] = (byte) i_41_;
						buffer[6] = (byte) (i_42_ >> 16);
						buffer[7] = (byte) (i_42_ >> 8);
						buffer[8] = (byte) i_42_;
						buffer[9] = (byte) anInt311;
						dataFile.seek(i_35_ * 520L);
						dataFile.write(buffer, 0, 10);
						int i_50_ = length - i_40_;
						if (i_50_ > 510)
							i_50_ = 510;
						dataFile.write(data, i_40_, i_50_);
						i_40_ += i_50_;
					}
					else {
						if (length - i_40_ <= 512)
							i_42_ = 0;
						buffer[0] = (byte) (fileId >> 8);
						buffer[1] = (byte) fileId;
						buffer[2] = (byte) (i_41_ >> 8);
						buffer[3] = (byte) i_41_;
						buffer[4] = (byte) (i_42_ >> 16);
						buffer[5] = (byte) (i_42_ >> 8);
						buffer[6] = (byte) i_42_;
						buffer[7] = (byte) anInt311;
						dataFile.seek(i_35_ * 520L);
						dataFile.write(buffer, 0, 8);
						int i_51_ = length - i_40_;
						if (i_51_ > 512)
							i_51_ = 512;
						dataFile.write(data, i_40_, i_51_);
						i_40_ += i_51_;
					}
					i_35_ = i_42_;
					i_41_++;
				}
				bool_34_ = true;
			}
			catch (IOException ioexception) {
				return false;
			}
			return bool_34_;
		}
	}

    public long getFileCount() {
        try {
            if (indexFile != null) {
                return (indexFile.length() / 6);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

	private static final byte[] buffer = new byte[520];
	private final RandomAccessFile dataFile;
	private final RandomAccessFile indexFile;
	private final int anInt311;
}
