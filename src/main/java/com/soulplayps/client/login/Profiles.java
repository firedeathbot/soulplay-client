package com.soulplayps.client.login;

import com.soulplayps.client.Client;
import com.soulplayps.client.data.SignLink;
import com.soulplayps.client.settings.Settings;
import com.soulplayps.client.util.Encryption;

import java.io.*;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Profiles {
	
	public static String username_1 = "", username_1_raw = "";
	public static String password_1 = "";
	
	public static String username_2 = "", username_2_raw = "";
	public static String password_2 = "";
	
	public static String username_3 = "", username_3_raw = "";
	public static String password_3 = "";

	public static void write() throws IOException {
		final File dir = new File(SignLink.cacheDir);
		if (!dir.exists()) {
			return;
		}

		DataOutputStream out = new DataOutputStream(new FileOutputStream(new File(dir, "profile_settings.dat")));
		out.writeUTF(username_1);
		out.writeUTF(password_1);
		out.writeUTF(username_2);
		out.writeUTF(password_2);
		out.writeUTF(username_3);
		out.writeUTF(password_3);
		out.close();
	}

	public static void writeProfiles(String username, String password) throws IOException, InvalidKeyException, UnrecoverableKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, KeyStoreException, CertificateException {
	    if (!LoginRenderer.rememberMeChecked) {
	        return;
        }
	    
	    String usernameRaw = username;
	    
	    username = Encryption.encrypt(username);

		if (username_1.length() == 0) {
			username_1 = username;
			username_1_raw = usernameRaw;
			password_1 = password;
			if (LoginRenderer.rememberMeChecked) {
				Settings.changeSetting("remember_profile", 1);
			}
		} else if (username_2.length() == 0 && !userExists(username)) {
			username_2 = username;
			username_2_raw = usernameRaw;
			password_2 = password;
			if (LoginRenderer.rememberMeChecked) {
				Settings.changeSetting("remember_profile", 2);
			}
		} else if (username_3.length() == 0 && !userExists(username)) {
			username_3 = username;
			username_3_raw = usernameRaw;
			password_3 = password;
			if (LoginRenderer.rememberMeChecked) {
				Settings.changeSetting("remember_profile", 3);
			}
		}

		write();
	}

	public static void readProfiles() {
		try {
			File f = new File(SignLink.cacheDir, "profile_settings.dat");
			if (!f.exists()) {
				return;
			}

			DataInputStream in = new DataInputStream(new FileInputStream(f));
			username_1 = in.readUTF();
			password_1 = in.readUTF();
			username_2 = in.readUTF();
			password_2 = in.readUTF();
			username_3 = in.readUTF();
			password_3 = in.readUTF();

			in.close();

			decryptUsernames();
			
			fillLoginText((Integer) Settings.getSetting("remember_profile"));
		} catch (Exception e) {
			/* empty */
		}
	}
	
	private static void decryptUsernames() throws InvalidKeyException, UnrecoverableKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, CertificateException, FileNotFoundException, IOException {
		if (username_1.length() != 0) {
			username_1_raw = Encryption.decrypt(username_1);
		}
		if (username_2.length() != 0) {
			username_2_raw = Encryption.decrypt(username_2);
		}
		if (username_3.length() != 0) {
			username_3_raw = Encryption.decrypt(username_3);
		}
	}

	public static void fillLoginText(int loginType) {
		if (loginType > 0) {
			try {
				if (loginType == 1) {
					if (username_1.length() != 0) {
						Client.instance.myUsername = Encryption.decrypt(username_1);
						Client.instance.myPassword = Encryption.decrypt(password_1);
	//					Client.instance.login(Encryption.decrypt(username_1), Encryption.decrypt(password_1), false);
					}
				}
				if (loginType == 2) {
					if (username_2.length() != 0) {
						Client.instance.myUsername = Encryption.decrypt(username_2);
						Client.instance.myPassword = Encryption.decrypt(password_2);
	//					Client.instance.login(Encryption.decrypt(username_2), Encryption.decrypt(password_2), false);
					}
				}
				if (loginType == 3) {
					if (username_3.length() != 0) {
						Client.instance.myUsername = Encryption.decrypt(username_3);
						Client.instance.myPassword = Encryption.decrypt(password_3);
	//					Client.instance.login(Encryption.decrypt(username_3), Encryption.decrypt(password_3), false);
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static boolean userExists(String username) {
		if (username.equalsIgnoreCase(username_1) || username.equalsIgnoreCase(username_2) || username.equalsIgnoreCase(username_3)) {
			return true;
		}
		return false;
	}
	
	public static int getSavedUserAmount() {
		int amount = 0;
		if (username_1.length() > 0) {
			amount++;
		}
		if (username_2.length() > 0) {
			amount++;
		}
		if (username_3.length() > 0) {
			amount++;
		}
		return amount;
	}
	
	public static void removeProfile(int slot) {
		if (slot == 1) {
			//If there are more than one account we swap user 1 with user 2
			if (getSavedUserAmount() == 2) {
				//Make 1 = 2
				username_1 = username_2;
				username_1_raw = username_2_raw;
				password_1 = password_2;
				
				//Reset 2
				username_2 = username_2_raw = "";
				password_2 = "";
			} else if (getSavedUserAmount() == 3) {
				//Make 1 = 2, Make 2 = 3
				username_1 = username_2;
				username_1_raw = username_2_raw;
				password_1 = password_2;
				username_2 = username_3;
				username_2_raw = username_3_raw;
				password_2 = password_3;
				
				//Reset 3
				username_3 = username_3_raw = "";
				password_3 = "";
			} else {
				username_1 = username_1_raw = "";
				password_1 = "";
			}
		}
		if (slot == 2) {
			//If there are more than two accounts we swap user 2 with user 3
			if (getSavedUserAmount() == 3) {
				//Make 2 = 3
				username_2 = username_3;
				username_2_raw = username_3_raw;
				password_2 = password_3;
				
				//Reset 3
				username_3 = username_3_raw = "";
				password_3 = "";
			} else {
				username_2 = username_2_raw = "";
				password_2 = "";
			}
		}
		if (slot == 3) {
			username_3 = username_3_raw = "";
			password_3 = "";
		}

		try {
			write();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
