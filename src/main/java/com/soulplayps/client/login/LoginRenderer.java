package com.soulplayps.client.login;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.Ping;
import com.soulplayps.client.node.raster.Raster;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.settings.Settings;
import com.soulplayps.client.util.Encryption;
import com.soulplayps.client.util.Strings;

import org.lwjgl.opengl.GL11;

import static com.soulplayps.client.Client.rotationWorlds;
import static com.soulplayps.client.login.Profiles.*;

public class LoginRenderer {
	
	public static TitleState titleState = TitleState.LOGIN;
	public static boolean profilesOpen;
	public static boolean rememberMeChecked;
	private String world1PlayerCount = "0";
	private String world2PlayerCount = "0";
	private String world3PlayerCount = "0";
	
	private Sprite usernameDefault;
	private Sprite usernameLit;
	
	private Sprite passwordDefault;
	private Sprite passwordLit;
	
	private Sprite playSpriteDefault;
	private Sprite playSpriteLit;
	
	private Sprite worldSpriteDefault;
	private Sprite worldSpriteLit;
	
	private Sprite profiles;
	private Sprite profilesHover;
	private Sprite profileDropdown;
	private Sprite profileX;
	private Sprite profileXHover;
	
	private Sprite checkBoxDefault;
	private Sprite checkBoxActive;
	
	private Sprite playerHead;
	private Sprite playerHeadHover;
	
	private Sprite rememberMe;
	private Sprite rememberMeHovered;
	
	//	private Sprite animatedWorldButton;
	private final int[] animWoldButtonLoc = { 10, 480 };
	private Sprite animateWorldCheckBoxDefault;
	private Sprite animateWorldCheckBoxActive;

	private final Client client;
	private int centerX, centerY, mouseX, mouseY;
	private int offsetX;
	
	public LoginRenderer(Client client) {
		this.client = client;
		client.loginMessage1 = "Welcome to SoulPlay!";
		client.loginMessage2 = "Please enter your username and password.";
	}

	public void draw() {

		if (client.fullGameScreen != null)  {
			client.fullGameScreen.initDrawingArea();
		}

		if (!GraphicsDisplay.enabled) {
			Raster.reset();
		} else {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		}

		if (Config.DEBUG) {
			client.newSmallFont.drawBasicString("Mouse (" + mouseX + ", " + mouseY + ") fps:" + client.fps, 10, 35, 0xffffff, 0);
		}

		if (titleState == TitleState.LOGIN) {
			drawRegular();
		} else if (titleState == TitleState.WORLD_LIST) {
			drawWorldList();
		} else {
			drawRestricted();
		}

		// Draws the screen
		if (!GraphicsDisplay.enabled) {
			if (client.fullGameScreen != null && client.graphics != null) {
				client.fullGameScreen.drawGraphics(client.graphics, offsetX, 0);
			}
		} else {
			GraphicsDisplay.getInstance().endRenderingWorld(null, 1);
		}

	}

	private void drawWorldList() {
		SpriteCache.get(414).drawSprite(0, 0);
		SpriteCache.get(438).drawOr(57, 150, SpriteCache.get(416));
		SpriteCache.get(440).drawOr(57, 172, SpriteCache.get(418));
		SpriteCache.get(442).drawOr(57, 194, SpriteCache.get(420));
		client.newBoldFont.drawBasicString(world1PlayerCount, 229, 166, 0xffffff, 0);
		client.newBoldFont.drawBasicString(world2PlayerCount, 229, 188, 0xffffff, 0);
		client.newBoldFont.drawBasicString(world3PlayerCount, 229, 210, 0xffffff, 0);
	}

	private void drawRestricted() {
		// Background
		client.drawAnimatedWorldBackground();

		SpriteCache.get(1061).drawSprite(centerX - SpriteCache.get(1061).myHeight / 2, centerY - SpriteCache.get(1061).myWidth / 2);
		SpriteCache.get(1062).drawOr(325, 224, SpriteCache.get(1063));
		SpriteCache.get(1062).drawOr(325, 282, SpriteCache.get(1063));
		client.newBoldFont.drawInterfaceText("The account you tried to log in with <br> does not exist. You can create a <br> new account for free using the <br> create account button.", 262, 110, 280, 100, 0xffffff, 0, 0, 1, 1, 16);
		client.newSmallFont.drawInterfaceText("Create new account at <col=ffff00> www.soulplayps.com/register", 253, 295, 300, 100, 0xffffff, 0, 0, 1, 1, 0);

		//temp until buttons with text are made
		client.newBoldFont.drawInterfaceText("Create Account", 262, 189, 280, 100, 0xffffff, 0, 0, 1, 1, 16);
		client.newBoldFont.drawInterfaceText("Existing User", 262, 247, 280, 100, 0xffffff, 0, 0, 1, 1, 16);
	}
	
	private void drawRegular() {
		// Background
		client.drawAnimatedWorldBackground();

		// Login Box
		SpriteCache.get(997).drawSprite(centerX - SpriteCache.get(997).myHeight / 2, centerY - SpriteCache.get(997).myWidth / 2);
		// Username
		if (usernameDefault == null)
			usernameDefault = SpriteCache.get(998);
		if (usernameLit == null)
			usernameLit = SpriteCache.get(999);
		usernameLit.drawOr(centerX - 104, centerY - 115, usernameDefault);
		client.newRegularFont.drawBasicString(Strings.formatName(client.myUsername) + ((client.loginScreenCursorPos == 0) & (Client.loopCycle % 40 < 20) ? "|" : ""), centerX - 95, centerY - 97, 0xFFFFFF, 0);

		// Password
		if (passwordDefault == null)
			passwordDefault = SpriteCache.get(998);
		if (passwordLit == null)
			passwordLit = SpriteCache.get(999);
		passwordLit.drawOr(centerX - 104, centerY - 65, passwordDefault);
		client.newRegularFont.drawBasicString(Strings.passwordAsterisks(client.myPassword) + ((client.loginScreenCursorPos == 1) & (Client.loopCycle % 40 < 20) ? "|" : ""), centerX - 95, centerY - 45, 0xFFFFFF, 0);
		
		// Play
		if (worldSpriteDefault == null)
			playSpriteDefault = SpriteCache.get(127);
		if (worldSpriteDefault == null)
			playSpriteLit = SpriteCache.get(128);
		playSpriteLit.drawOr(279, 248, playSpriteDefault);
		
		// World select
		if (worldSpriteDefault == null)
			worldSpriteDefault = SpriteCache.get(129);
		if (worldSpriteLit == null)
			worldSpriteLit = SpriteCache.get(130);
		worldSpriteLit.drawOr(279, 293, worldSpriteDefault);
		
		// Profiles
		if (profiles == null)
			profiles = SpriteCache.get(1000);
		if (profilesHover == null)
			profilesHover = SpriteCache.get(446);
		if (profileDropdown == null)
			profileDropdown = SpriteCache.get(1001);
		if (playerHead == null)
			playerHead = SpriteCache.get(1002);
		if (playerHeadHover == null)
			playerHeadHover = SpriteCache.get(448);
		if (profileX == null)
			profileX = SpriteCache.get(300);
		if (profileXHover == null)
			profileXHover = SpriteCache.get(301);
		profileDropdown.drawProfiles(280, 353, profiles, profilesHover, playerHead, playerHeadHover, profileX, profileXHover);
		
		//Checkboxes (Remember me)
		if (rememberMe == null)
			rememberMe = SpriteCache.get(450);
		if (rememberMeHovered == null)
			rememberMeHovered = SpriteCache.get(452);
		rememberMeHovered.drawOr(278, 218, rememberMe);
		
		if (checkBoxDefault == null)
			checkBoxDefault = SpriteCache.get(521);
		if (checkBoxActive == null)
			checkBoxActive = SpriteCache.get(523);
		checkBoxDefault.drawCheckbox(278, 218, checkBoxActive, rememberMeChecked);

		if (animateWorldCheckBoxDefault == null)
			animateWorldCheckBoxDefault = SpriteCache.get(521);
		if (animateWorldCheckBoxActive == null)
			animateWorldCheckBoxActive = SpriteCache.get(523);
		animateWorldCheckBoxDefault.drawCheckbox(animWoldButtonLoc[0], animWoldButtonLoc[1], animateWorldCheckBoxActive, Client.drawAnimatedWorldBackground);
		int textColor = 0xf0f0f0;
		if (client.mouseX >= animWoldButtonLoc[0] && client.mouseY >= animWoldButtonLoc[1] && client.mouseX <= (animWoldButtonLoc[0] + 155) && client.mouseY < +(animWoldButtonLoc[1] + animateWorldCheckBoxActive.myHeight)) {
			textColor = 0xffffff;
			if (!Client.drawAnimatedWorldBackground) {
				SpriteCache.get(519).drawSprite(animWoldButtonLoc[0], animWoldButtonLoc[1]);
			}
		}
		client.newBoldFont.drawBasicString("Animate Background", animWoldButtonLoc[0] + 21, animWoldButtonLoc[1] + 13, textColor, 0);
		
		/*if (Client.rotationWorlds == 0) {
			client.smallText.method382(Client.worldOnline ? 0x00FF00 : 0xFF0000, 490, "SoulEco", 340, true);
			client.smallText.method382(0xFFFFFF, 350, "World 1: North America", 340, true);
		} else if (Client.rotationWorlds == 1) {
			client.smallText.method382(Client.worldOnline ? 0x00FF00 : 0xFF0000, 490, "SoulEco", 340, true);
			client.smallText.method382(0xFFFFFF, 350, "World 2: Europe", 340, true);
		} else if (Client.rotationWorlds == 2) {
			client.smallText.method382(Client.worldOnline ? 0x00FF00 : 0xFF0000, 490, "Spawn", 340, true);
			client.smallText.method382(0xFFFFFF, 350, "World 3: North America", 340, true);
		}*/
		client.newRegularFont.drawCenteredString("Current World: " + (rotationWorlds + 1) + (client.worldOnline ? "" : "    OFFLINE"), 400, 340, 0xFFFFFF, 0);
		
		// failed login notification

		Client.newSmallFont.drawCenteredString(client.loginMessage2, centerX + 21, centerY - 151, 0xFF0000, 0);
		Client.newSmallFont.drawCenteredString(client.loginMessage1, centerX + 15, centerY - 165, 0xFF0000, 0);
	}
	
	public void process() {
		Client.clientWidth = Client.newClientWidth;
		Client.clientHeight = Client.newClientHeight;
		offsetX = (Client.clientWidth - Client.FIXED_FULL_WIDTH) / 2;
		
		centerX = Client.FIXED_FULL_WIDTH / 2;
		centerY = Client.FIXED_FULL_HEIGHT / 2;
		
		mouseX = client.mouseX - centerX;
		mouseY = client.mouseY - centerY;

		if (titleState == TitleState.REGISTER) {
			if (client.clickMode3 == 1) {
				if (mouseX >= -56 && mouseX <= 95 && mouseY >= 32 && mouseY <= 61) {
				    titleState = TitleState.LOGIN;
				} else if (mouseX >= -56 && mouseX <= 95 && mouseY >= -25 && mouseY <= 3) {
					client.launchURL("www.soulplayps.com/register");
				}
			}
			return;
		} else if (titleState == TitleState.WORLD_LIST) {
			if (client.clickMode3 == 1) {
				if (mouseX >= 305 && mouseY >= -194 && mouseX <= 330 && mouseY <= -174) {
					titleState = TitleState.LOGIN;
				} else if (mouseX >= -325 && mouseY >= -102 && mouseX <= 325 && mouseY <= -80) {
					Client.server = Config.SERVER_IP;
					Client.port = Config.SERVER_PORT;
					Client.rotationWorlds = 0;
					titleState = TitleState.LOGIN;
				} else if (mouseX >= -325 && mouseY >= -80 && mouseX <= 325 && mouseY <= -60) {
					Client.server = Config.SERVER_IP_EU;
					Client.port = Config.SERVER_PORT_EU;
					Client.rotationWorlds = 1;
					titleState = TitleState.LOGIN;
				} else if (mouseX >= -325 && mouseY >= -58 && mouseX <= 325 && mouseY <= -35) {
					Client.server = Config.SERVER_IP_PVP;
					Client.port = Config.SERVER_PORT_PVP;
					Client.rotationWorlds = 2;
					titleState = TitleState.LOGIN;
				}
			}
			return;
		}
		if (usernameDefault == null || passwordDefault == null)
			return;
		if (client.clickMode3 == 1) {
			// Username
			if (usernameDefault.mouseInPosition(centerX - 104, centerY - 115)) {
				client.loginScreenCursorPos = 0;
			}

			// Password
			if (passwordDefault.mouseInPosition(centerX - 104, centerY - 65)) {
				client.loginScreenCursorPos = 1;
			}

			// world select
			if (worldSpriteDefault.mouseInPosition(279, 293)) {
				titleState = TitleState.WORLD_LIST;
			}
		
			// login
			if (playSpriteDefault.mouseInPosition(279, /*293*/248)) {
				client.loginMessage1 = "";
				client.loginMessage2 = "<col=ffff00>Connecting to server...";
				client.login(client.myUsername, client.myPassword, false);
				if (Client.loggedIn) {
					return;
				}
			}
		
			//Profiles
			if (profiles.mouseInPosition(280, 353)) {
				profilesOpen = !profilesOpen;
			}
			if (playerHead != null && profilesOpen) {
				try {
					if (playerHead.mouseInPosition(303, 385)) {
						if (username_1.length() != 0) {
							client.myUsername = Encryption.decrypt(username_1);
							client.myPassword = Encryption.decrypt(password_1);
	//						Client.instance.login(Encryption.decrypt(username_1), Encryption.decrypt(password_1), false);
							if (rememberMeChecked) {
								Settings.changeSetting("remember_profile", 1);
							}
						}
					}
					if (playerHead.mouseInPosition(379, 385)) {
						if (username_2.length() != 0) {
							client.myUsername = Encryption.decrypt(username_2);
							client.myPassword = Encryption.decrypt(password_2);
	//						Client.instance.login(Encryption.decrypt(username_2), Encryption.decrypt(password_2), false);
							if (rememberMeChecked) {
								Settings.changeSetting("remember_profile", 2);
							}
						}
					}
					if (playerHead.mouseInPosition(455, 385)) {
						if (username_3.length() != 0) {
							client.myUsername = Encryption.decrypt(username_3);
							client.myPassword = Encryption.decrypt(password_3);
	//						Client.instance.login(Encryption.decrypt(username_3), Encryption.decrypt(password_3), false);
							if (rememberMeChecked) {
								Settings.changeSetting("remember_profile", 3);
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (profileX != null && profilesOpen) {
				if (profileX.mouseInPosition(350, 375)) {
					Profiles.removeProfile(1);
				}
				if (profileX.mouseInPosition(426, 375)) {
					Profiles.removeProfile(2);
				}
				if (profileX.mouseInPosition(502, 375)) {
					Profiles.removeProfile(3);
				}
			}
		
			//Remember me
			if (rememberMe.mouseInPosition(278, 218)) {
				rememberMeChecked = !rememberMeChecked;
				if (!rememberMeChecked) {
					Settings.changeSetting("remember_profile", 0);
				}
			}
		
			if (client.mouseX >= animWoldButtonLoc[0] && client.mouseY >= animWoldButtonLoc[1] && client.mouseX <= (animWoldButtonLoc[0] + 155) && client.mouseY < +(animWoldButtonLoc[1] + animateWorldCheckBoxActive.myHeight)) {
				Client.drawAnimatedWorldBackground = !Client.drawAnimatedWorldBackground;
				Settings.changeSetting("animate_bg", Client.drawAnimatedWorldBackground);
			}
		}

		// keyboard input
		do {
			int line = client.readChar();
			if (line == -1) {
				break;
			}
			boolean flag1 = false;
			for (int i2 = 0; i2 < Client.validUserPassChars.length(); i2++) {
				if (line != Client.validUserPassChars.charAt(i2)) {
					continue;
				}
				flag1 = true;
				break;
			}
			if (client.loginScreenCursorPos == 0) {
				if (line == 8 && !client.myUsername.isEmpty()) {
					client.myUsername = client.myUsername.substring(0, client.myUsername.length() - 1);
				}
				if (line == 9 || line == 10 || line == 13) {
					client.loginScreenCursorPos = 1;
				}
				if (flag1 && client.myUsername.length() < Config.USERNAME_LENGTH) {
					client.myUsername += (char) line;
				}
			} else if (client.loginScreenCursorPos == 1) {
				if (line == 8 && !client.myPassword.isEmpty()) {
					client.myPassword = client.myPassword.substring(0, client.myPassword.length() - 1);
				}
				if (line == 9 || line == 10 || line == 13) {
					if (!client.myPassword.isEmpty() && !client.myUsername.isEmpty()) {
						client.loginMessage1 = "";
						client.loginMessage2 = "<col=ffff00>Connecting to server...";
						client.login(client.myUsername, client.myPassword, false);
					} else {
						client.loginScreenCursorPos = 0;
					}
				}
				if (flag1 && client.myPassword.length() < Config.PASSWORD_LENGTH) {
					client.myPassword += (char) line;
				}
			}
		} while (true);
		return;
	}
	
	public void reloadWorldCount() {
		world1PlayerCount = Ping.playersOnline(1);
		world2PlayerCount = Ping.playersOnline(2);
		world3PlayerCount = Ping.playersOnline(3);
	}
	
}