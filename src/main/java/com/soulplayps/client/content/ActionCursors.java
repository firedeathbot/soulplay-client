package com.soulplayps.client.content;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.raster.sprite.SpriteCache;

public class ActionCursors {

	public static boolean enableActionCursors = false;
	private static int currentCursorId = -1;

	public static void setCursor(String option) {
		int newCursorId;
		if (enableActionCursors) {
			newCursorId = lookup(option);
		} else {
			newCursorId = -1;
		}
		if (currentCursorId == newCursorId) {
			return;
		}

		if (newCursorId != -1) {
			Client.instance.setCursor(SpriteCache.get(newCursorId));
			currentCursorId = newCursorId;
		} else if (newCursorId == -1 && currentCursorId != -1) {
			Client.instance.removeCursor();
			currentCursorId = -1;
		}
	}

    public static int lookup(String actionName) {
        if (actionName == null) {
            return 1067;
        }

        //System.out.println(actionName);

        if (actionName.startsWith("Attack")) {
            return 1064;
        } else if (actionName.startsWith("Walk-to")) {
            return 1067;
        } else if (actionName.startsWith("Take") || actionName.startsWith("Remove") || actionName.startsWith("Pick-up")) {
            return 1068;
        } else if (actionName.startsWith("Talk-to")) {
            return 1070;
        } else if (actionName.startsWith("Enter") || actionName.startsWith("Exit")) {
            return 1071;
        } else if (actionName.startsWith("Current Progress")) {
            return 1088;
        } else if (actionName.startsWith("Net") || actionName.startsWith("Bait") || actionName.startsWith("Cage") || actionName.startsWith("Harpoon")) {
            return 1072;
        } else if (actionName.startsWith("Chop")) {
            return 1073;
        }
//        else if (actionName.startsWith("Light")) { // sprite is not transparent for some reason
//            return 1116;
//        }
        else if (actionName.startsWith("Pray-at")) {
            return 1074;
        } else if (actionName.startsWith("Mine")) {
            return 1075;
        } else if (actionName.startsWith("Eat")) {
            return 1076;
        } else if (actionName.startsWith("Drink")) {
            return 1077;
        } else if (actionName.startsWith("Wield")) {
            return 1078;
        } else if (actionName.startsWith("Search")) {
            return 1082;
        } else if (actionName.startsWith("Steal")) {
            return 1083;
        } else if (actionName.startsWith("Smelt")) {
            return 1084;
        } else if (actionName.startsWith("Clean")) {
            return 1085;
        } else if (actionName.startsWith("Jump")) {
            return 1086;
        } else if (actionName.startsWith("Info") || actionName.startsWith("Inspect")) {
            return 1109;
        } else if (actionName.startsWith("Open")) {
            if (actionName.contains("teleport menu")) {
                return 1113;
            }
            return 1079;
        } else if (actionName.startsWith("Climb")) {
            if (actionName.contains("up")) {
                return 1080;
            } else if (actionName.contains("down")) {
                return 1081;
            } else {
                return 1182;
            }
        } else if (actionName.startsWith("Craft-rune")) {
            return 1112;
        } else if (actionName.startsWith("Cast")) {
            if (actionName.contains("Smoke Rush")) {
                return 1107;
            } else if (actionName.contains("Shadow Rush")) {
                return 1106;
            } else if (actionName.contains("Blood Rush")) {
                return 1105;
            } else if (actionName.contains("Ice Rush")) {
                return 1104;
            } else if (actionName.contains("Smoke Burst")) {
                return 1103;
            } else if (actionName.contains("Shadow Burst")) {
                return 1102;
            } else if (actionName.contains("Blood Burst")) {
                return 1101;
            } else if (actionName.contains("Ice Burst")) {
                return 1100;
            } else if (actionName.contains("Smoke Blitz")) {
                return 1099;
            } else if (actionName.contains("Shadow Blitz")) {
                return 1098;
            } else if (actionName.contains("Blood Blitz")) {
                return 1097;
            } else if (actionName.contains("Ice Blitz")) {
                return 1096;
            } else if (actionName.contains("Smoke Barrage")) {
                return 1095;
            } else if (actionName.contains("Shadow Barrage")) {
                return 1094;
            } else if (actionName.contains("Blood Barrage")) {
                return 1093;
            } else if (actionName.contains("Ice Barrage")) {
                return 1092;
            } else if (actionName.contains("Wind strike")) {
                return 1126;
            } else if (actionName.contains("Water strike")) {
                return 1129;
            } else if (actionName.contains("Earth strike")) {
                return 1127;
            } else if (actionName.contains("Fire strike")) {
                return 1128;
            } else if (actionName.contains("Wind bolt")) {
                return 1131;
            } else if (actionName.contains("Water bolt")) {
                return 1132;
            } else if (actionName.contains("Earth bolt")) {
                return 1130;
            } else if (actionName.contains("Fire bolt")) {
                return 1133;
            } else if (actionName.contains("Wind blast")) {
                return 1141;
            } else if (actionName.contains("Water blast")) {
                return 1142;
            } else if (actionName.contains("Earth blast")) {
                return 1143;
            } else if (actionName.contains("Fire blast")) {
                return 1144;
            } else if (actionName.contains("Wind wave")) {
                return 1145;
            } else if (actionName.contains("Water wave")) {
                return 1146;
            } else if (actionName.contains("Earth wave")) {
                return 1147;
            } else if (actionName.contains("Fire wave")) {
                return 1148;
            } else if (actionName.contains("Teleother Lumbridge")) {
                return 1179;
            } else if (actionName.contains("Teleother Falador")) {
                return 1180;
            } else if (actionName.contains("Teleother Camelot")) {
                return 1181;
            } else if (actionName.contains("Enchant Lvl-1 Jewelry")) {
                return 1168;
            } else if (actionName.contains("Enchant Lvl-2 Jewelry")) {
                return 1171;
            } else if (actionName.contains("Enchant Lvl-3 Jewelry")) {
                return 1173;
            } else if (actionName.contains("Enchant Lvl-4 Jewelry")) {
                return 1169;
            } else if (actionName.contains("Enchant Lvl-5 Jewelry")) {
                return 1170;
            } else if (actionName.contains("Weaken")) {
                return 1138;
            } else if (actionName.contains("Bind")) {
                return 1122;
            } else if (actionName.contains("Snare")) {
                return 1123;
            } else if (actionName.contains("Entangle")) {
                return 1124;
            } else if (actionName.contains("Low level alchemy")) {
                return 1111;
            } else if (actionName.contains("High level alchemy")) {
                return 1110;
            }
            else {
                return 1069;
            }
        } else if (actionName.startsWith("Activate") || actionName.startsWith("Use") || actionName.startsWith("Rub") || actionName.startsWith("Select")) {
            return 1069;
        } else {
            return 1067;
        }
    }

    private ActionCursors() {

    }

}
