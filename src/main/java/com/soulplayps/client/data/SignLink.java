package com.soulplayps.client.data;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public final class SignLink {
	
	public static void startpriv() {
		javaVersion = "1.1";
		javaVendor = "Unknown";

		try {
			javaVendor = System.getProperty("java.vendor");
			javaVersion = System.getProperty("java.version");

			javaVersionInt = getJavaVersion(javaVersion);
		} catch (Exception var17) {
			;
		}

		cacheDir = findcachedir();
		uid = getuid(cacheDir);
		System.out.println("Loading cache in: " + cacheDir);
		try {
			File file = new File(cacheDir + "main_file_cache.dat");
			if (file != null && file.exists() && !Client.correctCacheVersion() && !Config.USE_UPDATE_SERVER) {
				file.delete();
			}
			cache_dat = new RandomAccessFile(cacheDir + "main_file_cache.dat", "rw");
			for (int j = 0; j < INDEX_SIZE; j++) //5
				cache_idx[j] = new RandomAccessFile(cacheDir + "main_file_cache.idx" + j, "rw");
			
		} catch (Exception exception) {
			exception.printStackTrace();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					JOptionPane.showMessageDialog(null, "Unable to Update the Cache. Please Close all running Clients and try again.");
				}
			});
		}
	}
	
	private static boolean cacheFolderIsLocal = false;
	
	public static void checkForLocalCacheFolder() {
		File file = new File(localCacheFolderLoc);
		cacheFolderIsLocal = file.exists();
	}
	
	private static String getCacheFolder() {
		return cacheFolderIsLocal ? localCacheFolderLoc : cacheFolderLoc;
	}
	
	private static final String cacheFolderLoc = System.getProperty("user.home") + "/";
	private static final String localCacheFolderLoc = "./cache/";

	public static String findcachedir() {
		String s = Config.folderName;
		try {
			String s1 = getCacheFolder();
			if (s1.length() > 0) {
				File file = new File(s1);
				if (!file.exists())
					return secondDir();
			}
			File file1 = new File(s1 + s);
			if (file1.exists() || file1.mkdir())
				return s1 + s + "/";
		} catch (Exception _ex) {
		}
		return secondDir();
	}
	
	public static File findSoulSplit3Folder() {
		String s = ".SoulSplit3";
		String s1 = getCacheFolder();
		return new File(s1 + s);
	}
	
	public static String secondDir() {
		File file = new File("c:/" + Config.folderName);
		if (!file.exists())
			file.mkdir();
		return file.toString();
	}
	
	
	
	public static int getuid(String s) {
		try {
			File file = new File(s + "uid.dat");
			if (!file.exists() || file.length() < 4L) {
				DataOutputStream dataoutputstream = new DataOutputStream(new FileOutputStream(s + "uid.dat"));
				dataoutputstream.writeInt((int) (Math.random() * 99999999D));
				dataoutputstream.close();
			}
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
		try {
			DataInputStream datainputstream = new DataInputStream(new FileInputStream(s + "uid.dat"));
			int i = datainputstream.readInt();
			datainputstream.close();
			return i + 1;
		} catch (Exception _ex) {
			return 0;
		}
	}

	public static void reporterror(String s) {
		System.out.println("Error: " + s);
	}

	public static int getJavaVersion(String version) {
		try {
			String[] values = version.split("\\.");

			if (version.startsWith("1.")) {
				return Integer.parseInt(values[1]);
			} else {
				return Integer.parseInt(values[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 6;
	}

	private SignLink() {
		throw new RuntimeException();
	}
	
	public static String cacheDir;
	public static int uid;
	public static RandomAccessFile cache_dat = null;
	public static int INDEX_SIZE = 10;
	public static RandomAccessFile[] cache_idx = new RandomAccessFile[INDEX_SIZE];
	public static String javaVersion;
	public static int javaVersionInt;
	public static String javaVendor;
}
