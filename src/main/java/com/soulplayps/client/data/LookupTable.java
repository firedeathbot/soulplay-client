package com.soulplayps.client.data;

public class LookupTable {

	private final int[] identTable;

	public int lookupIdentifier(int neededHash) {
		int center = (identTable.length >> 1) - 1;
		int pos = neededHash & center;
		for (;;) {
			int id = identTable[pos + pos + 1];
			if (id == -1) {
				return -1;
			}

			if (identTable[pos + pos] == neededHash) {
				return id;
			}

			pos = center & pos + 1;
		}
	}

	public LookupTable(int[] hashes) {
		int size;
		for (size = 1; hashes.length + (hashes.length >> 1) >= size; size <<= 1) {
			/* empty */
		}

		identTable = new int[size + size];
		for (int id = 0; id < size + size; id++) {
			identTable[id] = -1;
		}

		for (int id = 0; id < hashes.length; id++) {
			int pos;
			for (pos = hashes[id] & size - 1; identTable[pos + pos + 1] != -1; pos = size - 1 & pos + 1) {
				/* empty */
			}

			identTable[pos + pos] = hashes[id];
			identTable[pos + pos + 1] = id;
		}
	}

}
