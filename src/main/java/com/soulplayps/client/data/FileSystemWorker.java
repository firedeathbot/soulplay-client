package com.soulplayps.client.data;

import com.soulplayps.client.fs.RSFileStore;
import com.soulplayps.client.node.Deque;

public class FileSystemWorker implements Runnable {

	public Deque ioList = new Deque();
	public Deque finishedRequests = new Deque();
	private boolean stopped = false;
	private Thread thread;

	public void run() {
		while (!stopped) {
			FileSystemRequest request;
			synchronized (ioList) {
				request = (FileSystemRequest) ioList.getFront();
				if (request == null) {
					try {
						ioList.wait();
					} catch (final InterruptedException interruptedexception) {
						/* empty */
					}
					continue;
				}
			}
			try {
				if (request.type == 0) {
					request.fileSystem.writeFile(request.buffer.length, request.buffer, (int) request.id);
					synchronized (ioList) {
						request.unlink();
					}
				} else if (request.type == 1) {
					request.buffer = request.fileSystem.readFile((int) request.id);
					synchronized (ioList) {
						finishedRequests.insertBack(request);
					}
				}
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	public void requestWrite(RSFileStore fileSystem, byte[] buffer, int id) {
		FileSystemRequest request = new FileSystemRequest();
		request.id = (long) id;
		request.buffer = buffer;
		request.type = 0;
		request.fileSystem = fileSystem;
		putRequest(request);
	}

	private void putRequest(FileSystemRequest fsRequest) {
		synchronized (ioList) {
			ioList.insertBack(fsRequest);
			ioList.notifyAll();
		}
	}

	public void requestRead(int id, RSFileStore fileSystem) {
		FileSystemRequest request = new FileSystemRequest();
		request.fileSystem = fileSystem;
		request.type = 1;
		request.id = (long) id;
		putRequest(request);
	}

	public void handleRequests() {
		for (;;) {
			FileSystemRequest node;
			synchronized (ioList) {
				node = (FileSystemRequest) finishedRequests.popFront();
			}
			if (node == null) {
				break;
			}

			//node.onDemand.processLoaded(node.indexId, (int) node.id, node.buffer);
		}
	}

	public void stop() {
		stopped = true;
		synchronized (ioList) {
			ioList.notifyAll();
		}
		try {
			if (thread != null)
				thread.join();
		} catch (final InterruptedException interruptedException) {
			/* empty */
		}
		thread = null;
	}

	public FileSystemWorker() {
		thread = new Thread(this, "File system worker");
		thread.setPriority(Thread.NORM_PRIORITY);
		thread.setDaemon(true);
		thread.start();
	}

}
