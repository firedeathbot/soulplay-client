package com.soulplayps.client.reflectioncheck;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.soulplayps.client.node.NodeList;
import com.soulplayps.client.node.io.RSBuffer;

public class ReflectionCheck {

	public static NodeList reflectionCheckDeque = new NodeList();

	public static void readReflectionCheckPacket(RSBuffer buffer) {
		ReflectionCheckEntry reflectionCheckEntry = new ReflectionCheckEntry();
		reflectionCheckEntry.count = buffer.readUnsignedByte();
		reflectionCheckEntry.uid = buffer.readDWord();
		reflectionCheckEntry.checkTypes = new int[reflectionCheckEntry.count];
		reflectionCheckEntry.methodsArgumentData = new byte[reflectionCheckEntry.count][][];
		reflectionCheckEntry.fieldsInformation = new Field[reflectionCheckEntry.count];
		reflectionCheckEntry.methodsInformation = new Method[reflectionCheckEntry.count];
		reflectionCheckEntry.errorTypes = new int[reflectionCheckEntry.count];
		reflectionCheckEntry.fieldsValue = new int[reflectionCheckEntry.count];
		for (int id = 0; id < reflectionCheckEntry.count; id++) {
			try {
				int type = buffer.readUnsignedByte();
				switch (type) {
					case 0:
					case 1:
					case 2: {
						String className = buffer.readString();
						int fieldValue = 0;
						String fieldName = buffer.readString();
						if (type == 1) {
							fieldValue = buffer.readDWord();
						}
						reflectionCheckEntry.checkTypes[id] = type;
						reflectionCheckEntry.fieldsValue[id] = fieldValue;
						Class<?> classType = getClassType(className);
						if (classType.getClassLoader() == null) {
							throw new SecurityException();
						}
	
						reflectionCheckEntry.fieldsInformation[id] = classType.getDeclaredField(fieldName);
						break;
					}
					case 3:
					case 4: {
						String className = buffer.readString();
						String methodName = buffer.readString();
						int argumentCount = buffer.readUnsignedByte();
						String[] argumentTypes = new String[argumentCount];
						for (int argId = 0; argId < argumentCount; argId++) {
							argumentTypes[argId] = buffer.readString();
						}
	
						String returnType = buffer.readString();
						byte[][] argumentData = new byte[argumentCount][];
						if (type == 3) {
							for (int argId = 0; argId < argumentCount; argId++) {
								int argumentDataLen = buffer.readDWord();
								argumentData[argId] = new byte[argumentDataLen];
								buffer.readBytes(argumentDataLen, 0, argumentData[argId]);
							}
						}
						reflectionCheckEntry.checkTypes[id] = type;
	
						Class[] argumentClassTypes = new Class[argumentCount];
						for (int argId = 0; argId < argumentCount; argId++) {
							argumentClassTypes[argId] = getClassType(argumentTypes[argId]);
						}
						Class returnTypeClass = getClassType(returnType);
						Class<?> classType = getClassType(className);
						if (classType.getClassLoader() == null) {
							throw new SecurityException();
						}
	
						Method[] methods = classType.getDeclaredMethods();
						for (int i = 0; i < methods.length; i++) {
							Method method = methods[i];
							if (!method.getName().equals(methodName)) {
								continue;
							}
	
							Class[] paramTypes = method.getParameterTypes();
							if (argumentCount != paramTypes.length) {
								continue;
							}
	
							boolean paramsMatch = true;
							for (int j = 0; j < argumentCount; j++) {
								if (paramTypes[j] != argumentClassTypes[j]) {
									paramsMatch = false;
									break;
								}
							}
	
							if (paramsMatch && returnTypeClass == method.getReturnType()) {
								reflectionCheckEntry.methodsInformation[id] = method;
							}
						}
	
						reflectionCheckEntry.methodsArgumentData[id] = argumentData;
						break;
					}
				}
			} catch (ClassNotFoundException classnotfoundexception) {
				reflectionCheckEntry.errorTypes[id] = -1;
			} catch (SecurityException securityexception) {
				reflectionCheckEntry.errorTypes[id] = -2;
			} catch (NullPointerException nullpointerexception) {
				reflectionCheckEntry.errorTypes[id] = -3;
			} catch (Exception exception) {
				reflectionCheckEntry.errorTypes[id] = -4;
			} catch (Throwable throwable) {
				reflectionCheckEntry.errorTypes[id] = -5;
			}
		}

		reflectionCheckDeque.insertHead(reflectionCheckEntry);
	}

	public static void processReflectionChecks(RSBuffer buffer) {
		for (;;) {
			ReflectionCheckEntry reflectionCheckEntry = (ReflectionCheckEntry) reflectionCheckDeque.reverseGetFirst();
			if (reflectionCheckEntry == null) {
				break;
			}

			buffer.sendPacket(78);
			buffer.writeByte(0);
			int startPos = buffer.currentOffset;
			buffer.writeDWord(reflectionCheckEntry.uid);
			for (int id = 0; id < reflectionCheckEntry.count; id++) {
				if (reflectionCheckEntry.errorTypes[id] != 0) {
					buffer.writeByte(reflectionCheckEntry.errorTypes[id]);
				} else {
					try {
						int type = reflectionCheckEntry.checkTypes[id];
						switch (type) {
							case 0: {
								Field field = (Field) reflectionCheckEntry.fieldsInformation[id];
								int fieldValue = field.getInt(null);
								buffer.writeByte(0);
								buffer.writeDWord(fieldValue);
								break;
							}
							case 1: {
								Field field = (Field) reflectionCheckEntry.fieldsInformation[id];
								field.setInt(null, reflectionCheckEntry.fieldsValue[id]);
								buffer.writeByte(0);
								break;
							}
							case 2: {
								Field field = (Field) reflectionCheckEntry.fieldsInformation[id];
								int fieldModifier = field.getModifiers();
								buffer.writeByte(0);
								buffer.writeDWord(fieldModifier);
								break;
							}
							case 3: {
								Method method = (Method) reflectionCheckEntry.methodsInformation[id];
								byte[][] argumentData = reflectionCheckEntry.methodsArgumentData[id];
								Object[] arguments = new Object[argumentData.length];
								for (int argId = 0; argId < argumentData.length; argId++) {
									ObjectInputStream objectinputstream = new ObjectInputStream(
											new ByteArrayInputStream(argumentData[argId]));
									arguments[argId] = objectinputstream.readObject();
								}
								Object object = method.invoke(null, arguments);
								if (object == null) {
									buffer.writeByte(0);
								} else if (object instanceof Number) {
									buffer.writeByte(1);
									buffer.writeQWord(((Number) object).longValue());
								} else if (object instanceof String) {
									buffer.writeByte(2);
									buffer.writeString((String) object);
								} else {
									buffer.writeByte(4);
								}
								break;
							}
							case 4:{
								Method method = (Method) reflectionCheckEntry.methodsInformation[id];
								int methodModifier = method.getModifiers();
								buffer.writeByte(0);
								buffer.writeDWord(methodModifier);
								break;
							}
						}
					} catch (ClassNotFoundException classnotfoundexception) {
						buffer.writeByte(-10);
					} catch (InvalidClassException invalidclassexception) {
						buffer.writeByte(-11);
					} catch (StreamCorruptedException streamcorruptedexception) {
						buffer.writeByte(-12);
					} catch (OptionalDataException optionaldataexception) {
						buffer.writeByte(-13);
					} catch (IllegalAccessException illegalaccessexception) {
						buffer.writeByte(-14);
					} catch (IllegalArgumentException illegalargumentexception) {
						buffer.writeByte(-15);
					} catch (InvocationTargetException invocationtargetexception) {
						buffer.writeByte(-16);
					} catch (SecurityException securityexception) {
						buffer.writeByte(-17);
					} catch (IOException ioexception) {
						buffer.writeByte(-18);
					} catch (NullPointerException nullpointerexception) {
						buffer.writeByte(-19);
					} catch (Exception exception) {
						buffer.writeByte(-20);
					} catch (Throwable throwable) {
						buffer.writeByte(-21);
					}
				}
			}

			buffer.putCrc32(startPos);
			buffer.writeBytes(buffer.currentOffset - startPos);
			reflectionCheckEntry.unlink();
		}
	}

	static Class getClassType(String string) throws ClassNotFoundException {
		if (string.equals("B")) {
			return Byte.TYPE;
		}
		if (string.equals("I")) {
			return Integer.TYPE;
		}
		if (string.equals("S")) {
			return Short.TYPE;
		}
		if (string.equals("J")) {
			return Long.TYPE;
		}
		if (string.equals("Z")) {
			return Boolean.TYPE;
		}
		if (string.equals("F")) {
			return Float.TYPE;
		}
		if (string.equals("D")) {
			return Double.TYPE;
		}
		if (string.equals("C")) {
			return Character.TYPE;
		}
		if (string.equals("void")) {
			return Void.TYPE;
		}
		return Class.forName(string);
	}

}
