package com.soulplayps.client.reflectioncheck;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.soulplayps.client.node.Node;

public class ReflectionCheckEntry extends Node {
	byte[][][] methodsArgumentData;
	int[] fieldsValue;
	int count;
	Field[] fieldsInformation;
	int[] checkTypes;
	int uid;
	Method[] methodsInformation;
	int[] errorTypes;

	public ReflectionCheckEntry() {
		/* empty */
	}
}
