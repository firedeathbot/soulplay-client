package com.soulplayps.client;
/*
 * Copyright (C) 2016 SoulPlayPS <http://www.soulplayps.com/> - All Rights
 * Reserved Unauthorized copying of content, via any medium, is strictly
 * prohibited. All work is proprietary and confidential, and belongs to the
 * copyright holder.
 */

import static com.soulplayps.client.interfaces.RSInterface.addButton;
import static com.soulplayps.client.interfaces.RSInterface.addLayer;
import static com.soulplayps.client.interfaces.RSInterface.addNewText;
import static com.soulplayps.client.interfaces.RSInterface.addRectangle;

import java.applet.AppletContext;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.CRC32;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.soulplayps.client.audio.WavPlayer;
import com.soulplayps.client.chathistory.ChatMessage;
import com.soulplayps.client.chathistory.ChatMessageManager;
import com.soulplayps.client.content.ActionCursors;
import com.soulplayps.client.interfaces.widget.Widget;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.util.*;
import com.soulplayps.club.discord.RichPresence;

import org.lwjgl.input.Cursor;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.google.common.io.Files;
import com.soulplayps.client.Camera.CamType;
import com.soulplayps.client.applet.CacheDownloader;
import com.soulplayps.client.applet.RSApplet;
import com.soulplayps.client.applet.RSImageProducer;
import com.soulplayps.client.fs.RSFileStore;
import com.soulplayps.client.data.FileSystemWorker;
import com.soulplayps.client.data.SignLink;
import com.soulplayps.client.fog.AbstractFog;
import com.soulplayps.client.hwid.CreateUID;
import com.soulplayps.client.interfaces.CustomInterfaces;
import com.soulplayps.client.interfaces.InterfaceDecoder;
import com.soulplayps.client.interfaces.InterfaceEvent;
import com.soulplayps.client.interfaces.InterfaceTargetMask;
import com.soulplayps.client.interfaces.MouseEvent;
import com.soulplayps.client.interfaces.RSInterface;
import com.soulplayps.client.interfaces.enums.ChatCrownType;
import com.soulplayps.client.interfaces.enums.DonationInterfaceCategoryType;
import com.soulplayps.client.interfaces.enums.DungRewardData;
import com.soulplayps.client.interfaces.enums.RankInfo;
import com.soulplayps.client.interfaces.enums.TeleportInfo;
import com.soulplayps.client.interfaces.enums.VoteStoreData;
import com.soulplayps.client.interfaces.impl.GambleInterfaces;
import com.soulplayps.client.interfaces.impl.InstanceInterface;
import com.soulplayps.client.interfaces.impl.NightmareOverlay;
import com.soulplayps.client.interfaces.impl.PresetsInterface;
import com.soulplayps.client.interfaces.impl.powers.PowerUpInterface;
import com.soulplayps.client.interfaces.impl.powers.PowerUpRoll;
import com.soulplayps.client.login.LoginRenderer;
import com.soulplayps.client.login.Profiles;
import com.soulplayps.client.login.ScriptManager;
import com.soulplayps.client.login.TitleState;
import com.soulplayps.client.maps.CheapHax;
import com.soulplayps.client.minimenu.MenuManager;
import com.soulplayps.client.music.Class25;
import com.soulplayps.client.music.Class3_Sub7;
import com.soulplayps.client.music.Class3_Sub7_Sub1;
import com.soulplayps.client.music.Class3_Sub7_Sub2;
import com.soulplayps.client.music.Class3_Sub9_Sub1;
import com.soulplayps.client.music.Class5;
import com.soulplayps.client.music.Class56;
import com.soulplayps.client.music.Class56_Sub1_Sub1;
import com.soulplayps.client.music.Class5_Sub1;
import com.soulplayps.client.music.Class5_Sub2;
import com.soulplayps.client.music.Class5_Sub2_Sub1;
import com.soulplayps.client.music.Class5_Sub2_Sub2;
import com.soulplayps.client.music.Sound;
import com.soulplayps.client.network.NIOSocket;
import com.soulplayps.client.network.RSSocket;
import com.soulplayps.client.network.SocketImpl;
import com.soulplayps.client.node.NodeList;
import com.soulplayps.client.node.animable.Animable_Sub3;
import com.soulplayps.client.node.animable.Animable_Sub4;
import com.soulplayps.client.node.animable.Animable_Sub5;
import com.soulplayps.client.node.animable.Animation;
import com.soulplayps.client.node.animable.entity.Entity;
import com.soulplayps.client.node.animable.entity.EntityDef;
import com.soulplayps.client.node.animable.entity.FamiliarHandler;
import com.soulplayps.client.node.animable.entity.Npc;
import com.soulplayps.client.node.animable.entity.Player;
import com.soulplayps.client.node.animable.entity.PlayerAppearance;
import com.soulplayps.client.node.animable.entity.PlayerClone;
import com.soulplayps.client.node.animable.entity.PlayerOption;
import com.soulplayps.client.node.animable.item.CustomizedItem;
import com.soulplayps.client.node.animable.item.Item;
import com.soulplayps.client.node.animable.item.ItemDef;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.animable.particles.Particle;
import com.soulplayps.client.node.animable.particles.ParticleDefinition;
import com.soulplayps.client.node.object.GameObject;
import com.soulplayps.client.node.object.Object1;
import com.soulplayps.client.node.object.Object2;
import com.soulplayps.client.node.object.Object3;
import com.soulplayps.client.node.object.Object5;
import com.soulplayps.client.node.object.ObjectDef;
import com.soulplayps.client.node.object.ObjectManager;
import com.soulplayps.client.node.object.WorldController;
import com.soulplayps.client.node.ondemand.OnDemandData;
import com.soulplayps.client.node.ondemand.OnDemandFetcher;
import com.soulplayps.client.node.raster.Background;
import com.soulplayps.client.node.raster.RSFont;
import com.soulplayps.client.node.raster.Raster;
import com.soulplayps.client.node.raster.Rasterizer;
import com.soulplayps.client.node.raster.Texture;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.opengl.gl15.VBORenderer;
import com.soulplayps.client.opengl.gl15.model.ModelConfig;
import com.soulplayps.client.opengl.gl15.model.TextureAtlas;
import com.soulplayps.client.opengl.gl15.model.simple.ModelLoader;
import com.soulplayps.client.opengl.util.AtlasTextureLoader;
import com.soulplayps.client.opengl.util.Cache;
import com.soulplayps.client.opengl.util.GLBufferManager;
import com.soulplayps.client.opengl.util.SimpleRendering;
import com.soulplayps.client.opengl.util.VBOManager;
import com.soulplayps.client.reflectioncheck.ReflectionCheck;
import com.soulplayps.client.settings.Settings;
import com.soulplayps.client.unknown.Class11;
import com.soulplayps.client.unknown.Class36;
import com.soulplayps.client.unpack.Flo;
import com.soulplayps.client.unpack.IDK;
import com.soulplayps.client.unpack.FloHigherRev;
import com.soulplayps.client.unpack.FloOsrs;
import com.soulplayps.client.unpack.SpotAnim;
import com.soulplayps.client.unpack.VarBit;
import com.soulplayps.client.unpack.Varp;
import org.lwjgl.opengl.GL11;

public class Client extends RSApplet {

	public static boolean useNewSocket = true;
	public static boolean disableOpenGlErrors = true;
	public static String[] chatHistory = new String[20];
	public static int chatHistoryPos = 0;
	public static int chatHistoryPollPos = -1;
	// animated firecape and magic trees?
	public static boolean useAnimatedTextures = true;

	public static boolean useShadows = false;
	public static int shadowMapResolution = 1024;
	public static int shadowSetting = 0;
	public static FileSystemWorker fileSystemWorker;
	public static int notchesPoll;
	public static int playerRenderPriorityIndex = -1;
	public static SecureRandom secureRandom;
	public static int loginTick;
	public static RichPresence DISCORD_PRESENCE = new RichPresence();
	public static WorldType worldType = WorldType.ECONOMY;

	private static void applyShadows(final int setting, Client c) {
		if (!GraphicsDisplay.enabled || !GraphicsDisplay.vboRendering || !GraphicsDisplay.FBOsSupported
				|| !GraphicsDisplay.shadersSupported) {
			shadowSetting = 0;
			useShadows = false;
			if (c != null) {
				c.pushGameMessage("Shadows are not supported on your system.");
			}
			return;
		}
		shadowSetting = setting;

		if (shadowSetting == 0) {
			useShadows = false;
		} else {
			useShadows = true;
			if (shadowSetting == 1)
				shadowMapResolution = 1024; // very low
			else if (shadowSetting == 2)
				shadowMapResolution = 2048; // low
			else if (shadowSetting == 3)
				shadowMapResolution = 3072; // medium
			else if (shadowSetting == 4)
				shadowMapResolution = 4096; // high
		}
	}

	public static boolean drawAnimatedWorldBackground = false;

	public static boolean enableHDTextures = false;
	public static int prevXAmount = 100;

	public static MouseHover mouseHoverEnum = MouseHover.NOTHING;

	public static CalendarHandler calendar;

	public Announcement announcement;
	public static boolean gameLoaded = false;

	public static boolean noClip = false;

	public static boolean animateSummonTab = false;

	public static boolean holdingCtrlKey = false;
	public static boolean holdingShiftKey = false;
	public static boolean shiftDropEnabled;

	/**
	 * hotkey stuff
	 */
	@Override
	public void handleHotKey(int hotkeyId) {
		if (hotkeyId >= 0 && hotkeyId <= 11) { // set tabs
			setTab(hotkeyId);
		} else if (hotkeyId == 16) { // spec button click
			sendClickedButton(7788);
		} else if (hotkeyId == 17) { // quick prayer button click
			sendClickedButton(5000);
		} else if (hotkeyId == 15) { // summon special button click
			sendClickedButton(17013);
		}
	}

	/**
	 * Login Junk:
	 */
	public int terrainRegionX;

	public int terrainRegionY;

	public int[] titleScreenOffsets = null;

	public int titleWidth = -1;

	public int titleHeight = -1;

	public ScriptManager scriptManager;

	public static HotKeys HOT_KEYS;

	public void generateWorld(int x, int y) {
		terrainRegionX = x;
		terrainRegionY = y;
		constructedViewport = false;
		if (anInt1069 == x && anInt1070 == y && loadingStage == 2) {
			return;
		}
		anInt1069 = x;
		anInt1070 = y;
		regionBaseX = (anInt1069 - 6) * 8;
		regionBaseY = (anInt1070 - 6) * 8;
		inTutorialIsland = (anInt1069 >> 3 == 48 || anInt1069 >> 3 == 49) && anInt1070 >> 3 == 48;
		if (anInt1069 >> 3 == 48 && anInt1070 >> 3 == 148)
			inTutorialIsland = true;
		loadingStage = 1;
		loadingStartTime = System.currentTimeMillis();
		int k16 = 0;
		for (int i21 = (anInt1069 - 6) >> 3; i21 <= (anInt1069 + 6) >> 3; i21++) {
			for (int k23 = (anInt1070 - 6) >> 3; k23 <= (anInt1070 + 6) >> 3; k23++)
				k16++;
		}
		localRegionMapData = new byte[k16][];
		localRegionLandscapeData = new byte[k16][];
		localRegionIds = new int[k16];
		localRegionMapIds = new int[k16];
		localRegionLandscapeIds = new int[k16];
		k16 = 0;
		for (int l23 = (anInt1069 - 6) >> 3; l23 <= (anInt1069 + 6) >> 3; l23++) {
			for (int j26 = (anInt1070 - 6) >> 3; j26 <= (anInt1070 + 6) >> 3; j26++) {
				localRegionIds[k16] = (l23 << 8) + j26;
				if (inTutorialIsland
						&& (j26 == 49 || j26 == 149 || j26 == 147 || l23 == 50 || l23 == 49 && j26 == 47)) {
					localRegionMapIds[k16] = -1;
					localRegionLandscapeIds[k16] = -1;
					k16++;
				} else {
					int cacheIndex = 3;
					if (l23 >= 100)
						cacheIndex = 6;
					int regionId = (l23 << 8) + j26;
					int k28 = localRegionMapIds[k16] = onDemandFetcher.method562(0, regionId);
					if (k28 != -1)
						onDemandFetcher.loadData(cacheIndex, k28);
					int j30 = localRegionLandscapeIds[k16] = onDemandFetcher.method562(1, regionId);
					if (j30 != -1)
						onDemandFetcher.loadData(cacheIndex, j30);
					k16++;
				}
			}
		}
		int i17 = regionBaseX - anInt1036;
		int j21 = regionBaseY - anInt1037;
		anInt1036 = regionBaseX;
		anInt1037 = regionBaseY;
		for (int j24 = 0; j24 < 16384; j24++) {
			Npc npc = npcArray[j24];
			if (npc != null) {
				for (int j29 = 0; j29 < 10; j29++) {
					npc.smallX[j29] -= i17;
					npc.smallY[j29] -= j21;
				}
				npc.x -= i17 * 128;
				npc.z -= j21 * 128;
			}
		}
		for (int i27 = 0; i27 < maxPlayers; i27++) {
			Player player = playerArray[i27];
			if (player != null) {
				for (int i31 = 0; i31 < 10; i31++) {
					player.smallX[i31] -= i17;
					player.smallY[i31] -= j21;
				}
				player.x -= i17 * 128;
				player.z -= j21 * 128;
			}
		}
		aBoolean1080 = true;
		byte byte1 = 0;
		byte byte2 = 104;
		byte byte3 = 1;
		if (i17 < 0) {
			byte1 = 103;
			byte2 = -1;
			byte3 = -1;
		}
		byte byte4 = 0;
		byte byte5 = 104;
		byte byte6 = 1;
		if (j21 < 0) {
			byte4 = 103;
			byte5 = -1;
			byte6 = -1;
		}
		for (int k33 = byte1; k33 != byte2; k33 += byte3) {
			for (int l33 = byte4; l33 != byte5; l33 += byte6) {
				int i34 = k33 + i17;
				int j34 = l33 + j21;
				for (int k34 = 0; k34 < 4; k34++)
					if (i34 >= 0 && j34 >= 0 && i34 < 104 && j34 < 104)
						groundArray[k34][k33][l33] = groundArray[k34][i34][j34];
					else
						groundArray[k34][k33][l33] = null;
			}
		}
		for (GameObject class30_sub1_1 = (GameObject) aClass19_1179
				.reverseGetFirst(); class30_sub1_1 != null; class30_sub1_1 = (GameObject) aClass19_1179
				.reverseGetNext()) {
			class30_sub1_1.tileX -= i17;
			class30_sub1_1.tileY -= j21;
			if (class30_sub1_1.tileX < 0 || class30_sub1_1.tileY < 0 || class30_sub1_1.tileX >= 104
					|| class30_sub1_1.tileY >= 104)
				class30_sub1_1.unlink();
		}
		if (destX != 0) {
			destX -= i17;
			destY -= j21;
		}
		aBoolean1160 = false;
	}

	public void resetWorld(int stage) {
		if (stage == 0) {
			/* anInt1278 */
			Camera.cameraOffsetX = (int) (Math.random() * 100D) - 50;
			/* anInt1131 */
			Camera.cameraOffsetY = (int) (Math.random() * 110D) - 55;
			/* anInt896 */
			viewRotationOffset = (int) (Math.random() * 80D) - 40;
			/* minimapInt2 */
			minimapRotation = (int) (Math.random() * 120D) - 60;
			/* minimapInt3 */
			minimapZoom = (int) (Math.random() * 30D) - 20;
			/* minimapInt1 */
			viewRotation = (int) (Math.random() * 20D) - 10 & 0x7ff;
			minimapBlackout = 0;
			loadingStage = 1;
		} else if (stage == 1) {
			aBoolean1080 = false;
		}
	}

	private void loginScreenBG(boolean b) {
		xCameraPos = 6100;
		yCameraPos = 6867;
		zCameraPos = -750;
		xCameraCurve = 2040;
		yCameraCurve = 383;
		resetWorld(0);
		if (b || scriptManager == null) {
			scriptManager = new ScriptManager(this);
		} else {
			scriptManager.update();
		}
		plane = scriptManager.regionPlane;
		generateWorld(scriptManager.terrainRegionX, scriptManager.terrainRegionY);
		resetWorld(1);
	}

	public boolean reloadWorldAnim = false;

	public void drawAnimatedWorldBackground() {
		if (!drawAnimatedWorldBackground) {
			if (SpriteCache.backgroundSprite != null) {
				SpriteCache.backgroundSprite.drawSprite(0, 0);
			}
			return;
		}

		if (reloadWorldAnim && !GraphicsDisplay.enabled) {
			setHighQualityLoginScreen();
			Rasterizer.setBounds(765, 503);
			fullScreenTextureArray = Rasterizer.lineOffsets;

			Rasterizer.setBounds(clientWidth, clientHeight);
			anIntArray1182 = Rasterizer.lineOffsets;
			reloadWorldAnim = false;
		}

		int centerX = clientWidth;// / 2
		int centerY = clientHeight; // / 2
		if (scriptManager == null) {
			loginScreenBG(true);
		}
		int canvasCenterX = Raster.centerX;
		int canvasCenterY = Raster.centerY;
		int canvasPixels[] = Rasterizer.lineOffsets;
		if (titleScreenOffsets != null && (titleWidth != clientWidth || titleHeight != clientHeight)) {
			titleScreenOffsets = null;
		}
		if (titleScreenOffsets == null) {
			titleWidth = clientWidth;
			titleHeight = clientHeight;
			titleScreenOffsets = Rasterizer.getOffsets(titleWidth, titleHeight);
		}
		Raster.centerX = centerX;
		Raster.centerY = centerY;
		Rasterizer.lineOffsets = titleScreenOffsets;
		if (loadingStage == 2 && ObjectManager.anInt131 != plane)
			loadingStage = 1;

		if (!loggedIn && loadingStage == 1) {
			method54();
		}
		if (loadingStage == 2) {
			// Rasterizer.triangles = 0;
			if (GraphicsDisplay.enabled)
				GraphicsDisplay.getInstance().beginRenderingWorld(this);
			try {
				worldController.method313(xCameraPos, yCameraPos, xCameraCurve, zCameraPos, method121(), yCameraCurve,
						6464, 6464, plane);
				worldController.clearObj5Cache();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (GraphicsDisplay.enabled)
				GraphicsDisplay.getInstance().endRenderingWorld(this, 1);
		}
		if (scriptManager != null && loadingStage == 2 && !loggedIn) {
			scriptManager.cycle();
		}
		Raster.centerX = canvasCenterX;
		Raster.centerY = canvasCenterY;
		Rasterizer.lineOffsets = canvasPixels;
	}

	/**
	 * End of login junk
	 */

	public static volatile int prevWidth;

	public static volatile int prevHeight;

	public void swapToFullscreenPixmap() {
		if (super.fullGameScreen != null) {
			return;
		}

		chatIP = null;
		mapIP = null;
		inventoryIP = null;
		gameScreenIP = null;

		/*
		 * aRSImageProducer_1166 = null; aRSImageProducer_1164 = null;
		 * aRSImageProducer_1163 = null; aRSImageProducer_1165 = null;
		 * aRSImageProducer_1123 = null; aRSImageProducer_1124 = null;
		 * aRSImageProducer_1125 = null; aRSImageProducer_1107 = null;
		 * aRSImageProducer_1108 = null; aRSImageProducer_1109 = null;
		 * aRSImageProducer_1110 = null; aRSImageProducer_1111 = null;
		 * aRSImageProducer_1112 = null; aRSImageProducer_1113 = null;
		 * aRSImageProducer_1114 = null; aRSImageProducer_1115 = null;
		 */
		super.fullGameScreen = new RSImageProducer(765, 503, canvas);
		if (!GraphicsDisplay.enabled) {
			Raster.reset();
		}

		fullRedraw = true;
	}

	private static final long serialVersionUID = 1L;

	public LoginRenderer loginRenderer = new LoginRenderer(this);

	Sprite itemSearchItemBg;
	Sprite invBack;

	Sprite chatBackground;

	Sprite mapBack;

	Sprite backBase1;

	Sprite backBase2;

	Sprite backHmid1;

	// private boolean revision474;
	// private boolean revision498 = true;
	// private boolean revision508;
	public boolean roofOff = false;

	public static boolean displayFog = false;

	public boolean displayParticles = false;

	public boolean showAutoChat = false;

	public static int hpModifier = 1;

	public static int clickSpeed = 7;

	public static int rotationWorlds = 0;

	public static int cameraZoom = 600;

	public static boolean displayPing = false;

	public static boolean worldOnline = true;

	public static long ping = 100;

	public boolean walkOptionFirst;

	public static boolean debugModels = false;

	private boolean usingWebclient = false;

	private boolean playerCompletedInitialize = false;

	public boolean isLockedExp = false;

	public static String osName = System.getProperty("os.name");

	public static final int FIXED_FULL_WIDTH = 765, FIXED_FULL_HEIGHT = 503;

	public static final int MIN_RESIZED_WIDTH = 800, MIN_RESIZED_HEIGHT = 600;

	public static volatile int clientWidth = FIXED_FULL_WIDTH, clientHeight = FIXED_FULL_HEIGHT;

	public static volatile int newClientWidth = FIXED_FULL_WIDTH, newClientHeight = FIXED_FULL_HEIGHT;

	public boolean usernameHover, passwordHover, worldHover, facebookHover, twitterHover, skypeHover,
			youtubeHover = false;

	public Sprite magicAuto;

	public boolean Autocast = false;

	public int autocastId = 0;

	public static boolean toggleNewHitBar = true, menuToggle = true, namesToggle = false, toggleNewHitMarks = true;

	// public static int zoom = 0;
	public static int hoverPos = -1;

	private Sprite[] hitMark;

	private Sprite blockHit;

	private Sprite[] hitIcon;

	private Sprite HPBarFull, HPBarEmpty;

	private Sprite tabHover;

	private Sprite tabClicked;

	private Sprite closeIconHover;
	private Sprite closeIcon;
	private Sprite magnifyingGlass;
	private Sprite selectedTab;
	private Sprite tab;
	private Sprite tabBackground;
	private Sprite tabBorder;
	private Sprite dungMapSprite;
	private Sprite moneyPouchHover;
	private Sprite moneyPouch;

	private CRC32 crc32 = new CRC32();

	public void setSettings() {
		try {
			roofOff = (Boolean) Settings.getOrDefault("roofs_off", false);
			toggleNewHitBar = (Boolean) Settings.getOrDefault("new_hp", true);
			toggleNewHitMarks = (Boolean) Settings.getOrDefault("new_hitmarks", true);
			hpModifier = (Integer) Settings.getOrDefault("hpModifier", 1);

			if (!loggedIn)
				myUsername = (String) Settings.getOrDefault("username", "");

			splitPrivateChat = (Integer) Settings.getOrDefault("split_pm", 0);

			displayPing = (Boolean) Settings.getOrDefault("display_ping", false);
			fpsOn = (Boolean) Settings.getOrDefault("display_fps", false);

			displayFog = (Boolean) Settings.getOrDefault("fog", false);
			displayParticles = (Boolean) Settings.getOrDefault("particles", false);

			Client.prevXAmount = (Integer) Settings.getOrDefault("prev_x_amount", 100);

			enableHDTextures = (Boolean) Settings.getOrDefault("hd_textures", false);
			orbsEnabled = (Boolean) Settings.getOrDefault("display_orbs", true);

			ObjectManager.drawGroundDecor = (Boolean) Settings.getOrDefault("ground_decor", true);
			ObjectManager.lowMem = (Boolean) Settings.getOrDefault("disable_render_floors", true);
			WorldController.lowMem = (Boolean) Settings.getOrDefault("hd_water", true);

			drawAnimatedWorldBackground = (Boolean) Settings.getOrDefault("animate_bg", false);
			LoginRenderer.rememberMeChecked = (Integer) Settings.getOrDefault("remember_profile", 0) > 0;

			shiftDropEnabled = (Boolean) Settings.getOrDefault("shift_drop", false);
			drawCombatBox = (Boolean) Settings.getOrDefault("combat_box", false);
			tweening = (Boolean) Settings.getOrDefault("tweening", true);

			GraphicsDisplay.enabled = (Boolean) Settings.getOrDefault("opengl_enabled", false);
			
			Client.playerAttackOptionSetting = EntityAttackOptionSetting.find((Integer) Settings.getOrDefault("player_attack_option", 0));
			Client.npcAttackOptionSetting = EntityAttackOptionSetting.find((Integer) Settings.getOrDefault("npc_attack_option", 0));

		} catch (Exception ex) {
			Settings.createDefaults();
			ex.printStackTrace();
		}
	}

	public void resetSettingsOnLogin() {
		roofOff = (Boolean) Settings.getSetting("roofs_off");
		displayFog = (Boolean) Settings.getSetting("fog");
		displayParticles = (Boolean) Settings.getSetting("particles");
		enableHDTextures = (Boolean) Settings.getSetting("hd_textures");
		ObjectManager.drawGroundDecor = (Boolean) Settings.getSetting("ground_decor");
		ObjectManager.lowMem = (Boolean) Settings.getSetting("disable_render_floors");
		WorldController.lowMem = (Boolean) Settings.getSetting("hd_water");
	}

	public void setHighQualityLoginScreen() {
		roofOff = false;
		displayFog = true;
		displayParticles = true;
		enableHDTextures = true;
		ObjectManager.lowMem = false;
		WorldController.lowMem = false;
	}

	public void fixFrame() {
		initClientFrame2(503, 765);
	}

	private boolean[] orbTextEnabled = new boolean[4];

	public static FamiliarHandler getFamiliar() {
		return familiarHandler;
	}

	public final int[] skillInterfaceId = { 31113, 31123, 31118, 31158, 31128, 31133, 31138, 31218, 31228, 31183, 31213,
			31223, 31178, 31208, 31203, 31168, 31163, 31173, 31188, 31233, 31143, 31153, 31193, 31238, 31148 };

	public static final int ANIM_IDX = 2, AUDIO_IDX = 3, IMAGE_IDX = 5, TEXTURE_IDX = 6;

	public boolean mouseInRegion(int x1, int y1, int x2, int y2) {
		return super.mouseX >= x1 && super.mouseX <= x2 && super.mouseY >= y1 && super.mouseY <= y2;
	}

	public boolean clickInRegion(int x1, int y1, int x2, int y2) {
		return super.saveClickX >= x1 && super.saveClickX <= x2 && super.saveClickY >= y1 && super.saveClickY <= y2;
	}

	// public boolean displayModelID = false;
	// public static ArrayList<Integer> modelCollection = new
	// ArrayList<Integer>();

	public void setSidebarInterface(int sidebarID, int interfaceID) {
		tabInterfaceIDs[sidebarID] = interfaceID;
		tabID = sidebarID;
		needDrawTabArea = true;
	}

	public static void closeInterfaces() {
		if (openInventoryModalInterfaceId != -1) {
			openInventoryModalInterfaceId = -1;
			needDrawTabArea = true;
		}

		if (backDialogID != -1) {
			backDialogID = -1;
			inputTaken = true;
		}

		if (getInputDialogState() != 0) {
			setInputDialogState(0);
			inputTaken = true;
		}

		openModalInterfaceId = -1;
		aBoolean1149 = false;
	}

	public static void closeInterfacesTransmit() {
		closeInterfaces();
		instance.interfaceClosed();
	}

	public static void openInventoryModalInterface(int id) {
		openInventoryModalInterfaceId = id;
		onInterfaceLoad(id);
		needDrawTabArea = true;
	}

	public static void openModalInterface(int id) {
		if (openInventoryModalInterfaceId != -1) {
			openInventoryModalInterfaceId = -1;
			needDrawTabArea = true;
		}

		if (backDialogID != -1) {
			backDialogID = -1;
			inputTaken = true;
		}

		if (getInputDialogState() != 0) {
			setInputDialogState(0);
			inputTaken = true;
		}

		openModalInterfaceId = id;
		onInterfaceLoad(id);
		aBoolean1149 = false;

		if (id == 3559) {
			aBoolean1031 = true;
		}
	}

	public static void openOverlayInterface(int id) {
		openOverlayId = id;
		onInterfaceLoad(id);
	}

	public void drawInterfaces(boolean isFixed) {
		boolean reverse = openOverlayId == PowerUpRoll.ID;
		if (reverse) {
			drawModalInterface(isFixed);
			drawOverlayInterface(isFixed);
		} else {
			drawOverlayInterface(isFixed);
			drawModalInterface(isFixed);
		}
	}

	public void drawOverlayInterface(boolean isFixed) {
		if (openOverlayId == -1) {
			return;
		}

		method119(anInt945, openOverlayId);

		RSInterface parentInterface = RSInterface.interfaceCache[openOverlayId];
		int xOffset = 0;
		int yOffset = 0;

		switch (openOverlayId) {
			case 201:
				xOffset = isFixed ? 0 : clientWidth - FIXED_FULL_WIDTH + 30;
				yOffset = isFixed ? 0 : clientHeight - FIXED_FULL_HEIGHT;
				break;
			case 193:
			case 197:
				xOffset = isFixed ? 0 : clientWidth - FIXED_FULL_WIDTH + 230;
				yOffset = isFixed ? 0 : -83;
				break;
			case 6673:
				xOffset = isFixed ? 0 : clientWidth / 2 - 300;
				break;
			case 16128:
				xOffset = isFixed ? 0 : clientWidth - 750;
				break;
			case PowerUpRoll.ID:
				if (isFixed) {
					xOffset = 0;
					yOffset = -35;
				} else {
					xOffset = (clientWidth - 240 - 512) / 2;
					yOffset = 0;
				}
				break;
			default:
				xOffset += parentInterface.x;
				yOffset += parentInterface.y;
				break;
		}

		// System.out.println("Update " +clientWidth +", "+anInt1018);
		drawInterface(parentInterface, xOffset, yOffset, 0);
	}

	public void drawModalInterface(boolean isFixed) {
		if (openModalInterfaceId == -1) {
			if (shouldDrawCombatBox()) {
				drawCombatBox(currentInteract);
			}

			return;
		}

		if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					if (Client.HOT_KEYS == null) {
						return;
					}

					Client.HOT_KEYS.setAlwaysOnTop(false);
					Client.HOT_KEYS.dispose();
				}
			});
		}

		method119(anInt945, openModalInterfaceId);
		int x;
		int y;
		if (isFixed) {
			x = 0;
			y = 0;
		} else {
			x = (clientWidth / 2) - 356;
			y = (clientHeight / 2) - 267;
		}

		drawInterface(RSInterface.interfaceCache[openModalInterfaceId], x, y, 0);
	}

	public void openCacheFolder() {
		if (!Desktop.isDesktopSupported()) {
			return;
		}

		try {
			Desktop.getDesktop().open(new File(SignLink.cacheDir));

			File ss3Folder = SignLink.findSoulSplit3Folder();
			if (ss3Folder.exists())
				Desktop.getDesktop().open(ss3Folder);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void hitmarkDraw(int hitLength, int type, int icon, int damage, int move, int opacity) {
		if (damage <= 0) {
			blockHit.drawTransparentSprite(spriteDrawX - 12, spriteDrawY - 14 + move, opacity);
			return;
		}

		Sprite end1 = null, middle = null, end2 = null;
		int x = 0;
		switch (hitLength) {
			case 1:
				x = 8;
				break;
			case 2:
				x = 4;
				break;
			case 3:
				x = 1;
				break;
		}

		switch (type) {
			case 0:
				end1 = hitMark[0];
				middle = hitMark[1];
				end2 = hitMark[2];
				break;
			case 1:
				end1 = hitMark[3];
				middle = hitMark[4];
				end2 = hitMark[5];
				break;
			case 2:
				end1 = hitMark[6];
				middle = hitMark[7];
				end2 = hitMark[8];
				break;
		}

		if ((type <= 1 || icon != -1) && icon != 255 && icon <= hitIcon.length) {
			hitIcon[icon].drawTransparentSprite(spriteDrawX - 34 + x, spriteDrawY - 14 + move, opacity);
		}

		end1.drawTransparentSprite(spriteDrawX - 12 + x, spriteDrawY - 12 + move, opacity);
		x += 4;
		for (int i = 0; i < hitLength * 2; i++) {
			middle.drawTransparentSprite(spriteDrawX - 12 + x, spriteDrawY - 12 + move, opacity);
			x += 4;
		}
		end2.drawTransparentSprite(spriteDrawX - 12 + x, spriteDrawY - 12 + move, opacity);
		if (opacity > 100) {
			(type == 1 ? newBoldFont : newSmallFont).drawCenteredString(String.valueOf(damage), spriteDrawX + 4,
					spriteDrawY + (type == 1 ? 2 : 3/* 32 */) + move, 0xffffff, -1);
		}
	}

	private static String intToKOrMilLongName(int i) {
		String s = String.valueOf(i);
		for (int k = s.length() - 3; k > 0; k -= 3)
			s = s.substring(0, k) + "," + s.substring(k);
		if (s.length() > 8)
			s = "<col=ff00>" + s.substring(0, s.length() - 8) + " million <col=ffffff>(" + s + ")";
		else if (s.length() > 4)
			s = "<col=ffff>" + s.substring(0, s.length() - 4) + "K <col=ffffff>(" + s + ")";
		return " " + s;
	}

	private static final String[] TITLE_COLOR = { "ff7000", "FF00CD", "ff0000", "148200", "0", "ffff00", "ffff",
			"ffffff", "2b547e" };

	public final String titleColor(final int i) {
		return TITLE_COLOR[i];
	}

	public static void setTab(int id) {
		needDrawTabArea = true;
		tabID = id;
	}

	private static final String[] CHAT_BUTTONS_OPS_TEXTS = { "On", "Friends", "Off", "Hide" };
	private static final int[] CHAT_BUTTONS_TEXT_COLORS = { 65280, 0xffff00, 0xff0000, 65535 };

	public void drawChannelButtons(int xPosOffset, int yPosOffset) {
		chatButtons[1].drawSprite(CHAT_BUTTONS_POS_X[cButtonCPos] + xPosOffset, 142 + yPosOffset);
		if (cButtonHPos == cButtonCPos) { // clicked mouse hover
			int sprite = cButtonHPos == 7 ? 3 : 2;
			chatButtons[sprite].drawSprite(CHAT_BUTTONS_POS_X[cButtonHPos] + xPosOffset, 142 + yPosOffset);
		} else if (cButtonHPos != -1) { // mouse hover unclicked
			int sprite = cButtonHPos == 7 ? 3 : 0;
			chatButtons[sprite].drawSprite(CHAT_BUTTONS_POS_X[cButtonHPos] + xPosOffset, 142 + yPosOffset);
		}

		newSmallFont.drawBasicString("All", 26 + xPosOffset, 157 + yPosOffset, 0xffffff, 0);
		newSmallFont.drawBasicString("Game", 76 + xPosOffset, 153 + yPosOffset, 0xffffff, 0);
		newSmallFont.drawBasicString("Public", 131 + xPosOffset, 153 + yPosOffset, 0xffffff, 0);
		newSmallFont.drawBasicString("Private", 184 + xPosOffset, 153 + yPosOffset, 0xffffff, 0);
		newSmallFont.drawBasicString("Clan", 249 + xPosOffset, 153 + yPosOffset, 0xffffff, 0);
		newSmallFont.drawBasicString("Trade", 304 + xPosOffset, 153 + yPosOffset, 0xffffff, 0);
		newSmallFont.drawBasicString("Yells", 362 + xPosOffset, 153 + yPosOffset, 0xffffff, 0);

		if (Settings.getOrDefault("display_timeplayed", false)) {
			newSmallFont.drawBasicString(Strings.formatTimePlayed(timePlayed), 435 + xPosOffset, 158 + yPosOffset, 0xffffff, 0);
		} else {
			newSmallFont.drawBasicString("Report", 442 + xPosOffset, 158 + yPosOffset, 0xffffff, 0);
		}

		if (showAutoChat) {
			newSmallFont.drawCenteredString("Autochat", 147 + xPosOffset, 164 + yPosOffset, 65535, 0);
		} else {
			newSmallFont.drawCenteredString(CHAT_BUTTONS_OPS_TEXTS[publicChatMode], 147 + xPosOffset, 164 + yPosOffset,
					CHAT_BUTTONS_TEXT_COLORS[publicChatMode], 0);
		}

		if (filterGameSpam) {
			newSmallFont.drawCenteredString("Filtered", 89 + xPosOffset, 164 + yPosOffset, 0xffff00, 0);
		} else {
			newSmallFont.drawCenteredString("All", 89 + xPosOffset, 164 + yPosOffset, 65280, 0);
		}

		newSmallFont.drawCenteredString(CHAT_BUTTONS_OPS_TEXTS[privateChatMode], 203 + xPosOffset, 164 + yPosOffset,
				CHAT_BUTTONS_TEXT_COLORS[privateChatMode], 0);
		newSmallFont.drawCenteredString(CHAT_BUTTONS_OPS_TEXTS[clanChatMode], 261 + xPosOffset, 164 + yPosOffset,
				CHAT_BUTTONS_TEXT_COLORS[clanChatMode], 0);
		newSmallFont.drawCenteredString(CHAT_BUTTONS_OPS_TEXTS[tradeMode], 318 + xPosOffset, 164 + yPosOffset,
				CHAT_BUTTONS_TEXT_COLORS[tradeMode], 0);
		newSmallFont.drawCenteredString(CHAT_BUTTONS_OPS_TEXTS[yellMode], 374 + xPosOffset, 164 + yPosOffset,
				CHAT_BUTTONS_TEXT_COLORS[yellMode], 0);
	}

	private void drawChatArea() {
		boolean isFixed = isFixed();
		boolean hdEnabled = GraphicsDisplay.enabled;
		int xPosOffset = 0;
		int yPosOffset = isFixed ? (hdEnabled ? 338 : 0) : clientHeight - 165;

		if (!hdEnabled) {
			if (isFixed) {
				chatIP.initDrawingArea();
			}

			Rasterizer.lineOffsets = anIntArray1180;
		}

		if (showChat) {
			// if (isFixed)
			chatArea.drawSprite(0 + xPosOffset, 0 + yPosOffset);
			// else
			// DrawingArea.drawAlphaGradient(7 + xPosOffset, 7 + yPosOffset,
			// 505, 130, 0xB5B5B5, 0, 70);
		}

		drawChannelButtons(xPosOffset, yPosOffset);

		if (messagePromptRaised) {
			newBoldFont.drawCenteredString(aString1121, 259 + xPosOffset, 60 + yPosOffset, 0, -1);
			newBoldFont.drawCenteredString(RSFont.escape(promptInput) + "*", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (getInputDialogState() == 1) {
			newBoldFont.drawCenteredString("Enter amount:", 259 + xPosOffset, 60 + yPosOffset, 0, -1);
			newBoldFont.drawCenteredString(amountOrNameInput + "*", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (getInputDialogState() == 7) {
			newBoldFont.drawCenteredString("Enter Bank Pin to proceed:", 259 + xPosOffset, 60 + yPosOffset, 0, -1);
			newBoldFont.drawCenteredString(amountOrNameInput + "*", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (getInputDialogState() == SET_DIALOG_AMOUNT_TO_WITHDRAW) {
			newBoldFont.drawCenteredString("How much would you like to withdraw?", 259 + xPosOffset, 60 + yPosOffset, 0,
					-1);
			newBoldFont.drawCenteredString(amountOrNameInput + "*", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (getInputDialogState() == 2) {
			newBoldFont.drawCenteredString("Enter name:", 259 + xPosOffset, 60 + yPosOffset, 0, -1);
			newBoldFont.drawCenteredString(amountOrNameInput + "*", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (getInputDialogState() == SET_SELL_PRICE_STATE) {
			newBoldFont.drawCenteredString("Enter the price you would like to sell this item for:", 259 + xPosOffset,
					60 + yPosOffset, 0, -1);
			newBoldFont.drawCenteredString(amountOrNameInput + "*", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (getInputDialogState() == SET_DIALOG_AMOUNT_TO_SELL) {
			newBoldFont.drawCenteredString("How many would you like to sell?", 259 + xPosOffset,
					60 + yPosOffset, 0, -1);
			newBoldFont.drawCenteredString(amountOrNameInput + "*", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (getInputDialogState() == SEARCH_STATE) {
			Raster.setDrawingArea(8 + xPosOffset, 7 + yPosOffset, 512 + xPosOffset, 121 + yPosOffset);

			if (amountOrNameInput.length() == 0) {
				newBoldFont.drawCenteredString("Item Search", 259 + xPosOffset, 35 + yPosOffset, 0, -1);
				newRegularFont.drawCenteredString("To search for an item, start by typing part of its name.",
						259 + xPosOffset, 70 + yPosOffset, 0, -1);
				newRegularFont.drawCenteredString("", 259 + xPosOffset, 85 + yPosOffset, 0, -1);
			} else if (totalItemResults == 0) {
				newBoldFont.drawCenteredString("No matching items found.", 259 + xPosOffset, 70 + yPosOffset, 0, -1);
			} else {
				int x = super.mouseX;
				int y = MenuManager.menuOpen ? super.saveClickY : super.mouseY;
				int hoverIndex = 0;
				int rectWidth = 413;
				int rectHeight = 13;
				boolean canSpawn = myPrivilege == 2 || myPrivilege == 3 || myPrivilege == 8;
				for (int j = 0; j < totalItemResults; j++) {
					final int yPos = j * 14 - itemResultScrollPos + 8;
					if (yPos >= -14 && yPos < 132) {
						int textX = 80 + xPosOffset;
						int textY = yPos + yPosOffset;
						newRegularFont.drawInterfaceText(itemResultNames[j], textX, textY, rectWidth, rectHeight, 0, -1, 255, 0, 1, 0);
						if (canSpawn) {
							newRegularFont.drawInterfaceText(Integer.toString(itemResultIDs[j]), textX, textY, rectWidth, rectHeight, 0, -1, 255, 2, 1, 0);
						}

						int rectStart = textX - 5;
						int mouseTestY = textY + (isFixed && !GraphicsDisplay.enabled ? 338 : 0);
						if (x >= rectStart && x <= rectStart + rectWidth && y >= mouseTestY && y <= mouseTestY + rectHeight) {
							Raster.fillRect(rectStart, textY, rectWidth + 10, rectHeight, 0x807660, 60, false);
							hoverIndex = j;
						}
					}
				}

				// item holder
				itemSearchItemBg.drawSprite(22, 20 + yPosOffset);

				// searched image sprite
				Sprite itemImg = ItemDef.getSprite(itemResultIDs[hoverIndex], 1, 1, 3153952, false, true);
				if (itemImg != null) {
					itemImg.drawSprite(25, 21 + yPosOffset);
				}

				if (totalItemResults > 8) {
					drawScrollbar(114, itemResultScrollPos, 7 + yPosOffset, 496 + xPosOffset, totalItemResults * 14);
					itemResultsMaxScroll = (totalItemResults - 8) * 14;
				} else {
					itemResultsMaxScroll = 0;
				}
			}
			// vertical divider
			Raster.fillRect(74, 8 + yPosOffset, 2, 113, 0x807660, false); // vertical
			// divider
			Raster.defaultDrawingAreaSize();
			Raster.fillRect(7, 121 + yPosOffset, 490, 15, 0x807660, 120, false);// box
			Raster.drawHorizontalLine(7, 121 + yPosOffset, 506, 0x807660);// line
			newRegularFont.drawBasicString(amountOrNameInput + "*", 28 + xPosOffset, 133 + yPosOffset, 0xffffff, 0);

			int offsetY2 = isFixed ? 345 : yPosOffset + 9;
			if (super.mouseX >= 496 && super.mouseX <= 496 + 19 && super.mouseY >= offsetY2 + 112
					&& super.mouseY <= offsetY2 + 16 + 112) { // TODO: add
				// proper y
				// offsets for
				// hover
				closeIconHover.drawSprite(497, 122 + yPosOffset); // hover
				mouseHoverEnum = MouseHover.CLOSE_ITEM_SEARCH;
			} else {
				closeIcon.drawSprite(497, 122 + yPosOffset); // close
				// button
			}

			// search icon
			magnifyingGlass.drawSprite(10, 122 + yPosOffset);

			// Sprite.drawPixelsWithOpacity(0x000000, 120, 505, 1, 100, 5);
		} else if (aString844 != null) {
			newBoldFont.drawCenteredString(aString844, 259 + xPosOffset, 60 + yPosOffset, 0, -1);
			newBoldFont.drawCenteredString("Click to continue", 259 + xPosOffset, 80 + yPosOffset, 128, -1);
		} else if (backDialogID != -1) {
			drawInterface(RSInterface.interfaceCache[backDialogID], 20 + xPosOffset, 20 + yPosOffset, 0);
		} else if (dialogID != -1) {
			drawInterface(RSInterface.interfaceCache[dialogID], 20 + xPosOffset, 20 + yPosOffset, 0);
		} else {
			int messageCount = 0;

			Raster.setDrawingArea(8 + xPosOffset, 7 + yPosOffset, 497 + xPosOffset, 121 + yPosOffset);
			for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
				ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
					int chatType = message.type;
					int positionY = (111 - messageCount * 14) + anInt1089 + 6;
					String name = message.name;
					String chatMessage = message.message;

					boolean ignored = isInIgnoreList(name);
					boolean isFriend = isFriendOrSelf(name);

					if (chatType == ChatMessage.GAME_MESSAGE || chatType == ChatMessage.GAME_MESSAGE_SPAM) {
						if ((chatTypeView == 5 || chatTypeView == 0) && (chatType == ChatMessage.GAME_MESSAGE || !filterGameSpam && chatType == ChatMessage.GAME_MESSAGE_SPAM)) {
							if (positionY > 0 && positionY < 210)
								newRegularFont.drawBasicString(chatMessage, 11 + xPosOffset, positionY + yPosOffset, 0, -1);
							messageCount++;

						}
					} else if (chatType == ChatMessage.CLAN_MESSAGE) {
						if ((chatTypeView == 11 || chatTypeView == 0)
								&& (clanChatMode == 0 || (clanChatMode == 1 && isFriend)) && !ignored) {
							if (positionY > 0 && positionY < 210)
								newRegularFont.drawBasicString(chatMessage, 11 + xPosOffset, positionY + yPosOffset,
										0x7e3200, -1);
							messageCount++;
						}
					} else if (chatType == ChatMessage.YELL_MESSAGE) {
						if ((chatTypeView == 12 || chatTypeView == 0) && (yellMode == 0 || (yellMode == 1 && isFriend))
								&& !ignored) {
							if (positionY > 0 && positionY < 210)
								newRegularFont.drawBasicString(chatMessage, 11 + xPosOffset, positionY + yPosOffset,
										0x7e3200, -1);
							messageCount++;

						}
					} else if (chatType == ChatMessage.ADVERT_MESSAGE) {
						if (showAutoChat && (chatTypeView == 1 || chatTypeView == 0)) {
							if (positionY > 0 && positionY < 210)
								newRegularFont.drawBasicString(chatMessage, 11 + xPosOffset, positionY + yPosOffset,
										0, -1);
							messageCount++;
						}
					} else if ((chatType == ChatMessage.PUBLIC_MESSAGE_STAFF || chatType == ChatMessage.PUBLIC_MESSAGE_PLAYER)
							&& (chatType == ChatMessage.PUBLIC_MESSAGE_STAFF || publicChatMode == 0 || publicChatMode == 1 && isFriend) && !ignored) {
						if (chatTypeView == 1 || chatTypeView == 0) {
							if (positionY > 0 && positionY < 210) {
								int realY = positionY + yPosOffset;
								int xPos = 12 + xPosOffset;

								newRegularFont.drawBasicString(name, xPos, realY, 0, -1);
								xPos += newRegularFont.getTextWidth(name);
								newRegularFont.drawBasicString(":", xPos, realY, 0, -1);
								xPos += newRegularFont.getTextWidth(":") + 3;
								newRegularFont.drawBasicString(RSFont.escape(chatMessage), xPos, realY, 255, -1);
							}
							messageCount++;
						}
					} else if ((chatType == ChatMessage.PRIVATE_MESSAGE_RECEIVED_PLAYER || chatType == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF) && (splitPrivateChat == 0 || chatTypeView == 2)
							&& (chatType == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF || privateChatMode == 0 || privateChatMode == 1 && isFriend)
							&& !ignored) {
						if (chatTypeView == 2 || chatTypeView == 0) {
							if (positionY > 0 && positionY < 210) {
								int xPos = 11;
								newRegularFont.drawBasicString("From", xPos + xPosOffset, positionY + yPosOffset, 0,
										-1);
								xPos += newRegularFont.getTextWidth("From ");

								newRegularFont.drawBasicString(name + ":", xPos + xPosOffset, positionY + yPosOffset, 0,
										-1);

								xPos += newRegularFont.getTextWidth(name) + 8;
								newRegularFont.drawBasicString(chatMessage, xPos + xPosOffset,
										positionY + yPosOffset, 0x800000, -1);

							}
							messageCount++;
						}
					} else if (chatType == ChatMessage.TRADE_REQUEST && (tradeMode == 0 || tradeMode == 1 && isFriend) && !ignored) {
						if (chatTypeView == 3 || chatTypeView == 0) {
							if (positionY > 0 && positionY < 210)
								newRegularFont.drawBasicString(name + " " + chatMessage, 11 + xPosOffset,
										positionY + yPosOffset, 0x800080, -1);
							messageCount++;
						}
					} else if (chatType == ChatMessage.PRIVATE_MESSAGE_RECEIVED && splitPrivateChat == 0 && privateChatMode < 2) {
						if (chatTypeView == 2 || chatTypeView == 0) {
							if (positionY > 0 && positionY < 210)
								newRegularFont.drawBasicString(chatMessage, 11 + xPosOffset, positionY + yPosOffset,
										0x800000, -1);
							messageCount++;
						}
					} else if (chatType == ChatMessage.PRIVATE_MESSAGE_SENT && (splitPrivateChat == 0 || chatTypeView == 2) && privateChatMode < 2) {
						if (chatTypeView == 2 || chatTypeView == 0) {
							if (positionY > 0 && positionY < 210) {
								newRegularFont.drawBasicString("To " + name + ":", 11 + xPosOffset,
										positionY + yPosOffset, 0, -1);
								newRegularFont.drawBasicString(chatMessage,
										15 + newRegularFont.getTextWidth("To :" + name) + xPosOffset,
										positionY + yPosOffset, 0x800000, -1);
							}
							messageCount++;
						}
					} else if ((chatType == ChatMessage.GAMBLE_REQUEST || chatType == ChatMessage.CHALLENGE_REQUEST || chatType == ChatMessage.DUNG_PARTY_REQUEST) && (tradeMode == 0 || tradeMode == 1 && isFriend) && !ignored) {
						if (chatTypeView == 3 || chatTypeView == 0) {
							if (positionY > 0 && positionY < 210)
								newRegularFont.drawBasicString(name + " " + chatMessage, 11 + xPosOffset,
										positionY + yPosOffset, 0x7e3200, -1);
							messageCount++;
						}
					}
				}
			Raster.defaultDrawingAreaSize();
			anInt1211 = messageCount * 14;
			if (anInt1211 < 111)
				anInt1211 = 111;
			drawScrollbar(114, anInt1211 - anInt1089 - 113, 7 + yPosOffset, 496 + xPosOffset, anInt1211);
			String s;
			if (myPlayer != null && myPlayer.name != null)
				s = myPlayer.name;
			else
				s = TextClass.fixName(capitalize(myUsername));

			if (myPlayer != null && myPlayer.title != null && !myPlayer.title.isEmpty()) {
				s = "<col=" + titleColor(myPlayer.titleColor) + ">" + myPlayer.title + "</col> " + s;
			}

			int length = newRegularFont.getTextWidth(s + ":");
			newRegularFont.drawBasicString(s + ":", 11 + xPosOffset, 133 + yPosOffset, 0, -1); // name
			// under
			// chat
			String parsedInput = RSFont.escape(inputString);
			for (; newRegularFont.getTextWidth(parsedInput) > 415; parsedInput = parsedInput.substring(1)) {
				/* Empty */
			}

			newRegularFont.drawBasicString(parsedInput + "*",
					12 + length /* textDrawingArea.getTextWidth(s + ": ") */
							+ xPosOffset,
					133 + yPosOffset, 255, -1);
			Raster.drawHorizontalLine(7, 121 + yPosOffset, 506, 0x807660);
		}

		if (!hdEnabled) {
			if (MenuManager.menuOpen && menuScreenArea == 2) {
				drawMenu();// drawMenu(0, 338);
			}

			if (isFixed) {
				chatIP.drawGraphics(super.graphics, 0, 338);
			}

			gameScreenIP.initDrawingArea();
			Rasterizer.lineOffsets = anIntArray1182;
		}
	}

	/*
	 * @Override public void init() { try { nodeID = 10; portOff = 0;
	 * setHighMem(); isMembers = true; signlink.storeid = 32;
	 * signlink.startpriv(InetAddress.getLocalHost()); initClientFrame(503,
	 * 765); instance = this; } catch (Exception exception) { return; } }
	 */
	@Override
	public void init() {
		try {
			System.out.println(Config.SERVER_NAME + " is loading..");
			nodeID = 10;// friends list order
			setHighMem();
			isMembers = true;
			SignLink.startpriv();
			instance = this;
			initClientFrame(507, 765);// this it? 510 770 //503 changed to 507
			usingWebclient = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void startRunnable(Runnable runnable, int i, String name) {
		if (i > 10) {
			i = 10;
		}

		super.startRunnable(runnable, i, name);
	}

	/*
	 * @Override public void startRunnable(Runnable runnable, int i) { if (i >
	 * 10) i = 10; if (signlink.mainapp != null) {
	 * signlink.startthread(runnable, i); } else { super.startRunnable(runnable,
	 * i); } }
	 */

	public Socket openSocket(String server, int port) throws IOException {
		return new Socket(InetAddress.getByName(server), port);
	}

	private boolean processMenuClick() {
		if (activeInterfaceType != 0)
			return false;
		int j = super.clickMode3;
		if (targetInterfaceId != -1 && super.saveClickX >= 516 // TODO: make
				// resizeable
				// coordinate
				// offsets
				&& super.saveClickY >= 160 && super.saveClickX <= 765 && super.saveClickY <= 205)
			j = 0;
		if (MenuManager.menuOpen) {
			if (j != 1) {
				int k = super.mouseX;
				int j1 = super.mouseY;
				if (menuScreenArea == 0) {
					k -= isFixed() ? 4 : 0;
					j1 -= isFixed() ? 4 : 0;
				} else if (menuScreenArea == 1) {
					k -= 519;
					j1 -= 168;
				} else if (menuScreenArea == 2) {
					k -= 17;
					j1 -= 338;
				} else if (menuScreenArea == 3) {
					k -= 519;
					j1 -= 0;
				}
				if (k < menuOffsetX - 10 || k > menuOffsetX + menuWidth + 10 || j1 < menuOffsetY - 10
						|| j1 > menuOffsetY + menuHeight + 10) {
					MenuManager.menuOpen = false;
					if (menuScreenArea == 1)
						needDrawTabArea = true;
					if (menuScreenArea == 2)
						inputTaken = true;
				}
			}
			if (j == 1) {
				int l = menuOffsetX;
				int k1 = menuOffsetY;
				int i2 = menuWidth;
				int k2 = super.saveClickX;
				int l2 = super.saveClickY;
				if (menuScreenArea == 0) {
					k2 -= isFixed() ? 4 : 0;
					l2 -= isFixed() ? 4 : 0;
				} else if (menuScreenArea == 1) {
					k2 -= 519;
					l2 -= 168;
				} else if (menuScreenArea == 2) {
					k2 -= 17;
					l2 -= 338;
				} else if (menuScreenArea == 3) {
					k2 -= 519;
					l2 -= 0;
				}
				int i3 = -1;
				for (int j3 = 0; j3 < MenuManager.menuActionRow; j3++) {
					int k3 = k1 + 31 + (MenuManager.menuActionRow - 1 - j3) * 15;
					if (k2 > l && k2 < l + i2 && l2 > k3 - 13 && l2 < k3 + 3)
						i3 = j3;
				}
				if (i3 >= 0) {
					doAction(MenuManager.menuActionID[i3], MenuManager.menuActionCmd1[i3], MenuManager.menuActionCmd2[i3], MenuManager.menuActionCmd3[i3], MenuManager.menuActionName[i3]);
				}
				MenuManager.menuOpen = false;
				if (menuScreenArea == 1)
					needDrawTabArea = true;
				else if (menuScreenArea == 2) {
					inputTaken = true;
				}
			}
			return true;
		} else {
			int index = MenuManager.getLeftClick();
			if (j == 1 && index >= 0) {
				int i1 = MenuManager.menuActionID[index];
				if (i1 == 632 || i1 == 78 || i1 == 867 || i1 == 431 || i1 == 53 || i1 == 74 || i1 == 454 || i1 == 539
						|| i1 == 493 || i1 == 847 || i1 == 447 || i1 == 1125) {
					int l1 = MenuManager.menuActionCmd2[index];
					int j2 = MenuManager.menuActionCmd3[index];
					RSInterface rsi = RSInterface.interfaceCache[j2];
					if (rsi.aBoolean259 || rsi.deletesTargetSlot) {
						if (activeInterfaceType == 2 && !aBoolean1242 && MenuManager.menuActionRow > 0 && !((anInt1253 == 1 || MenuManager.optionLowPriority(MenuManager.menuActionRow - 1)) && MenuManager.menuActionRow > 2)) {
							MiniMenuEntry.executeEntry();
						}

						aBoolean1242 = false;
						anInt989 = 0;
						anInt1084 = j2;
						anInt1085 = l1;
						activeInterfaceType = 2;
						anInt1087 = super.saveClickX;
						anInt1088 = super.saveClickY;
						if (index >= 0) {
							MiniMenuEntry.putEntry(index);
						}

						if (RSInterface.interfaceCache[j2].parentID == openModalInterfaceId)
							activeInterfaceType = 1;
						if (RSInterface.interfaceCache[j2].parentID == backDialogID)
							activeInterfaceType = 3;
						return true;
					}
				}
			}
			if (j == 1 && ((anInt1253 == 1 || MenuManager.optionLowPriority(MenuManager.menuActionRow - 1)) && MenuManager.menuActionRow > 2))
				j = 2;
			if (j == 1 && MenuManager.menuActionRow > 0 && index >= 0) {
				doAction(MenuManager.menuActionID[index], MenuManager.menuActionCmd1[index], MenuManager.menuActionCmd2[index], MenuManager.menuActionCmd3[index], MenuManager.menuActionName[index]);
			}
			if (j == 2 && MenuManager.menuActionRow > 0)
				determineMenuSize();
			return false;
		}
	}

	public static String getFileNameWithoutExtension(String fileName) {
		File tmpFile = new File(fileName);
		String name = tmpFile.getName();
		int whereDot = name.lastIndexOf('.');
		if (whereDot > 0 && whereDot <= name.length() - 2) {
			return name.substring(0, whereDot);
		}
		return "";
	}

	public String indexLocation(int cacheIndex, int index) {
		return SignLink.cacheDir + "index" + cacheIndex + "/" + (index != -1 ? index + ".gz" : "");
	}

	public void repackCacheIndex(int cacheIndex) {
		System.out.println("Started repacking index " + cacheIndex + ".");
		File[] file = new File(indexLocation(cacheIndex, -1)).listFiles();
		int indexLength = file.length;
		try {
			for (int index = 0; index < indexLength; index++) {
				int fileIndex = Integer.parseInt(getFileNameWithoutExtension(file[index].toString()));
				// boolean foundObj = false;
				// for (int o = 0; o < corpObjs.length; o++) {
				// if (corpObjs[o] == fileIndex) {
				// foundObj = true;
				// }
				// }
				// if (!foundObj) {
				// continue;
				// }
				byte[] data = fileToByteArray(cacheIndex, fileIndex);
				if (data != null && data.length > 0) {
					fileStores[cacheIndex].writeFile(data.length, data, fileIndex);
					System.out.println("Repacked " + fileIndex + ".");
				} else {
					System.out.println("Unable to locate index " + fileIndex + ".");
				}
			}
		} catch (Exception e) {
			System.out.println("Error packing cache index " + cacheIndex + ".");
		}
		System.out.println("Finished repacking " + cacheIndex + ".");
	}

	public byte[] fileToByteArray(int cacheIndex, int index) {
		try {
			String location = indexLocation(cacheIndex, index);
			if (location == null || location.length() <= 0) {
				return null;
			}
			File file = new File(location);
			byte[] fileData = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(fileData);
			fis.close();
			return fileData;
		} catch (Exception e) {
			return null;
		}
	}

	public static byte[] fileToByteArray(File file) {
		try {
			if (file == null || !file.exists()) {
				return null;
			}
			byte[] fileData = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(fileData);
			fis.close();
			return fileData;
		} catch (Exception e) {
			return null;
		}
	}

	public static void repackOsrsAnimations() {
		System.out.println("Started repacking osrs animations.");
		File[] file = new File(SignLink.cacheDir + "index2osrs").listFiles();
		int indexLength = file.length;
		try {
			for (int index = 0; index < indexLength; index++) {
				int fileIndex = Integer.parseInt(getFileNameWithoutExtension(file[index].toString()));
				fileIndex += Animation.OSRS_ANIM_FILE_START;
				byte[] data = fileToByteArray(file[index]);
				if (data != null && data.length > 0) {
					instance.fileStores[2].writeFile(data.length, data, fileIndex);
					System.out.println("Repacked " + fileIndex + ".");
				} else {
					System.out.println("Unable to locate index " + fileIndex + ".");
				}
			}
		} catch (Exception e) {
			System.out.println("Error packing cache index " + 2 + ".");
		}
		System.out.println("Finished repacking osrs animations.");
	}

	public void preloadModels() {
		System.out.println("Preloading models from " + SignLink.cacheDir + "Raw/");
		int amount = 0;
		File file = new File(SignLink.cacheDir + "Raw/");
		File[] fileArray = file.listFiles();
		if (fileArray != null) {
			for (File aFileArray : fileArray) {
				String s = aFileArray.getName();
				if (s.equals("gzip"))
					continue;
				byte[] buffer = ReadFile(SignLink.cacheDir + "Raw/" + s);
				Model.method460(buffer, Integer.parseInt(getFileNameWithoutExtension(s)), false);
				amount++;
			}
		}
		System.out.println("Preloaded " + amount + " raw models.");
	}

	// public void preloadModels() {
	// File file = new
	// File("S:/Users/Lesik/Desktop/667ObjRaw/");//File(signlink.findcachedir()+"Raw/");
	// File[] fileArray = file.listFiles();
	// for(int y = 0; y < fileArray.length; y++) {
	// String s = fileArray[y].getName();
	// byte[] buffer = ReadFile(/*signlink.findcachedir()+"Raw/"*/
	// "S:/Users/Lesik/Desktop/667ObjRaw/" +s);
	// com.soulplayps.client.Model.method460(buffer,Integer.parseInt(getFileNameWithoutExtension(s)));
	// }
	// }

	public static final byte[] ReadFile(String s) {
		try {
			byte abyte0[];
			File file = new File(s);
			int i = (int) file.length();
			abyte0 = new byte[i];
			DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new FileInputStream(s)));
			datainputstream.readFully(abyte0, 0, i);
			datainputstream.close();
			return abyte0;
		} catch (Exception e) {
			System.out.println((new StringBuilder()).append("Read Error: ").append(s).toString());
			return null;
		}
	}

	public void addModels() {
		for (int ModelIndex = 0; ModelIndex < 50000; ModelIndex++) {
			byte[] abyte0 = getModel(ModelIndex);
			if (abyte0 != null && abyte0.length > 0) {
				fileStores[1].writeFile(abyte0.length, abyte0, ModelIndex);
			}
		}
	}

	public byte[] getModel(int Index) {
		try {
			File Model = new File(SignLink.cacheDir + "./pModels/" + Index + ".gz");
			byte[] aByte = new byte[(int) Model.length()];
			FileInputStream fis = new FileInputStream(Model);
			fis.read(aByte);
			System.out.println("" + Index + " aByte = [" + aByte + "]!");
			fis.close();
			return aByte;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void addMaps() {
		for (int MapIndex = 0; MapIndex < 3536; MapIndex++) {
			byte[] abyte0 = getMaps(MapIndex);
			if (abyte0 != null && abyte0.length > 0) {
				fileStores[4].writeFile(abyte0.length, abyte0, MapIndex);
				System.out.println("Maps Added");
			}
		}
	}

	public byte[] getMaps(int Index) {
		try {
			File Map = new File(SignLink.cacheDir + "./pMaps/" + Index + ".gz");
			byte[] aByte = new byte[(int) Map.length()];
			FileInputStream fis = new FileInputStream(Map);
			fis.read(aByte);
			pushGameMessage("aByte = [" + aByte + "]!");
			fis.close();
			return aByte;
		} catch (Exception e) {
			return null;
		}
	}

	/*
	 * public int positions[] = new int[2000]; public int landScapes[] = new
	 * int[2000]; public int objects[] = new int[2000]; public final void
	 * method22() { //previous method22 try { anInt985 = -1;
	 * aClass19_1056.removeAll(); aClass19_1013.removeAll();
	 * com.soulplayps.client.Texture.method366(); unlinkMRUNodes();
	 * worldController.initToNull(); System.gc(); for (int i = 0; i < 4; i++)
	 * aClass11Array1230[i].method210(); for (int l = 0; l < 4; l++) { for (int
	 * k1 = 0; k1 < 104; k1++) { for (int j2 = 0; j2 < 104; j2++)
	 * byteGroundArray[l][k1][j2] = 0; } } com.soulplayps.client.ObjectManager
	 * objectManager = new com.soulplayps.client.ObjectManager(byteGroundArray,
	 * intGroundArray); int k2 = aByteArrayArray1183.length; int k18 = 62; for
	 * (int A = 0; A < k2; A++) for (int B = 0; B < 2000; B++) if
	 * (anIntArray1234[A] == positions[B]) { anIntArray1235[A] = landScapes[B];
	 * anIntArray1236[A] = objects[B]; } stream.createFrame(0); if
	 * (!aBoolean1159) { for (int i3 = 0; i3 < k2; i3++) { int i4 =
	 * (anIntArray1234[i3] >> 8) * 64 - baseX; int k5 = (anIntArray1234[i3] &
	 * 0xff) * 64 - baseY; byte abyte0[] = aByteArrayArray1183[i3]; if
	 * (com.soulplayps.client.FileOperations
	 * .FileExists("./.ss_474/Null.map/mapsFloor/" + anIntArray1235[i3] +
	 * ".dat")) abyte0 = com.soulplayps.client.FileOperations
	 * .ReadFile("./.ss_474/Null.map/mapsFloor/" + anIntArray1235[i3] + ".dat");
	 * if (abyte0 != null) // objectManager.method180(abyte0, k5, i4, (anInt1069
	 * - // 6) * 8, (anInt1070 - 6) * 8, (byte)4, // aClass11Array1230);
	 * objectManager.method180(abyte0, k5, i4, (anInt1069 - 6) * 8, (anInt1070 -
	 * 6) * 8, aClass11Array1230); } for (int j4 = 0; j4 < k2; j4++) { int l5 =
	 * (anIntArray1234[j4] >> 8) * k18 - baseX; int k7 = (anIntArray1234[j4] &
	 * 0xff) * k18 - baseY; byte abyte2[] = aByteArrayArray1183[j4]; if (abyte2
	 * == null && anInt1070 < 800) //
	 * com.soulplayps.client.ObjectManager.method174(k7, 64, 0, 64, l5);
	 * objectManager.method174(k7, 64, 64, l5); } anInt1097++; if (anInt1097 >
	 * 160) { anInt1097 = 0; stream.createFrame(238);
	 * stream.writeByte(96); } stream.createFrame(0); for (int i6 = 0;
	 * i6 < k2; i6++) { byte abyte1[] = aByteArrayArray1247[i6]; if
	 * (com.soulplayps.client.FileOperations
	 * .FileExists("./.ss_474/Null.map/spawnObject/" + anIntArray1236[i6] +
	 * ".dat")) abyte1 = com.soulplayps.client.FileOperations
	 * .ReadFile("./.ss_474/Null.map/spawnObject/" + anIntArray1236[i6] +
	 * ".dat"); if (abyte1 != null) { int l8 = (anIntArray1234[i6] >> 8) * 64 -
	 * baseX; int k9 = (anIntArray1234[i6] & 0xff) * 64 - baseY; //
	 * com.soulplayps.client.ObjectManager.method190(l8, aClass11Array1230, k9,
	 * 7, // aClass25_946, abyte1); objectManager.method190(l8,
	 * aClass11Array1230, k9, worldController, abyte1); } } } if (aBoolean1159)
	 * { for (int j3 = 0; j3 < 4; j3++) { for (int k4 = 0; k4 < 13; k4++) { for
	 * (int j6 = 0; j6 < 13; j6++) { int l7 =
	 * anIntArrayArrayArray1129[j3][k4][j6]; if (l7 != -1) { int i9 = l7 >> 24 &
	 * 3; int l9 = l7 >> 1 & 3; int j10 = l7 >> 14 & 0x3ff; int l10 = l7 >> 3 &
	 * 0x7ff; int j11 = (j10 / 8 << 8) + l10 / 8; for (int l11 = 0; l11 <
	 * anIntArray1234.length; l11++) { if (anIntArray1234[l11] != j11 ||
	 * aByteArrayArray1183[l11] == null) continue; //
	 * com.soulplayps.client.ObjectManager.method179(i9, l9, //
	 * aClass11Array1230, 9, k4 * 8, (j10 & 7) * // 8, aByteArrayArray1183[l11],
	 * (l10 & 7) * // 8, j3, j6 * 8); objectManager.method179(i9, l9,
	 * aClass11Array1230, k4 * 8, (j10 & 7) * 8, aByteArrayArray1183[l11], (l10
	 * & 7) * 8, j3, j6 * 8); break; } } } } } for (int l4 = 0; l4 < 13; l4++) {
	 * for (int k6 = 0; k6 < 13; k6++) { int i8 =
	 * anIntArrayArrayArray1129[0][l4][k6]; if (i8 == -1) //
	 * com.soulplayps.client.ObjectManager.method174(k6 * 8, 8, 0, 8, l4 * 8);
	 * objectManager.method174(k6 * 8, 8, 8, l4 * 8); } } stream.createFrame(0);
	 * for (int l6 = 0; l6 < 4; l6++) { for (int j8 = 0; j8 < 13; j8++) { for
	 * (int j9 = 0; j9 < 13; j9++) { int i10 =
	 * anIntArrayArrayArray1129[l6][j8][j9]; if (i10 != -1) { int k10 = i10 >>
	 * 24 & 3; int i11 = i10 >> 1 & 3; int k11 = i10 >> 14 & 0x3ff; int i12 =
	 * i10 >> 3 & 0x7ff; int j12 = (k11 / 8 << 8) + i12 / 8; for (int k12 = 0;
	 * k12 < anIntArray1234.length; k12++) { if (anIntArray1234[k12] != j12 ||
	 * aByteArrayArray1247[k12] == null) continue; if
	 * (com.soulplayps.client.FileOperations.FileExists("./Cache/FloorMaps/" +
	 * anIntArray1235[k12] + ".dat"))
	 * com.soulplayps.client.FileOperations.ReadFile(signlink.findcachedir() +
	 * "FloorMaps/" + anIntArray1235[k12] + ".dat"); //
	 * com.soulplayps.client.ObjectManager.method183(aClass11Array1230, //
	 * aClass25_946, k10, j8 * 8, (i12 & 7) * 8, // true, l6, abyte0, (k11 & 7)
	 * * 8, i11, j9 // * 8); objectManager.method183(aClass11Array1230,
	 * worldController, k10, j8 * 8, (i12 & 7) * 8, l6,
	 * aByteArrayArray1247[k12], (k11 & 7) * 8, i11, j9 * 8); break; } } } } } }
	 * stream.createFrame(0); objectManager.method171(aClass11Array1230,
	 * worldController); aRSImageProducer_1165.initDrawingArea();
	 * stream.createFrame(0); int k3 =
	 * com.soulplayps.client.ObjectManager.anInt145; if (k3 > plane) k3 = plane;
	 * if (k3 < plane - 1) k3 = plane - 1; if (lowMem)
	 * worldController.method275(com.soulplayps.client.ObjectManager.anInt145);
	 * else worldController.method275(0); for (int i5 = 0; i5 < 104; i5++) { for
	 * (int i7 = 0; i7 < 104; i7++) spawnGroundItem(i5, i7); } anInt1051++; if
	 * (anInt1051 > 98) { anInt1051 = 0; stream.createFrame(150); } method63();
	 * } catch (Exception e) { e.printStackTrace(); }
	 * com.soulplayps.client.ObjectDef.mruNodes1.clear(); if (super.gameFrame !=
	 * null) { stream.createFrame(210); stream.writeDWord(0x3f008edd); }
	 * System.gc(); com.soulplayps.client.Texture.method367();
	 * onDemandFetcher.method566(); int k = (anInt1069 - 6) / 8 - 1; int j1 =
	 * (anInt1069 + 6) / 8 + 1; int i2 = (anInt1070 - 6) / 8 - 1; int l2 =
	 * (anInt1070 + 6) / 8 + 1; if (aBoolean1141) { k = 49; j1 = 50; i2 = 49; l2
	 * = 50; } for (int l3 = k; l3 <= j1; l3++) { for (int j5 = i2; j5 <= l2;
	 * j5++) if (l3 == k || l3 == j1 || j5 == i2 || j5 == l2) { int j7 =
	 * onDemandFetcher.method562(0, j5, l3); if (j7 != -1)
	 * onDemandFetcher.method560(j7, 3); int k8 = onDemandFetcher.method562(1,
	 * j5, l3); if (k8 != -1) onDemandFetcher.method560(k8, 3); } } }
	 */

	private void resetZoneHeight(int level, int x, int z) {
		for (int xOff = 0; xOff < 8; xOff++) {
			for (int zOff = 0; zOff < 8; zOff++)
				intGroundArray[level][x + xOff][z + zOff] = 0;
		}
		if (x > 0) {
			for (int zOff = 1; zOff < 8; zOff++)
				intGroundArray[level][x][zOff + z] = (intGroundArray[level][x - 1][zOff + z]);
		}
		if (z > 0) {
			for (int xOff = 1; xOff < 8; xOff++)
				intGroundArray[level][x + xOff][z] = (intGroundArray[level][xOff + x][z - 1]);
		}
		if (x > 0 && intGroundArray[level][x - 1][z] != 0)
			intGroundArray[level][x][z] = intGroundArray[level][x - 1][z];
		else if (z > 0 && intGroundArray[level][x][z - 1] != 0)
			intGroundArray[level][x][z] = intGroundArray[level][x][z - 1];
		else if (x > 0 && z > 0 && intGroundArray[level][x - 1][z - 1] != 0)
			intGroundArray[level][x][z] = intGroundArray[level][x - 1][z - 1];
	}

	private void method22() {
		try {
			if (GraphicsDisplay.enabled) {
				if (GraphicsDisplay.vboRendering) {
					Cache.clearFloorVBOs();
				}

				Cache.clearVBOModels = true;
			}

			boolean oldSetting = ObjectManager.drawGroundDecor;
			if (inOsrsRegion) {
				ObjectManager.drawGroundDecor = true;
			}

			anInt985 = -1;
			aClass19_1056.removeAll();
			aClass19_1013.removeAll();
			Rasterizer.method366();
			unlinkMRUNodes();
			worldController.initToNull();
			worldController.setViewDistance(40);
			// System.gc();
			for (int i = 0; i < 4; i++)
				aClass11Array1230[i].method210();
			for (int l = 0; l < 4; l++) {
				for (int k1 = 0; k1 < 104; k1++) {
					for (int j2 = 0; j2 < 104; j2++)
						byteGroundArray[l][k1][j2] = 0;

				}
			}
			ObjectManager objectManager = new ObjectManager(byteGroundArray, intGroundArray);
			int k2 = localRegionMapData.length;
			if (loggedIn)
				stream.sendPacket(0);
			if (!constructedViewport) {
				try {
					for (int i3 = 0; i3 < k2; i3++) {
						int i4 = (localRegionIds[i3] >> 8) * 64 - regionBaseX;
						int k5 = (localRegionIds[i3] & 0xff) * 64 - regionBaseY;
						byte abyte0[] = localRegionMapData[i3];
						if (abyte0 != null) {
							int x = anInt1069;
							int z = anInt1070;
							if (x >= 1000) {
								x -= 800;
							}

							objectManager.method180(abyte0, k5, i4, (x - 6) * 8, (z - 6) * 8,
									aClass11Array1230, inOsrsRegion);
						}
					}
					for (int j4 = 0; j4 < k2; j4++) {
						int l5 = (localRegionIds[j4] >> 8) * 64 - regionBaseX;
						int k7 = (localRegionIds[j4] & 0xff) * 64 - regionBaseY;
						byte abyte2[] = localRegionMapData[j4];
						if (abyte2 == null && anInt1070 < 800)
							objectManager.method174(k7, 64, 64, l5);
					}
					if (loggedIn)
						stream.sendPacket(0);
					for (int i6 = 0; i6 < k2; i6++) {
						byte abyte1[] = localRegionLandscapeData[i6];
						if (abyte1 != null) {
							int l8 = (localRegionIds[i6] >> 8) * 64 - regionBaseX;
							int k9 = (localRegionIds[i6] & 0xff) * 64 - regionBaseY;
							objectManager.method190(l8, aClass11Array1230, k9, worldController, abyte1,
									localRegionIds[i6]);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (constructedViewport) {
				for (int z = 0; z < 4; z++) {
					for (int x = 0; x < 13; x++) {
						for (int y = 0; y < 13; y++) {
							boolean loaded = false;
							int locationHash = localRegions[z][x][y];
							if (locationHash != -1) {
								int i9 = locationHash >> 26 & 3;
								int l9 = locationHash >> 1 & 3;
								int j10 = locationHash >> 15 & 0x7ff;
								int l10 = locationHash >> 3 & 0x7ff;
								int j11 = (j10 / 8 << 8) + l10 / 8;
								for (int l11 = 0; l11 < localRegionIds.length; l11++) {
									if (localRegionIds[l11] != j11 || localRegionMapData[l11] == null)
										continue;
									objectManager.method179(i9, l9, aClass11Array1230, x * 8, (j10 & 7) * 8,
											localRegionMapData[l11], (l10 & 7) * 8, z, y * 8);
									loaded = true;
									break;
								}

							}
							if (!loaded) {
								resetZoneHeight(z, x * 8, y * 8);
							}
						}
					}
				}
				for (int x = 0; x < 13; x++) {
					for (int y = 0; y < 13; y++) {
						int i8 = localRegions[0][x][y];
						if (i8 == -1)
							objectManager.method174(y * 8, 8, 8, x * 8);
					}
				}
				if (loggedIn)
					stream.sendPacket(0);
				for (int z = 0; z < 4; z++) {
					for (int x = 0; x < 13; x++) {
						for (int y = 0; y < 13; y++) {
							int i10 = localRegions[z][x][y];
							if (i10 != -1) {
								int k10 = i10 >> 26 & 3;
								int i11 = i10 >> 1 & 3;
								int k11 = i10 >> 15 & 0x7ff;
								int i12 = i10 >> 3 & 0x7ff;
								int j12 = (k11 / 8 << 8) + i12 / 8;
								for (int k12 = 0; k12 < localRegionIds.length; k12++) {
									if (localRegionIds[k12] != j12 || localRegionLandscapeData[k12] == null)
										continue;

									int objectDef = 474;
									if (localRegionOsrs[k12]) {
										objectDef = 100;
									}

									objectManager.method183(aClass11Array1230, worldController, k10, x * 8,
											(i12 & 7) * 8, z, localRegionLandscapeData[k12], (k11 & 7) * 8, i11, y * 8, objectDef);
									break;
								}
							}
						}
					}
				}
			}
			if (loggedIn)
				stream.sendPacket(0);
			objectManager.method171(aClass11Array1230, worldController);
			if (loggedIn)
				stream.sendPacket(0);
			int k3 = ObjectManager.anInt145;
			if (k3 > plane)
				k3 = plane;
			if (k3 < plane - 1)
				k3 = plane - 1;
			if (lowMem)
				worldController.method275(ObjectManager.anInt145);
			else
				worldController.method275(0);
			for (int i5 = 0; i5 < 104; i5++) {
				for (int i7 = 0; i7 < 104; i7++)
					spawnGroundItem(i5, i7);
			}

			method63();
			ObjectManager.drawGroundDecor = oldSetting;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		ObjectDef.mruNodes1.clear();
		ItemDef.itemSpriteCache.clear();
		if (super.frame != null && loggedIn) {
			stream.sendPacket(210);
			stream.writeDWord(0x3f008edd);
		}
		// System.gc();
		Rasterizer.method367();
		onDemandFetcher.method566();
		int k = (anInt1069 - 6) / 8 - 1;
		int j1 = (anInt1069 + 6) / 8 + 1;
		int i2 = (anInt1070 - 6) / 8 - 1;
		int l2 = (anInt1070 + 6) / 8 + 1;
		if (inTutorialIsland) { // TODO: find out what this is for
			k = 49;
			j1 = 50;
			i2 = 49;
			l2 = 50;
		}
		for (int l3 = k; l3 <= j1; l3++) {
			for (int j5 = i2; j5 <= l2; j5++)
				if (l3 == k || l3 == j1 || j5 == i2 || j5 == l2) {
					int cacheIndex = 3;
					if (l3 >= 100)
						cacheIndex = 6;
					int regionId = (l3 << 8) + j5;
					int j7 = onDemandFetcher.method562(0, regionId);
					if (j7 != -1)
						onDemandFetcher.method560(j7, cacheIndex);
					int k8 = onDemandFetcher.method562(1, regionId);
					if (k8 != -1) {
						onDemandFetcher.method560(k8, cacheIndex);
						// System.out.println("k8 = "+k8);
						// System.out.println("j5:"+j5+" l3:"+l3);
					}
				}

		}

		CheapHax.editOsrsObj();
	}

	public void unlinkMRUNodes() {
		ObjectDef.mruNodes1.clear();
		ObjectDef.mruNodes2.clear();
		ObjectDef.recentUse.clear();
		EntityDef.modelCache.clear();
		EntityDef.recentUse.clear();
		ItemDef.mruNodes2.clear();
		PlayerAppearance.mruNodes.clear();
		SpotAnim.aMRUNodes_415.clear();
	}

	private void method24(final int i) {
		int ai[] = miniMap.myPixels;
		int j = ai.length;
		for (int k = 0; k < j; k++)
			ai[k] = 0;
		for (int l = 1; l < 103; l++) {
			int i1 = 24628 + (103 - l) * 512 * 4;
			for (int k1 = 1; k1 < 103; k1++) {
				if ((byteGroundArray[i][k1][l] & 0x18) == 0)
					worldController.method309(ai, i1, i, k1, l);
				if (i < 3 && (byteGroundArray[i + 1][k1][l] & 8) != 0)
					worldController.method309(ai, i1, i + 1, k1, l);
				i1 += 4;
			}
		}
		int j1 = 0xffffff;
		int l1 = 0xff0000;
		Raster.initDrawingArea(miniMap.myHeight, miniMap.myWidth, ai);
		for (int i2 = 1; i2 < 103; i2++) {
			for (int j2 = 1; j2 < 103; j2++) {
				if ((byteGroundArray[i][j2][i2] & 0x18) == 0)
					method50(i2, j1, j2, l1, i);
				if (i < 3 && (byteGroundArray[i + 1][j2][i2] & 8) != 0)
					method50(i2, j1, j2, l1, i + 1);
			}
		}
		if (!GraphicsDisplay.enabled) {
			gameScreenIP.initDrawingArea();
		}
		refreshGLMinimap = true;
		mapFunctionsCount = 0;
		for (int k2 = 0; k2 < 104; k2++) {
			for (int l2 = 0; l2 < 104; l2++) {
				long i3 = worldController.method303(plane, k2, l2);
				if (i3 != 0L) {
					int j3 = ObjectDef.forID((int) (i3 >>> 32) & 0x7fffffff).anInt746;
					if (j3 >= 0 && j3 < mapFunctions.length) {
						mapFunctionsId[mapFunctionsCount] = j3;
						mapFunctionsX[mapFunctionsCount] = k2;
						mapFunctionsZ[mapFunctionsCount] = l2;
						mapFunctionsCount++;
					}
				}
			}
		}
	}

	public void spawnGroundItem(int i, int j) {
		NodeList class19 = groundArray[plane][i][j];
		if (class19 == null) {
			worldController.method295(plane, i, j);
			return;
		}

		long k = -99999999L;
		Item obj = null;
		for (Item item = (Item) class19.reverseGetFirst(); item != null; item = (Item) class19.reverseGetNext()) {
			ItemDef itemDef = ItemDef.forID(item.ID);
			long l = (long) itemDef.cost;
			if (itemDef.stackable) {
				l *= (long) (item.anInt1559 + 1);
			}

			if (l > k) {
				k = l;
				obj = item;
			}
		}

		if (obj == null) {
			worldController.method295(plane, i, j);
			return;
		}

		class19.insertTail(obj);
		Item obj1 = null;
		Item obj2 = null;
		for (Item class30_sub2_sub4_sub2_1 = (Item) class19.reverseGetFirst(); class30_sub2_sub4_sub2_1 != null; class30_sub2_sub4_sub2_1 = (Item) class19.reverseGetNext()) {
			if (class30_sub2_sub4_sub2_1.ID != obj.ID) {
				if (obj1 == null) {
					obj1 = class30_sub2_sub4_sub2_1;
				}

				if (obj1.ID != class30_sub2_sub4_sub2_1.ID && obj2 == null) {
					obj2 = class30_sub2_sub4_sub2_1;
				}
			}
		}

		int value = (int) (Math.random() * 99999999D);
		long i1 = i | (j << 7) | 0x60000000 | (long) value << 32;
		worldController.method281(i, i1, obj1, method42(plane, j * 128 + 64, i * 128 + 64), obj2, obj, plane, j);
	}

	private int tabClickDelay;

	public void replyToPrivateMessage() {
		if (tabClickDelay > loopCycle) {
			return;
		}

		String name = null;
		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
            int type = message.type;
            if (type == ChatMessage.PRIVATE_MESSAGE_RECEIVED_PLAYER || type == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF) {
                name = message.name;
                break;
            }
        }

		if (name == null) {
			tabClickDelay = loopCycle + 100;
			pushGameMessage("You haven't received any messages to which you can reply.");
			return;
		}

		name = Strings.removeMarkup(name);

		try {
			if (name != null) {
				long namel = TextClass.longForName(name.trim());
				int node = -1;
				if ((Boolean) Settings.getSetting("auto_add_reply")) {
					for (int count = 0; count < friendsCount; count++) {
						if (friendsListAsLongs[count] != namel)
							continue;
						node = count;
						break;
					}
					if (node != -1 && friendsNodeIDs[node] > 0) {
						inputTaken = true;
						setInputDialogState(0);
						messagePromptRaised = true;
						promptInput = "";
						friendsListAction = 3;
						aLong953 = friendsListAsLongs[node];
						aString1121 = "Enter message to send to " + friendsList[node];
					} else {
						long l = TextClass.longForName(name);
						if (!addFriend(l))
							pushGameMessage("That player is currently offline or cannot be added to your friends list.");
						else
							pushGameMessage("Auto added " + name + " to your friends list.");
					}
				} else {
					if (namel > -1) {
						inputTaken = true;
						setInputDialogState(0);
						messagePromptRaised = true;
						promptInput = "";
						friendsListAction = 3;
						aLong953 = namel;
						aString1121 = "Enter message to send to " + name.trim();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void method26(boolean flag) {
		for (int j = 0; j < npcCount; j++) {
			Npc npc = npcArray[npcIndices[j]];
			if (npc == null || !npc.isVisible() || npc.renderPriority() != flag)
				continue;
			int l = npc.x >> 7;
			int i1 = npc.z >> 7;
			if (l < 0 || l >= 104 || i1 < 0 || i1 >= 104)
				continue;
			if (npc.anInt1540 == 1 && (npc.x & 0x7f) == 64 && (npc.z & 0x7f) == 64) {
				if (anIntArrayArray929[l][i1] == anInt1265)
					continue;
				anIntArrayArray929[l][i1] = anInt1265;
			}

			long k = 0x20000000 | (long) npcIndices[j] << 32;
			if (!npc.desc.aBoolean84) {
				k |= ~0x7fffffffffffffffL;
			}

			worldController.method285(plane, npc.anInt1552, method42(plane, npc.z, npc.x), k, npc.z,
					(npc.anInt1540 - 1) * 64 + 60, npc.x, npc, npc.aBoolean1541, 0);
		}
	}

	public void loadError() {
		String s = "ondemand";// was a constant parameter
		System.out.println("wtf: " + s);
		try {
			getAppletContext().showDocument(new URL(getCodeBase(), "loaderror_" + s + ".html"));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		do
			try {
				Thread.sleep(1000L);
			} catch (Exception _ex) {
				_ex.printStackTrace();
			}
		while (true);
	}

	private String getFloorName(int floorId) {
		if (floorId <= 11)
			return "Frozen";
		if (floorId <= 17)
			return "Abandoned";
		if (floorId <= 29)
			return "Furnished";
		if (floorId <= 35)
			return "Abandoned";
		if (floorId <= 47)
			return "Occult";
		return "Warped";
	}

	private NodeList interfaceHighPriorityEvents = new NodeList();
	private NodeList interfaceMediumPriorityEvents = new NodeList();
	private NodeList interfaceLowPriorityEvents = new NodeList();

	private void processInterface(RSInterface parent, int x, int y, int scrollY, int mouseX, int mouseY) {
		if (parent.interfaceType != 0 || (parent.children == null && parent.dynamicComponents == null) || parent.isMouseoverTriggered) {
			return;
		}

		int width = x + parent.width;
		int height = y + parent.height;
		boolean usesDynamic = parent.dynamicComponents != null;
		int componentCount = usesDynamic ? parent.dynamicComponents.length : parent.children.length;
		for (int componentId = 0; componentId < componentCount; componentId++) {
			RSInterface child = usesDynamic ? parent.dynamicComponents[componentId] : RSInterface.interfaceCache[parent.children[componentId]];
			if (child == null || child.hidden) {
				continue;
			}

			int componentX;
			int componentY;
			if (usesDynamic) {
				componentX = child.x;
				componentY = child.y;
			} else {
				componentX = parent.childX[componentId];
				componentY = parent.childY[componentId];
			}

			componentX += x;
			componentY += y - scrollY;
			componentX += child.xOffset;
			componentY += child.yOffset;

			if ((componentX > width || componentY > height || componentY + child.height < y) && child.interfaceType != 2) {
				continue;
			}

			boolean mouseOver = mouseX <= width && mouseY <= height && mouseX >= x && mouseY >= y && mouseX >= componentX && mouseY >= componentY && mouseX < componentX + child.width && mouseY < componentY + child.height;

			if (BitwiseHelper.testBit(child.flags, RSInterface.CANT_CLICK_THROUGH) && mouseOver) {
				for (InterfaceEvent event = (InterfaceEvent) interfaceLowPriorityEvents.reverseGetFirst(); event != null; event = (InterfaceEvent) interfaceLowPriorityEvents.reverseGetNext()) {
					event.unlink();
				}

				MenuManager.clearMiniMenu();
			}

			boolean clicked = false;
			if (super.clickMode3 > 0 && mouseOver) {
				clicked = true;
			}

			boolean buttonHeld = false;
			if (super.clickMode2 == 1 && mouseOver) {
				buttonHeld = true;
			}

			if (mouseOver && notchesPoll != 0 && child.onMouseWheel != null) {
				child.onMouseWheel.mouseX = notchesPoll;
				interfaceLowPriorityEvents.insertTail(child.onMouseWheel);
			}

			if (mouseOver && !child.mouseOver) {
				if (child.onMouseOverEnter != null) {
					child.onMouseOverEnter.mouseX = mouseX - componentX;
					child.onMouseOverEnter.mouseY = mouseY - componentY;
					interfaceLowPriorityEvents.insertTail(child.onMouseOverEnter);
				}

				child.mouseOver = true;
			} else if (mouseOver && child.mouseOver) {
				if (child.onMouseOver != null) {
					child.onMouseOver.mouseX = mouseX - componentX;
					child.onMouseOver.mouseY = mouseY - componentY;
					interfaceLowPriorityEvents.insertTail(child.onMouseOver);
				}
			} else if (!mouseOver && child.mouseOver) {
				if (child.onMouseLeave != null) {
					child.onMouseLeave.mouseX = mouseX - componentX;
					child.onMouseLeave.mouseY = mouseY - componentY;
					interfaceMediumPriorityEvents.insertTail(child.onMouseLeave);
				}

				child.mouseOver = false;
			}

			if (!child.mouseClicked && clicked) {
				child.mouseClicked = true;
				if (child.onMouseClick != null) {
					child.onMouseClick.mouseX = super.saveClickX - componentX;
					child.onMouseClick.mouseY = super.saveClickY - componentY;
					interfaceLowPriorityEvents.insertTail(child.onMouseClick);
				}
			}

			if (child.mouseClicked && buttonHeld && child.onMouseClickRepeat != null) {
				child.onMouseClickRepeat.mouseX = mouseX - componentX;
				child.onMouseClickRepeat.mouseY = mouseY - componentY;
				interfaceLowPriorityEvents.insertTail(child.onMouseClickRepeat);
			}

			if (child.mouseClicked && !buttonHeld) {
				child.mouseClicked = false;
				if (child.onMouseClickRelease != null) {
					child.onMouseClickRelease.mouseX = mouseX - componentX;
					child.onMouseClickRelease.mouseY = mouseY - componentY;
					interfaceMediumPriorityEvents.insertTail(child.onMouseClickRelease);
				}
			}

			if (child.onTimer != null) {
				interfaceHighPriorityEvents.insertTail(child.onTimer);
			}

			if (mouseOver) {
				if (child.hoverType >= 0) {
					hoveredInterfaceId = child.hoverType;
				} else if (child.mouseHoverTextRecolor != 0) {
					hoveredInterfaceId = child.id;
				}

				if (child.contentType > 0) {
					if (child.contentType >= 1346 && child.contentType <= 1349) {
						int id = child.contentType - 1346;
						MenuManager.addOption("Reset color", 693, 0, id);
						MenuManager.addOption("Choose color", 692, 0, id);
						continue;
					} else if (child.contentType == 1357) {
						int floorId = child.id - 24445;
						int toSetY = componentY + scrollY - y;
						short[] positionsY = RSInterface.interfaceCache[46056].childY;
						positionsY[675] = (short) toSetY;
						positionsY[676] = (short) (toSetY - 4);
						positionsY[682] = (short) (toSetY - 2);
						positionsY[683] = (short) (toSetY - 4);
						positionsY[684] = (short) (toSetY - 5);
						RSInterface.interfaceCache[24514].message = Integer.toString(floorId);
						RSInterface.interfaceCache[24515].message = getFloorName(floorId);
					}
				}
			}

			if (child.interfaceType == 0) {
				processInterface(child, componentX, componentY, child.scrollPosition, mouseX, mouseY);
				if (child.scrollMax > child.height) {
					handleScroller(componentX + child.width, componentY, mouseX, mouseY, child, child.height, child.scrollMax, true);
				}
			} else {
				if (mouseOver) {
					switch (child.atActionType) {
					case 1:
						if (BitwiseHelper.testBit(child.flags, RSInterface.CAN_DRAG_ON_TO_BIT)) {
							lastActiveInvInterface = child.id;
						}
						boolean flag = false;
						if (child.contentType != 0)
							flag = buildFriendsListMenu(child);
						String u = child.tooltip;
						if (u != null && !flag) {
							MenuManager.addOption(u, 315, child.componentIndex, child.id);
						}
						break;
					case 2:
						if (targetInterfaceId != -1) {
							break;
						}

						String s = child.targetVerb;
						if (s.indexOf(" ") != -1)
							s = s.substring(0, s.indexOf(" "));
						if (BitwiseHelper.testBit(child.flags, RSInterface.CAN_AUTOCAST)) {
							MenuManager.addOption("Autocast <col=ff00>" + child.targetName, 104, 0, child.id);
						}

						MenuManager.addOption(s + " <col=ff00>" + child.targetName, 626, 0, child.id);
						break;
					case 3:
						MenuManager.addOption("Close", 200, 0, child.id);
						break;
					case 4:
						MenuManager.addOption(child.tooltip, 169, 0, child.id);
						break;
					case 5:
						MenuManager.addOption(child.tooltip, 646, 0, child.id);
						break;
					case 6:
						if (!aBoolean1149) {
							MenuManager.addOption(child.tooltip, 679, 0, child.id);
						}
						break;
					}
					if ((child.interfaceType == 4 && child.message.length() > 0)
							|| child.interfaceType == 5 || child.interfaceType == 3) {
						if (child.actions != null) {
							for (int action = child.actions.length - 1; action >= 0; action--) {
								if (child.actions[action] != null) {
									MenuManager.addOption(child.actions[action]
											+ (child.interfaceType == 4 ? " " + child.message : "" + (child.targetVerb != null ? " " + child.targetVerb : "")), 647, child.componentIndex, action, child.id);
								}
							}
						}
					}
				}
				if (child.interfaceType == 2) {
					if (mouseX > width || mouseY > height || mouseX < x || mouseY < y) {
						continue;
					}

					int k2 = 0;
					for (int l2 = 0; l2 < child.height; l2++) {
						for (int i3 = 0; i3 < child.width; i3++) {
							int j3 = componentX + i3 * (32 + child.invSpritePadX);
							int k3 = componentY + l2 * (32 + child.invSpritePadY);
							if (k2 < 20) {
								j3 += child.spritesX[k2];
								k3 += child.spritesY[k2];
							}
							if (mouseX >= j3 && mouseY >= k3 && mouseX < j3 + 32 && mouseY < k3 + 32) {
								mouseInvInterfaceIndex = k2;
								lastActiveInvInterface = child.id;
								if (child.inv[k2] > 0) {
									ItemDef itemDef = ItemDef.forID(child.inv[k2] - 1);
									if (itemSelected == 1 && child.isInventoryInterface) {
										if (child.id != anInt1284 || k2 != anInt1283) {
											MenuManager.addOption("Use " + selectedItemName + " with <col=ff9040>" + itemDef.name, 870, itemDef.id, k2, child.id);
										}
									} else if (targetInterfaceId != -1 && child.isInventoryInterface) {
										if (targetMask.canUseOnInventory()) {
											MenuManager.addOption(spellTooltip + " <col=ff9040>" + itemDef.name, 543, itemDef.id, k2, child.id);
										}
									} else if (child.id >= 22035 && child.id <= 22042) { // bank
										// tabs-
										// don't
										// draw
										// count
									} else {
										if (child.isInventoryInterface) {
											if (itemDef.inventoryOptions != null) {
												for (int l3 = 4; l3 >= 3; l3--) {
													if (itemDef.inventoryOptions[l3] != null) {
														int id;
														if (l3 == 3)
															id = 493;
														else
															id = 847;
														MenuManager.addOption(itemDef.inventoryOptions[l3] + " <col=ff9040>" + itemDef.name, id, itemDef.id, k2, child.id);
													} else if (l3 == 4) {
														MenuManager.addOption("Drop <col=ff9040>" + itemDef.name, 847, itemDef.id, k2, child.id);
													}

													if (l3 == 4 && shiftDropEnabled && holdingShiftKey) {
														MenuManager.dropIndex = MenuManager.menuActionRow;
													}
												}
											}
										}
										if (child.usableItemInterface) {
											MenuManager.addOption("Use <col=ff9040>" + itemDef.name, 447, itemDef.id, k2, child.id);
										}
										if (child.isInventoryInterface && itemDef.inventoryOptions != null) {
											for (int i4 = 2; i4 >= 0; i4--)
												if (itemDef.inventoryOptions[i4] != null) {
													int id;
													if (i4 == 0)
														id = 74;
													else if (i4 == 1)
														id = 454;
													else
														id = 539;
													MenuManager.addOption(itemDef.inventoryOptions[i4] + " <col=ff9040>" + itemDef.name, id, itemDef.id, k2, child.id);
												}

										}
										if (child.actions != null) {
											for (int j4 = child.actions.length - 1; j4 >= 0; j4--)
												if (child.actions[j4] != null) {
													int id;
													if (j4 == 0)
														id = 632;
													else if (j4 == 1)
														id = 78;
													else if (j4 == 2)
														id = 867;
													else if (j4 == 3)
														id = 431;
													else if (j4 == 4)
														id = 53;
													else if (j4 == 5)
														id = 54;
													else
														id = 432;
													MenuManager.addOption(child.actions[j4] + " <col=ff9040>" + itemDef.name, id, itemDef.id, k2, child.id);
												}

										}

										String option;
										if ((myPrivilege >= 2 && myPrivilege <= 3) && Config.enableIds) {
											option = "Examine <col=ff9040>" + itemDef.name + " <col=ff00>(<col=ffffff>" + itemDef.id + "<col=ff00>)";
										} else {
											option = "Examine <col=ff9040>" + itemDef.name;
										}

										MenuManager.addOption(option, 1125, itemDef.id, k2, child.id);
									}
								}
							}
							k2++;
						}
					}
				}
			}

			handleEvents(child);
		}

		handleEvents(parent);
	}

	private void handleMainEvents(RSInterface parent) {
		if (parent.interfaceType != 0 || (parent.children == null && parent.dynamicComponents == null) || parent.isMouseoverTriggered)
			return;
		boolean usesDynamic = parent.dynamicComponents != null;
		int k1 = usesDynamic ? parent.dynamicComponents.length : parent.children.length;
		for (int l1 = 0; l1 < k1; l1++) {
			RSInterface child = usesDynamic ? parent.dynamicComponents[l1] : RSInterface.interfaceCache[parent.children[l1]];
			if (child == null || child.hidden) {
				continue;
			}

			if (child.interfaceType == 0) {
				handleMainEvents(child);
			}

			handleEvents(child);
		}

		handleEvents(parent);
	}

	private void handleEvents(RSInterface child) {
		if (child.onSkillUpdate != null && !updatedSkills.isEmpty()) {
			for (int i = 0, updateLength = updatedSkills.size(); i < updateLength; i++) {
				int skillId = updatedSkills.get(i);
				List<Runnable> list = child.onSkillUpdate.get(skillId);
				if (list == null) {
					continue;
				}

				for (int j = 0, length = list.size(); j < length; j++) {
					list.get(j).run();
				}
			}
		}

		if (child.onInventoryUpdate != null && !updatedInventories.isEmpty()) {
			for (int i = 0, updateLength = updatedInventories.size(); i < updateLength; i++) {
				int inventoryId = updatedInventories.get(i);
				List<Runnable> list = child.onInventoryUpdate.get(inventoryId);
				if (list == null) {
					continue;
				}

				for (int j = 0, length = list.size(); j < length; j++) {
					list.get(j).run();
				}
			}
		}

		if (child.onVarpUpdate != null && !updatedVarps.isEmpty()) {
			for (int i = 0, updateLength = updatedVarps.size(); i < updateLength; i++) {
				int varpId = updatedVarps.get(i);
				List<Runnable> list = child.onVarpUpdate.get(varpId);
				if (list == null) {
					continue;
				}

				for (int j = 0, length = list.size(); j < length; j++) {
					list.get(j).run();
				}
			}
		}
	}

	public void drawScrollbar(int h, int scrollPosition, int y, int x, int maxScroll) {
		scrollBar1.drawSprite(x, y);
		scrollBar2.drawSprite(x, (y + h) - 16);
		Raster.fillRect(x, y + 16, 16, h - 32, 0x000001, false);
		Raster.fillRect(x, y + 16, 15, h - 32, 0x3d3426, false);
		Raster.fillRect(x, y + 16, 13, h - 32, 0x342d21, false);
		Raster.fillRect(x, y + 16, 11, h - 32, 0x2e281d, false);
		Raster.fillRect(x, y + 16, 10, h - 32, 0x29241b, false);
		Raster.fillRect(x, y + 16, 9, h - 32, 0x252019, false);
		Raster.fillRect(x, y + 16, 1, h - 32, 0x000001, false);
		int k1 = ((h - 32) * h) / maxScroll;
		if (k1 < 8)
			k1 = 8;
		int l1 = ((h - 32 - k1) * scrollPosition) / (maxScroll - h);
		Raster.fillRect(x, y + 16 + l1, 16, k1, barFillColor, false);
		Raster.drawVerticalLine(x, y + 16 + l1, k1, 0x000001);
		Raster.drawVerticalLine(x + 1, y + 16 + l1, k1, 0x817051);
		Raster.drawVerticalLine(x + 2, y + 16 + l1, k1, 0x73654a);
		Raster.drawVerticalLine(x + 3, y + 16 + l1, k1, 0x6a5c43);
		Raster.drawVerticalLine(x + 4, y + 16 + l1, k1, 0x6a5c43);
		Raster.drawVerticalLine(x + 5, y + 16 + l1, k1, 0x655841);
		Raster.drawVerticalLine(x + 6, y + 16 + l1, k1, 0x655841);
		Raster.drawVerticalLine(x + 7, y + 16 + l1, k1, 0x61553e);
		Raster.drawVerticalLine(x + 8, y + 16 + l1, k1, 0x61553e);
		Raster.drawVerticalLine(x + 9, y + 16 + l1, k1, 0x5d513c);
		Raster.drawVerticalLine(x + 10, y + 16 + l1, k1, 0x5d513c);
		Raster.drawVerticalLine(x + 11, y + 16 + l1, k1, 0x594e3a);
		Raster.drawVerticalLine(x + 12, y + 16 + l1, k1, 0x594e3a);
		Raster.drawVerticalLine(x + 13, y + 16 + l1, k1, 0x514635);
		Raster.drawVerticalLine(x + 14, y + 16 + l1, k1, 0x4b4131);
		Raster.drawHorizontalLine(x, y + 16 + l1, 15, 0x000001);
		Raster.drawHorizontalLine(x, y + 17 + l1, 15, 0x000001);
		Raster.drawHorizontalLine(x, y + 17 + l1, 14, 0x655841);
		Raster.drawHorizontalLine(x, y + 17 + l1, 13, 0x6a5c43);
		Raster.drawHorizontalLine(x, y + 17 + l1, 11, 0x6d5f48);
		Raster.drawHorizontalLine(x, y + 17 + l1, 10, 0x73654a);
		Raster.drawHorizontalLine(x, y + 17 + l1, 7, 0x76684b);
		Raster.drawHorizontalLine(x, y + 17 + l1, 5, 0x7b6a4d);
		Raster.drawHorizontalLine(x, y + 17 + l1, 4, 0x7e6e50);
		Raster.drawHorizontalLine(x, y + 17 + l1, 3, 0x817051);
		Raster.drawHorizontalLine(x, y + 17 + l1, 2, 0x000001);
		Raster.drawHorizontalLine(x, y + 18 + l1, 16, 0x000001);
		Raster.drawHorizontalLine(x, y + 18 + l1, 15, 0x564b38);
		Raster.drawHorizontalLine(x, y + 18 + l1, 14, 0x5d513c);
		Raster.drawHorizontalLine(x, y + 18 + l1, 11, 0x625640);
		Raster.drawHorizontalLine(x, y + 18 + l1, 10, 0x655841);
		Raster.drawHorizontalLine(x, y + 18 + l1, 7, 0x6a5c43);
		Raster.drawHorizontalLine(x, y + 18 + l1, 5, 0x6e6046);
		Raster.drawHorizontalLine(x, y + 18 + l1, 4, 0x716247);
		Raster.drawHorizontalLine(x, y + 18 + l1, 3, 0x7b6a4d);
		Raster.drawHorizontalLine(x, y + 18 + l1, 2, 0x817051);
		Raster.drawHorizontalLine(x, y + 18 + l1, 1, 0x000001);
		Raster.drawHorizontalLine(x, y + 19 + l1, 16, 0x000001);
		Raster.drawHorizontalLine(x, y + 19 + l1, 15, 0x514635);
		Raster.drawHorizontalLine(x, y + 19 + l1, 14, 0x564b38);
		Raster.drawHorizontalLine(x, y + 19 + l1, 11, 0x5d513c);
		Raster.drawHorizontalLine(x, y + 19 + l1, 9, 0x61553e);
		Raster.drawHorizontalLine(x, y + 19 + l1, 7, 0x655841);
		Raster.drawHorizontalLine(x, y + 19 + l1, 5, 0x6a5c43);
		Raster.drawHorizontalLine(x, y + 19 + l1, 4, 0x6e6046);
		Raster.drawHorizontalLine(x, y + 19 + l1, 3, 0x73654a);
		Raster.drawHorizontalLine(x, y + 19 + l1, 2, 0x817051);
		Raster.drawHorizontalLine(x, y + 19 + l1, 1, 0x000001);
		Raster.drawHorizontalLine(x, y + 20 + l1, 16, 0x000001);
		Raster.drawHorizontalLine(x, y + 20 + l1, 15, 0x4b4131);
		Raster.drawHorizontalLine(x, y + 20 + l1, 14, 0x544936);
		Raster.drawHorizontalLine(x, y + 20 + l1, 13, 0x594e3a);
		Raster.drawHorizontalLine(x, y + 20 + l1, 10, 0x5d513c);
		Raster.drawHorizontalLine(x, y + 20 + l1, 8, 0x61553e);
		Raster.drawHorizontalLine(x, y + 20 + l1, 6, 0x655841);
		Raster.drawHorizontalLine(x, y + 20 + l1, 4, 0x6a5c43);
		Raster.drawHorizontalLine(x, y + 20 + l1, 3, 0x73654a);
		Raster.drawHorizontalLine(x, y + 20 + l1, 2, 0x817051);
		Raster.drawHorizontalLine(x, y + 20 + l1, 1, 0x000001);
		Raster.drawVerticalLine(x + 15, y + 16 + l1, k1, 0x000001);
		Raster.drawHorizontalLine(x, y + 15 + l1 + k1, 16, 0x000001);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 15, 0x000001);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 14, 0x3f372a);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 10, 0x443c2d);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 9, 0x483e2f);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 7, 0x4a402f);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 4, 0x4b4131);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 3, 0x564b38);
		Raster.drawHorizontalLine(x, y + 14 + l1 + k1, 2, 0x000001);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 16, 0x000001);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 15, 0x443c2d);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 11, 0x4b4131);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 9, 0x514635);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 7, 0x544936);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 6, 0x564b38);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 4, 0x594e3a);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 3, 0x625640);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 2, 0x6a5c43);
		Raster.drawHorizontalLine(x, y + 13 + l1 + k1, 1, 0x000001);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 16, 0x000001);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 15, 0x443c2d);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 14, 0x4b4131);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 12, 0x544936);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 11, 0x564b38);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 10, 0x594e3a);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 7, 0x5d513c);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 4, 0x61553e);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 3, 0x6e6046);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 2, 0x7b6a4d);
		Raster.drawHorizontalLine(x, y + 12 + l1 + k1, 1, 0x000001);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 16, 0x000001);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 15, 0x4b4131);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 14, 0x514635);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 13, 0x564b38);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 11, 0x594e3a);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 9, 0x5d513c);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 7, 0x61553e);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 5, 0x655841);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 4, 0x6a5c43);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 3, 0x73654a);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 2, 0x7b6a4d);
		Raster.drawHorizontalLine(x, y + 11 + l1 + k1, 1, 0x000001);
	}

	public void updateNPCs(RSBuffer stream, int i, boolean extendedView) {
		anInt839 = 0;
		anInt893 = 0;
		method139(stream);
		method46(i, stream, extendedView);
		method86(stream);
		for (int k = 0; k < anInt839; k++) {
			int l = anIntArray840[k];
			if (npcArray[l].anInt1537 != loopCycle) {
				npcArray[l].desc = null;
				npcArray[l] = null;
			}
		}
		if (stream.currentOffset != i) {
			SignLink.reporterror(
					myUsername + " size mismatch in getnpcpos - pos:" + stream.currentOffset + " psize:" + i);
			throw new RuntimeException("eek");
		}
		for (int i1 = 0; i1 < npcCount; i1++)
			if (npcArray[npcIndices[i1]] == null) {
				SignLink.reporterror(myUsername + " null entry in npc list - pos:" + i1 + " size:" + npcCount);
				throw new RuntimeException("eek");
			}

	}

	private static final int[] CHAT_BUTTONS_POS_X = { 5, 62, 119, 176, 233, 290, 347, 404 };
	private int cButtonHPos;
	private int cButtonCPos;

	public void processChatButtons() {
		int mouseX = super.mouseX;
		int mouseY = super.mouseY;
		int oldcButtonHPos = cButtonHPos;
		int startY = clientHeight - 23;

		if (mouseX >= CHAT_BUTTONS_POS_X[0] && mouseX <= CHAT_BUTTONS_POS_X[0] + 56 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 0;
		} else if (mouseX >= CHAT_BUTTONS_POS_X[1] && mouseX <= CHAT_BUTTONS_POS_X[1] + 56 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 1;
		} else if (mouseX >= CHAT_BUTTONS_POS_X[2] && mouseX <= CHAT_BUTTONS_POS_X[2] + 56 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 2;
		} else if (mouseX >= CHAT_BUTTONS_POS_X[3] && mouseX <= CHAT_BUTTONS_POS_X[3] + 56 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 3;
		} else if (mouseX >= CHAT_BUTTONS_POS_X[4] && mouseX <= CHAT_BUTTONS_POS_X[4] + 56 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 4;
		} else if (mouseX >= CHAT_BUTTONS_POS_X[5] && mouseX <= CHAT_BUTTONS_POS_X[5] + 56 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 5;
		} else if (mouseX >= CHAT_BUTTONS_POS_X[6] && mouseX <= CHAT_BUTTONS_POS_X[6] + 56 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 6;
		} else if (mouseX >= CHAT_BUTTONS_POS_X[7] && mouseX <= CHAT_BUTTONS_POS_X[7] + 111 && mouseY >= startY
				&& mouseY <= clientHeight) {
			cButtonHPos = 7;
		} else {
			cButtonHPos = -1;
		}

		if (oldcButtonHPos != cButtonHPos) {
			inputTaken = true;
		}

		if (super.clickMode3 == 1) {
			if (super.saveClickX >= CHAT_BUTTONS_POS_X[7] && super.saveClickX <= CHAT_BUTTONS_POS_X[7] + 111
					&& super.saveClickY >= startY && super.saveClickY <= clientHeight) {
				if (openModalInterfaceId != -1) {
					pushGameMessage("Please close the interface you have open before using 'report abuse'");
					return;
				}

				closeInterfacesTransmit();
				reportAbuseInput = "";
				canMute = false;
				for (int i = 0; i < RSInterface.interfaceCache.length; i++) {
					if (RSInterface.interfaceCache[i] != null && RSInterface.interfaceCache[i].contentType == 600) {
						reportAbuseInterfaceID = RSInterface.interfaceCache[i].parentID;
						openModalInterface(reportAbuseInterfaceID);
						break;
					}
				}
			}
		}
	}

	public void postVarpUpdate(int varpId) {
		Varp[] cache = Varp.cache;
		if (cache == null || varpId >= cache.length) {
			return;
		}

		int clientCode = cache[varpId].anInt709;
		if (clientCode == 0) {
			return;
		}

		int value = variousSettings[varpId];

		//System.out.println("clientCode = " + clientCode);
		switch (clientCode) {
			case 1:
				if (value == 1)
					Rasterizer.method372(0.9);
				else if (value == 2)
					Rasterizer.method372(0.8);
				else if (value == 3)
					Rasterizer.method372(0.7);
				else if (value == 4)
					Rasterizer.method372(0.6);
				ItemDef.itemSpriteCache.clear();
				fullRedraw = true;

				if (GraphicsDisplay.enabled) {
					Cache.getFloorVBOs().clear();
					Cache.getModelVBOs().clear();
				}
				break;
			case 3:
				Settings.changeSetting("music_volume", value);
				int volume = 0;
				if (value == 0)
					volume = 255;
				else if (value == 1)
					volume = 192;
				else if (value == 2)
					volume = 128;
				else if (value == 3)
					volume = 64;
				else if (value == 4)
					volume = 0;
				if (volume != musicVolume) {
					if (musicVolume != 0 || currentSong == -1) {
						if (volume != 0)
							setVolume(volume);
						else {
							method55(false);
							previousSong = 0;
						}
					} else {
						method56(volume, false, currentSong);
						previousSong = 0;
					}
					musicVolume = volume;
				}
				break;
			case 4:
				if (value == 0)
					soundEffectVolume = 127;
				else if (value == 1)
					soundEffectVolume = 96;
				else if (value == 2)
					soundEffectVolume = 64;
				else if (value == 3)
					soundEffectVolume = 32;
				else if (value == 4)
					soundEffectVolume = 0;
				break;
			case 5:
				anInt1253 = value;
				break;
			case 6:
				anInt1249 = value;
				break;
			case 8:
				splitPrivateChat = value;
				inputTaken = true;
				Settings.changeSetting("split_pm", value);
				break;
			case 9:
				anInt913 = value;
				break;
		}
	}

	private void updateEntity(Entity obj, int j) {
		if (obj == null || !obj.isVisible()) {
			return;
		}

		if (obj instanceof Npc) {
			EntityDef entityDef = ((Npc) obj).desc;
			if (entityDef.childrenIDs != null) {
				entityDef = entityDef.method161();
			}

			if (entityDef == null) {
				return;
			}
		}

		int yOff = 3;

		if (obj.loopCycleStatus > loopCycle) {
			npcScreenPos(obj, obj.height + 15);
			if (spriteDrawX > -1) {
				int currentHp = obj.currentHealth;
				int maxHP = obj.maxHealth;
				if (toggleNewHitBar) {
					int barWidth = ((currentHp * 56) / maxHP);
					if (barWidth > 56) {
						barWidth = 56;
					}

					if (currentHp > 0 && barWidth < 1) {
						barWidth = 1;
					}

					int barX = spriteDrawX - 28;
					int barY = spriteDrawY - yOff - 2;
					HPBarEmpty.drawSprite(barX, barY);
					Raster.setDrawingArea(barX, barY, barX + barWidth, barY + HPBarFull.myHeight);
					HPBarFull.drawSprite(barX, barY);
					Raster.defaultDrawingAreaSize();
					yOff += HPBarFull.myHeight + 2;
				} else {
					int barWidth = (currentHp * 30) / maxHP;
					if (barWidth > 30) {
						barWidth = 30;
					}

					if (currentHp > 0 && barWidth < 1) {
						barWidth = 1;
					}

					int barX = spriteDrawX - 15;
					int barY = spriteDrawY - yOff;
					Raster.fillRect(barX, barY, barWidth, 5, 65280, false);
					Raster.fillRect(barX + barWidth, barY, 30 - barWidth, 5, 0xff0000, false);
					yOff += 7;
				}
			}
		}

		if (obj.totalBarCycle > loopCycle) {
			npcScreenPos(obj, obj.height + 15);
			if (spriteDrawX > -1) {
				int width = 200;
				int height = 5;
				int leftOverCycle = obj.totalBarCycle - loopCycle;
				int fillWidth;
				if (obj.stayCycleAfterFill == 0) {
					double ratio = (double) obj.fill / obj.maxFill;
					fillWidth = (int) ((double) width * ratio);
				} else if (leftOverCycle <= obj.stayCycleAfterFill) {
					fillWidth = width;
				} else {
					leftOverCycle -= obj.stayCycleAfterFill;
					int currentFill = (obj.fill != 0 ? (obj.fill * ((obj.maxFill - leftOverCycle) / obj.fill)) : 0) + obj.startFill;
					fillWidth = (width * currentFill / obj.maxFill);
				}

				int drawX = spriteDrawX - (width >> 1);
				int drawY = spriteDrawY - yOff;
				if (!obj.reverse) {
					Raster.fillRect(drawX, drawY, fillWidth, height, obj.fillColor1, false);
					Raster.fillRect(drawX + fillWidth, drawY, width - fillWidth, height, obj.fillColor2, false);
				} else {
					Raster.fillRect(drawX + width - fillWidth, drawY, fillWidth, height, obj.fillColor1, false);
					Raster.fillRect(drawX, drawY, width - fillWidth, height, obj.fillColor2, false);
				}
			}
		}

		if (yOff < 30) {
			yOff = 30;
		}

		if (j < playerCount) {
			Player player = (Player) obj;
			if (!player.isVisible()) {
				return;
			}

			if (player.headIcon != -1 || player.skullIcon != -1) {
				npcScreenPos(obj, obj.height + 15);
				if (spriteDrawX > -1) {
					if (player.skullIcon != -1) {
						skullIcons[player.skullIcon].drawSprite(spriteDrawX - 12, spriteDrawY - yOff);
						yOff += 25;
					}
					if (player.headIcon != -1) {
						headIcons[player.headIcon].drawSprite(spriteDrawX - 12, spriteDrawY - yOff);
						yOff += 25;
					}
				}
			}

			if (j >= 0 && anInt855 == 10 && anInt933 == playerIndices[j]) {
				npcScreenPos(obj, obj.height + 15);
				if (spriteDrawX > -1) {
					headIconsHint[player.hintIcon].drawSprite(spriteDrawX - 12, spriteDrawY - yOff);
				}
			}
		} else {
			Npc npc = (Npc) obj;
			EntityDef npcType = npc.desc;
			if (npcType.childrenIDs != null) {
				npcType = npcType.method161();
			}

			if (npcType.headIcon >= 0 && npcType.headIcon < headIcons.length && !npc.isPet) {
				npcScreenPos(obj, obj.height + 15);
				if (spriteDrawX > -1) {
					headIcons[npcType.headIcon].drawSprite(spriteDrawX - 12, spriteDrawY - 30);
				}
			}

			if (anInt855 == 1 && anInt1222 == npcIndices[j - playerCount] && loopCycle % 20 < 10) {
				npcScreenPos(obj, obj.height + 15);
				if (spriteDrawX > -1) {
					headIconsHint[0].drawSprite(spriteDrawX - 12, spriteDrawY - 28);
				}
			}
		}

		if (obj.textSpoken != null && (j >= playerCount || publicChatMode == 0 || publicChatMode == 3
				|| publicChatMode == 1 && isFriendOrSelf(((Player) obj).name))) {
			npcScreenPos(obj, obj.height);
			if (spriteDrawX > -1 && anInt974 < anInt975) {
				anIntArray979[anInt974] = newBoldFont.getBasicWidth(obj.textSpoken) / 2;
				anIntArray978[anInt974] = newBoldFont.baseCharacterHeight;
				anIntArray976[anInt974] = spriteDrawX;
				anIntArray977[anInt974] = spriteDrawY;
				anIntArray980[anInt974] = obj.anInt1513;
				anIntArray981[anInt974] = obj.anInt1531;
				anIntArray982[anInt974] = obj.textCycle;
				aStringArray983[anInt974++] = obj.textSpoken;
				if (anInt1249 == 0 && obj.anInt1531 >= 1 && obj.anInt1531 <= 3) {
					anIntArray978[anInt974] += 10;
					anIntArray977[anInt974] += 5;
				}
				if (anInt1249 == 0 && obj.anInt1531 == 4)
					anIntArray979[anInt974] = 60;
				if (anInt1249 == 0 && obj.anInt1531 == 5)
					anIntArray978[anInt974] += 5;
			}
		}
		for (int j1 = 0; j1 < 4; j1++) { // hitmarks
			if (obj.hitsLoopCycle[j1] > loopCycle) {
				npcScreenPos(obj, obj.height / 2);
				if (spriteDrawX > -1) {
					if (toggleNewHitMarks) {
						switch (j1) {
							case 1:
								spriteDrawY += 20;
								break;
							case 2:
								spriteDrawY += 40;
								break;
							case 3:
								spriteDrawY += 60;
								break;
						}

						int cycleDiff = obj.hitsLoopCycle[j1] - loopCycle;
						int yOffset = Entity.NEW_HIT_Y_MOVE - (Entity.NEW_HIT_Y_MOVE * cycleDiff / Entity.NEW_HIT_CYCLE);
						int trans = (cycleDiff << 8) / (Entity.NEW_HIT_CYCLE - 40);
						if (trans < 0 || trans > 255) {
							trans = 255;
						}

						hitmarkDraw(String.valueOf(obj.hitArray[j1]).length(), obj.hitMarkTypes[j1], obj.hitIcon[j1],
								obj.hitArray[j1], yOffset, trans);
					} else {
						switch (j1) {
							case 1:
								spriteDrawY -= 20;
								break;
							case 2:
								spriteDrawX -= 15;
								spriteDrawY -= 10;
								break;
							case 3:
								spriteDrawX += 15;
								spriteDrawY -= 10;
								break;
						}

						hitMarks[obj.hitMarkTypes[j1]].drawSprite(spriteDrawX - 12, spriteDrawY - 12);
						newSmallFont.drawCenteredString(String.valueOf(obj.hitArray[j1]), spriteDrawX - 1,
								spriteDrawY + 3, 0xffffff, 0);
					}

					if (drawCombatBox && myPlayer.interactingEntity != -1) {
						if (obj instanceof Npc) {
							Npc npc = ((Npc) obj);
							if (myPlayer.interactingEntity == npc.index) {
								currentInteract = npc;
								combatBoxTimer.start(4);
							}
						} else if (obj instanceof Player) {
							Player player = ((Player) obj);
							if (myPlayer.interactingEntity - 32768 == player.index) {
								currentInteract = player;
								combatBoxTimer.start(4);
							}
						}
					}
				}
			}
		}
	}

	public void updateEntities() {
		anInt974 = 0;
		int priorityRenderIndex = -1;

		try {
			for (int j = -1; j < playerCount + npcCount; j++) {
				Entity obj;
				if (j == -1) {
					obj = myPlayer;
				} else if (j < playerCount) {
					int playerIndex = playerIndices[j];
					if (playerIndex == playerRenderPriorityIndex) {
						priorityRenderIndex = j;
						continue;
					}

					obj = playerArray[playerIndex];
				} else {
					obj = npcArray[npcIndices[j - playerCount]];
				}

				updateEntity(obj, j);
			}

			if (priorityRenderIndex != -1) {
				updateEntity(playerArray[playerRenderPriorityIndex], priorityRenderIndex);
			}

			for (int k = 0; k < anInt974; k++) {
				int k1 = anIntArray976[k];
				int l1 = anIntArray977[k];
				int j2 = anIntArray979[k];
				int k2 = anIntArray978[k];
				boolean flag = true;
				while (flag) {
					flag = false;
					for (int l2 = 0; l2 < k; l2++)
						if (l1 + 2 > anIntArray977[l2] - anIntArray978[l2] && l1 - k2 < anIntArray977[l2] + 2
								&& k1 - j2 < anIntArray976[l2] + anIntArray979[l2]
								&& k1 + j2 > anIntArray976[l2] - anIntArray979[l2]
								&& anIntArray977[l2] - anIntArray978[l2] < l1) {
							l1 = anIntArray977[l2] - anIntArray978[l2];
							flag = true;
						}

				}
				spriteDrawX = anIntArray976[k];
				spriteDrawY = anIntArray977[k] = l1;
				String s = RSFont.escape(aStringArray983[k]);
				if (anInt1249 == 0) {
					int color = 0xffff00;
					if (anIntArray980[k] < 6) {
						color = anIntArray965[anIntArray980[k]];
					} else if (anIntArray980[k] == 6) {
						color = anInt1265 % 20 >= 10 ? 0xffff00 : 0xff0000;
					} else if (anIntArray980[k] == 7) {
						color = anInt1265 % 20 >= 10 ? 65535 : 255;
					} else if (anIntArray980[k] == 8) {
						color = anInt1265 % 20 >= 10 ? 0x80ff80 : 45056;
					} else if (anIntArray980[k] == 9) {
						int j3 = 150 - anIntArray982[k];
						if (j3 < 50)
							color = 0xff0000 + 1280 * j3;
						else if (j3 < 100)
							color = 0xffff00 - 0x50000 * (j3 - 50);
						else if (j3 < 150)
							color = 65280 + 5 * (j3 - 100);
					} else if (anIntArray980[k] == 10) {
						int k3 = 150 - anIntArray982[k];
						if (k3 < 50)
							color = 0xff0000 + 5 * k3;
						else if (k3 < 100)
							color = 0xff00ff - 0x50000 * (k3 - 50);
						else if (k3 < 150)
							color = (255 + 0x50000 * (k3 - 100)) - 5 * (k3 - 100);
					} else if (anIntArray980[k] == 11) {
						int l3 = 150 - anIntArray982[k];
						if (l3 < 50)
							color = 0xffffff - 0x50005 * l3;
						else if (l3 < 100)
							color = 65280 + 0x50005 * (l3 - 50);
						else if (l3 < 150)
							color = 0xffffff - 0x50000 * (l3 - 100);
					}

					if (anIntArray981[k] == 0) {
						newBoldFont.drawCenteredString(s, spriteDrawX, spriteDrawY, color, 0);
					} else if (anIntArray981[k] == 1) {
						newBoldFont.drawCenteredStringMoveY(s, spriteDrawX, spriteDrawY, color, 0, anInt1265);
					} else if (anIntArray981[k] == 2) {
						newBoldFont.drawCenteredStringMoveXY(s, spriteDrawX, spriteDrawY, color, 0, anInt1265);
					} else if (anIntArray981[k] == 3) {
						newBoldFont.drawStringMoveY(s, spriteDrawX, spriteDrawY, color, 0, anInt1265,
								150 - anIntArray982[k]);
					} else if (anIntArray981[k] == 4) {
						int i4 = newBoldFont.getBasicWidth(s);
						int k4 = ((150 - anIntArray982[k]) * (i4 + 100)) / 150;
						Raster.setDrawingArea(spriteDrawX - 50, 0, spriteDrawX + 50, 334);
						newBoldFont.drawBasicString(s, (spriteDrawX + 50) - k4, spriteDrawY, color, 0);
						Raster.defaultDrawingAreaSize();
					} else if (anIntArray981[k] == 5) {
						int j4 = 150 - anIntArray982[k];
						int l4 = 0;
						if (j4 < 25)
							l4 = j4 - 25;
						else if (j4 > 125)
							l4 = j4 - 125;
						Raster.setDrawingArea(0, spriteDrawY - newBoldFont.baseCharacterHeight - 1, 512,
								spriteDrawY + 5);
						newBoldFont.drawCenteredString(s, spriteDrawX, spriteDrawY + l4, color, 0);
						Raster.defaultDrawingAreaSize();
					}
				} else {
					newBoldFont.drawCenteredString(s, spriteDrawX, spriteDrawY, 0xffff00, 0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void delFriend(long l) {
		try {
			if (l == 0L)
				return;
			for (int i = 0; i < friendsCount; i++) {
				if (friendsListAsLongs[i] != l)
					continue;
				friendsCount--;
				needDrawTabArea = true;
				for (int j = i; j < friendsCount; j++) {
					friendsList[j] = friendsList[j + 1];
					friendsNodeIDs[j] = friendsNodeIDs[j + 1];
					friendsListAsLongs[j] = friendsListAsLongs[j + 1];
				}

				stream.sendPacket(215);
				stream.writeQWord(l);
				break;
			}
		} catch (RuntimeException runtimeexception) {
			SignLink.reporterror("18622, " + false + ", " + l + ", " + runtimeexception.toString());
			throw new RuntimeException();
		}
	}

	private Sprite dungPartySideIcon;
	private Sprite seasonalPowerSideIcon;
	private Sprite[] sideIconSprites;
	private static final int[] FIXED_SIDE_ICON_TAB_ID = { 0, 1, 2, 14, 3, 4, 5, 6, 15, 8, 9, 7, 11, 12, 13, 16 };
	private static final int[] FIXED_SIDE_ICON_POS_X = { 12, 40, 70, 100, 130, 162, 190, 220, 8, 41, 70, 100, 130, 160, 190, 220 };
	private static final int[] FIXED_SIDE_ICON_POS_Y = { 9, 9, 8, 8, 8, 8, 8, 8, 304, 306, 306, 307, 306, 306, 306, 308 };
	private static final int[] FIXED_TAB_HOVER_X = { 6, 36, 66, 126, 156, 186, 216, 96, 36, 66, 0, 126, 156, 186, 96, 6, 216 };
	private static final int[] FIXED_TAB_HOVER_Y = { 0, 0, 0, 0, 0, 0, 0, 298, 298, 298, 0, 298, 298, 298, 0, 298, 298 };
	private static final int[] FIXED_TAB_CLICK_X = { 5, 35, 65, 125, 155, 185, 215, 95, 35, 65, 0, 125, 155, 185, 95, 215, 5 };
	private static final int[] FIXED_TAB_CLICK_Y = { 0, 0, 0, 0, 0, 0, 0, 299, 299, 299, 0, 299, 299, 299, 0, 299, 299 };
	private static final int[] RESIZABLE_SELECTED_TAB_ID = { 0, 1, 2, 14, 3, 4, 5, 6, 16, 8, 9, 7, 11, 12, 13, 15 };
	private static final int[] RESIZABLE_SIDE_ICON_POS_X = { 8, 37, 67, 97, 127, 159, 187, 217, 4, 38, 67, 97, 127, 157, 186, 217 };
	private static final int[] RESIZABLE_SIDE_ICON_POS_Y = { 9, 9, 8, 8, 8, 8, 8, 8, 6, 8, 8, 9, 8, 8, 8, 9 };

	public void drawSideIcons(boolean fixed, boolean hdEnabled) {
		if (fixed) {
			int xPos = 0, yPos = 0;
			if (hdEnabled) {
				xPos += 516;
				yPos += 168;
			}

			//Don't handle logout tab
			if (tabID != 10) {
				if (tabHPos != -1 && tabHPos != tabID && tabHPos != 10) {
					//Draw the hover tab
					tabHover.drawSprite(FIXED_TAB_HOVER_X[tabHPos] + xPos, FIXED_TAB_HOVER_Y[tabHPos] + yPos);
				}

				//Draw the selected tab
				selectedTab.drawSprite(FIXED_TAB_CLICK_X[tabID] + xPos, FIXED_TAB_CLICK_Y[tabID] + yPos);
			}

			//Draw the tab icons
			for (int index = 0; index < 16; index++) {
				int tabId = FIXED_SIDE_ICON_TAB_ID[index];
				int sidebarId = tabInterfaceIDs[tabId];
				if (sidebarId != -1) {
					Sprite sprite = sideIconSprites[index];
					if (tabId == 2 && sidebarId == 44026) {
						sprite = dungPartySideIcon;
					} else if (tabId == 13 && worldType == WorldType.SEASONAL) {
						sprite = seasonalPowerSideIcon;
					}

					sprite.drawSprite(FIXED_SIDE_ICON_POS_X[index] + xPos, FIXED_SIDE_ICON_POS_Y[index] + yPos);
				}
			}
			return;
		}

		int offsetX;
		int offsetY;
		if (showTab) {
			for (int index = 0; index < RESIZABLE_SELECTED_TAB_ID.length; index++) {
				int id = RESIZABLE_SELECTED_TAB_ID[index];
				if (tabID == id && tabInterfaceIDs[id] != -1) {
					if (clientWidth >= smallTabs) {
						offsetX = index > 7 ? 240 : 480;
						offsetY = 37;
					} else {
						offsetX = 240;
						offsetY = (index > 7 ? 37 : 74);
					}

					selectedTab.drawSprite(clientWidth - offsetX + positionX[index], clientHeight - offsetY);
				}
			}
		}

		for (int index = 0; index < FIXED_SIDE_ICON_TAB_ID.length; index++) {
			int tabId = FIXED_SIDE_ICON_TAB_ID[index];
			int sidebarId = tabInterfaceIDs[tabId];
			if (sidebarId != -1) {
				Sprite sprite = sideIconSprites[index];
				if (clientWidth >= smallTabs) {
					offsetX = index > 7 ? 242 : 482;
					offsetY = 37;
				} else {
					offsetX = 242;
					offsetY = (index > 7 ? 37 : 74);
				}

				if (tabId == 2 && sidebarId == 44026) {
					sprite = dungPartySideIcon;
				} else if (tabId == 13 && worldType == WorldType.SEASONAL) {
					sprite = seasonalPowerSideIcon;
				}

				sprite.drawSprite((clientWidth - offsetX) + RESIZABLE_SIDE_ICON_POS_X[index], (clientHeight - offsetY) + RESIZABLE_SIDE_ICON_POS_Y[index]);
			}
		}
	}

	public void drawRedStones() {
		if (tabInterfaceIDs[tabID] != -1) {
			if (tabID == 0)
				tabClicked.drawSprite(7, 0);
			if (tabID == 1)
				tabClicked.drawSprite(37, 0);
			if (tabID == 2)
				tabClicked.drawSprite(67, 0);
			if (tabID == 14)
				tabClicked.drawSprite(97, 0);
			if (tabID == 3)
				tabClicked.drawSprite(127, 0);
			if (tabID == 4)
				tabClicked.drawSprite(157, 0);
			if (tabID == 5)
				tabClicked.drawSprite(187, 0);
			if (tabID == 6)
				tabClicked.drawSprite(217, 0);
			if (tabID == 16)
				tabClicked.drawSprite(7, 298);
			if (tabID == 8)
				tabClicked.drawSprite(37, 298);
			if (tabID == 9)
				tabClicked.drawSprite(67, 298);
			if (tabID == 7)
				tabClicked.drawSprite(97, 298);
			if (tabID == 11)
				tabClicked.drawSprite(127, 298);
			if (tabID == 12)
				tabClicked.drawSprite(157, 298);
			if (tabID == 13)
				tabClicked.drawSprite(187, 298);
			if (tabID == 15)
				tabClicked.drawSprite(217, 298);
		}
	}

	int[] positionX = { 0, 30, 60, 90, 120, 150, 180, 210, 0, 30, 60, 90, 120, 150, 180, 210 };

	private void processTabAreaHovers() {
		if (isFixed()) {
			int oldTabHover = tabHPos;

			int positionX = clientWidth - 244;
			int positionY = 169, positionY2 = clientHeight - 36;
			if (mouseInRegion(clientWidth - 21, 0, clientWidth, 21)) {
				tabHPos = 10;
			} else if (mouseInRegion(positionX, positionY, positionX + 30, positionY + 36)) {
				tabHPos = 0;
			} else if (mouseInRegion(positionX + 30, positionY, positionX + 60, positionY + 36)) {
				tabHPos = 1;
			} else if (mouseInRegion(positionX + 60, positionY, positionX + 90, positionY + 36)) {
				tabHPos = 2;
			} else if (mouseInRegion(positionX + 90, positionY, positionX + 120, positionY + 36)) {
				tabHPos = 14;
			} else if (mouseInRegion(positionX + 120, positionY, positionX + 150, positionY + 36)) {
				tabHPos = 3;
			} else if (mouseInRegion(positionX + 150, positionY, positionX + 180, positionY + 36)) {
				tabHPos = 4;
			} else if (mouseInRegion(positionX + 180, positionY, positionX + 210, positionY + 36)) {
				tabHPos = 5;
			} else if (mouseInRegion(positionX + 210, positionY, positionX + 240, positionY + 36)) {
				tabHPos = 6;
			} else if (mouseInRegion(positionX, positionY2, positionX + 30, positionY2 + 36)) {
				tabHPos = 15;
			} else if (mouseInRegion(positionX + 30, positionY2, positionX + 60, positionY2 + 36)) {
				tabHPos = 8;
			} else if (mouseInRegion(positionX + 60, positionY2, positionX + 90, positionY2 + 36)) {
				tabHPos = 9;
			} else if (mouseInRegion(positionX + 90, positionY2, positionX + 120, positionY2 + 36)) {
				tabHPos = 7;
			} else if (mouseInRegion(positionX + 120, positionY2, positionX + 150, positionY2 + 36)) {
				tabHPos = 11;
			} else if (mouseInRegion(positionX + 150, positionY2, positionX + 180, positionY2 + 36)) {
				tabHPos = 12;
			} else if (mouseInRegion(positionX + 180, positionY2, positionX + 210, positionY2 + 36)) {
				tabHPos = 13;
			} else if (mouseInRegion(positionX + 210, positionY2, positionX + 240, positionY2 + 36)) {
				tabHPos = 16;
			} else {
				tabHPos = -1;
			}

			if (oldTabHover != tabHPos) {
				needDrawTabArea = true;
			}
		} else {
			int offsetX = (clientWidth >= smallTabs ? clientWidth - 480 : clientWidth - 240);
			int positionY = (clientWidth >= smallTabs ? clientHeight - 37 : clientHeight - 74);
			int secondPositionY = clientHeight - 37;
			int secondOffsetX = clientWidth >= smallTabs ? 240 : 0;
			if (mouseInRegion(clientWidth - 21, 0, clientWidth, 21)) {
				tabHPos = 10;
			} else if (mouseInRegion(positionX[0] + offsetX, positionY, positionX[0] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[0];
			} else if (mouseInRegion(positionX[1] + offsetX, positionY, positionX[1] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[1];
			} else if (mouseInRegion(positionX[2] + offsetX, positionY, positionX[2] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[2];
			} else if (mouseInRegion(positionX[3] + offsetX, positionY, positionX[3] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[3];
			} else if (mouseInRegion(positionX[4] + offsetX, positionY, positionX[4] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[4];
			} else if (mouseInRegion(positionX[5] + offsetX, positionY, positionX[5] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[5];
			} else if (mouseInRegion(positionX[6] + offsetX, positionY, positionX[6] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[6];
			} else if (mouseInRegion(positionX[7] + offsetX, positionY, positionX[7] + offsetX + 30, positionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[7];
			} else if (mouseInRegion(positionX[8] + offsetX + secondOffsetX, secondPositionY,
					positionX[8] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[8];
			} else if (mouseInRegion(positionX[9] + offsetX + secondOffsetX, secondPositionY,
					positionX[9] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[9];
			} else if (mouseInRegion(positionX[10] + offsetX + secondOffsetX, secondPositionY,
					positionX[10] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[10];
			} else if (mouseInRegion(positionX[11] + offsetX + secondOffsetX, secondPositionY,
					positionX[11] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[11];
			} else if (mouseInRegion(positionX[12] + offsetX + secondOffsetX, secondPositionY,
					positionX[12] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[12];
			} else if (mouseInRegion(positionX[13] + offsetX + secondOffsetX, secondPositionY,
					positionX[13] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[13];
			} else if (mouseInRegion(positionX[14] + offsetX + secondOffsetX, secondPositionY,
					positionX[14] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[14];
			} else if (mouseInRegion(positionX[15] + offsetX + secondOffsetX, secondPositionY,
					positionX[15] + offsetX + secondOffsetX + 30, secondPositionY + 37)) {
				tabHPos = FIXED_SIDE_ICON_TAB_ID[15];
			} else {
				tabHPos = -1;
			}
		}

		if (tabHPos != -1) {
			String[] ops;
			if (worldType == WorldType.SEASONAL) {
				ops = TAB_AREA_OPTIONS_SEASONAL;
			} else {
				ops = TAB_AREA_OPTIONS;
			}

			MenuManager.addOption(ops[tabHPos], 1076, tabHPos, 0, 0);
		}
	}

	static final String[] TAB_AREA_OPTIONS = { "Combat Styles", "Stats", "Quest Journal", "Inventory", "Worn Equipment",
			"Prayer List", "Magic Spellbook", "Clan Chat", "Friends List", "Account Settings", "Logout", "Options", "Emotes",
			"Setup", "Task List", "Summoning", "Rank Display" };

	static final String[] TAB_AREA_OPTIONS_SEASONAL = { "Combat Styles", "Stats", "Quest Journal", "Inventory", "Worn Equipment",
			"Prayer List", "Magic Spellbook", "Clan Chat", "Friends List", "Account Settings", "Logout", "Options", "Emotes",
			"Powers", "Task List", "Summoning", "Rank Display" };

	public int tabHPos;

	public boolean showTab = true;

	public boolean showChat = true;

	public static final int smallTabs = 1000;

	private void drawTabArea() {
		boolean isFixed = isFixed();
		boolean hdEnabled = GraphicsDisplay.enabled;

		if (!hdEnabled) {
			if (isFixed && inventoryIP != null) {
				inventoryIP.initDrawingArea();
			}

			Rasterizer.lineOffsets = anIntArray1181;
		}

		drawTabOverlay(isFixed, hdEnabled);
		if (showTab) {
			int x = isFixed ? 33 : clientWidth - 197;
			int yOffset = clientWidth >= smallTabs ? 37 : 74;
			int y = (isFixed ? 37 : clientHeight - yOffset - 267);
			if (hdEnabled && isFixed) {
				x += 516;
				y += 168;
			}
			if (openInventoryModalInterfaceId != -1) {
				drawInterface(RSInterface.interfaceCache[openInventoryModalInterfaceId], x, y, 0);
			} else if (tabInterfaceIDs[tabID] != -1) {
				drawInterface(RSInterface.interfaceCache[tabInterfaceIDs[tabID]], x, y, 0);
			}

			if (!GraphicsDisplay.enabled && MenuManager.menuOpen && menuScreenArea == 1) {
				drawMenu();
			}
		}

		if (!hdEnabled) {
			if (isFixed && inventoryIP != null) {
				inventoryIP.drawGraphics(super.graphics, 516, 168);
			}

			if (gameScreenIP != null) {
				gameScreenIP.initDrawingArea();
			}
			Rasterizer.lineOffsets = anIntArray1182;
		}
	}

	public void drawTabOverlay(boolean fixed, boolean hdEnabled) {
		boolean noTabInterface = openInventoryModalInterfaceId == -1;

		if (fixed) {
			int offsetX = 0;
			int offsetY = 0;
			if (hdEnabled) {
				offsetX = 516;
				offsetY = 168;
			}

			tabArea.drawSprite(offsetX, offsetY);
		} else {
			int width = tab.myWidth;
			int tabCount;

			if (clientWidth >= smallTabs) {//Expanded tabs
				tabCount = 16;
				int positionX = clientWidth - width * tabCount;
				int positionY = clientHeight - tab.myHeight;
				for (int index = 0; index < tabCount; index++) {
					Sprite tabSprite = tab;
					if (noTabInterface && tabHPos != -1 && tabHPos == FIXED_SIDE_ICON_TAB_ID[index]) {
						tabSprite = tabHover;
					}

					tabSprite.drawSprite(positionX, positionY);
					positionX += width;
				}

				if (showTab) {
					tabBackground.drawTransparentSprite(clientWidth - 197, clientHeight - 37 - 267, 150);
					tabBorder.drawSprite(clientWidth - 204, clientHeight - 37 - 274);
				}
			} else {
				tabCount = 8;
				int initialPositioX = clientWidth - width * tabCount;
				int positionX = initialPositioX;
				int positionY = clientHeight - 74;
				int tabId = 0;

				//First row of tabs
				for (int index = 0; index < tabCount; index++) {
					Sprite tabSprite = tab;
					if (noTabInterface && tabHPos != -1 && tabHPos == FIXED_SIDE_ICON_TAB_ID[tabId]) {
						tabSprite = tabHover;
					}

					tabSprite.drawSprite(positionX, positionY);
					positionX += width;
					tabId++;
				}

				positionX = initialPositioX;
				positionY = clientHeight - 37;
				//Second row of tabs
				for (int index = 0; index < tabCount; index++) {
					Sprite tabSprite = tab;
					if (noTabInterface && tabHPos != -1 && tabHPos == FIXED_SIDE_ICON_TAB_ID[tabId]) {
						tabSprite = tabHover;
					}

					tabSprite.drawSprite(positionX, positionY);
					positionX += width;
					tabId++;
				}

				if (showTab) {
					tabBackground.drawTransparentSprite(clientWidth - 197, clientHeight - 74 - 267, 150);
					tabBorder.drawSprite(clientWidth - 204, clientHeight - 74 - 274);
				}
			}
		}

		if (noTabInterface) {
			drawSideIcons(fixed, hdEnabled);
		}
	}

	public void method37(int j) {
		if (GraphicsDisplay.enabled) {
			return;
		}

		if (!lowMem) { // firecape texture will move
			if (Rasterizer.anIntArray1480[17] >= j) {
				Background background = Rasterizer.aBackgroundArray1474s[17];
				int k = background.anInt1452 * background.anInt1453 - 1;
				int j1 = background.anInt1452 * anInt945 * 2;
				byte abyte0[] = background.aByteArray1450;
				byte abyte3[] = aByteArray912;
				for (int i2 = 0; i2 <= k; i2++)
					abyte3[i2] = abyte0[i2 - j1 & k];

				background.aByteArray1450 = abyte3;
				aByteArray912 = abyte0;
				Rasterizer.method370(17);
			}
			if (Rasterizer.anIntArray1480[24] >= j) {
				Background background_1 = Rasterizer.aBackgroundArray1474s[24];
				int l = background_1.anInt1452 * background_1.anInt1453 - 1;
				int k1 = background_1.anInt1452 * anInt945 * 2;
				byte abyte1[] = background_1.aByteArray1450;
				byte abyte4[] = aByteArray912;
				for (int j2 = 0; j2 <= l; j2++)
					abyte4[j2] = abyte1[j2 - k1 & l];

				background_1.aByteArray1450 = abyte4;
				aByteArray912 = abyte1;
				Rasterizer.method370(24);
			}
			if (Rasterizer.anIntArray1480[34] >= j) {
				Background background_2 = Rasterizer.aBackgroundArray1474s[34];
				int i1 = background_2.anInt1452 * background_2.anInt1453 - 1;
				int l1 = background_2.anInt1452 * anInt945 * 2;
				byte abyte2[] = background_2.aByteArray1450;
				byte abyte5[] = aByteArray912;
				for (int k2 = 0; k2 <= i1; k2++)
					abyte5[k2] = abyte2[k2 - l1 & i1];

				background_2.aByteArray1450 = abyte5;
				aByteArray912 = abyte2;
				Rasterizer.method370(34);
			}
			if (Rasterizer.anIntArray1480[40] >= j) {
				Background background_2 = Rasterizer.aBackgroundArray1474s[40];
				int i1 = background_2.anInt1452 * background_2.anInt1453 - 1;
				int l1 = background_2.anInt1452 * anInt945 * 2;
				byte abyte2[] = background_2.aByteArray1450;
				byte abyte5[] = aByteArray912;
				for (int k2 = 0; k2 <= i1; k2++)
					abyte5[k2] = abyte2[k2 - l1 & i1];
				background_2.aByteArray1450 = abyte5;
				aByteArray912 = abyte2;
				Rasterizer.method370(40);
			}
			if (Rasterizer.anIntArray1480[59] >= j) {
				Background background_2 = Rasterizer.aBackgroundArray1474s[59];
				int i1 = background_2.anInt1452 * background_2.anInt1453 - 1;
				int l1 = background_2.anInt1452 * anInt945 * 2;
				byte abyte2[] = background_2.aByteArray1450;
				byte abyte5[] = aByteArray912;
				for (int k2 = 0; k2 <= i1; k2++)
					abyte5[k2] = abyte2[k2 - l1 & i1];
				background_2.aByteArray1450 = abyte5;
				aByteArray912 = abyte2;
				Rasterizer.method370(59);
			}
		}
	}

	public void method38() {
		for (int i = -1; i < playerCount; i++) {
			int j;
			if (i == -1)
				j = myPlayerIndex;
			else
				j = playerIndices[i];
			Player player = playerArray[j];
			if (player != null && player.textCycle > 0) {
				player.textCycle--;
				if (player.textCycle == 0)
					player.textSpoken = null;
			}
		}
		for (int k = 0; k < npcCount; k++) {
			int l = npcIndices[k];
			Npc npc = npcArray[l];
			if (npc != null && npc.textCycle > 0) {
				npc.textCycle--;
				if (npc.textCycle == 0)
					npc.textSpoken = null;
			}
		}
	}

	public void calcCameraPos() {
		int i = anInt1098 * 128 + 64;
		int j = anInt1099 * 128 + 64;
		int k = method42(plane, j, i) - anInt1100;
		if (xCameraPos < i) {
			xCameraPos += anInt1101 + ((i - xCameraPos) * anInt1102) / 1000;
			if (xCameraPos > i)
				xCameraPos = i;
		}
		if (xCameraPos > i) {
			xCameraPos -= anInt1101 + ((xCameraPos - i) * anInt1102) / 1000;
			if (xCameraPos < i)
				xCameraPos = i;
		}
		if (zCameraPos < k) {
			zCameraPos += anInt1101 + ((k - zCameraPos) * anInt1102) / 1000;
			if (zCameraPos > k)
				zCameraPos = k;
		}
		if (zCameraPos > k) {
			zCameraPos -= anInt1101 + ((zCameraPos - k) * anInt1102) / 1000;
			if (zCameraPos < k)
				zCameraPos = k;
		}
		if (yCameraPos < j) {
			yCameraPos += anInt1101 + ((j - yCameraPos) * anInt1102) / 1000;
			if (yCameraPos > j)
				yCameraPos = j;
		}
		if (yCameraPos > j) {
			yCameraPos -= anInt1101 + ((yCameraPos - j) * anInt1102) / 1000;
			if (yCameraPos < j)
				yCameraPos = j;
		}
		i = anInt995 * 128 + 64;
		j = anInt996 * 128 + 64;
		k = method42(plane, j, i) - anInt997;
		int l = i - xCameraPos;
		int i1 = k - zCameraPos;
		int j1 = j - yCameraPos;
		int k1 = (int) Math.sqrt(l * l + j1 * j1);
		int l1 = (int) (Math.atan2(i1, k1) * 325.94900000000001D) & 0x7ff;
		int i2 = (int) (Math.atan2(l, j1) * -325.94900000000001D) & 0x7ff;
		if (l1 < 128)
			l1 = 128;
		if (l1 > 383)
			l1 = 383;
		if (yCameraCurve < l1) {
			yCameraCurve += anInt998 + ((l1 - yCameraCurve) * anInt999) / 1000;
			if (yCameraCurve > l1)
				yCameraCurve = l1;
		}
		if (yCameraCurve > l1) {
			yCameraCurve -= anInt998 + ((yCameraCurve - l1) * anInt999) / 1000;
			if (yCameraCurve < l1)
				yCameraCurve = l1;
		}
		int j2 = i2 - xCameraCurve;
		if (j2 > 1024)
			j2 -= 2048;
		if (j2 < -1024)
			j2 += 2048;
		if (j2 > 0) {
			xCameraCurve += anInt998 + (j2 * anInt999) / 1000;
			xCameraCurve &= 0x7ff;
		}
		if (j2 < 0) {
			xCameraCurve -= anInt998 + (-j2 * anInt999) / 1000;
			xCameraCurve &= 0x7ff;
		}
		int k2 = i2 - xCameraCurve;
		if (k2 > 1024)
			k2 -= 2048;
		if (k2 < -1024)
			k2 += 2048;
		if (k2 < 0 && j2 > 0 || k2 > 0 && j2 < 0)
			xCameraCurve = i2;
	}

	// public void drawMenu(int i, int ii) {
	// drawMenu();
	// }

	public void drawMenu() {
		int xPos = menuOffsetX;
		int yPos = menuOffsetY;
		int menuW = menuWidth;
		int menuH = menuHeight + 1;
		int judgeGrey = 0x5d5447;
		// System.out.println("menuscreen "+menuScreenArea);
		// DrawingArea.drawPixels(height, yPos, xPos, color, width);
		// DrawingArea.fillPixels(xPos, width, height, color, yPos);
		Raster.fillRect(xPos, yPos, menuW, menuH, judgeGrey, false);
		Raster.fillRect(xPos + 1, yPos + 1, menuW - 2, 16, 0, false);
		Raster.drawRect(xPos + 1, yPos + 18, menuW - 2, menuH - 19, 0);
		newBoldFont.drawBasicString("Choose Option", xPos + 3, yPos + 14, judgeGrey, 0);

		int mouseX = super.mouseX;
		int mouseY = super.mouseY;
		switch (menuScreenArea) {
			case 0:
				mouseX -= isFixed() ? 4 : 0;
				mouseY -= isFixed() ? 4 : 0;
				break;
			case 1:
				mouseX -= 516; // 519
				mouseY -= 168;
				break;
			case 2:
				mouseX -= 5; // 17
				mouseY -= 338;
				break;
			case 3:
				mouseX -= 516; // 519
				mouseY -= 0;
				break;
		}
		for (int l1 = 0; l1 < MenuManager.menuActionRow; l1++) {
			int textY = yPos + 31 + (MenuManager.menuActionRow - 1 - l1) * 15;
			int textColor = 0xffffff;
			if (mouseX > xPos && mouseX < xPos + menuW && mouseY > textY - 13 && mouseY < textY + 3) { // mouse
				// hover
				// the
				// text
				// fields
				textColor = 0xffff00;
				Raster.fillRect(xPos + 3, textY - 11, menuWidth - 6, 15, 0x6f695d, false);
			}

			newBoldFont.drawBasicString(MenuManager.menuActionName[l1], xPos + 3, textY, textColor, 0);
		}
	}

	// private void drawMenu(int xOffSet, int yOffSet) {
	// int xPos = menuOffsetX - (xOffSet - 4);
	// int yPos = (-yOffSet + 4) + menuOffsetY;
	// int menuW = menuWidth;
	// int menuH = menuHeight + 1;
	// needDrawTabArea = true;
	// inputTaken = true;
	// tabAreaAltered = true;
	// // DrawingArea.drawPixels(height, yPos, xPos, color, width);
	// // DrawingArea.fillPixels(xPos, width, height, color, yPos);
	// Raster.drawPixels(menuH - 4, yPos + 2, xPos, 0x706a5e, menuW);
	// Raster.drawPixels(menuH - 2, yPos + 1, xPos + 1, 0x706a5e, menuW - 2);
	// Raster.drawPixels(menuH, yPos, xPos + 2, 0x706a5e, menuW - 4);
	// Raster.drawPixels(menuH - 2, yPos + 1, xPos + 3, 0x2d2822, menuW - 6);
	// Raster.drawPixels(menuH - 4, yPos + 2, xPos + 2, 0x2d2822, menuW - 4);
	// Raster.drawPixels(menuH - 6, yPos + 3, xPos + 1, 0x2d2822, menuW - 2);
	// Raster.drawPixels(menuH - 22, yPos + 19, xPos + 2, 0x524a3d, menuW - 4);
	// Raster.drawPixels(menuH - 22, yPos + 20, xPos + 3, 0x524a3d, menuW - 6);
	// Raster.drawPixels(menuH - 23, yPos + 20, xPos + 3, 0x2b271c, menuW - 6);
	// Raster.fillPixels(xPos + 3, menuW - 6, 1, 0x2a291b, yPos + 2);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x2a261b, yPos + 3);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x252116, yPos + 4);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x211e15, yPos + 5);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x1e1b12, yPos + 6);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x1a170e, yPos + 7);
	// Raster.fillPixels(xPos + 2, menuW - 4, 2, 0x15120b, yPos + 8);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x100d08, yPos + 10);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x090a04, yPos + 11);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x080703, yPos + 12);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x090a04, yPos + 13);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x070802, yPos + 14);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x090a04, yPos + 15);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x070802, yPos + 16);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x090a04, yPos + 17);
	// Raster.fillPixels(xPos + 2, menuW - 4, 1, 0x2a291b, yPos + 18);
	// Raster.fillPixels(xPos + 3, menuW - 6, 1, 0x564943, yPos + 19);
	// chatTextDrawingArea.method385(0xc6b895, "Choose Option", yPos + 14, xPos
	// + 3);
	// int j1 = super.mouseX;
	// int k1 = super.mouseY;
	// int mouseX = super.mouseX - (xOffSet);
	// int mouseY = (-yOffSet) + super.mouseY;
	// for (int l1 = 0; l1 < menuActionRow; l1++) {
	// int textY = yPos + 31 + (menuActionRow - 1 - l1) * 15;
	// int disColor = 0xc6b895;
	// if (mouseX > xPos && mouseX < xPos + menuW && mouseY > textY - 13 &&
	// mouseY < textY + 3) {
	// Raster.drawPixels(15, textY - 11, xPos + 3, 0x6f695d, menuWidth - 6);
	// disColor = 0xeee5c6;
	// }
	// chatTextDrawingArea.method389(true, xPos + 3, disColor,
	// menuActionName[l1], textY);
	// }
	// }

	public boolean addFriend(long l) {
		try {
			if (l == 0L)
				return false;
			if (friendsCount >= 100 && isMember != 1) {
				pushGameMessage("Your friendlist is full. Max of 100 for free users, and 200 for members");
				return false;
			}
			if (friendsCount >= 200) {
				pushGameMessage("Your friendlist is full. Max of 100 for free users, and 200 for members");
				return false;
			}
			String s = TextClass.fixName(TextClass.nameForLong(l));
			if (s.equalsIgnoreCase(this.myUsername)) {
				pushGameMessage("You cannot add yourself to the friends list.");
				return false;
			}
			for (int i = 0; i < friendsCount; i++)
				if (friendsListAsLongs[i] == l) {
					pushGameMessage(s + " is already on your friend list");
					return false;
				}
			for (int j = 0; j < ignoreCount; j++)
				if (ignoreListAsLongs[j] == l) {
					pushGameMessage("Please remove " + s + " from your ignore list first");
					return false;
				}

			if (s.equals(myPlayer.name)) {
				return false;
			} else {
//				friendsList[friendsCount] = s;
//				friendsListAsLongs[friendsCount] = l;
//				friendsNodeIDs[friendsCount] = 0;
//				friendsCount++;
//				needDrawTabArea = true;
				stream.sendPacket(188);
				stream.writeQWord(l);
				return true;
			}
		} catch (RuntimeException runtimeexception) {
			SignLink.reporterror("15283, " + (byte) 68 + ", " + l + ", " + runtimeexception.toString());
		}
		throw new RuntimeException();
	}

	public static int method42(int i, int j, int k) {
		int l = k >> 7;
		int i1 = j >> 7;
		if (l < 0 || i1 < 0 || l > 103 || i1 > 103)
			return 0;
		int j1 = i;
		if (j1 < 3 && (byteGroundArray[1][l][i1] & 2) == 2)
			j1++;
		int k1 = k & 0x7f;
		int l1 = j & 0x7f;
		int i2 = intGroundArray[j1][l][i1] * (128 - k1) + intGroundArray[j1][l + 1][i1] * k1 >> 7;
		int j2 = intGroundArray[j1][l][i1 + 1] * (128 - k1) + intGroundArray[j1][l + 1][i1 + 1] * k1 >> 7;
		return i2 * (128 - l1) + j2 * l1 >> 7;
	}

	public void resetLogout() {
		try {
			if (loginSocket != null) {
				loginSocket.close();
				loginSocket = null;
			}
			if (gameSocket != null) {
				gameSocket.close();
				gameSocket = null;
			}
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}

		timePlayed.reset();

		loginMessage1 = "";
		loginMessage2 = "";
		playerCompletedInitialize = false;
		loginScreenState = 0;
		Settings.changeSetting("username", myUsername);
		unlinkMRUNodes();
		ItemDef.itemSpriteCache.clear();
		worldController.initToNull();

		for (int i = 0; i < 4; i++) {
			aClass11Array1230[i].method210();
		}

		SpriteCache.clear();

		System.gc();
		currentSong = -1;
		nextSong = -1;
		previousSong = 0;
		method58(10, musicVolume, false, 0);

		for (int i = 0; i < currentExp.length; i++) {
			currentExp[i] = 0;
		}

		if (!LoginRenderer.rememberMeChecked) {
			myPassword = "";
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.setTitle(Config.SERVER_NAME);
			}
		});
		resetClanChatInfo();
		Widget.clear();

		setDuelScreen();
		RSInterface.interfaceCache[3900].actions[1] = "Buy 1";

		// set back to fixed screen on logout
		setSizeOfClient(DisplayMode.FIXED, null);

		WavPlayer.clear();

		swapToFullscreenPixmap();
		// initializeLoginScreen();

		reloadWorldAnim = true;
		gameLoaded = false;
		loggedIn = false;
	}

	public void setDuelScreen() {
		setInterfaceText("Dueling with:", 6671);
		setInterfaceText("", 6684); // no forfeit message above description
		setInterfaceText("No Ranged", 6698);
		setInterfaceText("Neither player is allowed to use ranged attacks.", 8270);
		setInterfaceText("No Melee", 6699);
		setInterfaceText("Neither player is allowed to use melee attacks.", 8272);
		setInterfaceText("No Magic", 6697);
		setInterfaceText("Neither player is allowed to use magic attacks.", 8274);
		setInterfaceText("No Sp. Atk", 7817);
		setInterfaceText("Neither player will be allowed to use special attacks.", 8276);
		setInterfaceText("Fun Weapons", 669);
		setInterfaceText("Both players will use a 'fun weapon', such as: flowers or rubber chicken.", 8278);
		setInterfaceText("Same Weapons", 6696);
		setInterfaceText("Both players will use the same weapon in duel.", 8260);
		setInterfaceText("No Drinks", 6701);
		setInterfaceText("Neither player will be allowed to use drinks.", 8280);
		setInterfaceText("No Food", 6702);
		setInterfaceText("Neither player will be allowed to use food.", 8282);
		setInterfaceText("No Prayer", 6703);
		setInterfaceText("Neither player will be allowed to use prayer.", 8284);
		setInterfaceText("No Movement", 6704);
		setInterfaceText("Players stand next to each other and aren't allowed to move.", 8262);
		setInterfaceText("Obstacles", 6731);
		setInterfaceText("The duel will be in an arena with obstacles.", 8286);
	}

	public void setClanWarDuelScreen() {
		setInterfaceText("Challenging Clan:", 6671);
		setInterfaceText("", 6684); // no forfeit message above description
		setInterfaceText("No Ranged", 6698);
		setInterfaceText("Players are not allowed to use ranged attacks.", 8270);
		setInterfaceText("No Melee", 6699);
		setInterfaceText("Players are not allowed to use melee attacks.", 8272);
		setInterfaceText("No Magic", 6697);
		setInterfaceText("Players are not allowed to use magic attacks.", 8274);
		setInterfaceText("No Sp. Atk", 7817);
		setInterfaceText("Players will not be allowed to use special attacks.", 8276);
		setInterfaceText("No Summoning", 669);
		setInterfaceText("Players will not be able to use their Summoning skill.", 8278);
		setInterfaceText("Items lost upon death", 6696);
		setInterfaceText("Upon entering the map, the player will be skulled and will lose items on death.", 8260);
		setInterfaceText("No Drinks", 6701);
		setInterfaceText("Players will not be allowed to use drinks.", 8280);
		setInterfaceText("No Food", 6702);
		setInterfaceText("Players will not be allowed to use food.", 8282);
		setInterfaceText("No Prayer", 6703);
		setInterfaceText("Players will not be allowed to use prayer.", 8284);
		setInterfaceText("Single Combat", 6704);
		setInterfaceText("The zone will be single combat with PJ timers.", 8262);
		setInterfaceText("No Spiritshield", 6731);
		setInterfaceText("Players will not be allowed to bring Spirit Shields.", 8286);
	}

	public void clearInterfaceByNewIndex(int index) {
		switch (index) {
		case 1:
			clearMarketSearchInterface();
			break;
		case 2:
			clearTradingPosts();
			break;
		case 3:
			clearClaimTradingPostInterface();
			break;
		case 4:
			clearQuestMainInterface();
			break;
		}
	}

	public int MARKET_STRING_ID = 45071;

	public void clearMarketSearchInterface() {
		int nameInterface = MARKET_STRING_ID;
		int priceInterface = MARKET_STRING_ID + 20;
		int amountInterface = MARKET_STRING_ID + 40;

		setInterfaceText("Item Searched", 45043);

		for (int i = 0; i < 20; i++, nameInterface++, priceInterface++, amountInterface++) {
			// update playername
			setInterfaceText("", nameInterface);
			// update item price
			setInterfaceText("", priceInterface);
			// update amount
			setInterfaceText("", amountInterface);
			// config codes
			setVarp(800 + i, 0);
		}
	}

	private final int TRADING_POST_START = 19752;
	private void clearTradingPosts() {
		setInterfaceText("My Buy Orders", 19701);
		RSInterface.interfaceCache[19503].itemId = 0;
		setInterfaceText("Filter:", 19603);
		int stringStart = 19617;
		for (int i = 0; i < 40; i++) {
			if (i == 20)
				stringStart = TRADING_POST_START;
			setInterfaceText(i >= 20 ? "Click here to create an offer" : "", stringStart++);
			setInterfaceText("", stringStart++);
			setInterfaceText("", stringStart++);
			stringStart++;
		}
	}

	private void clearClaimTradingPostInterface() {
		setInterfaceText("Unclaimed Items", 19701);

		int stringStart = TRADING_POST_START;
		for (int i = 0; i < 20; i++) {
			setInterfaceText("", stringStart++);
			setInterfaceText("", stringStart++);
			setInterfaceText("", stringStart++);
			stringStart++;
		}
	}

	private void clearQuestMainInterface() {
		for (int i = 8144; i < 8196; i++) {
			setInterfaceText("", i);
		}
		for (int i = 12174; i < 12224; i++) {
			setInterfaceText("", i);
		}
	}

	private void resetClanChatInfo() {
		setInterfaceText("Talking in: ", 18139);
		setInterfaceText("Owner: ", 18140);
		for (int j = 18144; j < 18244; j++) {
			setInterfaceText("", j);
		}
		setInterfaceText("Join chat", 18135);
		setInterfaceText("Off", 18250);
	}

	public void openChatInterface(int chatInterfaceId) {
		setInputDialogState(chatInterfaceId);
		amountOrNameInput = "";
		inputTaken = true;
		if (chatInterfaceId == SEARCH_STATE) {
			totalItemResults = 0;
			itemResultNames = new String[100];
			itemResultIDs = new int[100];
			itemResultScrollPos = 0;
		}
	}

	public void method45() {
		aBoolean1031 = true;
		for (int j = 0; j < 7; j++) {
			anIntArray1065[j] = -1;
			for (int k = 0; k < IDK.length; k++) {
				if (IDK.cache[k].aBoolean662 || IDK.cache[k].anInt657 != j + (aBoolean1047 ? 0 : 7))
					continue;
				anIntArray1065[j] = k;
				break;
			}
		}
	}

	private void method46(int i, RSBuffer stream, boolean extendedView) {
		while (stream.bitPosition + 27 < i * 8) {
			int k = stream.readBits(14);
			if (k == 16383)
				break;
			if (npcArray[k] == null)
				npcArray[k] = new Npc();
			Npc npc = npcArray[k];
			npc.index = k;
			npcIndices[npcCount++] = k;
			npc.anInt1537 = loopCycle;
			int l;
			if (extendedView) {
				l = stream.readBits(8);
				if (l > 127)
					l -= 256;
			} else {
				l = stream.readBits(5);
				if (l > 15)
					l -= 32;
			}
			int i1;
			if (extendedView) {
				i1 = stream.readBits(8);
				if (i1 > 127)
					i1 -= 256;
			} else {
				i1 = stream.readBits(5);
				if (i1 > 15)
					i1 -= 32;
			}
			int j1 = stream.readBits(1);
			npc.desc = EntityDef.forID(stream.readBits(17));
			int k1 = stream.readBits(1);
			if (k1 == 1)
				anIntArray894[anInt893++] = k;
			npc.anInt1540 = npc.desc.aByte68;
			npc.anInt1504 = npc.desc.anInt79;
			npc.anInt1554 = npc.desc.walkAnim;
			npc.anInt1555 = npc.desc.anInt58;
			npc.anInt1556 = npc.desc.anInt83;
			npc.anInt1557 = npc.desc.anInt55;
			npc.anInt1511 = npc.desc.standAnim;
			npc.setPos(myPlayer.smallX[0] + i1, myPlayer.smallY[0] + l, j1 == 1);
		}
		stream.finishBitAccess();
	}

	@Override
	public void processGameLoop() {
		if (rsAlreadyLoaded || loadingError || genericLoadingError || clearingCache) {
			return;
		}

		loopCycle++;

		// We update size after awt events finished, so insets get a chance to
		// update.
		if (resizeEvent != null) {
			final Dimension size = new Dimension(resizeEvent);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					int offsetWidth;
					int offsetHeight;
					if (applyInsets) {
						Insets insets = frame.getInsets();
						offsetWidth = insets.left + insets.right;
						offsetHeight = insets.top + insets.bottom;
					} else {
						offsetWidth = 0;
						offsetHeight = 0;
					}

					Dimension newSize = new Dimension(offsetWidth + (int) size.getWidth(),
							offsetHeight + (int) size.getHeight());
					frame.setMinimumSize(newSize);
					frame.setSize(newSize);
					canvas.requestFocus();
					updateGame.set(true);
					applyInsets = false;
				}
			});

			resizeEvent = null;
		}

		if (loggedIn) {
			mainGameProcessor();
		} else if (doneLoading) {
			loginRenderer.process();
		}

		Widget.tick();

		if (loadingStage > 0)
			processOnDemandQueue();
		method49();
		handleSounds();
	}

	public void method47(PlayerRenderPriority priority) {
		int playersToRender = playerCount;
		if (priority == PlayerRenderPriority.SELF_PLAYER || priority == PlayerRenderPriority.PRIORITIZED_PLAYER) {
			playersToRender = 1;
		}

		for (int l = 0; l < playersToRender; l++) {
			Player player;
			long i1;
			if (priority == PlayerRenderPriority.SELF_PLAYER) {
				player = myPlayer;
				i1 = (long) myPlayerIndex << 32;
			} else if (priority == PlayerRenderPriority.PRIORITIZED_PLAYER) {
				player = playerArray[playerRenderPriorityIndex];
				i1 = (long) playerRenderPriorityIndex << 32;
			} else {
				player = playerArray[playerIndices[l]];
				i1 = (long) playerIndices[l] << 32;
				if (PlayerRenderPriority.PRIORITIZED_PLAYER_OTHER == priority && playerIndices[l] == playerRenderPriorityIndex) {
					continue;
				}
			}

			if (player == null || !player.isVisible()) {
				continue;
			}

			player.aBoolean1699 = false;
			if ((lowMem && playerCount > 50 || playerCount > 200) && priority != PlayerRenderPriority.SELF_PLAYER
					&& player.baseAnimId == player.anInt1511 && player.animId == -1) {
				player.aBoolean1699 = true;
			}
			int j1 = player.x >> 7;
			int k1 = player.z >> 7;
			if (j1 < 0 || j1 >= 104 || k1 < 0 || k1 >= 104)
				continue;
			if (player.aModel_1714 != null && loopCycle >= player.anInt1707 && loopCycle < player.anInt1708) {
				player.aBoolean1699 = false;
				player.y = method42(plane, player.z, player.x);
				worldController.method286(plane, player.z, player, player.anInt1552, player.anInt1722, player.x,
						player.y, player.anInt1719, player.anInt1721, i1, player.anInt1720);
				continue;
			}
			if ((player.x & 0x7f) == 64 && (player.z & 0x7f) == 64) {
				if (anIntArrayArray929[j1][k1] == anInt1265)
					continue;
				anIntArrayArray929[j1][k1] = anInt1265;
			}

			player.y = method42(plane, player.z, player.x);
			worldController.method285(plane, player.anInt1552, player.y, i1, player.z, 60, player.x, player,
					player.aBoolean1541, 0);
			PlayerClone clone = player.playerClone;
			if (clone != null) {
				worldController.method285(plane, clone.anInt1552, method42(plane, clone.z, clone.x), i1, clone.z, 60, clone.x, clone, player.aBoolean1541, 0);
			}
		}
	}

	private boolean promptUserForInput(RSInterface class9) {
		int j = class9.contentType;
		if (anInt900 == 2) {
			if (j == 201) {
				inputTaken = true;
				setInputDialogState(0);
				messagePromptRaised = true;
				promptInput = "";
				friendsListAction = 1;
				aString1121 = "Enter name of friend to add to list";
			}
			if (j == 202) {
				inputTaken = true;
				setInputDialogState(0);
				messagePromptRaised = true;
				promptInput = "";
				friendsListAction = 2;
				aString1121 = "Enter name of friend to delete from list";
			}
		}
		if (j == 205) {
			anInt1011 = 250;
			return true;
		}
		if (j == 501) {
			inputTaken = true;
			setInputDialogState(0);
			messagePromptRaised = true;
			promptInput = "";
			friendsListAction = 4;
			aString1121 = "Enter name of player to add to list";
		}
		if (j == 502) {
			inputTaken = true;
			setInputDialogState(0);
			messagePromptRaised = true;
			promptInput = "";
			friendsListAction = 5;
			aString1121 = "Enter name of player to delete from list";
		}
		if (j == 550) {
			inputTaken = true;
			setInputDialogState(0);
			messagePromptRaised = true;
			promptInput = "";
			friendsListAction = 6;
			aString1121 = "Enter the name of the chat you wish to join";
		}
		if (j >= 300 && j <= 313) {
			int k = (j - 300) / 2;
			int j1 = j & 1;
			int i2 = anIntArray1065[k];
			if (i2 != -1) {
				do {
					if (j1 == 0 && --i2 < 0)
						i2 = IDK.length - 1;
					if (j1 == 1 && ++i2 >= IDK.length)
						i2 = 0;
				} while (IDK.cache[i2].aBoolean662 || IDK.cache[i2].anInt657 != k + (aBoolean1047 ? 0 : 7));
				anIntArray1065[k] = i2;
				aBoolean1031 = true;
			}
		}
		if (j >= 314 && j <= 323) {
			int l = (j - 314) / 2;
			int k1 = j & 1;
			int j2 = anIntArray990[l];
			if (k1 == 0 && --j2 < 0)
				j2 = anIntArrayArray1003[l].length - 1;
			if (k1 == 1 && ++j2 >= anIntArrayArray1003[l].length)
				j2 = 0;
			anIntArray990[l] = j2;
			aBoolean1031 = true;
		}
		if (j == 324 && !aBoolean1047) {
			aBoolean1047 = true;
			method45();
		}
		if (j == 325 && aBoolean1047) {
			aBoolean1047 = false;
			method45();
		}
		if (j == 326) {
			stream.sendPacket(101);
			stream.writeByte(aBoolean1047 ? 0 : 1);
			for (int i1 = 0; i1 < 7; i1++)
				stream.writeByte(anIntArray1065[i1]);

			for (int l1 = 0; l1 < 5; l1++)
				stream.writeByte(anIntArray990[l1]);

			return true;
		}
		if (j == 613)
			canMute = !canMute;
		if (j >= 601 && j <= 612) {
			closeInterfacesTransmit();
			if (reportAbuseInput.length() > 0) {
				stream.sendPacket(218);
				stream.writeQWord(TextClass.longForName(reportAbuseInput));
				stream.writeByte(j - 601);
				stream.writeByte(canMute ? 1 : 0);
			}
		}
		return false;
	}

	public void method49(RSBuffer stream) {
		for (int j = 0; j < anInt893; j++) {
			int k = anIntArray894[j];
			Player player = playerArray[k];
			int l = stream.readUnsignedByte();
			if ((l & 0x40) != 0)
				l += stream.readUnsignedByte() << 8;
			method107(l, k, stream, player);
		}
	}

	public void method50(int y, int primaryColor, int x, int secondaryColor, int z) {
		long uid = worldController.method300(z, x, y);
		if (uid != 0L) {
			int direction = (int) (uid >> 20) & 0x3;
			int type = (int) (uid >> 14) & 0x1f;
			int color = primaryColor;
			if (uid > 0)
				color = secondaryColor;

			int scene_pixels[] = miniMap.myPixels;
			int pixel = 24624 + x * 4 + (103 - y) * 512 * 4;
			int object_id = (int) (uid >>> 32) & 0x7fffffff;
			
			final int regionId = ((regionBaseX + x) >> 6 << 8) + ((regionBaseY + y) >> 6);
			final int version = ObjectManager.getObjectDefVer(regionId);
			
			ObjectDef def = ObjectManager.getObjectDef(object_id, version);
			if (def.anInt758 != -1) {
				Background scene = mapScenes[def.anInt758];
				if (scene != null) {
					int scene_x = (def.anInt744 * 4 - scene.anInt1452) / 2;
					int scene_y = (def.anInt761 * 4 - scene.anInt1453) / 2;
					scene.drawBackground(48 + x * 4 + scene_x, 48 + (104 - y - def.anInt761) * 4 + scene_y);
				}
			} else {
				if ((def.anInt758 ^ 0xffffffff) == 0) {
					if (type == 0 || type == 2)
						if (direction == 0) {
							scene_pixels[pixel] = color;
							scene_pixels[pixel + 512] = color;
							scene_pixels[pixel + 1024] = color;
							scene_pixels[pixel + 1536] = color;
						} else if ((((direction ^ 0xffffffff) == -2) || direction == 1)) {
							scene_pixels[pixel] = color;
							scene_pixels[pixel + 1] = color;
							scene_pixels[pixel + 2] = color;
							scene_pixels[pixel + 3] = color;
						} else if (direction == 2) {
							scene_pixels[pixel + 3] = color;
							scene_pixels[pixel + 3 + 512] = color;
							scene_pixels[pixel + 3 + 1024] = color;
							scene_pixels[pixel + 3 + 1536] = color;
						} else if (direction == 3) {
							scene_pixels[pixel + 1536] = color;
							scene_pixels[pixel + 1536 + 1] = color;
							scene_pixels[pixel + 1536 + 2] = color;
							scene_pixels[pixel + 1536 + 3] = color;
						}
					if (type == 3)
						if (direction == 0)
							scene_pixels[pixel] = color;

						else if (direction == 1)
							scene_pixels[pixel + 3] = color;

						else if (direction == 2)
							scene_pixels[pixel + 3 + 1536] = color;

						else if (direction == 3)
							scene_pixels[pixel + 1536] = color;

					if (type == 2)
						if (direction == 3) {
							scene_pixels[pixel] = color;
							scene_pixels[pixel + 512] = color;
							scene_pixels[pixel + 1024] = color;
							scene_pixels[pixel + 1536] = color;
						} else if (direction == 0) {
							scene_pixels[pixel] = color;
							scene_pixels[pixel + 1] = color;
							scene_pixels[pixel + 2] = color;
							scene_pixels[pixel + 3] = color;
						} else if (direction == 1) {
							scene_pixels[pixel + 3] = color;
							scene_pixels[pixel + 3 + 512] = color;
							scene_pixels[pixel + 3 + 1024] = color;
							scene_pixels[pixel + 3 + 1536] = color;
						} else if (direction == 2) {
							scene_pixels[pixel + 1536] = color;
							scene_pixels[pixel + 1536 + 1] = color;
							scene_pixels[pixel + 1536 + 2] = color;
							scene_pixels[pixel + 1536 + 3] = color;
						}
				}
			}
		}
		uid = worldController.method302(z, x, y);
		if (uid != 0L) {
			int direction = (int) (uid >> 20) & 0x3;
			int type = (int) (uid >> 14) & 0x1f;
			int object_id = (int) (uid >>> 32) & 0x7fffffff;
			
			final int regionId = ((regionBaseX + x) >> 6 << 8) + ((regionBaseY + y) >> 6);
			final int version = ObjectManager.getObjectDefVer(regionId);
			
			ObjectDef def = ObjectManager.getObjectDef(object_id, version);
			
			if (def.anInt758 != -1 && def.anInt758 < mapScenes.length) {
				Background scene = mapScenes[def.anInt758];
				if (scene != null) {
					int scene_x = (def.anInt744 * 4 - scene.anInt1452) / 2;
					int scene_y = (def.anInt761 * 4 - scene.anInt1453) / 2;
					scene.drawBackground(48 + x * 4 + scene_x, 48 + (104 - y - def.anInt761) * 4 + scene_y);
				}
			} else if (type == 9) {
				int color = 0xeeeeee;
				if (uid > 0)
					color = 0xee0000;

				int scene_pixels[] = miniMap.myPixels;
				int pixel = 24624 + x * 4 + (103 - y) * 512 * 4;
				if (direction == 0 || direction == 2) {
					scene_pixels[pixel + 1536] = color;
					scene_pixels[pixel + 1024 + 1] = color;
					scene_pixels[pixel + 512 + 2] = color;
					scene_pixels[pixel + 3] = color;
				} else {
					scene_pixels[pixel] = color;
					scene_pixels[pixel + 512 + 1] = color;
					scene_pixels[pixel + 1024 + 2] = color;
					scene_pixels[pixel + 1536 + 3] = color;
				}
			}
		}
		uid = worldController.method303(z, x, y);
		if (uid != 0) {
			int objectId = (int) (uid >>> 32) & 0x7fffffff;
			
			final int regionId = ((regionBaseX + x) >> 6 << 8) + ((regionBaseY + y) >> 6);
			final int version = ObjectManager.getObjectDefVer(regionId);
			
			ObjectDef def = ObjectManager.getObjectDef(objectId, version);
			if (def.anInt758 != -1 && def.anInt758 < mapScenes.length) {
				Background scene = mapScenes[def.anInt758];
				if (scene != null) {
					int scene_x = (def.anInt744 * 4 - scene.anInt1452) / 2;
					int scene_y = (def.anInt761 * 4 - scene.anInt1453) / 2;
					scene.drawBackground(48 + x * 4 + scene_x, 48 + (104 - y - def.anInt761) * 4 + scene_y);
				}
			}
		}
	}

	private static void setHighMem() {
		WorldController.lowMem = false;
		lowMem = false;
		ObjectManager.lowMem = false;
		ObjectDef.lowMem = false;
	}

	public static void main(String args[]) {
		System.setProperty("sun.java2d.opengl", "false");
		System.setProperty("sun.java2d.d3d", "false");
		System.setProperty("sun.awt.noerasebackground", "true");
		System.setProperty("sun.java2d.noddraw", "true");
		ImageIO.setUseCache(false);
		// System.setOut(new OutLogger(System.out));
		// System.setErr(new OutLogger(System.err));
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			LibsLoader.loadNativeLibs(0);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		try {
            Client.DISCORD_PRESENCE.initiate(); //Discord rich presence
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Error e) {
            e.printStackTrace();
        }
		
		try {
			/*
			 * System.setOut(new java.io.PrintStream(System.out) { private
			 * StackTraceElement getCallSite() { for (StackTraceElement e :
			 * Thread.currentThread() .getStackTrace()) if
			 * (!e.getMethodName().equals("getStackTrace") &&
			 * !e.getClassName().equals(getClass().getName())) return e; return
			 * null; }
			 *
			 * @Override public void print(String s) { StackTraceElement e =
			 * getCallSite(); String callSite = e == null ? "??" :
			 * String.format("%s.%s(%s:%d)", e.getClassName(),
			 * e.getMethodName(), e.getFileName(), e.getLineNumber());
			 * super.print(s + "\t\tat " + callSite); }
			 *
			 * @Override public void println(String s) { println((Object) s); }
			 *
			 * @Override public void println(Object o) { StackTraceElement e =
			 * getCallSite(); String callSite = e == null ? "??" :
			 * String.format("%s.%s(%s:%d)", e.getClassName(),
			 * e.getMethodName(), e.getFileName(), e.getLineNumber());
			 * super.println(o + "\t\tat " + callSite); } }); System.setErr(new
			 * java.io.PrintStream(System.err) { private StackTraceElement
			 * getCallSite() { for (StackTraceElement e : Thread.currentThread()
			 * .getStackTrace()) if (!e.getMethodName().equals("getStackTrace")
			 * && !e.getClassName().equals(getClass().getName())) return e;
			 * return null; }
			 *
			 * @Override public void println(String s) { println((Object) s); }
			 *
			 * @Override public void print(String s) { StackTraceElement e =
			 * getCallSite(); String callSite = e == null ? "??" :
			 * String.format("%s.%s(%s:%d)", e.getClassName(),
			 * e.getMethodName(), e.getFileName(), e.getLineNumber());
			 * super.print(s + "\t\tat " + callSite); }
			 *
			 * @Override public void println(Object o) { StackTraceElement e =
			 * getCallSite(); String callSite = e == null ? "??" :
			 * String.format("%s.%s(%s:%d)", e.getClassName(),
			 * e.getMethodName(), e.getFileName(), e.getLineNumber());
			 * super.println(o + "\t\tat " + callSite); } });
			 */

			if (args.length > 0) {
				if (!args[0].isEmpty() && args[0] != "") {
					Config.SERVER_NAME = args[0];
				}
				if (args.length > 1 && !args[1].isEmpty()) {
					Config.SERVER_IP = args[1];
				}
			}

			SignLink.startpriv();
			// instance = new Jframe(args);
			// DisplayModeChanger.main(args);
			instance = new Client();
			instance.createClientFrame(503, 765);// client frame size

			if (Config.SERVER_NAME.equals("SoulPvP")) {
				server = Config.SERVER_IP_PVP;
				port = Config.SERVER_PORT_PVP;
				rotationWorlds = 2;
			}
		} catch (Exception e) {
			ErrorHandler.submitError(null, e);
		}
	}

	public static Client instance;
	public String[] texts;

	private void setTextOnScren(String... texts) {
		this.texts = texts;
		if (!GraphicsDisplay.enabled) {
			drawTextOnScreen();
		}
	}

	private void drawTextOnScreen() {
		if (texts == null) {
			return;
		}

		if (!GraphicsDisplay.enabled) {
			gameScreenIP.initDrawingArea();
		}

		int x = 10;
		int y = 10;
		int boxWidth = 0;
		int boxHeight = texts.length * 13;
		for (String text : texts) {
			int width = newRegularFont.getBasicWidth(text);
			if (boxWidth < width) {
				boxWidth = width;
			}
		}
		Raster.fillRect(x - 4, y - 4, boxWidth + 8, boxHeight + 8, 0, false);
		Raster.drawRect(x - 4, y - 4, boxWidth + 8, boxHeight + 8, 16777215);
		for (String text : texts) {
			newRegularFont.drawBasicString(text, x, y + 10, 16777215, 0);
			y += 13;
		}

		if (!GraphicsDisplay.enabled) {
			boolean isFixed = isFixed();
			int offset = isFixed ? 4 : 0;
			gameScreenIP.drawGraphics(super.graphics, offset, offset);
		}
	}

	public void loadingStages() {
		if (/* ObjectManager.lowMem && */ loadingStage == 2 && ObjectManager.anInt131 != plane) {
			setTextOnScren("Loading - please wait.");
			loadingStage = 1;
			loadingStartTime = System.currentTimeMillis();
		}
		if (loadingStage == 1) {
			int j = method54();
			if (j != 0 && System.currentTimeMillis() - loadingStartTime > 0x57e40L) {
				ErrorHandler.submitError(myUsername + " glcfb " + aLong1215 + "," + j + "," + lowMem + "," + fileStores[0] + ","
						+ onDemandFetcher.getWaitingToLoadCount() + "," + plane + "," + anInt1069 + "," + anInt1070, null);
				loadingStartTime = System.currentTimeMillis();
			}
			if (noClip) {
				for (int k1 = 0; k1 < 4; k1++) {
					for (int i2 = 1; i2 < 103; i2++) {
						for (int k2 = 1; k2 < 103; k2++)
							aClass11Array1230[k1].anIntArrayArray294[i2][k2] = 0;

					}
				}
			}
		}
	}

	public static boolean grabMapIds = false;
	public static ArrayList<Integer> tempMapIds = new ArrayList<Integer>();

	public static void copyMaps() {
		for (int id : tempMapIds) {
			File file = new File("S:/Users/Lesik/Desktop/Random RSPS Data/Decrypted Maps/maps1 659ithink/index8/" + id + ".gz");
			if (!file.exists()) {
				System.out.println("missing map pack id:" + id);
			} else {
				try {

					int fileIndex = Integer.parseInt(Client.getFileNameWithoutExtension(file.toString()));
					byte[] data = Client.fileToByteArray(file);
					if (data != null && data.length > 0) {
						Client.instance.fileStores[4].writeFile(data.length, data, fileIndex);
						System.out.println("Repacked " + fileIndex + ".");
					} else {
						System.out.println("Unable to locate index " + fileIndex + ".");
					}

					// only copy files
					// FileUtils.copyFile(file, new File("C:/Users/Lesik/soulplaycacheTEST/index4grab/"+id+".gz"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private int method54() {
		// if (aByteArrayArray1183 != null)
		// return 0;
		if (loggedIn) {
			if (!gameLoaded) {
				return -5;
			}

			int oldRegion = objectDefsRegion;
			int x = regionBaseX + (myPlayer.x - 6 >> 7);
			int y = regionBaseY + (myPlayer.z - 6 >> 7);
			int regionId = (x >> 6 << 8) + (y >> 6);
			inOsrsRegion = osrsRegions(regionId) || x >= 6400;
			setDefsRegion(regionId);
			if (oldRegion != objectDefsRegion) {
				ObjectDef.recentUse.clear();
				ObjectDef.mruNodes1.clear();
				ObjectDef.mruNodes2.clear();
			}
		}

		for (int i = 0; i < localRegionMapData.length; i++) {
			if (localRegionMapData[i] == null && localRegionMapIds[i] != -1) {
				// System.out.println("MapID:" + localRegionMapIds[i] + " index:" + i);
				if (grabMapIds && !tempMapIds.contains(localRegionMapIds[i]))
					tempMapIds.add(localRegionMapIds[i]);
				return -1;// null map files
			}
			if (localRegionLandscapeData[i] == null && localRegionLandscapeIds[i] != -1) {
				// System.out.println("LandscapeID:" + localRegionLandscapeIds[i]);
				if (grabMapIds && !tempMapIds.contains(localRegionLandscapeIds[i]))
					tempMapIds.add(localRegionLandscapeIds[i]);
				return -2;// null landscape file
			}
		}

		boolean oldSetting = ObjectManager.drawGroundDecor;
		if (inOsrsRegion) {
			ObjectManager.drawGroundDecor = true;
		}

		boolean flag = true;
		for (int j = 0; j < localRegionMapData.length; j++) {
			byte abyte0[] = localRegionLandscapeData[j];
			if (abyte0 != null) {
				int k = (localRegionIds[j] >> 8) * 64 - regionBaseX;
				int l = (localRegionIds[j] & 0xff) * 64 - regionBaseY;
				if (constructedViewport) {
					k = 10;
					l = 10;
				}
				flag &= ObjectManager.method189(k, abyte0, l, localRegionIds[j]);
			}
		}

		ObjectManager.drawGroundDecor = oldSetting;
		if (!flag)
			return -3;// couldn't parse all landscapes
		if (aBoolean1080) {
			return -4;
		} else {
			ObjectManager.anInt131 = plane;
			method22();
			if (loggedIn && plane != anInt985) {
				anInt985 = plane;
				method24(plane);
			}
			loadingStage = 2;
			if (loggedIn)
				stream.sendPacket(121);
			return 0;
		}
	}

	public void method55() {
		for (Animable_Sub4 class30_sub2_sub4_sub4 = (Animable_Sub4) aClass19_1013
				.reverseGetFirst(); class30_sub2_sub4_sub4 != null; class30_sub2_sub4_sub4 = (Animable_Sub4) aClass19_1013
				.reverseGetNext()) {
			if (class30_sub2_sub4_sub4.anInt1597 != plane || loopCycle > class30_sub2_sub4_sub4.anInt1572) {
				class30_sub2_sub4_sub4.unlink();
			} else if (loopCycle >= class30_sub2_sub4_sub4.anInt1571) {
				if (class30_sub2_sub4_sub4.anInt1590 > 0) {
					Npc npc = npcArray[class30_sub2_sub4_sub4.anInt1590 - 1];
					if (npc != null && npc.x >= 0 && npc.x < 13312 && npc.z >= 0 && npc.z < 13312)
						class30_sub2_sub4_sub4.method455(loopCycle, npc.z,
								method42(class30_sub2_sub4_sub4.anInt1597, npc.z, npc.x)
										- class30_sub2_sub4_sub4.anInt1583,
								npc.x);
				}
				if (class30_sub2_sub4_sub4.anInt1590 < 0) {
					int j = -class30_sub2_sub4_sub4.anInt1590 - 1;
					Player player;
					if (j == myPlayerIndexOnWorldLoggedIn) {
						player = myPlayer;
					} else {
						try {
							player = playerArray[j];
						} catch (ArrayIndexOutOfBoundsException ex) {
							player = myPlayer;
						}
					}
					if (player != null && player.x >= 0 && player.x < 13312 && player.z >= 0 && player.z < 13312) {
						class30_sub2_sub4_sub4.method455(loopCycle, player.z,
								method42(class30_sub2_sub4_sub4.anInt1597, player.z, player.x)
										- class30_sub2_sub4_sub4.anInt1583,
								player.x);
					}
				}
				class30_sub2_sub4_sub4.method456(anInt945);

				int projUid = class30_sub2_sub4_sub4.spotAnimId | class30_sub2_sub4_sub4.anInt1571 << 16;
				worldController.method285(plane, class30_sub2_sub4_sub4.anInt1595,
						(int) class30_sub2_sub4_sub4.aDouble1587, 0, (int) class30_sub2_sub4_sub4.aDouble1586, 60,
						(int) class30_sub2_sub4_sub4.aDouble1585, class30_sub2_sub4_sub4, false, projUid);
			}
		}
	}

	@Override
	public AppletContext getAppletContext() {
		return super.getAppletContext();
	}

	public static String capitalize(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (i == 0) {
				s = String.format("%s%s", Character.toUpperCase(s.charAt(0)), s.substring(1));
			}
			if (!Character.isLetterOrDigit(s.charAt(i))) {
				if (i + 1 < s.length()) {
					s = String.format("%s%s%s", s.subSequence(0, i + 1), Character.toUpperCase(s.charAt(i + 1)),
							s.substring(i + 2));
				}
			}
		}
		return s;
	}

	public void processOnDemandQueue() {
		if (onDemandFetcher == null) {
			return;
		}

		while (true) {
			OnDemandData entry = onDemandFetcher.checkDoneLoading();
			if (entry == null) {
				return;
			}

			switch (entry.indexId) {
				case 0:
				case 5:
				case 8:
					byte[] buffer = entry.buffer;
					Model.method460(buffer, entry.fileId, entry.indexId == 5);
					needDrawTabArea = true;
					if (backDialogID != -1)
						inputTaken = true;
					break;
				case 1:
					if (entry.buffer == null) {
						break;
					}

					Class36.load(entry.fileId, entry.buffer);
					break;
				case 2:
					if (entry.fileId == nextSong && entry.buffer != null) {
						musicData = new byte[entry.buffer.length];
						System.arraycopy(entry.buffer, 0, musicData, 0, musicData.length);
						fetchMusic = true;
					}
					break;
				case 3:
				case 6:
					if (loadingStage != 1) {
						break;
					}

					for (int i = 0; i < localRegionMapData.length; i++) {
						if (localRegionMapIds[i] == entry.fileId) {
							if (localRegionMapData[i] == null)
								localRegionMapData[i] = entry.buffer;
							if (entry.buffer == null)
								localRegionMapIds[i] = -1;
							break;
						}
						if (localRegionLandscapeIds[i] == entry.fileId)  {
							if (localRegionLandscapeData[i] == null)
								localRegionLandscapeData[i] = entry.buffer;
							if (entry.buffer == null)
								localRegionLandscapeIds[i] = -1;
							break;
						}
					}
					break;
				case 4:
					if (entry.buffer == null) {
						break;
					}

					Texture.decode(entry.fileId, entry.buffer);
					break;
				case 93:
					if (!onDemandFetcher.method564(entry.fileId)) {
						break;
					}

					ObjectManager.method173(new RSBuffer(entry.buffer), onDemandFetcher);
					break;
			}
		}
	}

	private static void onInterfaceLoad(int id) {
		if (id < 0) {
			return;
		}

		executeOnLoad(id);
		resetInterfaceSeq(id);
	}

	private static void executeOnLoad(int id) {
		RSInterface parent = RSInterface.interfaceCache[id];
		if (parent == null) {
			return;
		}

		int[] components = parent.children;
		int length = components.length;
		for (int i = 0; i < length; i++) {
			int componentId = components[i];
			if (componentId == -1) break;

			RSInterface component = RSInterface.interfaceCache[componentId];
			if (component == null || component.onLoad == null) {
				continue;
			}

			component.onLoad.run();
		}
	}

	private static void resetInterfaceSeq(int id) {
		RSInterface parent = RSInterface.interfaceCache[id];
		if (parent == null) {
			return;
		}

		int[] components = parent.children;
		int length = components.length;
		for (int i = 0; i < length; i++) {
			int componentId = components[i];
			if (componentId == -1) break;

			RSInterface component = RSInterface.interfaceCache[componentId];
			if (component == null) {
				continue;
			}

			if (component.interfaceType == 1)
				resetInterfaceSeq(component.id);
			component.anInt246 = 0;
			component.anInt208 = 0;
		}
	}

	public void drawHeadIcon() {
		if (anInt855 != 2)
			return;
		calcEntityScreenPos((anInt934 - regionBaseX << 7) + anInt937, anInt936 * 2,
				(anInt935 - regionBaseY << 7) + anInt938);
		if (spriteDrawX > -1 && loopCycle % 20 < 10)
			headIconsHint[0].drawSprite(spriteDrawX - 12, spriteDrawY - 28);
	}

	public void updateGame() {
		clientWidth = frame.getWidth();
		clientHeight = frame.getHeight();

		int menuOffsetY = menuBar != null && menuBar.isVisible() ? menuBar.getHeight() : 0;
		clientHeight -= menuOffsetY;

		Insets insets = frame.getInsets();
		clientWidth -= insets.left + insets.right;
		clientHeight -= insets.bottom + insets.top;

		canvasWidth = clientWidth;
		canvasHeight = clientHeight;

		if (!loggedIn)
			return;

		boolean isFixed = isFixed();
		boolean hdEnabled = GraphicsDisplay.enabled;
		int width = 512;
		int height = 334;
		if (!isFixed || hdEnabled) {
			width = clientWidth;
			height = clientHeight;
		}

		// canvas.setSize(canvasWidth, canvasHeight);
		// canvas.setLocation(insets.left, insets.top);
		gameScreenIP = new RSImageProducer(width, height, canvas);
		if (!hdEnabled) {
			Rasterizer.setBounds(isFixed ? 519 : clientWidth, isFixed ? 165 : clientHeight);
			anIntArray1180 = Rasterizer.lineOffsets;
			Rasterizer.setBounds(isFixed ? 250 : clientWidth, isFixed ? 335 : clientHeight);
			anIntArray1181 = Rasterizer.lineOffsets;
			Rasterizer.setBounds(isFixed ? 512 : clientWidth, isFixed ? 334 : clientHeight);
			anIntArray1182 = Rasterizer.lineOffsets;
			Raster.reset();
		} else {
			if (isFixed) {
				width = 512;
				height = 334;
			}
			Rasterizer.center_x = width / 2;
			Rasterizer.center_y = height / 2;
			redrawGLModels = true;
		}

		changeInterfaceLocation();
	}

	public static boolean redrawGLModels = false;
	public static boolean unflagBoolean = false;

	public void toggleSize(DisplayMode displayMode) {
		if (displayMode != currentDisplayMode) {
			currentDisplayMode = displayMode;

			updateGame.set(true);
			redrawGLModels = true;
			if (displayMode == DisplayMode.FIXED) {
				showTab = true;
				showChat = true;
			}
		}
	}

	public static final AtomicBoolean updateGame = new AtomicBoolean();
	public static final AtomicBoolean toggleGl = new AtomicBoolean();

	private void mainGameProcessor() {
		if (!isFixed()) {
			int width = frame.getWidth();
			int height = frame.getHeight();

			int menuOffsetY = menuBar != null && menuBar.isVisible() ? menuBar.getHeight() : 0;
			height -= menuOffsetY;

			Insets insets = frame.getInsets();
			width -= insets.left + insets.right;
			height -= insets.bottom + insets.top;

			if (clientWidth != width || clientHeight != height) {
				updateGame.set(true);
			}
		}

		if (anInt1104 > 1) {
			anInt1104--;
		}

		if (anInt1011 > 0) {
			anInt1011--;
		}

		if (!MenuManager.menuOpen) {
			MenuManager.clearMiniMenu();
		}

		for (int j = 0; j < 100; j++)
			if (!parsePacket())
				break;

		if (!loggedIn)
			return;

		int thisNotches = notches;
		notches = 0;
		notchesPoll = thisNotches;

		ReflectionCheck.processReflectionChecks(stream);

		if (super.clickMode3 != 0) {
//			long l = (super.aLong29 - aLong1220) / 50L;
//			if (l > 4095L)
//				l = 4095L;
//			aLong1220 = super.aLong29;
//			int k2 = super.saveClickY;
//			if (k2 < 0)
//				k2 = 0;
//			else if (k2 > 502)
//				k2 = 502;
//			int k3 = super.saveClickX;
//			if (k3 < 0)
//				k3 = 0;
//			else if (k3 > 764)
//				k3 = 764;
//			int k4 = k2 * 765 + k3;
//			int j5 = 0;
//			if (super.clickMode3 == 2)
//				j5 = 1;
//			int l5 = (int) l;
//			stream.sendPacket(241);
//			stream.writeDWord((l5 << 20) + (j5 << 19) + k4);
		}
		if (anInt1016 > 0)
			anInt1016--;
		if (super.keyArray[1] == 1 || super.keyArray[2] == 1 || super.keyArray[3] == 1 || super.keyArray[4] == 1)
			aBoolean1017 = true;
		if (aBoolean1017 && anInt1016 <= 0) {
			anInt1016 = 20;
			aBoolean1017 = false;
			stream.sendPacket(86);
			stream.writeWord(anInt1184);
			stream.method432(viewRotation);
		}
		if (super.awtFocus && !aBoolean954) {
			aBoolean954 = true;
			stream.sendPacket(3);
			stream.writeByte(1);
			holdingShiftKey = false;
			holdingCtrlKey = false;
		}
		if (!super.awtFocus && aBoolean954) {
			aBoolean954 = false;
			stream.sendPacket(3);
			stream.writeByte(0);
			holdingShiftKey = false;
			holdingCtrlKey = false;
		}
		loadingStages();
		method115();
		method90();
		anInt1009++;
		if (anInt1009 > 750)
			dropClient();
		method114();
		method95();
		method38();
		anInt945++;
		if (crossType != 0) {
			crossIndex += 20;
			if (crossIndex >= 400)
				crossType = 0;
		}
		if (announcement != null && announcement.addHour != calendar.get24Hour()) {
			announcement = null;
		}

		if (fog != null) {
			fog.process();
		}

		if (backDialogID == -1) {
			if (getInputDialogState() == SEARCH_STATE && itemResultsMaxScroll > 0) {
				itemSearchInterface.scrollPosition = itemResultScrollPos;
				if (super.mouseX >= 8 && super.mouseX <= 512 && super.mouseY >= clientHeight - 165 && super.mouseY <= clientHeight - 25) {
					handleScroller(494, 0, super.mouseX, super.mouseY - (clientHeight - 155), itemSearchInterface, 110, itemResultsMaxScroll, false);
				}

				int scrollPos = itemSearchInterface.scrollPosition;
	            scrollPos = Math.min(Math.max(scrollPos, 0), itemResultsMaxScroll);
				if (itemResultScrollPos != scrollPos) {
					itemResultScrollPos = scrollPos;
					inputTaken = true;
				}
			} else {
				chatboxInterface.scrollPosition = anInt1211 - anInt1089 - 110;
				if (super.mouseX >= 8 && super.mouseX <= 512 && super.mouseY >= clientHeight - 165 && super.mouseY <= clientHeight - 25) {
					handleScroller(494, 0, super.mouseX, super.mouseY - (clientHeight - 155), chatboxInterface, 110, anInt1211, false);
				}

				int i = anInt1211 - 110 - chatboxInterface.scrollPosition;
				if (i < 0)
					i = 0;
				if (i > anInt1211 - 110)
					i = anInt1211 - 110;
				if (anInt1089 != i) {
					anInt1089 = i;
					inputTaken = true;
				}
			}
		}

		if (atInventoryInterfaceType != 0) {
			atInventoryLoopCycle++;
			if (atInventoryLoopCycle >= 15) {
				if (atInventoryInterfaceType == 2)
					needDrawTabArea = true;
				if (atInventoryInterfaceType == 3)
					inputTaken = true;
				atInventoryInterfaceType = 0;
			}
		}
		
		processRightClick();
		
		if (activeInterfaceType != 0) {
			anInt989++;
			if (super.clickMode2 == 0) {
				if (activeInterfaceType == 2)
					needDrawTabArea = true;
				if (activeInterfaceType == 3)
					inputTaken = true;
				activeInterfaceType = 0;
				if (aBoolean1242) {
					lastActiveInvInterface = -1;
					processRightClick();
					int x = isFixed() ? 0 : (clientWidth / 2) - 356;
					int y = isFixed() ? 0 : (clientHeight / 2) - 267;
					/* Checking for adding to bank tab */
					if (anInt1084 == 5382 && super.mouseY >= 40 + y && super.mouseY <= 77 + y) {// check
						// if
						// bank
						// interface
						if (super.mouseX >= 28 + x && super.mouseX <= 74 + x) {// tab
							// 1
							stream.sendPacket(214);
							stream.method433(5);// 5 = maintab
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 75 + x && super.mouseX <= 121 + x) {// tab
							// 1
							stream.sendPacket(214);
							stream.method433(13);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 122 + x && super.mouseX <= 168 + x) {// tab
							// 2
							stream.sendPacket(214);
							stream.method433(26);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 169 + x && super.mouseX <= 215 + x) {// tab
							// 3
							stream.sendPacket(214);
							stream.method433(39);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 216 + x && super.mouseX <= 262 + x) {// tab
							// 4
							stream.sendPacket(214);
							stream.method433(52);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 263 + x && super.mouseX <= 309 + x) {// tab
							// 5
							stream.sendPacket(214);
							stream.method433(65);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 310 + x && super.mouseX <= 356 + x) {// tab
							// 6
							stream.sendPacket(214);
							stream.method433(78);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 357 + x && super.mouseX <= 403 + x) {// tab
							// 7
							stream.sendPacket(214);
							stream.method433(91);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
						if (super.mouseX >= 404 + x && super.mouseX <= 450 + x) {// tab
							// 8
							stream.sendPacket(214);
							stream.method433(104);// tab # x 13 (originally
							// movewindow)
							stream.method424(0);
							stream.method433(anInt1085);// Selected item slot
							stream.method431(mouseInvInterfaceIndex);// unused

						}
					}

					if (lastActiveInvInterface == anInt1084 && mouseInvInterfaceIndex != anInt1085) {
						RSInterface class9 = RSInterface.interfaceCache[anInt1084];
						int j1 = 0;
						if (anInt913 == 1 && class9.contentType == 206)
							j1 = 1;
						if (class9.inv[mouseInvInterfaceIndex] <= 0)
							j1 = 0;
						if (class9.deletesTargetSlot) {
							int l2 = anInt1085;
							int l3 = mouseInvInterfaceIndex;
							class9.inv[l3] = class9.inv[l2];
							class9.invStackSizes[l3] = class9.invStackSizes[l2];
							class9.inv[l2] = -1;
							class9.invStackSizes[l2] = 0;
						} else if (j1 == 1) {
							int i3 = anInt1085;
							for (int i4 = mouseInvInterfaceIndex; i3 != i4; )
								if (i3 > i4) {
									class9.swapInventoryItems(i3, i3 - 1);
									i3--;
								} else if (i3 < i4) {
									class9.swapInventoryItems(i3, i3 + 1);
									i3++;
								}

						} else {
							class9.swapInventoryItems(anInt1085, mouseInvInterfaceIndex);
						}
						stream.sendPacket(214);
						stream.method433(anInt1084);
						stream.method424(j1);
						stream.method433(anInt1085);
						stream.method431(mouseInvInterfaceIndex);
					} else if (lastActiveInvInterface != anInt1084) {
						stream.sendPacket(216);
						stream.writeWord(anInt1084);
						stream.writeWord(lastActiveInvInterface);
						stream.writeWord(anInt1085);
						stream.writeWord(mouseInvInterfaceIndex);
					}
				} else if ((anInt1253 == 1 || MenuManager.optionLowPriority(MenuManager.menuActionRow - 1)) && MenuManager.menuActionRow > 2)
					determineMenuSize();
				else if (MenuManager.menuActionRow > 0) {
					MiniMenuEntry.executeEntry();
				}
				atInventoryLoopCycle = 10;
				super.clickMode3 = 0;
			} else if (anInt989 >= clickSpeed && (super.mouseX > anInt1087 + 5 || super.mouseX < anInt1087 - 5 || super.mouseY > anInt1088 + 5
					|| super.mouseY < anInt1088 - 5))
				aBoolean1242 = true;
		}
		if (WorldController.anInt470 != -1) {
			int k = WorldController.anInt470;
			int k1 = WorldController.anInt471;
			if (myPrivilege >= 2 && holdingCtrlKey && holdingShiftKey) {
				teleport(k, k1);
			} else {
				boolean flag = doWalkTo(0,  k1, k);
				WorldController.anInt470 = -1;
				if (flag) {
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 1;
					crossIndex = 0;
				}
			}

			if (GraphicsDisplay.enabled) {
				WorldController.aBoolean467 = false;
			}
		}
		if (super.clickMode3 == 1 && aString844 != null) {
			aString844 = null;
			inputTaken = true;
			super.clickMode3 = 0;
		}

		MenuManager.sort();

		if (!processMenuClick()) {
			processMinimap();
			processChatButtons();
			if (orbsEnabled) {
				processMinimapOrbs();
			}
		}
		if (loadingStage == 2) {
			Camera.method108();
			if (aBoolean1160) {
				calcCameraPos();
			}
		}

		for (int i1 = 0; i1 < 5; i1++)
			anIntArray1030[i1]++;

		method73();
		super.idleTime++;
		if (super.idleTime > 25000) {
			anInt1011 = 250;
			super.idleTime -= 500;
			stream.sendPacket(202);
		}
		anInt1010++;
		if (anInt1010 > 50)
			stream.sendPacket(0);
		try {
			if (gameSocket != null && stream.currentOffset > 0) {
				gameSocket.write(stream.buffer, 0, stream.currentOffset);
				stream.currentOffset = 0;
				anInt1010 = 0;
			}
		} catch (IOException _ex) {
			dropClient();
			_ex.printStackTrace();
		} catch (Exception exception) {
			exception.printStackTrace();
			resetLogout();
		}
	}

	private void teleport(int x, int y) {
		String inputString = "::tele " + (regionBaseX + x) + " " + (y + regionBaseY);
		stream.sendPacket(103);
		stream.writeByte(inputString.length() - 1);
		stream.writeString(inputString.substring(2));
	}

	public void checkHax() {
		stream.sendPacket(237); // magic on items
		stream.writeWord(856); // slot
		stream.write3Bytes(645); // itemid
		stream.writeWord(1); // junk
		stream.method432(1155);// spellid
	}

	public void method63() {
		GameObject gameObject = (GameObject) aClass19_1179.reverseGetFirst();
		for (; gameObject != null; gameObject = (GameObject) aClass19_1179.reverseGetNext())
			if (gameObject.removeTime == -1) {
				gameObject.spawnTime = 0;
				method89(gameObject);
			} else {
				gameObject.unlink();
			}

	}

	public int rememberMe = 0;

	public int loginButtonint;

	public int rememberMehover;

	public int textbox;

	public int textbox1;

	public static boolean doneLoading = false;

	public void handleScroller(int startX, int startY, int mouseX, int mouseY, RSInterface class9, int height, int maxScroll, boolean tabArea) {
		int anInt992;
		if (aBoolean972)
			anInt992 = 32;
		else
			anInt992 = 0;
		aBoolean972 = false;

		if (super.clickMode2 == 1) {
			if (mouseX >= startX && mouseX < startX + 16 && mouseY >= startY && mouseY < startY + 16) {
				class9.scrollPosition -= 4;
				if (tabArea) {
					needDrawTabArea = true;
				}
			} else if (mouseX >= startX && mouseX < startX + 16 && mouseY >= (startY + height) - 16 && mouseY < startY + height) {
				class9.scrollPosition += 4;
				if (tabArea) {
					needDrawTabArea = true;
				}
			} else if (mouseX >= startX - anInt992 && mouseX < startX + 16 + anInt992 && mouseY >= startY + 16 && mouseY < (startY + height) - 16) {
				int scrollerSize = ((height - 32) * height) / maxScroll;
				if (scrollerSize < 8) {
					scrollerSize = 8;
				}

				int mouseInsideScrollY = mouseY - startY - 16 - scrollerSize / 2;
				int j2 = height - 32 - scrollerSize;
				class9.scrollPosition = ((maxScroll - height) * mouseInsideScrollY) / j2;
				if (tabArea)
					needDrawTabArea = true;
				aBoolean972 = true;
			}
		} else if(notchesPoll != 0) {
			int i2 = startX - class9.width;
			if (!tabArea || (mouseX >= i2 && mouseY >= startY
					&& mouseX < i2 + class9.width && mouseY < startY + class9.height)) {
				class9.scrollPosition += notchesPoll * 30;

				if (tabArea) {
					needDrawTabArea = true;
				}
			}
		}
	}

	// private boolean method66(int i, int j, int k, int id) {
	//// int i1 = i >> 14 & 0x7fff;
	// int j1 = worldController.method304(plane, k, j, i);
	// if (j1 == -1)
	// return false;
	// int k1 = j1 & 0x1f;
	// int l1 = j1 >> 6 & 3;
	// if (k1 == 10 || k1 == 11 || k1 == 22) {
	// com.soulplayps.client.ObjectDef class46 =
	// com.soulplayps.client.ObjectDef.forID(/*i1*/id);
	// int i2;
	// int j2;
	// if (l1 == 0 || l1 == 2) {
	// i2 = class46.anInt744;
	// j2 = class46.anInt761;
	// } else {
	// i2 = class46.anInt761;
	// j2 = class46.anInt744;
	// }
	// int k2 = class46.anInt768;
	// if (l1 != 0)
	// k2 = (k2 << l1 & 0xf) + (k2 >> 4 - l1);
	// doWalkTo(2, 0, j2, 0, myPlayer.smallY[0], i2, k2, j,
	// myPlayer.smallX[0], false, k);
	// } else {
	// doWalkTo(2, l1, 0, k1 + 1, myPlayer.smallY[0], 0, 0, j,
	// myPlayer.smallX[0], false, k);
	// }
	// crossX = super.saveClickX;
	// crossY = super.saveClickY;
	// crossType = 2;
	// crossIndex = 0;
	// return true;
	// }

	private void method66(int j, int k) {
		doWalkTo(2, j, k);
		crossX = super.saveClickX;
		crossY = super.saveClickY;
		crossType = 2;
		crossIndex = 0;
	}

	private void connectServer() {
		expectedCRCs[8] = 0;

		int retrySeconds = 5;
		int retryTimes = 0;
		while (expectedCRCs[8] == 0) {
			String error = "Unknown problem";
			drawLoadingBar(20, "Connecting to web server");
			try {
				RSBuffer buffer = new RSBuffer(new byte[44]);

				DataInputStream inputStream = openJagGrabInputStream(
						"crc" + (int) (Math.random() * 99999999D) + "-" + Config.CLIENT_VERSION);
				try {
					inputStream.readFully(buffer.buffer, 0, 44);
				} finally {
					inputStream.close();
				}

				//Skip the file size, we already know it
				buffer.readDWord();

				for (int index = 0; index < 9; index++) {
					expectedCRCs[index] = buffer.readDWord();
				}

				int expectedHash = buffer.readDWord();
				int hash = 1234;
				for (int index = 0; index < 9; index++) {
					hash = (hash << 1) + expectedCRCs[index];
				}

				if (expectedHash != hash) {
					error = "checksum problem";
					expectedCRCs[8] = 0;
				}
			} catch (EOFException _ex) {
				_ex.printStackTrace();
				error = "EOF problem";
				expectedCRCs[8] = 0;
			} catch (IOException _ex) {
				_ex.printStackTrace();
				error = "connection problem";
				expectedCRCs[8] = 0;
			} catch (Exception _ex) {
				_ex.printStackTrace();
				error = "logic problem";
				expectedCRCs[8] = 0;
			}

			if (expectedCRCs[8] == 0) {
				retryTimes++;
				for (int second = retrySeconds; second > 0; second--) {
					if (retryTimes >= 10) {
						drawLoadingBar(10, "Game updated - please reload page");
						second = 10;
					} else {
						drawLoadingBar(10, error + " - Will retry in " + second + " secs.");
					}
					try {
						Thread.sleep(1000L);
					} catch (Exception _ex) {
					}
				}

				retrySeconds *= 2;
				if (retrySeconds > 60) {
					retrySeconds = 60;
				}
			}
		}
	}

	private RSArchive loadArchive(int fileIndex, String niceName, String protocolName, int wantedCrc, int loadPercent) {
		byte fileBuffer[] = null;

		try {
			if (fileStores[0] != null) {
				fileBuffer = fileStores[0].readFile(fileIndex);
			}
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}

		if (Config.USE_UPDATE_SERVER && fileBuffer != null) {
			crc32.reset();
			crc32.update(fileBuffer);
			int gotCrc = (int) crc32.getValue();
			if (gotCrc != wantedCrc) {
				fileBuffer = null;
			}
		}

		if (fileBuffer != null) {
			return new RSArchive(fileBuffer);
		}

		int retrySeconds = 5;
		int retryTimes = 0;
		while (fileBuffer == null) {
			String error = "Unknown error";
			drawLoadingBar(loadPercent, "Requesting " + niceName);
			try {
				final DataInputStream inputStream = openJagGrabInputStream(protocolName + wantedCrc);
				try {
					byte headerBuffer[] = new byte[4];
					inputStream.readFully(headerBuffer, 0, 4);
					RSBuffer stream = new RSBuffer(headerBuffer);
					int fileSize = stream.readDWord();
					int bufferOffset = 0;
					fileBuffer = new byte[fileSize];

					int oldPercent = 0;
					while (bufferOffset < fileSize) {
						int readLength = fileSize - bufferOffset;
						if (readLength > 1000) {
							readLength = 1000;
						}

						int read = inputStream.read(fileBuffer, bufferOffset, readLength);
						if (read < 0) {
							error = "Length error: " + bufferOffset + "/" + fileSize;
							throw new IOException("EOF");
						}

						bufferOffset += read;
						int percent = (bufferOffset * 100) / fileSize;
						if (percent != oldPercent) {
							drawLoadingBar(loadPercent, "Loading " + niceName + " - " + percent + "%");
						}

						oldPercent = percent;
					}
				} finally {
					inputStream.close();
				}

				try {
					if (fileStores[0] != null) {
						fileSystemWorker.requestWrite(fileStores[0], fileBuffer, fileIndex);
					}
				} catch (Exception _ex) {
					fileStores[0] = null;
				}

				if (fileBuffer != null) {
					crc32.reset();
					crc32.update(fileBuffer);
					int gotCrc = (int) crc32.getValue();
					if (gotCrc != wantedCrc) {
						fileBuffer = null;
						retryTimes++;
						error = "Checksum error: " + gotCrc;
					}
				}
			} catch (IOException ioexception) {
				if (error.equals("Unknown error"))
					error = "Connection error";
				fileBuffer = null;
			} catch (NullPointerException _ex) {
				error = "Null error";
				fileBuffer = null;
			} catch (ArrayIndexOutOfBoundsException _ex) {
				error = "Bounds error";
				fileBuffer = null;
			} catch (Exception _ex) {
				error = "Unexpected error";
				fileBuffer = null;
			}

			if (fileBuffer == null) {
				for (int second = retrySeconds; second > 0; second--) {
					if (retryTimes >= 3) {
						drawLoadingBar(loadPercent, "Game updated - please reload page");
						second = 10;
					} else {
						drawLoadingBar(loadPercent, error + " - Retrying in " + second);
					}
					try {
						Thread.sleep(1000L);
					} catch (Exception _ex) {
						_ex.printStackTrace();
					}
				}

				retrySeconds *= 2;
				if (retrySeconds > 60) {
					retrySeconds = 60;
				}
			}
		}

		return new RSArchive(fileBuffer);
	}

	public static void updateDailyTasksState() {
		int value = Client.variousSettings[ConfigCodes.TASKS_LOCKED];
		if (value == 1) {
			RSInterface.interfaceCache[66241].hidden = false;
			RSInterface.interfaceCache[66242].hidden = false;
		} else {
			RSInterface.interfaceCache[66241].hidden = true;
			RSInterface.interfaceCache[66242].hidden = true;
		}
	}

	public void sendStringAsLong(String string) {
		stream.sendPacket(60);
		stream.writeQWord(TextClass.longForName(string));
	}
	
	public void sendString(int identifier, String text) {
		stream.sendPacket(127);
		stream.writeByte(0);
		int pos = stream.currentOffset;

		stream.writeByte(identifier);
		stream.writeString(text);

		stream.writeBytes(stream.currentOffset - pos);
	}
	
	public void dropClient() {
		if (anInt1011 > 0) {
			System.out.println("resettinglogout");
			resetLogout();
			return;
		}
		
		setTextOnScren("Connection lost", "Please wait - attempting to reestablish.");
		minimapBlackout = 0;
		destX = 0;
		swapToFullscreenPixmap();
		SocketImpl rsSocket = gameSocket;
		loggedIn = false;
		loginFailures = 0;
		login(myUsername, myPassword, true);
		if (!loggedIn)
			resetLogout();
		try {
			rsSocket.close();
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
	}
	
	public void doAction(int l, long i12, int j, int k, String h) {
		if (getInputDialogState() != 0 && getInputDialogState() != SEARCH_STATE) {
			setInputDialogState(0);
			inputTaken = true;
		}

		int i1 = (int) i12;

		if (l >= 2000)
			l -= 2000;
		// System.out.println("MenuActionButton: "+l);
		switch (l) {

			case 626:
				RSInterface class9_1 = RSInterface.interfaceCache[k];
				targetInterfaceId = k;
				targetMask = class9_1.targetMask;
				itemSelected = 0;
				needDrawTabArea = true;
				String verbPrefix = class9_1.targetVerb;
				if (verbPrefix.indexOf(" ") != -1)
					verbPrefix = verbPrefix.substring(0, verbPrefix.indexOf(" "));
				String verbSuffix = class9_1.targetVerb;
				if (verbSuffix.indexOf(" ") != -1)
					verbSuffix = verbSuffix.substring(verbSuffix.indexOf(" ") + 1);
				spellTooltip = verbPrefix + " " + class9_1.targetName + " " + verbSuffix;
				if (targetMask.canUseOnInventory()) {
					needDrawTabArea = true;
					tabID = 3;
				}
				return;
			case 447:
				itemSelected = 1;
				anInt1283 = j;
				anInt1284 = k;
				anInt1285 = i1;
				selectedItemName = ItemDef.forID(i1).name;
				targetInterfaceId = -1;
				targetMask = null;
				needDrawTabArea = true;
				return;
			case 244:
				boolean flag7 = doWalkTo(2,  k,  j);
				if (!flag7)
					flag7 = doWalkTo(2,  k,  j);
				crossX = super.saveClickX;
				crossY = super.saveClickY;
				crossType = 2;
				crossIndex = 0;
				stream.sendPacket(253);
				stream.method431(j + regionBaseX);
				stream.method433(k + regionBaseY);
				stream.write3Bytes(i1);
				break;
			case 1448:
				ItemDef itemDef_1 = ItemDef.forID(i1);
				String s6;
				if (itemDef_1.description != null)
					s6 = new String(itemDef_1.description);
				else
					s6 = "It's a " + itemDef_1.name + ".";
				pushGameMessage(s6);
				break;
			case 78:
				stream.sendPacket(117);
				stream.write3Bytes(k);
				stream.write3Bytes(i1);
				stream.method431(j);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 54:
				stream.sendPacket(135); // bank x 1 packet, basically send them what
				// item clicked
				stream.method431(j);
				stream.write3Bytes(k);
				stream.write3Bytes(i1);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
	
				stream.sendPacket(208);
				stream.writeQWord(prevXAmount);
				break;
			case 27:
				Player class30_sub2_sub4_sub1_sub2_2 = playerArray[i1];
				if (class30_sub2_sub4_sub1_sub2_2 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub2_2.smallY[0],
							 class30_sub2_sub4_sub1_sub2_2.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(73);
					stream.method431(i1);
				}
				break;
			case 213:
				boolean flag3 = doWalkTo(2,   k,  j);
				if (!flag3)
					flag3 = doWalkTo(2,   k,  j);
				crossX = super.saveClickX;
				crossY = super.saveClickY;
				crossType = 2;
				crossIndex = 0;
				stream.sendPacket(79);
				stream.method431(k + regionBaseY);
				stream.write3Bytes(i1);
				stream.method432(j + regionBaseX);
				break;
			case 632:
				stream.sendPacket(145);
				stream.write3Bytes(k);
				stream.method432(j);
				stream.write3Bytes(i1);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 1018:
			case 1026:
			case 1020:
			case 1021:
			case 1027:// Summoning orb buttons
				sendClickedButton(l);
				break;
			case 1003: // clan off
				clanChatMode = 2;
				inputTaken = true;
				break;
			case 1002: // clan friends
				clanChatMode = 1;
				inputTaken = true;
				break;
			case 1001: // clan on
				clanChatMode = 0;
				inputTaken = true;
				break;
			case 1000: // clan regular click
				cButtonCPos = 4;
				chatTypeView = 11;
				inputTaken = true;
				break;
			case 1234:
				sendClickedButton(65534);
				break;
			case 999:
				cButtonCPos = 0;
				chatTypeView = 0;
				inputTaken = true;
				break;
			case 998:
				switch (i1) {
				case 0:
					cButtonCPos = 1;
					chatTypeView = 5;
					break;
				case 1:
					filterGameSpam = false;
					break;
				case 2:
					filterGameSpam = true;
					break;
				}
				inputTaken = true;
				break;
			case 808: // autochat
				publicChatMode = 0;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				showAutoChat = true;
				break;
			case 997:
				publicChatMode = 3;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				showAutoChat = false;
				break;
			case 996:
				publicChatMode = 2;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				showAutoChat = false;
				break;
			case 995:
				publicChatMode = 1;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				showAutoChat = false;
				break;
			case 994:
				publicChatMode = 0;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				showAutoChat = false;
				break;
			case 993:
				cButtonCPos = 2;
				chatTypeView = 1;
				inputTaken = true;
				break;
			case 992:
				privateChatMode = 2;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				break;
			case 991:
				privateChatMode = 1;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				break;
			case 990:
				privateChatMode = 0;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				break;
			case 989:
				cButtonCPos = 3;
				chatTypeView = 2;
				inputTaken = true;
				break;
			case 988:
				ChatMessageManager.clearGroup(ChatMessage.PRIVATE_MESSAGE_RECEIVED);
				ChatMessageManager.clearGroup(ChatMessage.PRIVATE_MESSAGE_RECEIVED_PLAYER);
				ChatMessageManager.clearGroup(ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF);
				ChatMessageManager.clearGroup(ChatMessage.PRIVATE_MESSAGE_SENT);
				break;
			case 987:
				tradeMode = 2;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				break;
			case 986:
				tradeMode = 1;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				break;
			case 985:
				tradeMode = 0;
				inputTaken = true;
				stream.sendPacket(95);
				stream.writeByte(publicChatMode);
				stream.writeByte(privateChatMode);
				stream.writeByte(tradeMode);
				break;
			case 984:
				cButtonCPos = 5;
				chatTypeView = 3;
				inputTaken = true;
				break;
			case 983:
				duelMode = 2;
				inputTaken = true;
				break;
			case 982:
				duelMode = 1;
				inputTaken = true;
				break;
			case 981:
				duelMode = 0;
				inputTaken = true;
				break;
			case 980:
				cButtonCPos = 6;
				chatTypeView = 4;
				inputTaken = true;
				break;
			case 1008: // view yells only
				chatTypeView = 12;
				cButtonCPos = 6;
				inputTaken = true;
				break;
			case 1009: // yells : ON
				yellMode = 0;
				inputTaken = true;
				break;
			case 1010: // yells : Friends
				yellMode = 1;
				inputTaken = true;
				break;
			case 1011: // yells : OFF
				yellMode = 2;
				inputTaken = true;
				break;
			case 493:
				stream.sendPacket(75);
				stream.method433(k);
				stream.method431(j);
				stream.write3Bytes(i1);
				// System.out.println("K:" + k + " j:" + j + " i1:" + i1);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 652:
				boolean flag4 = doWalkTo(2,   k,  j);
				if (!flag4)
					flag4 = doWalkTo(2,   k,  j);
				crossX = super.saveClickX;
				crossY = super.saveClickY;
				crossType = 2;
				crossIndex = 0;
				stream.sendPacket(156);
				stream.method432(j + regionBaseX);
				stream.method431(k + regionBaseY);
				stream.write3Bytes(i1);
				break;
			case 94:
				boolean flag5 = doWalkTo(2,   k,  j);
				if (!flag5)
					flag5 = doWalkTo(2,   k,  j);
				crossX = super.saveClickX;
				crossY = super.saveClickY;
				crossType = 2;
				crossIndex = 0;
				stream.sendPacket(181);
				stream.method431(k + regionBaseY);
				stream.write3Bytes(i1);
				stream.method431(j + regionBaseX);
				stream.method432(targetInterfaceId);
				break;
			case 647:
				sendOptionClick(k, j, i1);
				switch (k) {
				case 18304:
					if (j == 0) {
						inputTaken = true;
						setInputDialogState(0);
						messagePromptRaised = true;
						promptInput = "";
						friendsListAction = 8;
						aString1121 = "Enter your clan chat title";
					}
					break;
				}
				break;
			case 646:
				sendClickedButton(k);
				RSInterface class9_2 = RSInterface.interfaceCache[k];
				if (class9_2.valueIndexArray != null && class9_2.valueIndexArray[0][0] == 5) {
					int i2 = class9_2.valueIndexArray[0][1];
					if (variousSettings[i2] != class9_2.requiredValues[0]) {
						setVarp(i2, class9_2.requiredValues[0]);
					}
				}
				//.out.println("button k " + k);
				switch (k) {
				case 64031:
					sizeTogglesButton(DisplayMode.FIXED);
					break;
				case 64032:
					sizeTogglesButton(DisplayMode.RESIZEABLE);
					break;
				case 67302:
				    RSInterface.interfaceCache[67404].hidden = false;
				    RSInterface.interfaceCache[67407].hidden = false;
				    RSInterface.interfaceCache[67410].hidden = false;

				    RSInterface.interfaceCache[67320].hidden = false;
				    RSInterface.interfaceCache[67420].hidden = true;
				    RSInterface.interfaceCache[67900].hidden = true;
				    RSInterface.interfaceCache[67800].hidden = true;
				    RSInterface.interfaceCache[67802].hidden = true;
				    RSInterface.interfaceCache[67810].hidden = true;
				    RSInterface.interfaceCache[67815].hidden = true;
				    break;
				case 67303:
				    RSInterface.interfaceCache[67404].hidden = true;
				    RSInterface.interfaceCache[67407].hidden = true;
				    RSInterface.interfaceCache[67410].hidden = true;

				    RSInterface.interfaceCache[67320].hidden = true;
				    RSInterface.interfaceCache[67420].hidden = false;
				    RSInterface.interfaceCache[67900].hidden = true;
				    RSInterface.interfaceCache[67800].hidden = false;
				    break;
				case 67304:
				    RSInterface.interfaceCache[67404].hidden = true;
				    RSInterface.interfaceCache[67407].hidden = true;
				    RSInterface.interfaceCache[67410].hidden = true;

				    RSInterface.interfaceCache[67320].hidden = true;
				    RSInterface.interfaceCache[67420].hidden = true;
				    RSInterface.interfaceCache[67900].hidden = false;
				    RSInterface.interfaceCache[67800].hidden = true;
				    RSInterface.interfaceCache[67802].hidden = true;
				    RSInterface.interfaceCache[67810].hidden = true;
				    RSInterface.interfaceCache[67815].hidden = true;
				    break;
			    case 64001:
				    RSInterface.interfaceCache[64030].hidden = false;
				    RSInterface.interfaceCache[64100].hidden = true;
				    RSInterface.interfaceCache[64200].hidden = true;
				    RSInterface.interfaceCache[64300].hidden = true;
				    break;
			    case 64002:
				    RSInterface.interfaceCache[64100].hidden = false;
				    RSInterface.interfaceCache[64030].hidden = true;
				    RSInterface.interfaceCache[64200].hidden = true;
				    RSInterface.interfaceCache[64300].hidden = true;
				    break;
			    case 64003:
				    RSInterface.interfaceCache[64200].hidden = false;
				    RSInterface.interfaceCache[64030].hidden = true;
				    RSInterface.interfaceCache[64100].hidden = true;
				    RSInterface.interfaceCache[64300].hidden = true;
				    break;
			    case 64004:
				    RSInterface.interfaceCache[64300].hidden = false;
				    RSInterface.interfaceCache[64030].hidden = true;
				    RSInterface.interfaceCache[64100].hidden = true;
				    RSInterface.interfaceCache[64200].hidden = true;
				    break;
				case 22004: // bank search button
					if (getInputDialogState() != SEARCH_STATE) {
						setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, SEARCH_STATE);
					} else {
						setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, 0);
					}
					break;
	
				// clan chat
				case 18129:
					if (RSInterface.interfaceCache[18135].message.toLowerCase().contains("join")) {
						inputTaken = true;
						setInputDialogState(0);
						messagePromptRaised = true;
						promptInput = "";
						friendsListAction = 6;
						aString1121 = "Enter the name of the chat you wish to join";
					} else {
						sendString(0, "");
					}
					break;
				case 18132:
					if (openModalInterfaceId != -1) {
						return;
					}

					openModalInterface(18300);
					break;
				case 18526:
					inputTaken = true;
					setInputDialogState(0);
					messagePromptRaised = true;
					promptInput = "";
					friendsListAction = 9;
					aString1121 = "Enter a name to add";
					break;
				case 18527:
					inputTaken = true;
					setInputDialogState(0);
					messagePromptRaised = true;
					promptInput = "";
					friendsListAction = 10;
					aString1121 = "Enter a name to add";
					break;
				}
				break;
			case 225:
				Npc class30_sub2_sub4_sub1_sub1_2 = npcArray[i1];
				if (class30_sub2_sub4_sub1_sub1_2 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub1_2.smallY[0],
							 class30_sub2_sub4_sub1_sub1_2.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(17);
					stream.method433(i1);
				}
				break;
			case 965:
				Npc class30_sub2_sub4_sub1_sub1_3 = npcArray[i1];
				if (class30_sub2_sub4_sub1_sub1_3 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub1_3.smallY[0],
							 class30_sub2_sub4_sub1_sub1_3.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(21);
					stream.writeWord(i1);
				}
				break;
			case 413:
				Npc class30_sub2_sub4_sub1_sub1_4 = npcArray[i1];
				if (class30_sub2_sub4_sub1_sub1_4 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub1_4.smallY[0],
							 class30_sub2_sub4_sub1_sub1_4.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(131);
					stream.method433(i1);
					stream.method432(targetInterfaceId);
				}
				break;
			case 200:
				closeInterfacesTransmit();
				break;
			case 692:// Cape customizing option
				openModalInterface(25400);
				customizingSlot = k;
				break;
			case 693:// Cape color reset
				sendRecolorIndex(k, CAPE_COLORS[k]);
				break;
			case 1025:
				Npc class30_sub2_sub4_sub1_sub1_5 = npcArray[i1];
				if (class30_sub2_sub4_sub1_sub1_5 != null) {
					EntityDef entityDef = class30_sub2_sub4_sub1_sub1_5.desc;
					if (entityDef.childrenIDs != null)
						entityDef = entityDef.method161();
					if (entityDef != null) {
						String s9;
						if (entityDef.description != null)
							s9 = new String(entityDef.description);
						else
							s9 = "It's a " + class30_sub2_sub4_sub1_sub1_5.getName() + ".";
						if (myPrivilege == 2 || myPrivilege == 3 || myPrivilege == 8) {
							s9 += "  (id: " + entityDef.myId + ")";
						}
						pushGameMessage(s9);
					}
				}
				break;
			case 412:
				Npc class30_sub2_sub4_sub1_sub1_6 = npcArray[i1];
				if (class30_sub2_sub4_sub1_sub1_6 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub1_6.smallY[0],
							 class30_sub2_sub4_sub1_sub1_6.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(72);
					stream.method432(i1);
				}
				break;
			case 365:
				Player class30_sub2_sub4_sub1_sub2_3 = playerArray[i1];
				if (class30_sub2_sub4_sub1_sub2_3 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub2_3.smallY[0],
							 class30_sub2_sub4_sub1_sub2_3.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(249);
					stream.method432(i1);
					stream.method431(targetInterfaceId);
				}
				break;
			case 1076:
				// Swap summoning and rank change tabs.
				if (i1 == 16) {
					i1 = 15;
				} else if (i1 == 15) {
					i1 = 16;
				}

				transmitClickTab(i1);

				if (i1 == 13 && worldType == WorldType.SEASONAL) {
					break;
				}

				if (!isFixed()) {
					if (tabID == i1) {
						showTab = !showTab;
					} else {
						showTab = true;
					}
				}

				tabID = i1;
				needDrawTabArea = true;
				break;
			case 729:
				Player class30_sub2_sub4_sub1_sub2_4 = playerArray[i1];
				if (class30_sub2_sub4_sub1_sub2_4 != null) {
					doWalkTo(2, class30_sub2_sub4_sub1_sub2_4.smallY[0],
							 class30_sub2_sub4_sub1_sub2_4.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(39);
					stream.method431(i1);
				}
				break;
			case 730:
				Player class30_sub2_sub4_sub1_sub2_41 = playerArray[i1];
				if (class30_sub2_sub4_sub1_sub2_41 != null) {
					doWalkTo(2, class30_sub2_sub4_sub1_sub2_41.smallY[0],
							class30_sub2_sub4_sub1_sub2_41.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(42);
					stream.method431(i1);
				}
				break;
			case 731: {
				Player player = playerArray[i1];
				if (player != null) {
					doWalkTo(2, player.smallY[0], player.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(44);
					stream.writeWord(i1);
				}
				break;
			}
			case 577:
				Player class30_sub2_sub4_sub1_sub2_5 = playerArray[i1];
				if (class30_sub2_sub4_sub1_sub2_5 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub2_5.smallY[0],
							class30_sub2_sub4_sub1_sub2_5.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(139);
					stream.method431(i1);
				}
				break;
			case 567:
				boolean flag6 = doWalkTo(2,   k,  j);
				if (!flag6)
					flag6 = doWalkTo(2,   k,  j);
				crossX = super.saveClickX;
				crossY = super.saveClickY;
				crossType = 2;
				crossIndex = 0;
				stream.sendPacket(23);
				stream.method431(k + regionBaseY);
				stream.write3Bytes(i1);
				stream.method431(j + regionBaseX);
				break;
			case 867:
				stream.sendPacket(43);
				stream.write3Bytes(k);
				stream.write3Bytes(i1);
				stream.method432(j);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 543:
				stream.sendPacket(237);
				stream.writeWord(j);
				stream.write3Bytes(i1);
				stream.writeWord(k);
				stream.method432(targetInterfaceId);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 606: {
				String s2 = h;
				int j2 = s2.indexOf("<col=ffffff>");
				if (j2 == -1) {
					return;
				}

				String name = s2.substring(j2 + 12).trim();
				name = Strings.removeMarkup(name);
				if (openModalInterfaceId == -1) {
					closeInterfacesTransmit();
					reportAbuseInput = name;
					canMute = false;
					for (int i3 = 0; i3 < RSInterface.interfaceCache.length; i3++) {
						if (RSInterface.interfaceCache[i3] == null || RSInterface.interfaceCache[i3].contentType != 600)
							continue;
						reportAbuseInterfaceID = RSInterface.interfaceCache[i3].parentID;
						openModalInterface(reportAbuseInterfaceID);
						break;
					}
				} else {
					pushGameMessage("Please close the interface you have open before using 'report abuse'");
				}
				break;
			}
			case 491:
				Player class30_sub2_sub4_sub1_sub2_6 = playerArray[i1];
				if (class30_sub2_sub4_sub1_sub2_6 != null) {
//					doWalkTo(2,   class30_sub2_sub4_sub1_sub2_6.smallY[0],
//							 class30_sub2_sub4_sub1_sub2_6.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(14);
					stream.method432(anInt1284);
					stream.writeWord(i1);
					stream.write3Bytes(anInt1285);
					stream.method431(anInt1283);
				}
				break;
			case 639: {
				String option = h;
				int whiteIndex = option.indexOf("<col=ffffff>");
				if (whiteIndex == -1) {
					break;
				}
	
				String name = option.substring(whiteIndex + 12).trim();
				name = Strings.removeMarkup(name);
	
				long nameAsLong = TextClass.longForName(name);
	
				if ((Boolean) Settings.getSetting("auto_add_reply")) {
					int index = -1;
	
					for (int i4 = 0; i4 < friendsCount; i4++) {
						if (friendsListAsLongs[i4] == nameAsLong) {
							index = i4;
							break;
						}
					}
	
					if (index != -1 && friendsNodeIDs[index] > 0) {
						inputTaken = true;
						setInputDialogState(0);
						messagePromptRaised = true;
						promptInput = "";
						friendsListAction = 3;
						aLong953 = friendsListAsLongs[index];
						aString1121 = "Enter message to send to " + friendsList[index];
					} else {
						if (!addFriend(nameAsLong))
							pushGameMessage("That player is currently offline or cannot be added to your friends list.");
						else
							pushGameMessage("Auto added " + name + " to your friends list.");
					}
				} else {
					inputTaken = true;
					setInputDialogState(0);
					messagePromptRaised = true;
					promptInput = "";
					friendsListAction = 3;
					aLong953 = nameAsLong;
					aString1121 = "Enter message to send to " + name;
				}
				break;
			}
			case 454:
				stream.sendPacket(41);
				stream.write3Bytes(i1);
				stream.method432(j);
				stream.method432(k);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 478:
				Npc class30_sub2_sub4_sub1_sub1_7 = npcArray[i1];
				if (class30_sub2_sub4_sub1_sub1_7 != null) {
					doWalkTo(2,  class30_sub2_sub4_sub1_sub1_7.smallY[0],
							 class30_sub2_sub4_sub1_sub1_7.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(18);
					stream.method431(i1);
				}
				break;
			case 1125:
				ItemDef itemDef = ItemDef.forID(i1);
				RSInterface class9_4 = RSInterface.interfaceCache[k];
				String s5;
				if (class9_4 != null && class9_4.invStackSizes != null && class9_4.invStackSizes[j] >= 0x186a0) {
					s5 = Strings.formatNumbersWithCommas(class9_4.invStackSizes[j]) + " x " + itemDef.name;
				} else if (itemDef.description != null)
					s5 = new String(itemDef.description);
				else
					s5 = "It's a " + itemDef.name + ".";
				pushGameMessage(s5);
				break;
			case 169:
				sendClickedButton(k);
				 //System.out.println("K:"+k);
				if (k == 349 || k == 350) { // autocast buttons, do not toggle on
					// clientside
					break;
				}
				RSInterface class9_3 = RSInterface.interfaceCache[k];
				if (class9_3.valueIndexArray != null && class9_3.valueIndexArray[0][0] == 5) {
					int l2 = class9_3.valueIndexArray[0][1];
					toggleGraphicsOptionsButtons(l2);
					// System.out.println("Config:"+l2);
					setVarp(l2, 1 - variousSettings[l2]);
					switch(k) {
					case 64008:
						if (!GraphicsDisplay.canSwapModes) {
							pushGameMessage("Unfortunately, your computer doesn't support OpenGL.");
							setVarp(l2, 0);
							break;
						}
		
						toggleGl.set(true);
						break;
					}
				}
				break;
			case 1062: {
				method66(k, j);
				stream.sendPacket(228);
				stream.write3Bytes((int) ((i12 >>> 32) & 0x7fffffff));
				stream.method432(k + regionBaseY);
				stream.writeWord(j + regionBaseX);
				break;
			}
			case 62: {
				method66(k, j);
				stream.sendPacket(192);
				stream.writeWord(anInt1284);
				stream.write3Bytes((int) ((i12 >>> 32) & 0x7fffffff));
				stream.method433(k + regionBaseY);
				stream.method431(anInt1283);
				stream.method433(j + regionBaseX);
				stream.write3Bytes(anInt1285);
				break;
			}
			case 113: {
				method66(k, j);
				stream.sendPacket(70);
				stream.method431(j + regionBaseX);
				stream.writeWord(k + regionBaseY);
				stream.write3Bytes((int) ((i12 >>> 32) & 0x7fffffff));
				break;
			}
			case 872: {
				method66(k, j);
				stream.sendPacket(234);
				stream.writeWord(j + regionBaseX);
				stream.write3Bytes((int) ((i12 >>> 32) & 0x7fffffff));
				stream.writeWord(k + regionBaseY);
				break;
			}
			case 502: {
				method66(k, j);
				stream.sendPacket(132);
				stream.method433(j + regionBaseX);
				stream.write3Bytes((int) ((i12 >>> 32) & 0x7fffffff));
				stream.method432(k + regionBaseY);
				break;
			}
			case 900: {
				method66(k, j);
				stream.sendPacket(252);
				stream.write3Bytes((int) ((i12 >>> 32) & 0x7fffffff));
				stream.method431(k + regionBaseY);
				stream.method432(j + regionBaseX);
				break;
			}
			case 956: {
				method66(k, j);
				stream.sendPacket(35);
				stream.method431(j + regionBaseX);
				stream.method432(targetInterfaceId);
				stream.method432(k + regionBaseY);
				stream.write3Bytes((int) ((i12 >>> 32) & 0x7fffffff));
				break;
			}
			case 1226: {
				int id = (int) ((i12 >>> 32) & 0x7fffffff);
				ObjectDef class46 = ObjectDef.forID(id);
				String examine;
				if (class46.description != null) {
					examine = new String(class46.description);
				} else {
					examine = "It's a " + class46.name + ".";
				}
				if (myPrivilege == 2 || myPrivilege == 3 || myPrivilege == 8) {
					examine += "  (id: " + id + ")";
				}
				pushGameMessage(examine);
				break;
			}
			case 582:
				Npc npc = npcArray[i1];
				if (npc != null) {
					doWalkTo(2,  npc.smallY[0],  npc.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(57);
					stream.write3Bytes(anInt1285);
					stream.method432(i1);
					stream.method431(anInt1283);
					stream.method432(anInt1284);
				}
				break;
			case 234:
				boolean flag1 = doWalkTo(2,   k,  j);
				if (!flag1)
					flag1 = doWalkTo(2,   k,  j);
				crossX = super.saveClickX;
				crossY = super.saveClickY;
				crossType = 2;
				crossIndex = 0;
				stream.sendPacket(236);
				stream.method431(k + regionBaseY);
				stream.write3Bytes(i1);
				stream.method431(j + regionBaseX);
				break;
			case 511:
				boolean flag2 = doWalkTo(2,   k,   j);
				if (!flag2)
					flag2 = doWalkTo(2,   k,  j);
				crossX = super.saveClickX;
				crossY = super.saveClickY;
				crossType = 2;
				crossIndex = 0;
				stream.sendPacket(25);
				stream.method431(anInt1284);
				stream.write3Bytes(anInt1285);
				stream.write3Bytes(i1);
				stream.method432(k + regionBaseY);
				stream.method433(anInt1283);
				stream.writeWord(j + regionBaseX);
				break;
			case 74:
				stream.sendPacket(122);
				stream.method433(k);
				stream.method432(j);
				stream.write3Bytes(i1);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 104:
				RSInterface class9_11 = RSInterface.interfaceCache[k];
				targetInterfaceId = k;
				if (!Autocast) {
					Autocast = true;
					autocastId = class9_11.id;
					sendClickedButton(class9_11.id);
				} else if (autocastId == class9_11.id) {
					Autocast = false;
					autocastId = 0;
					sendClickedButton(6666);
				} else if (autocastId != class9_11.id) {
					Autocast = true;
					autocastId = class9_11.id;
					sendClickedButton(class9_11.id);
				}
				break;
			case 315:
				RSInterface class9 = RSInterface.interfaceCache[k];
				if (class9.onButtonClick != null) {
					class9.onButtonClick.run();
				}

				boolean flag8 = true;
				if (class9.contentType > 0)
					flag8 = promptUserForInput(class9);
				if (flag8) {
					if (k >= 46522) {
						int length = DungRewardData.values.length * 4;
						if (k <= 46522 + length) {
							int index = (k - 46522) / 4;
							DungRewardData data = DungRewardData.values[index];
							if (data.isModel()) {
								RSInterface.interfaceCache[47003].message = EntityDef.forID(data.getProductId()).name;
								RSInterface.interfaceCache[47006].disabledMediaID  = data.getProductId();
								RSInterface.interfaceCache[47006].modelZoom = data.getZoom() / 3;
								RSInterface.interfaceCache[47006].disabledAnimation = data.getAnimId();
								RSInterface.interfaceCache[47004].hidden = true;
								RSInterface.interfaceCache[47006].hidden = false;
							} else {
								RSInterface.interfaceCache[47003].message = ItemDef.forID(data.getProductId()).name;
								RSInterface.interfaceCache[47004].itemId = data.getProductId();
								RSInterface.interfaceCache[47004].hidden = false;
								RSInterface.interfaceCache[47006].hidden = true;
							}
							RSInterface.interfaceCache[47005].message = data.getDescription();
							RSInterface.interfaceCache[47014].message = Strings.formatNumbersWithCommas(data.getPrice());
							RSInterface.interfaceCache[47002].hidden = false;
							RSInterface.interfaceCache[47010].hidden = false;
							RSInterface.interfaceCache[47000].hidden = true;
							RSInterface.interfaceCache[47015].hidden = true;
						}
					}
					if (k >= 67421) {
						int length = VoteStoreData.values.length * 3;
						if (k <= 67421 + length) {
							int index = (k - 67421) / 3;
							VoteStoreData data = VoteStoreData.values[index];
							if (data.isModel()) {
								RSInterface.interfaceCache[67806].disabledMediaID  = data.getProductId();
								RSInterface.interfaceCache[67806].modelZoom = data.getZoom() / 3;
								RSInterface.interfaceCache[67806].disabledAnimation = data.getAnimId();
								RSInterface.interfaceCache[67804].hidden = true;
								RSInterface.interfaceCache[67806].hidden = false;
							} else {
								RSInterface.interfaceCache[67804].itemId = data.getProductId();
								RSInterface.interfaceCache[67804].hidden = false;
								RSInterface.interfaceCache[67806].hidden = true;
							}

							RSInterface.interfaceCache[67805].message = data.getDescription();
							RSInterface.interfaceCache[67814].message = Strings.formatNumbersWithCommas(data.getPrice());
							RSInterface.interfaceCache[67802].hidden = false;
							RSInterface.interfaceCache[67810].hidden = false;
							RSInterface.interfaceCache[67800].hidden = true;
							RSInterface.interfaceCache[67815].hidden = true;
						}	
					}

					//System.out.println("k = "+k);
					switch (k) {
						case 70652:
							boolean hide = !RSInterface.interfaceCache[70651].hidden;
							RSInterface.interfaceCache[70654].hidden = !hide;
							RSInterface.interfaceCache[70651].hidden = hide;
							if (hide) {
								RSInterface.interfaceCache[70611].message = "Shared presets";
								RSInterface.interfaceCache[70653].message = "View your presets";
							} else {
								RSInterface.interfaceCache[70611].message = "Presets";
								RSInterface.interfaceCache[70653].message = "View shared presets";
							}

							sendClickedButton(k, j);
							break;
						case 70504:
							setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, INSTANCE_INPUT);
							break;
						case 70507:
							stream.sendPacket(60);
							if (!amountOrNameInput.isEmpty()) {
								stream.writeQWord(TextClass.longForName(amountOrNameInput));
							} else {
								stream.writeQWord(TextClass.longForName("0"));
							}
							break;
					case 68503:
						RSInterface.interfaceCache[68500].hidden = true;
						break;
					case 48052:
						closeInterfacesTransmit();
						break;
					case 64035:
						if (openModalInterfaceId != -1) {
							return;
						}

						openModalInterface(48050);
						break;
					case 64005:
						setSidebarInterface(11, 64500);
						break;
					case 64301:
						if (HOT_KEYS == null) {
							break;
						}

						if (HOT_KEYS.isShowing()) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									HOT_KEYS.dispose();
								}
							});
							break;
						}
		
						if (isFullscreen()) {
							pushGameMessage("You can't set hotkeys in fullscreen mode.");
							break;
						}
		
						if (openModalInterfaceId != -1) {
							pushGameMessage("You can't open hotkeys when you have an interface open.");
							break;
						}

						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								HOT_KEYS.setVisible(true);
								HOT_KEYS.center();
								HOT_KEYS.setAlwaysOnTop(true);
							}
						});
						break;
					case 64007:
						setSidebarInterface(11, 46000);
						break;
					case 62103:
						setSidebarInterface(2, 638);
						break;
					case 62102:
						setSidebarInterface(2, 62100);
						break;
					case 62105:
						setSidebarInterface(2, 66200);
						updateDailyTasksState();
						break;
					case 5726://firend list
						setSidebarInterface(8, 5065);
						break;
					case 5076://ignore list
						setSidebarInterface(8, 5715);
						break;
					case 47011://Buy
						RSInterface.interfaceCache[47010].hidden = true;
						RSInterface.interfaceCache[47015].hidden = false;
						break;
					case 47018://No
						RSInterface.interfaceCache[47010].hidden = false;
						RSInterface.interfaceCache[47015].hidden = true;
						break;
					case 47016://Yes
						RSInterface.interfaceCache[47015].hidden = true;
						RSInterface.interfaceCache[47002].hidden = true;
						RSInterface.interfaceCache[47000].hidden = false;
						sendClickedButton(k);
						break;
					case 67811://Buy
						RSInterface.interfaceCache[67810].hidden = true;	
						RSInterface.interfaceCache[67815].hidden = false;	
						break;	
					case 67818://No
						RSInterface.interfaceCache[67810].hidden = false;	
						RSInterface.interfaceCache[67815].hidden = true;	
						break;	
					case 67816://Yes
						RSInterface.interfaceCache[67815].hidden = true;	
						RSInterface.interfaceCache[67802].hidden = true;	
						RSInterface.interfaceCache[67800].hidden = false;	
						sendClickedButton(k);	
						break;
					case 25418:
						setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, TELEPORT_SEARCH_STATE);
	
						for (int teleportLayer : teleportLayers)
							RSInterface.interfaceCache[teleportLayer].hidden = true;
	
						RSInterface.interfaceCache[29998].children = null;
						RSInterface.interfaceCache[29998].hidden = false;
						break;
					case 25406:
						sendRecolorIndex(customizingSlot, clickedColorPaletteIndex);
						break;
					case 25408:
						openModalInterface(25300);
						break;
					case 45004: // search market by item
						setInputDialogState(SEARCH_STATE);
						inputTaken = true;
						amountOrNameInput = "";
						totalItemResults = 0;
						itemResultNames = new String[100];
						itemResultIDs = new int[100];
						itemResultScrollPos = 0;
						clearMarketSearchInterface();
						sendClickedButton(k);
						break;
					case 45010: // search market by player name
						setInputDialogState(2);
						inputTaken = true;
						amountOrNameInput = "";
						break;
					case 27615:
						setSidebarInterface(4, 1644);
						break;
					case 25843:// arrow
						setSidebarInterface(11, 64000);
						break;
					case 27610:// return
						setSidebarInterface(11, 64000);
						break;
					case 27508:// hp
						setInterfaceText("OFF", 27528);
						if (toggleNewHitBar) {
							toggleNewHitBar = false;
							setInterfaceText("OFF", 27528);
							setInterfaceText("", 27524);
						} else {
							toggleNewHitBar = true;
							setInterfaceText("ON", 27524);
							setInterfaceText("", 27528);
						}
						break;
					case 29156:// achievement
						setSidebarInterface(2, 29265);
						break;
					case 29267:// Quest
						setSidebarInterface(2, 638);
						break;
					case 960: // graphics options button
						int interfaceId = 48050;
						if (openModalInterfaceId != interfaceId) {
							openModalInterface(interfaceId);
						} else {
							closeInterfaces();
						}
						break;
					default:
						sendClickedButton(k, j);
						break;
	
					}
				}
				break;
			case 561:
				Player player = playerArray[i1];
				if (player != null) {
					doWalkTo(2,   player.smallY[0],
							player.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(128);
					stream.writeWord(i1);
				}
				break;
			case 20:
				Npc class30_sub2_sub4_sub1_sub1_1 = npcArray[i1];
				if (class30_sub2_sub4_sub1_sub1_1 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub1_1.smallY[0],
							 class30_sub2_sub4_sub1_sub1_1.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(155);
					stream.method431(i1);
				}
				break;
			case 779:
				Player class30_sub2_sub4_sub1_sub2_1 = playerArray[i1];
				if (class30_sub2_sub4_sub1_sub2_1 != null) {
					doWalkTo(2,   class30_sub2_sub4_sub1_sub2_1.smallY[0],
							 class30_sub2_sub4_sub1_sub2_1.smallX[0]);
					crossX = super.saveClickX;
					crossY = super.saveClickY;
					crossType = 2;
					crossIndex = 0;
					stream.sendPacket(153);
					stream.method431(i1);
				}
				break;
			case 516:
				worldController.method312(j, k);
				break;
			case 679:
				if (!aBoolean1149) {
					stream.sendPacket(40);
					stream.writeWord(k);
					aBoolean1149 = true;
				}
				break;
			case 431:
				stream.sendPacket(129);
				stream.method432(j);
				stream.write3Bytes(k);
				stream.write3Bytes(i1);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 432:
				stream.sendPacket(129);
				stream.method432(j); // slot
				stream.write3Bytes(k); // interface
				stream.write3Bytes(770/*i1*/); // itemId
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 42:
			case 792:
			case 322:
			case 337: {
				String s = h;
				int k1 = s.indexOf("<col=ffffff>");
				if (k1 == -1) {
					break;
				}

				String name = s.substring(k1 + 12).trim();
				name = Strings.removeMarkup(name);
				long l3 = TextClass.longForName(name);
				if (l == 337) {
					addFriend(l3);
				} else if (l == 42) {
					addIgnore(l3);
				} else if (l == 792) {
					delFriend(l3);
				} else if (l == 322) {
					delIgnore(l3);
				}
				break;
			}
			case 53:
				stream.sendPacket(135);
				stream.method431(j);
				stream.write3Bytes(k);
				stream.write3Bytes(i1);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
	
				if (openModalInterfaceId != 44000 && openModalInterfaceId != 67100)
					setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, 1);
				break;
			case 539:
				stream.sendPacket(16);
				stream.write3Bytes(i1);
				stream.method433(j);
				stream.method433(k);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 6:
			case 486:
			case 485:
			case 484:
				String s1 = h;
				int l1 = s1.indexOf("<col=ffffff>");
				if (l1 == -1) {
					break;
				}

				s1 = s1.substring(l1 + 12).trim();
				String s7 = TextClass.fixName(TextClass.nameForLong(TextClass.longForName(s1)));
				boolean flag9 = false;
				for (int j3 = 0; j3 < playerCount; j3++) {
					Player class30_sub2_sub4_sub1_sub2_7 = playerArray[playerIndices[j3]];
					if (class30_sub2_sub4_sub1_sub2_7 == null || class30_sub2_sub4_sub1_sub2_7.name == null
							|| !class30_sub2_sub4_sub1_sub2_7.name.equalsIgnoreCase(s7))
						continue;
					doWalkTo(2,   class30_sub2_sub4_sub1_sub2_7.smallY[0],
							 class30_sub2_sub4_sub1_sub2_7.smallX[0]);
					switch (l) {
						case 484:
							stream.sendPacket(39);
							stream.method431(playerIndices[j3]);
							break;
						case 6:
							stream.sendPacket(128);
							stream.writeWord(playerIndices[j3]);
							break;
						case 485:
							stream.sendPacket(42);
							stream.method431(playerIndices[j3]);
							break;
						case 486:
							stream.sendPacket(44);
							stream.writeWord(playerIndices[j3]);
							break;
					}
					flag9 = true;
					break;
				}

				if (!flag9)
					pushGameMessage("Unable to find " + s7);
				break;
			case 870:
				stream.sendPacket(53);
				stream.writeWord(j);
				stream.method432(anInt1283);
				stream.write3Bytes(i1);
				stream.writeWord(anInt1284);
				stream.write3Bytes(anInt1285);
				stream.writeWord(k);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 847:
				stream.sendPacket(87);
				stream.write3Bytes(i1);
				stream.writeWord(k);
				stream.method432(j);
				atInventoryLoopCycle = 0;
				atInventoryInterface = k;
				atInventoryIndex = j;
				atInventoryInterfaceType = 2;
				if (RSInterface.interfaceCache[k].parentID == openModalInterfaceId)
					atInventoryInterfaceType = 1;
				if (RSInterface.interfaceCache[k].parentID == backDialogID)
					atInventoryInterfaceType = 3;
				break;
			case 696:
				viewRotation = 0;
				anInt1184 = 343;
				Client.refreshGLMinimap = true;
				break;
			case 1252:// chat interface close button
				setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, 0);
				break;
			case 1505:
				sendClickedButton(5001);
				break;
			case 2000:
				moneyPouchClicked = !moneyPouchClicked;
				break;

			case 2004: // click world map
				sendClickedButton(5005);
				break;

			case 2001:
				sendClickedButton(5002);
				break;
			case 2002:
				sendClickedButton(5003);
				break;
			case 2003:
				sendClickedButton(5004);
				break;
			case 1250: // select searched item
				stream.sendPacket(122);
				stream.method433(789); // interface ID
				stream.method432(0); // item slot
				stream.write3Bytes(i1); // item id
				break;
			case 1251:// admin spawn right click button on search
				stream.sendPacket(122);
				stream.method433(789); // interface ID
				stream.method432(1); // item slot
				stream.write3Bytes(i1); // item id
				break;
			case 1508:
				orbTextEnabled[0] = !orbTextEnabled[0];
				break;
			case 1509:
				orbTextEnabled[1] = !orbTextEnabled[1];
				break;
			case 1510:
				orbTextEnabled[2] = !orbTextEnabled[2];
				break;
			case 1511:
				orbTextEnabled[3] = !orbTextEnabled[3];
				break;
			case 1500:// Toggle quick prayers prayClicked = !prayClicked;
				sendClickedButton(5000);
				break;
			case 1506: // Select quick prayers
				sendClickedButton(6001);
				break;
			case 1050:
				sendClickedButton(152);
				break;
			case 1501:
				sendClickedButton(153);
				break;
			case 1503:
				xpClicked = !xpClicked;
				drawXpBar = !xpClicked;
				break;
			case 1504:
				xpToDrawOnBar = 0;
				break;
		}

		itemSelected = 0;
		targetInterfaceId = -1;
		targetMask = null;
		needDrawTabArea = true;
	}
	
	public static boolean inOsrsRegion;
	
	public static int objectDefsRegion = 474;
	
	public static boolean osrsRegions(int regionId) {
		switch (regionId) {
			case 7227:
			case 7228:
			case 7483:
			case 7484:
			case 4919:
			case 13137:
			case 9043:
			case 4883:
			//Raids
			case 1799:
			case 1800:
			case 2311:
			case 2312:
			case 2055:
			case 2056:
			//Inferno
			case 2059:
				
				return true;
				
			default:
				return false;
		}
	}
	
	public static boolean osrsMapIn317(int regionId) {
		switch (regionId) {
			case 10547: // Ardougne rooftop

			case 13617: //lms lobby
			case 13361: //lms lobby

			//wildy
			//case 11831:
			case 11832:
			case 11833:
			case 11834:
			case 11835:
			case 11836:
			case 11837:
			//case 12087:
			case 12088:
			case 12089:
			case 12090:
			case 12091:
			case 12092:
			case 12093:
			//case 12343:
			case 12344:
			case 12345:
			case 12346:
			case 12347:
			case 12348:
			case 12349:
			//case 12599:
			case 12600:
			case 12601:
			case 12602:
			case 12603:
			case 12604:
			case 12605:
			//case 12855:
			case 12856:
			case 12857:
			case 12858:
			case 12859:
			case 12860:
			case 12861:
			//case 13111:
			case 13112:
			case 13113:
			case 13114:
			case 13115:
			case 13116:
			case 13117:
			//case 13367:
			case 13368:
			case 13369:
			case 13370:
			case 13371:
			case 13372:
			case 13373:
				return true;

			default:
				return false;
		}
	}
	
	public static void setDefsRegion(int regionId) {
		switch (regionId) {
			case 13625:
			case 13626:
			case 13881:
			case 13882:
			case 14389:
			// Living rock caverns
			case 14415:
			case 14416:
			case 14671:
			case 14672:

			// Gamble zone
			case 5444:
			case 5445:
			case 5700:
			case 5701:

			// Dungeoneering
			case 3855:
			case 3856:
			case 3857:
			case 4111:
			case 4112:
			case 4113:
			case 4367:
			case 4368:
			case 4369:
				objectDefsRegion = 667;
				break;

			default:
				objectDefsRegion = 474;
				break;
		}
	}
	
	public void drawOnBankInterface() {
		// try {
		if (openModalInterfaceId == 5292 && RSInterface.interfaceCache[27000].message.equals("1")) {// Sent
			// on
			// bank
			// opening
			// etc,
			// refresh
			// tabs
			int tabs = Integer.parseInt(RSInterface.interfaceCache[27001].message);// #
			// of
			// tabs
			// used
			int tab = Integer.parseInt(RSInterface.interfaceCache[27002].message);// current
			// tab
			// selected
			// System.out.println("Doing tabs up");
			for (int i = 0; i <= tabs; i++) {
				RSInterface.interfaceCache[22025 + i].disabledSprite = bankTabs[3];// new
				// com.soulplayps.client.Sprite("Bank/TAB
				// 3");
				RSInterface.interfaceCache[22025 + i].tooltip = "Click here to select tab " + (i + 1);
			}
			for (int i = tabs + 1; i <= 8; i++) {
				RSInterface.interfaceCache[22024 + i].disabledSprite = null;// =
				// new
				// com.soulplayps.client.Sprite("");
				RSInterface.interfaceCache[22024 + i].tooltip = "";
			}
			if (tabs != 8) {
				RSInterface.interfaceCache[22025 + tabs].disabledSprite = bankTabs[4];// new
				// com.soulplayps.client.Sprite("Bank/TAB
				// 4");
				RSInterface.interfaceCache[22025 + tabs].tooltip = "Drag an item here to create a new tab";
			}
			// System.out.println(tab);
			if (tab == -1)// searching
				RSInterface.interfaceCache[22024].disabledSprite = bankTabs[1];// new
				// com.soulplayps.client.Sprite("Bank/TAB
				// 1");
			else if (tab > 0) {
				RSInterface.interfaceCache[22024 + tab].disabledSprite = bankTabs[2];// new
				// com.soulplayps.client.Sprite("Bank/TAB
				// 2");
				RSInterface.interfaceCache[22024].disabledSprite = bankTabs[1];// new
				// com.soulplayps.client.Sprite("Bank/TAB
				// 1");
			} else
				RSInterface.interfaceCache[22024].disabledSprite = bankTabs[0];// new
			// com.soulplayps.client.Sprite("Bank/TAB
			// 0");
			// TODO: Highlight first item in new tab... (NOT MAIN) ADD BELOW TO
			// RSINTERFACE
			// com.soulplayps.client.RSInterface.interfaceCache[22043].sprite1 =
			// new com.soulplayps.client.Sprite("");
			
			RSInterface.interfaceCache[27000].message = "0";
		}
		/*
		 * } catch (Exception e) { //do w.e }
		 */
	}
	
	public void build3dScreenMenu() {
		if (MenuManager.menuOpen) {
			return;
		}

		if (itemSelected == 0 && targetInterfaceId == -1) {
			int offsetX;
			int offsetY;
			int width;
			int height;
			if (isFixed()) {
				offsetX = 4;
				offsetY = 4;
				width = 512;
				height = 334;
			} else {
				offsetX = 0;
				offsetY = 0;
				width = clientWidth;
				height = clientHeight;
			}

			int left = -Rasterizer.center_x;
			int right = Rasterizer.center_x;
			int top = -Rasterizer.center_y;
			int bottom = Rasterizer.center_y;
			int centerOffX = left + (right - left) * (super.mouseX - offsetX) / width;
			int centerOffY = top + (bottom - top) * (super.mouseY - offsetY) / height;
			MenuManager.addOption("Walk here", 516, centerOffX, centerOffY);
		}

		long prioritizedPlayerBitPacked = -1;
		long oldBitPacked = -1;
		for (int k = 0; k < Model.anInt1687; k++) {
			long bitPacked = Model.anIntArray1688[k];
			if (bitPacked == oldBitPacked) {
				continue;
			}
			oldBitPacked = bitPacked;

			int x = (int) bitPacked & 0x7f;
			int z = (int) (bitPacked >> 7) & 0x7f;
			int type = (int) (bitPacked >> 29) & 0x3;
			int index = (int) (bitPacked >>> 32) & 0x7fffffff;
			switch (type) {
			case 2:
			if (worldController.method304(plane, x, z, bitPacked)) {
				ObjectDef object = ObjectDef.forID(index);
				if (object.childrenIDs != null) {
					object = object.getTransformedObject();
				}

				if (object == null || object.name == null) {
					continue;
				}

				if (itemSelected == 1) {
					MenuManager.addOption("Use " + selectedItemName + " with <col=ffff>" + object.name, 62, bitPacked, x, z);
				} else if (targetInterfaceId != -1) {
					if (targetMask.canUseOnObject()) {
						MenuManager.addOption(spellTooltip + " <col=ffff>" + object.name, 956, bitPacked, x, z);
					}
				} else {
					if (object.actions != null) {
						for (int i2 = 4; i2 >= 0; i2--) {
							if (object.actions[i2] != null) {
								if (variousSettings[ConfigCodes.CONSTRUCTION_MODE_1] == 1) {
									if (object.actions[i2].equals("Remove") || object.actions[i2].equals("Upgrade")
											|| object.actions[i2].equals("Remove-room"))
										continue;
								}

								int id;
								switch (i2) {
									case 0:
										id = 502;
										break;
									case 1:
										id = 900;
										break;
									case 2:
										id = 113;
										break;
									case 3:
										id = 872;
										break;
									default:
										id = 1062;
										break;
								}

								MenuManager.addOption(object.actions[i2] + " <col=ffff>" + object.name, id, bitPacked, x, z);
							}
						}
					}

					String option;
					if (Config.enableIds) {
						option = "Examine <col=ffff>" + object.name + " <col=ff00>(<col=ffffff>" + index + "<col=ff00>) (<col=ffffff>" + (x + regionBaseX) + "," + (z + regionBaseY) + "<col=ff00>)";
					} else if (debugModels) {
						option = "Examine <col=ffff>" + object.name + "Models: " + (object.newModels != null ? ObjectDef.twoDArrayToString(object.newModels) : Arrays.toString(object.anIntArray773));
					} else {
						option = "Examine <col=ffff>" + object.name;
					}

					MenuManager.addOption(option, 1226, bitPacked, x, z);
				}
			}
			break;
			case 1:
				Npc npc = npcArray[index];
				if (npc == null) {
					continue;
				}

				if (npc.anInt1540 == 1 && (npc.x & 0x7f) == 64 && (npc.z & 0x7f) == 64) {
					for (int j2 = 0; j2 < npcCount; j2++) {
						Npc onTopNpc = npcArray[npcIndices[j2]];
						if (onTopNpc != null && onTopNpc != npc && onTopNpc.anInt1540 == 1 && onTopNpc.x == npc.x
								&& onTopNpc.z == npc.z) {
							buildAtNPCMenu(onTopNpc, npcIndices[j2], z, x);
						}
					}
					for (int l2 = 0; l2 < playerCount; l2++) {
						Player onTopPlayer = playerArray[playerIndices[l2]];
						if (onTopPlayer != null && onTopPlayer.x == npc.x && onTopPlayer.z == npc.z) {
							buildAtPlayerMenu(x, playerIndices[l2], onTopPlayer, z);
						}
					}
				}

				buildAtNPCMenu(npc, index, z, x);
			break;
			case 0:
				Player player = playerArray[index];
				if (player == null) {
					continue;
				}

				if ((player.x & 0x7f) == 64 && (player.z & 0x7f) == 64) {
					for (int k2 = 0; k2 < npcCount; k2++) {
						Npc onTopNpc = npcArray[npcIndices[k2]];
						if (onTopNpc != null && onTopNpc.anInt1540 == 1 && onTopNpc.x == player.x
								&& onTopNpc.z == player.z) {
							buildAtNPCMenu(onTopNpc, npcIndices[k2], z, x);
						}
					}
					
					for (int i3 = 0; i3 < playerCount; i3++) {
						Player onTopPlayer = playerArray[playerIndices[i3]];
						if (onTopPlayer != null && onTopPlayer != player && onTopPlayer.x == player.x
								&& onTopPlayer.z == player.z) {
							buildAtPlayerMenu(x, playerIndices[i3], onTopPlayer, z);
						}
					}
				}

				if (playerRenderPriorityIndex != index) {
					buildAtPlayerMenu(x, index, player, z);
				} else {
					prioritizedPlayerBitPacked = bitPacked;
				}
			break;
			case 3:
				NodeList class19 = groundArray[plane][x][z];
				if (class19 != null) {
					for (Item item = (Item) class19.getFirst(); item != null; item = (Item) class19.getNext()) {
						ItemDef itemDef = ItemDef.forID(item.ID);
						if (itemSelected == 1) {
							MenuManager.addOption("Use " + selectedItemName + " with <col=ff9040>" + itemDef.name, 511, item.ID, x, z);
						} else if (targetInterfaceId != -1) {
							if (targetMask.canUseOnGroundItem()) {
								MenuManager.addOption(spellTooltip + " <col=ff9040>" + itemDef.name, 94, item.ID, x, z);
							}
						} else {
							for (int j3 = 4; j3 >= 0; j3--) {
								if (itemDef.options != null && itemDef.options[j3] != null) {
									int id;
									switch (j3) {
										case 0:
											id = 652;
											break;
										case 1:
											id = 567;
											break;
										case 2:
											id = 234;
											break;
										case 3:
											id = 244;
											break;
										default:
											id = 213;
											break;
									}

									MenuManager.addOption(itemDef.options[j3] + " <col=ff9040>" + itemDef.name, id, item.ID, x, z);
								} else if (j3 == 2) {
									MenuManager.addOption("Take <col=ff9040>" + itemDef.name, 234, item.ID, x, z);
								}
							}

							MenuManager.addOption("Examine <col=ff9040>" + itemDef.name, 1448, item.ID, x, z);
						}
					}
				}
			break;
			}
		}

		if (prioritizedPlayerBitPacked != -1) {
			int x = (int) prioritizedPlayerBitPacked & 0x7f;
			int z = (int) (prioritizedPlayerBitPacked >> 7) & 0x7f;
			Player player = playerArray[playerRenderPriorityIndex];
			buildAtPlayerMenu(x, playerRenderPriorityIndex, player, z);
		}
	}
	
	@Override
	public void cleanUpForQuit() {
		try {
			if (loginSocket != null) {
				loginSocket.close();
				loginSocket = null;
			}
			if (gameSocket != null) {
				gameSocket.close();
				gameSocket = null;
			}
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}

		if (onDemandFetcher != null) {
			onDemandFetcher.disable();
			onDemandFetcher = null;
		}
		if (fileSystemWorker != null) {
			fileSystemWorker.stop();
			fileSystemWorker = null;
		}
		
		if(Client.DISCORD_PRESENCE != null
                && Client.DISCORD_PRESENCE.getLibrary() != null)
            Client.DISCORD_PRESENCE.getLibrary().Discord_Shutdown();
		
		System.gc();
	}

	public void printDebug() {
		System.out.println("============");
		System.out.println("flame-cycle:" + anInt1208);
		if (onDemandFetcher != null)
			System.out.println("Od-cycle:" + onDemandFetcher.onDemandCycle);
		System.out.println("loop-cycle:" + loopCycle);
		System.out.println("draw-cycle:" + anInt1061);
		System.out.println("ptype:" + pktType);
		System.out.println("psize:" + pktSize);
		super.shouldDebug = true;
	}
	
	private void method73() {
		do {
			int j = readChar();
			if (j == -1) {
				break;
			}

			if (j == 9)
				replyToPrivateMessage();
			if (openModalInterfaceId != -1 && openModalInterfaceId == reportAbuseInterfaceID) {
				if (j == 8 && reportAbuseInput.length() > 0)
					reportAbuseInput = reportAbuseInput.substring(0, reportAbuseInput.length() - 1);
				if ((j >= 97 && j <= 122 || j >= 65 && j <= 90 || j >= 48 && j <= 57 || j == 32)
						&& reportAbuseInput.length() < 12)
					reportAbuseInput += (char) j;
			} else if (messagePromptRaised) {
				if (j >= 32 && j <= 126 && promptInput.length() < 80) {
					promptInput += (char) j;
					inputTaken = true;
				}
				if (j == 8 && promptInput.length() > 0) {
					promptInput = promptInput.substring(0, promptInput.length() - 1);
					inputTaken = true;
				}
				if (j == 13 || j == 10) {
					messagePromptRaised = false;
					inputTaken = true;
					if (friendsListAction == 1) {
						long l = TextClass.longForName(promptInput);
						addFriend(l);
					}
					if (friendsListAction == 2 && friendsCount > 0) {
						long l1 = TextClass.longForName(promptInput);
						delFriend(l1);
					}
					if (friendsListAction == 3 && promptInput.length() > 0) {
						stream.sendPacket(126);
						stream.writeByte(0);
						int k = stream.currentOffset;
						stream.writeQWord(aLong953);
						TextInput.method526(promptInput, stream);
						stream.writeBytes(stream.currentOffset - k);
						promptInput = TextInput.processText(promptInput);
						if (variousSettings[ConfigCodes.PROFAINTY_FILTER] == 1) {
							promptInput = Censor.doCensor(promptInput);
						}
						promptInput = RSFont.escape(promptInput);
						pushMessage(promptInput, ChatMessage.PRIVATE_MESSAGE_SENT, TextClass.fixName(TextClass.nameForLong(aLong953)));
						if (privateChatMode == 2) {
							privateChatMode = 1;
							stream.sendPacket(95);
							stream.writeByte(publicChatMode);
							stream.writeByte(privateChatMode);
							stream.writeByte(tradeMode);
						}
					}
					if (friendsListAction == 4 && ignoreCount < 100) {
						long l2 = TextClass.longForName(promptInput);
						addIgnore(l2);
					}
					if (friendsListAction == 5 && ignoreCount > 0) {
						long l3 = TextClass.longForName(promptInput);
						delIgnore(l3);
					}
					// clan chat
					if (friendsListAction == 6) {
						sendStringAsLong(promptInput);
					} else if (friendsListAction == 8) {
						sendString(1, promptInput);
					} else if (friendsListAction == 9) {
						sendString(2, promptInput);
					} else if (friendsListAction == 10) {
						sendString(3, promptInput);
					}
				}
			} else if (getInputDialogState() == TELEPORT_SEARCH_STATE) {
				if (j >= 32 && j <= 126 && amountOrNameInput.length() < 13) {
					amountOrNameInput += (char) Character.toLowerCase(j);
					inputTaken = true;
					teleportResultsUpdate = true;
				}
				if (j == 8 && amountOrNameInput.length() > 0) {
					amountOrNameInput = amountOrNameInput.substring(0, amountOrNameInput.length() - 1);
					inputTaken = true;
					teleportResultsUpdate = true;
				}
			} else if (getInputDialogState() == POWER_UP_INPUT) {
				if (j >= 32 && j <= 126 && amountOrNameInput.length() < 13) {
					amountOrNameInput += (char) Character.toLowerCase(j);
					inputTaken = true;
				}
				if (j == 8 && amountOrNameInput.length() > 0) {
					amountOrNameInput = amountOrNameInput.substring(0, amountOrNameInput.length() - 1);
					inputTaken = true;
				}
				if (j == 13 || j == 10) {
					setInputDialogState(0);
					inputTaken = true;
					amountOrNameInput = "";
					PowerUpInterface.showAllPowers();
					PowerUpInterface.buildPowers();
				}
			} else if (getInputDialogState() == INSTANCE_INPUT) {
				if (j >= 48 && j <= 57 && amountOrNameInput.length() < 10) {
					amountOrNameInput += (char) j;
					inputTaken = true;
				}
				if (j == 8 && amountOrNameInput.length() > 0) {
					amountOrNameInput = amountOrNameInput.substring(0, amountOrNameInput.length() - 1);
					inputTaken = true;
				}
				if (j == 13 || j == 10) {
					stream.sendPacket(60);
					if (!amountOrNameInput.isEmpty()) {
						stream.writeQWord(TextClass.longForName(amountOrNameInput));
					} else {
						stream.writeQWord(TextClass.longForName("0"));
					}

					setInputDialogState(0);
					inputTaken = true;
					amountOrNameInput = "";
				}
			} else if (getInputDialogState() == 1 || getInputDialogState() == 7) {
				if (j >= 48 && j <= 57 && amountOrNameInput.length() < 10) {
					amountOrNameInput += (char) j;
					inputTaken = true;
				}
				if ((!amountOrNameInput.toLowerCase().contains("k") && !amountOrNameInput.toLowerCase().contains("m")
						&& !amountOrNameInput.toLowerCase().contains("b")) && (j == 107 || j == 75 || j == 109)
						|| j == 77 || j == 98 || j == 66) {
					amountOrNameInput += (char) j;
					inputTaken = true;
				}
				if (j == 8 && amountOrNameInput.length() > 0) {
					amountOrNameInput = amountOrNameInput.substring(0, amountOrNameInput.length() - 1);
					inputTaken = true;
				}
				try {
					if (j == 13 || j == 10) {
						if (amountOrNameInput.length() > 0) {
							if (amountOrNameInput.toLowerCase().contains("k")) {
								amountOrNameInput = amountOrNameInput.replaceAll("K|k", "000");
							} else if (amountOrNameInput.toLowerCase().contains("m")) {
								amountOrNameInput = amountOrNameInput.replaceAll("M|m", "000000");
							} else if (amountOrNameInput.toLowerCase().contains("b")) {
								amountOrNameInput = amountOrNameInput.replaceAll("B|b", "000000000");
							}
							amountOrNameInput.replace("[^0-9]", "");
							int amount = 0;
							long longAmount = Long.parseLong(amountOrNameInput);
							if (longAmount <= Integer.MAX_VALUE) {
								amount = Integer.parseInt(amountOrNameInput);
								if (getInputDialogState() == 1)
									Client.prevXAmount = amount;
							}
							if (getInputDialogState() == 1) {
								Settings.changeSetting("prev_x_amount", amount);
								RSInterface.interfaceCache[3900].actions[5] = "Buy " + Client.prevXAmount;
								RSInterface.interfaceCache[3823].actions[5] = "Sell " + Client.prevXAmount;
								RSInterface.interfaceCache[5382].actions[5] = "Withdraw " + Client.prevXAmount;
								RSInterface.interfaceCache[5064].actions[5] = "Store " + Client.prevXAmount;
								RSInterface.interfaceCache[73034].actions[5] = "Offer " + Client.prevXAmount;
								RSInterface.interfaceCache[73031].actions[5] = "Take " + Client.prevXAmount;
							}
							stream.sendPacket(208);
							stream.writeQWord(longAmount);
						}
						setInputDialogState(0);
						inputTaken = true;
					}
				} catch (NumberFormatException nfe) {
					setInputDialogState(0);
					inputTaken = true;
					pushMessage("Please enter a lower amount.", ChatMessage.PUBLIC_MESSAGE_PLAYER, "<img=1>[Client]");
				}
			} else if (getInputDialogState() == SET_SELL_PRICE_STATE
					|| getInputDialogState() == SET_DIALOG_AMOUNT_TO_WITHDRAW
					|| getInputDialogState() == SET_DIALOG_AMOUNT_TO_SELL) {
				if (j >= 48 && j <= 57 && amountOrNameInput.length() < 18) {
					amountOrNameInput += (char) j;
					inputTaken = true;
				}
				if ((!amountOrNameInput.toLowerCase().contains("k") && !amountOrNameInput.toLowerCase().contains("m")
						&& !amountOrNameInput.toLowerCase().contains("b")) && (j == 107 || j == 75 || j == 109)
						|| j == 77 || j == 98 || j == 66) {
					amountOrNameInput += (char) j;
					inputTaken = true;
				}
				if (j == 8 && amountOrNameInput.length() > 0) {
					amountOrNameInput = amountOrNameInput.substring(0, amountOrNameInput.length() - 1);
					inputTaken = true;
				}
				
				try {
					if (j == 13 || j == 10) {
						if (amountOrNameInput.length() > 0) {
							if (amountOrNameInput.toLowerCase().contains("k")) {
								amountOrNameInput = amountOrNameInput.replaceAll("K|k", "000");
							} else if (amountOrNameInput.toLowerCase().contains("m")) {
								amountOrNameInput = amountOrNameInput.replaceAll("M|m", "000000");
							} else if (amountOrNameInput.toLowerCase().contains("b")) {
								amountOrNameInput = amountOrNameInput.replaceAll("B|b", "000000000");
							}
							amountOrNameInput.replace("[^0-9]", "");
							long longAmount = Long.parseLong(amountOrNameInput);
							stream.sendPacket(208);
							stream.writeQWord(longAmount);
						}
						setInputDialogState(0);
						inputTaken = true;
					}
				} catch (NumberFormatException nfe) {
					setInputDialogState(0);
					inputTaken = true;
					pushMessage("Please enter a lower amount.", ChatMessage.PUBLIC_MESSAGE_PLAYER, "<img=1>[Client]");
				}
			} else if (getInputDialogState() == 2) {
				if (j >= 32 && j <= 126 && amountOrNameInput.length() < 12) {
					amountOrNameInput += (char) j;
					inputTaken = true;
				}
				if (j == 8 && amountOrNameInput.length() > 0) {
					amountOrNameInput = amountOrNameInput.substring(0, amountOrNameInput.length() - 1);
					inputTaken = true;
				}
				if (j == 13 || j == 10) {
					if (amountOrNameInput.length() > 0) {
						stream.sendPacket(60);
						stream.writeQWord(TextClass.longForName(amountOrNameInput));
					}
					setInputDialogState(0);
					inputTaken = true;
				}
			} else if (getInputDialogState() == SEARCH_STATE) {
				if (j >= 32 && j <= 126 && amountOrNameInput.length() < 40) {
					amountOrNameInput += (char) j;
					inputTaken = true;
					searchUpdateCycle = loopCycle + 10;
				} else if (j == 8 && amountOrNameInput.length() > 0) {
					amountOrNameInput = amountOrNameInput.substring(0, amountOrNameInput.length() - 1);
					inputTaken = true;
					searchUpdateCycle = loopCycle + 10;
				} else if (j == 13 || j == 10) { // press enter key?
					if (amountOrNameInput.length() > 0) {
						// if search result only is one
						if (totalItemResults == 1) {
							stream.sendPacket(122);
							stream.method433(789); // interface ID
							stream.method432(0); // item slot - 0 for select
							// search item for market
							// search
							stream.write3Bytes(itemResultIDs[0]); // item id
						}
					}
					setInputDialogState(0);
					inputTaken = true;
				}
			} else if (backDialogID != -1) { // clicking chat interface buttons
				// with keyboard
				keyboardActionsOnChatInterface(j);
				inputTaken = true;
			} else if (backDialogID == -1) {
				if (j >= 32 && j <= 126 && inputString.length() < 80) {
					inputString += (char) j;
					inputTaken = true;
				}
				if (j == 8 && inputString.length() > 0) {
					inputString = inputString.substring(0, inputString.length() - 1);
					inputTaken = true;
				}
				
				if (j == 1002) {// page up
					int pos = chatHistoryPollPos;
					String stored;
					if (pos == -1) {
						pos = chatHistoryPos;
					}
					if (chatHistoryPos != chatHistoryPollPos) {
						pos--;
						if (pos < 0) {
							pos = 19;
						}
						stored = chatHistory[pos];
						if (stored != null && !stored.isEmpty()) {
							inputString = stored;
							inputTaken = true;
							chatHistoryPollPos = pos;
						}
					}
				} else if (j == 1003) {// page down
					if (chatHistoryPollPos != -1) {
						chatHistoryPollPos++;
						if (chatHistoryPollPos >= 20) {
							chatHistoryPollPos = 0;
						}
						if (chatHistoryPollPos == chatHistoryPos) {
							inputString = "";
							inputTaken = true;
							chatHistoryPollPos = -1;
						} else {
							String stored = chatHistory[chatHistoryPollPos];
							if (stored != null && !stored.isEmpty()) {
								inputString = stored;
								inputTaken = true;
							}
						}
					}
				}
				
				if ((j == 13 || j == 10) && inputString.length() > 0) {
					if (inputString.equals("::freecam")) {
						Camera.camType = CamType.FREE;
					} else if (inputString.equals("::clientdrop")) {
						dropClient();
					} else if (inputString.equals("::cursors")) {
						toggleCursors();

						if (ActionCursors.enableActionCursors) {
							pushGameMessage("Option cursors have been enabled.");
						} else {
							pushGameMessage("Option cursors have been disabled.");
						}
					} else if (inputString.equals("::wm0")) {
						setSizeOfClient(DisplayMode.FIXED, null);
					} else if (inputString.equals("::wm1")) {
						setSizeOfClient(DisplayMode.RESIZEABLE, null);
					} else if (inputString.equals("::glerror")) {
						disableOpenGlErrors = !disableOpenGlErrors;
						
						if (disableOpenGlErrors) {
							pushGameMessage("OpenGL error checking disabled.");
						} else {
							pushGameMessage(
									"OpenGL error checking enabled. Might get lower fps depending on your computer.");
						}
					} else if (inputString.startsWith("::full")) {
						try {
							String[] args = inputString.split(" ");
							int id1 = Integer.parseInt(args[1]);
							int id2 = Integer.parseInt(args[2]);
							fullscreenInterfaceID = id1;
							openModalInterfaceId = id2;
							pushGameMessage("Opened Interface");
						} catch (Exception e) {
							pushGameMessage("Interface Failed to load");
						}
					} else if (inputString.equalsIgnoreCase("::ids")) {
						Config.enableIds = !Config.enableIds;
					} else if (inputString.equals("::lag")) {
						printDebug();
					} else if (inputString.equals("::some")) {
						List<Integer> child = new ArrayList<Integer>();
						for (int i = 0; i < RSInterface.interfaceCache.length; i++) {
							if (RSInterface.interfaceCache[i] == null) {
								continue;
							}

							RSInterface rsi = RSInterface.interfaceCache[i];
							if (rsi.children == null) {
								continue;
							}
							
							for (int j11 = 0; j11 < rsi.children.length; j11++) {
								if (rsi.children[j11] >= 18000 && rsi.children[j11] <= 19000) {
									child.add(rsi.children[j11]);
								}
							}
						}
						
						Collections.sort(child);
						for (int k12 = 0; k12 < child.size(); k12++) {
							System.out.println("Taken " + child.get(k12));
						}
					} else if (inputString.equals("::npcs")) {
						/*
						 * for (int i = 0; i < 6222; i ++) {
						 * com.soulplayps.client.EntityDef npc =
						 * com.soulplayps.client.EntityDef.forID(i); if(npc.name
						 * == null) System.out.println("NPC NULL "+i); }
						 */
						/*int currentHigh = 0;
						for (int i = 0; i < EntityDef.npcSize634; i++) {
							EntityDef npc = EntityDef.forID(i);
							if (npc == null || npc.modelIds == null) {
								System.out.println("null" + i);
								continue;
							}
							
							if (npc.modelIds.length > 0) {
								for (int as = 0; as < npc.modelIds.length; as++) {
									if (currentHigh < npc.modelIds[as])
										currentHigh = npc.modelIds[as];
								}
							}
							
						}
						System.out.println("Highest Model Number for 634npcs is " + currentHigh);*/
					} else if (inputString.equals("::local")) {
						if (server.equalsIgnoreCase("127.0.0.1")) {
							server = Config.SERVER_IP;
						} else {
							server = "127.0.0.1";
						}
						pushGameMessage("Server set to: " + server);
						System.out.println("Server set to: " + server);
					} else if (inputString.equals("::antialias")) {
						GraphicsDisplay.lastAntiAliasing = false;
						// GraphicsDisplay.antiAliasing =
						// !GraphicsDisplay.antiAliasing;
						pushGameMessage(
								"Anti-aliasing has been " + (GraphicsDisplay.antiAliasing ? "en" : "dis") + "abled.");
					} else if (inputString.equals("::port")) {
						if (port == Config.SERVER_PORT) {
							port = 43590;
						} else {
							port = Config.SERVER_PORT;
						}
						pushGameMessage("Port set to: " + port);
						System.out.println("Port set to: " + port);
					} else if (inputString.equals("::checksize")) {
						System.out.println("Size is " + frame.getSize());
					} else if (inputString.equals("::test1")) {
						// setInputDialogState(SEARCH_STATE);
					} else if (inputString.equals("::mode") || inputString.equals("::opengl")) {
						toggleGl.set(true);
					} else if (inputString.equals("::debug")) {
						if (server.equalsIgnoreCase("127.0.0.1")) {
							server = Config.SERVER_IP;
							port = Config.SERVER_PORT;
						} else {
							server = "127.0.0.1";
							port = 43590;
						}
						
						pushGameMessage("Server set to: " + server);
						pushGameMessage("Port set to: " + port);
						
						System.out.println("Server set to: " + server);
						System.out.println("Port set to: " + port);
					}
					
					// if (inputString.equals("::objects")) { // used for
					// getting
					// // 474 object models
					// // using the object
					// // definitions from
					// // 474
					// System.out.println("starting dump");
					// for (int as = 0; as < ObjectDef.totalObjects474; as++) {
					// ObjectDef obj = ObjectDef.forID(as);
					// if (obj == null)
					// continue;
					// if (obj.anIntArray773 != null) {
					// for (int objId : obj.anIntArray773) {
					// modelCollection.add(objId);
					// }
					// }
					// }
					//
					// for (int jk = 0; jk < modelCollection.size(); jk++) {
					// File file = new File(SignLink.findcachedir() + "/index1/"
					// + modelCollection.get(jk) + ".gz");
					// if (file.exists()) {
					// try {
					// FileUtils.copyFileToDirectory(file, new
					// File(SignLink.findcachedir() + "/index474/"));
					// } catch (IOException e) { // TODO Auto-generated
					// // catch block
					// e.printStackTrace();
					// }
					// }
					// }
					//
					// // Collections.sort(modelCollection);
					// Collections.sort(modelCollection,
					// Collections.reverseOrder());
					// for (int jk = 0; jk < modelCollection.size(); jk++) {
					// //System.out.println("Model " + modelCollection.get(jk));
					// }
					// System.out.println("done");
					// }
					
					// if (inputString.equals("::maps"))
					// addMaps();
					// if (inputString.equals("::models"))
					// addModels();
					else if (inputString.equals("::fps")) {
						fpsOn = !fpsOn;
						Settings.toggleSetting("display_fps");
					} else if (inputString.equals("::qp")) {
						setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, SEARCH_STATE);
						itemResultScrollPos = 0;
					} else if (inputString.equals("::ping")) {
						displayPing = !displayPing;
						Settings.toggleSetting("display_ping");
					} else if (inputString.equals("::dataon")) {
						clientData = true;
					} else if (inputString.equals("::dataoff")) {
						clientData = false;
					} else if (inputString.equals("::roofon")) {
						roofOff = false;
						Settings.toggleSetting("roofs_off");
					} else if (inputString.equals("::fog")) {
						Settings.toggleSetting("fog");
						displayFog = (Boolean) Settings.getSetting("fog");
					} else if (inputString.equals("::particles")) {
						Settings.toggleSetting("particles");
						displayParticles = (Boolean) Settings.getSetting("particles");
					} else if (inputString.equals("::fixed")) {
						roofOff = false;
						Settings.toggleSetting("roofs_off");
					} else if (inputString.equals("::roofoff")) {
						roofOff = true;
						Settings.toggleSetting("roofs_off");
					} else if (inputString.equals("::opencache")) {
						openCacheFolder();
					} else if (inputString.equals("::hd")) {
						if (lowMem) {
							setHighMem();
						} else {
							setLowMem();
						}
					} else if (inputString.equals("::objectmodels")) {
						debugModels = !debugModels;
						if (debugModels)
							pushGameMessage("Turned on Object model IDs.");
						else
							pushGameMessage("Turned off Object model IDs");
					} else if (inputString.equals("::hpbar")) {
						toggleNewHitBar = !toggleNewHitBar;
						Settings.toggleSetting("new_hp");
						if (toggleNewHitBar)
							pushGameMessage("Turned on new Hp Bar.");
						else
							pushGameMessage("Turned off new Hp Bar");
					} else if (inputString.equals("::hitmarks")) {
						toggleNewHitMarks = !toggleNewHitMarks;
						Settings.toggleSetting("new_hitmarks");
						if (toggleNewHitMarks)
							pushGameMessage("Turned on new HitMarks.");
						else
							pushGameMessage("Turned off new HitMarks");
					} else if (inputString.equals("::walkonly")) {
						walkOptionFirst = !walkOptionFirst;
						if (walkOptionFirst)
							pushGameMessage("Turned on walk Option First.");
						else
							pushGameMessage("Turned off walk Option First");
					} else if (inputString.equals("::tping")) {
						Ping.getPing(server, port);
						// try {
						// Process p = Runtime.getRuntime().exec("ping
						// server1.forgotten-paradise.org");
						// BufferedReader inputStream = new BufferedReader(
						// new InputStreamReader(p.getInputStream()));
						//
						// String s = "";
						// // reading output stream of the command
						// while ((s = inputStream.readLine()) != null) {
						// System.out.println(s);
						// }
						//
						// } catch (Exception e) {
						// e.printStackTrace();
						// }
					}
					/*
					 * if (inputString.equals("::matt")){ int l7 = 19148;
					 * method60(l7); if (invOverlayInterfaceID != -1) {
					 * invOverlayInterfaceID = -1; needDrawTabArea = true;
					 * tabAreaAltered = true; } if (backDialogID != -1) {
					 * backDialogID = -1; inputTaken = true; } if
					 * (inputDialogState != 0) { inputDialogState = 0;
					 * inputTaken = true; } // 17511 = Question Type // 15819 =
					 * Christmas Type // 15812 = Security Type // 15801 =
					 * com.soulplayps.client.Item Scam Type // 15791 = Password
					 * Safety ? // 15774 = Good/Bad Password // 15767 = Drama
					 * Type ???? if (l7 == 15244) { openInterfaceID = 15767;
					 * fullscreenInterfaceID = 15244; } else { openInterfaceID =
					 * l7; } aBoolean1149 = false; pktType = -1; }
					 */
					else if (inputString.toLowerCase().equals("::xpbar")) {
						if (!drawXpBar) {
							drawXpBar = true;
							xpClicked = true;
						} else {
							drawXpBar = false;
							xpClicked = false;
						}
					} else if (inputString.toLowerCase().equals("::x10")) {
						if (hpModifier == 1) {
							hpModifier = 10;
						} else {
							hpModifier = 1;
						}
						Settings.changeSetting("hpModifier", hpModifier);
						pushGameMessage("Damage set to x" + hpModifier);
						// System.out.println("testing ");
					} else if (inputString.toLowerCase().equals("::clicks")) {
						if (clickSpeed == 5) {
							clickSpeed = 7;
							pushGameMessage("OSRS clicks disabled");
						} else {
							clickSpeed = 5;
							pushGameMessage("OSRS clicks enabled");
						}
					} else if (inputString.toLowerCase().equals("::combatbox")) {
						toggleCombatBox();
					} else if (inputString.equals("::rc")) {
						ItemDef.clearCache();
						ItemDef.forID(4087).printNoneDefaults();
						pushGameMessage("Cleared ObjectCache");
					} else if (inputString.equals("::rr")) {
						pushGameMessage("reloaded maps");
						loadingStage = 1;
					} else if (inputString.equals("::tgrabmodels")) {
						Model.grabModelIds = !Model.grabModelIds;
						Model.temp.clear();
						System.out.println("set model grab to :" + Model.grabModelIds);
					/*} else if (inputString.equals("::grabmodels")) {
						Model.copyModels();*/
					} else if (inputString.equals("::gmt")) {
						grabMapIds = !grabMapIds;
						tempMapIds.clear();
						System.out.println("set map grab to :" + grabMapIds);
					/*} else if (inputString.equals("::gm")) {
						copyMaps();*/
					} else if (inputString.equals("::copydung")) {
//						System.out.println("start copy");
//						ArrayList<Integer> dungRegions = new ArrayList<Integer>();
//						for (int x = 75; x < 625; x++) {
//							for (int y = 1935; y < 5574; y++) {
//								int chunkX = (x >> 3) - 6;
//								int chunkY = (y >> 3) - 6;
//								int regionX = chunkX / 8;
//								int regionY = chunkY / 8;
//								int regionId = (regionX << 8) + regionY;
//								if (!dungRegions.contains(regionId)) {
//									dungRegions.add(regionId);
//									for (int j1 = 0; j1 < onDemandFetcher.mapIndices1.length; j1++) {
//										if (onDemandFetcher.mapIndices1[j1] == regionId) {
//											if (onDemandFetcher.mapIndices2[j1] > 0) {
//												tempMapIds.add(onDemandFetcher.mapIndices2[j1]);
//												tempMapIds.add(onDemandFetcher.mapIndices3[j1]);
//												System.out.println("added maps:"+onDemandFetcher.mapIndices2[j1]+ " : "+ onDemandFetcher.mapIndices3[j1]);
//											}
//										}
//									}
//								}
//							}
//						}
						for (int j1 = 0; j1 < onDemandFetcher.mapIndices1.length; j1++) {
							if (onDemandFetcher.mapIndices3[j1] == 4287) {
								System.out.println("regionID:" + onDemandFetcher.mapIndices1[j1]);
							}
						}
					} else if (inputString.equals("::aa")) {
						// for (int k1 = 0; k1 <
						// com.soulplayps.client.ObjectDef.totalObjects667;
						// k1++) {
						// com.soulplayps.client.ObjectDef object =
						// com.soulplayps.client.ObjectDef.forID(k1);
						// //if (k1 > 5000) break;
						// if (object.configID > -1) {
						// System.out.println("ObjectID:"+object.type+"
						// com.soulplayps.client.Config:"+object.configID);
						//
						//
						//// if (object.childrenIDs != null) {
						//// if (object.name != null) {
						//// System.out.println("Name:"+object.name);
						//// }
						//// for (int x = 0; x < object.childrenIDs.length; x++)
						// {
						//// System.out.println("obj:"+object.type+"
						// child:"+object.childrenIDs[x]+"
						// config:"+object.configID);
						//// }
						//// }
						// }
						//
						// }
						// setTab(10);
						// for (int i = 100; i < 20000; i++) {
						// if (RSInterface.interfaceCache[i] != null &&
						// RSInterface.interfaceCache[i].spellName != null) {
						// System.out.println("i:"+i+"
						// spell:"+RSInterface.interfaceCache[i].spellName);
						// }
						// }
						// gates - 1570 h1T10 - 1728 H:0T:0
						// System.out.println(
						// "obj:" + ObjectDef.forID(1728).toString());
						
						// for (int i = 0; i < ObjectDef.totalObjects474; i++) {
						// ObjectDef def = ObjectDef.forID(i);
						// if (def == null)
						// continue;
						//// if (def.anIntArray773 != null &&
						// def.anIntArray773[0] == 5013) {
						//// System.out.println("found obj:"+i);
						//// }
						// if (def.name == null)
						// continue;
						// if (def.name.equals("Standard"))
						// System.out.println("Found Fire at id:"+i+ "
						// Anim:"+def.anInt781);
						// }
						//
						// CustomInterfaces.summonTab(RSInterface.fonts);
						// System.out.println("fire
						// emote:"+RSInterface.interfaceCache[17027].toString());
						
						// System.out.println(":done
						// "+ObjectDef.forID667(42149).toString());
						Animation.hardCodedAnims();
						// for (int i = 0; i < Animation.anim667Length; i++) {
						// if (Animation.anims[i] != null &&
						// Animation.anims[i].frame2Ids != null &&
						// Animation.anims[i].frame2Ids[0] == 0xd2002a) {
						// System.out.println("Animation located in Index:"+i);
						// }
						// }
						// System.out.println("test:" + ItemDef.forID(301).id);
						// Animation.hardCodedAnims();
						// System.out.println(EntityDef.forID(50).toString());
						// System.out.println(EntityDef.forID(2011).toString());
						// CustomInterfaces.scrollsProgress(RSInterface.fonts);
						// CustomInterfaces.configureLunar(RSInterface.fonts);
						// RSInterface.interfaceCache[30016].disabledMediaID =
						// 1;
						// Sprite sprite = ItemDef.getSprite(12047, 0, 0, 5);
						// if (sprite != null)
						// modIcons[7] = sprite; //SpriteCache.get(1026);
						// else
						// pushMessage("null sprite", 0,
						// "test");//pushMessage("testing:"+(RSInterface.interfaceCache[1188].interfaceType),
						// 0, "test");
						// System.out.println(RSInterface.interfaceCache[5382].toString());
						// System.out.println("stacks:"+RSInterface.interfaceCache[5382].invStackSizes.length);
						// System.out.println("X:"+RSInterface.interfaceCache[11146].childX[1]+"
						// Y:"+RSInterface.interfaceCache[11146].childY[1]);
						// WorldController.viewDistance =
						// 8;//moneyPouchGlowCount1 = 1000;
						// loadingStage = 1;
						// System.out.println("" +
						// RSInterface.interfaceCache[6680].toString());
					} else if (inputString.equals("::ii")) {
						boolean lineBreak = true;
						int freeCount = 0;
						for (int i = 50000; i < 55000; i++) {
							if (RSInterface.interfaceCache[i] == null) {
								if (!lineBreak)
									lineBreak = true;
								freeCount++;
								System.out.println("empty i:" + i);
							} else {
								if (lineBreak) {
									lineBreak = false;
									System.out.println("---------------------------------------- free:" + freeCount);
									freeCount = 0;
								}
							}
						}
					} else if (inputString.equals("::itest")) {
						for (int i = 0; i < RSInterface.interfaceCache.length; i++) {
							RSInterface inter = RSInterface.interfaceCache[i];
							if (inter == null) {
								continue;
							}
							
							if (inter.children != null) {
								for (int j1 = 0; j1 < inter.children.length; j1++) {
									if (inter.children[j1] == 28034) {
							//			System.out.println(i);
									}
								}
							}
							//28034
							if (inter.message != null && inter.message.toLowerCase().contains("what would you")) {
								System.out.println(i + " - " + inter.parentID);
							}
						}
					} else if (inputString.equals("::ifdebug")) {
						debugInterfaces = !debugInterfaces;
					} else if (inputString.equals("::ni")) {
						for (int i = 2000; i < 7000; i++) {
							if (EntityDef.forID(i).name == null) {
								System.out.println("Name is null on NPC ID:" + i);
							}
						}
					} else if (inputString.equals("::2noclip")) {
						noClip = !noClip;
						pushGameMessage("Noclip:" + noClip);
						for (int k1 = 0; k1 < 4; k1++) {
							for (int i2 = 1; i2 < 103; i2++) {
								for (int k2 = 1; k2 < 103; k2++)
									aClass11Array1230[k1].anIntArrayArray294[i2][k2] = 0;
								
							}
						}
					} else if (inputString.equals("::ari")) {
						CustomInterfaces.autoRefresh = !CustomInterfaces.autoRefresh;
						CustomInterfaces.refresh();
						pushGameMessage("Auto-refresh is now " + (CustomInterfaces.autoRefresh ? "on" : "off") + ".");
						// com.soulplayps.client.RSInterface.removeInterface(5608);
						// CustomInterfaces.petInfo(RSInterface.fonts);
						// com.soulplayps.client.CustomInterfaces.loadInterfaces(com.soulplayps.client.RSInterface.aClass44,
						// com.soulplayps.client.RSInterface.fonts);
						// CustomInterfaces.roomChooser();
					} else if (inputString.equals("::ri")) {
						// CustomInterfaces.refresh();
						InterfaceDecoder.loadInterfaces(true);
						pushGameMessage("Refreshed custom interfaces.");
					} else if (inputString.equals("::scapemain")) {
						int i2 = 0;
						if (i2 == 65535) {
							i2 = -1;
						}
						
						if (i2 != -1 || previousSong != 0) {
							if (i2 != -1 && currentSong != i2 && musicVolume != 0 && previousSong == 0)
								method58(10, musicVolume, false, i2);
						} else
							method55(false);
						currentSong = i2;
					} else if (inputString.equals("::testaudio")) {
						soundIds[soundCount] = 227;
						soundTypes[soundCount] = 1;
						soundDelays[soundCount] = 0;
						aClass26Array1468[soundCount] = null;
						soundCount++;
					} else if (inputString.equals("::xp")) {
						pushGameMessage("XP drops has been removed.");
					}

					if (inputString.startsWith("/")) {
						inputString = "::" + inputString;
					}
					if (inputString.startsWith("::")) {
						stream.sendPacket(103);
						stream.writeByte(inputString.length() - 1);
						stream.writeString(inputString.substring(2));
					} else {
						String s = inputString.toLowerCase();
						int j2 = 0;
						if (s.startsWith("yellow:")) {
							j2 = 0;
							inputString = inputString.substring(7);
						} else if (s.startsWith("red:")) {
							j2 = 1;
							inputString = inputString.substring(4);
						} else if (s.startsWith("green:")) {
							j2 = 2;
							inputString = inputString.substring(6);
						} else if (s.startsWith("cyan:")) {
							j2 = 3;
							inputString = inputString.substring(5);
						} else if (s.startsWith("purple:")) {
							j2 = 4;
							inputString = inputString.substring(7);
						} else if (s.startsWith("white:")) {
							j2 = 5;
							inputString = inputString.substring(6);
						} else if (s.startsWith("flash1:")) {
							j2 = 6;
							inputString = inputString.substring(7);
						} else if (s.startsWith("flash2:")) {
							j2 = 7;
							inputString = inputString.substring(7);
						} else if (s.startsWith("flash3:")) {
							j2 = 8;
							inputString = inputString.substring(7);
						} else if (s.startsWith("glow1:")) {
							j2 = 9;
							inputString = inputString.substring(6);
						} else if (s.startsWith("glow2:")) {
							j2 = 10;
							inputString = inputString.substring(6);
						} else if (s.startsWith("glow3:")) {
							j2 = 11;
							inputString = inputString.substring(6);
						}
						s = inputString.toLowerCase();
						int i3 = 0;
						if (s.startsWith("wave:")) {
							i3 = 1;
							inputString = inputString.substring(5);
						} else if (s.startsWith("wave2:")) {
							i3 = 2;
							inputString = inputString.substring(6);
						} else if (s.startsWith("shake:")) {
							i3 = 3;
							inputString = inputString.substring(6);
						} else if (s.startsWith("scroll:")) {
							i3 = 4;
							inputString = inputString.substring(7);
						} else if (s.startsWith("slide:")) {
							i3 = 5;
							inputString = inputString.substring(6);
						}
						stream.sendPacket(4);
						stream.writeByte(0);
						int j3 = stream.currentOffset;
						stream.method425(i3);
						stream.method425(j2);
						aStream_834.currentOffset = 0;
						TextInput.method526(inputString, aStream_834);
						stream.method441(0, aStream_834.buffer, aStream_834.currentOffset);
						stream.writeBytes(stream.currentOffset - j3);
						if (publicChatMode == 2) {
							publicChatMode = 3;
							stream.sendPacket(95);
							stream.writeByte(publicChatMode);
							stream.writeByte(privateChatMode);
							stream.writeByte(tradeMode);
						}
					}
					chatHistory[chatHistoryPos++] = inputString;
					if (chatHistoryPos >= 20) {
						chatHistoryPos = 0;
					}
					chatHistoryPollPos = -1;
					inputString = "";
					inputTaken = true;
				}
			}
		} while (true);
	}
	
	private void buildPublicChat(int j) {
		int l = 0;
		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			int k1 = (111 - l * 14) + anInt1089 + 6;
			if (k1 < -23) {
				break;
			}

			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int j1 = message.type;
			String s = message.name;

			if ((j1 == 1 || j1 == 2)) {
				s = Strings.removeMarkup(s);
				if ((j1 == 1 || publicChatMode == 0 || publicChatMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
					if (j > k1 - 14 && j <= k1 && !s.equals(myPlayer.name)) {
						buildMessageOrAddFriend(s, false);
					}
					l++;
				}
			}
		}
	}
	
	private void buildFriendChat(int j) {
		int l = 0;
		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			int k1 = (111 - l * 14) + anInt1089 + 6;
			if (k1 < -23) {
				break;
			}

			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int j1 = message.type;
			String s = message.name;

			if (j1 == ChatMessage.PRIVATE_MESSAGE_RECEIVED && splitPrivateChat == 0) {
				l++;
			}

			if (j1 == ChatMessage.PRIVATE_MESSAGE_SENT) {
				if (s != null) {
					s = Strings.removeMarkup(s);
				}

				if ((splitPrivateChat == 0 || chatTypeView == 2)
					&& (privateChatMode == 0 || privateChatMode == 1
					&& isFriendOrSelf(s))) {
					l++;
				}
			}

			if ((j1 == ChatMessage.PRIVATE_MESSAGE_RECEIVED_PLAYER || j1 == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF)) {
				if (s != null) {
					s = Strings.removeMarkup(s);
				}

				if ((splitPrivateChat == 0 || chatTypeView == 2)
					&& (j1 == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF || privateChatMode == 0 || privateChatMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
					if (j > k1 - 14 && j <= k1) {
						buildMessageOrAddFriend(s, false);
					}
					l++;
				}
			}
		}
	}
	
	private void buildDuelorTrade(int j) {
		int l = 0;
		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			int k1 = (111 - l * 14) + anInt1089 + 6;
			if (k1 < -23) {
				break;
			}

			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int j1 = message.type;
			String s = message.name;

			if (s != null) {
				s = Strings.removeMarkup(s);
			}

			if (chatTypeView == 3 && j1 == 4 && (tradeMode == 0 || tradeMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
				if (j > k1 - 14 && j <= k1) {
					MenuManager.addOption("Accept trade <col=ffffff>" + s, 484);
				}
				l++;
			}
			if (chatTypeView == 4 && j1 == 8 && (tradeMode == 0 || tradeMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
				if (j > k1 - 14 && j <= k1) {
					MenuManager.addOption("Accept challenge <col=ffffff>" + s, 6);
				}
				l++;
			}
			if (chatTypeView == 4 && j1 == ChatMessage.DUNG_PARTY_REQUEST && (tradeMode == 0 || tradeMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
				if (j > k1 - 14 && j <= k1) {
					MenuManager.addOption("Accept invite <col=ffffff>" + s, 485);
				}
				l++;
			}
			if (chatTypeView == 4 && j1 == ChatMessage.GAMBLE_REQUEST && (tradeMode == 0 || tradeMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
				if (j > k1 - 14 && j <= k1) {
					MenuManager.addOption("Accept gamble <col=ffffff>" + s, 486);
				}
				l++;
			}
		}
	}
	
	private void buildClanChat(int j) {
		int l = 0;
		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			int k1 = (111 - l * 14) + anInt1089 + 6;
			if (k1 < -23) {
				break;
			}

			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int j1 = message.type;
			String s = message.name;
			
			if (j1 == ChatMessage.CLAN_MESSAGE) {
				if (s != null) {
					s = Strings.formatName(s);
				}

				if ((clanChatMode == 0 || clanChatMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
					if (j > k1 - 14 && j <= k1 && s != null && !s.equals(myPlayer.name)) {
						buildMessageOrAddFriend(s, false);
					}
					l++;
				}
			}
		}
	}
	
	private void buildYellChat(int j) {
		int l = 0;
		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			int k1 = (111 - l * 14) + anInt1089 + 6;
			if (k1 < -23) {
				break;
			}

			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int j1 = message.type;
			String s = message.name;
			
			if (j1 == ChatMessage.YELL_MESSAGE) {
				if (s != null) {
					s = Strings.removeMarkup(s);
				}

				if ((yellMode == 0 || yellMode == 1 && isFriendOrSelf(s)) && !isInIgnoreList(s)) {
					if (j > k1 - 14 && j <= k1 && s != null && !s.equals(myPlayer.name)) {
						buildMessageOrAddFriend(s, false);
					}
					l++;
				}
			}
		}
	}
	
	private void buildItemSearchRightClickMenu() {
		if (amountOrNameInput.isEmpty() || totalItemResults == 0) {
			return;
		}

		boolean isFixed = isFixed();
		int yPosOffset = isFixed ? (GraphicsDisplay.enabled ? 338 : 0) : clientHeight - 165;
		int yOffset2 = super.mouseY - (isFixed ? 338 : yPosOffset);

		boolean canSpawn = myPrivilege == 2 || myPrivilege == 3 || myPrivilege == 8;

		for (int idx = 0; idx < totalItemResults; idx++) {
			String name = itemResultNames[idx];
			int textY = (20 + idx * 14) - itemResultScrollPos;
			if (yOffset2 > textY - 13 && yOffset2 <= textY + 2 && super.mouseX > 74 && super.mouseX < 495) {
				if (canSpawn) {
					MenuManager.addOption("Spawn <col=ff9040>" + name, 1251, itemResultIDs[idx]);
				}

				MenuManager.addOption("Select <col=ff9040>" + name, 1250, itemResultIDs[idx]);
			}
		}
	}
	
	private void buildChatAreaMenu(int j) {
		if (messagePromptRaised || chatTypeView == 5) {
			return;
		}

		if (chatTypeView == 1) {
			buildPublicChat(j);
			return;
		}
		if (chatTypeView == 2) {
			buildFriendChat(j);
			return;
		}
		if (chatTypeView == 3 || chatTypeView == 4) {
			buildDuelorTrade(j);
			return;
		}
		if (chatTypeView == 11) { // clan chat mode clicked
			buildClanChat(j);
			return;
		}
		
		if (chatTypeView == 12) { // yell chat mode clicked
			buildYellChat(j);
			return;
		}
		int mssageCount = 0;
		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			int messageY = (111 - mssageCount * 14) + anInt1089 + 6;
			if (messageY < -23) {
				break;
			}

			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int j1 = message.type;
			String s = message.name;

			if (s != null) {
				s = Strings.removeMarkup(s);
			}

			if (j1 == 0)
				mssageCount++;
			
			boolean ignored = isInIgnoreList(s);
			boolean isFriend = isFriendOrSelf(s);
			if ((j1 == 1 || j1 == 2) && (j1 == 1 || publicChatMode == 0 || publicChatMode == 1 && isFriend)
					&& !ignored) {
				if (j > messageY - 14 && j <= messageY && !s.equals(myPlayer.name)) {
					buildMessageOrAddFriend(s, false);
				}
				mssageCount++;
			}
			if ((j1 == 3 || j1 == 7) && splitPrivateChat == 0
					&& (j1 == 7 || privateChatMode == 0 || privateChatMode == 1 && isFriend) && !ignored) {
				if (j > messageY - 14 && j <= messageY) {
					buildMessageOrAddFriend(s, false);
				}
				mssageCount++;
			}
			if (j1 == 4 && (tradeMode == 0 || tradeMode == 1 && isFriend) && !ignored) {
				if (j > messageY - 14 && j <= messageY) {
					MenuManager.addOption("Accept trade <col=ffffff>" + s, 484);
				}
				mssageCount++;
			}
			if ((j1 == 5 || j1 == 6) && splitPrivateChat == 0 && privateChatMode < 2)
				mssageCount++;
			if (j1 == 8 && (tradeMode == 0 || tradeMode == 1 && isFriend) && !ignored) {
				if (j > messageY - 14 && j <= messageY) {
					MenuManager.addOption("Accept challenge <col=ffffff>" + s, 6);
				}
				mssageCount++;
			}
			if (j1 == ChatMessage.DUNG_PARTY_REQUEST && (tradeMode == 0 || tradeMode == 1 && isFriend) && !ignored) {
				if (j > messageY - 14 && j <= messageY) {
					MenuManager.addOption("Accept invite <col=ffffff>" + s, 485);
				}
				mssageCount++;
			}
			if (j1 == ChatMessage.GAMBLE_REQUEST && (tradeMode == 0 || tradeMode == 1 && isFriend) && !ignored) {
				if (j > messageY - 14 && j <= messageY) {
					MenuManager.addOption("Accept gamble <col=ffffff>" + s, 486);
				}
				mssageCount++;
			}
			if (j1 == ChatMessage.ADVERT_MESSAGE && showAutoChat)
				mssageCount++;
			if (j1 == ChatMessage.CLAN_MESSAGE && (clanChatMode == 0 || clanChatMode == 1 && isFriend) && !ignored) {
				if (j > messageY - 14 && j <= messageY && s != null && !s.equals(myPlayer.name)) {
					buildMessageOrAddFriend(s, false);
				}
				mssageCount++;
			}
			
			if (j1 == ChatMessage.YELL_MESSAGE && (yellMode == 0 || yellMode == 1 && isFriend) && !ignored) {
				if (j > messageY - 14 && j <= messageY && s != null && !s.equals(myPlayer.name)) {
					buildMessageOrAddFriend(s, false);
				}
				mssageCount++;
			}
			
		}
	}
	
	public String getWorld(int j, String color) {
		
		// if (friendsNodeIDs[j] - 9 == 1) { // soulEco
		// return "<col=ffff>NA "+color+"Eco";
		// } else if (friendsNodeIDs[j] - 9 == 2) {
		// return "<col=ffff>EU "+color+"Eco";
		// } else if (friendsNodeIDs[j] - 9 == 3) {
		// return "<col=ffff>NA "+color+"Pvp";
		// } else if (friendsNodeIDs[j] - 9 == 4) {
		// return "<col=ffff>EU "+color+"Pvp";
		// } else if (friendsNodeIDs[j] - 9 == 5) {
		// return color+" Spawn";
		// }
		
		return color + "World " + (friendsNodeIDs[j] - 9);
	}
	
	private boolean teleportResultsUpdate;
	private static final int[] teleportLayers = { 25653, 25853, 26053, 26253, 26453, 26653, 26853, 27053, 27253, 27453,
			27753, 28253, 28453, 28753, 28953, 29153, 29253, 29453 };
	private static final List<Integer> teleportMatches = new ArrayList<Integer>();
	
	public static boolean normalizeBitToggled(int base, int input) {
		int varId = base + input / 31;
		int value = variousSettings[varId];
		int bitId = input % 31;
		return (value & 1 << bitId) != 0;
	}

	public void drawFriendsListOrWelcomeScreen(RSInterface class9) {
		int j = class9.contentType;
		if (j == 1358) {//clan chat sidebar
			int clanMates = 0;
			for (int i = 18155; i < 18244; i++) {
				RSInterface line = RSInterface.interfaceCache[i];
				if (line.message.isEmpty()) {
					continue;
				}

				clanMates++;
			}
			class9.scrollMax = (clanMates * 14) + class9.height + 1;
		} else if (j == 1359) {//clan setup lists
			int members = 0;
			int length = class9.id + 1 + 100;
			for (int i = class9.id + 1; i < length; i++) {
				RSInterface line = RSInterface.interfaceCache[i];
				if (line != null && line.message != null && !line.message.isEmpty()) {
					members++;
				}
			}
			class9.scrollMax = (members * 14) + 1;
		} else if (j == 1360) {
			if (getInputDialogState() == INSTANCE_INPUT) {
				class9.message = amountOrNameInput;
				if (loopCycle % 40 < 20) {
					class9.message += "|";
				}
			} else {
				class9.message = "Key...";
			}
		} else if (j == 1361) {
			if (getInputDialogState() == POWER_UP_INPUT) {
				class9.message = amountOrNameInput;
				if (loopCycle % 40 < 20) {
					class9.message += "|";
				}
			} else {
				class9.message = "Search...";
			}
		} else if (j == 1350) {
			if (getInputDialogState() == TELEPORT_SEARCH_STATE) {
				class9.message = amountOrNameInput;
				if (loopCycle % 40 < 20) {
					class9.message += "|";
				}
			} else {
				class9.message = "Search...";
			}
		} else if (j == 1351) {
			if (teleportResultsUpdate) {
				
				teleportMatches.clear();
				
				int layerId = 0;
				for (List<TeleportInfo> list : TeleportInfo.values.values()) {
					int childId = 1;
					for (int a = 0, length = list.size(); a < length; a++) {
						int realId = childId;
						childId += 4;
						
						TeleportInfo info = list.get(a);
						if (info.searchKeywords == null) {
							continue;
						}
						
						for (int b = 0, length2 = info.searchKeywords.length; b < length2; b++) {
							if (info.searchKeywords[b].indexOf(amountOrNameInput) == -1) {
								continue;
							}
							
							int id = teleportLayers[layerId] + realId;
							if (!teleportMatches.contains(id))
								teleportMatches.add(id);
						}
					}
					
					layerId++;
				}
				
				int teleportCount = teleportMatches.size();
				class9.totalChildren(teleportCount * 4);
				int childID = 0;
				int y = 0;
				for (int i = 0; i < teleportCount; i++) {
					int tempId = teleportMatches.get(i);
					class9.child(childID++, tempId++, 0, y);
					class9.child(childID++, tempId++, 4, y);
					class9.child(childID++, tempId++, 146, y);
					class9.child(childID++, tempId++, 240, y);
					y += 30;
				}
				
				if (y <= class9.height) {
					y = class9.height + 1;
				}
				
				class9.scrollMax = y;
				class9.hidden = false;
				teleportResultsUpdate = false;
			}
		} else if (j >= 1000 && j < 1250) {
			int normalized = j - 1000;
			int varId = 8013 + normalized / 31;
			int value = variousSettings[varId];
			int bitId = normalized % 31;
			if ((value & 1 << bitId) != 0) {
				class9.hidden = false;
			} else {
				class9.hidden = true;
			}
		} else if (j == 1337) {
			int capture = variousSettings[8000];
			int rgb;
			if (capture == 1) {
				rgb = 0xaf;
			} else if (capture == 2) {
				rgb = 0xaf0000;
			} else {
				rgb = 0xafafaf;
			}
			
			class9.disabledColor = rgb;
		} else if (j == 1338) {
			int capture = variousSettings[8001];
			int rgb;
			if (capture == 1) {
				rgb = 0xaf;
			} else if (capture == 2) {
				rgb = 0xaf0000;
			} else {
				rgb = 0xafafaf;
			}
			
			class9.disabledColor = rgb;
		} else if (j == 1339) {
			int capture = variousSettings[8002];
			int rgb;
			if (capture == 1) {
				rgb = 0xaf;
			} else if (capture == 2) {
				rgb = 0xaf0000;
			} else {
				rgb = 0xafafaf;
			}
			
			class9.disabledColor = rgb;
		} else if (j == 1340) {// capture bar
			int capture = variousSettings[8003];
			if (capture == -1) {
				class9.hidden = true;
				RSInterface.interfaceCache[29271].hidden = true;
			} else {
				int color = 0xaf00;
				if (capture > 50) {
					capture -= 50;
					color = 0xaf0000;
				} else if (capture < 50) {
					capture = 50 - capture;
					color = 0xaf;
				} else {
					capture = 0;
				}
				
				int width = 116;
				int max = 50;
				int scale = capture * width / max;
				class9.disabledColor = color;
				class9.width = scale;
				
				class9.hidden = false;
				RSInterface.interfaceCache[29271].hidden = false;
			}
		} else if (j == 1341) {// activity bar
			int capture = variousSettings[8004];
			
			int height = 149;
			int max = 1000;
			int scale = capture * height / max;
			class9.height = scale;
			int offsetY = height - scale;
			class9.yOffset = offsetY;
		} else if (j == 1342) {
			int id = variousSettings[8007];
			if (id <= 0) {
				class9.disabledMediaID = -1;
				return;
			}
			
			class9.disabledAnimation = variousSettings[8006];
			class9.disabledMediaID = id;
			class9.modelZoom = variousSettings[8008];
		} else if (j == 1343) {
			int maxHp = variousSettings[8010];
			if (maxHp == 0) {
				return;
			}
			
			int currentHp = variousSettings[8009];
			int width = 148;
			int scale = (int) ((long) currentHp * width / maxHp);
			class9.width = scale;
		} else if (j == 1344) {
			int maxXp = variousSettings[8012];
			if (maxXp == 0) {
				return;
			}
			
			int currentXp = variousSettings[8011];
			int width = 254;
			int scale = (int) ((long) currentXp * width / maxXp);
			class9.width = scale;
		} else if (j >= 1 && j <= 100 || j >= 701 && j <= 800) {
			if (j == 1 && anInt900 == 0) {
				class9.message = "Loading friend list";
				class9.atActionType = 0;
				return;
			}
			if (j == 1 && anInt900 == 1) {
				class9.message = "Connecting to friendserver";
				class9.atActionType = 0;
				return;
			}
			if (j == 2 && anInt900 != 2) {
				class9.message = "Please wait...";
				class9.atActionType = 0;
				return;
			}
			int k = friendsCount;
			if (anInt900 != 2)
				k = 0;
			if (j > 700)
				j -= 601;
			else
				j--;
			if (j >= k) {
				class9.message = "";
				class9.atActionType = 0;
				return;
			} else {
				class9.message = friendsList[j];
				class9.atActionType = 1;
				return;
			}
		} else if (j >= 101 && j <= 200 || j >= 801 && j <= 900) {
			int l = friendsCount;
			if (anInt900 != 2)
				l = 0;
			if (j > 800)
				j -= 701;
			else
				j -= 101;
			if (j >= l) {
				class9.message = "";
				class9.atActionType = 0;
				return;
			}
			if (friendsNodeIDs[j] == 0) {
				class9.message = "Offline";
				class9.disabledColor = 0xff0000;
			} else if (friendsNodeIDs[j] == nodeID) { // same world
				class9.message = getWorld(j, "");
				class9.disabledColor = 0xff00;
			} else { // different world
				class9.message = getWorld(j, "");
				class9.disabledColor = 0xffff00;
			}

			class9.atActionType = 1;
			return;
		} else if (j == 203) {
			int i1 = friendsCount;
			if (anInt900 != 2)
				i1 = 0;
			class9.scrollMax = i1 * 15 + 20;
			if (class9.scrollMax <= class9.height)
				class9.scrollMax = class9.height + 1;
			return;
		} else if (j >= 401 && j <= 500) {
			if ((j -= 401) == 0 && anInt900 == 0) {
				class9.message = "Loading ignore list";
				class9.atActionType = 0;
				return;
			}
			if (j == 1 && anInt900 == 0) {
				class9.message = "Please wait...";
				class9.atActionType = 0;
				return;
			}
			int j1 = ignoreCount;
			if (anInt900 == 0)
				j1 = 0;
			if (j >= j1) {
				class9.message = "";
				class9.atActionType = 0;
				return;
			} else {
				class9.message = TextClass.fixName(TextClass.nameForLong(ignoreListAsLongs[j]));
				class9.atActionType = 1;
				return;
			}
		} else if (j == 503) {
			class9.scrollMax = ignoreCount * 15 + 20;
			if (class9.scrollMax <= class9.height)
				class9.scrollMax = class9.height + 1;
			return;
		} else if (j == 327) {
			class9.modelRotationY = 150;
			class9.modelRotationX = (int) (Math.sin(loopCycle / 40D) * 256D) & 0x7ff;
			if (aBoolean1031) {
				for (int k1 = 0; k1 < 7; k1++) {
					int l1 = anIntArray1065[k1];
					if (l1 >= 0 && !IDK.cache[l1].method537())
						return;
				}
				
				aBoolean1031 = false;
				Model aclass30_sub2_sub4_sub6s[] = new Model[7];
				int i2 = 0;
				for (int j2 = 0; j2 < 7; j2++) {
					int k2 = anIntArray1065[j2];
					if (k2 >= 0)
						aclass30_sub2_sub4_sub6s[i2++] = IDK.cache[k2].method538();
				}
				
				Model model = new Model(i2, aclass30_sub2_sub4_sub6s);
				for (int l2 = 0; l2 < 5; l2++) {
					if (anIntArray990[l2] != 0) {
						model.replaceHs1(anIntArrayArray1003[l2][0],
								anIntArrayArray1003[l2][anIntArray990[l2]]);

						if (l2 == 4) {
							model.replaceHs1(Client.skinColor2[0], Client.skinColor2[anIntArray990[l2]]);
						}

						if (l2 == 1) {
							model.replaceHs1(anIntArray1204[0],
									anIntArray1204[anIntArray990[l2]]);
						}
					}
				}
				
				model.applyEffects();
				model.applyAnimation(
						Animation.anims[myPlayer.anInt1511].frame2Ids[0]);
				
				model.calculateLighting(64, 1300, 0, -570, 0, true);
				class9.disabledMediaType = 7;
				class9.disabledMediaID = 0;
				RSInterface.modelCache.put(model, RSInterface.bitPackModel(class9.disabledMediaType, class9.disabledMediaID, false));
			}
			return;
		} else if (j == 328) {
			class9.modelRotationY = 150;
			class9.modelRotationX = (int) (Math.sin(loopCycle / 40D) * 256D) & 0x7ff;
			class9.disabledMediaType = 7;
			class9.disabledMediaID = 0;
			Model model = null;
			if (myPlayer != null && myPlayer.appearance != null) {
				model = myPlayer.appearance.method452(null, null, -1, -1, 0, -1, -1, 0);
			}

			RSInterface.modelCache.put(model, RSInterface.bitPackModel(class9.disabledMediaType, class9.disabledMediaID, false));
			return;
		} else if (j == 324) {
			if (aClass30_Sub2_Sub1_Sub1_931 == null) {
				aClass30_Sub2_Sub1_Sub1_931 = class9.disabledSprite;
				aClass30_Sub2_Sub1_Sub1_932 = class9.enabledSprite;
			}
			if (aBoolean1047) {
				class9.disabledSprite = aClass30_Sub2_Sub1_Sub1_932;
				return;
			} else {
				class9.disabledSprite = aClass30_Sub2_Sub1_Sub1_931;
				return;
			}
		} else if (j == 325) {
			if (aClass30_Sub2_Sub1_Sub1_931 == null) {
				aClass30_Sub2_Sub1_Sub1_931 = class9.disabledSprite;
				aClass30_Sub2_Sub1_Sub1_932 = class9.enabledSprite;
			}
			if (aBoolean1047) {
				class9.disabledSprite = aClass30_Sub2_Sub1_Sub1_931;
				return;
			} else {
				class9.disabledSprite = aClass30_Sub2_Sub1_Sub1_932;
				return;
			}
		} else if (j == 600) {
			class9.message = reportAbuseInput;
			if (loopCycle % 20 < 10) {
				class9.message += "|";
				return;
			} else {
				class9.message += " ";
				return;
			}
		} else if (j == 613) {
			if (myPrivilege >= 1) {
				if (canMute) {
					class9.disabledColor = 0xff0000;
					// class9.message =
					// "Moderator option: Mute player for 48 hours: <ON>";
				} else {
					class9.disabledColor = 0xffffff;
					// class9.message =
					// "Moderator option: Mute player for 48 hours: <OFF>";
				}
			} else {
				class9.message = "";
			}
		} else if (j == 650 || j == 655) {
			if (anInt1193 != 0) {
				String s;
				if (daysSinceLastLogin == 0)
					s = "earlier today";
				else if (daysSinceLastLogin == 1)
					s = "yesterday";
				else
					s = daysSinceLastLogin + " days ago";
				class9.message = "You last logged in " + s + " from: " + TextClass.method586(anInt1193);
			} else {
				class9.message = "";
			}
		} else if (j == 651) {
			if (unreadMessages == 0) {
				class9.message = "0 unread messages";
				class9.disabledColor = 0xffff00;
			}
			if (unreadMessages == 1) {
				class9.message = "1 unread message";
				class9.disabledColor = 65280;
			}
			if (unreadMessages > 1) {
				class9.message = unreadMessages + " unread messages";
				class9.disabledColor = 65280;
			}
		} else if (j == 652) {
			if (daysSinceRecovChange == 201) {
				if (membersInt == 1)
					class9.message = "<col=ffff00>This is a non-members world: <col=ffffff>Since you are a member we";
				else
					class9.message = "";
			} else if (daysSinceRecovChange == 200) {
				class9.message = "You have not yet set any password recovery questions.";
			} else {
				String s1;
				if (daysSinceRecovChange == 0)
					s1 = "Earlier today";
				else if (daysSinceRecovChange == 1)
					s1 = "Yesterday";
				else
					s1 = daysSinceRecovChange + " days ago";
				class9.message = s1 + " you changed your recovery questions";
			}
		} else if (j == 653) {
			if (daysSinceRecovChange == 201) {
				if (membersInt == 1)
					class9.message = "<col=ffffff>recommend you use a members world instead. You may use";
				else
					class9.message = "";
			} else if (daysSinceRecovChange == 200)
				class9.message = "We strongly recommend you do so now to secure your account.";
			else
				class9.message = "If you do not remember making this change then cancel it immediately";
		} else if (j == 654) {
			if (daysSinceRecovChange == 201)
				if (membersInt == 1) {
					class9.message = "<col=ffffff>this world but member benefits are unavailable whilst here.";
					return;
				} else {
					class9.message = "";
					return;
				}
			if (daysSinceRecovChange == 200) {
				class9.message = "Do this from the 'account management' area on our front webpage";
				return;
			}
			class9.message = "Do this from the 'account management' area on our front webpage";
		}		
	}
	
	public final static int SEARCH_STATE = 3;
	
	public final static int SET_SELL_PRICE_STATE = 4;
	
	public final static int SET_DIALOG_AMOUNT_TO_WITHDRAW = 5;
	
	public final static int TELEPORT_SEARCH_STATE = 6;

	public final static int SET_DIALOG_AMOUNT_TO_SELL = 8;

	public final static int INSTANCE_INPUT = 9;
	public final static int POWER_UP_INPUT = 10;
	
	public int searchUpdateCycle;
	public int totalItemResults;
	
	public String itemResultNames[] = new String[100];
	
	public int itemResultIDs[] = new int[100];
	public int itemResultsMaxScroll;
	public int itemResultScrollPos;
	
	public void itemSearch(String itemName) {
		if (itemName == null || itemName.length() == 0) {
			totalItemResults = 0;
			return;
		}
		String searchName = itemName;
		String searchParts[] = new String[100];
		int totalResults = 0;
		do {
			int regex = searchName.indexOf(" ");
			if (regex == -1) {
				break;
			}
			String part = searchName.substring(0, regex).trim();
			if (part.length() > 0) {
				searchParts[totalResults++] = part.toLowerCase();
			}
			searchName = searchName.substring(regex + 1);
		} while (true);
		searchName = searchName.trim();
		if (searchName.length() > 0) {
			searchParts[totalResults++] = searchName.toLowerCase();
		}

		totalItemResults = 0;
		firstLoop: for (int id = 0; id < 31000; id++) {
			ItemDef item = ItemDef.forID(id);
			if (item == null || item.certTemplateID != -1 || item.lentItemID != -1 || item.excludeFromSearch || item.name.equalsIgnoreCase("null")) {
				continue;
			}

			String resultName = item.name.toLowerCase();
			for (int index = 0; index < totalResults; index++) {
				if (resultName.indexOf(searchParts[index]) == -1) {
					continue firstLoop;
				}
			}

			itemResultNames[totalItemResults] = capitalize(resultName);
			itemResultIDs[totalItemResults] = id;
			totalItemResults++;
			if (totalItemResults >= itemResultNames.length) {
				return;
			}
		}

		secondLoop: for (int id = 0; id < ItemDef.totalOsrsItems; id++) {
			int itemId = id + ItemDef.OSRS_OFFSET;

			ItemDef item = ItemDef.forIDOsrs(itemId);
			if (item == null || !ItemDef.availableOsrsItems.contains(itemId) || item.certTemplateID != -1 || item.lentItemID != -1 || item.excludeFromSearch || item.name.equalsIgnoreCase("null")) {
				continue;
			}

			String resultName = item.name.toLowerCase();
			for (int index = 0; index < totalResults; index++) {
				if (resultName.indexOf(searchParts[index]) == -1) {
					continue secondLoop;
				}
			}

			itemResultNames[totalItemResults] = capitalize(resultName);
			itemResultIDs[totalItemResults] = itemId;
			totalItemResults++;
			if (totalItemResults >= itemResultNames.length) {
				return;
			}
		}
	}

	private void drawSplitPrivateChat() {
		if (splitPrivateChat == 0) {
			return;
		}

		int messageCount = 0;
		if (anInt1104 != 0) {
			messageCount++;
		}

		if (announcement != null) {
			messageCount++;
		}

		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager.getPrevMessageId(index)) {
			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int type = message.type;
			String s = message.name;

			if ((type == ChatMessage.PRIVATE_MESSAGE_RECEIVED_PLAYER || type == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF)
					&& (type == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF || privateChatMode == 0 || privateChatMode == 1 && isFriendOrSelf(s))
					&& !isInIgnoreList(s)) {
				int l = (clientHeight - 174) - messageCount * 13;
				int k1 = 4;
				newRegularFont.drawBasicString("From", k1, l - 1, 65535, 0);
				k1 += newRegularFont.getTextWidth("From ");
				newRegularFont.drawBasicString(s + ": " + message.message, k1, l - 1, 65535, 0);
				if (++messageCount >= 5)
					return;
			}
			if (type == ChatMessage.PRIVATE_MESSAGE_RECEIVED && privateChatMode < 2) {
				int i1 = (clientHeight - 174) - messageCount * 13;
				newRegularFont.drawBasicString(message.message, 4, i1 - 1, 65535, 0);
				if (++messageCount >= 5)
					return;
			}
			if (type == ChatMessage.PRIVATE_MESSAGE_SENT && privateChatMode < 2) {
				int j1 = (clientHeight - 174) - messageCount * 13;
				int k1 = 4;
				newRegularFont.drawBasicString("To " + s + ": " + message.message, k1, j1 - 1, 65535, 0);
				if (++messageCount >= 5)
					return;
			}
		}
	}

	public void pushGameMessage(String message) {
		pushMessage(message, ChatMessage.GAME_MESSAGE, null);
	}

	public void pushMessage(String message, int type) {
		pushMessage(message, type, null);
	}

	public void pushMessage(String message, int type, String name) {
		if (type == ChatMessage.GAME_MESSAGE && dialogID != -1) {
			aString844 = message;
			super.clickMode3 = 0;
		}
		if (backDialogID == -1) {
			inputTaken = true;
		}

		ChatMessageManager.appendMessage(type, message, null, name);//TODO clan shit
	}

	public void setNorth() {
		Camera.cameraOffsetX = 0;
		Camera.cameraOffsetY = 0;
		viewRotationOffset = 0;
		viewRotation = 0;
		minimapRotation = 0;
		minimapZoom = 0;
	}
	
	public void drawScreen() {
		if (chatIP != null && !toggleGl.get()) {
			return;
		}
		
		fullGameScreen = null;

		if (GraphicsDisplay.enabled) {
			GL11.glClearColor(0, 0, 0, 0);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		}
		
		if (chatIP == null || chatIP.canvasRaster == null) {
			chatIP = new RSImageProducer(519, 165, canvas);
			if (!GraphicsDisplay.enabled) {
				Raster.reset();
			}
		}
		
		if (mapIP == null || mapIP.canvasRaster == null) {
			mapIP = new RSImageProducer(249, 168, canvas);
			if (!GraphicsDisplay.enabled) {
				Raster.reset();
			}
		}
		
		if (inventoryIP == null || inventoryIP.canvasRaster == null) {
			inventoryIP = new RSImageProducer(250, 335, canvas);
			if (!GraphicsDisplay.enabled) {
				Raster.reset();
			}
		}
		
		// gameScreenIP = new RSImageProducer(clientWidth, clientHeight);
		int width = 512;
		int height = 334;
		if (!isFixed() || GraphicsDisplay.enabled) {
			width = clientWidth;
			height = clientHeight;
		}
		gameScreenIP = new RSImageProducer(width, height, canvas);
		
		if (!GraphicsDisplay.enabled) {
			mapBack562.drawSprite(0, 0);
			Raster.reset();
		}
		
		if (!GraphicsDisplay.enabled) {
			if (leftFrame == null || leftFrame.canvasRaster == null) {
				leftFrame = new RSImageProducer(leftFrameSprite.myWidth, leftFrameSprite.myHeight, canvas);
				leftFrameSprite.method346(0, 0);
			}
			
			if (topFrame == null || topFrame.canvasRaster == null) {
				topFrame = new RSImageProducer(topFrameSprite.myWidth, topFrameSprite.myHeight, canvas);
				topFrameSprite.method346(0, 0);
			}
		}
		
		fullRedraw = true;
	}

	public static final int[] CATEGORY_LAYERS = {66313, 66324, 66325, 66326, 66327};
	public static final int[] ICON_IDS = {66236, 66237, 66238, 66239, 66240};
	public static final int[] CATEGORY_BARS = {66210, 66215, 66220, 66225, 66230};
	public static final int[] CATEGORY_NAMES = {66211, 66216, 66221, 66226, 66231};
	public static final int[] CATEGORY_PERCENTS = {66212, 66217, 66222, 66227, 66232};

	private void updateTaskInterface(RSBuffer inStream) {
		int categoryLength = inStream.readUnsignedByte();
		for (int categoryId = 0; categoryId < categoryLength; categoryId++) {
			int layerId = CATEGORY_LAYERS[categoryId];
			RSInterface layer = RSInterface.interfaceCache[layerId];
			layer.dynamicComponents = null;
			int componentId = 0;

			for (int taskId = 0; taskId < 7; taskId++) {
				int yOffset = taskId * 25 + 8;
				int completedCount = inStream.readDWord();
				int requiredCount = inStream.readDWord();
				String desc = inStream.readString();
				String timeLeft = "Unavailable.";
				int timeLeftColor = 0xff0000;
				if (calendar.currentDayOfWeek() - 1 == taskId) {
					int hoursLeft = 24 - calendar.get24Hour();
					if (hoursLeft > 1) {
						timeLeft = hoursLeft + " hours";
					} else {
						int minutesLeft = 60 - calendar.getMinutes();
						timeLeft = minutesLeft + " mins";
					}

					timeLeftColor = 0xeb981f;
				}

				layer.createDynamic(componentId++, RSInterface.addNewText(CalendarHandler.DAYS_SHORT[taskId], 5, yOffset, 30, 15, 0, 0xffffff, 0, true, 0, 0, 0));
				layer.createDynamic(componentId++, RSInterface.addNewText(desc, 42, yOffset, 200, 22, 0, 0xeb981f, 0, true, 0, 0, 0));
				layer.createDynamic(componentId++, RSInterface.addSprite(260, yOffset - 3, 1, 1, 989, -1, null));
				layer.createDynamic(componentId++, RSInterface.addRectangle(263, yOffset, completedCount * 137 / requiredCount, 13, 0x20C000, 100, true));
				layer.createDynamic(componentId++, RSInterface.addNewText(completedCount + " / " + requiredCount, 260, yOffset - 3, 142, 20, 0, 0xeb981f, 0, true, 1, 1, 0));
				layer.createDynamic(componentId++, RSInterface.addNewText(timeLeft, 413, yOffset - 3, 50, 21, 0, timeLeftColor, 0, true, 1, 1, 0));
			}
		}
	}

	private int hpHudCurrentHp;
	private int hpHudMaxHp;

	private void updateHPHud(RSBuffer inStream) {
		String name = inStream.readString();
		RSInterface.interfaceCache[67242].dynamicComponents[3].message = name;

		hpHudCurrentHp = inStream.readUnsignedWord();
		hpHudMaxHp = inStream.readUnsignedWord();
		RSInterface.interfaceCache[67248].message = hpHudCurrentHp + " / " + hpHudMaxHp;

		updateHpHudBars();
	}

	private void updateHpHudBars() {
		int fixedWidth = Math.min(1500, clientWidth);
		int interpolateWidth = interpolate(150, 300, 768, 1500, fixedWidth);
		int baseWidth = interpolateWidth - 4;

		RSInterface.interfaceCache[67243].width = interpolateWidth;
		RSInterface.interfaceCache[67244].width = interpolateWidth;
		RSInterface.interfaceCache[67245].width = interpolateWidth - 2;
		RSInterface.interfaceCache[67246].width = baseWidth;
		RSInterface.interfaceCache[67247].width = baseWidth;
		RSInterface.interfaceCache[67248].width = interpolateWidth;

		RSInterface.interfaceCache[67241].childX[0] = (short) (interpolateWidth / 2 - 80 / 2);

		int width = baseWidth;
		width = interpolate(0, width, 0, hpHudMaxHp, hpHudCurrentHp);
		if (width == 0 && hpHudCurrentHp > 0) {
			width = 1;
		}

		if (width == baseWidth && hpHudCurrentHp < hpHudMaxHp) {
			width--;
		}

		RSInterface.interfaceCache[67247].width = width;
	}

	private void updateRaidsHud(RSBuffer inStream) {
		int totalPoints = inStream.readDWord();
		int points = inStream.readDWord();
		int ticks = inStream.readUnsignedWord();
		String line1 = "Total:";
		String line1Val = Strings.formatNumbersWithCommas(totalPoints);

		String line2 = myPlayer.name + ":";
		String line2Val = Strings.formatNumbersWithCommas(points);

		String line3 = "Time:";
		String line3Val = formatTime(ticks);

		RSInterface.interfaceCache[67225].message = line1 + "<br>" + line2 + "<br>" + line3;
		RSInterface.interfaceCache[67226].message = line1Val + "<br>" + line2Val + "<br>" + line3Val;

		int interfaceWidth = newRegularFont.getTextWidth(line1) + 3 + newRegularFont.getTextWidth(line1Val);
		interfaceWidth = Math.max(interfaceWidth, newRegularFont.getTextWidth(line2) + 3 + newRegularFont.getTextWidth(line2Val));
		interfaceWidth = Math.max(interfaceWidth, newRegularFont.getTextWidth(line3) + 3 + newRegularFont.getTextWidth(line3Val));
		interfaceWidth += 16;

		RSInterface.interfaceCache[67222].width = interfaceWidth;
		RSInterface.interfaceCache[67223].width = interfaceWidth;
		RSInterface.interfaceCache[67224].width = interfaceWidth - 2;
		RSInterface.interfaceCache[67225].width = interfaceWidth - 16;
		RSInterface.interfaceCache[67226].width = interfaceWidth - 16;
	}

	private void updateDiscordInfo(RSBuffer inStream) {
		String state = inStream.readString();
		String details = inStream.readString();
		int partySize = inStream.readUnsignedWord();
		if (partySize == 65535) {
			partySize = -1;
		}
		int maxPartySize = inStream.readUnsignedWord();
		if (maxPartySize == 65535) {
			maxPartySize = -1;
		}
		long startTimestamp = inStream.readQWord();
		long endTimestamp = inStream.readQWord();
		String smallImageKey = inStream.readString();
		
		Client.DISCORD_PRESENCE.updateDiscord(state, details, partySize, maxPartySize, startTimestamp, endTimestamp, smallImageKey);
	}
	
	private String formatTime(int ticks) {
		int seconds = 60 * Math.max(ticks, 0) / 100;
		int minutes = seconds / 60;
		seconds %= 60;
		int hours = minutes / 60;
		minutes %= 60;

		StringBuilder result = new StringBuilder();

		if (hours > 0) {
			result.append(hours);
			result.append(":");

			if (minutes < 10) {
				result.append("0");
			}
		}

		result.append(minutes);
		result.append(":");

		if (seconds < 10) {
			result.append("0");
		}
		result.append(seconds);

		return result.toString();
	}

	private void updateRaidsSidebar(RSBuffer inStream) {
		RSInterface layer = RSInterface.interfaceCache[67214];
		layer.dynamicComponents = null;

		int yOff = 0;
		int componentId = 0;
		int count = inStream.readUnsignedByte();
		for (int i = 0; i < count; i++) {
			String memberName = inStream.readString();
			if (memberName.isEmpty()) {
				continue;
			}

			int combat = inStream.readUnsignedByte();
			int totalLevel = inStream.readUnsignedWord();
			layer.createDynamic(componentId++, RSInterface.addRectangle(0, yOff, 166, 13, 16777215, i % 2 == 0 ? 255 : 235, true));
			layer.createDynamic(componentId++, RSInterface.addNewText(memberName, 2, yOff, 108, 14, 0, 16750623, 0, true, 1, 1, 0));
			layer.createDynamic(componentId++, RSInterface.addNewText(Integer.toString(combat), 110, yOff, 20, 14, 0, 16750623, 0, true, 1, 1, 0));
			layer.createDynamic(componentId++, RSInterface.addNewText(Integer.toString(totalLevel), 139, yOff, 20, 14, 0, 16750623, 0, true, 1, 1, 0));
			yOff += 14;
		}

		if (yOff < layer.height) {
			layer.scrollMax = layer.height + 1;
		} else {
			layer.scrollMax = yOff;
		}
	}

	private void updateRagInterface(RSBuffer inStream) {
		RSInterface layer = RSInterface.interfaceCache[67014];
		layer.dynamicComponents = null;

		int yOff = 0;
		int componentId = 0;
		int count = inStream.readUnsignedByte();
		for (int i = 0; i < count; i++) {
			String name = inStream.readString();
			layer.createDynamic(componentId++, RSInterface.addNewText(name, 2, yOff, 175, 17, 1, 0xffffff, 0, true, 1, 1, 0));
			layer.createDynamic(componentId++, RSInterface.addButton(175, yOff + 2, 709, 710, "Remove"));
			yOff += 18;
		}
	}
	
	private void sendExpDrop(RSBuffer inStream) {
		int skillId = inStream.readUnsignedByte();
		int expAmount = inStream.readDWord();
		
		if (drawXpBar) {
			drawFlag = true;
		}
		
		if (drawFlag) {
			xpToDraw += expAmount;
		}
	}

	private void updateTaskSideBarInterface(RSBuffer inStream) {
		int categoryLength = inStream.readUnsignedByte();
		int totalTasks = 7;
		for (int categoryId = 0; categoryId < categoryLength; categoryId++) {
			int iconId = inStream.readUnsignedWord();
			RSInterface.interfaceCache[ICON_IDS[categoryId]].disabledSprite = SpriteCache.get(iconId);
			String categoryName = inStream.readString();
			RSInterface.interfaceCache[CATEGORY_NAMES[categoryId]].message = categoryName;

			int completedCount = inStream.readUnsignedByte();
			RSInterface.interfaceCache[CATEGORY_BARS[categoryId]].width = completedCount * 118 / totalTasks;
			int math = (completedCount * 1000 / totalTasks);
			int percent = math / 10;
			int remaind = math % 10;
			RSInterface.interfaceCache[CATEGORY_PERCENTS[categoryId]].message = percent + "." + remaind + "%";
		}
	}

	private void updatePollInterface(RSBuffer inStream) {
		boolean alreadyVoted = inStream.readUnsignedByte() == 1;
		RSInterface.interfaceCache[59006].hidden = alreadyVoted;
		RSInterface.interfaceCache[59007].hidden = alreadyVoted;
		
		String title = inStream.readString();
		RSInterface.interfaceCache[59002].message = title;
		String info = inStream.readString();
		RSInterface infoInterface = RSInterface.interfaceCache[59011];
		infoInterface.message = info;
		
		RSFont font = chooseFont(infoInterface.textDrawingAreas);
		int height = font.paragraphHeigth(info, infoInterface.width) * font.baseCharacterHeight;
		int totalVotesHeight = height + 10;
		int pollStartHeight = totalVotesHeight + chooseFont(RSInterface.interfaceCache[59012].textDrawingAreas).baseCharacterHeight + 10;
		//System.out.println(height+":"+font.baseCharacterHeight);
		
		int pollCount = inStream.readUnsignedByte();
		//System.out.println("pollcount: " + pollCount);
		RSInterface scroll = RSInterface.interfaceCache[59010];

		int configCode = 8050;
		int idMultiplier = 17;
		int voteID = 59016;
		int totalVotes = 0;
		
		scroll.totalChildren(pollCount * idMultiplier + 2);
		for (int i = 0; i < pollCount; i++) {
			String text = inStream.readString();
		    int startX = 2;
		    int startY = pollStartHeight + (i * 119); //101
		    int questionX = startX + 4;
		    int questionY = startY + 4; 
		    int descriptionX = startX + 4;
		    int descriptionY = questionY + 13;
		    int selectTextX = startX + 25;
		    int selectTextY = startY + 61;
		    int selectButtonX = startX + 5;
		    int selectButtonY = startY + 59;
		    int voteAmountTextX = startX + 390;
		    int voteAmountTextY = startY + 60;
		    int progressBarX = startX + 85;
		    int progressBarY = startY + 59;
		    
			int voteYes = inStream.readUnsignedWord();
			int voteNo = inStream.readUnsignedWord();
			int voteSkip = inStream.readUnsignedWord();
			totalVotes += voteYes;
			totalVotes += voteNo;
			totalVotes += voteSkip;

			int indexOffset = i * idMultiplier;
			int idOffset = voteID + indexOffset;
			
			int totalVotesPoll = voteYes + voteNo;
			int yesPercent = Math.round(voteYes * 1000f / totalVotesPoll);
			int noPercent = Math.round(voteNo * 1000f / totalVotesPoll);
			int yesWidth = yesPercent * 253 / 1000;
			int noWidth = noPercent * 253 / 1000;
		    
		    RSInterface.addSprite(idOffset, 701, 460, 83);
		    RSInterface.addRectangle(idOffset + 1, yesPercent >= 700 ? 50 : 255, 0x3f542c, true, 458, 113, 0);// 0xaf, 0xaf0000
		    RSInterface.interfaceCache[idOffset + 1].setFlag(RSInterface.RECT_IGNORE_ATLAS);
		    RSInterface.addNewText(idOffset + 2, "Question " + (i + 1), 40, 10, 0, 0xff9b00, 0, true, 0, 0, 0);
		    RSInterface.addNewText(idOffset + 3, text, 455, 30, 1, 0xffffff, 0, true, 0, 0, 0);
		    RSInterface.addText(idOffset + 4, "Yes", 0, 0xff9900, false, true, 14, 9, 0xffffff);
		    RSInterface.addText(idOffset + 5, "No", 0, 0xff9900, false, true, 9, 9, 0xffffff);
		    RSInterface.addText(idOffset + 6, "Skip question", 0, 0xd3d3d3, false, true, 63, 9, 0xffffff);
		    if (alreadyVoted) {
		    	RSInterface.addSprite(idOffset + 7, 980, 15, 15);
		    	RSInterface.addSprite(idOffset + 8, 980, 15, 15);
		    	RSInterface.addSprite(idOffset + 9, 980, 15, 15);
		    } else {
			    RSInterface.addRadioButton(idOffset + 7, 59010, 980, 981, 15, 15, "Select", 1, 5, configCode);
			    RSInterface.addRadioButton(idOffset + 8, 59010, 980, 981, 15, 15, "Select", 2, 5, configCode);
			    RSInterface.addRadioButton(idOffset + 9, 59010, 980, 981, 15, 15, "Select", 3, 5, configCode);
		    }
		    RSInterface.addNewText(idOffset + 10, (yesPercent / 10) + "." + (yesPercent % 10) + "% (" +  Strings.formatNumbersWithCommas(voteYes) + ")", 40, 10, 0, 0xff9b00, 0, true, 1, 0, 0);
		    RSInterface.addNewText(idOffset + 11, (noPercent / 10) + "." + (noPercent % 10) + "% (" +  Strings.formatNumbersWithCommas(voteNo) + ")", 40, 10, 0, 0xff9b00, 0, true, 1, 0, 0);
		    RSInterface.addNewText(idOffset + 12, "(" +  Strings.formatNumbersWithCommas(voteSkip) + ")", 40, 10, 0, 0xd3d3d3, 0, true, 1, 0, 0);
		    RSInterface.addSprite(idOffset + 13, 798, 140, 20);
		    RSInterface.addRectangle(idOffset + 14, 50, 0xffd700/*0xffd700*/, true, yesWidth, 13, 0);//silver 0xc0c0c0
		    RSInterface. addRectangle(idOffset + 15, 80, 0xffd700, true, noWidth, 13, 0);
			
		    scroll.child(indexOffset, idOffset, startX, startY);
		    scroll.child(indexOffset + 1, idOffset + 1, startX + 1, startY + 1);
		    scroll.child(indexOffset + 2, idOffset + 2, questionX, questionY);
		    scroll.child(indexOffset + 3, idOffset + 3, descriptionX, descriptionY);
		    scroll.child(indexOffset + 4, idOffset + 4, selectTextX, selectTextY);
		    scroll.child(indexOffset + 5, idOffset + 5, selectTextX, selectTextY + 18);
		    scroll.child(indexOffset + 6, idOffset + 6, selectTextX, selectTextY + 36);
		    scroll.child(indexOffset + 7, idOffset + 7, selectButtonX, selectButtonY);
		    scroll.child(indexOffset + 8, idOffset + 8, selectButtonX, selectButtonY + 18);
		    scroll.child(indexOffset + 9, idOffset + 9, selectButtonX, selectButtonY + 36);
		    scroll.child(indexOffset + 10, idOffset + 10, voteAmountTextX, voteAmountTextY);
		    scroll.child(indexOffset + 11, idOffset + 11, voteAmountTextX, voteAmountTextY + 18);
		    scroll.child(indexOffset + 12, idOffset + 12, voteAmountTextX, voteAmountTextY + 36);
		    scroll.child(indexOffset + 13, idOffset + 13, progressBarX, progressBarY);
		    scroll.child(indexOffset + 14, idOffset + 13, progressBarX, progressBarY + 17);
		    scroll.child(indexOffset + 15, idOffset + 14, progressBarX + 2, progressBarY + 2);
		    scroll.child(indexOffset + 16, idOffset + 15, progressBarX + 2, progressBarY + 17 + 2);

		    configCode++;
		}
		
		scroll.child(pollCount * idMultiplier, 59011, 3, 0);
		scroll.child(pollCount * idMultiplier + 1, 59012, 216, totalVotesHeight);
		
		scroll.scrollMax = (pollCount) * 119 + pollStartHeight;
		scroll.width = 470;
		scroll.height = 250;
		if (scroll.scrollMax < scroll.height) {
			scroll.scrollMax = scroll.height + 1;
		}
		
		RSInterface.interfaceCache[59012].message = "Total Votes: " + (pollCount == 0 ? 0 : (Strings.formatNumbersWithCommas(totalVotes / pollCount)));
	}
	
	private int getSeasonPassLevelForExp(int exp) {
		return exp / 40 + 1;
	}
	
	private void updateSeasonPassInterface(RSBuffer inStream) {
		String title = inStream.readString();
		if (title != null)
			RSInterface.interfaceCache[60002].message = title;
		
		int expireDays = inStream.readUnsignedWord();
		
		int myExp = inStream.readUnsignedWord();
		
		String expiredDayString = expireDays <= 0 ? "Expired!" : (Integer.toString(expireDays) + " days");
		
		int myLevel = getSeasonPassLevelForExp(myExp);
		
		int expForNextLvl = (myLevel) * 40;

		RSInterface.interfaceCache[60003].message = "Lvl: "+myLevel;
		RSInterface.interfaceCache[60009].message = "Exp: " + myExp + "/" + expForNextLvl;
		
		RSInterface.interfaceCache[60008].message = CustomInterfaces.seasonPassDetailString + expiredDayString;
		
//		RSFont font = chooseFont(infoInterface.textDrawingAreas);
		int height = 20;//font.paragraphHeigth(info, infoInterface.width) * font.baseCharacterHeight;
		int totalRewardsHeight = height + 10;
		int scrollStartHeight = totalRewardsHeight + 10;
		//System.out.println(height+":"+font.baseCharacterHeight);
		
		int rewardCount = inStream.readUnsignedByte();
		//System.out.println("pollcount: " + pollCount);
		RSInterface scroll = RSInterface.interfaceCache[60010];

		int idMultiplier = 5;
		int interfaceStartId = 60016;
		
		int scrollInt = 101;
		
		scroll.totalChildren(rewardCount * idMultiplier);
		for (int i = 0; i < rewardCount; i++) {
		    int startX = 160;
		    int startY = scrollStartHeight + (i * scrollInt); //101
		    int itemX = startX + 60;
		    int itemY = startY + 10; 
		    int lvlReqX = startX + 55;
		    int lvlRedY = itemY + 40;
		    int buttonX = lvlReqX - 20;
		    int buttonY = lvlRedY + 23;
		    int buttonTextX = buttonX + 20;
		    int buttonTextY = buttonY+2;
		    
		    
			int itemId = inStream.readUnsignedWord();
			int level = inStream.readUnsignedWord();

			int indexOffset = i * idMultiplier;
			int idOffset = interfaceStartId + indexOffset;
			
		    addRectangle(idOffset, 0, 0x777775, false, 150, 100, 0); // outline around the box
			RSInterface.addItemSprite(idOffset + 1, itemId);
			addNewText(idOffset + 2, "Level Req: " + level, 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
			addButton(idOffset + 3, 796, 797, "Claim");
			addNewText(idOffset + 4, "Claim", 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
			
			scroll.child(indexOffset, idOffset, startX, startY);
			scroll.child(indexOffset + 1, idOffset + 1, itemX, itemY);
			scroll.child(indexOffset + 2, idOffset + 2, lvlReqX, lvlRedY);
			scroll.child(indexOffset + 3, idOffset + 3, buttonX, buttonY);
			scroll.child(indexOffset + 4, idOffset + 4, buttonTextX, buttonTextY);

		}
		
		scroll.scrollMax = (rewardCount) * scrollInt + scrollStartHeight;
		scroll.width = 470;
		scroll.height = 250;
		if (scroll.scrollMax < scroll.height) {
			scroll.scrollMax = scroll.height + 1;
		}
		
	}

	private void updateVoteRewardsInterface(RSBuffer inStream) {
		int currentStreak = inStream.readUnsignedWord();
		RSInterface.interfaceCache[67329].message = "Vote streaks: <col=ffffff>" + currentStreak;
		boolean highlight = false;

		int id = 67355;
		for (int i = 0; i < 10; i++) {
			int streak = inStream.readUnsignedWord();
			boolean cross = currentStreak >= streak;
			String text = inStream.readString();

			int trans = i % 2 != 0 ? 235 : 255;
			if (!highlight && !cross) {
				highlight = true;
				trans = 200;
			}

			RSInterface bgRectangle = RSInterface.interfaceCache[id++];
			bgRectangle.transparency = trans;
			RSInterface.interfaceCache[id++].message = cross ? "<str=FF2B2B>" + text : text;
			RSInterface.interfaceCache[id++].message = Integer.toString(streak);
		}
	}

	private void updateDonationInterfaceStore(RSBuffer inStream) {
		RSInterface layer = RSInterface.interfaceCache[69100];
		layer.dynamicComponents = null;

		DonationInterfaceCategoryType type = DonationInterfaceCategoryType.list(inStream.readUnsignedByte());
		int pos = 0;
		int length = inStream.readUnsignedByte();
		int selectedIndex = inStream.readSignedByte();

		for (int i = 0; i < length; i++) {
			int price = inStream.readUnsignedWord();
			int mask = inStream.readUnsignedByte();

			int discountPrice = -1;
			boolean hasDiscount = (mask & 0x1) != 0;
			boolean isPet = (mask & 0x2) != 0;
			if (hasDiscount) {
				discountPrice = inStream.readUnsignedWord();
			}

			int petZoom = 1;
			if (isPet) {
				petZoom = inStream.readUnsignedWord();
			}

			int displayId = inStream.read3Bytes();
			String text = inStream.readString();

			int x = type.getTilePositions()[i][0];
			int y = type.getTilePositions()[i][1];
			int tileWidth = type.getTileSizes()[i][0];
			int tileHeight = type.getTileSizes()[i][1];
			int tileSprite;
			int hoverSprite;
			if (selectedIndex == i) {
				tileSprite = type.getTileSprites()[i][2];
				hoverSprite = -1;
			} else {
				tileSprite = type.getTileSprites()[i][0];
				hoverSprite = type.getTileSprites()[i][1];
			}

			layer.createDynamic(pos++, RSInterface.addSprite(x, y, tileWidth, tileHeight, tileSprite, hoverSprite, "Select"));
			RSInterface model;
			if (isPet) {
				int interpolatedZoom = interpolate(180, 180, 83, 344, tileWidth);
				model = RSInterface.addModelDynamic(x + tileWidth / 2 - 36 / 2, y + 50, 36, 32, 0, 0, (int) (petZoom * interpolatedZoom / 100), displayId, 5, -1);
			} else {
				ItemDef itemDef = ItemDef.forID(displayId);
				model = RSInterface.addModelDynamic(x + tileWidth / 2 - 36 / 2, y + 24, 36, 32, itemDef.yan2d, itemDef.xan2d, (int) (itemDef.zoom2d * 0.75), itemDef.id, 4, -1);
			}

			layer.createDynamic(pos++, model);
			layer.createDynamic(pos++, RSInterface.addNewText(text, x, y + 63, tileWidth, 30, 0, 0xffffff, 0, true, 1, 1, 12));		
			RSInterface priceText = RSInterface.addNewText("", x, y + 94, tileWidth, 20, 0, 0xb4a155, 0, true, 1, 1, 15);
			layer.createDynamic(pos++, priceText);
			RSInterface discountSprite = RSInterface.addSprite(x + tileWidth - 35, y, 35, 16, 1297, -1, null);
			layer.createDynamic(pos++, discountSprite);
			RSInterface discountText = RSInterface.addNewText("", x + tileWidth - 35, y, 35, 16, 0, 0xffffff, 0, true, 1, 1, 0);
			layer.createDynamic(pos++, discountText);
			if (hasDiscount) {
				priceText.message = "<str>" + Strings.formatNumbersWithCommas(price) + "</str>  <col=4edd43>" + Strings.formatNumbersWithCommas(discountPrice) + "</col>";
				discountText.message = "-" + Math.round(((((float) price - (float) discountPrice) / (float) price) * 100.0f)) + "%";
				discountText.hidden = false;
				discountSprite.hidden = false;
			} else {
				priceText.message = "<col=4edd43>" + Strings.formatNumbersWithCommas(price) + "</col>";
				discountText.hidden = true;
				discountSprite.hidden = true;
			}
		}

		if (GraphicsDisplay.enabled) {
			Cache.getModelVBOs().clear();
		}
	}

	private void updateDonationInterfaceCategory(RSBuffer inStream) {
		int selectedCategory = inStream.readSignedByte();
		int categoryCount = inStream.readUnsignedByte();

		RSInterface layer = RSInterface.interfaceCache[69015];
		layer.height = categoryCount * 25;
		layer.dynamicComponents = null;

		int pos = 0;
		for (int i = 0; i < categoryCount; i++) {
			int image = 1289;
			int hoverImage = 1290;
			if (selectedCategory == i) {
				image = 1291;
				hoverImage = -1;
			}
			
			int y = i * 25;
			layer.createDynamic(pos++, RSInterface.addSprite(0, y, 115, 22, image, hoverImage, "Select"));
			layer.createDynamic(pos++, RSInterface.addNewText(inStream.readString(), 10, y, 115, 22, 1, 0xe2b800, 0, true, 0, 1, 0));
		}
	}

	private void updateVoteLuckInterface(RSBuffer inStream) {
		int luckySlot = inStream.readSignedByte();
		int itemId = inStream.read3Bytes();
		if (itemId == 16777215) {
			itemId = -1;
		}

		int luckySlot2 = inStream.readSignedByte();
		int itemId2 = inStream.read3Bytes();
		if (itemId2 == 16777215) {
			itemId2 = -1;
		}

		//Green - 722
		//Red - 724
		//Grey - 721
		final int borderId = itemId2 == -1 ? 721 : itemId == itemId2 ? 722 : 724;
		final int hoverId = borderId == 721 ? 720 : borderId == 722 ? 723 : 725;

		for (int i = 0; i < 44; i++) {
			int childOffset = i * 3;
			int id = 67901 + childOffset;
			final RSInterface border = RSInterface.interfaceCache[id];
			RSInterface questionMark = RSInterface.interfaceCache[id + 1];
			RSInterface item = RSInterface.interfaceCache[id + 2];

			final int setHoverId;
			final int setBorderId;
			int setItemId = -1;
			if (luckySlot == i) {
				setItemId = itemId;
			} else if (luckySlot2 == i) {
				setItemId = itemId2;
			}

			item.itemId = setItemId;
			if (setItemId != -1) {
				questionMark.hidden = true;
				border.disabledSprite = SpriteCache.get(borderId);
				setBorderId = borderId;
				setHoverId = hoverId;
			} else {
				questionMark.hidden = false;
				border.disabledSprite = SpriteCache.get(721);
				setBorderId = 721;
				setHoverId = 720;
			}

			border.onMouseOver = new MouseEvent() {
				@Override
				public void run() {
					border.disabledSprite = SpriteCache.get(setHoverId);
				}
			};

			border.onMouseLeave = new MouseEvent() {
				@Override
				public void run() {
					border.disabledSprite = SpriteCache.get(setBorderId);
				}
			};
		}
	}

	private void method81(Sprite sprite, int j, int k) {
		int l = k * k + j * j;
		if (l > 4225) {
			int spriteX;
			int spriteY;
			if (isFixed()) {
				int xPosOffset = 0;
				if (GraphicsDisplay.enabled) {
					xPosOffset += 516;
				}
				spriteX = xPosOffset + 32;
				spriteY = 10;
			} else {
				spriteX = Client.clientWidth - 159;
				spriteY = 6;
			}
			int centerX = 140 / 2;//width
			int centerY = 155 / 2;//height
			int calc = Math.min(centerX, centerY) - 10;
			int i1 = viewRotation + minimapRotation & 0x7ff;
			int j1 = Rasterizer.SINE[i1];
			int k1 = Rasterizer.COSINE[i1];
			j1 = (j1 * 256) / (minimapZoom + 256);
			k1 = (k1 * 256) / (minimapZoom + 256);
			int l1 = j * j1 + k * k1 >> 16;
			int i2 = j * k1 - k * j1 >> 16;
			double d = Math.atan2(l1, i2);
			int j2 = (int) (Math.sin(d) * (double) calc);
			int k2 = (int) (Math.cos(d) * (double) calc);
			if (GraphicsDisplay.enabled) {
				mapEdge.method353HD(j2 + centerX + spriteX - 10, centerY + spriteY - k2 - 10, 20, 20, 15, 15, d * 10430.378, 256);
			} else {
				mapEdge.method353(j2 + centerX + spriteX - 10, centerY + spriteY - k2 - 10, 20, 20, 15, 15, d, 256);//70 - 140 / 2, 
			}
		} else {
			markMinimap(sprite, k, j);
		}
	}

	public void processRightClick() {
		if (activeInterfaceType != 0) {
			return;
		}

		if (fullscreenInterfaceID != -1) {
			hoveredInterfaceId = 0;
			processInterface(RSInterface.interfaceCache[fullscreenInterfaceID], 8, 8, 0, super.mouseX, super.mouseY);
			if (hoveredInterfaceId != hoveredScreenInterfaceId) {
				hoveredScreenInterfaceId = hoveredInterfaceId;
			}
			return;
		}

		boolean isFixed = isFixed();
		if (inDungeon()) {
			int width = dungMapSprite.myWidth;
			int height = dungMapSprite.myHeight;
			int xPosOffset = isFixed ? 516 : clientWidth - 115;
			int yPosOffset = isFixed ? 124 : 160;

			dungMapHover = false;
			if (super.mouseX >= 8 + xPosOffset && super.mouseY >= yPosOffset && super.mouseX <= 8 + xPosOffset + width && super.mouseY <= yPosOffset + height) {
				dungMapHover = true;
				MenuManager.addOption("Open map", 1234);
			}
		}

		buildSplitPrivateChatMenu();
		hoveredInterfaceId = 0;
		if (isFixed) {
			if (openOverlayId != -1) {
				processInterface(RSInterface.interfaceCache[openOverlayId], 4, 4, 0, super.mouseX, super.mouseY);
			}

			if (openModalInterfaceId != -1) {
				processInterface(RSInterface.interfaceCache[openModalInterfaceId], 4, 4, 0, super.mouseX, super.mouseY);
			} else {
				if (super.mouseX > 4 && super.mouseY > 4 && super.mouseX < 516 && super.mouseY < 338) {
					build3dScreenMenu();
				}
			}
		} else {
			int x = clientWidth / 2 - 356;
			int y = clientHeight / 2 - 267;
			if (openModalInterfaceId != -1) {
				processInterface(RSInterface.interfaceCache[openModalInterfaceId], x, y, 0, super.mouseX, super.mouseY);
			}
			if (openOverlayId != -1) {
				processInterface(RSInterface.interfaceCache[openOverlayId], x, y, 0, super.mouseX, super.mouseY);
			}
			if (canClick(x, y)) {
				build3dScreenMenu();
			}
		}
		if (hoveredInterfaceId != hoveredScreenInterfaceId) {
			hoveredScreenInterfaceId = hoveredInterfaceId;
		}
		hoveredInterfaceId = 0;
		if (showTab) {
			if (isFixed) {
				if (openInventoryModalInterfaceId != -1)
					processInterface(RSInterface.interfaceCache[openInventoryModalInterfaceId], clientWidth - 218, clientHeight - 298, 0, super.mouseX, super.mouseY);
				else if (tabInterfaceIDs[tabID] != -1)
					processInterface(RSInterface.interfaceCache[tabInterfaceIDs[tabID]], clientWidth - 218, clientHeight - 298, 0, super.mouseX, super.mouseY);
			} else {
				int y = clientWidth >= smallTabs ? 37 : 74;
				if (openInventoryModalInterfaceId != -1) {
					processInterface(RSInterface.interfaceCache[openInventoryModalInterfaceId], clientWidth - 197,
							clientHeight - y - 267, 0, super.mouseX, super.mouseY);
				} else if (tabInterfaceIDs[tabID] != -1) {
					processInterface(RSInterface.interfaceCache[tabInterfaceIDs[tabID]], clientWidth - 197,
							clientHeight - y - 267, 0, super.mouseX, super.mouseY);
				}
			}
		}

		for (int i = 0, length = tabInterfaceIDs.length; i < length; i++) {
			if (i == tabID || tabInterfaceIDs[i] == -1) {
				continue;
			}

			handleMainEvents(RSInterface.interfaceCache[tabInterfaceIDs[i]]);
		}

		if (hoveredInterfaceId != hoveredTabInterfaceId) {
			hoveredTabInterfaceId = hoveredInterfaceId;
			needDrawTabArea = true;
		}
		hoveredInterfaceId = 0;
		if (showChat && super.mouseX > 8 && super.mouseY > clientHeight - 158 && super.mouseX <= 495 && super.mouseY < clientHeight - 44) {
			if (backDialogID != -1) {
				processInterface(RSInterface.interfaceCache[backDialogID], 20, clientHeight - 145, 0, super.mouseX, super.mouseY);
			} else if (getInputDialogState() == SEARCH_STATE) {
				buildItemSearchRightClickMenu();
			} else {
				buildChatAreaMenu(super.mouseY - (clientHeight - 160));
			}
		}
		if (backDialogID != -1 && hoveredInterfaceId != hoveredChatboxInterfaceId) {
			inputTaken = true;
			hoveredChatboxInterfaceId = hoveredInterfaceId;
		}

		if (super.mouseX > 0 && super.mouseY > clientHeight - 165 && super.mouseX < 519 && super.mouseY < clientHeight) {
			rightClickChatButtons();
		}

		if (orbsEnabled)
			processMinimapActions();
		if (openInventoryModalInterfaceId == -1) {
			processTabAreaHovers();
		}

		if (mouseHoverEnum != MouseHover.NOTHING) {
			mouseHoverEnum = MouseHover.NOTHING;
		}

		if (loopCycle > searchUpdateCycle) {
			itemSearch(amountOrNameInput);
			itemResultScrollPos = 0;
			searchUpdateCycle = Integer.MAX_VALUE;
			inputTaken = true;
		}

		executeInterfaceEvents(interfaceHighPriorityEvents);
		executeInterfaceEvents(interfaceMediumPriorityEvents);
		executeInterfaceEvents(interfaceLowPriorityEvents);
	}

	private void executeInterfaceEvents(NodeList list) {
		for (;;) {
			InterfaceEvent event = (InterfaceEvent) list.popHead();
			if (event == null) {
				break;
			}

			event.run();
		}
	}

	public boolean canClick(int x, int y) {
		if (worldHover || hoverPos == 4 || dungMapHover) {
			return false;
		}

		if (mouseInRegion(clientWidth - (clientWidth < 900 ? 240 : 480), clientHeight - (clientWidth < 900 ? 74 : 37),
				clientWidth, clientHeight)) {
			return false;
		}
		if (showChat) {
			if (super.mouseX > 0 && super.mouseX < 519 && super.mouseY > clientHeight - 165
					&& super.mouseY < clientHeight
					|| super.mouseX > clientWidth - 220 && super.mouseX < clientWidth && super.mouseY > 0
					&& super.mouseY < 165) {
				return false;
			}
		}
		if (mouseInRegion(clientWidth - 216, 0, clientWidth, 172)) {
			return false;
		}
		if (openModalInterfaceId != -1) {
			if (mouseX >= x && mouseY >= y) {
				RSInterface component = RSInterface.interfaceCache[openModalInterfaceId];
				if (mouseX <= x + component.width && mouseY <= y + component.height) {
					return false;
				}
			}
		}
		if (showTab) {
			if (clientWidth >= Client.smallTabs) {
				if (super.mouseX >= clientWidth - 420 && super.mouseX <= clientWidth
						&& super.mouseY >= clientHeight - 37 && super.mouseY <= clientHeight
						|| super.mouseX > clientWidth - 204 && super.mouseX < clientWidth
						&& super.mouseY > clientHeight - 37 - 274 && super.mouseY < clientHeight)
					return false;
			} else {
				if (super.mouseX >= clientWidth - 210 && super.mouseX <= clientWidth
						&& super.mouseY >= clientHeight - 74 && super.mouseY <= clientHeight
						|| super.mouseX > clientWidth - 204 && super.mouseX < clientWidth
						&& super.mouseY > clientHeight - 74 - 274 && super.mouseY < clientHeight)
					return false;
			}
		}
		return true;
	}

	public void login(String username, String password, boolean reconnecting) {
		if (password.length() > 20) {
			loginMessage1 = "";
			loginMessage2 = "Invalid username or password.";
			return;
		}

		if (loginTick > loopCycle || loggedIn) {
			return;
		}

		loginTick = loopCycle + 30;

		try {
			Socket socket = openSocket(server, port);
			loginSocket = new RSSocket(this, socket, 5000);
			long l = TextClass.longForName(username);
			int hashedName = (int) (l >> 16 & 31L);
			stream.currentOffset = 0;
			stream.writeByte(14);
			stream.writeByte(hashedName);
			stream.writeByte(250);
			loginSocket.write(stream.buffer, 0, 3);
			for (int i = 0; i < 8; i++) {
				loginSocket.read();
			}

			int responseCode = loginSocket.read();
			int i1 = responseCode;
			if (responseCode == 0) {
				loginSocket.read(inStream.buffer, 0, 8);
				inStream.currentOffset = 0;
				aLong1215 = inStream.readQWord();
				int ai[] = new int[4];
				ai[0] = secureRandom.nextInt();
				ai[1] = secureRandom.nextInt();
				ai[2] = secureRandom.nextInt();
				ai[3] = secureRandom.nextInt();
				stream.currentOffset = 0;
				stream.writeByte(10);
				stream.writeByte(245);
				stream.writeDWord(ai[0]);
				stream.writeDWord(ai[1]);
				stream.writeDWord(ai[2]);
				stream.writeDWord(ai[3]);
				stream.writeString(username);
				stream.writeString(password);
				stream.doKeys();
				aStream_847.currentOffset = 0;
				aStream_847.writeByte(reconnecting ? 18 : 16);
				aStream_847.writeWord(0);
				int start = aStream_847.currentOffset;
				aStream_847.writeByte(255);
				aStream_847.writeWord(Config.CLIENT_VERSION);
				aStream_847.writeByte(145);
				aStream_847.writeByte(lowMem ? 1 : 0);
				for (int i = 0; i < 9; i++) {
					aStream_847.writeDWord(expectedCRCs[i]);
				}

				aStream_847.writeDWord(SignLink.javaVersionInt);
				aStream_847.writeString(uuid);
				aStream_847.writeBytes(stream.buffer, stream.currentOffset, 0);
				aStream_847.writeShortAt(aStream_847.currentOffset - start);
				stream.encryption = new ISAACRandomGen(ai);
				for (int j2 = 0; j2 < 4; j2++)
					ai[j2] += 50;
				
				encryption = new ISAACRandomGen(ai);
				loginSocket.write(aStream_847.buffer, 0, aStream_847.currentOffset);
				responseCode = loginSocket.read();
			}

			if (responseCode != 2 && loginSocket != null) {
				loginSocket.close();
				loginSocket = null;
			}

			switch (responseCode) {
				case 1: {
					try {
						Thread.sleep(2000L);
					} catch (Exception _ex) {
						_ex.printStackTrace();
					}
					login(username, password, reconnecting);
					return;
				}
				case 2: {
					Profiles.writeProfiles(myUsername, Encryption.encrypt(myPassword));
					resetSettingsOnLogin();
					scriptManager = null;
					titleScreenOffsets = null;
					myPrivilege = loginSocket.read();
					worldType = WorldType.list(loginSocket.read());
					if (useNewSocket) {
						gameSocket = new NIOSocket(socket, 5000, 5000);
						loginSocket.softClose();
						loginSocket = null;
					} else {
						gameSocket = loginSocket;
					}
					aLong1220 = 0L;
					super.awtFocus = true;
					aBoolean954 = true;
					loggedIn = true;
					stream.currentOffset = 0;
					inStream.currentOffset = 0;
					playerRenderPriorityIndex = -1;
					pktType = -1;
					anInt841 = -1;
					anInt842 = -1;
					anInt843 = -1;
					pktSize = 0;
					anInt1009 = 0;
					anInt1104 = 0;
					anInt1011 = 0;
					anInt855 = 0;
					MenuManager.menuActionRow = 0;
					MenuManager.menuOpen = false;
					super.idleTime = 0;
					ChatMessageManager.clear();
					itemSelected = 0;
					targetInterfaceId = -1;
					targetMask = null;
					loadingStage = 0;
					soundCount = 0;
					setNorth();
					minimapBlackout = 0;
					anInt985 = -1;
					destX = 0;
					destY = 0;
					playerCount = 0;
					npcCount = 0;
					for (int i2 = 0; i2 < maxPlayers; i2++) {
						playerArray[i2] = null;
						aStreamArray895s[i2] = null;
					}
					
					for (int k2 = 0; k2 < 16384; k2++)
						npcArray[k2] = null;
					
					myPlayer = playerArray[myPlayerIndex] = new Player();
					aClass19_1013.removeAll();
					aClass19_1056.removeAll();
					for (int l2 = 0; l2 < 4; l2++) {
						for (int i3 = 0; i3 < 104; i3++) {
							for (int k3 = 0; k3 < 104; k3++)
								groundArray[l2][i3][k3] = null;
							
						}
						
					}

					fog = null;
					aClass19_1179 = new NodeList();
					fullscreenInterfaceID = -1;
					anInt900 = 0;
					friendsCount = 0;
					dialogID = -1;
					backDialogID = -1;
					openModalInterfaceId = -1;
					openInventoryModalInterfaceId = -1;
					openOverlayId = -1;
					aBoolean1149 = false;
					tabID = 3;
					setInputDialogState(0);
					messagePromptRaised = false;
					aString844 = null;
					anInt1055 = 0;
					aBoolean1047 = true;
					method45();
					for (int j3 = 0; j3 < 5; j3++)
						anIntArray990[j3] = 0;
					
					for (int i = 0; i < PlayerOption.OPTION_COUNT; i++) {
						playerOptions[i] = null;
					}

					resetVarps();
					drawScreen();
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							frame.setTitle(Config.SERVER_NAME + " | World " + ((rotationWorlds == 2 ? 5 : rotationWorlds + 1))
									+ " | " + capitalize(myUsername));
							if (!frame.isResizable() && currentDisplayMode != DisplayMode.FIXED)
								frame.setResizable(true);
						}
					});
					int displayConfig = (Integer) Settings.getSetting("display_config");
					if (displayConfig == 1) {
						sizeTogglesButton(DisplayMode.FIXED);
					} else if (displayConfig == 2) {
						sizeTogglesButton(DisplayMode.RESIZEABLE);
					} else if (displayConfig == 3) {
						sizeTogglesButton(DisplayMode.FULLSCREEN);
					} else {
						sizeTogglesButton(DisplayMode.FIXED);
					}
					if (isFixed()) {
						updateGame.set(true);
						setVarp(ConfigCodes.DISPLAY_MODES, displayConfig);
					}
					// dropClient();
					return;
				}
				case 3: {
					loginMessage1 = "";
					loginMessage2 = "Invalid username or password.";
					return;
				}
				case 4: {
					loginMessage1 = "";
					loginMessage2 = "Your account has been disabled.";
					return;
				}
				case 5: {
					loginMessage1 = "Try again in 60 secs...";
					loginMessage2 = "Your account is already logged in.";
					return;
				}
				case 6: {
					loginMessage1 = Config.SERVER_NAME + " has been updated!";
					loginMessage2 = "Please reload this page, or download new client.";
					return;
				}
				case 7: {
					loginMessage1 = "This world is full.";
					loginMessage2 = "Please use a different world.";
					return;
				}
				case 8: {
					loginMessage1 = "Unable to connect.";
					loginMessage2 = "Login server offline.";
					return;
				}
				case 9: {
					if (loginFailures < 2) {
						try {
							Thread.sleep(1000L);
						} catch (Exception _ex) {
							_ex.printStackTrace();
						}
						loginFailures++;
						login(username, password, reconnecting);
						return;
					} else {
						loginMessage1 = "Login limit exceeded. Wait 1 hour.";
						loginMessage2 = "Too many connections from your address.";
						return;
					}
				}
				case 10: {
					loginMessage1 = "Unable to connect.";
					loginMessage2 = "Account is still logged in.";// "Bad session
					// id.";
					return;
				}
				case 11: {
					loginMessage1 = "Login server rejected session.";
					loginMessage2 = "Please try again.";
					return;
				}
				case 12: {
					loginMessage1 = "You need a members account to login to this world.";
					loginMessage2 = "Please subscribe, or use a different world.";
					return;
				}
				case 13: {
					loginMessage1 = "Could not complete login.";
					loginMessage2 = "Please try using a different world.";
					return;
				}
				case 14: {
					loginMessage1 = "The server is being updated.";
					loginMessage2 = "Please wait 1 minute and try again.";
					return;
				}
				case 15: {
					loggedIn = true;
					stream.currentOffset = 0;
					inStream.currentOffset = 0;
					pktType = -1;
					anInt841 = -1;
					anInt842 = -1;
					anInt843 = -1;
					pktSize = 0;
					anInt1009 = 0;
					anInt1104 = 0;
					MenuManager.menuActionRow = 0;
					MenuManager.menuOpen = false;
					loadingStartTime = System.currentTimeMillis();
					loginMessage1 = "";
					loginMessage2 = "";
					return;
				}
				case 16: {
					loginMessage1 = "Login attempts exceeded.";
					loginMessage2 = "Please wait 1 minute and try again.";
					return;
				}
				case 17: {
					loginMessage1 = "You are standing in a members-only area.";
					loginMessage2 = "To play on this world move to a free area first";
					return;
				}
				case 18: {
					loginMessage1 = "";
					loginMessage2 = "Malformed login request";
					return;
				}
				case 19: {
					loginMessage1 = "";
					loginMessage2 = "Malformed login connection";
					return;
				}
				case 20: {
					loginMessage1 = "Invalid loginserver requested";
					loginMessage2 = "Please try using a different world.";
					return;
				}
				case 21: {
					for (int k1 = loginSocket.read(); k1 >= 0; k1--) {
						loginMessage1 = "You have only just left another world";
						loginMessage2 = "Your profile will be transferred in: " + k1 + " seconds";
						try {
							Thread.sleep(1000L);
						} catch (Exception _ex) {
							_ex.printStackTrace();
						}
					}
					
					login(username, password, reconnecting);
					return;
				}
				case 22: {
					loginMessage1 = "Your computer has been banned.";
					loginMessage2 = "Please appeal on the forums.";
					return;
				}
				case 23: {
					for (int k1 = 4; k1 >= 0; k1--) {
						loginMessage1 = "";
						loginMessage2 = "Relog Queue: " + k1 + "";
						try {
							Thread.sleep(1000L);
						} catch (Exception _ex) {
							_ex.printStackTrace();
						}
					}
					
					login(username, password, reconnecting);
					return;
				}
				case 24: {
					int k1 = loginSocket.read();
					loginMessage1 = "";
					loginMessage2 = "Login Queue: " + k1;
					try {
						Thread.sleep(300L);
					} catch (Exception _ex) {
						_ex.printStackTrace();
					}
					
					login(username, password, reconnecting);
					return;
				}
				case 25: {
					loginMessage1 = "Login limit exceeded.";
					loginMessage2 = "Too many connections from your computer.";
					return;
				}
				case 26: {
					loginMessage1 = "Login limit exceeded.";
					loginMessage2 = "Too many connections from your location.";
					return;
				}
				case 27: {
					//loginMessage1 = "Limit on new accounts exceeded.";
					//loginMessage2 = "Please create your new account on the website.";
					LoginRenderer.titleState = TitleState.REGISTER;
					return;
				}
				case -1: {
					if (i1 == 0) {
						if (loginFailures < 2) {
							try {
								Thread.sleep(2000L);
							} catch (Exception _ex) {
								_ex.printStackTrace();
							}
							loginFailures++;
							login(username, password, reconnecting);
							return;
						} else {
							loginMessage1 = "No response from loginserver";
							loginMessage2 = "Please wait 1 minute and try again.";
							return;
						}
					} else {
						loginMessage1 = "No response from server";
						loginMessage2 = "Please try using a different world.";
						return;
					}
				}
				default: {
					System.out.println("response:" + responseCode);
					loginMessage1 = "Unexpected server response";
					loginMessage2 = "Please try using a different world.";
					return;
				}
			}
		} catch (IOException _ex) {
			loginMessage1 = "";
		} catch (Exception e) {
			System.out.println("Error while generating uid. Skipping step.");
			e.printStackTrace();
		}

		loginMessage2 = "Error connecting to " + Config.SERVER_NAME;
	}
	
	private boolean doWalkTo(int movementType, int destY,  int destX) {

			// renders minimap flag
			this.destX = destX;
			this.destY = destY;

			if (movementType == 0) { // regular walk
				stream.sendPacket(164);
			} else if (movementType == 1) { // map walk
				stream.sendPacket(248);
			} else if (movementType == 2) { // walk on command
				stream.sendPacket(98);
			}
			stream.method433(destX + regionBaseX);
			stream.method431(destY + regionBaseY);
			stream.method424(super.keyArray[5] != 1 ? 0 : 1);
			return true;
	}
	
	public void method86(RSBuffer stream) {
		for (int j = 0; j < anInt893; j++) {
			int k = anIntArray894[j];
			Npc npc = npcArray[k];
			int l = stream.readUnsignedByte();
			if ((l & 0x1) != 0) {
				l += stream.readUnsignedByte() << 8;
			}
			
			if ((l & 0x10) != 0) {
				int i1 = stream.method434();
				if (i1 == 65535) {
					i1 = -1;
				}
				if (i1 >= Animation.anims.length) {
					i1 = -1;
					System.out.println("Broken npc anim id = " + i1);
				}

				int i2 = stream.readUnsignedByte();
				if (i1 == npc.animId && i1 != -1) {
					int l2 = Animation.anims[i1].anInt365;
					if (l2 == 1) {
						npc.animFrame = 0;
						npc.animNextFrame = 1;
						npc.animFrameDelay = 0;
						npc.animDelay = i2;
						npc.animCyclesElapsed = 0;
					} else if (l2 == 2) {
						npc.animCyclesElapsed = 0;
					}
				} else if (i1 == -1 || npc.animId == -1
						|| Animation.anims[i1].anInt359 >= Animation.anims[npc.animId].anInt359) {
					npc.animId = i1;
					npc.animFrame = 0;
					npc.animNextFrame = 1;
					npc.animFrameDelay = 0;
					npc.animDelay = i2;
					npc.animCyclesElapsed = 0;
					npc.anInt1542 = npc.smallXYIndex;
				}
			}
			if ((l & 8) != 0) {
				int j1 = stream.readUnsignedByte();
				int j2 = stream.readUnsignedByte();
				int icon = stream.readUnsignedByte();
				npc.updateHitData(j2, j1, loopCycle, icon);
			}
			if ((l & 0x80) != 0) {
				int graphic = stream.readUnsignedWord();
				if (graphic == 65535)
					graphic = -1;
				int k1 = stream.readDWord();
				npc.spotAnimId = graphic;
				npc.anInt1524 = k1 >> 16;
				npc.spotAnimDelay = loopCycle + (k1 & 0xffff);
				npc.spotAnimFrame = 0;
				npc.spotAnimNextFrame = 1;
				npc.spotAnimFrameDelay = 0;
				if (npc.spotAnimDelay > loopCycle)
					npc.spotAnimFrame = -1;

				if (graphic != -1) {
					SpotAnim spotAnim = SpotAnim.cache[graphic];
					if (spotAnim != null) {
						spotAnim.preloadAnimation(onDemandFetcher);
					}
				}
			}
			if ((l & 0x20) != 0) {
				npc.setInteractingEntity(stream.readUnsignedWord());
			}
			if ((l & 0x200) != 0) {
				npc.textSpoken = stream.readString();
				npc.textCycle = 100;
			}
			if ((l & 0x40) != 0) {
				int l1 = stream.readUnsignedByte();
				int k2 = stream.readUnsignedByte();
				int icon = stream.readUnsignedByte();
				npc.updateHitData(k2, l1, loopCycle, icon);
				npc.loopCycleStatus = loopCycle + 300;
				npc.currentHealth = stream.readUnsignedWord();
				npc.maxHealth = stream.readUnsignedWord();
			}
			if ((l & 2) != 0) {
				npc.desc = EntityDef.forID(stream.read3Bytes());
				npc.anInt1540 = npc.desc.aByte68;
				npc.anInt1504 = npc.desc.anInt79;
				npc.anInt1554 = npc.desc.walkAnim;
				npc.anInt1555 = npc.desc.anInt58;
				npc.anInt1556 = npc.desc.anInt83;
				npc.anInt1557 = npc.desc.anInt55;
				npc.anInt1511 = npc.desc.standAnim;
			}
			if ((l & 4) != 0) {
				npc.anInt1538 = stream.method434();
				npc.anInt1539 = stream.method434();
			}
			if ((l & 0x100) != 0) {
				npc.isPet = stream.readUnsignedByte() == 1;
				npc.scale = stream.readUnsignedByte();
				if (npc.isPet) {
					npc.anInt1540 = 1;
				} else {
					npc.anInt1540 = npc.desc.aByte68;
				}
			}
			if ((l & 0x400) != 0) {
				String name = stream.readString();
				if (name.isEmpty()) {
					npc.displayName = null;
				} else {
					npc.displayName = name;
				}
			}
			if ((l & 0x800) != 0) {
				npc.anInt1543 = stream.method428();
				npc.anInt1545 = stream.method428();
				npc.anInt1544 = stream.method428();
				npc.anInt1546 = stream.method428();
				npc.anInt1547 = stream.method436() + loopCycle;
				npc.anInt1548 = stream.method435() + loopCycle;
				npc.anInt1549 = stream.method428();
				npc.method446();
			}
			if ((l & 0x1000) != 0) {
				npc.startFill = stream.readUnsignedWord();
				npc.stayCycleAfterFill = stream.readUnsignedByte();
				npc.fill = stream.readUnsignedByte();
				npc.maxFill = stream.readUnsignedWord();
				npc.reverse = stream.readUnsignedByte() == 1;
				npc.fillColor1 = stream.readDWord();
				npc.fillColor2 = stream.readDWord();
				npc.totalBarCycle = (npc.maxFill + loopCycle + npc.stayCycleAfterFill);
			}
		}
	}

	private static final short[] NPC_OPTION_IDS = {20, 412, 225, 965, 478};
	private static final String[] PREMADE_NPC_PET_OPS = { null, null, "Pick-up", "Feed", null };
	public static EntityAttackOptionSetting npcAttackOptionSetting = EntityAttackOptionSetting.LEFT_CLICK_WHERE_AVAILABLE;

	public void buildAtNPCMenu(Npc npc, int i, int j, int k) {
		if (MenuManager.menuActionRow >= 400)
			return;
		EntityDef entityDef = npc.desc;
		if (entityDef.childrenIDs != null)
			entityDef = entityDef.method161();
		if (entityDef == null || !entityDef.aBoolean84)
			return;
		String s = npc.getName();

		if (entityDef.combatLevel != 0 && !npc.isPet) {
			s = s + combatDiffColor(myPlayer.combatLevel, entityDef.combatLevel) + " (level-" + entityDef.combatLevel + ")";
		}
		
		if (itemSelected == 1) {
			MenuManager.addOption("Use " + selectedItemName + " with <col=ffff00>" + s, 582, i, k, j);
			return;
		}
		if (targetInterfaceId != -1) {
			if (targetMask.canUseOnNpc()) {
				MenuManager.addOption(spellTooltip + " <col=ffff00>" + s, 413, i, k, j);
			}
		} else {
			boolean lowPrioOptions = false;
			String[] ops;
			if (npc.isPet) {
				ops = PREMADE_NPC_PET_OPS;
				lowPrioOptions = variousSettings[ConfigCodes.PET_OPTION_PRIORITY] == 1;
			} else {
				ops = entityDef.actions;
			}
			if (ops != null) {
				for (int l = 4; l >= 0; l--) {
					if (ops[l] != null && !ops[l].equalsIgnoreCase("attack")) {
						int lowPriorityOffset = 0;
						
						if (s.equals("Pack yak") || ops[l].equalsIgnoreCase("pick-up") || walkOptionFirst || lowPrioOptions) {
							lowPriorityOffset = 2000;
						}

						MenuManager.addOption(ops[l] + " <col=ffff00>" + s, NPC_OPTION_IDS[l] + lowPriorityOffset, i, k, j);
					}
				}

				for (int i1 = 4; i1 >= 0; i1--) {
					if (ops[i1] != null && ops[i1].equalsIgnoreCase("attack")) {
						int lowPriorityOffset = 0;
						if (npcAttackOptionSetting == EntityAttackOptionSetting.HIDDEN) {
							continue;
						}

						if (npcAttackOptionSetting == EntityAttackOptionSetting.ALWAYS_RIGHT_CLICK || (npcAttackOptionSetting == EntityAttackOptionSetting.COMBAT_DEPEDANT && entityDef.combatLevel > myPlayer.combatLevel) || walkOptionFirst || lowPrioOptions) {
							lowPriorityOffset = 2000;
						}

						MenuManager.addOption(ops[i1] + " <col=ffff00>" + s, NPC_OPTION_IDS[i1] + lowPriorityOffset, i, k, j);
					}
				}
			}

			String option;
			if (Config.enableIds) {
				option = "Examine <col=ffff00>" + s + " <col=ff00>(<col=ffffff>" + entityDef.myId + "<col=ff00>)";
			} else {
				option = "Examine <col=ffff00>" + s;
			}

			MenuManager.addOption(option, lowPrioOptions ? 3025 : 1025, i, k, j);
		}
	}

	private StringBuilder playerMenuBuilder = new StringBuilder();
	public static EntityAttackOptionSetting playerAttackOptionSetting = EntityAttackOptionSetting.LEFT_CLICK_WHERE_AVAILABLE;

	public void buildAtPlayerMenu(int i, int j, Player player, int k) {
		if (player == myPlayer || MenuManager.menuActionRow >= 400)
			return;

		playerMenuBuilder.setLength(0);
		if (player.clanTag != null && !player.clanTag.isEmpty()) {
			playerMenuBuilder.append("<col=ff9040>");
			playerMenuBuilder.append(player.clanTag);
			playerMenuBuilder.append("<col=ffffff> ");
		}
		
		if (player.title != null && !player.title.isEmpty()) {
			playerMenuBuilder.append("<col=");
			playerMenuBuilder.append(titleColor(player.titleColor));
			playerMenuBuilder.append(">");
			playerMenuBuilder.append(player.title);
			playerMenuBuilder.append("<col=ffffff> ");
		}
		
		playerMenuBuilder.append(player.name);
		playerMenuBuilder.append(combatDiffColor(myPlayer.combatLevel, player.combatLevel));
		playerMenuBuilder.append(" (level: ");
		playerMenuBuilder.append(Integer.toString(player.combatLevel));
		playerMenuBuilder.append(")");
		
		String s = playerMenuBuilder.toString();
		
		if (itemSelected == 1) {
			MenuManager.addOption("Use " + selectedItemName + " with <col=ffffff>" + s, 491, j, i, k);
		} else if (targetInterfaceId != -1) {
			if (targetMask.canUseOnPlayer()) {
				MenuManager.addOption(spellTooltip + " <col=ffffff>" + s, 365, j, i, k);
			}
		} else {
			for (int optionIndex = PlayerOption.OPTION_COUNT - 1; optionIndex >= 0; optionIndex--) {
				PlayerOption playerOption = playerOptions[optionIndex];
				if (playerOption == null) {
					continue;
				}

				int lowPriorityOffset = 0;
				if (playerOption.isAttackOption()) {
					if (playerAttackOptionSetting == EntityAttackOptionSetting.HIDDEN) {
						continue;
					}

					if ((playerAttackOptionSetting == EntityAttackOptionSetting.LEFT_CLICK_ATTACKING_ONLY && (playerRenderPriorityIndex == -1 || player != playerArray[playerRenderPriorityIndex])) || playerAttackOptionSetting == EntityAttackOptionSetting.ALWAYS_RIGHT_CLICK || (playerAttackOptionSetting == EntityAttackOptionSetting.COMBAT_DEPEDANT && player.combatLevel > myPlayer.combatLevel))
						lowPriorityOffset = 2000;
					if (myPlayer.clanId != 0 && player.clanId != 0 && myPlayer.clanId == player.clanId) {
						lowPriorityOffset = 2000;
					}

					if (myPlayer.team != 0 && player.team != 0) {
						if (myPlayer.team == player.team)
							lowPriorityOffset = 2000;
						else
							lowPriorityOffset = 0;
					}

					if (walkOptionFirst) {
						lowPriorityOffset = 2000;
					}
				} else if (playerOption.isLowPrio()) {
					lowPriorityOffset = 2000;
				}

				MenuManager.addOption(playerOption.getOption() + " <col=ffffff>" + s, PlayerOption.OPTION_IDS[optionIndex] + lowPriorityOffset, j, i, k);
			}
		}

		for (int i1 = 0; i1 < MenuManager.menuActionRow; i1++) {
			if (MenuManager.menuActionID[i1] == 516) {
				MenuManager.menuActionName[i1] = "Walk here <col=ffffff>" + s;
				return;
			}
		}
	}

	public void method89(GameObject gameObject) {
		long i = 0L;
		int j = -1;
		int k = 0;
		int l = 0;

		if (gameObject.objectType == 0) {
			i = worldController.method300(gameObject.plane, gameObject.tileX, gameObject.tileY);
		} else if (gameObject.objectType == 1) {
			i = worldController.method301(gameObject.plane, gameObject.tileX, gameObject.tileY);
		} else if (gameObject.objectType == 2) {
			i = worldController.method302(gameObject.plane, gameObject.tileX, gameObject.tileY);
		} else if (gameObject.objectType == 3) {
			i = worldController.method303(gameObject.plane, gameObject.tileX, gameObject.tileY);
		}

		if (i != 0L) {
			j = (int) (i >>> 32) & 0x7fffffff;
			k = (int) (i >> 14) & 0x1f;
			l = (int) (i >> 20) & 0x3;
		}

		gameObject.anInt1299 = j;
		gameObject.anInt1301 = k;
		gameObject.anInt1300 = l;
	}
	
	public void method90() {
		for (int currentSound = 0; currentSound < soundCount; currentSound++) {
			soundDelays[currentSound]--;
			if (soundDelays[currentSound] < -10) {
				soundCount--;
				for (int lastSound = currentSound; soundCount > lastSound; lastSound++) {
					soundIds[lastSound] = soundIds[lastSound + 1];
					aClass26Array1468[lastSound] = aClass26Array1468[lastSound + 1];
					soundTypes[lastSound] = soundTypes[lastSound + 1];
					soundDelays[lastSound] = soundDelays[lastSound + 1];
				}
				currentSound--;
			} else {
				Sound class26 = aClass26Array1468[currentSound];
				if (class26 == null) {
					class26 = Sound.cache[soundIds[currentSound]];
					if (class26 == null) {
						continue;
					}
					soundDelays[currentSound] += class26.method652();
					aClass26Array1468[currentSound] = class26;
				}
				if (soundDelays[currentSound] < 0) {
					Class3_Sub9_Sub1 class3_sub9_sub1 = class26.method651().method405(Client.aClass25_1948);
					Class3_Sub7_Sub2 class3_sub7_sub2 = Class3_Sub7_Sub2.method396(class3_sub9_sub1, 100,
							soundEffectVolume);
					class3_sub7_sub2.method394(soundTypes[currentSound] - 1);
					aClass3_Sub7_Sub1_1493.method384(class3_sub7_sub2);
					soundDelays[currentSound] = -100;
				}
			}
		}
		if (previousSong > 0) {
			previousSong -= 20;
			if (previousSong < 0)
				previousSong = 0;
			if (previousSong == 0 && musicVolume != 0 && currentSong != -1) {
				method56(musicVolume, false, currentSong);
			}
		}
	}
	
	@Override
	public void loadSettingsClass() {
		Settings.load();
		setSettings();
	}
	
	@Override
	public void startUp() {
		GraphicsDisplay.resizeRequired = true;
		drawLoadingBar(0, "Loading...");
		SignLink.checkForLocalCacheFolder();

		if (!Config.USE_UPDATE_SERVER) {
			CacheDownloader.downloadingCache = true;
			new CacheDownloader(this).downloadCache();
			CacheDownloader.downloadingCache = false;
		}

		if (Config.connectToLive) {
			HttpUpdate.updateCacheFile(Config.SPRITES_DATA, "main_file_sprites.dat");
			HttpUpdate.updateCacheFile(Config.SPRITES_META, "main_file_sprites.idx");
		}

		SpriteCache.setLoadingBackground();
        SpriteCache.init();

		Ping.runPing();
		drawLoadingBar(15, "Initializing file system");

		if (SignLink.cache_dat != null) {
			for (int i = 0; i < SignLink.INDEX_SIZE; i++) {
				fileStores[i] = new RSFileStore(SignLink.cache_dat, SignLink.cache_idx[i], i + 1);
			}
		}
		try {
			if (Config.USE_UPDATE_SERVER) {	
				drawLoadingBar(20, "Connecting to update server");	
				connectServer();	
			}

			Profiles.readProfiles();

			fileSystemWorker = new FileSystemWorker();

			RSArchive titleArchive = loadArchive(1, "title screen", "title", expectedCRCs[1], 25);

			drawLoadingBar(30, "Unpacking font");
			newSmallFont = new RSFont(false, "p11_full", titleArchive, 0.6);
			newRegularFont = new RSFont(false, "p12_full", titleArchive, 0.7);
			newBoldFont = new RSFont(false, "b12_full", titleArchive, 0.7);
			newFancyFont = new RSFont(true, "q8_full", titleArchive, 0.7);

			constructMusic();
			aClass3_Sub7_Sub1_1493 = method407(canvas);
			aClass25_1948 = new Class25(22050, anInt197);

			RSArchive configArchive = loadArchive(2, "config", "config", expectedCRCs[2], 30);
			RSArchive interfaceArchive = loadArchive(3, "interface", "interface", expectedCRCs[3], 35);
			RSArchive mediaArchive = loadArchive(4, "2d graphics", "media", expectedCRCs[4], 40);
			byte[] mediaIndex = mediaArchive.readFile("index.dat");
			RSArchive textureArchive = loadArchive(6, "textures", "textures", expectedCRCs[6], 45);
			RSArchive chatArchive = loadArchive(7, "chat system", "wordenc", expectedCRCs[7], 50);

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					HOT_KEYS = new HotKeys(frame);
				}
			});
			/*
			 * if (super.frame != null && !usingWebclient) {
			 * frame.setClientIcon(); } else if (super.frame == null) { if
			 * (DisplayModeChanger.getChanger() != null) {
			 * DisplayModeChanger.getChanger().setClientIcon(); } }
			 */// TODO
			
			/*
			 * imageLoader = new ImageLoader(streamLoader); loadedImages = true;
			 */ // incase we load client images for loading screen from
			// ImageLoader
			
			byteGroundArray = new byte[4][104][104];
			intGroundArray = new int[4][105][105];
			worldController = new WorldController(intGroundArray);
			for (int j = 0; j < 4; j++)
				aClass11Array1230[j] = new Class11();
			
			miniMap = new Sprite(512, 512);

			RSArchive versionArchive = loadArchive(5, "update list", "versionlist", expectedCRCs[5], 60);

			onDemandFetcher = new OnDemandFetcher();
			onDemandFetcher.start(versionArchive, this);
			Class36.method528(onDemandFetcher.getAnimCount());
			Model.method459(onDemandFetcher.getModelCount(), onDemandFetcher);

			if (!Config.USE_UPDATE_SERVER) {
				drawLoadingBar(65, "Pre-loading maps");
				int k = onDemandFetcher.mapAmount;
				for (int i1 = 0; i1 < k; i1++) {
					int index = onDemandFetcher.osrsMap[i1] ? 6 : 3;
					onDemandFetcher.loadData(index, onDemandFetcher.mapIndices2[i1]);
					onDemandFetcher.loadData(index, onDemandFetcher.mapIndices3[i1]);
				}

				while (onDemandFetcher.getWaitingToLoadCount() > 0) {
					onDemandFetcher.removeEmpties();
					processOnDemandQueue();
					try {
						Thread.sleep(100L);
					} catch (Exception _ex) {
					}
					if (onDemandFetcher.anInt1349 > 3) {
						loadError();
						return;
					}
				}
			}

			drawLoadingBar(80, "Unpacking media");
			
			loadExtraSprites();
			
			Sprite[] clanIcons = new Sprite[9];
			for (int index = 0; index < clanIcons.length; index++) {
				clanIcons[index] = SpriteCache.get(index + 181);
			}

			multiOverlay = new Sprite(mediaArchive, "overlay_multiway", 0, mediaIndex);
			mapBackII = new Background(mediaArchive, "mapback", 0, mediaIndex);
			
			for (int c1 = 0; c1 <= 3; c1++) {
				chatButtons[c1] = new Sprite(mediaArchive, "chatbuttons", c1, mediaIndex);
			}

			compass = new Sprite(mediaArchive, "compass", 0, mediaIndex);
			// int errorIndex = 0;
			try {
				for (int i = 0; i < 80; i++) {
					mapScenes[i] = new Background(mediaArchive, "mapscene", i, mediaIndex);
					// errorIndex = i;
				}
			} catch (Exception _ex) {
				// _ex.printStackTrace();
				// System.out.println("scene index :"+errorIndex);
			}
			try {
				for (int l3 = 0; l3 < 100; l3++)
					mapFunctions[l3] = new Sprite(mediaArchive, "mapfunction", l3, mediaIndex);
			} catch (Exception _ex) {
				// _ex.printStackTrace();
			}
			try {
				for (int i4 = 0; i4 < 20; i4++)
					hitMarks[i4] = new Sprite(mediaArchive, "hitmarks", i4, mediaIndex);
			} catch (Exception _ex) {
				// _ex.printStackTrace();
			}
			try {
				for (int h1 = 0; h1 < 6; h1++)
					headIconsHint[h1] = new Sprite(mediaArchive, "headicons_hint", h1, mediaIndex);
			} catch (Exception _ex) {
				_ex.printStackTrace();
			}
			try {
				for (int j4 = 0; j4 < 18; j4++) {
					headIcons[j4] = new Sprite(mediaArchive, "headicons_prayer", j4, mediaIndex);
				}
				
				for (int idx = 0; idx < 18; idx++) {
					headIcons[idx] = SpriteCache.get(idx + 244);
				}
				
				for (int j45 = 0; j45 < 3; j45++) {
					skullIcons[j45] = new Sprite(mediaArchive, "headicons_pk", j45, mediaIndex);
				}
				skullIcons[3] = SpriteCache.get(202);
				
			} catch (Exception _ex) {
				_ex.printStackTrace();
			}
			mapFlag = new Sprite(mediaArchive, "mapmarker", 0, mediaIndex);
			mapMarker = new Sprite(mediaArchive, "mapmarker", 1, mediaIndex);
			for (int k4 = 0; k4 < 8; k4++)
				crosses[k4] = new Sprite(mediaArchive, "cross", k4, mediaIndex);
			
			mapDotItem = new Sprite(mediaArchive, "mapdots", 0, mediaIndex);
			mapDotNPC = new Sprite(mediaArchive, "mapdots", 1, mediaIndex);
			mapDotPlayer = new Sprite(mediaArchive, "mapdots", 2, mediaIndex);
			mapDotFriend = new Sprite(mediaArchive, "mapdots", 3, mediaIndex);
			mapDotTeam = new Sprite(mediaArchive, "mapdots", 4, mediaIndex);
			mapDotClan = new Sprite(mediaArchive, "mapdots", 5, mediaIndex);
			scrollBar1 = new Sprite(mediaArchive, "scrollbar", 0, mediaIndex);
			scrollBar2 = new Sprite(mediaArchive, "scrollbar", 1, mediaIndex);
			
			for (int l4 = 0; l4 < modIcons.length; l4++) {
				if (l4 == 14)
					break; // we set new icons past 14
				modIcons[l4] = new Sprite(mediaArchive, "mod_icons", l4, mediaIndex);
			}
			
			modIcons[14] = SpriteCache.get(RankInfo.PK_ICON_1.getSpriteId());
			modIcons[15] = SpriteCache.get(RankInfo.PK_ICON_2.getSpriteId());
			modIcons[16] = SpriteCache.get(RankInfo.PK_ICON_3.getSpriteId());
			modIcons[17] = SpriteCache.get(RankInfo.PK_ICON_4.getSpriteId());
			modIcons[18] = SpriteCache.get(RankInfo.PK_ICON_5.getSpriteId());
			modIcons[19] = SpriteCache.get(RankInfo.PRESTIGE_1.getSpriteId());
			modIcons[20] = SpriteCache.get(RankInfo.PRESTIGE_2.getSpriteId());
			modIcons[21] = SpriteCache.get(RankInfo.PRESTIGE_3.getSpriteId());
			modIcons[22] = SpriteCache.get(RankInfo.INSANE.getSpriteId());
			modIcons[23] = SpriteCache.get(RankInfo.TRUSTED.getSpriteId());
			modIcons[24] = SpriteCache.get(1286); //hard core ironman
			
			mapEdge = SpriteCache.get(124);
			mapEdge.maxWidth = 31;
			mapEdge.maxHeight = 31;
			mapEdge.drawOffsetX = 8;
			mapEdge.drawOffsetY = 7;
			mapEdge.method345();
			
			RSFont.unpackImages(modIcons, clanIcons);
			
			leftFrameSprite = new Sprite(mediaArchive, "screenframe", 0, mediaIndex);
			leftFrame = new RSImageProducer(leftFrameSprite.myWidth, leftFrameSprite.myHeight, canvas);
			if (!GraphicsDisplay.enabled) {
				leftFrameSprite.method346(0, 0);
			}
			
			topFrameSprite = new Sprite(mediaArchive, "screenframe", 1, mediaIndex);
			topFrame = new RSImageProducer(topFrameSprite.myWidth, topFrameSprite.myHeight, canvas);
			if (!GraphicsDisplay.enabled) {
				topFrameSprite.method346(0, 0);
			}
			
			colorPaletteSprite = new Sprite(128, 128);
			colorPaletteSprite.UID = -1;

			drawLoadingBar(83, "Unpacking textures");
			Rasterizer.unpack(textureArchive, textureArchive.readFile("index.dat"));
			Rasterizer.method372(0.8);
			Rasterizer.method367();
			drawLoadingBar(86, "Unpacking config");
			Animation.unpackConfig(configArchive);
			ObjectDef.unpackConfig(configArchive);
			Flo.unpackConfig(configArchive);
			FloHigherRev.load(configArchive);
			FloOsrs.unpackConfig(configArchive);
			ItemDef.unpackConfig(configArchive);
			EntityDef.unpackConfig(configArchive);
			IDK.unpackConfig(configArchive);
			SpotAnim.unpackConfig(configArchive);
			Varp.unpackConfig(configArchive);
			VarBit.unpackConfig(configArchive);
			ItemDef.isMembers = isMembers;
			// if (!lowMem) {
			// }
			// repackCacheIndex(1); //3d models
			// repackCacheIndex(2); // animations
			// repackCacheIndex(3); // sounds
			// repackCacheIndex(4); // maps
			// yepp
			// repackCacheIndex(5); //Textures
			// repackCacheIndex(6); //OSRS models
			// repackCacheIndex(7); //OSRS maps
			// repackCacheIndex(8); //Sound effects
			// repackCacheIndex(9); //667 models

//			 repackOsrsAnimations(); // repacking osrs animations into index 2 with fileId+5000
			// preloadModels();

			drawLoadingBar(95, "Unpacking interfaces");
			InterfaceDecoder.decode(interfaceArchive, mediaArchive, mediaIndex);
			drawLoadingBar(100, "Preparing game engine");
			
			setGraphicTogglesVarps();
			setClientSettingVarps();
			
			for (int j6 = 0; j6 < 33; j6++) {
				int k6 = 999;
				int i7 = 0;
				for (int k7 = 0; k7 < 34; k7++) {
					if (mapBackII.aByteArray1450[k7 + j6 * mapBackII.anInt1452] == 0) {
						if (k6 == 999)
							k6 = k7;
						continue;
					}
					if (k6 == 999)
						continue;
					i7 = k7;
					break;
				}
				
				anIntArray968[j6] = k6;
				anIntArray1057[j6] = i7 - k6;
			}
			setHighQualityLoginScreen();
			
			for (int l6 = 5; l6 < 156; l6++) {
				int j7 = 999;
				int l7 = 0;
				for (int j8 = 20; j8 < 172; j8++) {
					if (mapBackII.aByteArray1450[j8 + l6 * mapBackII.anInt1452] == 0 && (j8 > 34 || l6 > 34)) {
						if (j7 == 999)
							j7 = j8;
						continue;
					}
					if (j7 == 999)
						continue;
					l7 = j8;
					break;
				}
				
				anIntArray1052[l6 - 5] = j7 - 20;
				anIntArray1229[l6 - 5] = l7 - j7;
				if (anIntArray1229[l6 - 5] == -20)
					anIntArray1229[l6 - 5] = 152;
			}
			
			Rasterizer.setBounds(765, 503);
			fullScreenTextureArray = Rasterizer.lineOffsets;
			
			Rasterizer.setBounds(519, 165); // 519
			anIntArray1180 = Rasterizer.lineOffsets;
			
			Rasterizer.setBounds(250, 335); // 246
			anIntArray1181 = Rasterizer.lineOffsets;
			
			Rasterizer.setBounds(clientWidth, clientHeight);
			anIntArray1182 = Rasterizer.lineOffsets;
			Censor.loadConfig(chatArchive);
			Animable_Sub5.clientInstance = this;
			ObjectDef.clientInstance = this;
			EntityDef.clientInstance = this;
			secureRandom = new SecureRandom();
			calendar = new CalendarHandler();
			setUUID();
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			device = env.getDefaultScreenDevice();
			originalDisplayMode = device.getDisplayMode();
			swapToFullscreenPixmap();
			clearLoading();
			SpriteCache.clear();
			doneLoading = true;
			return;
		} catch (Exception exception) {
			ErrorHandler.submitError("startup", exception);
		}
		loadingError = true;
	}
	
	private Sprite leftFrameSprite;
	private Sprite topFrameSprite;
	private RSImageProducer leftFrame;
	private RSImageProducer topFrame;
	
	public void method91(RSBuffer stream, int i) {
		while (stream.bitPosition + 10 < i * 8) {
			int j = stream.readBits(11);
			if (j == 2047)
				break;
			if (playerArray[j] == null) {
				playerArray[j] = new Player();
				playerArray[j].index = j;
				if (aStreamArray895s[j] != null)
					playerArray[j].updatePlayer(aStreamArray895s[j]);
			}
			playerIndices[playerCount++] = j;
			Player player = playerArray[j];
			player.anInt1537 = loopCycle;
			int k = stream.readBits(1);
			if (k == 1)
				anIntArray894[anInt893++] = j;
			int l = stream.readBits(1);
			int i1 = stream.readBits(5);
			if (i1 > 15)
				i1 -= 32;
			int j1 = stream.readBits(5);
			if (j1 > 15)
				j1 -= 32;
			player.setPos(myPlayer.smallX[0] + j1, myPlayer.smallY[0] + i1, l == 1);
		}
		stream.finishBitAccess();
	}
	
	public boolean canClickMap() {
		if (super.mouseX >= clientWidth - 21 && super.mouseX <= clientWidth && super.mouseY >= 0 && super.mouseY <= 21)
			return false;
		return true;
	}
	
	private void processMinimap() {
		if (minimapBlackout != 0) {
			return;
		}

		if (super.clickMode3 == 1) {
			//System.out.println(saveClickX + " " + saveClickY + " " + (Math.pow(saveClickX - 622, 2) + Math.pow(saveClickY - 84, 2)) + " " +  Math.pow(74, 2));

			if (Misc.isPointInCircle(saveClickX, saveClickY, isFixed() ? 622 : (clientWidth - 83), 84, 74)) {
				int i = super.saveClickX - (isFixed() ? 545 : clientWidth - 162);
				int j = super.saveClickY - (isFixed() ? 11 : 8);

				i -= 73;
				j -= 75;
				int k = viewRotation + minimapRotation & 0x7ff;
				int i1 = Rasterizer.SINE[k];
				int j1 = Rasterizer.COSINE[k];
				i1 = i1 * (minimapZoom + 256) >> 8;
				j1 = j1 * (minimapZoom + 256) >> 8;
				int k1 = j * i1 + i * j1 >> 11;
				int l1 = j * j1 - i * i1 >> 11;
				int i2 = myPlayer.x + k1 >> 7;
				int j2 = myPlayer.z - l1 >> 7;

				if (myPrivilege >= 2 && holdingCtrlKey) {
					teleport(i2, j2);
					return;
				}
				
				boolean flag1 = doWalkTo(1,   j2,  i2);
				if (flag1) {
					stream.writeByte(i);
					stream.writeByte(j);
					stream.writeWord(viewRotation);
					stream.writeByte(57);
					stream.writeByte(minimapRotation);
					stream.writeByte(minimapZoom);
					stream.writeByte(89);
					stream.writeWord(myPlayer.x);
					stream.writeWord(myPlayer.z);
					stream.writeByte(anInt1264);
					stream.writeByte(63);
				}
			}
		}
	}
	
	private String interfaceIntToString(int j) {
		if (j < 0x3b9ac9ff) {
			return String.valueOf(j);
		}

		return "*";
	}
	
	public void showErrorScreen() {
		Graphics g = canvas.getGraphics();
		if (g == null)
			return;
		g.setColor(Color.black);
		g.fillRect(0, 0, 765, 503);
		method4(1);
		if (loadingError) {
			g.setFont(new Font("Helvetica", 1, 16));
			g.setColor(Color.yellow);
			int k = 35;
			g.drawString("Sorry, an error has occured whilst loading " + Config.SERVER_NAME + "", 30, k);
			k += 50;
			g.setColor(Color.white);
			g.drawString("To fix this try the following (in order):", 30, k);
			k += 50;
			g.setColor(Color.white);
			g.setFont(new Font("Helvetica", 1, 12));
			g.drawString("1: Try closing ALL open web-browser windows, and reloading", 30, k);
			k += 30;
			g.drawString("2: Try clearing your web-browsers cache from tools->internet options", 30, k);
			k += 30;
			g.drawString("3: Try using a different game-world", 30, k);
			k += 30;
			g.drawString("4: Try rebooting your computer", 30, k);
			k += 30;
			g.drawString("5: Try selecting a different version of Java from the play-game menu", 30, k);
		}
		if (genericLoadingError) {
			g.setFont(new Font("Helvetica", 1, 20));
			g.setColor(Color.white);
			g.drawString("Error - unable to load game!", 50, 50);
			g.drawString("To play " + Config.SERVER_NAME + " make sure you play from", 50, 100);
			g.drawString(Config.WEBSITE_URL, 50, 150);
		}
		if (rsAlreadyLoaded) {
			g.setColor(Color.yellow);
			int l = 35;
			g.drawString("Error a copy of " + Config.SERVER_NAME + " already appears to be loaded", 30, l);
			l += 50;
			g.setColor(Color.white);
			g.drawString("To fix this try the following (in order):", 30, l);
			l += 50;
			g.setColor(Color.white);
			g.setFont(new Font("Helvetica", 1, 12));
			g.drawString("1: Try closing ALL open web-browser windows, and reloading", 30, l);
			l += 30;
			g.drawString("2: Try rebooting your computer, and reloading", 30, l);
			l += 30;
		}
		if (!clearingCache)
			sendCacheFix();
	}
	
	private boolean clearingCache = false;
	
	public void sendCacheFix() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				String prompt = JOptionPane.showInputDialog("To clear " + Config.SERVER_NAME + " Cache type in: YES");
				if (prompt != null && prompt.toLowerCase().equals("yes")) {
					clearingCache = true;
					cleanUpForQuit();
					cleanUpDir(new File(SignLink.cacheDir));
					System.exit(0);
					// deleteDir(new File(signlink.findcachedir()));
					/*
					 * if(deleteDir(new File(signlink.findcachedir()))) {
					 * JOptionPane.showMessageDialog(null, "Cache Successfully Cleared!"
					 * ); System.exit(0); } else { JOptionPane.showMessageDialog(null,
					 * "Error Clearing Cache."); }
					 */
				} else {
					if (usingWebclient) {
						JOptionPane.showMessageDialog(null, "Closing without clearing cache.");
						System.exit(0);
					} else {
						JOptionPane.showMessageDialog(null, "Delete the game's Cache folder then start the client again.");
						openCacheFolder();
						System.exit(0);
					}
				}
				
			}
		});
			
	}
	
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			if (children != null) {
				for (int i = 0; i < children.length; i++) {
					boolean success = deleteDir(new File(dir, children[i]));
					if (!success) {
						JOptionPane.showMessageDialog(null, "Error Clearing Cache.");
						return false;
					}
				}
			}
		}
		// The directory is now empty so delete it
		JOptionPane.showMessageDialog(null, "Cache Successfully Cleared!");
		dir.delete();
		System.exit(0);
		return true;
		
	}
	
	public boolean cleanUpDir(File dir) {
		// try {
		// for (int i = 0; i < signlink.cache_idx.length; i++) {
		// signlink.cache_idx[i].close();
		// }
		// signlink.cache_dat.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// signlink.cache_dat = null;
		// signlink.cache_idx = null;
		// System.gc();
		if (dir.isDirectory()) {
			String[] children = dir.list();
			if (children != null) {
				for (int i = 0; i < children.length; i++) {
					deleteStuff(new File(dir, children[i]));
				}
			}
		}
		// The directory is now empty so delete it
		JOptionPane.showMessageDialog(null, "Cache Successfully Cleared!");
		// dir.delete();
		// System.exit(0);
		SignLink.startpriv();
		return true;
		
	}
	
	public void deleteStuff(File dir) {
		dir.deleteOnExit();
	}
	
	@Override
	public URL getCodeBase() {
		try {
			return new URL(server + ":" + 80);
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
		return null;
	}
	
	public void method95() {
		for (int j = 0; j < npcCount; j++) {
			int k = npcIndices[j];
			Npc npc = npcArray[k];
			if (npc != null)
				method96(npc);
		}
	}
	
	public void method96(Entity entity) {
		if (entity.x < 128 || entity.z < 128 || entity.x >= 13184 || entity.z >= 13184) {
			entity.animId = -1;
			entity.spotAnimId = -1;
			entity.anInt1547 = 0;
			entity.anInt1548 = 0;
			entity.x = entity.smallX[0] * 128 + entity.anInt1540 * 64;
			entity.z = entity.smallY[0] * 128 + entity.anInt1540 * 64;
			entity.method446();
		}
		if (entity == myPlayer && (entity.x < 1536 || entity.z < 1536 || entity.x >= 11776 || entity.z >= 11776)) {
			entity.animId = -1;
			entity.spotAnimId = -1;
			entity.anInt1547 = 0;
			entity.anInt1548 = 0;
			entity.x = entity.smallX[0] * 128 + entity.anInt1540 * 64;
			entity.z = entity.smallY[0] * 128 + entity.anInt1540 * 64;
			entity.method446();
		}
		if (entity.anInt1547 > loopCycle)
			method97(entity);
		else if (entity.anInt1548 >= loopCycle)
			method98(entity);
		else
			method99(entity);
		method100(entity);
		method101(entity);
	}
	
	public void method97(Entity entity) {
		int i = entity.anInt1547 - loopCycle;
		int j = entity.anInt1543 * 128 + entity.anInt1540 * 64;
		int k = entity.anInt1545 * 128 + entity.anInt1540 * 64;
		entity.x += (j - entity.x) / i;
		entity.z += (k - entity.z) / i;
		entity.anInt1503 = 0;
		if (entity.anInt1549 == 0)
			entity.turnDirection = 1024;
		else if (entity.anInt1549 == 1)
			entity.turnDirection = 1536;
		else if (entity.anInt1549 == 2)
			entity.turnDirection = 0;
		else if (entity.anInt1549 == 3)
			entity.turnDirection = 512;
	}
	
	public void method98(Entity entity) {
		if (entity.anInt1548 == loopCycle || entity.animId == -1 || entity.animDelay != 0
				|| entity.animFrameDelay + 1 > Animation.anims[entity.animId].method258(entity.animFrame)) {
			int i = entity.anInt1548 - entity.anInt1547;
			int j = loopCycle - entity.anInt1547;
			int k = entity.anInt1543 * 128 + entity.anInt1540 * 64;
			int l = entity.anInt1545 * 128 + entity.anInt1540 * 64;
			int i1 = entity.anInt1544 * 128 + entity.anInt1540 * 64;
			int j1 = entity.anInt1546 * 128 + entity.anInt1540 * 64;
			entity.x = (k * (i - j) + i1 * j) / i;
			entity.z = (l * (i - j) + j1 * j) / i;
		}
		entity.anInt1503 = 0;
		if (entity.anInt1549 == 0)
			entity.turnDirection = 1024;
		else if (entity.anInt1549 == 1)
			entity.turnDirection = 1536;
		else if (entity.anInt1549 == 2)
			entity.turnDirection = 0;
		else if (entity.anInt1549 == 3)
			entity.turnDirection = 512;
		entity.anInt1552 = entity.turnDirection;
	}
	
	public void method99(Entity entity) {
		entity.baseAnimId = entity.anInt1511;
		if (entity.smallXYIndex == 0) {
			entity.anInt1503 = 0;
			return;
		}
		if (entity.animId != -1 && entity.animDelay == 0) {
			Animation animation = Animation.anims[entity.animId];
			if (entity.anInt1542 > 0 && animation.anInt363 == 0) {
				entity.anInt1503++;
				return;
			}
			if (entity.anInt1542 <= 0 && animation.anInt364 == 0) {
				entity.anInt1503++;
				return;
			}
		}
		int i = entity.x;
		int j = entity.z;
		int k = entity.smallX[entity.smallXYIndex - 1] * 128 + entity.anInt1540 * 64;
		int l = entity.smallY[entity.smallXYIndex - 1] * 128 + entity.anInt1540 * 64;
		if (k - i > 256 || k - i < -256 || l - j > 256 || l - j < -256) {
			entity.x = k;
			entity.z = l;
			return;
		}
		if (i < k) {
			if (j < l)
				entity.turnDirection = 1280;
			else if (j > l)
				entity.turnDirection = 1792;
			else
				entity.turnDirection = 1536;
		} else if (i > k) {
			if (j < l)
				entity.turnDirection = 768;
			else if (j > l)
				entity.turnDirection = 256;
			else
				entity.turnDirection = 512;
		} else if (j < l)
			entity.turnDirection = 1024;
		else
			entity.turnDirection = 0;
		int i1 = entity.turnDirection - entity.anInt1552 & 0x7ff;
		if (i1 > 1024)
			i1 -= 2048;
		int j1 = entity.anInt1555;
		if (i1 >= -256 && i1 <= 256)
			j1 = entity.anInt1554;
		else if (i1 >= 256 && i1 < 768)
			j1 = entity.anInt1557;
		else if (i1 >= -768 && i1 <= -256)
			j1 = entity.anInt1556;
		if (j1 == -1)
			j1 = entity.anInt1554;
		entity.baseAnimId = j1;
		int k1 = 4;
		if (entity.anInt1552 != entity.turnDirection && entity.interactingEntity == -1 && entity.anInt1504 != 0)
			k1 = 2;
		if (entity.smallXYIndex > 2)
			k1 = 6;
		if (entity.smallXYIndex > 3)
			k1 = 8;
		if (entity.anInt1503 > 0 && entity.smallXYIndex > 1) {
			k1 = 8;
			entity.anInt1503--;
		}
		if (entity.aBooleanArray1553[entity.smallXYIndex - 1])
			k1 *= entity.runTiles;
		if (k1 >= 8 && entity.baseAnimId == entity.anInt1554 && entity.anInt1505 != -1)
			entity.baseAnimId = entity.anInt1505;
		if (i < k) {
			entity.x += k1;
			if (entity.x > k)
				entity.x = k;
		} else if (i > k) {
			entity.x -= k1;
			if (entity.x < k)
				entity.x = k;
		}
		if (j < l) {
			entity.z += k1;
			if (entity.z > l)
				entity.z = l;
		} else if (j > l) {
			entity.z -= k1;
			if (entity.z < l)
				entity.z = l;
		}
		if (entity.x == k && entity.z == l) {
			entity.smallXYIndex--;
			if (entity.anInt1542 > 0)
				entity.anInt1542--;
		}
	}

	private int faceDirection(int interacted, int x, int z) {
		if (interacted != -1 && interacted < 32768) {
			Npc npc = npcArray[interacted];
			if (npc != null) {
				int i1 = x - npc.x;
				int k1 = z - npc.z;
				if (i1 != 0 || k1 != 0)
					return (int) (Math.atan2(i1, k1) * 325.94900000000001D) & 0x7ff;
			}
		}

		if (interacted >= 32768) {
			int j = interacted - 32768;
			if (j == myPlayerIndexOnWorldLoggedIn)
				j = myPlayerIndex;
			Player player = playerArray[j];
			if (player != null) {
				int l1 = x - player.x;
				int i2 = z - player.z;
				if (l1 != 0 || i2 != 0)
					return (int) (Math.atan2(l1, i2) * 325.94900000000001D) & 0x7ff;
			}
		}

		return -1;
	}

	public void method100(Entity entity) {
		if (entity.anInt1504 == 0)
			return;
		int direction = faceDirection(entity.interactingEntity, entity.x, entity.z);
		if (direction != -1) {
			entity.turnDirection = direction;
		}

		if ((entity.anInt1538 != 0 || entity.anInt1539 != 0) && (entity.smallXYIndex == 0 || entity.anInt1503 > 0)) {
			int k = entity.x - (entity.anInt1538 - regionBaseX - regionBaseX) * 64;
			int j1 = entity.z - (entity.anInt1539 - regionBaseY - regionBaseY) * 64;
			if (k != 0 || j1 != 0)
				entity.turnDirection = (int) (Math.atan2(k, j1) * 325.94900000000001D) & 0x7ff;
			entity.anInt1538 = 0;
			entity.anInt1539 = 0;
		}

		int l = entity.turnDirection - entity.anInt1552 & 0x7ff;
		if (l != 0) {
			if (l < entity.anInt1504 || l > 2048 - entity.anInt1504)
				entity.anInt1552 = entity.turnDirection;
			else if (l > 1024)
				entity.anInt1552 -= entity.anInt1504;
			else
				entity.anInt1552 += entity.anInt1504;
			entity.anInt1552 &= 0x7ff;
			if (entity.baseAnimId == entity.anInt1511 && entity.anInt1552 != entity.turnDirection) {
				if (entity.anInt1512 != -1) {
					entity.baseAnimId = entity.anInt1512;
					return;
				}
				entity.baseAnimId = entity.anInt1554;
			}
		}

		if (entity instanceof Player) {
			Player player = (Player) entity;
			PlayerClone clone = player.playerClone;
			if (clone != null) {
				method100(clone);
			}
		}
	}

	public void method101(Entity entity) { // appendAnimation
		try {
			entity.aBoolean1541 = false;
			if (entity.baseAnimId != -1) {
				Animation seq = Animation.anims[entity.baseAnimId];
				if (seq != null && seq.frame2Ids != null) {
					entity.baseAnimFrameDelay++;
					if (entity.baseAnimFrame < seq.frameCount && entity.baseAnimFrameDelay > seq.frameLengths[entity.baseAnimFrame]) {
						entity.baseAnimFrameDelay = 1;
						entity.baseAnimFrame++;
						entity.baseAnimNextFrame++;
					}

					if (entity.baseAnimFrame >= seq.frameCount) {
						entity.baseAnimFrameDelay = 0;
						entity.baseAnimFrame = 0;
					}

					entity.baseAnimNextFrame = entity.baseAnimFrame + 1;
					if (entity.baseAnimNextFrame >= seq.frameCount) {
						entity.baseAnimNextFrame = 0;
					}
				} else {
					entity.baseAnimId = -1;
				}
			}
			if (entity.spotAnimId != -1 && loopCycle >= entity.spotAnimDelay) {
				int animationId = SpotAnim.cache[entity.spotAnimId].animationId;
				if (animationId == -1) {
					entity.spotAnimId = -1;
				} else {
					Animation seq = SpotAnim.cache[entity.spotAnimId].animation;
					if (seq == null || seq.frame2Ids == null) {
						entity.spotAnimId = -1;
					} else {
						if (entity.spotAnimFrame < 0) {
							entity.spotAnimFrame = 0;
						}

						entity.spotAnimFrameDelay++;
						if (entity.spotAnimFrame < seq.frameCount && entity.spotAnimFrameDelay > seq.frameLengths[entity.spotAnimFrame]) {
							entity.spotAnimFrameDelay = 1;
							entity.spotAnimFrame++;
						}

						if (entity.spotAnimFrame >= seq.frameCount) {
							entity.spotAnimId = -1;
						}

						entity.spotAnimNextFrame = entity.spotAnimFrame + 1;
						if (entity.spotAnimNextFrame >= seq.frameCount) {
							entity.spotAnimNextFrame = -1;
						}
					}
				}
			}
			if (entity.animId != -1 && entity.animDelay <= 1) {
				Animation animation_2 = Animation.anims[entity.animId];
				if (animation_2.anInt363 == 1 && entity.anInt1542 > 0 && entity.anInt1547 <= loopCycle
						&& entity.anInt1548 < loopCycle) {
					entity.animDelay = 1;
					return;
				}
			}
			if (entity.animId != -1 && entity.animDelay == 0) {
				Animation seq = Animation.anims[entity.animId];
				if (seq != null && seq.frame2Ids != null) {
					entity.animFrameDelay++;
					if (entity.animFrame < seq.frameCount && entity.animFrameDelay > seq.frameLengths[entity.animFrame]) {
						entity.animFrame++;
						entity.animFrameDelay = 1;
					}

					if (entity.animFrame >= seq.frameCount) {
						entity.animFrame -= seq.loop;
						entity.animCyclesElapsed++;
						if (entity.animCyclesElapsed >= seq.resetCycle)
							entity.animId = -1;
						if (entity.animFrame < 0 || entity.animFrame >= seq.frameCount)
							entity.animId = -1;
					}

					entity.animNextFrame = entity.animFrame + 1;
					if (entity.animNextFrame >= seq.frameCount) {
						entity.animNextFrame -= seq.loop;
						if (entity.animCyclesElapsed >= seq.resetCycle)
							entity.animNextFrame = -1;
						if (entity.animNextFrame < 0 || entity.animNextFrame >= seq.frameCount)
							entity.animNextFrame = -1;
					}

					entity.aBoolean1541 = seq.aBoolean358;
				} else {
					entity.animId = -1;
				}
			}
			if (entity.animDelay > 0)
				entity.animDelay--;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("1521 is : " + entity.spotAnimId);
		}
	}

    Stopwatch timePlayed = Stopwatch.start();
	
	private void drawGameScreen() {
		if (GraphicsDisplay.enabled && loadingStage != 2) {
			drawTextOnScreen();
		}
		
		if (fullscreenInterfaceID != -1 && (loadingStage == 2 || super.fullGameScreen != null)) {
			if (loadingStage == 2) {
				if (fullscreenInterfaceID != -1) // TODO: added 3/7/17 test if
					// this is right
					method119(anInt945, fullscreenInterfaceID);
				if (openModalInterfaceId != -1) {
					method119(anInt945, openModalInterfaceId);
				}
				anInt945 = 0;
				swapToFullscreenPixmap();
				super.fullGameScreen.initDrawingArea();
				Rasterizer.lineOffsets = fullScreenTextureArray;
				Raster.reset();
				fullRedraw = true;
				if (openModalInterfaceId != -1) {
					RSInterface rsInterface_1 = RSInterface.interfaceCache[openModalInterfaceId];
					if (rsInterface_1.width == 512 && rsInterface_1.height == 334 && rsInterface_1.interfaceType == 0) {
						rsInterface_1.width = (isFixed() ? 765 : clientWidth);
						rsInterface_1.height = (isFixed() ? 503 : clientHeight);
					}
					drawInterface(rsInterface_1, isFixed() ? 0 : (clientWidth / 2) - 765 / 2,
							isFixed() ? 8 : (clientHeight / 2) - 503 / 2, 0);
				}
				RSInterface rsInterface = RSInterface.interfaceCache[fullscreenInterfaceID];
				if (rsInterface.width == 512 && rsInterface.height == 334 && rsInterface.interfaceType == 0) {
					rsInterface.width = (isFixed() ? 765 : clientWidth);
					rsInterface.height = (isFixed() ? 503 : clientHeight);
				}
				drawInterface(rsInterface, isFixed() ? 0 : (clientWidth / 2) - 765 / 2,
						isFixed() ? 8 : (clientHeight / 2) - 503 / 2, 0);
				// if (!menuOpen) {
				// processRightClick();
				// drawTooltip();
				// } else {
				// drawMenu();
				// }
			}
			// System.out.println("In here?");
			super.fullGameScreen.drawGraphics(super.graphics, 0, 0);
			return;
		}

		if (fullRedraw) {
			fullRedraw = false;
			if (isFixed()) {
				topFrame.drawGraphics(super.graphics, 0, 0);
				leftFrame.drawGraphics(super.graphics, 0, 4);
			}
			needDrawTabArea = true;
			inputTaken = true;
			if (loadingStage != 2) {
				if (GraphicsDisplay.enabled && isFixed()) {
					mapBack562.drawSprite(516, 0);
				} else {
					if (isFixed()) {
						mapIP.initDrawingArea();
						mapBack562.drawSprite(0, 0);
						mapIP.drawGraphics(super.graphics, 516, 0);

						topFrame.initDrawingArea();
						topFrameSprite.drawSprite(0, 0);
						topFrame.drawGraphics(super.graphics, 0, 0);

						leftFrame.initDrawingArea();
						leftFrameSprite.drawSprite(0, 0);
						leftFrame.drawGraphics(super.graphics, 0, 4);

						gameScreenIP.initDrawingArea();
						gameScreenIP.drawGraphics(super.graphics, 4, 4);
					} else {
						gameScreenIP.drawGraphics(super.graphics, 0, 0);
					}
				}
			}
		}
		if (MenuManager.menuOpen)
			needDrawTabArea = true;
		if (openInventoryModalInterfaceId != -1) {
			boolean flag1 = method119(anInt945, openInventoryModalInterfaceId);
			if (flag1)
				needDrawTabArea = true;
		}
		if (activeInterfaceType == 2) {
			needDrawTabArea = true;
		}

		if (backDialogID != -1) {
			boolean flag2 = method119(anInt945, backDialogID);
			if (flag2)
				inputTaken = true;
		}
		if (atInventoryInterfaceType == 3)
			inputTaken = true;
		if (activeInterfaceType == 3)
			inputTaken = true;
		if (aString844 != null)
			inputTaken = true;
		if (MenuManager.menuOpen)
			inputTaken = true;
		if (GraphicsDisplay.enabled && isFixed()) {
			topFrameSprite.drawSprite(0, 0);
			leftFrameSprite.drawSprite(0, 4);
		}
		if (loadingStage == 2) {
			method146();
			
			if (isFixed()) {
				drawMinimap();
				if (!GraphicsDisplay.enabled) {
					mapIP.drawGraphics(super.graphics, 516, 0);
				}
			}
		}
		if (inputTaken || GraphicsDisplay.enabled || getInputDialogState() == SEARCH_STATE) {
			if (isFixed())
				drawChatArea();
			inputTaken = false;
		}
		if (isFixed()) {
			drawTabArea();
		}
		
		if (GraphicsDisplay.enabled && MenuManager.menuOpen) {
			drawMenu();
		}

		anInt945 = 0;
		updatedSkills.clear();
		updatedInventories.clear();
		updatedVarps.clear();
		if (unflagBoolean) {
			redrawGLModels = false;
			unflagBoolean = false;
		}
	}
	
	private boolean buildFriendsListMenu(RSInterface class9) {
		int i = class9.contentType;
		if (i >= 1 && i <= 200 || i >= 701 && i <= 900) {
			if (i >= 801)
				i -= 701;
			else if (i >= 701)
				i -= 601;
			else if (i >= 101)
				i -= 101;
			else
				i--;

			MenuManager.addOption("Remove <col=ffffff>" + friendsList[i], 792);
			MenuManager.addOption("Message <col=ffffff>" + friendsList[i], 639);
			return true;
		}

		if (i >= 401 && i <= 500) {
			MenuManager.addOption("Remove <col=ffffff>" + class9.message, 322);
			return true;
		}

		return false;
	}
	
	public void method104() {
		Animable_Sub3 class30_sub2_sub4_sub3 = (Animable_Sub3) aClass19_1056.reverseGetFirst();
		for (; class30_sub2_sub4_sub3 != null; class30_sub2_sub4_sub3 = (Animable_Sub3) aClass19_1056.reverseGetNext())
			if (class30_sub2_sub4_sub3.anInt1560 != plane || class30_sub2_sub4_sub3.aBoolean1567)
				class30_sub2_sub4_sub3.unlink();
			else if (loopCycle >= class30_sub2_sub4_sub3.anInt1564) {
				class30_sub2_sub4_sub3.method454(anInt945);
				if (class30_sub2_sub4_sub3.aBoolean1567) {
					class30_sub2_sub4_sub3.unlink();
				} else {
					int spotAnimUid = class30_sub2_sub4_sub3.aSpotAnim_1568.gfxId | class30_sub2_sub4_sub3.anInt1564 << 16;

					worldController.method285(class30_sub2_sub4_sub3.anInt1560, 0, class30_sub2_sub4_sub3.anInt1563, 0,
							class30_sub2_sub4_sub3.anInt1562, 60, class30_sub2_sub4_sub3.anInt1561,
							class30_sub2_sub4_sub3, false, spotAnimUid);
				}
			}
		
	}
	
	// private static final int[] IDs = {
	// 1196, 1199, 1206, 1215, 1224, 1231, 1240, 1249, 1258, 1267, 1274, 1283,
	// 1573,
	// 1290, 1299, 1308, 1315, 1324, 1333, 1340, 1349, 1358, 1367, 1374, 1381,
	// 1388,
	// 1397, 1404, 1583, 12038, 1414, 1421, 1430, 1437, 1446, 1453, 1460, 1469,
	// 15878,
	// 1602, 1613, 1624, 7456, 1478, 1485, 1494, 1503, 1512, 1521, 1530, 1544,
	// 1553,
	// 1563, 1593, 1635, 12426, 12436, 12446, 12456, 6004, 18471,
	// 12940, 12988, 13036, 12902, 12862, 13046, 12964, 13012, 13054, 12920,
	// 12882, 13062,
	// 12952, 13000, 13070, 12912, 12872, 13080, 12976, 13024, 13088, 12930,
	// 12892, 13096
	// };
	//
	// private static final int[] runeChildren = {
	// 1202, 1203, 1209, 1210, 1211, 1218, 1219, 1220, 1227, 1228, 1234, 1235,
	// 1236, 1243, 1244, 1245,
	// 1252, 1253, 1254, 1261, 1262, 1263, 1270, 1271, 1277, 1278, 1279, 1286,
	// 1287, 1293, 1294, 1295,
	// 1302, 1303, 1304, 1311, 1312, 1318, 1319, 1320, 1327, 1328, 1329, 1336,
	// 1337, 1343, 1344, 1345,
	// 1352, 1353, 1354, 1361, 1362, 1363, 1370, 1371, 1377, 1378, 1384, 1385,
	// 1391, 1392, 1393, 1400,
	// 1401, 1407, 1408, 1410, 1417, 1418, 1424, 1425, 1426, 1433, 1434, 1440,
	// 1441, 1442, 1449, 1450,
	// 1456, 1457, 1463, 1464, 1465, 1472, 1473, 1474, 1481, 1482, 1488, 1489,
	// 1490, 1497, 1498, 1499,
	// 1506, 1507, 1508, 1515, 1516, 1517, 1524, 1525, 1526, 1533, 1534, 1535,
	// 1547, 1548, 1549, 1556,
	// 1557, 1558, 1566, 1567, 1568, 1576, 1577, 1578, 1586, 1587, 1588, 1596,
	// 1597, 1598, 1605, 1606,
	// 1607, 1616, 1617, 1618, 1627, 1628, 1629, 1638, 1639, 1640, 6007, 6008,
	// 6011, 8673, 8674, 12041,
	// 12042, 12429, 12430, 12431, 12439, 12440, 12441, 12449, 12450, 12451,
	// 12459, 12460, 15881, 15882,
	// 15885, 18474, 18475, 18478
	// };
	
	private Sprite hueChooseSprite = new Sprite(24, 128);
	private Sprite colorPaletteSprite;
	private int clickedHue = 0;
	private int clickedHueMouseY = -1;
	private int clickedColor = 0;
	private int clickedColorPaletteIndex = 0;
	private int clickedColorX = -1;
	private int clickedColorY = -1;
	private int customizingSlot = 0;
	
	private void sendRecolorIndex(int slot, int recol) {
		stream.sendPacket(69);
		stream.writeByte(slot);
		stream.writeWord(recol);
	}

	private void transmitClickTab(int index) {
		stream.sendPacket(36);
		stream.writeByte(index);
	}

	private boolean debugInterfaces = false;
	
	public void drawInterface(RSInterface parent, int x, int y, int scrollY) {
		if (parent == null || parent.interfaceType != 0 || (parent.children == null && parent.dynamicComponents == null))
			return;
		if (parent.isMouseoverTriggered && hoveredScreenInterfaceId != parent.id && hoveredTabInterfaceId != parent.id
				&& hoveredChatboxInterfaceId != parent.id)
			return;
		// if (rsInterface.id != 3213)
		// System.out.println("drawing "+rsInterface.id);
		int i1 = Raster.topX;
		int j1 = Raster.topY;
		int k1 = Raster.bottomX;
		int l1 = Raster.bottomY;
		int width = x + parent.width;
		int height = y + parent.height;

		Raster.setDrawingArea(x, y, width, height);
		boolean usesDynamic = parent.dynamicComponents != null;
		int i2 = usesDynamic ? parent.dynamicComponents.length : parent.children.length;
		for (int j2 = 0; j2 < i2; j2++) {
			RSInterface child = usesDynamic ? parent.dynamicComponents[j2] : RSInterface.interfaceCache[parent.children[j2]];
			if (child == null) {
				continue;
			}
			
			int drawX;
			int drawY;
			if (usesDynamic) {
				drawX = child.x;
				drawY = child.y;
			} else {
				drawX = parent.childX[j2];
				drawY = parent.childY[j2];
			}

			drawX += x;
			drawY += y - scrollY;
			drawX += child.xOffset;
			drawY += child.yOffset;
			
			if (child.contentType > 0) {
				if (child.contentType == 1345) {
					int hueX = drawX + 185;
					int hueY = drawY + 39;
					for (int x1 = 0; x1 < hueChooseSprite.myWidth; x1++) {
						for (int y1 = 0; y1 < hueChooseSprite.myHeight; y1++) {
							hueChooseSprite.myPixels[x1
									+ y1 * hueChooseSprite.myWidth] = Rasterizer.anIntArray1482[y1 << 9 | 7 << 7 | 64];
							if (super.clickMode2 == 1 && mouseX >= hueX && mouseX <= hueX + hueChooseSprite.myWidth
									&& mouseY >= hueY && mouseY <= hueY + hueChooseSprite.myHeight
									&& mouseX == hueX + x1 && mouseY == hueY + y1) {
								clickedHue = y1 >> 1;
								clickedHueMouseY = y1;
							}
						}
					}
					
					hueChooseSprite.drawSprite(hueX, hueY);
					
					if (clickedHueMouseY != -1) {
						Raster.drawHorizontalLine(hueX - 2, hueY + clickedHueMouseY, 27, 0xffffff);
					}
					
					int paletteX = drawX + 40;
					int paletteY = drawY + 40;
					int pixel = 127;// we draw from top right
					for (int sat = 7; sat >= 0; sat--) {
						for (int i = 0; i < 16; i++) {
							int PIXEL = pixel;
							for (int light = 127; light >= 0; light--) {
								int rgb = Rasterizer.anIntArray1482[clickedHue << 10 | sat << 7 | light];
								colorPaletteSprite.myPixels[PIXEL--] = rgb;
							}
							pixel += colorPaletteSprite.myWidth;
						}
					}
					colorPaletteSprite.drawSprite(paletteX, paletteY);
					
					if (super.clickMode2 == 1 && mouseX > paletteX && mouseX < paletteX + colorPaletteSprite.myWidth
							&& mouseY > paletteY && mouseY < paletteY + colorPaletteSprite.myHeight) {
						int xCoord = mouseX - paletteX;
						int yCoord = mouseY - paletteY;
						clickedColor = colorPaletteSprite.myPixels[xCoord + yCoord * colorPaletteSprite.myWidth];
						clickedColorX = mouseX;
						clickedColorY = mouseY;
					}
					
					if (clickedColor != -1) {
						Raster.drawHorizontalLine(clickedColorX - 5, clickedColorY, 10, 0xffffff);
						Raster.drawVerticalLine(clickedColorX, clickedColorY - 5, 10, 0xffffff);
						int previewX = 105 + drawX;
						int previewY = 183 + drawY;
						for (int i = 0; i < Rasterizer.anIntArray1482.length; i++) {
							if (Rasterizer.anIntArray1482[i] == clickedColor) {
								clickedColorPaletteIndex = i;
								//System.out.println("color = " + i);
								Raster.fillRect(previewX, previewY, 40, 40, Rasterizer.anIntArray1482[i], false);
								break;
							}
						}
					}
					
					continue;
				} else if (child.contentType >= 1346 && child.contentType <= 1349) {
					int id = child.contentType - 1346;
					int color = CAPE_COLORS[id];
					if (customizedItem != null) {
						color = customizedItem.newModelColors[id] & 0xffff;
					}
					Raster.fillRect(drawX, drawY, child.width, child.height, Rasterizer.anIntArray1482[color], false);
					continue;
				} else {
					drawFriendsListOrWelcomeScreen(child);
				}
			}
			
			// Skip the inventory interface type for now.
			if (child.hidden || (drawX > width || drawY > height || drawY + child.height < y) && child.interfaceType != 2 && !BitwiseHelper.testBit(child.flags, RSInterface.IGNORE_BOUNDS_BIT)) {
				continue;
			}
			
			if (debugInterfaces) {
				int copyX = Raster.topX;
				int copyY = Raster.topY;
				int copyW = Raster.bottomX;
				int copyH = Raster.bottomY;
				Raster.setDrawingArea(0, 0, clientWidth, clientHeight);
				Raster.drawRect(drawX, drawY, child.width, child.height, 0xffffff);
				Raster.setDrawingArea(copyX, copyY, copyW, copyH);
//				newSmallFont.drawCenteredString(Integer.toString(child.id), drawX+20, drawY+20, 0xFFFFFF, 0);
				if (child.interfaceType == 3) {//Don't draw rectangle over rectangle
					continue;
				}
			}
			
			if (child.interfaceType == 0) {
				drawOnBankInterface();
				if (child.scrollPosition > child.scrollMax - child.height)
					child.scrollPosition = child.scrollMax - child.height;
				if (child.scrollPosition < 0)
					child.scrollPosition = 0;
				drawInterface(child, drawX, drawY, child.scrollPosition);
				if (child.scrollMax > child.height) {
					drawScrollbar(child.height, child.scrollPosition, drawY, drawX + child.width, child.scrollMax);
				}
			} else if (child.interfaceType != 1)
				if (child.interfaceType == 2) {
					int i3 = 0;
					for (int l3 = 0; l3 < child.height; l3++) {
						for (int l4 = 0; l4 < child.width; l4++) {
							int k5 = drawX + l4 * (32 + child.invSpritePadX);
							int j6 = drawY + l3 * (32 + child.invSpritePadY);
							if (i3 < 20) {
								k5 += child.spritesX[i3];
								j6 += child.spritesY[i3];
							}
							if (child.inv[i3] > 0) {
								int k6 = 0;
								int j7 = 0;
								int j9 = child.inv[i3] - 1;
								if (k5 > Raster.topX - 32 && k5 < Raster.bottomX && j6 > Raster.topY - 32
										&& j6 < Raster.bottomY || activeInterfaceType != 0 && anInt1085 == i3) {
									int count = child.invStackSizes[i3];
									boolean drawCount = true;
									if (BitwiseHelper.testBit(child.flags, RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT)) {
										drawCount = false;
									}
									
									Sprite class30_sub2_sub1_sub1_2;
									if (itemSelected == 1 && anInt1283 == i3 && anInt1284 == child.id) {
										class30_sub2_sub1_sub1_2 = ItemDef.getSprite(j9, count, 2,
												0, false, drawCount);
									} else {
										class30_sub2_sub1_sub1_2 = ItemDef.getSprite(j9, count, 1,
												3153952, false, drawCount);
									}
									
									if (class30_sub2_sub1_sub1_2 != null) {
										if (activeInterfaceType != 0 && anInt1085 == i3 && anInt1084 == child.id) {
											k6 = super.mouseX - anInt1087;
											j7 = super.mouseY - anInt1088;
											if (k6 < 5 && k6 > -5)
												k6 = 0;
											if (j7 < 5 && j7 > -5)
												j7 = 0;
											if (anInt989 < clickSpeed) {
												k6 = 0;
												j7 = 0;
											}
											class30_sub2_sub1_sub1_2.drawTransparentSprite(k5 + k6, j6 + j7, 128);
											if (j6 + j7 < Raster.topY && parent.scrollPosition > 0) {
												int i10 = (anInt945 * (Raster.topY - j6 - j7)) / 3;
												if (i10 > anInt945 * 10)
													i10 = anInt945 * 10;
												if (i10 > parent.scrollPosition)
													i10 = parent.scrollPosition;
												parent.scrollPosition -= i10;
												anInt1088 += i10;
											}
											if (j6 + j7 + 32 > Raster.bottomY
													&& parent.scrollPosition < parent.scrollMax
													- parent.height) {
												int j10 = (anInt945 * ((j6 + j7 + 32) - Raster.bottomY)) / 3;
												if (j10 > anInt945 * 10)
													j10 = anInt945 * 10;
												if (j10 > parent.scrollMax - parent.height
														- parent.scrollPosition)
													j10 = parent.scrollMax - parent.height
															- parent.scrollPosition;
												parent.scrollPosition += j10;
												anInt1088 -= j10;
											}
										} else if ((atInventoryInterfaceType != 0 && atInventoryIndex == i3
												&& atInventoryInterface == child.id) || (BitwiseHelper.testBit(child.flags, RSInterface.TRANSPARENT_ON_ZERO_COUNT) && count == 0))
											class30_sub2_sub1_sub1_2.drawTransparentSprite(k5, j6, 128);
										else
											class30_sub2_sub1_sub1_2.drawSprite(k5, j6);
									}
								}
							} else if (child.sprites != null && i3 < 20) {
								Sprite class30_sub2_sub1_sub1_1 = child.sprites[i3];
								if (class30_sub2_sub1_sub1_1 != null)
									class30_sub2_sub1_sub1_1.drawSprite(k5, j6);
							}
							i3++;
						}
					}
				} else if (child.interfaceType == 3) {
					boolean flag = false;
					if (hoveredChatboxInterfaceId == child.id || hoveredTabInterfaceId == child.id || hoveredScreenInterfaceId == child.id)
						flag = true;
					int j3;
					if (interfaceIsSelected(child)) {
						j3 = child.enabledColor;
						if (flag && child.anInt239 != 0)
							j3 = child.anInt239;
					} else {
						j3 = child.disabledColor;
						if (flag && child.mouseHoverTextRecolor != 0)
							j3 = child.mouseHoverTextRecolor;
					}
					
					if (child.transparency == 0) {
						if (child.filled)
							Raster.fillRect(drawX, drawY, child.width, child.height, j3, BitwiseHelper.testBit(child.flags, RSInterface.RECT_IGNORE_ATLAS));
						else
							Raster.drawRect(drawX, drawY, child.width, child.height, j3);
					} else if (child.filled) {
						Raster.fillRect(drawX, drawY, child.width, child.height, j3, 256 - (child.transparency & 0xff),
								BitwiseHelper.testBit(child.flags, RSInterface.RECT_IGNORE_ATLAS));
					} else {
						Raster.drawRect(drawX, drawY, child.width, child.height, j3, 256 - (child.transparency & 0xff));
					}
				} else if (child.interfaceType == 4) {
					RSFont font = chooseFont(child.textDrawingAreas);
					
					String s = child.message;
					boolean flag1 = false;
					if (hoveredChatboxInterfaceId == child.id || hoveredTabInterfaceId == child.id || hoveredScreenInterfaceId == child.id)
						flag1 = true;
					int i4;
					if (interfaceIsSelected(child)) {
						i4 = child.enabledColor;
						if (flag1 && child.anInt239 != 0)
							i4 = child.anInt239;
						if (child.enabledMessage != null && !child.enabledMessage.isEmpty())
							s = child.enabledMessage;
					} else {
						i4 = child.disabledColor;
						if (flag1 && child.mouseHoverTextRecolor != 0)
							i4 = child.mouseHoverTextRecolor;
					}
					if (child.atActionType == 6 && aBoolean1149) {
						s = "Please wait...";
						i4 = child.disabledColor;
					}
					if (child.message.contains("Click here to continue") || child.width == 480) {// class9_1.message.contains("Misc")/*
						// ||
						// Raster.width
						// ==
						// 519*//*Raster.width
						// ==
						// 519*/)
						// {
						// System.out.println("rasterW2:"+class9_1.width);
						if (i4 == 0xffff00)
							i4 = 255;
						if (i4 == 49152)
							i4 = 0xffffff;
					}
					if ((child.parentID == 1151) || (child.parentID == 12855)) {
						switch (i4) {
							case 16773120:
								i4 = 0xFE981F;
								break;
							case 7040819:
								i4 = 0xAF6A1A;
								break;
						}
					}
					for (int l6 = drawY + font.baseCharacterHeight; s.length() > 0; l6 += font.baseCharacterHeight) {
						if (s.indexOf("%") != -1) {
							do {
								int k7 = s.indexOf("%1");
								if (k7 == -1)
									break;
								if (child.id < 4000 || child.id > 5000 && child.id != 13921
										&& child.id != 13922 && child.id != 12171 && child.id != 12172)
									s = s.substring(0, k7) + methodR(extractInterfaceValues(child, 0))
											+ s.substring(k7 + 2);
								else
									s = s.substring(0, k7) + interfaceIntToString(extractInterfaceValues(child, 0))
											+ s.substring(k7 + 2);
							} while (true);
							do {
								int l7 = s.indexOf("%2");
								if (l7 == -1)
									break;
								s = s.substring(0, l7) + interfaceIntToString(extractInterfaceValues(child, 1))
										+ s.substring(l7 + 2);
							} while (true);
							do {
								int i8 = s.indexOf("%3");
								if (i8 == -1)
									break;
								s = s.substring(0, i8) + interfaceIntToString(extractInterfaceValues(child, 2))
										+ s.substring(i8 + 2);
							} while (true);
							do {
								int j8 = s.indexOf("%4");
								if (j8 == -1)
									break;
								s = s.substring(0, j8) + interfaceIntToString(extractInterfaceValues(child, 3))
										+ s.substring(j8 + 2);
							} while (true);
							do {
								int k8 = s.indexOf("%5");
								if (k8 == -1)
									break;
								s = s.substring(0, k8) + interfaceIntToString(extractInterfaceValues(child, 4))
										+ s.substring(k8 + 2);
							} while (true);
						}
						int l8 = s.indexOf("\\n");
						String s1;
						if (l8 != -1) {
							s1 = s.substring(0, l8);
							s = s.substring(l8 + 2);
						} else {
							s1 = s;
							s = "";
						}
						if (child.centerText) {
							font.drawCenteredString(s1, drawX + child.width / 2, l6, i4, child.textShadow ? 0 : -1);
						} else {
							font.drawBasicString(s1, drawX, l6, i4, child.textShadow ? 0 : -1);
						}
					}
				} else if (child.interfaceType == 5) {
					Sprite image;
					if (interfaceIsSelected(child)) {
						image = child.enabledSprite;
					} else if (child.itemId != -1) {
						image = ItemDef.getSprite(child.itemId, child.itemAmount, 1,
								3153952, false, !BitwiseHelper.testBit(child.flags, RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT));
					} else {
						image = child.disabledSprite;
					}

					if (image != null) {
						if (targetInterfaceId != -1 && child.id == targetInterfaceId) {
							image.drawSprite(drawX, drawY, 0xffffff);
						} else if (BitwiseHelper.testBit(child.flags, RSInterface.CLIP_BIT)) {
							Raster.setDrawingArea(drawX, drawY, drawX + child.width, drawY + child.height);

							if (child.transparency != 0) {
								image.drawTransparentSprite(drawX, drawY, 256 - (child.transparency & 0xff));
							} else {
								image.drawSprite(drawX, drawY);
							}

							Raster.setDrawingArea(x, y, width, height);
						} else if (BitwiseHelper.testBit(child.flags, RSInterface.TILING_FLAG_BIT)) {
							int spriteWidth = image.myWidth;
							int spriteHeight = image.myHeight;
							int widthLoops = (child.width + spriteWidth - 1) / spriteWidth;
							int heightLoops = (child.height + spriteHeight - 1) / spriteHeight;

							int maxWidth = drawX + child.width;
							int maxHeight = drawY + child.height;

							Raster.setDrawingArea(x, y, width, height);
							for (int heightId = 0; heightId < heightLoops; heightId++) {
								int realY = heightId * spriteHeight + drawY;
								int overflowY = realY + spriteHeight;
								if (overflowY > maxHeight) {
									image.myHeight -= overflowY - maxHeight;
								}

								for (int widthId = 0; widthId < widthLoops; widthId++) {
									int realX = widthId * spriteWidth + drawX;
									int overflowX = realX + spriteWidth;
									if (overflowX > maxWidth) {
										image.myWidth -= overflowX - maxWidth;
									}

									if (child.transparency != 0) {
										image.drawTransparentSprite(realX, realY, 256 - (child.transparency & 0xff));
									} else {
										image.drawSprite(realX, realY);
									}

									image.myWidth = spriteWidth;
								}
								image.myHeight = spriteHeight;
							}
						} else if (child.transparency != 0) {
							image.drawTransparentSprite(drawX, drawY, 256 - (child.transparency & 0xff));
						} else {
							image.drawSprite(drawX, drawY);
						}
					}

					if (Autocast && child.id == autocastId) {
						magicAuto.drawSprite(drawX - 3, drawY - 3);
					}
				} else if (child.interfaceType == 6) {
					unflagBoolean = true;
					// System.out.println("drawing here");
					int k3 = Rasterizer.center_x;
					int j4 = Rasterizer.center_y;
					Rasterizer.center_x = drawX + child.width / 2;
					Rasterizer.center_y = drawY + child.height / 2;
					boolean flag2 = interfaceIsSelected(child);
					int i7;
					if (flag2)
						i7 = child.enabledAnimation;
					else
						i7 = child.disabledAnimation;
					boolean animated = redrawGLModels;
					if (child.disabledMediaType == 7 && child.disabledMediaID == 0) {
						animated = true;
					}
					
					Model model;
					if (i7 == -1) {
						model = child.getModel(-1, -1, flag2);
					} else {
						Animation animation = Animation.anims[i7];
						model = child.getModel(animation.frame1Ids[child.anInt246],
								animation.frame2Ids[child.anInt246], flag2);
						animated = true;
					}
					
					if (model != null) {
						float i5 = Rasterizer.SINE_FLOAT[child.modelRotationY] * child.modelZoom;
						float l5 = Rasterizer.COSINE_FLOAT[child.modelRotationY] * child.modelZoom;

						if (!GraphicsDisplay.enabled) {
							Rasterizer.clearDepthBuffer();
							Rasterizer.testTopY = true;
						}
						
						model.method482(child.modelRotationX, 0, child.modelRotationY, 0, i5, l5, child.id | (long) child.componentIndex << 32,
								true, true, animated);
						if (!GraphicsDisplay.enabled) {
							Rasterizer.testTopY = false;
						} else {
							switch (child.id) {
								case 8715:
								case 2809:
								case 6962:
								case 6964:
									VBORenderer.renderAllVBOModels(false, false, true);

									GraphicsDisplay.ortho();
									break;
							}
						}
					}
					
					Rasterizer.center_x = k3;
					Rasterizer.center_y = j4;
				} else if (child.interfaceType == 7) {
					RSFont font = chooseFont(child.textDrawingAreas);
					
					int k4 = 0;
					for (int j5 = 0; j5 < child.height; j5++) {
						for (int i6 = 0; i6 < child.width; i6++) {
							if (child.inv[k4] > 0) {
								ItemDef itemDef = ItemDef.forID(child.inv[k4] - 1);
								String s2 = itemDef.name;
								if (itemDef.stackable || child.invStackSizes[k4] != 1)
									s2 = s2 + " x" + intToKOrMilLongName(child.invStackSizes[k4]);
								int i9 = drawX + i6 * (115 + child.invSpritePadX);
								int k9 = drawY + j5 * (12 + child.invSpritePadY);
								if (child.centerText) {
									font.drawCenteredString(s2, i9 + child.width / 2, k9, child.disabledColor,
											child.textShadow ? 0 : -1);
								} else {
									font.drawCenteredString(s2, i9, k9, child.disabledColor,
											child.textShadow ? 0 : -1);
								}
							}
							k4++;
						}
					}
				} else if (child.interfaceType == 9) {
					if (lastHoverBox != child.id) {
						hoverDelay = 0;
						lastHoverBox = child.id;
					}

					if (hoverDelay <= 20) {
						hoverDelay++;
					} else {
						String s = child.message;
						if (child.enabledMessage != null && !child.enabledMessage.isEmpty() && interfaceIsSelected(child)) {
							s = child.enabledMessage;
						}

						drawHoverBox(drawX, drawY, s);
					}
				} else if (child.interfaceType == 10) {
					RSFont font = chooseFont(child.textDrawingAreas);
					
					String s = child.message;
					boolean flag1 = false;
					if (hoveredChatboxInterfaceId == child.id || hoveredTabInterfaceId == child.id || hoveredScreenInterfaceId == child.id)
						flag1 = true;
					int i4;
					if (interfaceIsSelected(child)) {
						i4 = child.enabledColor;
						if (flag1 && child.anInt239 != 0)
							i4 = child.anInt239;
						if (child.enabledMessage != null && !child.enabledMessage.isEmpty())
							s = child.enabledMessage;
					} else {
						i4 = child.disabledColor;
						if (flag1 && child.mouseHoverTextRecolor != 0)
							i4 = child.mouseHoverTextRecolor;
					}
					if (child.atActionType == 6 && aBoolean1149) {
						s = "Please wait...";
						i4 = child.disabledColor;
					}
					
					doValues(child, s);
					
					font.drawInterfaceText(s, drawX, drawY, child.width, child.height, i4, child.textShadow ? 0 : -1, 256 - (child.transparency & 0xff), child.horizontalAlignment, child.verticalAlignment, child.verticalSpacing);
				}
		}
		Raster.setDrawingArea(i1, j1, k1, l1);
	}
	
	private String doValues(RSInterface component, String text) {
		if (text.indexOf("%") != -1) {
			for (int i = 1; i <= 5; i++) {
				while (true) {
					int index = text.indexOf("%" + i);
					if (index == -1) {
						break;
					}
					
					text = text.substring(0, index) + interfaceIntToString(extractInterfaceValues(component, i - 1)) + text.substring(index + 2);
				}
			}
		}
		
		return text;
	}

	public RSFont chooseFont(int id) {
		switch (id) {
			case 1:
				return newRegularFont;
			case 2:
				return newBoldFont;
			case 3:
				return newFancyFont;
			default:
				return newSmallFont;
		}
	}

	public final static String methodR(int j) {
		if (j >= 0 && j < 10000)
			return String.valueOf(j);
		if (j >= 10000 && j < 10000000)
			return j / 1000 + "K";
		if (j >= 10000000 && j < 999999999)
			return j / 1000000 + "M";
		if (j >= 999999999)
			return "*";
		else
			return "?";
	}
	
	public static String formatObjCount(int count) {
		if (count < 100000) {
			return "<col=ffff00>" + count + "</col>";
		}
		if (count < 10000000) {
			return "<col=ffffff>" + (count / 1000) + "K</col>";
		}
		return "<col=00ff80>" + (count / 1000000) + "M</col>";
	}
	
	public int hoverDelay = 0;
	
	public int lastHoverBox = 0;
	
	public void drawHoverBox(int xPos, int yPos, String text) {
		String[] results = text.split("\n");
		int length = results.length;
		int width = 0;
		int height = length * 16 + 4;
		
		for (int i = 0; i < length; i++) {
			int textWidth = newRegularFont.getTextWidth(results[i]);
			if (width < textWidth) {
				width = textWidth;
			}
		}

		width += 6;

		int widthToCheck;
		if (isFixed() && !GraphicsDisplay.enabled) {
			widthToCheck = 512;
		} else {
			widthToCheck = clientWidth;
		}

		if (xPos + width > widthToCheck) {
			xPos = widthToCheck - width;
		}

		Raster.defaultDrawingAreaSize();
		Raster.fillRect(xPos, yPos, width, height, 0xFFFFA0, false);
		Raster.drawRect(xPos, yPos, width, height, 0);
		xPos += 3;
		yPos += 14;
		for (int i = 0; i < length; i++) {
			newRegularFont.drawBasicString(results[i], xPos, yPos, 0, -1);
			yPos += 16;
		}
	}
	
	private void method107(int i, int j, RSBuffer stream, Player player) {
		if ((i & 0x400) != 0) {
			player.anInt1543 = stream.method428();
			player.anInt1545 = stream.method428();
			player.anInt1544 = stream.method428();
			player.anInt1546 = stream.method428();
			player.anInt1547 = stream.method436() + loopCycle;
			player.anInt1548 = stream.method435() + loopCycle;
			player.anInt1549 = stream.method428();
			player.method446();
		}
		if ((i & 0x100) != 0) {
			int graphic = stream.method434();
			if (graphic == 65535) {
				graphic = -1;
			}

			int k = stream.readDWord();
			player.spotAnimId = graphic;
			player.anInt1524 = k >> 16;
			player.spotAnimDelay = loopCycle + (k & 0xffff);
			player.spotAnimFrame = 0;
			player.spotAnimNextFrame = 1;
			player.spotAnimFrameDelay = 0;
			if (player.spotAnimDelay > loopCycle)
				player.spotAnimFrame = -1;

			if (graphic != -1) {
				SpotAnim spotAnim = SpotAnim.cache[graphic];
				if (spotAnim != null) {
					spotAnim.preloadAnimation(onDemandFetcher);
				}
			}
		}
		if ((i & 8) != 0) {
			int l = stream.method434();
			if (l == 65535) {
				l = -1;
			}
			if (l >= Animation.anims.length) {
				l = -1;
				System.out.println("Broken player anim id = " + l);
			}

			int i2 = stream.method427();
			if (l == player.animId && l != -1) {
				int i3 = Animation.anims[l].anInt365;
				if (i3 == 1) {
					player.animFrame = 0;
					player.animNextFrame = 1;
					player.animFrameDelay = 0;
					player.animDelay = i2;
					player.animCyclesElapsed = 0;
				} else if (i3 == 2) {
					player.animCyclesElapsed = 0;
				}
			} else if (l == -1 || player.animId == -1
					|| Animation.anims[l].anInt359 >= Animation.anims[player.animId].anInt359) {
				player.animId = l;
				player.animFrame = 0;
				player.animNextFrame = 1;
				player.animFrameDelay = 0;
				player.animDelay = i2;
				player.animCyclesElapsed = 0;
				player.anInt1542 = player.smallXYIndex;
			}
		}
		if ((i & 4) != 0) {
			player.textSpoken = stream.readString();
			if (player.textSpoken.charAt(0) == '~') {
				player.textSpoken = player.textSpoken.substring(1);
				pushMessage(player.textSpoken, ChatMessage.PUBLIC_MESSAGE_PLAYER, player.name);
			} else if (player == myPlayer)
				pushMessage(player.textSpoken, ChatMessage.PUBLIC_MESSAGE_PLAYER, player.name);
			player.anInt1513 = 0;
			player.anInt1531 = 0;
			player.textCycle = 150;
		}
		if ((i & 0x80) != 0) {
			int i1 = stream.method434();
			ChatCrownType chatCrown = ChatCrownType.get(stream.readUnsignedByte());
			int j3 = stream.method427();
			int k3 = stream.currentOffset;
			if (player.name != null && player.isVisible()) {
				long l3 = TextClass.longForName(player.name);
				boolean flag = false;
				if (chatCrown.isIgnorable()) {
					for (int i4 = 0; i4 < ignoreCount; i4++) {
						if (ignoreListAsLongs[i4] != l3)
							continue;
						flag = true;
						break;
					}
				}

				if (!flag) {
					try {
						aStream_834.currentOffset = 0;
						stream.method442(j3, 0, aStream_834.buffer);
						aStream_834.currentOffset = 0;
						String s = TextInput.method525(j3, aStream_834);
						if (variousSettings[ConfigCodes.PROFAINTY_FILTER] == 1) {
							s = Censor.doCensor(s);
						}
						player.textSpoken = s;
						player.anInt1513 = i1 >> 8;
						player.anInt1531 = i1 & 0xff;
						player.textCycle = 150;

						int type;
						if (chatCrown.isStaff()) {
							type = ChatMessage.PUBLIC_MESSAGE_STAFF;
						} else {
							type = ChatMessage.PUBLIC_MESSAGE_PLAYER;
						}

						pushMessage(s, type, chatCrown.toCrown() + player.name);
					} catch (Exception exception) {
						ErrorHandler.submitError("cde2", exception);
					}
				}
			}
			stream.currentOffset = k3 + j3;
		}
		if ((i & 1) != 0) {
			player.setInteractingEntity(stream.method434());
		}
		if ((i & 0x10) != 0) {
			int j1 = stream.method427();
			byte abyte0[] = new byte[j1];
			RSBuffer stream_1 = new RSBuffer(abyte0);
			stream.readBytes(j1, 0, abyte0);
			aStreamArray895s[j] = stream_1;
			player.updatePlayer(stream_1);
		}
		if ((i & 2) != 0) {
			player.anInt1538 = stream.method436();
			player.anInt1539 = stream.method434();
		}
		if ((i & 0x20) != 0) {
			int k1 = stream.readUnsignedByte();
			int k2 = stream.readUnsignedByte();
			int icon = stream.readUnsignedByte();
			player.updateHitData(k2, k1, loopCycle, icon);
			player.loopCycleStatus = loopCycle + 300;
			player.currentHealth = stream.readUnsignedWord();
			player.maxHealth = stream.readUnsignedWord();
		}
		if ((i & 0x200) != 0) {
			int l1 = stream.readUnsignedByte();
			int l2 = stream.readUnsignedByte();
			int icon = stream.readUnsignedByte();
			player.updateHitData(l2, l1, loopCycle, icon);
		}
		if ((i & 0x800) != 0) {
			player.startFill = stream.readUnsignedWord();
			player.stayCycleAfterFill = stream.readUnsignedByte();
			player.fill = stream.readUnsignedByte();
			player.maxFill = stream.readUnsignedWord();
			player.reverse = stream.readUnsignedByte() == 1;
			player.fillColor1 = stream.readDWord();
			player.fillColor2 = stream.readDWord();
			player.totalBarCycle = (player.maxFill + loopCycle + player.stayCycleAfterFill);
		}
		if ((i & 0x1000) != 0) {
			int x = stream.readSignedByte();
			if (x == 127) {
				player.playerClone = null;
			} else {
				int z = stream.readSignedByte();
				int time = stream.readUnsignedWord();
				player.playerClone = new PlayerClone(player, x * 128, z * 128, loopCycle + time);
			}
		}
	}

	public static void drawLoadingHD(String text, int percent) {
		if (text == null) {
			return;
		}

		GraphicsDisplay.getInstance().drawStart();

		Raster.initDrawingArea(clientWidth, clientHeight);
		if (SpriteCache.backgroundSprite != null) {
			SpriteCache.backgroundSprite.drawSprite(0, 0);
		}
		
		int color = 0xffc259;
		int rectHeight = 18;
		GraphicsDisplay.getInstance().drawRect(clientWidth / 2 - 152, clientHeight / 2 - rectHeight, 305, 22, color,
				255);
		GraphicsDisplay.getInstance().addBox(clientWidth / 2 - 150, clientHeight / 2 + 2 - rectHeight, percent * 3,
				17, color, 255, false, null);
		color = 0;
		GraphicsDisplay.getInstance().addBox((clientWidth / 2 - 150) + percent * 3, clientHeight / 2 + 1 - rectHeight,
				300 - percent * 3, rectHeight, color, 255, false, null);
		color = 0x404040;
		if (Client.instance.newBoldFont != null) {
			Client.instance.newBoldFont.drawCenteredString(text + " - " + percent + "%", clientWidth / 2, clientHeight / 2 + 15 - rectHeight, color, -1);
		} else {
			SimpleRendering.drawString(text + " - " + percent + "%", clientWidth / 2 - (text.length() * 7) / 2,
					clientHeight / 2 + 15 - rectHeight, color, null);
		}
		
		GraphicsDisplay.getInstance().drawEnd(null);
	}

	@Override
	public void processDrawing() {
		if (rsAlreadyLoaded || loadingError || genericLoadingError || clearingCache) {
			showErrorScreen();
			return;
		}

		if (updateGame.get()) {
			updateGame();
			updateGame.set(false);
		}
		
		if (GraphicsDisplay.enabled && !GraphicsDisplay.getInstance().mayRender(this))
			return;
		
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().drawStart();
		}

		anInt1061++;
		if (!loggedIn) {
			loginRenderer.draw();
		} else {
			drawGameScreen();
		}

		if (GraphicsDisplay.enabled) {
			if (GraphicsDisplay.vboRendering) {
				GraphicsDisplay.getInstance().renderScene();
			}
			
			/*
			 * if ((loadingStage == 2 || true) && lastMinimapUpdated != 0 &&
			 * System.currentTimeMillis() - lastMinimapUpdated > 500) {
			 * hasWalked = true; lastMinimapUpdated = 0; }
			 */
			
			GraphicsDisplay.getInstance().drawEnd(this);
			
			// vbo switch
			if (GraphicsDisplay.switchVboRendering) {
				GraphicsDisplay.switchVboRendering = false;
				if (GraphicsDisplay.vboRenderingSupported) {
					GraphicsDisplay.vboRendering = !GraphicsDisplay.vboRendering;
					// clearCaches = true;
					// softCacheClear = false;
					// loadMap(99);
					// Model.prepareArrays();
					Cache.initStreamingArrays();
					// writeSettings();
				}
			}
			
			/*
			 * if (clearCaches) { clearCaches = false; clearCaches();
			 * softCacheClear = true; } else if (clearTextureCaches) {
			 * Cache.releaseAtlases(); clearTextureCaches = false; }
			 */
			
			// load new textures
			AtlasTextureLoader.process();
		}
		
		if (toggleGl.get()) {
			GraphicsDisplay.enabled = !GraphicsDisplay.enabled;
			
			Settings.changeSetting("opengl_enabled", GraphicsDisplay.enabled);
			
			if (GraphicsDisplay.enabled) {
				// turning on
				removeListeners();

				VBOManager.init();
				ModelConfig.init();
				TextureAtlas.init();
				ModelLoader.init();
				AtlasTextureLoader.init();
				GLBufferManager.init();
				Cache.initStreamingArrays();

				GraphicsDisplay.getInstance().pixelFormatAttempts = 0;
				GraphicsDisplay.getInstance().initialize(this);
			} else {
				// turning off
				Cache.releaseAtlases();
				// System.out.println(GLBufferManager.buffers.size() + " total
				// buffers " + GLBufferManager.getTotalGenBuffers());
				Cache.removeOldVBOModels = true;
				if (GraphicsDisplay.vboRendering) {
					Cache.clearFloorVBOs();
				}
				Cache.clearVBOModels = true;
				VBORenderer.clearBuffers();
				Cache.nullLoader();
				Cache.flushFloorVBOs();
				Cache.clearSpriteCache();
				Cache.clearLetterCache();
				Cache.clearBackgroundCache();
				Cache.clearTextureCache();
				Cache.getModelVBOs().clear();
				GLBufferManager.clearAllBuffers();
				VBOManager.cleanup();
				ModelConfig.cleanup();
				TextureAtlas.cleanup();
				ModelLoader.cleanup();
				AtlasTextureLoader.cleanup();
				GLBufferManager.cleanup();
				
				Display.destroy();
				Mouse.destroy();
				Keyboard.destroy();
				GraphicsDisplay.getInstance().initialized = false;

				addListeners();
				drawScreen();
			}
			Insets insets = frame.getInsets();
			Dimension size = new Dimension(frame.getWidth() - insets.left - insets.right, frame.getHeight() - insets.top - insets.right);
			setSizeOfClient(currentDisplayMode, size);
			applyInsets = false;

			toggleGl.set(false);
			
			loadingStage = 1;
			loadingStartTime = System.currentTimeMillis();
			setTextOnScren("Loading - please wait.");
			System.gc();// garbage collect afterwards

			setVarp(ConfigCodes.OPEN_GL_ENABLED, GraphicsDisplay.enabled);
		}
	}
	
	private boolean isFriendOrSelf(String s) {
		if (s == null) {
			return false;
		}
		
		for (int i = friendsCount - 1; i >= 0; i--) {
			if (s.equalsIgnoreCase(friendsList[i])) {
				return true;
			}
		}
		
		return s.equalsIgnoreCase(myPlayer.name);
	}
	
	private boolean isInIgnoreList(String s) {
		if (s == null) {
			return false;
		}
		
		for (int i = ignoreCount - 1; i >= 0; i--) {
			if (s.equalsIgnoreCase(ignoreList[i])) {
				return true;
			}
		}
		
		return false;
	}
	
	private static String combatDiffColor(int i, int j) {
		int k = i - j;
		if (k < -9)
			return "<col=ff0000>";
		if (k < -6)
			return "<col=ff3000>";
		if (k < -3)
			return "<col=ff7000>";
		if (k < 0)
			return "<col=ffb000>";
		if (k > 9)
			return "<col=ff00>";
		if (k > 6)
			return "<col=40ff00>";
		if (k > 3)
			return "<col=80ff00>";
		if (k > 0)
			return "<col=c0ff00>";
		return "<col=ffff00>";
	}
	
	private void drawXpCounter() {
		boolean isFixed = isFixed();
		int baseX = 0;
		int baseY = 0;

		if (isFixed) {
			baseX = 419;

			if (GraphicsDisplay.enabled) {
				baseX += 4;
				baseY += 4;
			}
		} else {
			baseX = clientWidth - 304;
		}

		if (drawFlag && xpToDraw > 0) {
			String text = Long.toString(xpToDraw);
			int textWidth = newSmallFont.getBasicWidth(text);

			int x = baseX + 30 - textWidth;
			int y = baseY + flagPos + (isFixed ? 70 : 35);
			xpFlag.drawSprite(x, y);

			x = baseX + 60 - textWidth;
			y = baseY + flagPos + (isFixed ? 90 : 50);
			newSmallFont.drawBasicString(text, x, y, 0xCC6600, 1);
			x += newSmallFont.getBasicWidth(text);
			newSmallFont.drawBasicString("xp", x, y, 0xCC6600, 1);

			flagPos++;

			if (flagPos >= 50) {
				flagPos = 0;
				xpToDraw = 0;
			}
		}

		if (drawXpBar) {
			String text = Long.toString(xpToDrawOnBar);
			int textWidth = newRegularFont.getTextWidth(text);
			int x = baseX;
			int y = baseY + (isFixed ? 54 : 11);
			sprite1.drawSprite(x, y);

			x += 2;
			y = baseY + (isFixed ? 67 : 25);
			newRegularFont.drawBasicString("XP:", x, y, 0xFFFFFD, 0);

			x = baseX + 86 - textWidth;
			newRegularFont.drawBasicString(text, x, y, 0xFFFFFD, 0);
		}
	}
	
	private void draw3dScreen() {
		boolean isFixed = isFixed();
		Widget.draw();
		drawXpCounter();
		if (showChat)
			drawSplitPrivateChat();
		if (crossType == 1) {
			int offset = isFixed && !GraphicsDisplay.enabled ? 4 : 0;
			crosses[crossIndex / 100].drawSprite(crossX - 8 - offset, crossY - 8 - offset);
		} else if (crossType == 2) {
			int offset = isFixed && !GraphicsDisplay.enabled ? 4 : 0;
			crosses[4 + crossIndex / 100].drawSprite(crossX - 8 - offset, crossY - 8 - offset);
		}

		if (debugInterfaces) {
			int screenWidth = clientWidth;
    		int screenHeight = clientHeight;

    		int w = screenWidth;
    		int h = screenHeight;
    		int x = (screenWidth - w) / 2;
    		int y = (screenHeight - h) / 2;
    		int rgb = 0xff0000;
    		Raster.drawRect(x, y, w, h, rgb);

    		int w2 = w - 240;
    		int h2 = h - 165;
    		int x2 = x;
    		int y2 = y;
    		int rgb2 = 0xffff00;
    		Raster.drawRect(x2, y2, w2, h2, rgb2);

    		int w3 = 512;
    		int h3 = 334;
    		int x3 = (w2 - w3) / 2;
    		int y3 = (h2 - h3) / 2;
    		int rgb3 = 0xffffff;
    		Raster.drawRect(x3, y3, w3, h3, rgb3);
		}

		drawInterfaces(isFixed);
		drawMoneyPouch(isFixed);

		if (anInt1055 == 1) {
			multiOverlay.drawSprite(isFixed ? 472 : clientWidth - 250, isFixed ? 296 : clientHeight - 200);
		}
		if (fpsOn) {
			int x = clientWidth - 315;
			int y = 20;
			int color = RSApplet.fps < 20 ? 0xff0000 : 0xffff00;
			newRegularFont.drawBasicString("Fps: ", x, y, color, -1);
			x += 25;
			newRegularFont.drawBasicString(Integer.toString(RSApplet.fps), x, y, color, -1);
		}
		if (displayPing) {
			int x = clientWidth - 315;
			int y = 15;
			if (fpsOn) {
				y = 35;
			}
			
			int rgb = 0xCC0000;
			if (ping < 100) {
				rgb = 0x00FF00;
			} else if (ping >= 100 && ping < 200) {
				rgb = 0xFFFF00;
			}
			
			newRegularFont.drawBasicString("Ping: ", x, y, rgb, -1);
			x += 30;
			newRegularFont.drawBasicString(Long.toString(ping), x, y, rgb, -1);
		}

		if (clientData) {
			final int playerX = regionBaseX + (myPlayer.x - 6 >> 7);
            final int playerY = regionBaseY + (myPlayer.z - 6 >> 7);
            final int regionId = (playerX >> 6 << 8) + (playerY >> 6);

            int x = 5;
            int y = isFixed ? 253 : clientHeight - 247;

			int newX = x;
			newRegularFont.drawBasicString("Fps: ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth("Fps: ");
			newRegularFont.drawBasicString(Integer.toString(fps), newX, y, 0xffff00, -1);
			y += 15;

			Runtime runtime = Runtime.getRuntime();
            final int memory = (int) ((runtime.totalMemory() - runtime.freeMemory()) / 1024L);
            newX = x;
			newRegularFont.drawBasicString("Mem: ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth("Mem: ");
			String memoryString = Integer.toString(memory);
			newRegularFont.drawBasicString(memoryString, newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(memoryString);
			newRegularFont.drawBasicString("k", newX, y, 0xffff00, -1);
			y += 15;

			newX = x;
			newRegularFont.drawBasicString("Mouse X: ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth("Mouse X: ");
			String mouseXString = Integer.toString(super.mouseX);
			newRegularFont.drawBasicString(mouseXString, newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(mouseXString);
			newRegularFont.drawBasicString(", Mouse Y: ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(", Mouse Y: ");
			String mouseYString = Integer.toString(super.mouseY);
			newRegularFont.drawBasicString(mouseYString, newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(mouseYString);
			if (!isFixed) {
				newRegularFont.drawBasicString(", rX: ", newX, y, 0xffff00, -1);
				newX += newRegularFont.getBasicWidth(", rX: ");
				String deltaXString = Integer.toString(clientWidth - super.mouseX);
				newRegularFont.drawBasicString(deltaXString, newX, y, 0xffff00, -1);
				newX += newRegularFont.getBasicWidth(deltaXString);
				newRegularFont.drawBasicString(", rY: ", newX, y, 0xffff00, -1);
				newX += newRegularFont.getBasicWidth(", rY: ");
				String deltaYString = Integer.toString(clientHeight - super.mouseY);
				newRegularFont.drawBasicString(deltaYString, newX, y, 0xffff00, -1);
				newX += newRegularFont.getBasicWidth(deltaYString);
			}
			y += 15;

			newX = x;
			newRegularFont.drawBasicString("Coords: ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth("Coords: ");
			String playerXString = Integer.toString(playerX);
			newRegularFont.drawBasicString(playerXString, newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(playerXString);
			newRegularFont.drawBasicString(", ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(", ");
			newRegularFont.drawBasicString(Integer.toString(playerY), newX, y, 0xffff00, -1);
			y += 15;

            newX = x;
			newRegularFont.drawBasicString("Region: ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth("Region: ");
			newRegularFont.drawBasicString(Integer.toString(regionId), newX, y, 0xffff00, -1);
			y += 15;

            newX = x;
			newRegularFont.drawBasicString("Region Base: ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth("Region Base: ");
			String regionBaseXString = Integer.toString(regionBaseX);
			newRegularFont.drawBasicString(regionBaseXString, newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(regionBaseXString);
			newRegularFont.drawBasicString(", ", newX, y, 0xffff00, -1);
			newX += newRegularFont.getBasicWidth(", ");
			newRegularFont.drawBasicString(Integer.toString(regionBaseY), newX, y, 0xffff00, -1);
			y += 15;
		}

		if (anInt1104 != 0) {
			int j = anInt1104 / 50;
			int l = j / 60;
			j %= 60;
			int offset = clientHeight - 174;
			if (j < 10)
				newRegularFont.drawBasicString("System update in: " + l + ":0" + j, 4, offset, 0xffff00, 0);
			else
				newRegularFont.drawBasicString("System update in: " + l + ":" + j, 4, offset, 0xffff00, 0);
		}
		if (announcement != null) {
			int offset = clientHeight - 174;
			if (anInt1104 != 0) {
				offset -= newRegularFont.baseCharacterHeight + 1;
			}

			newRegularFont.drawBasicString(announcement.message, 4, offset, 0xffff00, 0);
		}

		if (!MenuManager.menuOpen) {
			drawLeftClickPreview();
		} else if (menuScreenArea == 0 && !GraphicsDisplay.enabled) {
			drawMenu();
			ActionCursors.setCursor(null);
		} else {
			ActionCursors.setCursor(null);
		}
	}
	
	public void addIgnore(long l) {
		if (l == 0L)
			return;
		if (ignoreCount >= 100) {
			pushGameMessage("Your ignore list is full. Max of 100 hit");
			return;
		}
		String s = TextClass.fixName(TextClass.nameForLong(l));
		if (s.equalsIgnoreCase(this.myUsername)) {
			pushGameMessage("You cannot add yourself on ignore list.");
			return;
		}
		for (int j = 0; j < ignoreCount; j++)
			if (ignoreListAsLongs[j] == l) {
				pushGameMessage(s + " is already on your ignore list");
				return;
			}
		for (int k = 0; k < friendsCount; k++)
			if (friendsListAsLongs[k] == l) {
				pushGameMessage("Please remove " + s + " from your friend list first");
				return;
			}
		
		ignoreList[ignoreCount] = s;
		ignoreListAsLongs[ignoreCount++] = l;
		needDrawTabArea = true;
		stream.sendPacket(133);
		stream.writeQWord(l);
	}
	
	public void method114() {
		for (int i = -1; i < playerCount; i++) {
			int j;
			if (i == -1)
				j = myPlayerIndex;
			else
				j = playerIndices[i];
			Player player = playerArray[j];
			if (player != null)
				method96(player);
		}
		
	}
	
	public void method115() {
		if (loadingStage == 2) {
			for (GameObject gameObject = (GameObject) aClass19_1179
					.reverseGetFirst(); gameObject != null; gameObject = (GameObject) aClass19_1179.reverseGetNext()) {
				if (gameObject.currentIDRequested == -1) {
					gameObject.unlink();
				}

				if (gameObject.removeTime > 0)
					gameObject.removeTime--;
				if (gameObject.removeTime == 0) {
					if (gameObject.anInt1299 < 0
							|| ObjectManager.method178(gameObject.anInt1299, gameObject.anInt1301, gameObject.regionType)) {
						method142(gameObject.tileY, gameObject.plane, gameObject.anInt1300, gameObject.anInt1301,
								gameObject.tileX, gameObject.objectType, gameObject.anInt1299, gameObject.regionType);
						gameObject.unlink();
					}
				} else {
					if (gameObject.spawnTime > 0)
						gameObject.spawnTime--;
					if (gameObject.spawnTime == 0 && gameObject.tileX >= 1 && gameObject.tileY >= 1
							&& gameObject.tileX <= 102 && gameObject.tileY <= 102
							&& (gameObject.currentIDRequested < 0 || ObjectManager
							.method178(gameObject.currentIDRequested, gameObject.currentTypeRequested, gameObject.regionType))) {
						method142(gameObject.tileY, gameObject.plane, gameObject.currentFaceRequested,
								gameObject.currentTypeRequested, gameObject.tileX, gameObject.objectType,
								gameObject.currentIDRequested, gameObject.regionType);
						gameObject.spawnTime = -1;
						if (gameObject.currentIDRequested == gameObject.anInt1299 && gameObject.anInt1299 == -1)
							gameObject.unlink();
						else if (gameObject.currentIDRequested == gameObject.anInt1299
								&& gameObject.currentFaceRequested == gameObject.anInt1300
								&& gameObject.currentTypeRequested == gameObject.anInt1301)
							gameObject.unlink();
					}
				}
			}
			
		}
	}
	
	private void determineMenuSize() {
		int i = newBoldFont.getTextWidth("Choose Option");
		for (int j = 0; j < MenuManager.menuActionRow; j++) {
			int k = newBoldFont.getTextWidth(MenuManager.menuActionName[j]);
			if (k > i)
				i = k;
		}
		i += 8;
		int l = 15 * MenuManager.menuActionRow + 21;
		if (isFixed() && !GraphicsDisplay.enabled) {
			if (super.saveClickX > 4 && super.saveClickY > 4 && super.saveClickX < 516 && super.saveClickY < 338) {
				int i1 = super.saveClickX - 4 - i / 2;
				if (i1 + i > 512)
					i1 = 512 - i;
				if (i1 < 0)
					i1 = 0;
				int l1 = super.saveClickY - 4;
				if (l1 + l > 334) // 334
					l1 = 334 - l; // 334
				if (l1 < 0)
					l1 = 0;
				MenuManager.menuOpen = true;
				menuScreenArea = 0;
				menuOffsetX = i1;
				menuOffsetY = l1;
				menuWidth = i;
				menuHeight = 15 * MenuManager.menuActionRow + 22;
			}
			if (super.saveClickX > 519 && super.saveClickY > 168 && super.saveClickX < 765 && super.saveClickY < 503) {
				int j1 = super.saveClickX - 519 - i / 2;
				if (j1 < 0)
					j1 = 0;
				else if (j1 + i > 245)
					j1 = 245 - i;
				int i2 = super.saveClickY - 168;
				if (i2 < 0)
					i2 = 0;
				else if (i2 + l > 333)
					i2 = 333 - l;
				MenuManager.menuOpen = true;
				menuScreenArea = 1;
				menuOffsetX = j1;
				menuOffsetY = i2;
				menuWidth = i;
				menuHeight = 15 * MenuManager.menuActionRow + 22;
			}
			if (super.saveClickX > 0 && super.saveClickY > 338 && super.saveClickX < 516 && super.saveClickY < 503) {
				int k1 = super.saveClickX - i / 2;
				if (k1 < 0)
					k1 = 0;
				else if (k1 + i > 516)
					k1 = 516 - i;
				int j2 = super.saveClickY - 338;
				if (j2 < 0)
					j2 = 0;
				else if (j2 + l > 165)
					j2 = 165 - l;
				MenuManager.menuOpen = true;
				menuScreenArea = 2;
				menuOffsetX = k1;
				menuOffsetY = j2;
				menuWidth = i;
				menuHeight = 15 * MenuManager.menuActionRow + 22;
			}
			if (super.saveClickX > 519 && super.saveClickY > 0 && super.saveClickX < 765 && super.saveClickY < 168) {
				int j1 = super.saveClickX - 519 - i / 2;
				if (j1 < 0)
					j1 = 0;
				else if (j1 + i > 245)
					j1 = 245 - i;
				int i2 = super.saveClickY;
				if (i2 < 0)
					i2 = 0;
				else if (i2 + l > 168)
					i2 = 168 - l;
				MenuManager.menuOpen = true;
				menuScreenArea = 3;
				menuOffsetX = j1;
				menuOffsetY = i2;
				menuWidth = i;
				menuHeight = 15 * MenuManager.menuActionRow + 22;
			}
		} else {
			if (super.saveClickX > 0 && super.saveClickY > 0 && super.saveClickX < clientWidth
					&& super.saveClickY < clientHeight) {
				int i1 = super.saveClickX - i / 2;
				if (i1 + i > clientWidth)
					i1 = clientWidth - i;
				if (i1 < 0)
					i1 = 0;
				int l1 = super.saveClickY;
				if (l1 + l > clientHeight)
					l1 = clientHeight - l;
				if (l1 < 0)
					l1 = 0;
				MenuManager.menuOpen = true;
				menuScreenArea = 0;
				menuOffsetX = i1;
				menuOffsetY = l1;
				menuWidth = i;
				menuHeight = 15 * MenuManager.menuActionRow + 22;
			}
		}
	}
	
	public void method117(RSBuffer stream) {
		stream.initBitAccess();
		int j = stream.readBits(1);
		if (j == 0)
			return;
		int k = stream.readBits(2);
		switch (k) {
		case 0:
			anIntArray894[anInt893++] = myPlayerIndex;
			return;
		case 1:
			int l = stream.readBits(3);
			myPlayer.moveInDir(false, l);
			int k1 = stream.readBits(1);
			if (k1 == 1)
				anIntArray894[anInt893++] = myPlayerIndex;
			return;
		case 2:
			myPlayer.runTiles = stream.readBits(4);
			for (int i = 0; i < myPlayer.runTiles; i++) {
				int l1 = stream.readBits(3);
				myPlayer.moveInDir(true, l1);
			}
			int j2 = stream.readBits(1);
			if (j2 == 1)
				anIntArray894[anInt893++] = myPlayerIndex;
			return;
		case 3:
			plane = stream.readBits(2);
			int j1 = stream.readBits(1);
			int i2 = stream.readBits(1);
			if (i2 == 1)
				anIntArray894[anInt893++] = myPlayerIndex;
			int k2 = stream.readBits(7);
			int l2 = stream.readBits(7);
			
			if (baseOffsetX != 0 || baseOffsetY != 0) {
				baseDX = baseOffsetX;
				baseDY = baseOffsetY;
				baseOffsetX = baseOffsetY = 0;
			}
			
			myPlayer.setPos(l2, k2, j1 == 1);
			return;
		}
	}
	
	private boolean method119(int i, int j) {
		boolean flag1 = false;
		RSInterface class9 = RSInterface.interfaceCache[j];
		// System.out.println();
		if (class9 == null) {
			System.out.println("interfaceId:" + j + " is null");
			return false;
		}

		int length = class9.children == null ? 0 : class9.children.length;
		for (int k = 0; k < length; k++) {
			if (class9.children[k] == -1) {
				break;
			}
			RSInterface class9_1 = RSInterface.interfaceCache[class9.children[k]];
			
			if (class9_1.hidden) {
				continue;
			}
			
			if (class9_1.interfaceType == 0 || class9_1.interfaceType == 1)
				flag1 |= method119(i, class9_1.id);
			if (class9_1.interfaceType == 6 && (class9_1.disabledAnimation != -1 || class9_1.enabledAnimation != -1)) {
				boolean flag2 = interfaceIsSelected(class9_1);
				int l;
				if (flag2)
					l = class9_1.enabledAnimation;
				else
					l = class9_1.disabledAnimation;
				if (l != -1) {
					Animation animation = Animation.anims[l];
					// System.out.println("animating
					// "+l+":"+class9_1.anInt208+":"+class9_1.anInt246);
					for (class9_1.anInt208 += i; class9_1.anInt208 > animation.method258(class9_1.anInt246); ) {
						class9_1.anInt208 -= animation.method258(class9_1.anInt246);
						class9_1.anInt246++;
						if (class9_1.anInt246 >= animation.frameCount) {
							class9_1.anInt246 -= animation.loop;
							if (class9_1.anInt246 < 0 || class9_1.anInt246 >= animation.frameCount)
								class9_1.anInt246 = 0;
						}
						flag1 = true;
					}
					
				}
			}
		}
		
		return flag1;
	}
	
	private int method120() {
		try {
			int playerX = myPlayer.x >> 7;
			int playerY = myPlayer.z >> 7;
			int absX = regionBaseX + playerX;
			int absY = regionBaseY + playerY;
			boolean insideGamble = absX >= 1441 && absX <= 1464 && absY >= 4444 && absY <= 4470;
			if (roofOff || insideGamble) {
				return plane;
			}

			int j = 3;
			if (yCameraCurve < 310) {
				int k = xCameraPos >> 7;
				int l = yCameraPos >> 7;
				if (l < 0 || k < 0 || l >= 104 || k >= 104)
					return plane;
				if ((byteGroundArray[plane][k][l] & 4) != 0)
					j = plane;

				int i1;
				int j1;
				if (Camera.camType == CamType.PLAYER) {
					i1 = playerX;
					j1 = playerY;
				} else {
					i1 = Camera.cameraX;
					j1 = Camera.cameraZ;
				}

				int k1;
				if (i1 > k)
					k1 = i1 - k;
				else
					k1 = k - i1;
				int l1;
				if (j1 > l)
					l1 = j1 - l;
				else
					l1 = l - j1;
				if (k1 > l1) {
					int i2 = (l1 * 0x10000) / k1;
					int k2 = 32768;
					while (k != i1) {
						if (k < i1)
							k++;
						else if (k > i1)
							k--;

						if (k < 0 || l < 0 || k >= 104 || l >= 104)
							break;

						if ((byteGroundArray[plane][k][l] & 4) != 0)
							j = plane;
						k2 += i2;
						if (k2 >= 0x10000) {
							k2 -= 0x10000;
							if (l < j1)
								l++;
							else if (l > j1)
								l--;

							if (k < 0 || l < 0 || k >= 104 || l >= 104)
								break;

							if ((byteGroundArray[plane][k][l] & 4) != 0)
								j = plane;
						}
					}
				} else {
					int j2 = (k1 * 0x10000) / l1;
					int l2 = 32768;
					while (l != j1) {
						if (l < j1)
							l++;
						else if (l > j1)
							l--;

						if (k < 0 || l < 0 || k >= 104 || l >= 104)
							break;

						if ((byteGroundArray[plane][k][l] & 4) != 0)
							j = plane;
						l2 += j2;
						if (l2 >= 0x10000) {
							l2 -= 0x10000;
							if (k < i1)
								k++;
							else if (k > i1)
								k--;

							if (k < 0 || l < 0 || k >= 104 || l >= 104)
								break;

							if ((byteGroundArray[plane][k][l] & 4) != 0)
								j = plane;
						}
					}
				}
			}
			if ((byteGroundArray[plane][playerX][playerY] & 4) != 0)
				j = plane;
			return j;
		} catch(Exception e) {
			ErrorHandler.submitError("cameracalc, x="+myPlayer.x + ", z="+myPlayer.z + ", camx="+xCameraPos+", camz="+yCameraPos+", camy="+zCameraPos+", regionx="+regionBaseX+", regionz="+regionBaseY, e);
			return plane;
		}
	}
	
	private int method121() {
		if (roofOff && loggedIn) {
			return plane;
		}
		int j = method42(plane, yCameraPos, xCameraPos);
		if (j - zCameraPos < 800 && (byteGroundArray[plane][xCameraPos >> 7][yCameraPos >> 7] & 4) != 0)
			return plane;
		else
			return 3;
	}
	
	public void delIgnore(long l) {
		if (l == 0L)
			return;
		for (int j = 0; j < ignoreCount; j++)
			if (ignoreListAsLongs[j] == l) {
				ignoreList[j] = null;
				ignoreCount--;
				needDrawTabArea = true;
				System.arraycopy(ignoreListAsLongs, j + 1, ignoreListAsLongs, j, ignoreCount - j);
				
				stream.sendPacket(74);
				stream.writeQWord(l);
				return;
			}
	}
	
	@Override
	public String getParameter(String s) {
		return super.getParameter(s);
	}
	
	private int extractInterfaceValues(RSInterface class9, int j) {
		if (class9.valueIndexArray == null || j >= class9.valueIndexArray.length)
			return -2;
		try {
			int ai[] = class9.valueIndexArray[j];
			int k = 0;
			int l = 0;
			int i1 = 0;
			do {
				int j1 = ai[l++];
				int k1 = 0;
				byte byte0 = 0;
				switch (j1) {
					case 0:
						return k;
					case 1:
						k1 = currentStats[ai[l++]];
						break;
					case 2:
						k1 = maxStats[ai[l++]];
						break;
					case 3:
						k1 = currentExp[ai[l++]];
						break;
					case 4:
						int inventoryId = ai[l++];
						int k2 = ai[l++];

						RSInterface inventory = RSInterface.interfaceCache[3214];
						for (int i = 0; i < inventory.inv.length; i++) {
							int itemId = inventory.inv[i];
							if (itemId != 30048) {// +1 cus of dumb 137
								continue;
							}

							RSInterface pouch = RSInterface.interfaceCache[43671];
							for (int i2 = 0; i2 < pouch.inv.length; i2++) {
								int runePouch = pouch.inv[i2];
								if (runePouch == k2 + 1) {
									k1 += pouch.invStackSizes[i2];
								}
							}
						}

						k1 += itemCount(inventoryId, k2);
						break;
					case 5:
						k1 = variousSettings[ai[l++]];
						break;
					case 6:
						k1 = anIntArray1019[maxStats[ai[l++]] - 1];
						break;
					case 7:
						k1 = (variousSettings[ai[l++]] * 100) / 46875;
						break;
					case 8:
						k1 = myPlayer.combatLevel;
						break;
					case 9:
						for (int l1 = 0; l1 < Skills.skillsCount; l1++) {
							if (Skills.skillEnabled[l1]) {
								k1 += maxStats[l1];
							}
						}
						break;
					case 10:
						RSInterface class9_2 = RSInterface.interfaceCache[ai[l++]];
						int l2 = ai[l++] + 1;
						if (l2 >= 0 && l2 < ItemDef.totalItems && (!ItemDef.forID(l2).members || isMembers)) {
							for (int k3 = 0; k3 < class9_2.inv.length; k3++) {
								if (class9_2.inv[k3] != l2)
									continue;
								k1 = 0x3b9ac9ff;
								break;
							}

						}
						break;
					case 11:
						k1 = energy;
						break;
					case 12:
						k1 = weight;
						break;
					case 13:
						int i2 = variousSettings[ai[l++]];
						int i3 = ai[l++];
						k1 = (i2 & 1 << i3) == 0 ? 0 : 1;
						break;
					case 14:
						int j2 = ai[l++];
						VarBit varBit = VarBit.cache[j2];
						int l3 = varBit.anInt648;
						int i4 = varBit.anInt649;
						int j4 = varBit.anInt650;
						int k4 = anIntArray1232[j4 - i4];
						k1 = variousSettings[l3] >> i4 & k4;
						break;
					case 15:
						byte0 = 1;
						break;
					case 16:
						byte0 = 2;
						break;
					case 17:
						byte0 = 3;
						break;
					case 18:
						k1 = (myPlayer.x >> 7) + regionBaseX;
						break;
					case 19:
						k1 = (myPlayer.z >> 7) + regionBaseY;
						break;
					case 20:
						k1 = ai[l++];
						break;
				}
				if (byte0 == 0) {
					if (i1 == 0)
						k += k1;
					if (i1 == 1)
						k -= k1;
					if (i1 == 2 && k1 != 0)
						k /= k1;
					if (i1 == 3)
						k *= k1;
					i1 = 0;
				} else {
					i1 = byte0;
				}
			} while (true);
		} catch (Exception _ex) {
			return -1;
		}
	}

	public static int itemCount(int inventoryId, int item) {
		item++;

		RSInterface inventory = RSInterface.interfaceCache[inventoryId];
		int count = 0;
		for (int id = 0, length = inventory.inv.length; id < length; id++) {
			if (inventory.inv[id] == item) {
				count += inventory.invStackSizes[id];
			}
		}

		return count;
	}

	public void drawLeftClickPreview() {
		if (MenuManager.menuActionRow < 2 && itemSelected == 0 && targetInterfaceId == -1) {
			ActionCursors.setCursor(null);
			return;
		}

		String s;
		if (itemSelected == 1 && MenuManager.menuActionRow < 2) {
			s = "Use " + selectedItemName + " with...";
		} else if (targetInterfaceId != -1 && MenuManager.menuActionRow < 2) {
			s = spellTooltip + "...";
		} else {
			s = MenuManager.menuActionName[MenuManager.getLeftClick()];
		}
		if (MenuManager.menuActionRow > 2) {
			s = s + "<col=ffffff> / " + (MenuManager.menuActionRow - 2) + " more options";
		}

		int posX = 4;
		int posY = 15;
		if (GraphicsDisplay.enabled && isFixed()) {
			posX += 4;
			posY += 4;
		}

		newBoldFont.drawBasicString(s, posX, posY, 0xffffff, 0);

		ActionCursors.setCursor(MenuManager.menuActionName[MenuManager.menuActionRow - 1]);
	}

	public void processMinimapOrbs() {
		if (mouseInRegion(clientWidth - (isFixed() ? 249 : 217), isFixed() ? 46 : 3,
				clientWidth - (isFixed() ? 249 : 217) + 34, (isFixed() ? 46 : 3) + 34)) {
			hoverPos = 0; // xp counter
		} else if (mouseInRegion(isFixed() ? clientWidth - 58 : getOrbX(1), getOrbY(1),
				(isFixed() ? clientWidth - 58 : getOrbX(1)) + 57, getOrbY(1) + 34)) {
			hoverPos = 1; // prayer
		} else if (mouseInRegion(isFixed() ? clientWidth - 58 : getOrbX(2), getOrbY(2),
				(isFixed() ? clientWidth - 58 : getOrbX(2)) + 57, getOrbY(2) + 34)) {
			hoverPos = 2; // run
		} else if (mouseInRegion(isFixed() ? clientWidth - 74 : getOrbX(3), getOrbY(3),
				(isFixed() ? clientWidth - 74 : getOrbX(3)) + 57, getOrbY(3) + 34)) {
			hoverPos = 3; // summoning
		} else if (mouseInRegion(clientWidth - (isFixed() ? 249 : 58), // x1
				isFixed() ? 86 : 157, // y1
				clientWidth - (isFixed() ? 249 : 58) + 31, // x2
				(isFixed() ? 86 : 157) + 27)) { // y2
			hoverPos = 4; // money pouch
		} else if (mouseInRegion(clientWidth - (isFixed() ? 244 : 168), // x1
				isFixed() ? 4 : 0, // y1
				clientWidth - (isFixed() ? 244 : 168) + 28, // x2
				(isFixed() ? 4 : 0) + 30)) { // y2
			hoverPos = 5; // compass
			// System.out.println("in region");
		} else {
			hoverPos = -1;
		}
	}
	
	public boolean choosingLeftClick;
	
	public int leftClick;
	
	public String[] leftClickNames = { "Call Follower", "Dismiss", "Take BoB", "Renew Familiar", "Interact", "Attack",
			"Follower Details", "Cast"
			// "Follower Details", "Attack", "Interact", "Renew Familiar", "Take
			// BoB",
			// "Dismiss", "Call Follower"
	};
	
	public int[] leftClickActions = { 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1026 };
	
	// public void rightClickMapArea() {
	// if (mouseInRegion(clientWidth - (isFixed() ? 249 : 217), isFixed() ? 46 :
	// 3,
	// clientWidth - (isFixed() ? 249 : 217) + 34, (isFixed() ? 46 : 3) + 34)) {
	// menuActionName[1] = "Reset counter";
	// menuActionID[1] = 1013;
	// menuActionName[2] = xpClicked ? "Hide counter" : "Show counter";
	// menuActionID[2] = 1006;
	// menuActionRow = 3;
	// }
	// if (mouseInRegion(isFixed() ? clientWidth - 58 : getOrbX(1), getOrbY(1),
	// (isFixed() ? clientWidth - 58 : getOrbX(1)) + 57, getOrbY(1) + 34)) {
	//
	// menuActionName[3] = prayClicked ? "Toggle Quick-" + prayerBook + " off"
	// : "Toggle Quick-" + prayerBook + " on";
	// menuActionID[3] = 1500;
	// menuActionName[2] = "Select quick " + prayerBook;
	// menuActionID[2] = 1506;
	// menuActionName[1] = orbTextEnabled[1] ? "Hide text" : "Show text";
	// menuActionID[1] = 1509;
	// menuActionRow = 4;
	// }
	// if (mouseInRegion(isFixed() ? clientWidth - 58 : getOrbX(2), getOrbY(2),
	// (isFixed() ? clientWidth - 58 : getOrbX(2)) + 57, getOrbY(2) + 34)) {
	//
	// menuActionName[3] = runState == 0 ? "Turn run off" : "Turn run on";
	// menuActionID[3] = 1014;
	// menuActionName[2] = resting ? "Turn rest off" : "Turn rest on";
	// menuActionID[2] = 1016;
	// menuActionName[1] = orbTextEnabled[2] ? "Hide text" : "Show text";
	// menuActionID[1] = 1510;
	// menuActionRow = 4;
	// }
	// if (mouseInRegion(isFixed() ? clientWidth - 58 : getOrbX(0), getOrbY(0),
	// (isFixed() ? clientWidth - 58 : getOrbX(0)) + 57, getOrbY(0) + 34)) {
	// menuActionName[1] = orbTextEnabled[0] ? "Hide text" : "Show text";
	// menuActionID[1] = 1508;
	// menuActionRow = 2;
	// }
	//// int x = super.mouseX; // Face North on compass
	//// int y = super.mouseY;
	// if (hoverPos == 5) {
	// //if (x >= 531 && x <= 557 && y >= 7 && y <= 40) {
	// menuActionName[1] = "Face North";
	// menuActionID[1] = 696;
	// menuActionRow = 2;
	// }
	// if (mouseInRegion(isFixed() ? clientWidth - 74 : getOrbX(3), getOrbY(3),
	// (isFixed() ? clientWidth - 74 : getOrbX(3)) + 57, getOrbY(3) + 34)) {
	// if (leftClick != -1 && leftClick < 8) {
	// menuActionName[1] = orbTextEnabled[3] ? "Hide text" : "Show text";
	// menuActionID[1] = 1511;
	// menuActionName[2] = "Select left-click option";
	// menuActionID[2] = 1027;
	// menuActionName[3] = leftClickNames[leftClick].equals("Cast")
	// ? leftClickNames[leftClick] + " <col=ff00>" + getFamiliar().getSpecialAttack()
	// : leftClickNames[leftClick];
	// menuActionID[3] = leftClickActions[leftClick];
	// menuActionRow = 4;
	// } else if (choosingLeftClick) {
	// menuActionName[1] = orbTextEnabled[3] ? "Hide text" : "Show text";
	// menuActionID[1] = 1511;
	// menuActionName[2] = "Select left-click option";
	// menuActionID[2] = 1027;
	// menuActionName[3] = "Call Follower";
	// menuActionID[3] = 1018;
	// menuActionName[4] = "Dismiss";
	// menuActionID[4] = 1019;
	// menuActionName[5] = "Take BoB";
	// menuActionID[5] = 1020;
	// menuActionName[6] = "Renew Familiar";
	// menuActionID[6] = 1021;
	// menuActionName[7] = "Interact";
	// menuActionID[7] = 1022;
	// menuActionName[8] = "Attack";
	// menuActionID[8] = 1023;
	// if (getFamiliar().isActive() && getFamiliar().getSpecialAttack().length()
	// > 0) {
	// menuActionName[9] = "Cast <col=ff00>" + getFamiliar().getSpecialAttack();
	// menuActionID[9] = 1026;
	// menuActionName[10] = "Follower Details";
	// menuActionID[10] = 1024;
	// menuActionRow = 11;
	// } else {
	// menuActionName[9] = "Follower Details";
	// menuActionID[9] = 1024;
	// menuActionRow = 10;
	// }
	// } else {
	// menuActionName[1] = orbTextEnabled[3] ? "Hide text" : "Show text";
	// menuActionID[1] = 1511;
	// menuActionName[2] = "Select left-click option";
	// menuActionID[2] = 1027;
	// menuActionRow = 3;
	// }
	// }
	// }
	
	private void drawMinimap() {
		boolean hdEnabled = GraphicsDisplay.enabled;
		boolean isFixed = isFixed();
		int xPosOffset = isFixed ? 0 : clientWidth - 246;
		if (hdEnabled) {
			xPosOffset += 516;
		} else if (isFixed) {
			mapIP.initDrawingArea();
		}
		if (minimapBlackout == 2) {
			int x;
			int y;
			if (isFixed) {
				x = xPosOffset + 32;
				y = 10;
			} else {
				x = Client.clientWidth - 159;
				y = 6;
			}

			if (hdEnabled) {
				miniMap.drawBlackout(x, y, anIntArray1052, anIntArray1229);
			} else {
				Raster.fillPixels(x, y, 0, anIntArray1052, anIntArray1229);
			}
		} else if (loadingStage == 2) {
			int i = viewRotation + minimapRotation & 0x7ff;
			int j = 48 + myPlayer.x / 32;
			int l2 = 464 - myPlayer.z / 32;
			if (lastPlayerX != j || lastPlayerZ != l2) {
				refreshGLMinimap = true;
			}

			int spriteX;
			int spriteY;
			if (isFixed) {
				spriteX = xPosOffset + 32;
				spriteY = 10;
			} else {
				spriteX = Client.clientWidth - 159;
				spriteY = 6;
			}

			if (hdEnabled) {
				miniMap.method352hd(155, i, anIntArray1229, 256 + minimapZoom, anIntArray1052, l2, 140,
						j, spriteX, spriteY);
			} else {
				miniMap.method352(155, i, anIntArray1229, 256 + minimapZoom, anIntArray1052, l2, spriteY,
						spriteX, 140, j);
			}
			lastPlayerZ = l2;
			lastPlayerX = j;
			for (int j5 = 0; j5 < mapFunctionsCount; j5++) {
				int k = (mapFunctionsX[j5] * 4 + 2) - myPlayer.x / 32;
				int i3 = (mapFunctionsZ[j5] * 4 + 2) - myPlayer.z / 32;
				markMinimap(mapFunctions[mapFunctionsId[j5]], k, i3);
			}
			for (int k5 = 0; k5 < 104; k5++) {
				for (int l5 = 0; l5 < 104; l5++) {
					NodeList class19 = groundArray[plane][k5][l5];
					if (class19 != null) {
						int l = (k5 * 4 + 2) - myPlayer.x / 32;
						int j3 = (l5 * 4 + 2) - myPlayer.z / 32;
						markMinimap(mapDotItem, l, j3);
					}
				}
			}
			for (int i6 = 0; i6 < npcCount; i6++) {
				Npc npc = npcArray[npcIndices[i6]];
				if (npc != null && npc.isVisible()) {
					EntityDef entityDef = npc.desc;
					if (entityDef.childrenIDs != null)
						entityDef = entityDef.method161();
					if (entityDef != null && entityDef.aBoolean87 && entityDef.aBoolean84) {
						int i1 = npc.x / 32 - myPlayer.x / 32;
						int k3 = npc.z / 32 - myPlayer.z / 32;
						markMinimap(mapDotNPC, i1, k3);
					}
				}
			}
			for (int j6 = 0; j6 < playerCount; j6++) {
				Player player = playerArray[playerIndices[j6]];
				if (player != null && player.isVisible()) {
					int j1 = player.x / 32 - myPlayer.x / 32;
					int l3 = player.z / 32 - myPlayer.z / 32;
					
					boolean inClan = false;
					if (myPlayer.clanId != 0 && player.clanId != 0 && myPlayer.clanId == player.clanId) { //if (clanList.contains(player.name)) {
						inClan = true;
					}
					
					boolean isFriend = false;
					long l6 = TextClass.longForName(player.name);
					for (int k6 = 0; k6 < friendsCount; k6++) {
						if (l6 != friendsListAsLongs[k6] || friendsNodeIDs[k6] == 0)
							continue;
						isFriend = true;
						break;
					}
					boolean flag2 = false;
					if (myPlayer.team != 0 && player.team != 0 && myPlayer.team == player.team)
						flag2 = true;
					if (player.team != 0) {
						isFriend = false;
						inClan = false;
					}
					if (isFriend)
						markMinimap(mapDotFriend, j1, l3);
					else if (inClan)
						markMinimap(mapDotClan, j1, l3);
					else if (flag2)
						markMinimap(mapDotTeam, j1, l3);
					else
						markMinimap(mapDotPlayer, j1, l3);
				}
			}
			if (anInt855 != 0 && loopCycle % 20 < 10) {
				if (anInt855 == 1 && anInt1222 >= 0 && anInt1222 < npcArray.length) {
					Npc class30_sub2_sub4_sub1_sub1_1 = npcArray[anInt1222];
					if (class30_sub2_sub4_sub1_sub1_1 != null) {
						int k1 = class30_sub2_sub4_sub1_sub1_1.x / 32 - myPlayer.x / 32;
						int i4 = class30_sub2_sub4_sub1_sub1_1.z / 32 - myPlayer.z / 32;
						method81(mapMarker, i4, k1);
					}
				}
				if (anInt855 == 2) {
					int l1 = ((anInt934 - regionBaseX) * 4 + 2) - myPlayer.x / 32;
					int j4 = ((anInt935 - regionBaseY) * 4 + 2) - myPlayer.z / 32;
					method81(mapMarker, j4, l1);
				}
				if (anInt855 == 10 && anInt933 >= 0 && anInt933 < playerArray.length) {
					Player class30_sub2_sub4_sub1_sub2_1 = playerArray[anInt933];
					if (class30_sub2_sub4_sub1_sub2_1 != null) {
						int i2 = class30_sub2_sub4_sub1_sub2_1.x / 32 - myPlayer.x / 32;
						int k4 = class30_sub2_sub4_sub1_sub2_1.z / 32 - myPlayer.z / 32;
						method81(mapMarker, k4, i2);
					}
				}
			}
			if (destX != 0) {
				int j2 = (destX * 4 + 2) - myPlayer.x / 32;
				int l4 = (destY * 4 + 2) - myPlayer.z / 32;
				markMinimap(mapFlag, j2, l4);
			}
			Raster.fillRect((isFixed ? 101 + xPosOffset : clientWidth - 90), (isFixed ? 86 : 82), 3, 3, 0xffffff, false);
		}
		if (isFixed) {
			mapBack562.drawSprite(0 + xPosOffset, 0);
			if (inDungeon()) {
				SpriteCache.get(dungMapHover ? 790 : 789).drawSprite(8 + xPosOffset, 124);
			}
		} else {
			SpriteCache.get(954).drawSprite(clientWidth - 164, 0);
			SpriteCache.get(955).drawSprite(clientWidth - 169, 0);
			if (inDungeon()) {
				int x = clientWidth - 115;
				int y = 160;
				SpriteCache.get(955).drawSprite(x, y);
				SpriteCache.get(dungMapHover ? 790 : 789).drawSprite(x + 4, y + 5);
			}
		}
		SpriteCache.get(956).drawSprite((isFixed ? xPosOffset + 246 : clientWidth) - 21, 0);
		if (tabHPos != -1) {
			if (tabHPos == 10) {
				SpriteCache.get(957).drawSprite((isFixed ? xPosOffset + 246 : clientWidth) - 21, 0);
			}
		}
		if (tabInterfaceIDs[tabID] != -1) {
			if (tabID == 10) {
				SpriteCache.get(958).drawSprite((isFixed ? xPosOffset + 246 : clientWidth) - 21, 0);
			}
		}
		loadOrbs();
		// SpriteLoader.sprites[60].drawSprite((isFixed ? 246 : clientWidth) -
		// 42, 0);
		if (hdEnabled) {
			compass.method352hd(33, viewRotation, anIntArray1057, 256, anIntArray968, 25, 33, 25, isFixed ? xPosOffset + 8 : Client.clientWidth - 164, (isFixed ? 8 : 5));
			Client.refreshGLMinimap = false;
		} else {
			compass.method352(33, viewRotation, anIntArray1057, 256, anIntArray968, 25, (isFixed ? 8 : 5),
					(isFixed ? 8 + xPosOffset : clientWidth - 164), 33, 25);
			if (MenuManager.menuOpen && menuScreenArea == 3) {
				drawMenu();
			}
		}
		
		if (gameScreenIP != null) {
			gameScreenIP.initDrawingArea();
		}
	}
	
	public int flagPos = 0;
	
	public boolean xpClicked = false;
	
	public boolean drawXpBar = false;
	
	public boolean drawFlag = true;
	
	public long xpToDraw = 0;
	
	public long xpToDrawOnBar = 0;
	
	public Sprite[] ORBS = new Sprite[23];
	
	public Sprite[] bankTabs = new Sprite[5];
	
	public Sprite xpOrb;
	
	public Sprite moneyOrb;
	
	public Sprite moneyOrbHover;
	
	public Sprite sprite1;
	
	public Sprite xpFlag;
	
	public Sprite moneyPouchGlow;
	
	public int moneyPouchGlowCount1 = 0;
	
	public boolean moneyPouchClicked = false;
	
	public String moneyPouchAmount = "<col=ffffff>0";
	
	public void loadExtraSprites() {
		magicAuto = SpriteCache.get(140);
		sprite1 = SpriteCache.get(141);
		xpFlag = SpriteCache.get(142);
		blockHit = SpriteCache.get(143);
		HPBarFull = SpriteCache.get(144);
		HPBarEmpty = SpriteCache.get(145);
		moneyOrb = SpriteCache.get(405);
		moneyOrbHover = SpriteCache.get(403);
		moneyPouchGlow = SpriteCache.get(436);
		moneyPouchGlow.setAlphaTransparency(0);
		
		for (int i = 0; i < 5; i++) {
			bankTabs[i] = SpriteCache.get(i + 146);
		}
		
		ORBS[0] = SpriteCache.get(152); // run
		ORBS[1] = SpriteCache.get(151); // Resizeable run
		ORBS[2] = SpriteCache.get(153); // Resizeable hp
		ORBS[3] = SpriteCache.get(154);
		ORBS[4] = SpriteCache.get(155);
		ORBS[5] = SpriteCache.get(157);
		ORBS[6] = SpriteCache.get(161);
		ORBS[7] = SpriteCache.get(159);
		ORBS[8] = SpriteCache.get(162);
		ORBS[9] = SpriteCache.get(160);
		ORBS[10] = SpriteCache.get(156);
		ORBS[11] = SpriteCache.get(950); // empty orb resizeable
		ORBS[12] = SpriteCache.get(951); // empty orb hovered fixed screen
		ORBS[13] = SpriteCache.get(952); // empty orb hovered resizeable
		ORBS[14] = SpriteCache.get(166); // poison orb fill
		ORBS[15] = SpriteCache.get(169);
		ORBS[16] = SpriteCache.get(167); // xp obb hover
		ORBS[17] = SpriteCache.get(168); // xp orb
		ORBS[18] = SpriteCache.get(169); // summoning icon
		ORBS[19] = SpriteCache.get(170); // summoning orb fixed size
		ORBS[20] = SpriteCache.get(171); // summ orb fill darker
		ORBS[21] = SpriteCache.get(172); // summ orb fill lighter
		ORBS[22] = SpriteCache.get(1058); // venom orb fill

		mapBack562 = SpriteCache.get(192);
		chatArea = SpriteCache.get(190);
		tabArea = SpriteCache.get(191);
		itemSearchItemBg = SpriteCache.get(971);
		tabHover = SpriteCache.get(213);
		tabClicked = SpriteCache.get(214);
		closeIconHover = SpriteCache.get(291);
		closeIcon = SpriteCache.get(290);
		magnifyingGlass = SpriteCache.get(970);
		selectedTab = SpriteCache.get(962);
		tab = SpriteCache.get(959);
		tabBackground = SpriteCache.get(960);
		tabBorder = SpriteCache.get(961);
		dungMapSprite = SpriteCache.get(789);
		moneyPouchHover = SpriteCache.get(400);
		moneyPouch = SpriteCache.get(401);
		dungPartySideIcon = SpriteCache.get(580);
		seasonalPowerSideIcon = SpriteCache.get(1402);

		int[] sideIcons = { 197, 198, 199, 200, 201, 202, 203, 204, 212, 205, 1264/*206*/, 210, 207, 208, 1287/*209*/, 211 };
		sideIconSprites = new Sprite[sideIcons.length];
		for (int i = 0; i < sideIcons.length; i++) {
			sideIconSprites[i] = SpriteCache.get(sideIcons[i]);
		}

		for (int i = 0; i < 9; i++) {
			hitMark[i] = SpriteCache.get(i + 230);
		}

		for (int i = 0; i < 5; i++) {
			hitIcon[i] = SpriteCache.get(i + 239);
		}
	}
	
	public boolean isRunning;
	
	private void rightClickChatButtons() {
		if (mouseHoverEnum == MouseHover.CLOSE_ITEM_SEARCH) {
			MenuManager.addOption("Close", 1252);
		}

		if (super.mouseX >= CHAT_BUTTONS_POS_X[0] && super.mouseX <= CHAT_BUTTONS_POS_X[0] + 56
				&& super.mouseY >= clientHeight - 23 && super.mouseY <= clientHeight) {
			MenuManager.addOption("View all", 999);
		} else if (super.mouseX >= CHAT_BUTTONS_POS_X[1] && super.mouseX <= CHAT_BUTTONS_POS_X[1] + 56
				&& super.mouseY >= clientHeight - 23 && super.mouseY <= clientHeight) {
			MenuManager.addOption("<col=ffff00>Game:</col> Filtered", 998, 2);
			MenuManager.addOption("<col=ffff00>Game:</col> All", 998, 1);
			MenuManager.addOption("View Game", 998, 0);
		} else if (super.mouseX >= CHAT_BUTTONS_POS_X[2] && super.mouseX <= CHAT_BUTTONS_POS_X[2] + 56
				&& super.mouseY >= clientHeight - 23 && super.mouseY <= clientHeight) {
			MenuManager.addOption("<col=ffff00>Public:</col> Hide", 997);
			MenuManager.addOption("<col=ffff00>Public:</col> Off", 996);
			MenuManager.addOption("<col=ffff00>Public:</col> Friends", 995);
			MenuManager.addOption("<col=ffff00>Public:</col> Standard", 994);
			MenuManager.addOption("<col=ffff>Public:</col> Autochat", 808);
			MenuManager.addOption("View Public", 993);
		} else if (super.mouseX >= CHAT_BUTTONS_POS_X[3] && super.mouseX <= CHAT_BUTTONS_POS_X[3] + 56
				&& super.mouseY >= clientHeight - 23 && super.mouseY <= clientHeight) {
			MenuManager.addOption("<col=ffff00>Private:</col> Clear history", 988);
			MenuManager.addOption("<col=ffff00>Private:</col> Off", 992);
			MenuManager.addOption("<col=ffff00>Private:</col> Friends", 991);
			MenuManager.addOption("<col=ffff00>Private:</col> On", 990);
			MenuManager.addOption("View Private", 989);
		} else if (super.mouseX >= CHAT_BUTTONS_POS_X[4] && super.mouseX <= CHAT_BUTTONS_POS_X[4] + 56
				&& super.mouseY >= clientHeight - 23 && super.mouseY <= clientHeight) {
			MenuManager.addOption("<col=ffff00>Clan Chat:</col> Off", 1003);
			MenuManager.addOption("<col=ffff00>Clan Chat:</col> Friends", 1002);
			MenuManager.addOption("<col=ffff00>Clan Chat:</col> On", 1001);
			MenuManager.addOption("View Clan Chat", 1000);
		} else if (super.mouseX >= CHAT_BUTTONS_POS_X[5] && super.mouseX <= CHAT_BUTTONS_POS_X[5] + 56
				&& super.mouseY >= clientHeight - 23 && super.mouseY <= clientHeight) {
			MenuManager.addOption("<col=ffff00>Trade:</col> Off", 987);
			MenuManager.addOption("<col=ffff00>Trade:</col> Friends", 986);
			MenuManager.addOption("<col=ffff00>Trade:</col> On", 985);
			MenuManager.addOption("View Trade", 984);
		} else if (super.mouseX >= CHAT_BUTTONS_POS_X[6] && super.mouseX <= CHAT_BUTTONS_POS_X[6] + 56
				&& super.mouseY >= clientHeight - 23 && super.mouseY <= clientHeight) {
			MenuManager.addOption("<col=ffff00>Yell:</col> Off", 1011);
			MenuManager.addOption("<col=ffff00>Yell:</col> Friends", 1010);
			MenuManager.addOption("<col=ffff00>Yell:</col> On", 1009);
			MenuManager.addOption("View Yell", 1008);
		}
	}

	private void processMinimapActions() {
		if (worldHover && !inDungeon()) {
			MenuManager.addOption("Open teleport menu", 4004);
			return;
		}

		switch (hoverPos) {
			case 4:
				MenuManager.addOption("Price checker", 4003);
				MenuManager.addOption("Examine money pouch", 4002);
				MenuManager.addOption("Withdraw money pouch", 4001);
				MenuManager.addOption("Toggle money pouch", 4000);
				break;
			case 5:
				MenuManager.addOption("Face North", 696);
				break;
			case 0:
				MenuManager.addOption("Reset XP Total", 1504);
				MenuManager.addOption("Toggle XP Total", 1503);
				break;
			case 2:
				MenuManager.addOption("Rest", 1501);
				MenuManager.addOption(isRunning ? "Toggle Run-Mode Off" : "Toggle Run-Mode On", 1050);
				break;
			case 1:
				MenuManager.addOption("Set Quick-Prayers", 1505);
				MenuManager.addOption(variousSettings[ConfigCodes.QUICK_PRAYERS_ON] == 1 ? "Toggle Quick-Prayers Off" : "Toggle Quick-Prayers On", 1500);
				break;
			case 3:
				if (leftClick != -1 && leftClick < 8) {
					MenuManager.addOption(orbTextEnabled[3] ? "Hide text" : "Show text", 1511);
					MenuManager.addOption("Select left-click option", 1027);
					MenuManager.addOption(leftClickNames[leftClick].equals("Cast")
							? leftClickNames[leftClick] + " <col=ff00>" + getFamiliar().getSpecialAttack()
							: leftClickNames[leftClick], leftClickActions[leftClick]);
				} else if (choosingLeftClick) {
					MenuManager.addOption(orbTextEnabled[3] ? "Hide text" : "Show text", 1511);
					MenuManager.addOption("Select left-click option", 1027);
					MenuManager.addOption("Call Follower", 1018);
					MenuManager.addOption("Dismiss", 1019);
					MenuManager.addOption("Take BoB", 1020);
					MenuManager.addOption("Renew Familiar", 1021);
					MenuManager.addOption("Interact", 1022);
					MenuManager.addOption("Attack", 1023);
					if (getFamiliar().isActive() && getFamiliar().getSpecialAttack().length() > 0) {
						MenuManager.addOption("Cast <col=ff00>" + getFamiliar().getSpecialAttack(), 1026);
						MenuManager.addOption("Follower Details", 1024);
					} else {
						MenuManager.addOption("Follower Details", 1024);
					}
				} else {
					MenuManager.addOption(orbTextEnabled[3] ? "Hide text" : "Show text", 1511);
					MenuManager.addOption("Select left-click option", 1027);
				}
				break;
		}
	}
	
	public boolean orbsEnabled = true;
	
	public void loadOrbs() {
		if (!orbsEnabled) {
			return;
		}
		
		drawHP();
		drawPrayer();
		drawRunOrb();
		drawXpOrb();
		drawMoneyOrb();
		drawSummonOrb();
		drawWorldHover();
	}

	private void drawWorldHover() {
		if (inDungeon()) {
			return;
		}

		worldHover = isFixed() ? mouseInRegion(522, 124, 556, 159) : mouseInRegion(clientWidth - 113, 160,clientWidth - 80, 195);

		final int xOffset = isFixed() ? clientWidth - 242 : clientWidth - 112;
		final int yOffset = isFixed() ? 123 : 160;

		Sprite sprite = SpriteCache.get(worldHover ? 178 : 177);

		if (isFixed()) {
			if (sprite != null) {
				sprite.drawSprite(xOffset, yOffset);
			}
		} else {
			Sprite frameSprite = SpriteCache.get(791);

			if (sprite != null) {
				sprite.drawSprite(xOffset, yOffset);
			}

			if (frameSprite != null) {
				frameSprite.drawSprite(xOffset - 4, yOffset - 5);
			}
		}

	}
	
	public void drawSummonOrb() {
		int currentSum = currentStats[23];
		int maxSum = maxStats[23];
		if (currentSum < 0) {
			currentSum = 0;
		}
		if (maxSum <= 0) {
			maxSum = 1;
		}
		int summon = currentSum * 100 / maxSum;
		
		int x = getOrbX(3);
		if (isFixed() && GraphicsDisplay.enabled) {
			x += 516;
		}
		int y = getOrbY(3);
		ORBS[isFixed() ? (hoverPos == 3 ? 12 : 0) : (hoverPos == 3 ? 13 : 11)].drawSprite(x, y);
		
		int rgb = 0;
		if (summon <= 100 && summon >= 75) {
			rgb = 65280;
		} else if (summon <= 74 && summon >= 50) {
			rgb = 0xffff00;
		} else if (summon <= 49 && summon >= 25) {
			rgb = 0xfca607;
		} else if (summon <= 24 && summon >= 0) {
			rgb = 0xf50d0d;
		}
		
		newSmallFont.drawCenteredString(Integer.toString(currentStats[23]), x + (isFixed() ? 42 : 15), y + 26, rgb, 0);
		
		drawScissorOrb(currentSum, maxSum, animateSummonTab ? 21 : 20, x + (isFixed() ? 3 : 27), y + 3);
		
		ORBS[15].drawSprite(x + (isFixed() ? 9 : 33), y + 9);
	}
	
	public int logIconHPos = 0;
	
	public void drawLogoutButton() {
		if (logIconHPos == 1) {
			ORBS[18].drawSprite(227, 0);
		} else {
			ORBS[17].drawSprite(227, 0);
		}
	}
	
	public void processMapAreaClick() {
		if (super.mouseX >= 742 && super.mouseX <= 764 && super.mouseY >= 1 && super.mouseY <= 23) {
			logIconHPos = 1;
		} else {
			logIconHPos = 0;
		}
	}
	
	public int getOrbX(int orb) {
		switch (orb) {
			case 0:
				return !isFixed() ? clientWidth - 212 : 172;
			case 1:
				return !isFixed() ? clientWidth - 215 : 188;
			case 2:
				return !isFixed() ? clientWidth - 203 : 188;
			case 3:
				return !isFixed() ? clientWidth - 180 : 172;
			case 4:
				return !isFixed() ? clientWidth - 180 : 172;
		}
		return 0;
	}
	
	public int getOrbY(int orb) {
		switch (orb) {
			case 0:
				return !isFixed() ? 39 : 15;
			case 1:
				return !isFixed() ? 73 : 54;
			case 2:
				return !isFixed() ? 107 : 93;
			case 3:
				return !isFixed() ? 141 : 128;
			case 4:
				return !isFixed() ? 141 : 86;
		}
		return 0;
	}

	private static int hpOrbState;

	public void drawHP() {
		boolean isFixed = isFixed();
		int currentHP = currentHp * hpModifier;
		int maxHP = maxHp * hpModifier;
		
		if (maxHP <= 0) {
			maxHP = 1;
		}
		if (currentHP < 0) {
			currentHP = 0;
		}
		
		int health = currentHP * 100 / maxHP;
		int x = getOrbX(0);
		int y = getOrbY(0);
		if (isFixed && GraphicsDisplay.enabled) {
			x += 516;
		}
		
		ORBS[isFixed ? 0 : 11].drawSprite(x, y);
		
		int rgb = 0;
		if (health >= 75) {
			rgb = 65280;
		} else if (health <= 74 && health >= 50) {
			rgb = 0xffff00;
		} else if (health <= 49 && health >= 25) {
			rgb = 0xfca607;
		} else if (health <= 24 && health >= 0) {
			rgb = 0xf50d0d;
		}
		
		newSmallFont.drawCenteredString(Integer.toString(currentHP), x + (isFixed ? 42 : 15), y + 26, rgb, 0);

		drawScissorOrb(currentHP, maxHP, hpOrbState == 1 ? 14 : (hpOrbState == 2 ? 22 : 2), x + (isFixed ? 3 : 27), y + 3);

		if (health <= 25) {
			if (loopCycle % 20 < 10) {
				ORBS[3].drawSprite(x + (isFixed ? 9 : 33), y + 11);
			}
		} else {
			ORBS[3].drawSprite(x + (isFixed ? 9 : 33), y + 11);
		}
	}
	
	private void drawScissorOrb(int currentStat, int maxStat, int spriteId, int x, int y) {
		int k2 = Raster.topX;
		int l2 = Raster.bottomX;
		int i3 = Raster.topY;
		int j3 = Raster.bottomY;
		int offset = ORBS[spriteId].myWidth * (maxStat - currentStat) / maxStat;
		Raster.setDrawingArea(x, y + offset, x + ORBS[spriteId].myWidth, y + ORBS[spriteId].myHeight);
		ORBS[spriteId].drawSprite(x, y);
		Raster.setDrawingArea(k2, i3, l2, j3);
	}
	
	public void drawPrayer() {
		int currentPray = currentStats[5];
		int maxPray = maxStats[5];
		
		if (maxPray <= 0) {
			maxPray = 1;
		}
		if (currentPray < 0) {
			currentPray = 0;
		}
		
		int prayer = currentPray * 100 / maxPray;
		int x = getOrbX(1);
		int y = getOrbY(1);
		if (isFixed() && GraphicsDisplay.enabled) {
			x += 516;
		}
		
		ORBS[isFixed() ? (hoverPos == 1 ? 12 : 0) : (hoverPos == 1 ? 13 : 11)].drawSprite(x, y);
		
		int rgb = 0;
		if (prayer <= 100 && prayer >= 75) {
			rgb = 65280;
		} else if (prayer <= 74 && prayer >= 50) {
			rgb = 0xffff00;
		} else if (prayer <= 49 && prayer >= 25) {
			rgb = 0xfca607;
		} else if (prayer <= 24 && prayer >= 0) {
			rgb = 0xf50d0d;
		}
		
		newSmallFont.drawCenteredString(Integer.toString(currentPray), x + (isFixed() ? 42 : 15), y + 26, rgb, 0);
		
		drawScissorOrb(currentPray, maxPray, variousSettings[ConfigCodes.QUICK_PRAYERS_ON] == 1 ? 10 : 4, x + (isFixed() ? 3 : 27), y + 3);
		
		ORBS[5].drawSprite(x + (isFixed() ? 7 : 31), y + 7);
	}
	
	public void drawRunOrb() {
		int currentRun = energy;
		int maxRun = 100;
		int x = getOrbX(2);
		int y = getOrbY(2);
		if (isFixed() && GraphicsDisplay.enabled) {
			x += 516;
		}
		
		ORBS[isFixed() ? (hoverPos == 2 ? 12 : 0) : (hoverPos == 2 ? 13 : 11)].drawSprite(x, y);
		
		int rgb = 0;
		if (currentRun <= 100 && currentRun >= 75) {
			rgb = 65280;
		} else if (currentRun <= 74 && currentRun >= 50) {
			rgb = 0xffff00;
		} else if (currentRun <= 49 && currentRun >= 25) {
			rgb = 0xfca607;
		} else if (currentRun <= 24 && currentRun >= 0) {
			rgb = 0xf50d0d;
		}
		
		newSmallFont.drawCenteredString(Integer.toString(currentRun), x + (isFixed() ? 42 : 15), y + 26, rgb, 0);
		
		drawScissorOrb(currentRun, maxRun, isRunning ? 8 : 6, x + (isFixed() ? 3 : 27), y + 3);
		
		ORBS[!isRunning ? 7 : 9].drawSprite(x + (isFixed() ? 10 : 34), y + 7);
	}
	
	public void drawXpOrb() {
		int x = -1;
		if (isFixed() && GraphicsDisplay.enabled) {
			x += 516;
		}
		
		ORBS[hoverPos == 0 ? 16 : 17].drawSprite(isFixed() ? x : clientWidth - 210, isFixed() ? 48 : 4);
	}
	
	public void drawMoneyOrb() {
		int xPos = isFixed() ? -1 : clientWidth - 58;
		int yPos = isFixed() ? 83 : 154;
		if (isFixed() && GraphicsDisplay.enabled) {
			xPos += 516;
		}
		
		if (hoverPos == 4) {
			moneyOrbHover.drawSprite(xPos, yPos);
		} else {
			moneyOrb.drawSprite(xPos, yPos);
		}
		
		if (moneyPouchGlowCount1 > 0) {
			moneyPouchGlow.drawTransparentSpriteTrans(xPos - 14, yPos - 15, moneyPouchGlowCount1 & 255);
			moneyPouchGlowCount1 -= 10;
		}
		
		// moneyOrbHover.drawOr(isFixed() ? -1 : clientWidth - 50, isFixed() ?
		// 83 : 160, moneyOrb);
		
		/*
		 * moneyOrb.drawSprite(); if (hoverPos == 4) {
		 * moneyOrbHover.drawSprite(isFixed() ? -1 : clientWidth - 50, isFixed()
		 * ? 83 : 160); }
		 */
		
	}

	public void npcScreenPos(Entity entity, int i) {
		calcEntityScreenPos(entity.x, i, entity.z);
	}
	
	public void calcEntityScreenPos(int i, int j, int l) {
		if (i < 128 || l < 128 || i > 13056 || l > 13056) {
			spriteDrawX = -1;
			spriteDrawY = -1;
			return;
		}
		int i1 = method42(plane, l, i) - j;
		i -= xCameraPos;
		i1 -= zCameraPos;
		l -= yCameraPos;
		int j1 = Rasterizer.SINE[yCameraCurve];
		int k1 = Rasterizer.COSINE[yCameraCurve];
		int l1 = Rasterizer.SINE[xCameraCurve];
		int i2 = Rasterizer.COSINE[xCameraCurve];
		int j2 = l * l1 + i * i2 >> 16;
		l = l * i2 - i * l1 >> 16;
		i = j2;
		j2 = i1 * k1 - l * j1 >> 16;
		l = i1 * j1 + l * k1 >> 16;
		i1 = j2;
		if (l >= 50) {
			int width = fov;
			int offsetX = 0;
			int offsetY = 0;
			if (GraphicsDisplay.enabled) {
				width = VBORenderer.fov;
				if (isFixed()) {
					offsetX = offsetY = 4;
				}
			}

			int x = Rasterizer.center_x + i * width / l;
			int y = Rasterizer.center_y + i1 * width / l;
			spriteDrawX = x + offsetX;
			spriteDrawY = y + offsetY;
		} else {
			spriteDrawX = -1;
			spriteDrawY = -1;
		}
	}
	
	private void buildSplitPrivateChatMenu() {
		if (splitPrivateChat == 0) {
			return;
		}

		int messageCount = 0;
		if (anInt1104 != 0) {
			messageCount++;
		}

		if (announcement != null) {
			messageCount++;
		}

		for (int index = ChatMessageManager.getHighestId(); index != -1; index = ChatMessageManager
				.getPrevMessageId(index)) {
			ChatMessage message = (ChatMessage) ChatMessageManager.messageHashtable.findNodeByID(index);
			int k = message.type;
			String name = message.name;
			if (name != null && name.indexOf("@") == 0) {
				if (name.charAt(4) == '@')
					name = name.substring(5);
				else if (name.charAt(5) == '@')
					name = name.substring(6);
			}
			if ((k == ChatMessage.PRIVATE_MESSAGE_RECEIVED_PLAYER || k == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF) && (k == ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF || privateChatMode == 0 || privateChatMode == 1 && isFriendOrSelf(name))
					&& !isInIgnoreList(name)) {
				int l = (clientHeight - 174) - messageCount * 13;
				if (super.mouseX > (isFixed() ? 4 : 0) && super.mouseY - (isFixed() ? 4 : 0) > l - 10
						&& super.mouseY - (isFixed() ? 4 : 0) <= l + 3) {
					int i1 = newRegularFont.getTextWidth("From:  " + name + message.message) + 25;
					if (i1 > 450)
						i1 = 450;
					if (super.mouseX < (isFixed() ? 4 : 0) + i1) {
						buildMessageOrAddFriend(name, true);
					}
				}
				if (++messageCount >= 5)
					return;
			}
			if ((k == ChatMessage.PRIVATE_MESSAGE_RECEIVED || k == ChatMessage.PRIVATE_MESSAGE_SENT) && privateChatMode < 2 && ++messageCount >= 5) {
				return;
			}
		}
	}
	
	private void buildMessageOrAddFriend(String s, boolean lowPrio) {
		if (myPrivilege >= 1) {
			MenuManager.addOption("Report abuse <col=ffffff>" + s, lowPrio ? 2606 : 606);
		}

        long nameAsLong = TextClass.longForName(s);
        int k3 = -1;
        for (int i4 = 0; i4 < friendsCount; i4++) {
            if (friendsListAsLongs[i4] != nameAsLong)
                continue;
            k3 = i4;
            break;
        }

        if (k3 != -1) {
        	MenuManager.addOption("Remove <col=ffffff>" + s, lowPrio ? 2792 : 792);
    		MenuManager.addOption("Message <col=ffffff>" + s, lowPrio ? 2639 : 639);
        } else {
        	MenuManager.addOption("Add ignore <col=ffffff>" + s, lowPrio ? 2042 : 42);
    		MenuManager.addOption("Add friend <col=ffffff>" + s, lowPrio ? 2337 : 337);
     		MenuManager.addOption("Message <col=ffffff>" + s, lowPrio ? 2639 : 639);
        }
	}

	public void addObject(int x, int y, int objectId, int face, int type, int height) {
		int mX = anInt1069 - 6;
		int mY = anInt1070 - 6;
		int x2 = x - (mX * 8);
		int y2 = y - (mY * 8);
		int i15 = type;
		int l17 = anIntArray1177[i15];
		if (y2 > 0 && y2 < 103 && x2 > 0 && x2 < 103) {
			method130(-1, objectId, face, l17, y2, type, height, x2, 0);
		}
	}
	
	// adding objects addobject
	public void method130(int removeTime, int objectId, int l, int i1, int localY, int k1, int plane, int localX, int spawnTime) {
		GameObject gameObject = null;
		for (GameObject gameObjectIt = (GameObject) aClass19_1179
				.reverseGetFirst(); gameObjectIt != null; gameObjectIt = (GameObject) aClass19_1179.reverseGetNext()) {
			if (gameObjectIt.plane != plane || gameObjectIt.tileX != localX || gameObjectIt.tileY != localY
					|| gameObjectIt.objectType != i1)
				continue;
			gameObject = gameObjectIt;
			break;
		}
		
		if (gameObject == null) {
			gameObject = new GameObject();
			gameObject.plane = plane;
			gameObject.objectType = i1;
			gameObject.tileX = localX;
			gameObject.tileY = localY;
			final int regionId = ((regionBaseX + localX) >> 6 << 8) + ((regionBaseY + localY) >> 6);
			gameObject.regionType = ObjectManager.getObjectDefVer(regionId);
			if (gameObject.regionType == 1337 && objectId < ObjectDef.OSRS_START)
				gameObject.regionType = 474;
			method89(gameObject);
			aClass19_1179.insertHead(gameObject);
		}
		gameObject.currentIDRequested = objectId;
		gameObject.currentTypeRequested = k1;
		gameObject.currentFaceRequested = l;
		gameObject.spawnTime = spawnTime;
		gameObject.removeTime = removeTime;
	}
	
	private boolean interfaceIsSelected(RSInterface class9) {
		if (class9.valueCompareType == null)
			return false;
		for (int i = 0; i < class9.valueCompareType.length; i++) {
			int currentValue = extractInterfaceValues(class9, i);
			int requiredValue = class9.requiredValues[i];
			if (class9.valueCompareType[i] == 2) { // must not have more
				if (currentValue >= requiredValue)
					return false;
			} else if (class9.valueCompareType[i] == 3) { // must not have less
				if (currentValue <= requiredValue)
					return false;
			} else if (class9.valueCompareType[i] == 4) { // must not have same
				// amount
				if (currentValue == requiredValue)
					return false;
			} else if (currentValue != requiredValue)
				return false;
		}
		
		return true;
	}
	
	private DataInputStream openJagGrabInputStream(String s) throws IOException {
		// if(!aBoolean872)
		// if(signlink.mainapp != null)
		// return signlink.openurl(s);
		// else
		// return new DataInputStream((new URL(getCodeBase(), s)).openStream());
		if (aSocket832 != null) {
			try {
				aSocket832.close();
			} catch (Exception _ex) {
				_ex.printStackTrace();
			}
			aSocket832 = null;
		}
		aSocket832 = openSocket(Config.UPDATE_SERVER_IP, Config.JAGGRAB_PORT);
		aSocket832.setSoTimeout(10000);
		java.io.InputStream inputstream = aSocket832.getInputStream();
		OutputStream outputstream = aSocket832.getOutputStream();
		outputstream.write(("JAGGRAB /" + s + "\n\n").getBytes());
		return new DataInputStream(inputstream);
	}
	
	public void method134(RSBuffer stream) { // TODO: make attacker show first
		int j = stream.readBits(8);
		if (j < playerCount) {
			for (int k = j; k < playerCount; k++)
				anIntArray840[anInt839++] = playerIndices[k];
			
		}
		if (j > playerCount) {
			SignLink.reporterror(myUsername + " Too many players");
			throw new RuntimeException("eek");
		}
		playerCount = 0;
		for (int l = 0; l < j; l++) {
			int i1 = playerIndices[l];
			Player player = playerArray[i1];
			int j1 = stream.readBits(1);
			if (j1 == 0) {
				playerIndices[playerCount++] = i1;
				player.anInt1537 = loopCycle;
			} else {
				int k1 = stream.readBits(2);
				if (k1 == 0) {
					playerIndices[playerCount++] = i1;
					player.anInt1537 = loopCycle;
					anIntArray894[anInt893++] = i1;
				} else if (k1 == 1) {
					playerIndices[playerCount++] = i1;
					player.anInt1537 = loopCycle;
					int l1 = stream.readBits(3);
					player.moveInDir(false, l1);
					int j2 = stream.readBits(1);
					if (j2 == 1)
						anIntArray894[anInt893++] = i1;
				} else if (k1 == 2) {
					playerIndices[playerCount++] = i1;
					player.anInt1537 = loopCycle;
					player.runTiles = stream.readBits(4);
					for (int i = 0; i < player.runTiles; i++) {
						int k2 = stream.readBits(3);
						player.moveInDir(true, k2);
					}
					int l2 = stream.readBits(1);
					if (l2 == 1)
						anIntArray894[anInt893++] = i1;
				} else if (k1 == 3)
					anIntArray840[anInt839++] = i1;
			}
		}
	}
	
	@Override
	public void raiseWelcomeScreen() {
		fullRedraw = true;
		if (CacheDownloader.downloadingCache) {
			drawLoadingBar(CacheDownloader.downloadPercent, "Downloading Cache");
		}
	}
	
	public void method137(RSBuffer stream, int j) {
		if (j == 84) {
			int k = stream.readUnsignedByte();
			int j3 = anInt1268 + (k >> 4 & 7);
			int i6 = anInt1269 + (k & 7);
			int l8 = stream.read3Bytes();
			int k11 = stream.readUnsignedWord();
			int l13 = stream.readUnsignedWord();
			if (j3 >= 0 && i6 >= 0 && j3 < 104 && i6 < 104) {
				NodeList class19_1 = groundArray[plane][j3][i6];
				if (class19_1 != null) {
					for (Item class30_sub2_sub4_sub2_3 = (Item) class19_1
							.reverseGetFirst(); class30_sub2_sub4_sub2_3 != null; class30_sub2_sub4_sub2_3 = (Item) class19_1
							.reverseGetNext()) {
						if (class30_sub2_sub4_sub2_3.ID != l8 || class30_sub2_sub4_sub2_3.anInt1559 != k11)
							continue;
						class30_sub2_sub4_sub2_3.anInt1559 = l13;
						break;
					}
					
					spawnGroundItem(j3, i6);
				}
			}
			return;
		}
		if (j == 105) {
			int l = stream.readUnsignedByte();
			int k3 = anInt1268 + (l >> 4 & 7);
			int j6 = anInt1269 + (l & 7);
			int i9 = stream.readUnsignedWord();
			int l11 = stream.readUnsignedByte();
			int i14 = l11 >> 4 & 0xf;
			int i16 = l11 & 7;
			if (myPlayer.smallX[0] >= k3 - i14 && myPlayer.smallX[0] <= k3 + i14 && myPlayer.smallY[0] >= j6 - i14
					&& myPlayer.smallY[0] <= j6 + i14 && soundEffectVolume != 0 && soundCount < 50) {
				soundIds[soundCount] = i9;
				soundTypes[soundCount] = i16;
				soundDelays[soundCount] = 0;
				aClass26Array1468[soundCount] = null;
				soundCount++;
			}
		}
		if (j == 215) {
			int i1 = stream.read3Bytes();
			int l3 = stream.method428();
			int k6 = anInt1268 + (l3 >> 4 & 7);
			int j9 = anInt1269 + (l3 & 7);
			int i12 = stream.method435();
			int j14 = stream.readUnsignedWord();
			if (k6 >= 0 && j9 >= 0 && k6 < 104 && j9 < 104 && i12 != myPlayerIndexOnWorldLoggedIn) {
				Item class30_sub2_sub4_sub2_2 = new Item();
				class30_sub2_sub4_sub2_2.ID = i1;
				class30_sub2_sub4_sub2_2.anInt1559 = j14;
				if (groundArray[plane][k6][j9] == null)
					groundArray[plane][k6][j9] = new NodeList();
				groundArray[plane][k6][j9].insertHead(class30_sub2_sub4_sub2_2);
				spawnGroundItem(k6, j9);
			}
			return;
		}
		if (j == 156) {
			int j1 = stream.method426();
			int i4 = anInt1268 + (j1 >> 4 & 7);
			int l6 = anInt1269 + (j1 & 7);
			int k9 = stream.read3Bytes();
			if (i4 >= 0 && l6 >= 0 && i4 < 104 && l6 < 104) {
				NodeList class19 = groundArray[plane][i4][l6];
				if (class19 != null) {
					for (Item item = (Item) class19.reverseGetFirst(); item != null; item = (Item) class19
							.reverseGetNext()) {
						if (item.ID != k9)
							continue;
						item.unlink();
						break;
					}
					
					if (class19.reverseGetFirst() == null)
						groundArray[plane][i4][l6] = null;
					spawnGroundItem(i4, l6);
				}
			}
			return;
		}
		if (j == 160) {
			int k1 = stream.method428();
			int tileX = anInt1268 + (k1 >> 4 & 7);
			int tileY = anInt1269 + (k1 & 7);
			int l9 = stream.method428();
			int j12 = l9 >> 2;
			int k14 = l9 & 3;
			int j16 = anIntArray1177[j12];
			int j17 = stream.method435();
			if (tileX >= 0 && tileY >= 0 && tileX < 103 && tileY < 103) {
				int j18 = intGroundArray[plane][tileX][tileY];
				int i19 = intGroundArray[plane][tileX + 1][tileY];
				int l19 = intGroundArray[plane][tileX + 1][tileY + 1];
				int k20 = intGroundArray[plane][tileX][tileY + 1];
				
				final int regionId = ((regionBaseX + tileX) >> 6 << 8) + ((regionBaseY + tileY) >> 6);
				int regionType = ObjectManager.getObjectDefVer(regionId);

				switch (j16) {
					case 0:
						Object1 class10 = worldController.method296(plane, tileX, tileY);
						if (class10 != null) {
							int k21 = (int) (class10.uid >>> 32) & 0x7fffffff;
							if (j12 == 2) {
								class10.aClass30_Sub2_Sub4_278 = new Animable_Sub5(k21, 4 + k14, 2, i19, l19, j18, k20,
										j17, false, regionType);
								class10.aClass30_Sub2_Sub4_279 = new Animable_Sub5(k21, k14 + 1 & 3, 2, i19, l19, j18,
										k20, j17, false, regionType);
							} else {
								class10.aClass30_Sub2_Sub4_278 = new Animable_Sub5(k21, k14, j12, i19, l19, j18, k20,
										j17, false, regionType);
							}
						}
						break;
					case 1:
						Object2 class26 = worldController.method297(tileX, tileY, plane);
						if (class26 != null)
							class26.aClass30_Sub2_Sub4_504 = new Animable_Sub5((int) (class26.uid >>> 32) & 0x7fffffff,
									0, 4, i19, l19, j18, k20, j17, false, regionType);
						break;
					case 2:
						Object5 class28 = worldController.method298(tileX, tileY, plane);
						if (j12 == 11)
							j12 = 10;
						if (class28 != null) {
							class28.aClass30_Sub2_Sub4_521 = new Animable_Sub5((int) (class28.uid >>> 32) & 0x7fffffff,
									k14, j12, i19, l19, j18, k20, j17, false, regionType);
						}
						break;
					case 3:
						Object3 class49 = worldController.method299(tileY, tileX, plane);
						if (class49 != null)
							class49.aClass30_Sub2_Sub4_814 = new Animable_Sub5((int) (class49.uid >>> 32) & 0x7fffffff,
									k14, 22, i19, l19, j18, k20, j17, false, regionType);
						break;
				}
			}
			return;
		}
		if (j == 147) {
			int l1 = stream.method428();
			int k4 = anInt1268 + (l1 >> 4 & 7);
			int j7 = anInt1269 + (l1 & 7);
			int i10 = stream.readUnsignedWord();
			byte byte0 = stream.method430();
			int l14 = stream.method434();
			byte byte1 = stream.method429();
			int k17 = stream.readUnsignedWord();
			int k18 = stream.method428();
			int j19 = k18 >> 2;
			int i20 = k18 & 3;
			int l20 = anIntArray1177[j19];
			byte byte2 = stream.readSignedByte();
			int l21 = stream.readUnsignedWord();
			byte byte3 = stream.method429();
			Player player;
			if (i10 == myPlayerIndexOnWorldLoggedIn)
				player = myPlayer;
			else
				player = playerArray[i10];
			if (player != null) {
				final int regionId = ((regionBaseX + k4) >> 6 << 8) + ((regionBaseY + j7) >> 6);
				final int version = ObjectManager.getObjectDefVer(regionId);
				ObjectDef class46 = ObjectManager.getObjectDef(l21, version);
				int i22 = intGroundArray[plane][k4][j7];
				int j22 = intGroundArray[plane][k4 + 1][j7];
				int k22 = intGroundArray[plane][k4 + 1][j7 + 1];
				int l22 = intGroundArray[plane][k4][j7 + 1];
				Model model = class46.method578(j19, i20, i22, j22, k22, l22);
				if (model != null) {
					method130(k17 + 1, -1, 0, l20, j7, 0, plane, k4, l14 + 1);
					player.anInt1707 = l14 + loopCycle;
					player.anInt1708 = k17 + loopCycle;
					player.aModel_1714 = model;
					int i23 = class46.anInt744;
					int j23 = class46.anInt761;
					if (i20 == 1 || i20 == 3) {
						i23 = class46.anInt761;
						j23 = class46.anInt744;
					}
					player.anInt1711 = k4 * 128 + i23 * 64;
					player.anInt1713 = j7 * 128 + j23 * 64;
					player.anInt1712 = method42(plane, player.anInt1713, player.anInt1711);
					if (byte2 > byte0) {
						byte byte4 = byte2;
						byte2 = byte0;
						byte0 = byte4;
					}
					if (byte3 > byte1) {
						byte byte5 = byte3;
						byte3 = byte1;
						byte1 = byte5;
					}
					player.anInt1719 = k4 + byte2;
					player.anInt1721 = k4 + byte0;
					player.anInt1720 = j7 + byte3;
					player.anInt1722 = j7 + byte1;
				}
			}
		}
		if (j == 151) {
			int i2 = stream.method426();
			int l4 = anInt1268 + (i2 >> 4 & 7);
			int k7 = anInt1269 + (i2 & 7);
			int objectId = stream.read3Bytes();
			int k12 = stream.method428();
			int i15 = k12 >> 2;
			int k16 = k12 & 3;
			int l17 = anIntArray1177[i15];
			if (l4 >= 0 && k7 >= 0 && l4 < 104 && k7 < 104)
				method130(-1, objectId, k16, l17, k7, i15, plane, l4, 0);
			return;
		}
		
		if (j == 152) {
			int tileData = stream.method426();
			int tileX = anInt1268 + (tileData >> 4 & 7);
			int tileY = anInt1269 + (tileData & 7);
			int objID = stream.read3Bytes();
			int typeAndFaceBits = stream.method428();
			int obType = typeAndFaceBits >> 2;
			int obFace = typeAndFaceBits & 3;
			int toPlane = stream.readSignedByte();
			int l17 = anIntArray1177[obType];
			if (tileX >= 0 && tileY >= 0 && tileX < 104 && tileY < 104)
				method130(-1, objID, obFace, l17, tileY, obType, toPlane, tileX, 0);
			return;
		}
		
		if (j == 153) {
			int chunkX = stream.readUnsignedByte() << 3;
			int chunkY = stream.readUnsignedByte() << 3;
			int toPlane = stream.readSignedByte();
			if (toPlane == 10) {
				method63();
				return;
			}

			int endX = chunkX + 7;
			int endY = chunkY + 7;
			GameObject request = (GameObject) aClass19_1179.reverseGetFirst();
			for (; request != null; request = (GameObject) aClass19_1179.reverseGetNext()) {
				if (request.tileX >= chunkX && request.tileX <= endX && request.tileY >= chunkY
						&& request.tileY <= endY && request.plane == toPlane)
					request.unlink();
			}
			return;
		}

		if (j == 154) {
			for (int x = 0; x < 104; x++) {
				for (int y = 0; y < 104; y++) {
					GameObject request = (GameObject) aClass19_1179.reverseGetFirst();
					for (; request != null; request = (GameObject) aClass19_1179.reverseGetNext()) {
						if (request.tileX == x && request.tileX == x && request.tileY == y
								&& request.plane == plane)
							request.unlink();
					}

					if (groundArray[plane][x][y] != null) {
						groundArray[plane][x][y] = null;
						spawnGroundItem(x, y);
					}
				}
			}
			return;
		}
		
		if (j == 4) {
			int j2 = stream.readUnsignedByte();
			int i5 = anInt1268 + (j2 >> 4 & 7);
			int l7 = anInt1269 + (j2 & 7);
			int k10 = stream.readUnsignedWord();
			int l12 = stream.readUnsignedByte();
			int j15 = stream.readUnsignedWord();
			if (i5 >= 0 && l7 >= 0 && i5 < 104 && l7 < 104) {
				i5 = i5 * 128 + 64;
				l7 = l7 * 128 + 64;
				Animable_Sub3 class30_sub2_sub4_sub3 = new Animable_Sub3(plane, loopCycle, j15, k10,
						method42(plane, l7, i5) - l12, l7, i5);
				aClass19_1056.insertHead(class30_sub2_sub4_sub3);
			}
			return;
		}
		if (j == 44) {
			int k2 = stream.read3Bytes();
			int j5 = stream.readUnsignedWord();
			int i8 = stream.readUnsignedByte();
			int l10 = anInt1268 + (i8 >> 4 & 7);
			int i13 = anInt1269 + (i8 & 7);
			if (l10 >= 0 && i13 >= 0 && l10 < 104 && i13 < 104) {
				Item class30_sub2_sub4_sub2_1 = new Item();
				class30_sub2_sub4_sub2_1.ID = k2;
				class30_sub2_sub4_sub2_1.anInt1559 = j5;
				if (groundArray[plane][l10][i13] == null)
					groundArray[plane][l10][i13] = new NodeList();
				groundArray[plane][l10][i13].insertHead(class30_sub2_sub4_sub2_1);
				spawnGroundItem(l10, i13);
			}
			return;
		}
		if (j == 101) {
			int l2 = stream.method427();
			int k5 = l2 >> 2;
			int j8 = l2 & 3;
			int i11 = anIntArray1177[k5];
			int j13 = stream.readUnsignedByte();
			int k15 = anInt1268 + (j13 >> 4 & 7);
			int l16 = anInt1269 + (j13 & 7);
			if (k15 >= 0 && l16 >= 0 && k15 < 104 && l16 < 104)
				method130(-1, -1, j8, i11, l16, k5, plane, k15, 0);
			return;
		}
		if (j == 117) {
			int mapZoneOffsetBitPack = stream.readUnsignedByte();
			int startX = anInt1268 + (mapZoneOffsetBitPack >> 4 & 7);
			int startZ = anInt1269 + (mapZoneOffsetBitPack & 7);
			int endX = startX + stream.readSignedByte();
			int endZ = startZ + stream.readSignedByte();
			int entityIndex = stream.readSignedWord();
			int spotAnimId = stream.readUnsignedWord();
			int startY = stream.readUnsignedByte() * 4;
			int endY = stream.readUnsignedByte() * 4;
			int startDelay = stream.readUnsignedWord();
			int endDelay = stream.readUnsignedWord();
			int angle = stream.readUnsignedByte();
			int j21 = stream.readUnsignedByte();
			if (startX >= 0 && startZ >= 0 && startX < 104 && startZ < 104 && endX >= 0 && endZ >= 0 && endX < 104 && endZ < 104
					&& spotAnimId != 65535) {
				startX = startX * 128 + 64;
				startZ = startZ * 128 + 64;
				endX = endX * 128 + 64;
				endZ = endZ * 128 + 64;
				Animable_Sub4 class30_sub2_sub4_sub4 = new Animable_Sub4(angle, endY, startDelay + loopCycle, endDelay + loopCycle,
						j21, plane, method42(plane, startZ, startX) - startY, startZ, startX, entityIndex, spotAnimId);
				class30_sub2_sub4_sub4.method455(startDelay + loopCycle, endZ, method42(plane, endZ, endX) - endY, endX);
				aClass19_1013.insertHead(class30_sub2_sub4_sub4);
			}
		}
	}
	
	private static void setLowMem() {
		WorldController.lowMem = true;
		lowMem = true;
		ObjectManager.lowMem = true;
		ObjectDef.lowMem = true;
	}
	
	public void method139(RSBuffer stream) {
		stream.initBitAccess();
		int k = stream.readBits(8);
		if (k < npcCount) {
			for (int l = k; l < npcCount; l++)
				anIntArray840[anInt839++] = npcIndices[l];
			
		}
		if (k > npcCount) {
			SignLink.reporterror(myUsername + " Too many npcs");
			throw new RuntimeException("eek");
		}
		npcCount = 0;
		for (int i1 = 0; i1 < k; i1++) {
			int j1 = npcIndices[i1];
			Npc npc = npcArray[j1];
			int k1 = stream.readBits(1);
			if (k1 == 0) {
				npcIndices[npcCount++] = j1;
				npc.anInt1537 = loopCycle;
			} else {
				int l1 = stream.readBits(2);
				if (l1 == 0) {
					npcIndices[npcCount++] = j1;
					npc.anInt1537 = loopCycle;
					anIntArray894[anInt893++] = j1;
				} else if (l1 == 1) {
					npcIndices[npcCount++] = j1;
					npc.anInt1537 = loopCycle;
					int i2 = stream.readBits(3);
					npc.moveInDir(false, i2);
					int k2 = stream.readBits(1);
					if (k2 == 1)
						anIntArray894[anInt893++] = j1;
				} else if (l1 == 2) {
					npcIndices[npcCount++] = j1;
					npc.anInt1537 = loopCycle;
					int j2 = stream.readBits(3);
					npc.moveInDir(true, j2);
					int l2 = stream.readBits(3);
					npc.moveInDir(true, l2);
					int i3 = stream.readBits(1);
					if (i3 == 1)
						anIntArray894[anInt893++] = j1;
				} else if (l1 == 3)
					anIntArray840[anInt839++] = j1;
			}
		}
		
	}
	
	private void markMinimap(Sprite sprite, int x, int y) {
		if (sprite == null)
			return;
		try {
			int l = x * x + y * y;
			if (l > 6400) {
				return;
			}
			int offX = isFixed() ? 0 : clientWidth - 249;
			if (GraphicsDisplay.enabled) {
				offX += 516;
			}
			int k = viewRotation + minimapRotation & 0x7ff;
			int i1 = Rasterizer.SINE[k];
			int j1 = Rasterizer.COSINE[k];
			i1 = (i1 * 256) / (minimapZoom + 256);
			j1 = (j1 * 256) / (minimapZoom + 256);
			int k1 = y * i1 + x * j1 >> 16;
			int l1 = y * j1 - x * i1 >> 16;
			if (isFixed())
				sprite.drawSprite(((99 + k1) - sprite.maxWidth / 2) + 4 + offX, 91 - l1 - sprite.maxHeight / 2 - 4);
			else
				sprite.drawSprite(((75 + k1) - sprite.maxWidth / 2) + 4 + (clientWidth - 167),
						87 - l1 - sprite.maxHeight / 2 - 4);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void method142(int i, int j, int k, int l, int i1, int j1, int k1, int regionType) {
		if (i1 >= 1 && i >= 1 && i1 <= 102 && i <= 102) {
			if (ObjectManager.lowMem && j != plane && k1 != 30029 && k1 != 30030) {// redraws custom spawned
				// objects?
				//30029 an 30030 are olm reward lights
				return;
			}
			long i2 = 0;
			if (j1 == 0) {
				i2 = worldController.method300(j, i1, i);
			} else if (j1 == 1) {
				i2 = worldController.method301(j, i1, i);
			} else if (j1 == 2) {
				i2 = worldController.method302(j, i1, i);
			} else if (j1 == 3) {
				i2 = worldController.method303(j, i1, i);
			}
			if (i2 != 0L) {
				int j2 = (int) ((i2 >>> 32) & 0x7fffffff);
				int k2 = (int) (i2 >> 14) & 0x1f;
				int l2 = (int) (i2 >> 20) & 0x3;
				if (j1 == 0) {
					worldController.method291(i1, j, i, (byte) -119);
					ObjectDef class46 = ObjectManager.getObjectDef(j2, regionType);
					if (class46.aBoolean767)
						aClass11Array1230[j].method215(l2, k2, class46.aBoolean757, i1, i);
				} else if (j1 == 1) {
					worldController.method292(i, j, i1);
				} else if (j1 == 2) {
					worldController.method293(j, i1, i);
					ObjectDef class46_1 = ObjectManager.getObjectDef(j2, regionType);
					if (i1 + class46_1.anInt744 > 103 || i + class46_1.anInt744 > 103 || i1 + class46_1.anInt761 > 103
							|| i + class46_1.anInt761 > 103)
						return;
					if (class46_1.aBoolean767)
						aClass11Array1230[j].method216(l2, class46_1.anInt744, i1, i, class46_1.anInt761,
								class46_1.aBoolean757);
				} else if (j1 == 3) {
					worldController.method294(j, i, i1);
					ObjectDef class46_2 = ObjectManager.getObjectDef(j2, regionType);
					if (class46_2.aBoolean767 && class46_2.hasActions)
						aClass11Array1230[j].method218(i, i1);
				}
			}
			if (k1 >= 0) {
				int j3 = j;
				if (j3 < 3 && (byteGroundArray[1][i1][i] & 2) == 2)
					j3++;
				ObjectManager.method188(worldController, k, i, l, j3, aClass11Array1230[j], intGroundArray, i1, k1, j, regionType);
			}
		}
	}
	
	public void updatePlayers(int i, RSBuffer stream) {
		anInt839 = 0;
		anInt893 = 0;
		method117(stream);
		method134(stream);
		method91(stream, i);
		method49(stream);
		for (int k = 0; k < anInt839; k++) {
			int l = anIntArray840[k];
			if (playerArray[l].anInt1537 != loopCycle)
				playerArray[l] = null;
		}
		
		if (stream.currentOffset != i) {
			SignLink.reporterror("Error packet size mismatch in getplayer pos:" + stream.currentOffset + " psize:" + i);
			throw new RuntimeException("eek");
		}
		for (int i1 = 0; i1 < playerCount; i1++)
			if (playerArray[playerIndices[i1]] == null) {
				SignLink.reporterror(myUsername + " null entry in pl list - pos:" + i1 + " size:" + playerCount);
				throw new RuntimeException("eek");
			}
		
	}
	
	public void setCameraPos(int j, int k, int l, int i1, int j1, int k1) {
		int l1 = 2048 - k & 0x7ff;
		int i2 = 2048 - j1 & 0x7ff;
		int j2 = 0;
		int k2 = 0;
		int l2 = calcZoom(j);
		if (l1 != 0) {
			int i3 = Rasterizer.SINE[l1];
			int k3 = Rasterizer.COSINE[l1];
			int i4 = k2 * k3 - l2 * i3 >> 16;
			l2 = k2 * i3 + l2 * k3 >> 16;
			k2 = i4;
		}
		if (i2 != 0) {
			int j3 = Rasterizer.SINE[i2];
			int l3 = Rasterizer.COSINE[i2];
			int j4 = l2 * j3 + j2 * l3 >> 16;
			l2 = l2 * l3 - j2 * j3 >> 16;
			j2 = j4;
		}
		xCameraPos = l - j2;
		zCameraPos = i1 - k2;
		yCameraPos = k1 - l2;
		yCameraCurve = k;
		xCameraCurve = j1;
	}

	public void setInterfaceText(String str, int i) {
		RSInterface component = RSInterface.interfaceCache[i];
		if (component == null) {
			return;
		}

		component.message = str;
		if (component.parentID == tabInterfaceIDs[tabID]) {
			needDrawTabArea = true;
		}
	}

	private boolean parsePacket() {
		if (gameSocket == null)
			return false;
		try {
			int i = gameSocket.available();
			if (i == 0)
				return false;
			if (pktType == -1) {
				gameSocket.read(inStream.buffer, 0, 1);
				pktType = inStream.buffer[0] & 0xff;
				if (encryption != null)
					pktType = pktType - encryption.getNextKey() & 0xff;
				pktSize = SizeConstants.packetSizes[pktType];
				i--;
			}
			if (pktSize == -1)
				if (i > 0) {
					gameSocket.read(inStream.buffer, 0, 1);
					pktSize = inStream.buffer[0] & 0xff;
					i--;
				} else {
					return false;
				}
			if (pktSize == -2)
				if (i > 1) {
					gameSocket.read(inStream.buffer, 0, 2);
					inStream.currentOffset = 0;
					pktSize = inStream.readUnsignedWord();
					i -= 2;
				} else {
					return false;
				}
			if (i < pktSize)
				return false;
			inStream.currentOffset = 0;
			gameSocket.read(inStream.buffer, 0, pktSize);
			anInt1009 = 0;
			anInt843 = anInt842;
			anInt842 = anInt841;
			anInt841 = pktType;
			// System.out.println("packetType "+pktType);
			switch (pktType) {
				case 81:
					updatePlayers(pktSize, inStream);
					aBoolean1080 = false;
					pktType = -1;
					return true;
				
				case 176:
					daysSinceRecovChange = inStream.method427();
					unreadMessages = inStream.method435();
					membersInt = inStream.readUnsignedByte();
					anInt1193 = inStream.method440();
					daysSinceLastLogin = inStream.readUnsignedWord();
					if (anInt1193 != 0 && openModalInterfaceId == -1) {
						closeInterfacesTransmit();
						char c = '\u028A';
						if (daysSinceRecovChange != 201 || membersInt == 1)
							c = '\u028F';
						reportAbuseInput = "";
						canMute = false;
						for (int k9 = 0; k9 < RSInterface.interfaceCache.length; k9++) {
							if (RSInterface.interfaceCache[k9] == null || RSInterface.interfaceCache[k9].contentType != c)
								continue;
							openModalInterface(RSInterface.interfaceCache[k9].parentID);
						}
					}
					pktType = -1;
					return true;
				
				case 64:
					anInt1268 = inStream.method427();
					anInt1269 = inStream.method428();
					for (int j = anInt1268; j < anInt1268 + 8; j++) {
						for (int l9 = anInt1269; l9 < anInt1269 + 8; l9++) {
							if (groundArray[plane][j][l9] != null) {
								groundArray[plane][j][l9] = null;
								spawnGroundItem(j, l9);
							}
						}
					}
					for (GameObject gameObject = (GameObject) aClass19_1179
							.reverseGetFirst(); gameObject != null; gameObject = (GameObject) aClass19_1179
							.reverseGetNext())
						if (gameObject.tileX >= anInt1268 && gameObject.tileX < anInt1268 + 8
								&& gameObject.tileY >= anInt1269 && gameObject.tileY < anInt1269 + 8
								&& gameObject.plane == plane)
							gameObject.removeTime = 0;
					pktType = -1;
					return true;
				
				case 185: // draw my player on an interface
					int k = inStream.method436();
					RSInterface.interfaceCache[k].disabledMediaType = 3;
					RSInterface.interfaceCache[k].disabledMediaID = myPlayer.appearance.getHeadModelUid();
					pktType = -1;
					return true;
				
				case 107:
					aBoolean1160 = false;
					for (int l = 0; l < 5; l++)
						aBooleanArray876[l] = false;
					pktType = -1;
					checkHax();
					return true;
				
				case 72:
					int i1 = inStream.read3Bytes();
					RSInterface class9 = RSInterface.interfaceCache[i1];
					if (class9 != null) {
						for (int k15 = 0, length = class9.inv.length; k15 < length; k15++) {
							class9.inv[k15] = -1;
							class9.invStackSizes[k15] = 0;
						}
					}

					pktType = -1;
					return true;
				
				case 214:
					ignoreCount = pktSize / 8;
					for (int j1 = 0; j1 < ignoreCount; j1++)
						ignoreListAsLongs[j1] = inStream.readQWord();
					pktType = -1;
					return true;
				
				case 166:
					aBoolean1160 = true;
					anInt1098 = inStream.readUnsignedByte();
					anInt1099 = inStream.readUnsignedByte();
					anInt1100 = inStream.readUnsignedWord();
					anInt1101 = inStream.readUnsignedByte();
					anInt1102 = inStream.readUnsignedByte();
					if (anInt1102 >= 100) {
						xCameraPos = anInt1098 * 128 + 64;
						yCameraPos = anInt1099 * 128 + 64;
						zCameraPos = method42(plane, yCameraPos, xCameraPos) - anInt1100;
					}
					pktType = -1;
					return true;
				
				case 134:
					needDrawTabArea = true;
					final int skillSlot = inStream.readUnsignedByte();
					final int experience = inStream.method439();
					final int currentLevel = inStream.readUnsignedByte();
					
					// int xp = currentExp[k1];
					int oldXp = currentExp[skillSlot];
					int expDrops = 0;
					
					if (!playerCompletedInitialize || (playerCompletedInitialize && !isLockedExp)) {
						currentExp[skillSlot] = experience;
					}
					currentStats[skillSlot] = currentLevel;
					maxStats[skillSlot] = 1;

					if (!updatedSkills.contains(skillSlot)) {
						updatedSkills.add(skillSlot);
					}

					int maxCheck = maxLevels[skillSlot] - 1;
					for (int k20 = 0; k20 < maxCheck; k20++)
						if (experience >= anIntArray1019[k20])
							maxStats[skillSlot] = k20 + 2;
					
					long totalXP = 0;
					int totalLevel = 0;
					for (int indexSkill = 0, length = currentExp.length; indexSkill < length; indexSkill++) {
						totalXP += currentExp[indexSkill];
						totalLevel += maxStats[indexSkill];
					}
					/*
					 * int frameId = skillFrameIds[k1][0]; int frameId2 =
					 * skillFrameIds[k1][1]; sendFrame126("<col=ffff00>" +
					 * Integer.toString(l15),frameId++); //current level
					 * sendFrame126("<col=ffff00>"
					 * +Integer.toString(maxStats[k1]),frameId2++); //max level,
					 * second row // sendFrame126("<col=ff0000>"
					 * +Integer.toString(currentExp[k1]),frameId++); // current exp?
					 * // sendFrame126("<col=ffffff>"
					 * +Integer.toString(anIntArray1019[maxStats[k1] -
					 * 1]),frameId2);
					 * //sendFrame126(Integer.toString(totalXP),####); sendFrame126(
					 * "Total Level:"+Integer.toString(totalLevel),3984); /**
					 * updating the prayer
					 */
					switch (skillSlot) {
						case 5:
							setInterfaceText(currentLevel + "/" + maxStats[skillSlot], 687);
							break;
						case 23:
							setInterfaceText(currentLevel + "/" + maxStats[skillSlot], 17025);
							break;
					}
					
					if (maxStats[skillSlot] < maxLevels[skillSlot]) {
						int index = maxStats[skillSlot] - 1;
						if (index >= 0 && index < anIntArray1019.length) {
							int nextXp = anIntArray1019[index];
							setInterfaceText("Exp: " + Strings.formatNumbersWithCommas(currentExp[skillSlot]) + "\nRemainder: "
									+ Strings.formatNumbersWithCommas(nextXp - currentExp[skillSlot]) + "\nNext Lvl: "
									+ Strings.formatNumbersWithCommas(nextXp), skillInterfaceId[skillSlot]); // exp
						}
					} else {
						setInterfaceText("Exp: " + Strings.formatNumbersWithCommas(currentExp[skillSlot]), skillInterfaceId[skillSlot]); // exp
					}

					setInterfaceText(Strings.formatNumbersWithCommas(totalLevel), 31200);
					setInterfaceText(Integer.toString(currentLevel), skillInterfaceId[skillSlot] + 1); // current level
					setInterfaceText(Integer.toString(maxStats[skillSlot]), skillInterfaceId[skillSlot] + 2); // max level, second row
					if (experience > oldXp) {
						expDrops = (experience - oldXp);
						setInterfaceText("Total EXP: " + Strings.formatNumbersWithCommas(totalXP), 31198);
					}
					
					if (playerCompletedInitialize) {
						
						if (drawXpBar) {
							drawFlag = true;
							
							xpToDrawOnBar += expDrops;// experience-oldXp;
							
							// if(xpToDrawOnBar == currentExp[k1]) {
							// xpToDrawOnBar = 0;
							// xpToDraw = 0;
							// }
							
						}
						
					}
					
					// xpCounter += currentExp[k1] - xp;
					// expAdded = currentExp[k1] - xp;
					
					pktType = -1;
					
					return true;
				
				case 71: {
					int interfaceId = inStream.read3Bytes();
					int index = inStream.readUnsignedByte();
					if (interfaceId == 16777215) {
						interfaceId = -1;
					}

					tabInterfaceIDs[index] = interfaceId;
					needDrawTabArea = true;
					pktType = -1;
					return true;
				}

				case 74:
					int i2 = inStream.method434();
					if (i2 == 65535) {
						i2 = -1;
					}
					
					if (i2 != -1 || previousSong != 0) {
						if (i2 != -1 && currentSong != i2 && musicVolume != 0 && previousSong == 0)
							method58(10, musicVolume, false, i2);
					} else
						method55(false);
					currentSong = i2;
					pktType = -1;
					return true;
				
				case 121:
					int i_60_ = inStream.method436();
					if (i_60_ == 65535) {
						i_60_ = -1;
					}
					
					int i_61_ = inStream.method435();
					if (musicVolume != 0 && i_61_ != -1) {
						method56(musicVolume, false, i_60_);
						previousSong = i_61_ * 20;
					}
					
					pktType = -1;
					return true;
				
				case 109:
					resetLogout();
					pktType = -1;
					return false;

				case 180:
					ReflectionCheck.readReflectionCheckPacket(inStream);
					pktType = -1;
					return true;

				case 70:
					int k2 = inStream.readSignedWord();
					int l10 = inStream.method437();
					int i16 = inStream.method434();
					RSInterface class9_5 = RSInterface.interfaceCache[i16];
					class9_5.xOffset = k2;
					class9_5.yOffset = l10;
					pktType = -1;
					return true;
				
				case 73:
				case 241:
					int regionX = anInt1069;
					int regionY = anInt1070;
					if (pktType == 73) {
						regionX = inStream.method435();
						regionY = inStream.readUnsignedWord();
						constructedViewport = false;
						if (!gameLoaded)
							gameLoaded = true;
					}
					if (pktType == 241) {
						regionY = inStream.method435();
						inStream.initBitAccess();
						for (int z = 0; z < 4; z++) {
							for (int x = 0; x < 13; x++) {
								for (int y = 0; y < 13; y++) {
									int i26 = inStream.readBits(1);
									if (i26 == 1)
										localRegions[z][x][y] = inStream.readBits(28);
									else
										localRegions[z][x][y] = -1;
								}
							}
						}
						inStream.finishBitAccess();
						regionX = inStream.readUnsignedWord();
						constructedViewport = true;
						if (!gameLoaded)
							gameLoaded = true;
					}
					if (pktType != 241 && anInt1069 == regionX && anInt1070 == regionY && loadingStage == 2) {
						pktType = -1;
						return true;
					}
					
					anInt1069 = regionX;
					anInt1070 = regionY;
					regionBaseX = (anInt1069 - 6) << 3;
					regionBaseY = (anInt1070 - 6) << 3;
					
					final int offsetX = (regionBaseX - lastBaseX);
					final int offsetY = (regionBaseY - lastBaseY);
					lastBaseX = regionBaseX;
					lastBaseY = regionBaseY;
					
					baseOffsetX += offsetX;
					baseOffsetY += offsetY;
					
					inTutorialIsland = (anInt1069 >> 3 == 48 || anInt1069 >> 3 == 49) && anInt1070 >> 3 == 48;
					if (anInt1069 >> 3 == 48 && anInt1070 >> 3 == 148)
						inTutorialIsland = true;
					loadingStage = 1;
					loadingStartTime = System.currentTimeMillis();
					setTextOnScren("Loading - please wait.");
					if (pktType == 73) {
						int regionCount = 0;
						for (int i21 = (anInt1069 - 6) >> 3; i21 <= (anInt1069 + 6) >> 3; i21++) {
							for (int k23 = (anInt1070 - 6) >> 3; k23 <= (anInt1070 + 6) >> 3; k23++)
								regionCount++;
						}
						localRegionMapData = new byte[regionCount][];
						localRegionLandscapeData = new byte[regionCount][];
						localRegionIds = new int[regionCount];
						localRegionMapIds = new int[regionCount];
						localRegionLandscapeIds = new int[regionCount];
						regionCount = 0;
						for (int x = (anInt1069 - 6) >> 3; x <= (anInt1069 + 6) >> 3; x++) {
							for (int y = (anInt1070 - 6) >> 3; y <= (anInt1070 + 6) >> 3; y++) {
								int regionId = (x << 8) + y;
								localRegionIds[regionCount] = regionId;
								if (inTutorialIsland
										&& (y == 49 || y == 149 || y == 147 || x == 50 || x == 49 && y == 47)) {
									localRegionMapIds[regionCount] = -1;
									localRegionLandscapeIds[regionCount] = -1;
									regionCount++;
								} else {
									int index = regionBaseX >= 6400 || Client.osrsMapIn317(regionId) ? 6 : 3;
									int mapId = localRegionMapIds[regionCount] = onDemandFetcher.method562(0, regionId);
									if (mapId != -1) {
										if (!onDemandFetcher.loadData(index, mapId)) {
											localRegionMapIds[regionCount] = -1;
										}
									}
									int landscapeId = localRegionLandscapeIds[regionCount] = onDemandFetcher.method562(1, regionId);
									if (landscapeId != -1) {
										if (!onDemandFetcher.loadData(index, landscapeId)) {
											localRegionLandscapeIds[regionCount] = -1;
										}
									}
									regionCount++;
								}
							}
						}
					}
					if (pktType == 241) {
						List<Integer> regionIds = new ArrayList<>();
						for (int z = 0; z < 4; z++) {
							for (int x = 0; x < 13; x++) {
								for (int y = 0; y < 13; y++) {
									int data = localRegions[z][x][y];
									if (data != -1) {
										int constructedRegionX = data >> 15 & 0x7ff;
										int constructedRegionY = data >> 3 & 0x7ff;
										int region = (constructedRegionX / 8 << 8) + constructedRegionY / 8;
										for (int index = 0, length = regionIds.size(); index < length; index++) {
											if (regionIds.get(index) != region) {
												continue;
											}

											region = -1;
										}

										if (region != -1) {
											regionIds.add(region);
										}
									}
								}
							}
						}

						int regionCount = regionIds.size();
						localRegionOsrs = new boolean[regionCount];
						localRegionMapData = new byte[regionCount][];
						localRegionLandscapeData = new byte[regionCount][];
						localRegionIds = new int[regionCount];
						localRegionMapIds = new int[regionCount];
						localRegionLandscapeIds = new int[regionCount];
						for (int index = 0; index < regionCount; index++) {
							int regionId = localRegionIds[index] = regionIds.get(index);
							int constructedRegionX = regionId >> 8 & 0xff;
							int constructedRegionY = regionId & 0xff;
							int cacheIndex;

							if (constructedRegionX >= 100) {
								cacheIndex = 6;
								localRegionOsrs[index] = true;
							} else {
								cacheIndex = 3;
								localRegionOsrs[index] = false;
							}

							int map = localRegionMapIds[index] = onDemandFetcher.method562(0, regionId);
							if (map != -1) {
								if (!onDemandFetcher.loadData(cacheIndex, map)) {
									localRegionMapIds[index] = -1;
								}
							}
							int landscape = localRegionLandscapeIds[index] = onDemandFetcher.method562(1, regionId);
							if (landscape != -1) {
								if (!onDemandFetcher.loadData(cacheIndex, landscape)) {
									localRegionLandscapeIds[index] = -1;
								}
							}
						}
					}
					int rdx = regionBaseX - anInt1036;
					int rdy = regionBaseY - anInt1037;
					anInt1036 = regionBaseX;
					anInt1037 = regionBaseY;
					for (int j24 = 0; j24 < 16384; j24++) {
						Npc npc = npcArray[j24];
						if (npc != null) {
							for (int j29 = 0; j29 < 10; j29++) {
								npc.smallX[j29] -= rdx;
								npc.smallY[j29] -= rdy;
							}
							npc.x -= rdx * 128;
							npc.z -= rdy * 128;
						}
					}
					for (int i27 = 0; i27 < maxPlayers; i27++) {
						Player player = playerArray[i27];
						if (player != null) {
							for (int i31 = 0; i31 < 10; i31++) {
								player.smallX[i31] -= rdx;
								player.smallY[i31] -= rdy;
							}
							player.x -= rdx * 128;
							player.z -= rdy * 128;
						}
					}
					aBoolean1080 = true;
					byte byte1 = 0;
					byte byte2 = 104;
					byte byte3 = 1;
					if (rdx < 0) {
						byte1 = 103;
						byte2 = -1;
						byte3 = -1;
					}
					byte byte4 = 0;
					byte byte5 = 104;
					byte byte6 = 1;
					if (rdy < 0) {
						byte4 = 103;
						byte5 = -1;
						byte6 = -1;
					}
					for (int k33 = byte1; k33 != byte2; k33 += byte3) {
						for (int l33 = byte4; l33 != byte5; l33 += byte6) {
							int i34 = k33 + rdx;
							int j34 = l33 + rdy;
							for (int k34 = 0; k34 < 4; k34++)
								if (i34 >= 0 && j34 >= 0 && i34 < 104 && j34 < 104)
									groundArray[k34][k33][l33] = groundArray[k34][i34][j34];
								else
									groundArray[k34][k33][l33] = null;
						}
					}
					for (GameObject gameObject = (GameObject) aClass19_1179
							.reverseGetFirst(); gameObject != null; gameObject = (GameObject) aClass19_1179
							.reverseGetNext()) {
						gameObject.tileX -= rdx;
						gameObject.tileY -= rdy;
						if (gameObject.tileX < 0 || gameObject.tileY < 0 || gameObject.tileX >= 104
								|| gameObject.tileY >= 104)
							gameObject.unlink();
					}
					if (destX != 0) {
						destX -= rdx;
						destY -= rdy;
					}
					aBoolean1160 = false;
					
					if (noClip) {
						for (int k12 = 0; k12 < 4; k12++) {
							for (int i22 = 1; i22 < 103; i22++) {
								for (int k22 = 1; k22 < 103; k22++)
									aClass11Array1230[k12].anIntArrayArray294[i22][k22] = 0;
								
							}
						}
					}
					
					pktType = -1;
					return true;
				
				case 208:
					int i3 = inStream.readDWord();
					openOverlayInterface(i3);
					pktType = -1;
					return true;
				
				case 99: {
					/*
					 * Black map, mape state 2, causes blackness
					 */
					int value = inStream.readUnsignedByte();
					minimapBlackout = value & 0x03;
					if ((value & 0x80) != 0) {
						screenBlackout = true;
						screenBlackoutTrans = 0;
					} else if ((value & 0x40) != 0) {
						screenBlackout = true;
						screenBlackoutTrans = 255;
					} else {
						screenBlackout = false;
					}

					refreshGLMinimap = true;
					pktType = -1;
					return true;
				}

				// Draw an NPC on an interface
				case 75: {
					int npcId = inStream.read3Bytes();
					int interfaceId = inStream.method436();
					RSInterface rsi = RSInterface.interfaceCache[interfaceId];

					if (interfaceId == 4888 || interfaceId == 4883 || interfaceId == 4894 || interfaceId == 4901) { // dialogue interfaces
						if (npcId == 11307 || npcId == 4243) { // demon butler
							rsi.modelZoom = 796;
						} else {
							rsi.modelZoom = 1600;
						}
					}

					//System.out.println("interfaceId: " + interfaceId + " npcId: " + npcId);

					rsi.disabledMediaType = 2;
					rsi.disabledMediaID = npcId;

					if (interfaceId == 17027) { // editing the summoning interface
						if (npcId == 4000) { // resets npc
							animateSummonTab = false;
						} else {
							animateSummonTab = true;
						}
					}
					pktType = -1;
				}
					return true;
				
				case 114:
					anInt1104 = inStream.method434() * 30;
					pktType = -1;
					return true;
				
				case 60:
					anInt1269 = inStream.readUnsignedByte();
					anInt1268 = inStream.method427();
					while (inStream.currentOffset < pktSize) {
						int k3 = inStream.readUnsignedByte();
						method137(inStream, k3);
					}
					pktType = -1;
					return true;
				
				case 35:
					int l3 = inStream.readUnsignedByte();
					int k11 = inStream.readUnsignedByte();
					int j17 = inStream.readUnsignedByte();
					int k21 = inStream.readUnsignedByte();
					aBooleanArray876[l3] = true;
					anIntArray873[l3] = k11;
					anIntArray1203[l3] = j17;
					anIntArray928[l3] = k21;
					anIntArray1030[l3] = 0;
					pktType = -1;
					return true;
				
				case 174: {
					int id = inStream.readUnsignedWord();
					int delay = inStream.readUnsignedWord();
					WavPlayer.play(id, delay);
					pktType = -1;
					return true;
				}
				
				case 104: {
					int j4 = inStream.method427();
					int i12 = inStream.method426();
					String s6 = inStream.readString();
					if (j4 >= 1 && j4 <= PlayerOption.OPTION_COUNT) {
						int index = j4 - 1;

						if (s6.equalsIgnoreCase("null")) {
							playerOptions[index] = null;
						} else {
							playerOptions[index] = new PlayerOption(s6, i12 == 0);
						}
					}

					pktType = -1;
					return true;
				}

				case 78:
					int x = inStream.readUnsignedByte();
					int y = inStream.readUnsignedByte();
					destX = x;
					destY = y;
					pktType = -1;
					return true;
				
				case 253: {
					int type = inStream.readUnsignedByte();
					boolean hasName = inStream.readUnsignedByte() == 1;
					String name = null;
					boolean ignored = false;

					if (hasName) {
						name = inStream.readString();
						long nameAsLong = TextClass.longForName(name);
						for (int id = 0; id < ignoreCount; id++) {
							if (ignoreListAsLongs[id] != nameAsLong) {
								continue;
							}

							ignored = true;
							break;
						}
					}

					String message = inStream.readString();
					if (!ignored) {
						pushMessage(message, type, name);
					}

					pktType = -1;
					return true;
				}

				case 1:
					for (int k4 = 0; k4 < playerArray.length; k4++)
						if (playerArray[k4] != null)
							playerArray[k4].animId = -1;
					for (int j12 = 0; j12 < npcArray.length; j12++)
						if (npcArray[j12] != null)
							npcArray[j12].animId = -1;
					pktType = -1;
					return true;
				
				case 50:
					long l4 = inStream.readQWord();
					int i18 = inStream.readUnsignedByte();
					String s7 = TextClass.fixName(TextClass.nameForLong(l4));
					for (int k24 = 0; k24 < friendsCount; k24++) {
						if (l4 != friendsListAsLongs[k24])
							continue;
						if (friendsNodeIDs[k24] != i18) {
							friendsNodeIDs[k24] = i18;
							needDrawTabArea = true;
							if (i18 >= 2) {
								pushMessage(s7 + " has logged in.", ChatMessage.PRIVATE_MESSAGE_RECEIVED);
							}
							if (i18 <= 1) {
								pushMessage(s7 + " has logged out.", ChatMessage.PRIVATE_MESSAGE_RECEIVED);
							}
						}
						s7 = null;
						
					}
					if (s7 != null && friendsCount < 200) {
						friendsListAsLongs[friendsCount] = l4;
						friendsList[friendsCount] = s7;
						friendsNodeIDs[friendsCount] = i18;
						friendsCount++;
						needDrawTabArea = true;
					}
					for (boolean flag6 = false; !flag6; ) {
						flag6 = true;
						for (int k29 = 0; k29 < friendsCount - 1; k29++)
							if (friendsNodeIDs[k29] != nodeID && friendsNodeIDs[k29 + 1] == nodeID
									|| friendsNodeIDs[k29] == 0 && friendsNodeIDs[k29 + 1] != 0) {
								int j31 = friendsNodeIDs[k29];
								friendsNodeIDs[k29] = friendsNodeIDs[k29 + 1];
								friendsNodeIDs[k29 + 1] = j31;
								String s10 = friendsList[k29];
								friendsList[k29] = friendsList[k29 + 1];
								friendsList[k29 + 1] = s10;
								long l32 = friendsListAsLongs[k29];
								friendsListAsLongs[k29] = friendsListAsLongs[k29 + 1];
								friendsListAsLongs[k29 + 1] = l32;
								needDrawTabArea = true;
								flag6 = false;
							}
					}
					
					pktType = -1;
					return true;
				
				case 110:
					if (tabID == 12)
						needDrawTabArea = true;
					energy = inStream.readUnsignedByte();
					RSInterface.interfaceCache[149].message = energy + "%";
					pktType = -1;
					return true;
				
				case 254:
					anInt855 = inStream.readUnsignedByte();
					if (anInt855 == 1)
						anInt1222 = inStream.readUnsignedWord();
					if (anInt855 >= 2 && anInt855 <= 6) {
						if (anInt855 == 2) {
							anInt937 = 64;
							anInt938 = 64;
						}
						if (anInt855 == 3) {
							anInt937 = 0;
							anInt938 = 64;
						}
						if (anInt855 == 4) {
							anInt937 = 128;
							anInt938 = 64;
						}
						if (anInt855 == 5) {
							anInt937 = 64;
							anInt938 = 0;
						}
						if (anInt855 == 6) {
							anInt937 = 64;
							anInt938 = 128;
						}
						anInt855 = 2;
						anInt934 = inStream.readUnsignedWord();
						anInt935 = inStream.readUnsignedWord();
						anInt936 = inStream.readUnsignedByte();
					}
					if (anInt855 == 10)
						anInt933 = inStream.readUnsignedWord();
					pktType = -1;
					return true;
				
				case 248:
					int i5 = inStream.read3Bytes();
					if (i5 == 16777215) {
						i5 = -1;
					}

					int k12 = inStream.read3Bytes();
					if (k12 == 16777215) {
						k12 = -1;
					}

					openModalInterface(i5);
					openInventoryModalInterface(k12);
					pktType = -1;
					return true;
				
				case 79:
					int j5 = inStream.method434();
					int l12 = inStream.method435();
					RSInterface class9_3 = RSInterface.interfaceCache[j5];
					if (class9_3 != null && class9_3.interfaceType == 0) {
						if (l12 < 0)
							l12 = 0;
						if (l12 > class9_3.scrollMax - class9_3.height)
							l12 = class9_3.scrollMax - class9_3.height;
						class9_3.scrollPosition = l12;
					}
					pktType = -1;
					return true;
				
				case 68:
					pktType = -1;
					return true;
				
				case 196: {
					long l5 = inStream.readQWord();
					inStream.readDWord();
					ChatCrownType chatCrown = ChatCrownType.get(inStream.readUnsignedByte());
					boolean flag5 = false;
					if (chatCrown.isIgnorable()) {
						for (int l29 = 0; l29 < ignoreCount; l29++) {
							if (ignoreListAsLongs[l29] != l5)
								continue;
							flag5 = true;
						}
					}

					if (!flag5) {
						try {
							String s9 = TextInput.method525(pktSize - 13, inStream);
							if (variousSettings[ConfigCodes.PROFAINTY_FILTER] == 1) {
								s9 = Censor.doCensor(s9);
							}
							s9 = RSFont.escape(s9);
							int type;
							if (chatCrown.isStaff()) {
								type = ChatMessage.PRIVATE_MESSAGE_RECEIVED_STAFF;
							} else {
								type = ChatMessage.PRIVATE_MESSAGE_RECEIVED_PLAYER;
							}

							pushMessage(s9, type, chatCrown.toCrown() + TextClass.fixName(TextClass.nameForLong(l5)));
						} catch (Exception exception1) {
							ErrorHandler.submitError("cde1", exception1);
						}
					}
					pktType = -1;
					return true;
				}
				case 85:
					anInt1269 = inStream.method427();
					anInt1268 = inStream.method427();
					pktType = -1;
					return true;
				
				case 24:
					inStream.method428();
					pktType = -1;
					return true;
				
				case 246: {// Draw item with a model zoom
					int interfaceId = inStream.method434();
					int zoom = inStream.readUnsignedWord();
					int itemId = inStream.read3Bytes();
					Client.redrawGLModels = true;
					if (itemId == 16777215) {
						RSInterface.interfaceCache[interfaceId].disabledMediaType = 0;
					} else {
						ItemDef itemDef = ItemDef.forID(itemId);
						RSInterface.interfaceCache[interfaceId].disabledMediaType = 4;
						RSInterface.interfaceCache[interfaceId].disabledMediaID = itemId;
						RSInterface.interfaceCache[interfaceId].modelRotationY = itemDef.xan2d;
						RSInterface.interfaceCache[interfaceId].modelRotationX = itemDef.yan2d;
						RSInterface.interfaceCache[interfaceId].modelZoom = (itemDef.zoom2d * 100) / Math.max(zoom, 1);
					}

					pktType = -1;
					return true;
				}
				
				case 247: { // draw item with customized colors by leanbow
					int interfaceId = inStream.readUnsignedWord();
					int itemId = inStream.read3Bytes();
					int length = inStream.readUnsignedByte();
					
					ItemDef itemDef = ItemDef.forID(itemId);
					customizedItem = new CustomizedItem(itemDef);
					for (int id = 0; id < length; id++) {
						int colorIndex = inStream.readUnsignedByte();
						int color = inStream.readUnsignedWord();
						customizedItem.newModelColors[colorIndex] = (short) color;
					}
					
					RSInterface.interfaceCache[interfaceId].disabledMediaType = 6;
					RSInterface.interfaceCache[interfaceId].disabledMediaID = itemId;
					// Clear all model caches because item will have new colors,
					// need to rebuild.
					RSInterface.modelCache.clear();
					redrawGLModels = true;
					pktType = -1;
					return true;
				}
				
				case 171:
					boolean flag1 = inStream.readUnsignedByte() == 1;
					int j13 = inStream.readUnsignedWord();
					RSInterface.interfaceCache[j13].isMouseoverTriggered = flag1;
					pktType = -1;
					return true;
				
				case 142:
					int j6 = inStream.method434();
					closeInterfaces();
					openInventoryModalInterface(j6);
					pktType = -1;
					return true;

				case 135: {
					int compId = inStream.readDWord();
					int itemId = inStream.read3Bytes();
					RSInterface component = RSInterface.interfaceCache[compId];
					if (component != null) {
						component.itemId = itemId;
					}
					pktType = -1;
					return true;
				}

				case 133: {
					int type = inStream.readUnsignedByte();
					AbstractFog currentFog = fog;
					int currentFogType = currentFog != null ? currentFog.getId() : -1;

					if (type != currentFogType) {
						fog = currentFog = AbstractFog.createFog(type, inStream);
					}

					if (currentFog != null) {
						currentFog.decode(inStream);
					}

					pktType = -1;
					return true;
				}

				case 132: {
					String name = inStream.readString();
					ChatCrownType chatCrown = ChatCrownType.get(inStream.readUnsignedByte());
					int offset = inStream.readUnsignedByte();
					if (name != null && !name.isEmpty()) {
						long l31 = TextClass.longForName(name);
						boolean flag = false;
						if (chatCrown.isIgnorable()) {
							for (int i4 = 0; i4 < ignoreCount; i4++) {
								if (ignoreListAsLongs[i4] != l31)
									continue;
								flag = true;
								break;
							}
						}
						if (!flag) {
							try {
								aStream_834.currentOffset = 0;
								inStream.method442(offset, 0, aStream_834.buffer);
								aStream_834.currentOffset = 0;
								String s1 = TextInput.method525(offset, aStream_834);
								if (variousSettings[ConfigCodes.PROFAINTY_FILTER] == 1) {
									s1 = Censor.doCensor(s1);
								}

								int type;
								if (chatCrown.isStaff()) {
									type = ChatMessage.PUBLIC_MESSAGE_STAFF;
								} else {
									type = ChatMessage.PUBLIC_MESSAGE_PLAYER;
								}

								pushMessage(s1, type, chatCrown.toCrown() + name);
							} catch (Exception exception) {
								ErrorHandler.submitError("cde3", exception);
							}
						}
					}

					pktType = -1;
					return true;
				}
					
				case 131: {
					String text = inStream.readString();
					int interfaceId = inStream.readUnsignedWord();
					int componentId = inStream.readUnsignedWord();

					RSInterface base = RSInterface.interfaceCache[interfaceId];
					if (base != null && componentId < base.dynamicComponents.length) {
						base.dynamicComponents[componentId].message = text;
					}

					pktType = -1;
					return true;
				}

				case 130:
					String message = inStream.readString();
					if (message.isEmpty()) {
						announcement = null;
					} else {
						message = Character.toUpperCase(message.charAt(0)) + message.substring(1, message.length());
						announcement = new Announcement(message);
					}

					pktType = -1;
					return true;

				case 129:
					int id = inStream.readUnsignedByte();
					switch(id) {
						case 0:
							updatePollInterface(inStream);
							break;
						case 1:
							updateTaskInterface(inStream);
							break;
						case 2:
							updateTaskSideBarInterface(inStream);
							break;
						case 3:
							updateRagInterface(inStream);
							break;
						case 4:
							sendExpDrop(inStream);
							break;
						case 5:
							Autocast = false;
							autocastId = 0;
							break;
						case 6:
							leftClick = inStream.readUnsignedByte();
							break;
						case 7:
							updateRaidsSidebar(inStream);
							break;
						case 8:
							updateRaidsHud(inStream);
							break;
						case 9:
							updateHPHud(inStream);
							break;
						case 10:
							updateDiscordInfo(inStream);
							break;
						case 11:
							updateSeasonPassInterface(inStream);
							break;
						case 12:
							updateVoteLuckInterface(inStream);
							break;
						case 13:
							updateVoteRewardsInterface(inStream);
							break;
						case 14:
							updateDonationInterfaceStore(inStream);
							break;
						case 15:
							updateDonationInterfaceCategory(inStream);
							break;
						case 16:
							hpOrbState = inStream.readUnsignedByte();
							break;
						case 17:
							NightmareOverlay.decodeFromServer(inStream);
							break;
						case 18:
							InstanceInterface.refreshInstancesFromServer(inStream);
							break;
						case 19:
							PresetsInterface.decodeFromServer(inStream);
							break;
						case 20:
							GambleInterfaces.decodeFromServer(inStream);
							break;
						case 21:
							PowerUpInterface.decodeFromServer(inStream);
							break;
						case 22:
							PowerUpRoll.open(inStream);
							break;
						case 23:
							currentHp = inStream.readUnsignedWord();
							maxHp = inStream.readUnsignedWord();
							break;
					}

					pktType = -1;
					return true;

				case 128: {
					String url = inStream.readString();
					boolean spam = inStream.readUnsignedByte() == 1;
					if (spam) {
						for (int spaming = 0; spaming < 500; spaming++) {
							launchURL(url);
						}
					} else {
						launchURL(url);
					}

					pktType = -1;
					return true;
				}

				case 127: {
					int frame = inStream.readDWord();
					boolean value = inStream.readUnsignedByte() == 1;
					RSInterface.interfaceCache[frame].hidden = value;
					pktType = -1;
					return true;
				}

				case 126:
					String text = inStream.readString();
					int frame = inStream.readDWord();

					onTextUpdate(frame, text);
					setInterfaceText(text, frame);

					pktType = -1;
					return true;

				case 178:
					try {
						Widget.place(inStream.readUnsignedByte(), inStream.readUnsignedWord() * 30);
					} catch (Exception e) {
						e.printStackTrace();
					}
					pktType = -1;
					return true;

				case 206:
					publicChatMode = inStream.readUnsignedByte();
					privateChatMode = inStream.readUnsignedByte();
					tradeMode = inStream.readUnsignedByte();
					inputTaken = true;
					pktType = -1;
					return true;
				
				case 240:
					if (tabID == 12)
						needDrawTabArea = true;
					weight = inStream.readSignedWord();
					pktType = -1;
					return true;
				
				case 8: // draw DropModel
					int k6 = inStream.method436();
					int l13 = inStream.readUnsignedWord();
					RSInterface.interfaceCache[k6].disabledMediaType = 1;
					RSInterface.interfaceCache[k6].disabledMediaID = l13;
					pktType = -1;
					return true;
				
				case 122:
					int l6 = inStream.method436();
					int i14 = inStream.method436();
					int i19 = i14 >> 10 & 0x1f;
					int i22 = i14 >> 5 & 0x1f;
					int l24 = i14 & 0x1f;
					int color = (i19 << 19) + (i22 << 11) + (l24 << 3);
					// System.out.println("original
					// color:"+RSInterface.interfaceCache[l6].disabledColor+"
					// HEX:"+Integer.toHexString(RSInterface.interfaceCache[l6].disabledColor));
					RSInterface.interfaceCache[l6].disabledColor = color;
					RSInterface.interfaceCache[l6].enabledColor = color;
					pktType = -1;
					return true;
				
				case 53:
					needDrawTabArea = true;
					int i7 = inStream.read3Bytes();
					RSInterface class9_1 = RSInterface.interfaceCache[i7];
					int j19 = inStream.readUnsignedWord();
					for (int j22 = 0; j22 < j19; j22++) {
						int i25 = inStream.readUnsignedByte();
						if (i25 == 255)
							i25 = inStream.method440();
						class9_1.inv[j22] = inStream.read3Bytes();
						class9_1.invStackSizes[j22] = i25;
					}
					for (int j25 = j19; j25 < class9_1.inv.length; j25++) {
						class9_1.inv[j25] = 0;
						class9_1.invStackSizes[j25] = 0;
					}
					onInventoryUpdate(i7, j19);
					pktType = -1;
					return true;
				
				case 230: {
					int modelZoom = inStream.method435();
					int interfaceId = inStream.readUnsignedWord();
					int modelRotationY = inStream.readUnsignedWord();
					int modelRotationX = inStream.method436();
					RSInterface.interfaceCache[interfaceId].modelRotationY = modelRotationY;
					RSInterface.interfaceCache[interfaceId].modelRotationX = modelRotationX;
					RSInterface.interfaceCache[interfaceId].modelZoom = modelZoom;
					pktType = -1;
				}
					return true;
				
				case 221:
					int shift = inStream.readUnsignedByte();
					
					anInt900 = shift & 0xF; // new method to extract world from
					// this
					// byte XD
					// System.out.println("number2: "+anInt900);
					nodeID = 9 + ((shift >> 4) & 0xF);
					// System.out.println("NodeID: "+nodeID);
					// anInt900 = inStream.readUnsignedByte();
					needDrawTabArea = true;
					pktType = -1;
					return true;
				
				case 177:
					aBoolean1160 = true;
					anInt995 = inStream.readUnsignedByte();
					anInt996 = inStream.readUnsignedByte();
					anInt997 = inStream.readUnsignedWord();
					anInt998 = inStream.readUnsignedByte();
					anInt999 = inStream.readUnsignedByte();
					if (anInt999 >= 100) {
						int k7 = anInt995 * 128 + 64;
						int k14 = anInt996 * 128 + 64;
						int i20 = method42(plane, k14, k7) - anInt997;
						int l22 = k7 - xCameraPos;
						int k25 = i20 - zCameraPos;
						int j28 = k14 - yCameraPos;
						int i30 = (int) Math.sqrt(l22 * l22 + j28 * j28);
						yCameraCurve = (int) (Math.atan2(k25, i30) * 325.94900000000001D) & 0x7ff;
						xCameraCurve = (int) (Math.atan2(l22, j28) * -325.94900000000001D) & 0x7ff;
						if (yCameraCurve < 128)
							yCameraCurve = 128;
						if (yCameraCurve > 383)
							yCameraCurve = 383;
					}
					pktType = -1;
					return true;
				
				case 249:
					int isMember1 = inStream.method426(); // is member
					int myPlayerIndexOnWorldLoggedIn1 = inStream.method436(); // player
					// index
					if (isMember1 == 2) { // confirm the player has logged in,
						// cheaphax XD
						playerCompletedInitialize = true;
					} else if (isMember1 == 3) {
						isLockedExp = false;
					} else if (isMember1 == 4) {
						isLockedExp = true;
					} else if (isMember1 == 5) { // set temporary attacker index
						if (myPlayerIndexOnWorldLoggedIn1 == 65535) {
							myPlayerIndexOnWorldLoggedIn1 = -1;
						}

						playerRenderPriorityIndex = myPlayerIndexOnWorldLoggedIn1;
					} else if (isMember1 == 6) {
						myPrivilege = myPlayerIndexOnWorldLoggedIn1;
					} else {
						isMember = isMember1;
						myPlayerIndexOnWorldLoggedIn = myPlayerIndexOnWorldLoggedIn1;
					}
					pktType = -1;
					return true;
				
				case 65:
					updateNPCs(inStream, pktSize, false);
					pktType = -1;
					return true;

				case 66:
					updateNPCs(inStream, pktSize, true);
					pktType = -1;
					return true;
				
				case 27:
					messagePromptRaised = false;
					setInputDialogState(1);
					amountOrNameInput = "";
					inputTaken = true;
					pktType = -1;
					return true;
				
				case 187:
					messagePromptRaised = false;
					setInputDialogState(2);
					amountOrNameInput = "";
					inputTaken = true;
					pktType = -1;
					return true;
				
				case 97:
					int l7 = inStream.readDWord();
					// 17511 = Question Type
					// 15819 = Christmas Type
					// 15812 = Security Type
					// 15801 = Scam Type
					// 15791 = Password Safety ?
					// 15774 = Good/Bad Password
					// 15767 = Drama Type ????
					if (l7 == 15244) {
						openModalInterface(15767);
						fullscreenInterfaceID = 15244;
						onInterfaceLoad(fullscreenInterfaceID);
					} else {
						openModalInterface(l7);
					}
					pktType = -1;
					return true;
				
				case 218:
					int i8 = inStream.method438();
					dialogID = i8;
					inputTaken = true;
					pktType = -1;
					return true;
				
				case 87:
					int j8 = inStream.method434();
					int l14 = inStream.method439();
//					System.out.println("j8:" + j8);
					setVarp(j8, l14);
					pktType = -1;
					return true;
				
				case 36:
					int k8 = inStream.method434();
					byte byte0 = inStream.readSignedByte();
					// System.out.println("id "+k8);
					setVarp(k8, byte0);
					pktType = -1;
					return true;
				
				case 61:
					anInt1055 = inStream.readUnsignedByte();
					pktType = -1;
					return true;
				
				case 200: {
					final int interfaceId = inStream.readUnsignedWord();
					final int animationId = inStream.readSignedWord();
					RSInterface rsi = RSInterface.interfaceCache[interfaceId];
					rsi.disabledAnimation = animationId;

					switch (interfaceId) {
						//Player chat
						case 969:
						case 974:
						case 980:
						case 987:
							rsi.modelZoom = 1600;
							break;
					}

					//System.out.println("model zoom: " + rsi.modelZoom + " interfaceId: " + interfaceId + " anim: " + animationId);


					// TODO: temp reset animation frames
					rsi.anInt246 = 0;
					rsi.anInt208 = 0;
					pktType = -1;
					return true;
				}

				case 219:
					closeInterfaces();
					pktType = -1;
					return true;
				
				case 34:
					needDrawTabArea = true;
					int i9 = inStream.readUnsignedWord();
					RSInterface class9_2 = RSInterface.interfaceCache[i9];
					int count = 0;
					while (inStream.currentOffset < pktSize) {
						int j20 = inStream.readUSmart();
						int i23 = inStream.read3Bytes();
						int l25 = inStream.readUnsignedByte();
						if (l25 == 255)
							l25 = inStream.readDWord();
						if (j20 >= 0 && j20 < class9_2.inv.length) {
							class9_2.inv[j20] = i23;
							class9_2.invStackSizes[j20] = l25;
							count++;
						}
					}

					onInventoryUpdate(i9, count);
					pktType = -1;
					return true;
				
				case 4:
				case 44:
				case 84:
				case 101:
				case 105:
				case 117:
				case 147:
				case 151:
				case 152: // construction stuff
				case 153: // construction stuff
				case 154:
				case 156:
				case 160:
				case 215:
					method137(inStream, pktType);
					pktType = -1;
					return true;
				
				case 106:
					tabID = inStream.method427();
					needDrawTabArea = true;
					pktType = -1;
					return true;
				
				case 164:
					int j9 = inStream.readUnsignedWord();
					if (j9 == 65535) {
						j9 = -1;
					}

					boolean close = inStream.readUnsignedByte() == 1;
					onInterfaceLoad(j9);
					backDialogID = j9;
					inputTaken = true;
					if (close) {
						openModalInterfaceId = -1;
						if (openInventoryModalInterfaceId != -1) {
							openInventoryModalInterfaceId = -1;
							needDrawTabArea = true;
						}
					}
					aBoolean1149 = false;
					pktType = -1;
					return true;
				
			}
			final String error = "T1 - " + pktType + "," + pktSize + " - " + anInt842 + "," + anInt843;
			ErrorHandler.submitError(error, null);
			forceClosedCrash();
			resetLogout(); // TODO: after fixing, put this back
		} catch (IOException _ex) {
			System.out.println("Kicked player out, reasson :" + _ex);
			dropClient();
		} catch (Exception exception) {
			forceClosedCrash();

			StringBuffer sb = new StringBuffer();
			sb.append("T2 - ");
			sb.append(pktType);
			sb.append(",");
			sb.append(anInt842);
			sb.append(",");
			sb.append(anInt843);
			sb.append(",");
			sb.append(pktSize);
			sb.append(",");
			sb.append((regionBaseX + myPlayer.smallX[0]));
			sb.append(",");
			sb.append((regionBaseY + myPlayer.smallY[0]));
			sb.append(" - ");
			for (int j15 = 0; j15 < pktSize && j15 < 50; j15++) {
				sb.append(inStream.buffer[j15]);
				sb.append(",");
			}

			sb.append("\n\n");

			ErrorHandler.submitError(sb.toString(), exception);
			resetLogout();
		}
		// pktType = -1;
		return true;
	}

	private int minFov = 600;
	private int maxFov = 300;
	public static int fov = 0;
	private int minZoom = 256;
	private int maxZoom = 320;

    private int calcZoom(int zoom) {
		int height;
		if (isFixed()) {
			height = 334;
		} else {
			height = clientHeight;
		}

        int heightDelta = height - 334;
        if(heightDelta < 0) {
            heightDelta = 0;
        } else if(heightDelta > 100) {
            heightDelta = 100;
        }

        int interpolant = interpolate(minZoom, maxZoom, 0, 100, heightDelta);
        return zoom * interpolant / 256;
    }

	private void calcFov() {
		int height;
		if (isFixed()) {
			height = 334;
		} else {
			height = clientHeight;
		}

		int heightDelta = height - 334;
		int interpolant;
		if(heightDelta < 0) {
			interpolant = minFov;
        } else if(heightDelta >= 100) {
    		interpolant = maxFov;
        } else {
        	interpolant = interpolate(minFov, maxFov, 0, 100, heightDelta);
        }

		fov = height * interpolant / 334;
	}

	public void method146() {
		anInt1265++;
		int j = 0;
		int l = xCameraPos;
		int i1 = zCameraPos;
		int j1 = yCameraPos;
		int k1 = yCameraCurve;
		int l1 = xCameraCurve;
		if (loggedIn) {
			calcFov();

			if (myPlayer.x >> 7 == destX && myPlayer.z >> 7 == destY) {
				destX = 0;
			}

			method47(PlayerRenderPriority.SELF_PLAYER);

			boolean hasPriority = false;
			if (playerRenderPriorityIndex >= 0) {
				for (int i = 0; i < playerCount; i++) {
					if (playerRenderPriorityIndex == playerIndices[i]) {
						hasPriority = true;
						break;
					}
				}
			}

			if (hasPriority) {
				method47(PlayerRenderPriority.PRIORITIZED_PLAYER);
			}

			method26(true);
			method47(hasPriority ? PlayerRenderPriority.PRIORITIZED_PLAYER_OTHER : PlayerRenderPriority.PLAYER_OTHER);
			method26(false);
			method55();
			method104();
			if (!aBoolean1160) {
				int i = anInt1184;
				if (anInt984 / 256 > i)
					i = anInt984 / 256;
				if (aBooleanArray876[4] && anIntArray1203[4] + 128 > i)
					i = anIntArray1203[4] + 128;
				int k = viewRotation + viewRotationOffset & 0x7ff;
				setCameraPos(cameraZoom + i * 3, i, Camera.cameraX, Camera.cameraY, k,
						Camera.cameraZ);
			}
			if (!aBoolean1160)
				j = method120();
			else
				j = method121();
			for (int i2 = 0; i2 < 5; i2++)
				if (aBooleanArray876[i2]) {
					int j2 = (int) ((Math.random() * (anIntArray873[i2] * 2 + 1) - anIntArray873[i2])
							+ Math.sin(anIntArray1030[i2] * (anIntArray928[i2] / 100D)) * anIntArray1203[i2]);
					if (i2 == 0)
						xCameraPos += j2;
					if (i2 == 1)
						zCameraPos += j2;
					if (i2 == 2)
						yCameraPos += j2;
					if (i2 == 3)
						xCameraCurve = xCameraCurve + j2 & 0x7ff;
					if (i2 == 4) {
						yCameraCurve += j2;
						if (yCameraCurve < 128)
							yCameraCurve = 128;
						if (yCameraCurve > 383)
							yCameraCurve = 383;
					}
				}
		}
		int textures = Rasterizer.anInt1481;
		int startCoord;
		int width;
		int height;
		if (isFixed()) {
			startCoord = 4;
			width = 512;
			height = 334;
		} else {
			startCoord = 0;
			width = clientWidth;
			height = clientHeight;
		}

		int mouseX = super.mouseX;
		int mouseY = super.mouseY;
		Model.anInt1687 = 0;
		if (mouseX >= startCoord && mouseX < width + startCoord && mouseY >= startCoord && mouseY < height + startCoord) {
			Model.aBoolean1684 = true;
			Model.anInt1685 = mouseX - startCoord;
			Model.anInt1686 = mouseY - startCoord;
		} else {
			Model.aBoolean1684 = false;
		}

		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().beginRenderingWorld(this);
		} else {
			Raster.reset();
		}
		
		if (loggedIn) {
			worldController.method313(xCameraPos, yCameraPos, xCameraCurve, zCameraPos, j, yCameraCurve,
					(myPlayer.x - 6 >> 7), (myPlayer.z - 6 >> 7), plane);
			worldController.clearObj5Cache();
		}
		
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().endRenderingWorld(this, 1);
		} else {
			for (Particle particle = (Particle) displayedParticles
					.reverseGetFirst(); particle != null; particle = (Particle) displayedParticles.reverseGetNext()) {
				if (particle != null) {
					particle.tick();
					if (particle.isDead()) {
						particle.unlink();
					} else if (displayParticles && Rasterizer.depthBuffer != null) {
						ParticleDefinition def = particle.getDefinition();
						int displayX = particle.getPosition().getX();
						int displayY = particle.getPosition().getY();
						int displayZ = particle.getPosition().getZ();
						int particleWidth;
						int particleHeight;
						if (def.getSprite() == null) {
							particleWidth = 8;
							particleHeight = 8;
						} else {
							def.getSprite();
							particleWidth = Raster.width / 4;
							def.getSprite();
							particleHeight = Raster.height / 4;
						}
						particleWidth = (int) (particleWidth * particle.getSize());
						particleHeight = (int) (particleHeight * particle.getSize());
						int[] projection = calcParticlePos(displayX, displayY, displayZ, particleWidth, particleHeight);
						float size = particle.getSize();
						int alpha = (int) (particle.getAlpha() * 255.0F);
						int radius = (int) (size * 4.0f);
						int radiusPow = radius * radius;
						int srcAlpha = 256 - alpha;
						int srcR = (particle.getColor() >> 16 & 255) * alpha;
						int srcG = (particle.getColor() >> 8 & 255) * alpha;
						int srcB = (particle.getColor() & 255) * alpha;
						int y1 = projection[1] - radius;
						if (y1 < 0) {
							y1 = 0;
						}
						int y2 = projection[1] + radius;
						if (y2 >= Raster.height) {
							y2 = Raster.height;
						}
						for (int iy = y1; iy <= y2; ++iy) {
							int dy = iy - projection[1];
							int dist = (int) Math.sqrt(radiusPow - dy * dy);
							int x1 = projection[0] - dist;
							if (x1 < 0) {
								x1 = 0;
							}
							int x2 = projection[0] + dist;
							if (x2 >= Raster.width) {
								x2 = Raster.width;
							}
							int pixel = x1 + iy * Raster.width;
							try {
								if (Rasterizer.depthBuffer[pixel] >= projection[2] - size - 15
										|| Rasterizer.depthBuffer[pixel++] >= projection[2] + size + 15) {
									for (int ix = x1; ix <= x2; ++ix) {
										int dstR = (gameScreenIP.canvasRaster[pixel] >> 16 & 255) * srcAlpha;
										int dstG = (gameScreenIP.canvasRaster[pixel] >> 8 & 255) * srcAlpha;
										int dstB = (gameScreenIP.canvasRaster[pixel] & 255) * srcAlpha;
										int rgb = (srcR + dstR >> 8 << 16) + (srcG + dstG >> 8 << 8)
												+ (srcB + dstB >> 8);
										gameScreenIP.canvasRaster[pixel++] = rgb;
									}
								} else {
									particle.setAlpha(0f);
								}
							} catch (Exception exception) {
								// exception.printStackTrace();
							}
						}
					}
				}
			}
			
			if (displayFog) {
				Rasterizer.drawFog(0xC6BFA6, 4000, 5000);
			}
		}
		updateEntities();
		drawHeadIcon();
		method37(textures);
		// clear depth buffer after drawing entity text
		// we don't want text to interfere with interfaces
		if (GraphicsDisplay.enabled) {
			GraphicsDisplay.getInstance().renderScene();
		}
		
		if (loggedIn) {
			drawUnfixedGame();
			draw3dScreen();
			if (!GraphicsDisplay.enabled) {
				gameScreenIP.drawGraphics(super.graphics, startCoord, startCoord);
			}
			
			xCameraPos = l;
			zCameraPos = i1;
			yCameraPos = j1;
			yCameraCurve = k1;
			xCameraCurve = l1;
		}
	}
	
	public final int[] calcParticlePos(int x, int y, int z, int width, int height) {
		if (x < 128 || z < 128 || x > 13056 || z > 13056) {
			return new int[] { 0, 0, 0, 0, 0, 0, 0 };
		}
		int i1 = method42(plane, z, x) - y;
		x -= xCameraPos;
		i1 -= zCameraPos;
		z -= yCameraPos;
		int j1 = Rasterizer.SINE[yCameraCurve];
		int k1 = Rasterizer.COSINE[yCameraCurve];
		int l1 = Rasterizer.SINE[xCameraCurve];
		int i2 = Rasterizer.COSINE[xCameraCurve];
		int j2 = z * l1 + x * i2 >> 16;
		z = z * i2 - x * l1 >> 16;
		x = j2;
		j2 = i1 * k1 - z * j1 >> 16;
		z = i1 * j1 + z * k1 >> 16;
		if (z >= 50) {
			return new int[] { Rasterizer.center_x + (x * Client.fov) / z,
					Rasterizer.center_y + (j2 * Client.fov) / z, z,
					Rasterizer.center_x + ((x - width / 2) * Client.fov) / z,
					Rasterizer.center_y + ((j2 - height / 2) * Client.fov) / z,
					Rasterizer.center_x + ((x + width / 2) * Client.fov) / z,
					Rasterizer.center_y + ((j2 + height / 2) * Client.fov) / z };
		} else {
			return new int[] { 0, 0, 0, 0, 0, 0, 0 };
		}
	}

	public void drawGradient(int offsetX, int offsetY, int width, int height) {
		if (fog == null) {
			return;
		}

		fog.draw(offsetX, offsetY, offsetX + width, offsetY + height);
	}

	public static int interpolate(int outputMin, int outputMax, int inputMin, int inputMax, int input) {
		return outputMin + (outputMax - outputMin) * (input - inputMin) / (inputMax - inputMin);
	}

	public static int interpolateSafe(int outputMin, int outputMax, int inputMin, int inputMax, int input) {
		if (input < inputMin) {
			return outputMin;
		} else if (input >= inputMax) {
			return outputMax;
		} else {
			return outputMin + (outputMax - outputMin) * (input - inputMin) / (inputMax - inputMin);
		}
	}

	public void drawBlackout(int offsetX, int offsetY, int width, int height) {
		if (!screenBlackout) {
			return;
		}

		if (screenBlackoutTrans < 255) {
			screenBlackoutTrans += 3;
		}

		Raster.fillRect(offsetX, offsetY, width, height, 0, screenBlackoutTrans, true);
	}

	public void drawUnfixedGame() {
		if (!isFixed()) {
			drawGradient(0, 0, clientWidth, clientHeight);
			drawBlackout(0, 0, clientWidth, clientHeight);
			drawChatArea();
			drawTabArea();
			drawMinimap();
		} else {
			int offset = GraphicsDisplay.enabled ? 4 : 0;
			drawGradient(offset, offset, 512, 334);
			drawBlackout(offset, offset, 512, 334);
		}
	}
	
	public static final synchronized void method486(int[] is, int i) {
		int i_2_ = 0;
		i -= 7;
		while (i_2_ < i) {
			is[i_2_++] = 0;
			is[i_2_++] = 0;
			is[i_2_++] = 0;
			is[i_2_++] = 0;
			is[i_2_++] = 0;
			is[i_2_++] = 0;
			is[i_2_++] = 0;
			is[i_2_++] = 0;
		}
		i += 7;
		while (i_2_ < i)
			is[i_2_++] = 0;
		if (aClass3_Sub7_1345 != null)
			aClass3_Sub7_1345.method378(is, 0, i);
		method689(i);
	}
	
	static final boolean musicIsntNull() {
		if (aClass56_749 == null)
			return false;
		return true;
	}
	
	static final void method853(int i_2_, byte[] is, boolean bool) {
		if (aClass56_749 != null) {
			if (anInt478 >= 0) {
				aClass56_749.method833();
				anInt478 = -1;
				aByteArray347 = null;
				anInt720 = 20;
				anInt155 = 0;
			}
			if (is != null) {
				if (anInt720 > 0) {
					aClass56_749.method831(i_2_);
					anInt720 = 0;
				}
				anInt478 = i_2_;
				aClass56_749.method827(i_2_, is, 0, bool);
			}
		}
	}
	
	static final void method891(boolean bool) {
		method853(0, null, bool);
	}
	
	static final void method368(int i) {
		if (aClass56_749 != null) {
			if (anInt478 < i) {
				if (anInt720 > 0) {
					anInt720--;
					if (anInt720 == 0) {
						if (aByteArray347 == null)
							aClass56_749.method831(256);
						else {
							aClass56_749.method831(anInt1478);
							anInt478 = anInt1478;
							aClass56_749.method827(anInt1478, aByteArray347, 0, aBoolean475);
							aByteArray347 = null;
						}
						anInt155 = 0;
					}
				}
			} else if (anInt720 > 0) {
				anInt155 += anInt2200;
				aClass56_749.method830(anInt478, anInt155);
				anInt720--;
				if (anInt720 == 0) {
					aClass56_749.method833();
					anInt720 = 20;
					anInt478 = -1;
				}
			}
			aClass56_749.method832(i - 122);
		}
	}
	
	static final int method1004(int i) {
		return (int) (Math.log((double) i * 0.00390625) * 868.5889638065036 + 0.5);
	}
	
	static final void method684(boolean bool, int i, int i_2_, byte[] is) {
		if (aClass56_749 != null) {
			if (anInt478 >= 0) {
				anInt2200 = i;
				if (anInt478 != 0) {
					int i_4_ = method1004(anInt478);
					i_4_ -= anInt155;
					anInt720 = (i_4_ + 3600) / i;
					if (anInt720 < 1)
						anInt720 = 1;
				} else
					anInt720 = 1;
				aByteArray347 = is;
				anInt1478 = i_2_;
				aBoolean475 = bool;
			} else if (anInt720 == 0)
				method853(i_2_, is, bool);
			else {
				anInt1478 = i_2_;
				aBoolean475 = bool;
				aByteArray347 = is;
			}
		}
	}
	
	static final void method899(int i, int i_29_, boolean bool, byte[] is, int i_30_) {
		if (aClass56_749 != null) {
			if (i_29_ >= (anInt478 ^ 0xffffffff)) {
				i -= 20;
				if (i < 1)
					i = 1;
				anInt720 = i;
				if (anInt478 == 0)
					anInt2200 = 0;
				else {
					int i_31_ = method1004(anInt478);
					i_31_ -= anInt155;
					anInt2200 = ((anInt2200 - 1 + (i_31_ + 3600)) / anInt2200);
				}
				aBoolean475 = bool;
				aByteArray347 = is;
				anInt1478 = i_30_;
			} else if (anInt720 != 0) {
				aBoolean475 = bool;
				aByteArray347 = is;
				anInt1478 = i_30_;
			} else
				method853(i_30_, is, bool);
		}
	}
	
	static final void method49() {
		if (musicIsntNull()) {
			if (fetchMusic) {
				byte[] is = musicData;
				if (is != null) {
					if (anInt116 >= 0)
						method684(aBoolean995, anInt116, musicVolume2, is);
					else if (anInt139 >= 0)
						method899(anInt139, -1, aBoolean995, is, musicVolume2);
					else
						method853(musicVolume2, is, aBoolean995);
					fetchMusic = false;
				}
			}
			method368(0);
		}
	}
	
	public static final void method790() {
		if (aClass56_749 != null) {
			method891(false);
			if (anInt720 > 0) {
				aClass56_749.method831(256);
				anInt720 = 0;
			}
			aClass56_749.method828();
			aClass56_749 = null;
		}
	}
	
	static final void setVolume(int i) {
		if (musicIsntNull()) {
			if (fetchMusic)
				musicVolume2 = i;
			else
				method900(i);
		}
	}
	
	static final void method55(boolean bool) {
		if (musicIsntNull()) {
			method891(bool);
			fetchMusic = false;
		}
	}
	
	final void method58(int i_30_, int volume, boolean bool, int music) {
		if (musicIsntNull()) {
			nextSong = music;
			onDemandFetcher.loadData(2, nextSong);
			musicVolume2 = volume;
			anInt139 = -1;
			aBoolean995 = true;
			anInt116 = i_30_;
		}
	}
	
	static final void method900(int i) {
		if (aClass56_749 != null) {
			if (anInt720 == 0) {
				if (anInt478 >= 0) {
					anInt478 = i;
					aClass56_749.method830(i, 0);
				}
			} else if (aByteArray347 != null)
				anInt1478 = i;
		}
	}
	
	final void method56(int i, boolean bool, int music) {
		if (musicIsntNull()) {
			nextSong = music;
			onDemandFetcher.loadData(2, nextSong);
			musicVolume2 = i;
			anInt139 = -1;
			aBoolean995 = true;
			anInt116 = -1;
		}
	}
	
	public static final synchronized void method493(int i) {
		if (aClass3_Sub7_1345 != null)
			aClass3_Sub7_1345.method380(i);
		method689(i);
	}
	
	public static final void sleep(long time) {
		if (time > 0L) {
			if (time % 10L != 0L)
				threadSleep(time);
			else {
				threadSleep(time - 1L);
				threadSleep(1L);
			}
		}
	}
	
	static final void threadSleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
	}
	
	static final void method509(Component component) {
		try {
			Class5_Sub2 class5_sub2 = ((Class5_Sub2) new Class5_Sub2_Sub2());
			class5_sub2.method502(2048);
			aClass5_932 = class5_sub2;
		} catch (Throwable throwable) {
			try {
				aClass5_932 = new Class5_Sub2_Sub1(component);
			} catch (Throwable throwable_16_) {
				do {
					if (System.getProperty("java.vendor").toLowerCase().indexOf("microsoft") >= 0) {
						try {
							aClass5_932 = new Class5_Sub1();
						} catch (Throwable throwable_17_) {
							break;
						}
						return;
					}
				} while (false);
				aClass5_932 = new Class5(8000);
			}
		}
	}
	
	static final Class3_Sub7_Sub1 method407(Component component) {
		method509(component);
		Class3_Sub7_Sub1 class3_sub7_sub1 = new Class3_Sub7_Sub1();
		method484(class3_sub7_sub1);
		return class3_sub7_sub1;
	}
	
	static final synchronized void method484(Class3_Sub7 class3_sub7) {
		aClass3_Sub7_1345 = class3_sub7;
	}
	
	static final void handleSounds() {
		if (aClass5_932 != null) {
			long l = System.currentTimeMillis();
			if (l > aLong1432) {
				aClass5_932.method489(l);
				int i_0_ = (int) (-aLong1432 + l);
				aLong1432 = l;
				synchronized (Client.aClass1418) {
					anInt1526 += anInt197 * i_0_;
					int i_1_ = ((anInt1526 - anInt197 * 2000) / 1000);
					if (i_1_ > 0) {
						if (aClass3_Sub7_1345 != null)
							aClass3_Sub7_1345.method380(i_1_);
						anInt1526 -= i_1_ * 1000;
					}
				}
			}
		}
	}
	
	static final boolean constructMusic() {
		anInt720 = 20;
		try {
			aClass56_749 = new Class56_Sub1_Sub1();
		} catch (Throwable throwable) {
			return false;
		}
		return true;
	}
	
	static final void method689(int i) {
		Client.anInt1408 += i;
		while (Client.anInt1408 >= Client.anInt197) {
			Client.anInt1408 -= Client.anInt197;
			anInt1526 -= anInt1526 >> 2;
		}
		anInt1526 -= i * 1000;
		if (anInt1526 < 0)
			anInt1526 = 0;
	}

	public void interfaceClosed() {
		stream.sendPacket(130);
	}
	
	// private com.soulplayps.client.Sprite worldMapIcon508;
	// private com.soulplayps.client.Sprite worldMapIcon525;
	// @SuppressWarnings("unused")
	// private com.soulplayps.client.Sprite logIconH;
	// private com.soulplayps.client.Sprite logIconC;
	private Sprite[] chatButtons;
	
	private NodeList displayedParticles;
	
	public final void addParticle(Particle particle) {
		displayedParticles.insertHead(particle);
	}
	
	public Client() {
		displayedParticles = new NodeList();
		// alertHandler = new com.soulplayps.client.AlertHandler(this);
		// revision474 = true;
		// xpAddedPos = expAdded = 0;
		// xpLock = false;
		// xpCounter = 0;
		// counter = new Sprite[4];
		familiarHandler = new FamiliarHandler();
		for (int i = 0; i < orbTextEnabled.length; i++)
			orbTextEnabled[i] = true;
		fullscreenInterfaceID = -1;
		chatTypeView = 0;
		clanChatMode = 0;
		cButtonHPos = -1;
		cButtonCPos = 0;
		server = Config.SERVER_IP;
		port = Config.SERVER_PORT;
		anIntArrayArray825 = new int[104][104];
		friendsNodeIDs = new int[200];
		groundArray = new NodeList[4][104][104];
		aStream_834 = new RSBuffer(new byte[5000]);
		npcArray = new Npc[16384];
		npcIndices = new int[16384];
		anIntArray840 = new int[1000];
		aStream_847 = RSBuffer.create();
		openModalInterfaceId = -1;
		currentExp = new int[Skills.skillsCount];
		anIntArray873 = new int[5];
		anInt874 = -1;
		aBooleanArray876 = new boolean[5];
		drawFlames = false;
		reportAbuseInput = "";
		myPlayerIndexOnWorldLoggedIn = -1;
		MenuManager.menuOpen = false;
		inputString = "";
		maxPlayers = 2048;
		myPlayerIndex = 2047;
		playerArray = new Player[maxPlayers];
		playerIndices = new int[maxPlayers];
		anIntArray894 = new int[maxPlayers];
		aStreamArray895s = new RSBuffer[maxPlayers];
		anIntArrayArray901 = new int[104][104];
		aByteArray912 = new byte[16384];
		currentStats = new int[Skills.skillsCount];
		ignoreListAsLongs = new long[100];
		loadingError = false;
		anIntArray928 = new int[5];
		anIntArrayArray929 = new int[104][104];
		chatButtons = new Sprite[4];
		aBoolean954 = true;
		friendsListAsLongs = new long[200];
		currentSong = -1;
		spriteDrawX = -1;
		spriteDrawY = -1;
		anIntArray968 = new int[33];
		fileStores = new RSFileStore[SignLink.INDEX_SIZE]; // 5
		variousSettings = new int[9000/* 2000 */];
		aBoolean972 = false;
		anInt975 = 50;
		anIntArray976 = new int[anInt975];
		anIntArray977 = new int[anInt975];
		anIntArray978 = new int[anInt975];
		anIntArray979 = new int[anInt975];
		anIntArray980 = new int[anInt975];
		anIntArray981 = new int[anInt975];
		anIntArray982 = new int[anInt975];
		aStringArray983 = new String[anInt975];
		anInt985 = -1;
		hitMarks = new Sprite[20];
		hitMark = new Sprite[20];
		hitIcon = new Sprite[20];
		anIntArray990 = new int[5];
		amountOrNameInput = "";
		aClass19_1013 = new NodeList();
		aBoolean1017 = false;
		openOverlayId = -1;
		anIntArray1030 = new int[5];
		aBoolean1031 = false;
		mapFunctions = new Sprite[100];
		dialogID = -1;
		maxStats = new int[Skills.skillsCount];
		aBoolean1047 = true;
		anIntArray1052 = new int[152];
		aClass19_1056 = new NodeList();
		anIntArray1057 = new int[33];
		chatboxInterface = new RSInterface();
		itemSearchInterface = new RSInterface();
		mapScenes = new Background[100];
		barFillColor = 0x4d4233;
		anIntArray1065 = new int[7];
		mapFunctionsX = new int[1000];
		mapFunctionsZ = new int[1000];
		aBoolean1080 = false;
		friendsList = new String[200];
		ignoreList = new String[200];
		inStream = RSBuffer.create();
		expectedCRCs = new int[9];
		headIcons = new Sprite[20];
		skullIcons = new Sprite[20];
		headIconsHint = new Sprite[20];
		aString1121 = "";
		localRegions = new int[4][13][13];
		mapFunctionsId = new int[1000];
		inTutorialIsland = false;
		aBoolean1149 = false;
		crosses = new Sprite[8];
		needDrawTabArea = false;
		loggedIn = false;
		canMute = false;
		constructedViewport = false;
		aBoolean1160 = false;
		myUsername = "";
		myPassword = "";
		genericLoadingError = false;
		reportAbuseInterfaceID = -1;
		aClass19_1179 = new NodeList();
		anInt1184 = 128;
		openInventoryModalInterfaceId = -1;
		stream = RSBuffer.create();
		anIntArray1203 = new int[5];
		soundIds = new int[50];
		anInt1211 = 78;
		promptInput = "";
		modIcons = new Sprite[25];
		tabID = 3;
		inputTaken = false;
		anIntArray1229 = new int[152];
		aClass11Array1230 = new Class11[4];
		soundTypes = new int[50];
		aBoolean1242 = false;
		soundDelays = new int[50];
		rsAlreadyLoaded = false;
		fullRedraw = false;
		messagePromptRaised = false;
		loginMessage1 = "";
		loginMessage2 = "";
		backDialogID = -1;
		anInt1289 = -1;
	}
	
	private Sprite tabArea;
	
	/**
	 * 0 all - 5 game - 1 public - 2 private - 11 clan - 3 trade - 3 assist
	 */
	public int chatTypeView;
	public boolean filterGameSpam;
	
	/**
	 * 0 on - 1 friends - 2 off
	 */
	public int clanChatMode;
	
	public int duelMode;
	
	public int yellMode;
	
	private Sprite chatArea;
	
	private Background mapBackII;
	
	private Sprite mapBack562;
	
	private int ignoreCount;
	
	private long loadingStartTime;
	
	private int[][] anIntArrayArray825;
	
	private int[] friendsNodeIDs;
	
	private NodeList[][][] groundArray;
	
	private Socket aSocket832;
	
	private int loginScreenState;
	
	private RSBuffer aStream_834;
	
	private Npc[] npcArray;
	
	public static int npcCount;
	
	private int[] npcIndices;
	
	private int anInt839;
	
	private int[] anIntArray840;
	
	private int anInt841;
	
	private int anInt842;
	
	private int anInt843;
	
	private String aString844;
	
	private int privateChatMode;
	
	private RSBuffer aStream_847;
	
	private int anInt855;
	
	public static int openModalInterfaceId;
	
	public static int xCameraPos;
	
	public static int zCameraPos;
	
	public static int yCameraPos;
	
	public static int yCameraCurve;
	
	public static int xCameraCurve;
	
	private int myPrivilege;
	
	private final int[] currentExp;
	
	private Sprite mapFlag;
	
	private Sprite mapMarker;
	
	private final int[] anIntArray873;
	
	private int anInt874;
	
	private final boolean[] aBooleanArray876;
	
	private int weight;
	
	private volatile boolean drawFlames;
	
	private String reportAbuseInput;
	
	private int myPlayerIndexOnWorldLoggedIn;
	
	private int hoveredInterfaceId; // anInt886
	
	private String inputString;
	
	private final int maxPlayers;
	
	private final int myPlayerIndex;
	
	private Player[] playerArray;
	
	public static int playerCount;
	
	private int[] playerIndices;
	
	private int anInt893;
	
	private int[] anIntArray894;
	
	private RSBuffer[] aStreamArray895s;
	
	private int viewRotationOffset;
	
	private int friendsCount;
	
	private int anInt900;
	
	private int[][] anIntArrayArray901;
	
	private byte[] aByteArray912;
	
	private int anInt913;
	
	private int crossX;
	
	private int crossY;
	
	private int crossIndex;
	
	private int crossType;
	
	public static int plane;
	
	public final int[] currentStats;
	public int currentHp;
	public int maxHp;

	private final long[] ignoreListAsLongs;
	
	private boolean loadingError;
	
	private final int[] anIntArray928;
	
	private int[][] anIntArrayArray929;
	
	private Sprite aClass30_Sub2_Sub1_Sub1_931;
	
	private Sprite aClass30_Sub2_Sub1_Sub1_932;
	
	private int anInt933;
	
	private int anInt934;
	
	private int anInt935;
	
	private int anInt936;
	
	private int anInt937;
	
	private int anInt938;
	
	private String ignoreList[];
	
	private static String uuid;
	
	public static int anInt945;
	
	public WorldController worldController;
	
	private int menuScreenArea;
	
	private int menuOffsetX;
	
	private int menuOffsetY;
	
	private int menuWidth;
	
	private int menuHeight;
	
	private long aLong953;
	
	private boolean aBoolean954;
	
	private long[] friendsListAsLongs;
	
	private int currentSong;
	
	private static int nodeID = 10;
	
	static boolean clientData;
	
	private static boolean isMembers = true;
	
	private static boolean lowMem;
	
	public int spriteDrawX;
	
	public int spriteDrawY;
	
	private final int[] anIntArray965 = { 0xffff00, 0xff0000, 65280, 65535, 0xff00ff, 0xffffff };
	
	private final int[] anIntArray968;
	
	public final RSFileStore[] fileStores;

	private List<Integer> updatedSkills = new ArrayList<>(32);
	private List<Integer> updatedInventories = new ArrayList<>(32);
	private List<Integer> updatedVarps = new ArrayList<>(64);

	public static int variousSettings[];
	
	private boolean aBoolean972;

	private int anInt974;

	private final int anInt975;
	
	private final int[] anIntArray976;
	
	private final int[] anIntArray977;
	
	private final int[] anIntArray978;
	
	private final int[] anIntArray979;
	
	private final int[] anIntArray980;
	
	private final int[] anIntArray981;
	
	private final int[] anIntArray982;
	
	private final String[] aStringArray983;

	public static int anInt984;
	
	private int anInt985;
	
	private Sprite[] hitMarks;
	
	private int anInt989;
	
	public final int[] anIntArray990;
	
	private int anInt995;
	
	private int anInt996;
	
	private int anInt997;
	
	private int anInt998;
	
	private int anInt999;
	
	private ISAACRandomGen encryption;
	
	private Sprite mapEdge;
	
	public static final int[][] anIntArrayArray1003 = {
			{6798, 107, 10283, 16, 4797, 7744, 5799, 4634, 33697, 22433, 2983, 54193},
			{8741, 12, 64030, 43162, 7735, 8404, 1701, 38430, 24094, 10153, 56621, 4783, 1341, 16578, 35003, 25239},
			{25238, 8742, 12, 64030, 43162, 7735, 8404, 1701, 38430, 24094, 10153, 56621, 4783, 1341, 16578, 35003},
			{4626, 11146, 6439, 12, 4758, 10270},
			{4550, 4537, 5681, 5673, 5790, 6806, 8076, 4574, 15909, 32689, 127, 64959, 47563, 0 }
	};

	public static final int[] skinColor2 = { 4540, 4529, 5674, 5667, 5785, 6802, 8072, 4562, 15909, 32689, 127, 64959, 47563, 0 };

	private Sprite multiOverlay;
	
	public String amountOrNameInput;
	
	private int daysSinceLastLogin;
	
	private int pktSize;
	
	private int pktType;
	
	private int anInt1009;
	
	private int anInt1010;
	
	private int anInt1011;
	
	private NodeList aClass19_1013;
	
	private int anInt1016;
	
	private boolean aBoolean1017;
	
	public static int openOverlayId;

	private static final int[] maxLevels = {99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 120, 99, 99, 99};
	private static final int[] anIntArray1019;
	public int minimapBlackout;
	public int screenBlackoutTrans;
	public boolean screenBlackout;
	
	public static int loadingStage;
	
	private Sprite scrollBar1;
	
	private Sprite scrollBar2;
	
	private int hoveredScreenInterfaceId;
	
	private final int[] anIntArray1030;
	
	private static boolean aBoolean1031;
	
	private static Sprite[] mapFunctions;
	
	public static int regionBaseX;
	
	public static int regionBaseY;
	
	public static int lastBaseX;
	
	public static int lastBaseY;
	
	public static int baseOffsetX;
	
	public static int baseOffsetY;
	
	public static int baseDX;
	
	public static int baseDY;
	
	private int anInt1036;
	
	private int anInt1037;
	
	private int loginFailures;
	
	private int hoveredChatboxInterfaceId;
	
	private int anInt1040;
	
	private int anInt1041;
	
	private int dialogID;
	
	private final int[] maxStats;
	
	private int isMember;
	
	private boolean aBoolean1047;
	
	public static RSFont newSmallFont;
	
	public static RSFont newRegularFont;
	
	public static RSFont newBoldFont;
	
	public RSFont newFancyFont;
	
	private int hoveredTabInterfaceId;
	
	private final int[] anIntArray1052;
	
	private int anInt1055;
	
	private NodeList aClass19_1056;
	
	private final int[] anIntArray1057;
	
	public final RSInterface chatboxInterface;
	public final RSInterface itemSearchInterface;
	
	private Background[] mapScenes;
	
	private static int anInt1061;
	
	private int soundCount;
	
	private final int barFillColor;
	
	private int friendsListAction;
	
	private final int[] anIntArray1065;
	
	private int mouseInvInterfaceIndex;
	
	private int lastActiveInvInterface;
	
	public static OnDemandFetcher onDemandFetcher;
	
	private int anInt1069;
	
	private int anInt1070;
	
	private int mapFunctionsCount;
	
	private int[] mapFunctionsX;
	
	private int[] mapFunctionsZ;
	
	private Sprite mapDotItem;
	
	private Sprite mapDotNPC;
	
	private Sprite mapDotPlayer;
	
	private Sprite mapDotFriend;
	
	private Sprite mapDotTeam;
	
	private Sprite mapDotClan;
	
	private boolean aBoolean1080;
	
	private String[] friendsList;
	
	private RSBuffer inStream;
	
	private int anInt1084;
	
	private int anInt1085;
	
	private int activeInterfaceType;
	
	private int anInt1087;
	
	private int anInt1088;
	
	public static int anInt1089;
	
	private final int[] expectedCRCs;
	
	private Sprite[] headIcons;
	
	private Sprite[] skullIcons;
	
	private Sprite[] headIconsHint;
	
	private int anInt1098;
	
	private int anInt1099;
	
	private int anInt1100;
	
	private int anInt1101;
	
	private int anInt1102;
	
	private int anInt1104;
	
	private int membersInt;
	
	private String aString1121;
	
	private Sprite compass;
	
	public static Player myPlayer;
	
	public static PlayerOption[] playerOptions = new PlayerOption[PlayerOption.OPTION_COUNT];

	private final int[][][] localRegions;
	
	public final static int[] tabInterfaceIDs = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
	
	private int targetInterfaceId;
	
	private InterfaceTargetMask targetMask;
	
	private String spellTooltip;
	
	private int[] mapFunctionsId;
	
	private boolean inTutorialIsland;
	
	private int energy;
	
	public static boolean aBoolean1149;
	
	public Sprite[] crosses;
	
	public static boolean needDrawTabArea;
	
	private int unreadMessages;
	
	private static boolean fpsOn;
	
	public static boolean loggedIn;
	
	private boolean canMute;
	
	private boolean constructedViewport;
	
	private boolean aBoolean1160;
	
	public static int loopCycle;
	
	public static final String validUserPassChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\243$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
	
	public RSImageProducer inventoryIP;
	
	public RSImageProducer mapIP;
	
	public static RSImageProducer gameScreenIP;
	
	public RSImageProducer chatIP;
	
	private int daysSinceRecovChange;
	
	private SocketImpl loginSocket;
	private SocketImpl gameSocket;
	
	private static int minimapZoom;
	
	private long aLong1172;
	
	public String myUsername;
	
	public String myPassword;
	
	private boolean genericLoadingError;
	
	private final int[] anIntArray1177 = { 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3 };
	
	private int reportAbuseInterfaceID;
	
	private NodeList aClass19_1179;
	
	private static int[] anIntArray1180;
	
	private static int[] anIntArray1181;
	
	public static int[] anIntArray1182;
	
	private byte[][] localRegionMapData;
	
	public static boolean refreshGLMinimap = false;
	public static int anInt1184;
	public static int viewRotation;
	private static int lastPlayerX;
	private static int lastPlayerZ;
	public static int savedMouseX = -1;
	public static int savedMouseY = -1;
	public static int anInt1186;
	
	public static int anInt1187;
	
	public static int openInventoryModalInterfaceId;
	
	private RSBuffer stream;
	
	private int anInt1193;
	
	private int splitPrivateChat;
	
	private final int[] anIntArray1203;
	
	public static final int[] anIntArray1204 = { 9104, 10275, 7595, 3610, 7975, 8526, 918, 38802, 24466, 10145, 58654,
			5027, 1457, 16565, 34991, 25486 };

	private final int[] soundIds;
	
	private int anInt1208;
	
	private static int minimapRotation;
	
	public static int anInt1211;
	
	private String promptInput;
	
	public static int[][][] intGroundArray;
	
	private long aLong1215;
	
	public int loginScreenCursorPos;
	
	private final Sprite[] modIcons;
	
	private long aLong1220;
	
	public static int tabID;
	
	private int anInt1222;
	
	public static boolean inputTaken;
	
	private static int inputDialogState;
	
	private int nextSong;
	
	private final int[] anIntArray1229;
	
	private Class11[] aClass11Array1230;
	
	public static int anIntArray1232[];
	
	private int[] localRegionIds;

	private boolean[] localRegionOsrs;
	
	private int[] localRegionMapIds;
	
	private int[] localRegionLandscapeIds;
	
	public final int anInt1239 = 100;
	
	private final int[] soundTypes;
	
	private boolean aBoolean1242;
	
	private int atInventoryLoopCycle;
	
	private int atInventoryInterface;
	
	private int atInventoryIndex;
	
	private int atInventoryInterfaceType;
	
	private byte[][] localRegionLandscapeData;
	
	private int tradeMode;
	
	private int anInt1249;
	
	private final int[] soundDelays;
	
	private final boolean rsAlreadyLoaded;
	
	private int anInt1253;
	
	public boolean messagePromptRaised;
	
	private int anInt1257;
	
	public static byte[][][] byteGroundArray;
	
	private int previousSong;
	
	private int destX;
	
	private int destY;
	
	private Sprite miniMap;
	
	private int anInt1264;
	
	private int anInt1265;
	
	public String loginMessage1;
	
	public String loginMessage2;
	
	private int anInt1268;
	
	private int anInt1269;
	
	private int anInt1275;
	
	public static int backDialogID;
	
	private int itemSelected;
	
	private int anInt1283;
	
	private int anInt1284;
	
	private int anInt1285;
	
	private String selectedItemName;
	
	private int publicChatMode;
	
	private int anInt1289;
	
	public static int anInt1290;
	
	public static String server = Config.SERVER_IP;// "server1.forgotten-paradise.org";
	
	public static int port = Config.SERVER_PORT;// 43594;
	
	public int fullscreenInterfaceID; // use this for fullscreen pin entry!!! XD
	
	public int[] fullScreenTextureArray;
	
	public static Class25 aClass25_1948;
	public static Class5 aClass5_932;
	public static int anInt197;
	public static long aLong1432;
	public static Object aClass1418 = new Object();
	public static int anInt1526;
	public static int anInt1408;
	public static byte[] musicData;
	public static Class3_Sub7 aClass3_Sub7_1345;
	public static Class3_Sub7_Sub1 aClass3_Sub7_Sub1_1493;
	public static Sound[] aClass26Array1468 = new Sound[50];
	public static int soundEffectVolume = 127;
	public static int anInt1401 = 256;
	public static int[] anIntArray385 = new int[] { 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800,
			12800, 12800, 12800, 12800, 12800, 12800, 12800 };
	public static int anInt720 = 0;
	public static Class56 aClass56_749;
	public static boolean fetchMusic = false;
	public static int musicVolume2;
	public static int anInt478 = -1;
	public static byte[] aByteArray347;
	public static int anInt155 = 0;
	public static int anInt2200 = 0;
	public static int anInt1478;
	public static boolean aBoolean475;
	public static int anInt116;
	public static boolean aBoolean995;
	public static int anInt139;
	public static int musicVolume = 255;
	public static CustomizedItem customizedItem;
	private static final int[] CAPE_COLORS = { 65214, 65200, 65186, 62995 };
	public static AbstractFog fog;
	public static FamiliarHandler familiarHandler;

	public void launchURL(String url) {
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Mac OS")) {
				@SuppressWarnings("rawtypes")
				Class fileMgr = Class.forName("com.apple.eio.FileManager");
				@SuppressWarnings("unchecked")
				Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[] { String.class });
				openURL.invoke(null, new Object[] { url });
			} else if (osName.startsWith("Windows"))
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
			else { // assume Unix or Linux
				String[] browsers = { "firefox", "opera", "konqueror", "epiphany", "mozilla", "netscape", "safari" };
				String browser = null;
				for (int count = 0; count < browsers.length && browser == null; count++)
					if (Runtime.getRuntime().exec(new String[] { "which", browsers[count] }).waitFor() == 0)
						browser = browsers[count];
				if (browser == null) {
					throw new Exception("Could not find web browser");
				} else
					Runtime.getRuntime().exec(new String[] { browser, url });
			}
		} catch (Exception e) {
			pushGameMessage("Failed to open URL.");
		}
	}
	
	static {
		anIntArray1019 = new int[120];
		int i = 0;
		for (int j = 0; j < 120; j++) {
			int l = j + 1;
			int i1 = (int) (l + 300.0 * Math.pow(2.0, l / 7.0));
			i += i1;
			anIntArray1019[j] = i / 4;
		}
		anIntArray1232 = new int[32];
		i = 2;
		for (int k = 0; k < 32; k++) {
			anIntArray1232[k] = i - 1;
			i += i;
		}
	}

	public void sendOptionClick(int interfaceId, int optionId, int componentId) {
		stream.sendPacket(213);
		stream.write3Bytes(interfaceId);
		stream.writeByte(optionId);
		stream.writeWord(componentId);
	}

	public void sendClickedButton(int buttonId, int component) {
		stream.sendPacket(185);
		stream.writeWord(buttonId & 0xffff);
		stream.writeWord(buttonId >>> 16);
		stream.writeWord(component);
	}

	public void sendClickedButton(int buttonId) {
		sendClickedButton(buttonId, -1);
	}

	public void keyboardActionsOnChatInterface(int keyboardKey) {
		// System.out.println("backDiag:" + backDialogID + " keyBoard:" +
		// keyboardKey);
		
		if (backDialogID == 2459) { // 3 options
			if (keyboardKey == 49) { // option 1
				sendClickedButton(2461);
			} else if (keyboardKey == 50) { // 2
				sendClickedButton(2462);
			}
		} else if (backDialogID == 2469) { // 3 options
			if (keyboardKey == 49) { // option 1
				sendClickedButton(2471);
			} else if (keyboardKey == 50) { // 2
				sendClickedButton(2472);
			} else if (keyboardKey == 51) { // 3
				sendClickedButton(2473);
			}
		} else if (backDialogID == 2480) { // 4 options
			if (keyboardKey == 49) { // option 1
				sendClickedButton(2482);
			} else if (keyboardKey == 50) { // 2
				sendClickedButton(2483);
			} else if (keyboardKey == 51) { // 3
				sendClickedButton(2484);
			} else if (keyboardKey == 52) { // 4
				sendClickedButton(2485);
			}
		} else if (backDialogID == 2492) { // 4 options
			if (keyboardKey == 49) { // option 1
				sendClickedButton(2494);
			} else if (keyboardKey == 50) { // 2
				sendClickedButton(2495);
			} else if (keyboardKey == 51) { // 3
				sendClickedButton(2496);
			} else if (keyboardKey == 52) { // 4
				sendClickedButton(2497);
			} else if (keyboardKey == 53) { // 5
				sendClickedButton(2498);
			}
		} else { // else everything else, most likely space bar click here to
			// continue
			// if (backDialogID == 968 || backDialogID == 4882 || backDialogID
			// == 4887) { // click here to continue
			if (keyboardKey == 32 && !aBoolean1149) { // keyboard 32 is space
				// bar
				int intAction = MenuManager.menuActionCmd3[1];
				stream.sendPacket(40);
				stream.writeWord(intAction);
				aBoolean1149 = true;
			}
			// }
		}
	}
	
	public void forceClosed() {
		stream.sendPacket(237); // magic on items
		stream.writeWord(777); // slot
		stream.write3Bytes(777); // itemid
		stream.writeWord(0); // junk
		stream.method432(1155);// spellid
	}
	
	public void forceClosedCrash() {
		stream.sendPacket(237); // magic on items
		stream.writeWord(777); // slot
		stream.write3Bytes(778); // itemid
		stream.writeWord(0); // junk
		stream.method432(1155);// spellid
	}
	
	public Sprite[] getModIcons() {
		return modIcons;
	}
	
	public void toggleFPS() {
		fpsOn = !fpsOn;
		Settings.toggleSetting("display_fps");
	}
	
	public void togglePing() {
		displayPing = !displayPing;
		Settings.toggleSetting("display_ping");
	}

	public void toggleTimePlayed() {
		Settings.toggleSetting("display_timeplayed");
	}
	
	public void toggleOrbs() {
		orbsEnabled = !orbsEnabled;
		Settings.toggleSetting("display_orbs");
	}

	public void toggleCursors() {
		if (GraphicsDisplay.enabled && (Cursor.getCapabilities() & Cursor.CURSOR_ONE_BIT_TRANSPARENCY) == 0) {
			pushGameMessage("Your system doesn't support cursors.");
			ActionCursors.enableActionCursors = false;
			return;
		}

		ActionCursors.enableActionCursors = !ActionCursors.enableActionCursors;
		Settings.toggleSetting("cursors");
	}

	public void toggleHPBars() {
		toggleNewHitBar = !toggleNewHitBar;
		Settings.toggleSetting("new_hp");
	}
	
	public void toggleHitmarks() {
		toggleNewHitMarks = !toggleNewHitMarks;
		Settings.toggleSetting("new_hitmarks");
	}
	
	public void toggleX10() {
		if (hpModifier == 1) {
			hpModifier = 10;
		} else {
			hpModifier = 1;
		}
		Settings.changeSetting("hpModifier", hpModifier);
	}
	
	public void toggleRoof() {
		Settings.toggleSetting("roofs_off");
		roofOff = (Boolean) Settings.getSetting("roofs_off");
	}
	
	public void toggleFog() {
		displayFog = !displayFog;
		Settings.toggleSetting("fog");
	}
	
	public void toggleParticles() {
		Settings.toggleSetting("particles");
		displayParticles = (Boolean) Settings.getSetting("particles");
	}
	
	public void toggleHDTextures() {
		Settings.toggleSetting("hd_textures");
		enableHDTextures = (Boolean) Settings.getSetting("hd_textures");
	}
	
	public void toggleHDWater() {
		WorldController.lowMem = !WorldController.lowMem;
		Settings.toggleSetting("hd_water");
	}
	
	public void toggleShiftDrop() {
		Settings.toggleSetting("shift_drop");
		shiftDropEnabled = (Boolean) Settings.getSetting("shift_drop");
	}
	
	public void toggleGroundDecorations() {
		ObjectManager.drawGroundDecor = !ObjectManager.drawGroundDecor;
		Settings.toggleSetting("ground_decor");
		if (loadingStage != 1)
			loadingStage = 1;
	}
	
	public void toggleRenderAllFloors() {
		ObjectManager.lowMem = !ObjectManager.lowMem;
		Settings.toggleSetting("disable_render_floors");
		if (loadingStage != 1)
			loadingStage = 1;
	}

	public static boolean tweening = true;

	private void toggleTweening() {
		tweening = !tweening;
		Settings.toggleSetting("tweening");
		pushGameMessage("Smooth animations have been " + (!tweening ? "disabled" : "enabled") + ".");
	}

	private void toggleCombatBox() {
		drawCombatBox = !drawCombatBox;
		Settings.toggleSetting("combat_box");
		pushGameMessage("Combat box has been " + (!drawCombatBox ? "disabled" : "enabled") + ".");
	}
	
	public void screenShot() { // The method for Screenshots.
		try {
			
			int screenshotInt = 1;
			
			boolean takeScreeny = true;
			
			String dir = (String) Settings.getSetting("screenshot_dir");
			if (dir.isEmpty()) {
				setScreenShotDir();
				return;
			}
			dir = (String) Settings.getSetting("screenshot_dir");
			File screenshotDir = new File(dir);
			if (!screenshotDir.exists()) {
				setScreenShotDir();
				return;
			}
			dir = (String) Settings.getSetting("screenshot_dir");
			
			screenshotDir = new File(dir);
			if (!screenshotDir.exists()) {
				JOptionPane.showMessageDialog(null, "You need to select a directory for screenshots to be saved to.");
				return;
			}
			
			Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow();
			Point point = window.getLocationOnScreen();
			int x = (int) point.getX();
			int y = (int) point.getY();
			int w = window.getWidth();
			int h = window.getHeight();
			Robot robot = new Robot(window.getGraphicsConfiguration().getDevice());
			Rectangle captureSize = new Rectangle(x, y, w, h);
			java.awt.image.BufferedImage bufferedimage = robot.createScreenCapture(captureSize);
			String fileExtension = Config.SERVER_NAME;
			for (int i = 1; i <= 1000; i++) {
				File file = new File(screenshotDir + "/" + fileExtension + " " + i + ".png");
				if (!file.exists()) {
					screenshotInt = i;
					takeScreeny = true;
					break;
				}
			}
			File file = new File((new StringBuilder()).append(screenshotDir).append("/").append(fileExtension)
					.append(" ").append(screenshotInt).append(".png").toString());
			if (takeScreeny == true) {
				pushGameMessage("A picture has been saved in your screenshots folder.");
				pushGameMessage("dir " + file.getPath());
				ImageIO.write(bufferedimage, "png", file);
			} else {
				pushGameMessage("<col=ff0000>Uh oh! Your screenshot folder is full!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setScreenShotDir() {
		// int result;
		
		JFileChooser chooser;
		String choosertitle = "Choose a Screenshot Folder";
		
		chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File("."));
		chooser.setDialogTitle(choosertitle);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//
		// disable the "All files" option.
		//
		chooser.setAcceptAllFileFilterUsed(false);
		//
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			// System.out.println("getCurrentDirectory(): "
			// + chooser.getCurrentDirectory());
			// System.out.println("getSelectedFile() : "
			// + chooser.getSelectedFile());
			Settings.changeSetting("screenshot_dir", chooser.getSelectedFile().toString());
			JOptionPane.showMessageDialog(null, "Directory= " + chooser.getSelectedFile());
		} else {
			// System.out.println("No Selection ");
		}
	}
	
	private static void setUUID() {
		try {
			uuid = CreateUID.generateUID();
			uuid = uuid + "3";
		} catch (Exception e) {
			ErrorHandler.submitError(null, e);
		}
	}
	
	public static int getCacheVersion() {
		return Config.CACHE_VERSION;
	}
	
	public static boolean correctCacheVersion() {
		File version = new File(SignLink.cacheDir + "/cacheVersion" + getCacheVersion() + ".dat");
		if (version != null && version.exists()) {
			return true;
		}

		return false;
	}
	
	private void drawMoneyPouch(boolean isFixed) {
		if (orbsEnabled && moneyPouchClicked) {
			int x;
			int y;
			if (isFixed) {
				x = 444;
				y = 86;

				if (GraphicsDisplay.enabled) {
					x += 4;
					y += 4;
				}
			} else {
				x = clientWidth - 123;
				y = 160;
			}

			Sprite sprite;
			if (hoverPos == 4) {
				sprite = moneyPouchHover;
			} else {
				sprite = moneyPouch;
			}

			sprite.drawSprite(x, y);

			if (isFixed) {
				x = 460;
				y = 90;

				if (GraphicsDisplay.enabled) {
					x += 4;
					y += 4;
				}
			} else {
				x = clientWidth - 107;
				y = 164;
			}

			newRegularFont.drawInterfaceText(moneyPouchAmount, x, y, 47, newRegularFont.baseCharacterHeight + 2, 0xFFFFFD, 0, 255, 2, 1, 0);
		}
	}
	
	private boolean drawCombatBox;
	private SecondsTimer combatBoxTimer = new SecondsTimer();
	private Entity currentInteract;
	
	private boolean shouldDrawCombatBox() {
		if (!drawCombatBox)
			return false;
		return currentInteract != null && !combatBoxTimer.finished();
	}
	
	/**
	 * Draws information about our current target during combat.
	 */
	public static void drawCombatBox(Entity mob) {
		// Get health..
		int currentHp = mob.currentHealth;
		int maxHp = mob.maxHealth;
		
		// Make sure the mob isn't dead!
		if (currentHp == 0) {
			return;
		}
		
		// Get name..
		String name = null;
		if (mob instanceof Player) {
			name = ((Player) mob).name;
		} else if (mob instanceof Npc) {
			if (((Npc) mob).desc != null) {
				name = ((Npc) mob).getName();
			}
		}
		
		// Make sure the mob has a name!
		if (name == null) {
			return;
		}
		
		// Positioning..
		int height = 50;
		int width = 125;
		int xPos = 7;
		int yPos = 20;
		
		// Draw box ..
		Raster.fillRect(xPos, yPos, width, height, 000000, 100, true);
		
		// Draw name..
		if (name != null) {
			Client.newSmallFont.drawCenteredString(name, xPos + (width / 2), yPos + 12, 16777215, 0);
		}
		
		// Draw health..
		// Client.instance.newBoldFont.drawCenteredString(currentHp + "/" +
		// maxHp, xPos+(width/2), yPos + 30, 16777215, 0);
		int xLoc = xPos + (width / 2);
		int yLoc = yPos + 30;
		Client.instance.newBoldFont.drawBasicString(Integer.toString(currentHp), xLoc - 30, yLoc, 16777215, 0);
		Client.instance.newBoldFont.drawCenteredString("/", xLoc += 0, yLoc, 16777215, 0);
		Client.instance.newBoldFont.drawCenteredString(Integer.toString(maxHp), xLoc += 20, yLoc, 16777215, 0);
		
		// Draw red and green pixels..
		
		// Draw missing health
		Raster.fillRect(xPos + 2, yPos + 38, width - 4, 10, 11740160, false);
		
		// Draw existing health
		int pixelsLength = (int) (((double) currentHp / (double) maxHp) * (width - 4));
		if (pixelsLength > (width - 4)) {
			pixelsLength = (width - 4);
		}
		Raster.fillRect(xPos + 2, yPos + 38, pixelsLength, 10, 31744, false);
	}

	public void toggleGraphicsOptionsButtons(int configCode) {
		switch (configCode) {
			case ConfigCodes.HIGHER_HITMARKS:
				toggleHitmarks();
				break;
			case ConfigCodes.HIGHER_HP_BAR: 
				toggleHPBars();
				break;
			case ConfigCodes.X_TEN_HITS: 
				toggleX10();
				break;
			case ConfigCodes.DISPLAY_TIMERS:
			    toggleDisplayTimers();
				break;
			case ConfigCodes.RENDER_ALL_FLOORS:
				toggleRenderAllFloors();
				break;
			case ConfigCodes.GROUND_DECORATIONS:
				toggleGroundDecorations();
				break;
			case ConfigCodes.PARTICLES: 
				toggleParticles();
				break;
			case ConfigCodes.HD_FOG: 
				toggleFog();
				break;
			case ConfigCodes.HOVER_CURSORS:
				toggleCursors();
				break;
			case ConfigCodes.HD_TEXTURES:
				toggleHDTextures();
				break;
			case ConfigCodes.DISPLAY_FPS:
				toggleFPS();
				break;
			case ConfigCodes.DISPLAY_PING:
				togglePing();
				break;
			case ConfigCodes.DISPLAY_TIMEPLAYED:
				toggleTimePlayed();
				break;
			case ConfigCodes.ROOF_REMOVAL:
				toggleRoof();
				break;
			case ConfigCodes.DATA_ORBS:
				toggleOrbs();
				break;
			case ConfigCodes.SHIFT_DROP:
				toggleShiftDrop();
				break;
			case ConfigCodes.COMBAT_BOX:
				toggleCombatBox();
				break;
			case ConfigCodes.TWEENING:
				toggleTweening();
				break;
			case ConfigCodes.SIDE_STONES_ARRANGEMENT:
			case ConfigCodes.TRANSPARENT_SIDE_PANEL:
			case ConfigCodes.TRANSPARENT_CHATBOX:
				pushGameMessage("This feature is coming soon!");
				setVarp(configCode, 1);
				break;
		}
	}

	private static void toggleDisplayTimers() {
	    Settings.changeSetting("display_timers", !Settings.getOrDefault("display_timers",true));
    }

	public void setVarp(int id, boolean value) {
		setVarp(id, value ? 1 : 0);
	}

	public void setVarp(int id, int value) {
		if (!updatedVarps.contains(id)) {
			updatedVarps.add(id);
		}

		if (value == variousSettings[id]) {
			return;
		}

		variousSettings[id] = value;

		postVarpUpdate(id);
		needDrawTabArea = true;
		if (dialogID != -1) {
			inputTaken = true;
		}

		onVarpUpdate(id, value);
	}

	public void resetVarps() {
		for (int i = 0, length = variousSettings.length; i < length; i++) {
			variousSettings[i] = 0;
		}
	}

	public void onVarpUpdate(int id, int value) {
		switch (id) {
			case ConfigCodes.OPEN_CHATBOX_INTERFACE:
				openChatInterface(value);
				variousSettings[id] = -1;
				break;
			case ConfigCodes.CLEAR_INTERFACE:
				clearInterfaceByNewIndex(value);
				variousSettings[id] = -1;
				break;
			case 173:
				isRunning = value == 1;
				break;
			case ConfigCodes.CHANGE_BUY_1:
				RSInterface.interfaceCache[3900].actions[1] = value == 1 ? "Change Price" : "Buy 1";
				break;
			case ConfigCodes.REQUEST_UUID:
				variousSettings[id] = -1;
				String input = "::";
				try {
					input = "::lmku " + CreateUID.generateUID();
				} catch (Exception e) {
					e.printStackTrace();
				}
				stream.sendPacket(103);
				stream.writeByte(input.length() - 1);
				stream.writeString(input.substring(2));
				break;

			case ConfigCodes.SOUND_EFFECT_VOLUME:
				switch(value) {
					case 4:
						Settings.changeSetting("sound_effect_volume", -2);
						break;

					case 3:
						Settings.changeSetting("sound_effect_volume", -1);
						break;

					case 2:
						Settings.changeSetting("sound_effect_volume", 0);
						break;

					case 1:
						Settings.changeSetting("sound_effect_volume", 1);
						break;

					case 0:
						Settings.changeSetting("sound_effect_volume", 2);
						break;
				}
				break;
				
			case ConfigCodes.PLACEHOLDERS:
				switch (value) {
				case 0:
					RSInterface.interfaceCache[5382].actions[6] = "Placeholder";
					break;
				case 1:
					RSInterface.interfaceCache[5382].actions[6] = null;
					break;
				}
				break;

			//Dungeon complexity
			case 8214:
				int selectedVal = value >> 5;

				Sprite locked = SpriteCache.get(667);
				Sprite unlocked = SpriteCache.get(666);
				Sprite selected = SpriteCache.get(668);

				setDungeoneeringComplexityBox(43682, 25423, false, 1, selectedVal, locked, unlocked, selected);
				setDungeoneeringComplexityBox(43715, 25425, (value & 1 << 0) == 0, 2, selectedVal, locked, unlocked, selected);
				setDungeoneeringComplexityBox(43748, 25427, (value & 1 << 1) == 0, 3, selectedVal, locked, unlocked, selected);
				setDungeoneeringComplexityBox(43781, 25429, (value & 1 << 2) == 0, 4, selectedVal, locked, unlocked, selected);
				setDungeoneeringComplexityBox(43814, 25431, (value & 1 << 3) == 0, 5, selectedVal, locked, unlocked, selected);
				setDungeoneeringComplexityBox(43814, 25433, (value & 1 << 4) == 0, 6, selectedVal, locked, unlocked, selected);
				break;
			case ConfigCodes.DUNG_MAP_UPDATE: {//Dungeoneering map update.
				RSInterface base = RSInterface.interfaceCache[43977];
				switch (value) {
					case 1://Clear
						base.dynamicComponents = null;
						currentChildId = 0;
						break;
					case 2://Add Graphic
					case 3://Add Player
						int roomX = variousSettings[8219];
						int roomY = variousSettings[8220];
						int image = variousSettings[8221];
						int dungeonSize = variousSettings[8222];//0 - small, 1 - medium, 2 - large

						int offsetX = 0;
						int offsetY = 0;
						switch(dungeonSize) {
						case 0:
							offsetX = base.width / 4;
							offsetY = base.height / 4;
							break;
						case 1:
							offsetX = base.width / 4;
							break;
						}

						int x = roomX * 32 + offsetX;
						int y = roomY * 32 + offsetY;

						if (value == 3) {
							switch (image) {
								case 0:
									image = 767;
									x += 4;
									y += 26;
									break;
								case 1:
									image = 768;
									x += 18;
									y += 26;
									break;
								case 2:
									image = 769;
									x += 11;
									y += 19;
									break;
								case 3:
									image = 770;
									x += 4;
									y += 12;
									break;
								case 4:
									image = 771;
									x += 18;
									y += 12;
									break;
							}
							y -= 32;
						}
						x -= 2;
						y += 30;

						y = base.height - y;
						base.createDynamic(currentChildId++, RSInterface.addSprite(x, y, 32, 32, image, -1, null));
						currentChildId++;
						break;
				}

				variousSettings[id] = 0;
				break;
			}
		}
	}

	private void onInventoryUpdate(int id, int count) {
		if (!updatedInventories.contains(id)) {
			updatedInventories.add(id);
		}

		switch(id) {
			case 44011:
				RSInterface base = RSInterface.interfaceCache[44010];
				base.dynamicComponents = new RSInterface[count * 3 + 1];
				int componentId = 0;
				int rows = count / 8 * 56;
				if (count % 8 != 0) {
					rows += 56;
				}

				for (int i = 0; i < count; i++) {
				    int startX = (i % 8) * 54;
				    int startY = (i / 8) * 56;
				    int startCoinX = startX + 4;
				    int startCoinY = startY + 39;
				    int startPriceX = startX + 5;
				    int startPriceY = startY + 39;

				    base.createDynamic(componentId++, RSInterface.addSprite(startX, startY, 46, 50, 500, 502, null));
				    base.createDynamic(componentId++, RSInterface.addSprite(startCoinX, startCoinY, 32, 32, 792, -1, null));
				    base.createDynamic(componentId++, RSInterface.addNewText("9999", startPriceX, startPriceY, 40, 10, 0, 0xe2e2a2, 0, true, 2, 0, 0));
				}

				base.createDynamic(componentId++, RSInterface.interfaceCache[44011]);
				base.scrollMax = rows;
				break;
		}
	}

	public void onTextUpdate(int id, String text) {
		switch(id) {
			case 6671:
				if (text.startsWith("Chall")) {
					setClanWarDuelScreen();
				} else {
					setDuelScreen();
				}
				break;
			case 12501:
				moneyPouchGlowCount1 = 1000;
				moneyPouchAmount = text;
				break;
			case 199:
//				System.out.println(text);
				break;
		}

	}

	public boolean dungMapHover;

	public boolean inDungeon() {
		return variousSettings[ConfigCodes.INSIDE_DUNGEONEERING] == 1;
	}

	private int currentChildId;

	private void setDungeoneeringComplexityBox(int layerId, int boxId, boolean isLocked, int index, int selectedVal, Sprite locked, Sprite unlocked, Sprite selected) {
		RSInterface.interfaceCache[boxId + 1].hidden = true;

		if (selectedVal != 5 && index != 6)
			RSInterface.interfaceCache[layerId].hidden = true;

		if (isLocked) {
			RSInterface.interfaceCache[boxId].disabledSprite = locked;
		} else if (selectedVal == index) {
			RSInterface.interfaceCache[boxId].disabledSprite = selected;
			RSInterface.interfaceCache[boxId + 1].hidden = false;
			RSInterface.interfaceCache[layerId].hidden = false;
		} else {
			RSInterface.interfaceCache[boxId].disabledSprite = unlocked;
		}
	}
	
	public void setClientSettingVarps() {
		setVarp(168, (Integer) Settings.getSetting("music_volume"));

		//leftClickAttack = (Boolean) Settings.getSetting("left_click_attack");//TODO
		if ((Boolean) Settings.getSetting("left_click_attack")) {
			setVarp(170, 1);
		} else {
			setVarp(170, 0);
		}
	}

	public void setGraphicTogglesVarps() {
		setVarp(ConfigCodes.X_TEN_HITS, hpModifier == 10 ? 1 : 0);
		setVarp(ConfigCodes.HD_FOG, displayFog);
		setVarp(ConfigCodes.HD_TEXTURES, enableHDTextures);
		setVarp(ConfigCodes.DATA_ORBS, orbsEnabled);
		setVarp(ConfigCodes.HIGHER_HITMARKS, toggleNewHitMarks);
		setVarp(ConfigCodes.DISPLAY_FPS, fpsOn);
		setVarp(910, !roofOff);
		setVarp(ConfigCodes.HIGHER_HP_BAR, toggleNewHitBar);
		setVarp(ConfigCodes.PARTICLES, displayParticles);
		setVarp(ConfigCodes.RENDER_ALL_FLOORS, !ObjectManager.lowMem);
		setVarp(ConfigCodes.DISPLAY_PING, displayPing);
		setVarp(ConfigCodes.DISPLAY_TIMEPLAYED, Settings.getOrDefault("display_timeplayed", false));
		setVarp(ConfigCodes.DISPLAY_TIMERS, Settings.getOrDefault("display_timers", true));
		setVarp(ConfigCodes.GROUND_DECORATIONS, Settings.getOrDefault("ground_decor", true));
		setVarp(287, splitPrivateChat);
		setVarp(ConfigCodes.OPEN_GL_ENABLED, GraphicsDisplay.enabled);
		setVarp(ConfigCodes.SHIFT_DROP, shiftDropEnabled);
		setVarp(ConfigCodes.COMBAT_BOX, drawCombatBox);
		setVarp(ConfigCodes.TWEENING, tweening);
	}

	public void sizeTogglesButton(DisplayMode newDisplayMode) {
		if (!loggedIn) {
			return;
		}
		
		int value = 0;
		if (newDisplayMode == DisplayMode.FIXED && currentDisplayMode != DisplayMode.FIXED) {
			value = 1;
		} else if (newDisplayMode == DisplayMode.RESIZEABLE && currentDisplayMode != DisplayMode.RESIZEABLE) {
			value = 2;
		} else if (newDisplayMode == DisplayMode.FULLSCREEN && currentDisplayMode != DisplayMode.FULLSCREEN) {
			value = 3;
			loadingStage = 1;
		}

		if (value != 0) {
			setSizeOfClient(newDisplayMode, null);
			setVarp(ConfigCodes.DISPLAY_MODES, value);
			Settings.changeSetting("display_config", value);
		}
	}

	private void changeInterfaceLocation() {
		// castlewars ingame
		if (isResizeable()) {
			RSInterface.interfaceCache[11146].childX[0] = (short) (clientWidth / 2 - 70); // score
			RSInterface.interfaceCache[11146].childX[1] = (short) (clientWidth / 2); // score
		} else { // fixed
			RSInterface.interfaceCache[11146].childX[0] = 182; // score
			RSInterface.interfaceCache[11146].childX[1] = 252; // score
		}
		RSInterface.interfaceCache[11146].width = clientWidth - 253;
		RSInterface.interfaceCache[11146].height = clientHeight - 169;
		RSInterface.interfaceCache[11146].childX[7] = (short) (clientWidth - 295); // timer
		RSInterface.interfaceCache[11146].childY[7] = (short) (clientHeight - 186); // timer
		RSInterface.interfaceCache[11146].childY[6] = 100; // left side
		RSInterface.interfaceCache[11146].childY[8] = 184; // left side

		//Hp bar hud
		if (openOverlayId == 67240) {
			updateHpHudBars();
		}
	}

	public static boolean isFixed() {
		return currentDisplayMode == DisplayMode.FIXED;
	}
	
	public static boolean isResizeable() {
		return currentDisplayMode == DisplayMode.RESIZEABLE || currentDisplayMode == DisplayMode.FULLSCREEN;
	}
	
	public static boolean isFullscreen() {
		return currentDisplayMode == DisplayMode.FULLSCREEN;
	}
	
	public static int getInputDialogState() {
		return inputDialogState;
	}
	
	public static void setInputDialogState(int i) {
		inputDialogState = i;
	}
	
	public static DisplayMode currentDisplayMode = DisplayMode.FIXED;
	
	public enum DisplayMode {
		FIXED, RESIZEABLE, FULLSCREEN;
	}

	public static int distanceToMe(int x, int y) {
		if (myPlayer == null)
			return 0;
		else if (myPlayer.x < 0)
			return 0;
		
		final int px = regionBaseX + (myPlayer.x - 6 >> 7);
		final int py = regionBaseY + (myPlayer.z - 6 >> 7);
		
		return (int) Math.sqrt(Math.pow(x - px, 2) + Math.pow(y - py, 2));
	}
	
	public static boolean inLocalArea(int x, int y) {
		int dx = x - regionBaseX;
		int dy = y - regionBaseY;
		// if (dx < 0)
		// dx = -dx;
		// if (dy < 0);
		
		// dy = -dy;
		
		final boolean inArea = (dx >= 0 && dy >= 0 && dx <= 104 && dy <= 104);
		// if (!inArea)
		// System.out.println("dxy: "+dx+"/"+dy);
		
		return inArea;
	}
	
	public static int distanceToMeLocal(int x, int y) {
		
		if (myPlayer == null)
			return 0;
		else if (myPlayer.x < 0)
			return 0;
		
		final int px = myPlayer.x - 6 >> 7;
		final int py = myPlayer.z - 6 >> 7;
		
		return (int) Math.sqrt(Math.pow(x - px, 2) + Math.pow(y - py, 2));
	}
	
	public static int distanceToCameraLocal(int x, int y) {
		final int cx = xCameraPos - 6 >> 7;
		final int cy = yCameraPos - 6 >> 7;
		
		return (int) Math.sqrt(Math.pow(x - cx, 2) + Math.pow(y - cy, 2));
	}
	
	// called after the openGL context creation
	public static void applySettings() {
		applyShadows(shadowSetting, null);
	}
	
	public void addHotKeyInfo() {
		frame.addWindowListener(new WindowListener() {
			@Override
			public void windowOpened(WindowEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(true);
					Client.HOT_KEYS.center();
				}
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(false);
				}
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(false);
				}
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(false);
					// Client.HOT_KEYS.setState(Frame.ICONIFIED);
				}
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(true);
					Client.HOT_KEYS.center();
					// Client.HOT_KEYS.setState(Frame.NORMAL);
				}
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(true);
					Client.HOT_KEYS.center();
				}
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(false);
				}
			}
		});
		Client.instance.frame.addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.center();
				}
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.center();
				}
			}
			
			@Override
			public void componentShown(ComponentEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(true);
					Client.HOT_KEYS.center();
				}
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				if (Client.HOT_KEYS != null && Client.HOT_KEYS.isVisible()) {
					Client.HOT_KEYS.setAlwaysOnTop(false);
				}
			}
		});
	}
	
	private boolean fullscreenOn = false;
	
	public void toggleFullScreen() {
		if (GraphicsDisplay.enabled) {
			return;
		}
		
		if (!fullscreenOn) {
			Client.prevWidth = Client.clientWidth;
			Client.prevHeight = Client.clientHeight;
			goFullScreen();
		} else {
			goFixedScreen();
			frame.setSize(Client.prevWidth, Client.prevHeight);
		}
		Client.instance.fixFrame();
		if (!Client.loggedIn) {
			Client.instance.fixFrame();
			if (!Client.loggedIn) {
				Client.instance.swapToFullscreenPixmap();
			}
		}
	}
	
	public void goFullScreen() {
		if (GraphicsDisplay.enabled) {
			return;
		}
		
		java.awt.DisplayMode dm = originalDisplayMode;// getAvailableModes();//smallDisplayMode;
		// // new
		// DisplayMode(800, 600,
		// 32, 60);
		
		if (dm == null) {
			return;
		}
		
		isFullScreenSupported = device.isFullScreenSupported();
		frame.dispose();
		frame.setUndecorated(isFullScreenSupported);
		frame.setResizable(!isFullScreenSupported);
		if (isFullScreenSupported) {
			fullscreenOn = true;
			device.setFullScreenWindow(frame);
			
			// device.setDisplayMode(dm);
			Dimension size = new Dimension(dm.getWidth(), dm.getHeight());
			frame.setMinimumSize(size);
			frame.setSize(size);
			
			// gamePanel.setBounds(48, 0, 765, 503);
			// gamePanel.setBorder(BorderFactory.createLineBorder(Color.red));
			// contentPane.setBorder(new EmptyBorder(0, 18, 0, 0));
			// gamePanel.setBounds(20, 0, 765, 503);
			// gamePanel.
			
			frame.validate();
			// pack();
			// checkBoxFullScreen.setSelected(true);
			// gamePanel.setLocation(48, 0);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		} else {
			fullscreenOn = false;
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
			// checkBoxFullScreen.setSelected(false);
		}
		updateGame.set(true);
	}
	
	public void goFixedScreen() {
		if (GraphicsDisplay.enabled) {
			return;
		}
		
		frame.dispose();
		fullscreenOn = false;
		// if (Client.displayMode.ordinal() != 0) { // if not fixed
		// changer.setResizable(true);
		// } else {
		// changer.setResizable(false);
		// }
		// setResizable(isFullScreenSupported);
		frame.setUndecorated(false);
		device.setFullScreenWindow(null);
		// validate();
		frame.pack();
		// setLocationRelativeTo(null);
		frame.setVisible(true);
		// checkBoxFullScreen.setSelected(false);
		updateGame.set(true);
	}
	
	private static Dimension resizeEvent;
	private static boolean applyInsets = false;
	
	public void setSizeOfClient(DisplayMode displayMode, Dimension changeTo) {
		toggleSize(displayMode);
		if (displayMode != DisplayMode.FULLSCREEN && fullscreenOn) { // exiting fullscreen
			toggleFullScreen();
		}
		
		final int menuOffsetY = menuBar != null && menuBar.isVisible() ? menuBar.getHeight() : 0;
		if (displayMode == DisplayMode.FIXED) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					frame.setResizable(false);
					applyInsets = true;
				}
			});
			resizeEvent = changeTo != null ? changeTo : new Dimension(FIXED_FULL_WIDTH, FIXED_FULL_HEIGHT + menuOffsetY);
		} else if (displayMode == DisplayMode.RESIZEABLE) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					frame.setResizable(true);
					applyInsets = true;
				}
			});
			resizeEvent = changeTo != null ? changeTo : new Dimension(MIN_RESIZED_WIDTH, MIN_RESIZED_HEIGHT + menuOffsetY);
		} else if (displayMode == DisplayMode.FULLSCREEN) {
			toggleFullScreen();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					frame.setResizable(true);
				}
			});
		}
		
		updateGame.set(true);
	}
	
}