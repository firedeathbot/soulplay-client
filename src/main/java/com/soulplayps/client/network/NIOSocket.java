package com.soulplayps.client.network;

import java.io.IOException;
import java.net.Socket;

public class NIOSocket extends SocketImpl {

	Socket socket;
	ClientOutputStream outputStream;
	ClientInputStream inputStream;

	@Override
	public int available() throws IOException {
		return this.inputStream.available();
	}

	@Override
	public boolean hasRemaining(int bytes) throws IOException {
		return this.inputStream.hasRemaining(bytes);
	}

	@Override
	public void write(byte[] buf, int off, int length) throws IOException {
		outputStream.write(buf, off, length);
	}

	@Override
	public void softClose() {
		this.outputStream.close();
		this.inputStream.close();
	}

	@Override
	public void close() {
		this.outputStream.close();

		try {
			this.socket.close();
		} catch (IOException var3) {
			;
		}

		this.inputStream.close();
	}

	public void finalize() {
		close();
	}

	@Override
	public void read(byte[] buf, int off, int length) throws IOException {
		this.inputStream.read(buf, off, length);
	}

	@Override
	public int read() throws IOException {
		return this.inputStream.read();
	}

	public NIOSocket(Socket socket, int inputSize, int outputSize) throws IOException {
		this.socket = socket;
		this.socket.setSoTimeout(30000);
		this.socket.setTcpNoDelay(true);
		this.socket.setReceiveBufferSize(65536);
		this.socket.setSendBufferSize(65536);
		this.inputStream = new ClientInputStream(this.socket.getInputStream(), inputSize);
		this.outputStream = new ClientOutputStream(this.socket.getOutputStream(), outputSize);
	}

}
