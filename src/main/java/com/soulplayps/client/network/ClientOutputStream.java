package com.soulplayps.client.network;

import java.io.IOException;
import java.io.OutputStream;

public class ClientOutputStream implements Runnable {
	int anInt1007 = 0;
	Thread thread;
	OutputStream outputStream;
	byte[] buffer;
	int anInt1011 = 0;
	boolean closed;
	IOException anIOException1013;
	int capacity;

	boolean method522() {
		if (this.closed) {
			try {
				this.outputStream.close();
				if (null == this.anIOException1013) {
					this.anIOException1013 = new IOException("");
				}
			} catch (IOException var2) {
				if (null == this.anIOException1013) {
					this.anIOException1013 = new IOException(var2);
				}
			}

			return true;
		} else {
			return false;
		}
	}

	public void run() {
		do {
			int var2;
			synchronized (this) {
				while (true) {
					if (null != this.anIOException1013) {
						return;
					}

					if (this.anInt1011 <= this.anInt1007) {
						var2 = this.anInt1007 - this.anInt1011;
					} else {
						var2 = this.anInt1007 + (this.capacity - this.anInt1011);
					}

					if (var2 > 0) {
						break;
					}

					try {
						this.outputStream.flush();
					} catch (IOException var10) {
						this.anIOException1013 = var10;
						return;
					}

					if (this.method522()) {
						return;
					}

					try {
						this.wait();
					} catch (InterruptedException var11) {
						;
					}
				}
			}

			try {
				if (this.anInt1011 + var2 <= this.capacity) {
					this.outputStream.write(this.buffer, this.anInt1011, var2);
				} else {
					int var13 = this.capacity - this.anInt1011;
					this.outputStream.write(this.buffer, this.anInt1011, var13);
					this.outputStream.write(this.buffer, 0, var2 - var13);
				}
			} catch (IOException var9) {
				IOException var1 = var9;
				synchronized (this) {
					this.anIOException1013 = var1;
					return;
				}
			}

			synchronized (this) {
				this.anInt1011 = (var2 + this.anInt1011) % this.capacity;
			}
		} while (!this.method522());

	}

	void write(byte[] var1, int var2, int var3) throws IOException {
		if (var3 >= 0 && var2 >= 0 && var3 + var2 <= var1.length) {
			synchronized (this) {
				if (null != this.anIOException1013) {
					throw new IOException(this.anIOException1013.toString());
				} else {
					int var5;
					if (this.anInt1011 <= this.anInt1007) {
						var5 = this.anInt1011 + (this.capacity - this.anInt1007) - 1;
					} else {
						var5 = this.anInt1011 - this.anInt1007 - 1;
					}

					if (var5 < var3) {
						throw new IOException("");
					} else {
						if (var3 + this.anInt1007 <= this.capacity) {
							System.arraycopy(var1, var2, this.buffer, this.anInt1007, var3);
						} else {
							int var6 = this.capacity - this.anInt1007;
							System.arraycopy(var1, var2, this.buffer, this.anInt1007, var6);
							System.arraycopy(var1, var2 + var6, this.buffer, 0, var3 - var6);
						}

						this.anInt1007 = (this.anInt1007 + var3) % this.capacity;
						this.notifyAll();
					}
				}
			}
		} else {
			throw new IOException();
		}
	}

	void close() {
		synchronized (this) {
			this.closed = true;
			this.notifyAll();
		}

		try {
			this.thread.join();
		} catch (InterruptedException var3) {
			;
		}

	}

	ClientOutputStream(OutputStream var1, int capacity) {
		this.outputStream = var1;
		this.capacity = capacity + 1;
		this.buffer = new byte[this.capacity];
		this.thread = new Thread(this, "ClientOutputStream");
		this.thread.setDaemon(true);
		this.thread.start();
	}
}
