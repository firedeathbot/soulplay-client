package com.soulplayps.client.network;

import java.io.IOException;

public abstract class SocketImpl {

	public abstract int available() throws IOException;

	public abstract boolean hasRemaining(int bytes) throws IOException;

	public abstract void write(byte[] buf, int off, int length) throws IOException;

	public abstract void close();

	public abstract void softClose();

	public abstract void read(byte[] buf, int off, int length) throws IOException;

	public abstract int read() throws IOException;

}
