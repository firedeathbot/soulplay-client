package com.soulplayps.client.network;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public class ClientInputStream implements Runnable {
	byte[] buffer;
	Thread thread;
	int capacity;
	IOException exception;
	InputStream inputStream;
	int offset = 0;
	int bufferPos = 0;

	public void run() {
		while (true) {
			int bytesToRead;
			synchronized (this) {
				while (true) {
					if (this.exception != null) {
						return;
					}

					if (this.bufferPos == 0) {
						bytesToRead = this.capacity - this.offset - 1;
					} else if (this.bufferPos <= this.offset) {
						bytesToRead = this.capacity - this.offset;
					} else {
						bytesToRead = this.bufferPos - this.offset - 1;
					}

					if (bytesToRead > 0) {
						break;
					}

					try {
						this.wait();
					} catch (InterruptedException var9) {
						;
					}
				}
			}

			int bytesRead;
			try {
				bytesRead = this.inputStream.read(this.buffer, this.offset, bytesToRead);
				if (bytesRead == -1) {
					throw new EOFException();
				}
			} catch (IOException exception) {
				synchronized (this) {
					this.exception = exception;
					return;
				}
			}

			synchronized (this) {
				this.offset = (this.offset + bytesRead) % this.capacity;
			}
		}
	}

	boolean hasRemaining(int bytes) throws IOException {
		if (bytes == 0) {
			return true;
		}

		if (bytes <= 0 || bytes >= this.capacity) {
			throw new IOException();
		}

		synchronized (this) {
			int availableBytes;
			if (this.bufferPos <= this.offset) {
				availableBytes = this.offset - this.bufferPos;
			} else {
				availableBytes = this.offset + (this.capacity - this.bufferPos);
			}

			if (availableBytes >= bytes) {
				return true;
			}

			if (this.exception != null) {
				throw new IOException(this.exception.toString());
			}

			this.notifyAll();
			return false;
		}
	}

	int available() throws IOException {
		synchronized (this) {
			int availableBytes;
			if (this.bufferPos <= this.offset) {
				availableBytes = this.offset - this.bufferPos;
			} else {
				availableBytes = this.capacity - this.bufferPos + this.offset;
			}

			if (availableBytes <= 0 && this.exception != null) {
				throw new IOException(this.exception.toString());
			}

			this.notifyAll();
			return availableBytes;
		}
	}

	void close() {
		synchronized (this) {
			if (this.exception == null) {
				this.exception = new IOException("");
			}

			this.notifyAll();
		}

		try {
			this.thread.join();
		} catch (InterruptedException var3) {
			;
		}
	}

	int read() throws IOException {
		synchronized (this) {
			if (this.bufferPos == this.offset) {
				if (this.exception != null) {
					throw new IOException(this.exception.toString());
				} else {
					return -1;
				}
			} else {
				int value = this.buffer[this.bufferPos] & 255;
				this.bufferPos = (this.bufferPos + 1) % this.capacity;
				this.notifyAll();
				return value;
			}
		}
	}

	ClientInputStream(InputStream inputStream, int capacity) {
		this.inputStream = inputStream;
		this.capacity = capacity + 1;
		this.buffer = new byte[this.capacity];
		this.thread = new Thread(this, "ClientInputStream");
		this.thread.setDaemon(true);
		this.thread.start();
	}

	int read(byte[] buffer, int offset, int length) throws IOException {
		if (length < 0 || offset < 0 || length + offset > buffer.length) {
			throw new IOException();
		}

		synchronized (this) {
			int var5;
			if (this.bufferPos <= this.offset) {
				var5 = this.offset - this.bufferPos;
			} else {
				var5 = this.offset + (this.capacity - this.bufferPos);
			}

			if (length > var5) {
				length = var5;
			}

			if (length == 0 && null != this.exception) {
				throw new IOException(this.exception.toString());
			} else {
				if (this.bufferPos + length <= this.capacity) {
					System.arraycopy(this.buffer, this.bufferPos, buffer, offset, length);
				} else {
					int var6 = this.capacity - this.bufferPos;
					System.arraycopy(this.buffer, this.bufferPos, buffer, offset, var6);
					System.arraycopy(this.buffer, 0, buffer, var6 + offset, length - var6);
				}

				this.bufferPos = (this.bufferPos + length) % this.capacity;
				this.notifyAll();
				return length;
			}
		}
	}
}
