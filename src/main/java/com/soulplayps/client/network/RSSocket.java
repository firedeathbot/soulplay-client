package com.soulplayps.client.network;

import com.soulplayps.client.applet.RSApplet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class RSSocket extends SocketImpl implements Runnable {
	
	public RSSocket(RSApplet RSApplet_, Socket socket1, int limit) throws IOException {
		this.limit = limit;
		this.softLimit = limit - 100;
		this.rsApplet = RSApplet_;
		this.socket = socket1;
		this.socket.setSoTimeout(30000);
		this.socket.setReceiveBufferSize(65536);
		this.socket.setSendBufferSize(65536);
		this.socket.setTcpNoDelay(true);
		this.inputStream = socket.getInputStream();
		this.outputStream = socket.getOutputStream();
	}

	public void softClose() {
		isWriter = false;
		synchronized (this) {
			notify();
		}
		buffer = null;
	}

	public void close() {
		closed = true;
		try {
			if (inputStream != null)
				inputStream.close();
			if (outputStream != null)
				outputStream.close();
			if (socket != null)
				socket.close();
		} catch (IOException _ex) {
			System.out.println("Error closing stream");
		}
		isWriter = false;
		synchronized (this) {
			notify();
		}
		buffer = null;
	}
	
	public int read() throws IOException {
		if (closed)
			return 0;
		return inputStream.read();
	}
	
	public int available() throws IOException {
		if (closed)
			return 0;
		return inputStream.available();
	}

	public boolean hasRemaining(int bytes) throws IOException {
		if (closed)
			return false;
		
		try {
        	return inputStream.available() >= bytes;
		} catch (IOException var4) {
			throw new IOException("Error reading from clientstream", var4);
		}
	}

	public void read(byte buf[], int offset, int length) throws IOException {
		if (closed) {
			return;
		}

		int bytesRead;
		while (length > 0) {
			bytesRead = inputStream.read(buf, offset, length);
			if (bytesRead <= 0) {
				throw new IOException("EOF");
			}
			offset += bytesRead;
			length -= bytesRead;
		}
	}

	public void write(byte[] buf, int off, int length) throws IOException {
		queueBytes(length, buf, 0);
	}

	public void queueBytes(int i, byte abyte0[], int offset) throws IOException {
		if (closed)
			return;
		if (hasIOError) {
			hasIOError = false;
			throw new IOException("Error in writer thread");
		}
		if (buffer == null)
			buffer = new byte[limit];
		synchronized (this) {
			for (int l = 0; l < i; l++) {
				buffer[buffIndex] = abyte0[l + offset];
				buffIndex = (buffIndex + 1) % limit;
				if (buffIndex == (writeIndex + softLimit) % limit)
					throw new IOException("buffer overflow");
			}
			
			if (!isWriter) {
				isWriter = true;
				rsApplet.startRunnable(this, 3, "Clientstream");
			}
			notifyAll();
		}
	}
	
	public void run() {
		while (isWriter) {
			int i;
			int j;
			synchronized (this) {
				if (buffIndex == writeIndex)
					try {
						wait();
					} catch (InterruptedException _ex) {
						_ex.printStackTrace();
					}
				if (!isWriter)
					return;
				j = writeIndex;
				if (buffIndex >= writeIndex)
					i = buffIndex - writeIndex;
				else
					i = limit - writeIndex;
			}
			if (i > 0) {
				try {
					outputStream.write(buffer, j, i);
				} catch (IOException _ex) {
//					if (!hasIOError)
//						_ex.printStackTrace();
					hasIOError = true;
				}
				writeIndex = (writeIndex + i) % limit;
				try {
					if (buffIndex == writeIndex)
						outputStream.flush();
				} catch (IOException _ex) {
//					if (!hasIOError)
//						_ex.printStackTrace();
					hasIOError = true;
				}
			}
		}
	}

	private int limit;
	private int softLimit;
	private InputStream inputStream;
	private OutputStream outputStream;
	private final Socket socket;
	private boolean closed;
	private final RSApplet rsApplet;
	private byte[] buffer;
	private int writeIndex;
	private int buffIndex;
	private boolean isWriter;
	private boolean hasIOError;
}
