package com.soulplayps.client.chathistory;

import java.util.HashMap;
import java.util.Map;

import com.soulplayps.client.node.NodeCache;
import com.soulplayps.client.node.NodeSubList;

public class ChatMessageManager {

	public static Map<Integer, ChatMessageGroup> messageGroups = new HashMap<>();
	public static NodeSubList messageQueue = new NodeSubList();
	public static NodeCache messageHashtable = new NodeCache(1024);
	public static int messageCount = 0;

	public static int getPrevMessageId(int id) {
		ChatMessage chatMessage = (ChatMessage) messageHashtable.findNodeByID((long) id);
		if (chatMessage == null) {
			return -1;
		}
		if (messageQueue.head == chatMessage.nextNodeSub) {
			return -1;
		}
		return ((ChatMessage) chatMessage.nextNodeSub).uid;
	}

	public static void clear() {
		messageGroups.clear();
		messageQueue.clear();
		messageHashtable.clear();
		messageCount = 0;
	}

	public static void appendMessage(int type, String message, String clanTitle, String name) {
		ChatMessageGroup group = messageGroups.get(type);
		if (group == null) {
			group = new ChatMessageGroup();
			messageGroups.put(type, group);
		}

		ChatMessage chatMessage = group.append(type, message, clanTitle, name);
		messageHashtable.removeFromCache(chatMessage, chatMessage.uid);
		messageQueue.insertHead(chatMessage);
	}

	public static int getHighestId() {
		int highestId = -1;
		for (int type = 0; type < 20; type++) {
			ChatMessageGroup messageGroup = messageGroups.get(type);
			if (messageGroup != null && messageGroup.getPos() > 0) {
				ChatMessage chatMessage = messageGroup.getMessage(0);
				int id = -1;
				if (chatMessage != null) {
					id = chatMessage.uid;
				}
				if (id > highestId) {
					highestId = id;
				}
			}
		}
		return highestId;
	}

	public static void clearGroup(int type) {
		ChatMessageGroup group = ChatMessageManager.messageGroups.get(type);
		if (group == null) {
			return;
		}

		group.clear();
	}

}
