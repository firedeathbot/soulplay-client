package com.soulplayps.client.interfaces;

public class Component {

	private final int id;
	private final int x, y;
	
	public Component(int id, int x, int y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}
	
	public int getID() {
		return id;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
}
