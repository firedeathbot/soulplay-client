package com.soulplayps.client.interfaces;

import com.soulplayps.client.Client;

public class Tooltip {

	private static int cycle;
	private static boolean tooltipOver;

	public static void build(final String text, final RSInterface tooltipLayer, final RSInterface hookLayer,
			final int showTime, final int width) {
		hookLayer.onMouseOver = new MouseEvent() {
			@Override
			public void run() {
				int clock = Client.loopCycle;
				if (cycle < clock + showTime) {
					if (cycle < clock) {
						cycle = clock;
					}

					cycle += 2;
					return;
				}

				cycle = clock + showTime + 10;
				if (tooltipLayer.hidden || tooltipOver) {
					return;
				}

				int newWidth = width - 4;
				tooltipLayer.width = 4 + Client.newRegularFont.paragraphWidth(text, newWidth);
				tooltipLayer.height = 7 + 12 * Client.newRegularFont.paragraphHeigth(text, newWidth);

				int x = hookLayer.x;
				int y = hookLayer.y;
				int height = hookLayer.height;
				int parent = hookLayer.parentID;
				while (parent != 0) {
					x += RSInterface.interfaceCache[parent].x;
					y += RSInterface.interfaceCache[parent].y - RSInterface.interfaceCache[parent].scrollPosition;
					parent = RSInterface.interfaceCache[parent].parentID;
				}

				int finalX = x + 5;
				int finalY = y + height + 5;
				/*if($int5 != -1) {
				if($int10 > sub(if_getwidth($int5), if_getwidth($int2))) {
				$int10 = sub(if_getwidth($int5), if_getwidth($int2));
				}
				if($int11 > sub(if_getheight($int5), if_getheight($int2))) {
				$int11 = sub(sub($int7, if_getheight($int2)), 5);
				}
				}*/

				int origParent = tooltipLayer.parentID;
				if (origParent != 0) {
					RSInterface parentInter = RSInterface.interfaceCache[origParent];
					for (int i = 0, length = parentInter.children.length; i < length; i++) {
						if (parentInter.children[i] == tooltipLayer.id) {
							parentInter.childX[i] = (short) finalX;
							parentInter.childY[i] = (short) finalY;
						}
					}
				}

				tooltipLayer.dynamicComponents = null;
				tooltipLayer.createDynamic(0, RSInterface.addRectangle(0, 0, tooltipLayer.width, tooltipLayer.height, 16777120, 0, true));
				tooltipLayer.createDynamic(1, RSInterface.addRectangle(0, 0, tooltipLayer.width, tooltipLayer.height, 0, 0, false));
				tooltipLayer.createDynamic(2, RSInterface.addNewText(text, 2, 1, tooltipLayer.width - 4, tooltipLayer.height - 1, 1, 0, 0, false, 0, 0, 0));
				tooltipOver = true;
			}
		};

		hookLayer.onMouseLeave = new MouseEvent() {
			@Override
			public void run() {
				tooltipOver = false;
				tooltipLayer.dynamicComponents = null;
			}
		};
	}

}
