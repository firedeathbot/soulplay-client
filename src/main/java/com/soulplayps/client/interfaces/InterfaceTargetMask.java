package com.soulplayps.client.interfaces;

public class InterfaceTargetMask {

	public int mask;

	public InterfaceTargetMask(int mask) {
		this.mask = mask;
	}

	public InterfaceTargetMask() {
		this(0);
	}

	public boolean canUseOnInventory() {
		return (mask & 0x10) != 0;
	}

	public InterfaceTargetMask useOnInventory() {
		mask |= 0x10;
		return this;
	}

	public boolean canUseOnPlayer() {
		return (mask & 0x8) != 0;
	}

	public InterfaceTargetMask useOnPlayer() {
		mask |= 0x8;
		return this;
	}

	public boolean canUseOnObject() {
		return (mask & 0x4) != 0;
	}

	public InterfaceTargetMask useOnObject() {
		mask |= 0x4;
		return this;
	}

	public boolean canUseOnNpc() {
		return (mask & 0x2) != 0;
	}

	public InterfaceTargetMask useOnNpc() {
		mask |= 0x2;
		return this;
	}

	public boolean canUseOnGroundItem() {
		return (mask & 0x1) != 0;
	}

	public InterfaceTargetMask useOnGroundItem() {
		mask |= 0x1;
		return this;
	}
	
	public int getMask() {
		return mask;
	}

}
