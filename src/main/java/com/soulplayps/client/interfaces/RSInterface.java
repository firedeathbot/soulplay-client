package com.soulplayps.client.interfaces;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.util.BitwiseHelper;
import com.soulplayps.client.util.TextClass;
import com.soulplayps.client.node.ObjectCache;
import com.soulplayps.client.node.animable.entity.EntityDef;
import com.soulplayps.client.node.animable.entity.PlayerAppearance;
import com.soulplayps.client.node.animable.item.ItemDef;
import com.soulplayps.client.node.animable.model.Model;
import com.soulplayps.client.node.animable.model.ModelTypeEnum;
import com.soulplayps.client.node.object.ObjectDef;
import com.soulplayps.client.node.raster.RSFont;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.fs.RSArchive;
import com.soulplayps.client.summoning.Creation;
import com.soulplayps.client.summoning.Creation.CreationData;
import com.soulplayps.client.unknown.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RSInterface {

	private static final InterfaceTargetMask EMPTY = new InterfaceTargetMask(0);
	public static final int TILING_FLAG_BIT = 0;
	public static final int IGNORE_BOUNDS_BIT = 1;
	public static final int CLIP_BIT = 2;
	public static final int CAN_DRAG_ON_TO_BIT = 3;
	public static final int DONT_DRAW_INVENTORY_COUNT_BIT = 4;
	public static final int RECT_IGNORE_ATLAS = 5;
	public static final int CANT_CLICK_THROUGH = 6;
	public static final int TRANSPARENT_ON_ZERO_COUNT = 7;
	public static final int CAN_AUTOCAST = 8;
	public static final int OSRS_MODEL = 9;
	
	public static final int RIGHT_CLICK_ACTIONS_SIZE = 7;

	public int itemId = -1;
	public int itemAmount = 1;
	public int flags;
	public Sprite disabledSprite;
	public int anInt208;
	public Sprite sprites[];
	public static RSInterface interfaceCache[];
	public int requiredValues[];
	public int contentType;
	public short[] spritesX;
	public boolean hidden;
	public int mouseHoverTextRecolor;
	public int atActionType;
	public String targetName;
	public int enabledColor;
	public int width;
	public String tooltip;
	public String targetVerb;
	public boolean centerText;
	public int scrollPosition;
	public String actions[];
	public int valueIndexArray[][];
	public boolean filled;
	public String enabledMessage;
	public int hoverType;
	public int invSpritePadX;
	public int disabledColor;
	public int disabledMediaType;
	public int disabledMediaID;
	public int[] recolorModified;
	public int[] recolorOriginal;
	public boolean deletesTargetSlot;
	public int parentID;
	public int componentIndex = -1;
	public InterfaceTargetMask targetMask = EMPTY;
	public static ObjectCache aMRUNodes_238;
	public int anInt239;
	public int children[];
	public short[] childX;
	public boolean usableItemInterface;
	public int textDrawingAreas;
	public int invSpritePadY;
	public byte[] valueCompareType;
	public int anInt246;
	public short[] spritesY;
	public String message;
	public boolean isInventoryInterface;
	public int id;
	public int invStackSizes[];
	public int inv[];
	public int transparency;
	public int enabledMediaType;
	public int enabledMediaID;
	public int disabledAnimation;
	public int enabledAnimation;
	public boolean aBoolean259;
	public Sprite enabledSprite;
	public int scrollMax;
	public int interfaceType;
	public int xOffset;
	public static final ObjectCache modelCache = new ObjectCache(30);
	public boolean aBoolean251;
	public int yOffset;
	public boolean isMouseoverTriggered;
	public int height;
	public boolean textShadow;
	public int horizontalAlignment;//0 - left, 1 - middle, 2 - right
	public int verticalAlignment;//0 - top, 1 - middle, 2 - bottom
	public int verticalSpacing;
	public int modelZoom;
	public int modelRotationY;
	public int modelRotationX;
	public short[] childY;

	//Dynamic data start
	public RSInterface[] dynamicComponents;
	public int x, y;
	public MouseEvent onMouseOverEnter;
	public MouseEvent onMouseOver;
	public MouseEvent onMouseLeave;
	public boolean mouseOver;
	public MouseEvent onMouseClick;
	public MouseEvent onMouseClickRepeat;
	public MouseEvent onMouseClickRelease;
	public MouseEvent onMouseWheel;
	public InterfaceEvent onLoad;
	public InterfaceEvent onTimer;
	public InterfaceEvent onButtonClick;
	public boolean mouseClicked;
	public Map<Integer, List<Runnable>> onSkillUpdate;
	public Map<Integer, List<Runnable>> onInventoryUpdate;
	public Map<Integer, List<Runnable>> onVarpUpdate;
	//End

	//new shit
//	public boolean greyScale;
//	public int drawItemSpriteByItemId;
//	public int drawItemSpriteByItemIdScale;
//	public int drawItemSpriteByItemIdWH;

	public static boolean showIds = false;

	public static RSArchive aClass44;
	public static byte[] mediaIndex;

	public static Set<Integer> list;

	public static void checkTaken(int id) {
		if (Config.connectToLive) {
			return;
		}

		if (RSInterface.list.contains(id)) {
			System.out.println("OVERWRITTEN ID:"+id);
		} else {
			RSInterface.list.add(id);
		}
	}

	public void setFlag(int bit) {
		flags = BitwiseHelper.setBit(flags, bit);
	}

	public void flipHorizontal() {
		if (disabledSprite != null)
			disabledSprite = disabledSprite.flipHorizontal();
		if (enabledSprite != null)
			enabledSprite = enabledSprite.flipHorizontal();
	}

	public void flipVertical() {
		if (disabledSprite != null)
			disabledSprite = disabledSprite.flipVertical();
		if (enabledSprite != null)
			enabledSprite = enabledSprite.flipVertical();
	}

	public RSInterface() {
		hoverType = -1; // set this to -1, if 0 and above that means it has a hover. if you want a hover then set it in a function.
		disabledAnimation = -1;
		enabledAnimation = -1;
	}
	
	public static void addBackground(int id, int opacity, int color) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.disabledColor = color;
		tab.id = id;
		tab.parentID = id;
		tab.interfaceType = 11;
		tab.atActionType = 0;
		tab.contentType = 0;
		tab.transparency = opacity;
	}

	public static void addFurnitureX(int i) {
		RSInterface rsi = interfaceCache[i] = new RSInterface();
		RSInterface.checkTaken(i);
		rsi.id = i;
		rsi.parentID = i;
		rsi.interfaceType = 5;
		rsi.atActionType = 0;
		rsi.contentType = 0;
		rsi.enabledSprite = SpriteCache.get(358);
		rsi.width = 512;
		rsi.height = 334;
		setSelectableValues(i, 1000 + i - 38324, 1);
	}
	
	public static void setSelectableValues(int frame, int configId,
			int requiredValue) {
		RSInterface rsi = interfaceCache[frame];
		rsi.valueCompareType = new byte[] { 5 };
		rsi.requiredValues = new int[] { requiredValue };
		rsi.valueIndexArray = new int[1][3];
		rsi.valueIndexArray[0][0] = 5;
		rsi.valueIndexArray[0][1] = configId;
		rsi.valueIndexArray[0][2] = 0;
	}

	public static void addRectangleClickable(int id, int opacity, int color,
			boolean filled, int width, int height) {
		addRectangleClickable(id, opacity, color, filled, width, height, "Select", 0);
	}

	public static void addRectangleClickable(int id, int opacity, int color,
			boolean filled, int width, int height, String option, int clientCode) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.disabledColor = color;
		tab.filled = filled;
		tab.id = id;
		tab.parentID = id;
		tab.interfaceType = 3;
		tab.atActionType = 5;
		tab.contentType = clientCode;
		tab.transparency = opacity;
		tab.width = width;
		tab.height = height;
		tab.tooltip = option;
	}

	public static void addRectangle(int id, int opacity, int color,
			boolean filled, int width, int height) {
		addRectangle(id, opacity, color, filled, width, height, 0);
	}

	public static void addRectangle(int id, int opacity, int color,
			boolean filled, int width, int height, int clientCode) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.id = id;
		rsi.width = width;
		rsi.height = height;
		rsi.interfaceType = 3;
		rsi.disabledColor = color;
		rsi.filled = filled;
		rsi.contentType = clientCode;
		rsi.transparency = opacity;
	}
	
	public static void addTransparentSprite(int i, int spriteId)  {
        RSInterface rsinterface = interfaceCache[i] = new RSInterface();
        RSInterface.checkTaken(i);
        rsinterface.id = i;
        rsinterface.parentID = i;
        rsinterface.interfaceType = 5;
        rsinterface.atActionType = 0;
        rsinterface.contentType = 0;
        rsinterface.hoverType = 52;
        rsinterface.disabledSprite = SpriteCache.get(spriteId);
        rsinterface.width = 512;
        rsinterface.height = 334;
    }
	
	public static void addToItemGroup(RSInterface rsi, int w, int h, int x,
			int y, String[] actions) {
		rsi.width = w;
		rsi.height = h;
		rsi.inv = new int[w * h];
		rsi.invStackSizes = new int[w * h];
		rsi.usableItemInterface = false;
		rsi.isInventoryInterface = false;
		rsi.invSpritePadX = x;
		rsi.invSpritePadY = y;
		rsi.spritesX = new short[20];
		rsi.spritesY = new short[20];
		rsi.sprites = new Sprite[20];
		rsi.actions = new String[RSInterface.RIGHT_CLICK_ACTIONS_SIZE];
		for (int i = 0; i < actions.length; i++) {
			rsi.actions[i] = actions[i];
		}
		rsi.interfaceType = 2;
	}
	
	public static void textSize(int id, int idx) {
		RSInterface rsi = interfaceCache[id];
		rsi.textDrawingAreas = idx;
	}
	
	public static void addAttackStyleButton2(int id, int spriteOff, int spriteOn, int setconfig,
			int width, int height, String s, int hoverID, int hW, int hH,
			String hoverText) {
		RSInterface rsi = addInterface(id, InterfaceType.TAB);
		rsi.disabledSprite = SpriteCache.get(spriteOff);
		rsi.enabledSprite = SpriteCache.get(spriteOn);
		rsi.valueCompareType = new byte[1];
		rsi.valueCompareType[0] = 1;
		rsi.requiredValues = new int[1];
		rsi.requiredValues[0] = 1;
		rsi.valueIndexArray = new int[1][3];
		rsi.valueIndexArray[0][0] = 5;
		rsi.valueIndexArray[0][1] = setconfig;
		rsi.valueIndexArray[0][2] = 0;
		rsi.atActionType = 4;
		rsi.width = width;
		rsi.hoverType = hoverID;
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 5;
		rsi.height = height;
		rsi.tooltip = s;
		rsi = addInterface(hoverID, InterfaceType.TAB);
		rsi.isMouseoverTriggered = true;
		rsi.interfaceType = 0;
		rsi.atActionType = 0;
		rsi.hoverType = -1;
		rsi.parentID = hoverID;
		rsi.id = hoverID;
		addTooltipBox(hoverID + 1, hoverText);
		rsi.totalChildren(1);
		rsi.child(0, hoverID + 1, 0, 0);
	}
	
	public static void addActionButton(int id, int spriteOff, int spriteOn,
			int width, int height, String s) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.disabledSprite = SpriteCache.get(spriteOff);
		if (spriteOn == spriteOff)
			rsi.enabledSprite = SpriteCache.get(spriteOff);
		else
//			if (spriteOn == -1)
//				rsi.enabledSprite = null;
//			else
		if (spriteOn != -1) {
			rsi.enabledSprite = SpriteCache.get(spriteOn);
		}
		rsi.tooltip = s;
		rsi.contentType = 0;
		rsi.atActionType = 1;
		rsi.width = width;
		rsi.hoverType = 52;
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 5;
		rsi.height = height;
	}
	
	public static void changeSpriteInInterface(int id, int sprite) {
		RSInterface class9 = interfaceCache[id];
		class9.disabledSprite = SpriteCache.get(sprite);
	}
	
	public void specialBar(int id) // 7599
	{
		addActionButton(id - 12, 346, -1, 150, 26, "Use <col=ff00>Special Attack");
		for (int i = id - 11; i < id; i++)
			removeInterface(i);

		RSInterface rsi = interfaceCache[id - 12];
		rsi.width = 150;
		rsi.height = 26;
		rsi.hoverType = 40005;

		rsi = interfaceCache[id];
		rsi.width = 150;
		rsi.height = 26;

		rsi.child(0, id - 12, 0, 0);

		rsi.child(12, id + 1, 3, 7);

		rsi.child(23, id + 12, 0, 0);
		
		for (int i = 13; i < 23; i++) {
			rsi.childY[i] -= 1;
		}

		rsi = interfaceCache[id + 1];
		rsi.interfaceType = 5;
		rsi.disabledSprite = SpriteCache.get(347); // spec bar shit

		for (int i = id + 2; i < id + 12; i++) {
			rsi = interfaceCache[i];
			rsi.interfaceType = 5;
		}

		changeSpriteInInterface(id + 2, 348);
		changeSpriteInInterface(id + 3, 349);
		changeSpriteInInterface(id + 4, 350);
		changeSpriteInInterface(id + 5, 351);
		changeSpriteInInterface(id + 6, 352);
		changeSpriteInInterface(id + 7, 353);
		changeSpriteInInterface(id + 8, 354);
		changeSpriteInInterface(id + 9, 355);
		changeSpriteInInterface(id + 10, 356);
		changeSpriteInInterface(id + 11, 357);

		if (RSInterface.interfaceCache[40005] == null) {
			rsi = addInterface(40005, InterfaceType.TAB);
			rsi.isMouseoverTriggered = true;
			rsi.interfaceType = 0;
			rsi.atActionType = 0;
			rsi.hoverType = -1;
			rsi.parentID = 40005;
			rsi.id = 40005;
		}
		if (RSInterface.interfaceCache[40006] == null)
			addTooltipBox(40006, "Select to perform a special\nattack.");
		rsi.totalChildren(1);
		rsi.child(0, 40006, 0, 0);

		rsi = interfaceCache[id + 12];
		rsi.width = 150;
		rsi.height = 28;
		rsi.interfaceType = 10;
		rsi.horizontalAlignment = 1;
		rsi.verticalAlignment = 1;
	}
	
	public static void addAttackHover(int id, int hoverID, String hoverText) {
		RSInterface rsi = interfaceCache[id];
		rsi.hoverType = hoverID;

		rsi = addInterface(hoverID);
		rsi.isMouseoverTriggered = true;
		rsi.interfaceType = 0;
		rsi.atActionType = 0;
		rsi.hoverType = -1;
		rsi.parentID = hoverID;
		rsi.id = hoverID;
		if (RSInterface.interfaceCache[hoverID + 1] == null)
			addTooltipBox(hoverID + 1, hoverText);
		rsi.totalChildren(1);
		rsi.child(0, hoverID + 1, 0, 0);
	}
	
	public static void addAttackText(int id, String text,
			int idx, int color, boolean centered) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		if (centered)
			rsi.centerText = true;
		rsi.textShadow = true;
		rsi.textDrawingAreas = idx;
		rsi.message = text;
		rsi.disabledColor = color;
		rsi.id = id;
		rsi.interfaceType = 4;
	}
	
	public static void addToggleButton(int id, int spriteOff, int spriteOn, int setconfig,
			int width, int height, String s) {
		RSInterface rsi = addInterface(id);
		rsi.disabledSprite = SpriteCache.get(spriteOff);
		rsi.enabledSprite = SpriteCache.get(spriteOn);
		rsi.requiredValues = new int[1];
		rsi.requiredValues[0] = 1;
		rsi.valueCompareType = new byte[1];
		rsi.valueCompareType[0] = 1;
		rsi.valueIndexArray = new int[1][3];
		rsi.valueIndexArray[0][0] = 5;
		rsi.valueIndexArray[0][1] = setconfig;
		rsi.valueIndexArray[0][2] = 0;
		rsi.atActionType = 4;
		rsi.width = width;
		rsi.hoverType = -1;
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 5;
		rsi.height = height;
		rsi.tooltip = s;
	}

	public static void addRadioButton(int id, int spriteOff, int spriteOn, int setconfig,
			int width, int height, String s, int value) {
		RSInterface rsi = addInterface(id);
		rsi.disabledSprite = SpriteCache.get(spriteOff);
		rsi.enabledSprite = SpriteCache.get(spriteOn);
		rsi.requiredValues = new int[1];
		rsi.requiredValues[0] = value;
		rsi.valueCompareType = new byte[1];
		rsi.valueCompareType[0] = 1;
		rsi.valueIndexArray = new int[1][3];
		rsi.valueIndexArray[0][0] = 5;
		rsi.valueIndexArray[0][1] = setconfig;
		rsi.valueIndexArray[0][2] = 0;
		rsi.atActionType = 5;
		rsi.width = width;
		rsi.hoverType = -1;
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 5;
		rsi.height = height;
		rsi.tooltip = s;
	}

	public void swapInventoryItems(int i, int j) {
		int k = inv[i];
		inv[i] = inv[j];
		inv[j] = k;
		k = invStackSizes[i];
		invStackSizes[i] = invStackSizes[j];
		invStackSizes[j] = k;
	}

	public static void addCacheSprite(int id, int sprite1, String sprites) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.disabledSprite = getSprite(sprite1, aClass44, sprites, mediaIndex);
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 5;
	}

	public static Sprite getSprite(int id, RSArchive archive, String name, byte[] mediaIndex) {
		if (name.equals("headicons") && id == 0)
			return null;
		long l = (TextClass.method585(name) << 8) + (long) id;
		Sprite sprite = (Sprite) aMRUNodes_238.get(l);
		if (sprite != null)
			return sprite;
		try {
			sprite = new Sprite(archive, name, id, mediaIndex);
			aMRUNodes_238.put(sprite, l);
		} catch (Exception _ex) {
			_ex.printStackTrace();
			System.out.println("Unable to load sprite: " + name + " " + id);
			return null;
		}
		return sprite;
	}
	
	public static Sprite addSpriteModIcon(int interfaceId, int spriteIndex) {
		RSInterface rsi = interfaceCache[interfaceId] = new RSInterface();
		RSInterface.checkTaken(interfaceId);
		rsi.id = interfaceId;
		rsi.parentID = interfaceId;
		rsi.interfaceType = 5;
		rsi.atActionType = 1;
		rsi.contentType = 0;
		rsi.hoverType = 52;
		rsi.disabledSprite = Client.instance.getModIcons()[spriteIndex];
		rsi.width = rsi.disabledSprite.myWidth;
		rsi.height = rsi.disabledSprite.myHeight;
		return rsi.disabledSprite;
	}

	public static long bitPackModel(int type, int id, boolean hasRecolor) {
		return (long) ((type << 17) + id + (hasRecolor ? 1 : 0 << 16));
	}

	public Model getModel(int frame, int frame2, boolean triggered) {
		int type = 0;
		int id = -1;
		if (triggered) {
			type = enabledMediaType;
			id = enabledMediaID;
		} else {
			type = disabledMediaType;
			id = disabledMediaID;
		}

		if (type == 0 || id == -1) {
			return null;
		}

		long uid = bitPackModel(type, id, recolorOriginal != null);
		Model model = (Model) modelCache.get(uid);
		if (model == null) {
			switch (type) {
				case 1:
					model = Model.loadDropModel(id, BitwiseHelper.testBit(flags, OSRS_MODEL) ? ModelTypeEnum.OSRS : ModelTypeEnum.REGULAR);
					if (model == null) {
						return null;
					}

					if (recolorOriginal != null) {
						for (int i = 0, length = recolorOriginal.length; i < length; i++) {
							model.replaceHs1(recolorOriginal[i], recolorModified[i]);
						}
					}

					model.applyEffects();
					model.calculateLighting(64, 768, -50, -10, -50, true);
					break;
				case 2:
					model = EntityDef.forID(id).method160();
					if (model == null) {
						return null;
					}

					model.applyEffects();
					model.calculateLighting(64, 768, -50, -10, -50, true);
					break;
				case 3:
					PlayerAppearance appearance = Client.myPlayer.appearance;
					if (appearance == null) {
						return null;
					}

					model = appearance.getChatHeadModel();
					if (model == null) {
						return null;
					}

					model.applyEffects();
					model.calculateLighting(64, 768, -50, -10, -50, true);
					break;
				case 4:
					model = ItemDef.forID(id).method202(50);
					if (model == null) {
						return null;
					}

					model.applyEffects();
					model.calculateLighting(64, 768, -50, -10, -50, true);
					break;
				case 5:
					model = EntityDef.forID(id).method164();
					if (model == null) {
						return null;
					}

					model.applyEffects();
					model.calculateLighting(64, 768, -50, -10, -50, true);
					break;
				case 6:
					ItemDef objType = ItemDef.forID(id);
					model = objType.getEquippedModel(Client.myPlayer.appearance.gender, Client.customizedItem);
					if (model == null) {
						return null;
					}

					model.applyEffects();
					model.calculateLighting(64 + objType.ambient, 850 + objType.contrast, -30, -50, -30, true);
					break;
				case 7://Already used as character design.
					break;
			}

			modelCache.put(model, uid);
		}

		if (frame2 == -1 && frame == -1) {
			return model;
		}

		boolean leaveAlpha = false;
		if (frame2 != -1) {
			leaveAlpha |= Class36.hasAlpha(frame2);
		}
		if (frame != -1) {
			leaveAlpha |= Class36.hasAlpha(frame);
		}

		Model transformedModel = model.copyEntity(!leaveAlpha);
		if (frame2 != -1) {
			transformedModel.applyAnimation(frame2);
		}

		if (frame != -1) {
			transformedModel.applyAnimation(frame);
		}

		transformedModel.method466();
		return transformedModel;
	}
	
	public static RSInterface addInterface(int id) {
		return addInterface(id, InterfaceType.MAIN);
	}

	public static RSInterface addInterface(int id, InterfaceType type) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.id = id;
		rsi.width = type.getWidth();
		rsi.height = type.getHeight();
		rsi.hoverType = -1;
		return rsi;
	}

	public static RSInterface addLayer(int id, int width, int height) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.id = id;
		rsi.width = width;
		rsi.height = height;
		return rsi;
	}

	public static RSInterface addLayer(int x, int y, int width, int height) {
		RSInterface rsi = new RSInterface();
		rsi.x = x;
		rsi.y = y;
		rsi.width = width;
		rsi.height = height;
		return rsi;
	}

	public void totalChildren(int t) {
		children = new int[t];
		childX = new short[t];
		childY = new short[t];
	}

	public void createSmart(int index, int id, int x, int y) {
		if (children == null) {
			children = new int[index + 1];
			childX = new short[index + 1];
			childY = new short[index + 1];
		}

		if (index >= children.length) {
			int[] newChildren = new int[index + 1];
			short[] newChildX = new short[index + 1];
			short[] newChildY = new short[index + 1];

			for (int i = 0; i < children.length; i++) {
				newChildren[i] = children[i];
				newChildX[i] = childX[i];
				newChildY[i] = childY[i];
			}

			children = newChildren;
			childX = newChildX;
			childY = newChildY;
		}

		children[index] = id;
		childX[index] = (short) x;
		childY[index] = (short) y;
	}

	public void createDynamic(int index, RSInterface component) {
		if (dynamicComponents == null) {
			dynamicComponents = new RSInterface[index + 1];
		}

		int length = dynamicComponents.length;
		if (index >= length) {
			RSInterface[] newChildren = new RSInterface[index + 1];

			for (int i = 0; i < length; i++) {
				newChildren[i] = dynamicComponents[i];
			}

			dynamicComponents = newChildren;
		}

		dynamicComponents[index] = component;

		component.parentID = id;
		component.id = id;
		component.componentIndex = index;
	}

	public static RSInterface addContainer(int id, int width, int height, int clientCode) {
		RSInterface tab = addInterface(id);
		tab.id = id;// 250
		tab.parentID = id;// 236
		tab.interfaceType = 1337;// 262
		tab.atActionType = 0;// 217
		tab.contentType = clientCode;
		tab.width = width;// 220
		tab.height = height;// 267
		tab.hoverType = -1;// Int 230
		return tab;
	}
	
	public static void addRecoloredModel(int ID, int zoom) {
		RSInterface t = addInterface(ID);
		t.id = ID;
		t.parentID = ID;
		t.interfaceType = 6;
		t.atActionType = 0;
		t.width = 165;
		t.height = 168;
		t.hoverType = 0;
		t.modelZoom = zoom;
		t.modelRotationY = 0;
		t.modelRotationX = 1024;
		t.disabledMediaType = 6;
		t.disabledMediaID = 20769;
		t.disabledAnimation = -1;
		t.enabledAnimation = -1;
	}

	public static void addText2(int i, String string, int color, boolean centered, boolean shadow, int hoverType, int textSize) {
		RSInterface rsinterface = addInterface(i);
		rsinterface.parentID = i;
		rsinterface.id = i;
		rsinterface.interfaceType = 4;
		rsinterface.atActionType = 0;
		rsinterface.width = 0;
		rsinterface.height = 0;
		rsinterface.contentType = 0;
		rsinterface.hoverType = hoverType;
		rsinterface.centerText = centered;
		rsinterface.textShadow = shadow;
		rsinterface.textDrawingAreas = textSize;
		rsinterface.message = string;
		rsinterface.disabledColor = color;
	}

	public static void addText(int id, String text, int idx, int color, boolean center, boolean shadow) {
		RSInterface rsi = addInterface(id);
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 4;
		rsi.atActionType = 0;
		rsi.width = 0;
		rsi.height = 11;
		rsi.contentType = 0;
		rsi.hoverType = -1;
		rsi.centerText = center;
		rsi.textShadow = shadow;
		rsi.textDrawingAreas = idx;
		rsi.message = text;
		rsi.disabledColor = color;
	}

	public static RSInterface addNewText(int id, String text, int width, int height, int font, int color, int hoverColor, boolean shadow, int horizontalAlign, int verticalAlign, int verticalSpacing) {
		RSInterface rsi = addInterface(id);
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 10;
		rsi.width = width;
		rsi.height = height;
		rsi.hoverType = -1;
		rsi.textShadow = shadow;
		rsi.textDrawingAreas = font;
		rsi.message = text;
		rsi.disabledColor = color;
		rsi.horizontalAlignment = horizontalAlign;
		rsi.verticalAlignment = verticalAlign;
		rsi.verticalSpacing = verticalSpacing;
		rsi.mouseHoverTextRecolor = hoverColor;
		return rsi;
	}

	public static void addText(int id, String text, int idx, int color, boolean center,
			boolean shadow, int width) {
		RSInterface rsi = addInterface(id);
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 4;
		rsi.atActionType = 0;
		rsi.width = width;
		rsi.height = 11;
		rsi.contentType = 0;
		rsi.hoverType = -1;
		rsi.centerText = center;
		rsi.textShadow = shadow;
		rsi.textDrawingAreas = idx;
		rsi.message = text;
		rsi.disabledColor = color;
	}
	
	public static void addText(int id, String text, int idx, int color, boolean center,
			boolean shadow, int width, int height, int hoverColor) {
		RSInterface rsi = addInterface(id);
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 4;
		rsi.atActionType = 0;
		rsi.width = width;
		rsi.height = height;
		rsi.contentType = 0;
		rsi.hoverType = -1;
		rsi.centerText = center;
		rsi.textShadow = shadow;
		rsi.textDrawingAreas = idx;
		rsi.message = text;
		rsi.disabledColor = color;
		rsi.mouseHoverTextRecolor = hoverColor;
	}
	
	public static void addText(int id, int width, String text, int idx, int color, boolean center, boolean shadow) {
		RSInterface rsi = addInterface(id);
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 4;
		rsi.atActionType = 0;
		rsi.width = width;
		rsi.height = 11;
		rsi.contentType = 0;
		rsi.hoverType = -1;
		rsi.centerText = center;
		rsi.textShadow = shadow;
		rsi.textDrawingAreas = idx;
		rsi.message = text;
		rsi.disabledColor = color;
	}

	public static void addText(int id, String message, int color, boolean center, boolean textShadow, int hoverType, int fontType) {
		RSInterface rsInterface = addInterface(id);
		rsInterface.parentID = id;
		rsInterface.id = id;
		rsInterface.interfaceType = 4;
		rsInterface.atActionType = 0;
		rsInterface.width = 0;// RSInterface.fonts[fontType].getTextWidth(message);
		rsInterface.height = 0; //RSInterface.fonts[fontType].getTextHeight(fontType);
		rsInterface.contentType = 0;
		rsInterface.hoverType = hoverType;
		rsInterface.centerText = center;
		rsInterface.textShadow = textShadow;
		rsInterface.mouseHoverTextRecolor = 0xffffff;
		rsInterface.textDrawingAreas = fontType;
		rsInterface.message = message;
		rsInterface.disabledColor = color;
	}

	public static Sprite addSprite(int interfaceId, int disabledSprite) {
		return addSprite(interfaceId, disabledSprite, -1, -1, 0, -1, 20, 20, "", true);
	}
	
	public static Sprite addSprite(int interfaceId, int disabledSprite, int hoverId) {
		return addSprite(interfaceId, disabledSprite, -1, -1, 0, hoverId, 20, 20, "", true);
	}

	public static Sprite addSprite(int interfaceId, int disabledSprite, int width, int height) {
		return addSprite(interfaceId, disabledSprite, -1, -1, 0, -1, width, height, "", true);
	}

	public static Sprite addSprite(int interfaceId, int disabledSprite, int hoverId, int width, int height) {
		return addSprite(interfaceId, disabledSprite, -1, -1, 0, hoverId, width, height, "", true);
	}

	public static Sprite addSprite(int interfaceId, int disabledSprite, int hoverId, int width, int height, String text) {
		return addSprite(interfaceId, disabledSprite, -1, -1, 0, hoverId, width, height, text, true);
	}

	public static Sprite addSprite(int interfaceId, final int disabledSpriteId, final int enabledSpriteId, int varId, int varValue, final int hoverId, int width, int height, String text, final boolean hoverEnabled) {
		final RSInterface rsi = interfaceCache[interfaceId] = new RSInterface();
		RSInterface.checkTaken(interfaceId);
		rsi.id = interfaceId;
		rsi.parentID = interfaceId;
		rsi.interfaceType = 5;
		rsi.width = width;
		rsi.height = height;
		if (!text.isEmpty()) {
			rsi.atActionType = 1;
			rsi.tooltip = text;
		}

		if (disabledSpriteId != -1) {
			rsi.disabledSprite = SpriteCache.get(disabledSpriteId);
		}

		if (enabledSpriteId != -1) {
			rsi.enabledSprite = SpriteCache.get(enabledSpriteId);
		}

		if (hoverId != -1) {
			final Sprite sprite = SpriteCache.get(hoverId);
			rsi.onMouseOver = new MouseEvent() {
				@Override
				public void run() {
					rsi.disabledSprite = sprite;
					if (hoverEnabled) {
						rsi.enabledSprite = sprite;
					}
				}
			};
			rsi.onMouseLeave = new MouseEvent() {
				Sprite disabledSprite = disabledSpriteId == -1 ? null : SpriteCache.get(disabledSpriteId);
				Sprite enabledSprite = enabledSpriteId == -1 ? null : SpriteCache.get(enabledSpriteId);

				@Override
				public void run() {
					rsi.disabledSprite = disabledSprite;
					if (hoverEnabled) {
						rsi.enabledSprite = enabledSprite;
					}
				}
			};
		}

		if (varId != -1) {
			rsi.valueCompareType = new byte[1];
			rsi.requiredValues = new int[1];
			rsi.valueCompareType[0] = 1;
			rsi.requiredValues[0] = varValue;
			rsi.valueIndexArray = new int[1][3];
			rsi.valueIndexArray[0][0] = 5;
			rsi.valueIndexArray[0][1] = varId;
			rsi.valueIndexArray[0][2] = 0;
		}

		return rsi.disabledSprite;
	}

	public static void addItemSprite(int interfaceId, int itemId) {
		addItemSprite(interfaceId, itemId, 36, 32);
	}

	public static void addItemSprite(int interfaceId, int itemId, int width, int height) {
		RSInterface rsi = interfaceCache[interfaceId] = new RSInterface();
		RSInterface.checkTaken(interfaceId);
		rsi.id = interfaceId;
		rsi.interfaceType = 5;
		rsi.width = width;
		rsi.height = height;
		rsi.itemId = itemId;
	}

	public static void addHoverButton(int i, int spriteId,
	                                  int width, int height, String text, int contentType, int hoverOver,
	                                  int aT) {// hoverable button
		RSInterface tab = addTabInterface(i);
		tab.id = i;
		tab.parentID = i;
		tab.interfaceType = 5;
		tab.atActionType = aT;
		tab.contentType = contentType;
		tab.hoverType = hoverOver;
		tab.disabledSprite = SpriteCache.get(spriteId);
		tab.width = width;
		tab.height = height;
		tab.tooltip = text;
	}
	
	
	public static void addHoverButton(int i, int spriteID, int j,
	                                  int width, int height, String text, int contentType, int hoverOver,
	                                  int aT) {// hoverable button
		RSInterface tab = addTabInterface(i);
		tab.id = i;
		tab.parentID = i;
		tab.interfaceType = 5;
		tab.atActionType = aT;
		tab.contentType = contentType;
		tab.hoverType = hoverOver;
		tab.disabledSprite = SpriteCache.get(spriteID);
		tab.width = width;
		tab.height = height;
		tab.tooltip = text;
	}

	public static void addHoveredButton(int interfaceId, int spriteId, int spriteInterfaceId) {
		RSInterface rsi = addInterface(interfaceId);
		rsi.parentID = interfaceId;
		rsi.id = interfaceId;
		rsi.interfaceType = 0;
		rsi.atActionType = 0;
		
		Sprite sprite = SpriteCache.get(spriteId);
		
		rsi.width = sprite.myWidth;
		rsi.height = sprite.myHeight;
		rsi.isMouseoverTriggered = true;
		rsi.hoverType = -1;
		rsi.scrollMax = 0;
		addHoverImage(spriteInterfaceId, spriteId);
		rsi.totalChildren(1);
		rsi.child(0, spriteInterfaceId, 0, 0);
	}

	public static void addHoverImage(int interfaceId, int disabledSprite) {		
		RSInterface tab = addInterface(interfaceId);
		tab.id = interfaceId;
		tab.parentID = interfaceId;
		tab.interfaceType = 5;
		tab.atActionType = 0;
		tab.contentType = 0;
		tab.width = 512;
		tab.height = 334;
		tab.hoverType = 52;
		tab.disabledSprite = SpriteCache.get(disabledSprite);
	}

	public void child(int id, int interID, int x, int y) {
		children[id] = interID;
		childX[id] = (short) x;
		childY[id] = (short) y;

		RSInterface component = RSInterface.interfaceCache[interID];
		if (component != null) {
			component.x = x;
			component.y = y;
			component.parentID = this.id;
		}
	}

	public static void addHoverButton(int i, int disabledSprite, String text, int contentType, int hoverOver,
			int aT) {		
		RSInterface tab = addInterface(i);
		tab.id = i;
		tab.parentID = i;
		tab.interfaceType = 5;
		tab.atActionType = aT;
		tab.contentType = contentType;
		tab.hoverType = hoverOver;
		
		Sprite sprite = SpriteCache.get(disabledSprite);
		if (sprite == null) {
			System.err.println("Could not load sprite " + disabledSprite);
			return;
		}
		
		tab.disabledSprite = sprite;
		tab.width = sprite.myWidth;
		tab.height = sprite.myHeight;
		tab.tooltip = text;
	}

	public static void AddInterfaceButtons(int id, int sid, String tooltip, int mOver, int atAction) {
		RSInterface rsinterface = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsinterface.id = id;
		rsinterface.parentID = id;
		rsinterface.interfaceType = 5;
		rsinterface.atActionType = atAction;
		rsinterface.contentType = 0;
		rsinterface.hoverType = mOver;
		rsinterface.disabledSprite = SpriteCache.get(sid);
		rsinterface.width = rsinterface.disabledSprite.myWidth;
		rsinterface.height = rsinterface.disabledSprite.myHeight;
		rsinterface.tooltip = tooltip;
	}

	public static void drawTooltip(int id, String text) {
		RSInterface rsinterface = addInterface(id);
		rsinterface.parentID = id;
		rsinterface.isMouseoverTriggered = true;
		rsinterface.hoverType = -1;
		addTooltipBox(id + 1, text);
		rsinterface.totalChildren(1);
		rsinterface.child(0, id + 1, 0, 0);
	}

	public static void addTooltipBox(int id, String text) {
		RSInterface rsi = addInterface(id);
		rsi.id = id;
		rsi.parentID = id;
		rsi.interfaceType = 9;
		rsi.message = text;
	}

	public static void addTooltipBox(int id, String text, int width, int height) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.id = id;
		rsi.width = width;
		rsi.height = height;
		rsi.parentID = id;
		rsi.interfaceType = 9;
		rsi.message = text;
	}

	public static void addTextButton(int i, String s, String tooltip, int k, boolean l, boolean m,
			int j, int w) {
		RSInterface rsinterface = addInterface(i);
		rsinterface.parentID = i;
		rsinterface.id = i;
		rsinterface.interfaceType = 4;
		rsinterface.atActionType = 1;
		rsinterface.width = w;
		rsinterface.height = 16;
		rsinterface.contentType = 0;
		rsinterface.hoverType = -1;
		rsinterface.centerText = l;
		rsinterface.textShadow = m;
		rsinterface.textDrawingAreas = j;
		rsinterface.message = s;
		rsinterface.disabledColor = k;
		rsinterface.tooltip = tooltip;
	}
	
	public static void addTextButtonLesik(int i, String disabledMessage, String tooltip, int disabledColor, boolean centerText, boolean textShadow,
			int j) {
		RSFont font = Client.instance.chooseFont(j);
		RSInterface rsinterface = addInterface(i);
		rsinterface.parentID = i;
		rsinterface.id = i;
		rsinterface.interfaceType = 4;
		rsinterface.atActionType = 1;
		rsinterface.height = font.baseCharacterHeight;
		rsinterface.mouseHoverTextRecolor = 0xFFFFFF; // mouse hover text color
		rsinterface.centerText = centerText;
		rsinterface.textShadow = textShadow;
		rsinterface.textDrawingAreas = j;
		rsinterface.width = font.getTextWidth(disabledMessage);
		rsinterface.message = disabledMessage;
		rsinterface.disabledColor = disabledColor;
		rsinterface.tooltip = tooltip;
	}

	public static void AddInterfaceButton(int id, int sid, String tooltip, int mOver, int atAction, int width,
			int height) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.id = id;
		tab.parentID = id;
		tab.interfaceType = 5;
		tab.atActionType = atAction;
		tab.contentType = 0;
		tab.hoverType = mOver;
		tab.disabledSprite = SpriteCache.get(sid);
		tab.width = width;
		tab.height = height;
		tab.tooltip = tooltip;
	}

	public static void addTooltip(int id, String text) {
		RSInterface rsi = addInterface(id);
		rsi.id = id;
		rsi.interfaceType = 0;
		rsi.isMouseoverTriggered = true;
		rsi.hoverType = -1;
		addTooltipBox(id + 1, text);
		rsi.totalChildren(1);
		rsi.child(0, id + 1, 0, 0);
	}

	public static void addTooltip(int id, String text, int width, int height) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.id = id;
		rsi.width = width;
		rsi.height = height;
		rsi.isMouseoverTriggered = true;
		rsi.hoverType = -1;
		addTooltipBox(id + 1, text, width, height);
		rsi.totalChildren(1);
		rsi.child(0, id + 1, 0, 0);
	}

	public static void addChar(int ID) {
		RSInterface t = interfaceCache[ID] = new RSInterface();
		RSInterface.checkTaken(ID);
		t.id = ID;
		t.parentID = ID;
		t.interfaceType = 6;
		t.atActionType = 0;
		t.contentType = 328;
		t.width = 180;
		t.height = 168;
		t.hoverType = 0;
		t.modelZoom = 560;
		t.modelRotationY = 150;
		t.modelRotationX = 0;
		t.disabledAnimation = -1;
		t.enabledAnimation = -1;
	}

	public static void addHovered(int i, int j, int sid) {		
		RSInterface rsinterfaceHover = addInterface(i);
		rsinterfaceHover.parentID = i;
		rsinterfaceHover.id = i;
		rsinterfaceHover.interfaceType = 0;
		rsinterfaceHover.atActionType = 0;
		
		Sprite sprite = addSprite(sid, j);
		
		rsinterfaceHover.width = sprite.myWidth;
		rsinterfaceHover.height = sprite.myHeight;
		rsinterfaceHover.isMouseoverTriggered = true;
		rsinterfaceHover.hoverType = -1;
		
		rsinterfaceHover.totalChildren(1);
		rsinterfaceHover.child(0, sid, 0, 0);
	}

//	public static void addItemSpriteLesik(int i, int itemId) {
//		RSInterface rsi = addInterface(i);
//		rsi.id = i;
//		rsi.parentID = i;
//		rsi.interfaceType = 55;
////		rsi.hoverType = hoverid;
//		
//		rsi.drawItemSpriteByItemId = itemId;
//		rsi.drawItemSpriteByItemIdScale = 100;
//		rsi.drawItemSpriteByItemIdWH = 50;
//	}
	
	public static void addPouchLesik(int i, int hoverId, int disabledSpriteId, int enabledSpriteId, int hoverSpriteId, CreationData data) {
		final RSInterface rsi = addInterface(i);
		rsi.id = i;
		rsi.parentID = 1151;
		rsi.interfaceType = 5;
		rsi.atActionType = 5;
		rsi.contentType = 0;
		rsi.hoverType = hoverId;
		
		final Sprite sprite = SpriteCache.get(enabledSpriteId);
		final Sprite disabledSprite = SpriteCache.get(disabledSpriteId);
		final Sprite hoverSprite = SpriteCache.get(hoverSpriteId);
		
		rsi.onMouseOver = new MouseEvent() {
			@Override
			public void run() {
				rsi.disabledSprite = hoverSprite;
				rsi.enabledSprite = hoverSprite;
			}
		};
		rsi.onMouseLeave = new MouseEvent() {
			@Override
			public void run() {
				rsi.disabledSprite = disabledSprite;
				rsi.enabledSprite = sprite;
			}
		};

		rsi.disabledSprite = disabledSprite;
		rsi.enabledSprite = sprite; // sprite to show they have all the items required
		rsi.width = sprite.myWidth;
		rsi.height = sprite.myHeight;
		
		String name = ItemDef.forID(data.getPouchId()).name;
		
		int r1 = Creation.POUCH_ID;
		int ra1 = 1;
		int r2 = Creation.SHARD_ID;
		int ra2 = data.getShardsRequired();
		int r3 = data.getRequiredItems()[0];
		int ra3 = 1;
		int r4 = data.getRequiredItems()[1];
		int ra4 = 1;

		rsi.tooltip = "Infuse <col=ffb000>" + name;
		rsi.targetName = name;
		rsi.valueCompareType = new byte[5];
		rsi.requiredValues = new int[5];
		rsi.valueCompareType[0] = 3;
		rsi.requiredValues[0] = ra1-1; // required amount?
		rsi.valueCompareType[1] = 3;
		rsi.requiredValues[1] = ra2-1; // required amount?
		rsi.valueCompareType[2] = 3;
		rsi.requiredValues[2] = ra3-1; // required amount?
		rsi.valueCompareType[3] = 3;
		rsi.requiredValues[3] = ra4-1; // required amount?
		rsi.valueCompareType[4] = 3;
		rsi.requiredValues[4] = data.getLevelRequired() - 1;
		
		rsi.valueIndexArray = new int[5][];
		rsi.valueIndexArray[0] = new int[4];
		rsi.valueIndexArray[0][0] = 4;
		rsi.valueIndexArray[0][1] = 3214;
		rsi.valueIndexArray[0][2] = r1;
		rsi.valueIndexArray[0][3] = 0;
		rsi.valueIndexArray[1] = new int[4];
		rsi.valueIndexArray[1][0] = 4;
		rsi.valueIndexArray[1][1] = 3214;
		rsi.valueIndexArray[1][2] = r2;
		rsi.valueIndexArray[1][3] = 0;
		rsi.valueIndexArray[2] = new int[4];
		rsi.valueIndexArray[2][0] = 4;
		rsi.valueIndexArray[2][1] = 3214;
		rsi.valueIndexArray[2][2] = r3;
		rsi.valueIndexArray[2][3] = 0;
		rsi.valueIndexArray[3] = new int[4];
		rsi.valueIndexArray[3][0] = 4;
		rsi.valueIndexArray[3][1] = 3214;
		rsi.valueIndexArray[3][2] = r4;
		rsi.valueIndexArray[3][3] = 0;

		rsi.valueIndexArray[4] = new int[3];
		rsi.valueIndexArray[4][0] = 1;
		rsi.valueIndexArray[4][1] = 6;
		rsi.valueIndexArray[4][2] = 0;
		
		
		
		RSInterface hover = addInterface(hoverId);
        hover.hoverType = -1;
        hover.isMouseoverTriggered = true;
        hover.totalChildren(11);
        //addSprite(hoverId + 1, 0, "Lunar/BOX");
        hover.child(0, 39990, 0, 0);
        addText(hoverId + 2, (new StringBuilder()).append("Level ").append(data.getLevelRequired()).append(": ").append(name).toString(), 0xff981f, true, true, 52, 1);
        hover.child(1, hoverId + 2, 90, 4);
        //addText(hoverId + 3, "This item requires", 0xaf6a1a, true, true, 52, 0);
        hover.child(2, 39991, 90, 19);
        addRuneText(hoverId + 4, ra1, r1);
        hover.child(3, hoverId + 4, 20, 66);
        addItemSprite(hoverId + 5, r1);
        hover.child(4, hoverId + 5, 4, 33);
        
        addRuneText(hoverId + 6, ra2, r2);
        hover.child(5, hoverId + 6, 65, 66);
        addItemSprite(hoverId + 7, r2);
        hover.child(6, hoverId + 7, 49, 33);
        
        addRuneText(hoverId + 8, ra3, r3);
        hover.child(7, hoverId + 8, 110, 66);
        addItemSprite(hoverId + 9, r3);
        hover.child(8, hoverId + 9, 94, 33);
        
        addRuneText(hoverId + 10, ra4, r4);
        hover.child(9, hoverId + 10, 150, 66);
        addItemSprite(hoverId + 11, r4);
        hover.child(10, hoverId + 11, 134, 33);
	}
	
	public static void addScrollLesik(int i, int hoverId, int disabledSpriteId, int enabledSpriteId, int hoverSpriteId, CreationData data) {
		final RSInterface rsi = addInterface(i);
		rsi.id = i;
		rsi.parentID = 1151;
		rsi.interfaceType = 5;
		rsi.atActionType = 5;
		rsi.hoverType = hoverId;
		
		final Sprite sprite = SpriteCache.get(enabledSpriteId);
		final Sprite disabledSprite = SpriteCache.get(disabledSpriteId);
		
		final Sprite hoverSprite = SpriteCache.get(hoverSpriteId);
		
		rsi.onMouseOver = new MouseEvent() {
			@Override
			public void run() {
				rsi.disabledSprite = hoverSprite;
				rsi.enabledSprite = hoverSprite;
			}
		};
		rsi.onMouseLeave = new MouseEvent() {
			@Override
			public void run() {
				rsi.disabledSprite = disabledSprite;
				rsi.enabledSprite = sprite;
			}
		};

		rsi.disabledSprite = disabledSprite;
		rsi.enabledSprite = sprite;
		
		rsi.width = sprite.myWidth;
		rsi.height = sprite.myHeight;
		
		int requiredItem = data.getPouchId();
		String name = ItemDef.forID(data.getScrollId()).name;
		
		rsi.tooltip = "Transform <col=ff9040>" + name;

		rsi.targetName = name;
		rsi.valueCompareType = new byte[2];
		rsi.requiredValues = new int[2];
		rsi.valueCompareType[0] = 3;
		rsi.requiredValues[0] = 1-1; // required amount
		rsi.valueCompareType[1] = 3;
		rsi.requiredValues[1] = data.getLevelRequired() - 1;
		rsi.valueIndexArray = new int[2][];
		
		rsi.valueIndexArray[0] = new int[4];
		rsi.valueIndexArray[0][0] = 4;
		rsi.valueIndexArray[0][1] = 3214;
		rsi.valueIndexArray[0][2] = requiredItem;
		rsi.valueIndexArray[0][3] = 0;

		rsi.valueIndexArray[1] = new int[3];
		rsi.valueIndexArray[1][0] = 1;
		rsi.valueIndexArray[1][1] = 6;
		rsi.valueIndexArray[1][2] = 0;
		
		
		
		RSInterface hover = addInterface(hoverId);
        hover.hoverType = -1;
        hover.isMouseoverTriggered = true;
        hover.totalChildren(5);
        addText(hoverId + 1, (new StringBuilder()).append("Level ").append(data.getLevelRequired()).append(": ").append(name).toString(), 0xff981f, true, true, 52, 1);
        addRuneText(hoverId + 2, 1, requiredItem);
        addItemSprite(hoverId + 3, requiredItem);
        hover.child(0, 39990, 0, 0); // hover box, lunar box?
        hover.child(1, hoverId + 1, 90, 4);
        hover.child(2, 39992, 90, 19);
        hover.child(3, hoverId + 2, 87, 66);
        hover.child(4, hoverId + 3, 70, 33);
	}

	public static void addHover(int i, int aT, int cT, int hoverid, int sId, String tip) {
		RSInterface rsinterfaceHover = addInterface(i);
		rsinterfaceHover.id = i;
		rsinterfaceHover.parentID = i;
		rsinterfaceHover.interfaceType = 5;
		rsinterfaceHover.atActionType = aT;
		rsinterfaceHover.contentType = cT;
		rsinterfaceHover.hoverType = hoverid;
		
		Sprite sprite = SpriteCache.get(sId);
		
		rsinterfaceHover.disabledSprite = sprite;
		rsinterfaceHover.width = sprite.myWidth;
		rsinterfaceHover.height = sprite.myHeight;
		rsinterfaceHover.tooltip = tip;
	}

	public static void addButton(int id, int sid, String tooltip, int mOver, int atAction, int width, int height) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.id = id;
		tab.parentID = id;
		tab.interfaceType = 5;
		tab.atActionType = atAction;
		tab.contentType = 0;
		tab.hoverType = mOver;
		tab.disabledSprite = SpriteCache.get(sid);
		tab.width = width;
		tab.height = height;
		tab.tooltip = tooltip;
	}

	public static void addButton(int id, int sid, String tooltip) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.id = id;
		tab.parentID = id;
		tab.interfaceType = 5;
		tab.atActionType = 1;
		tab.contentType = 0;
		tab.hoverType = 52;
		tab.disabledSprite = SpriteCache.get(sid);
		tab.width = tab.disabledSprite.myWidth;
		tab.height = tab.disabledSprite.myHeight;
		tab.tooltip = tooltip;
	}

	public static RSInterface addButton(int id, final int spriteId, final int hoverSpriteId, String option) {
		final RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.id = id;
		rsi.interfaceType = 5;

		if (option != null && !option.isEmpty()) {
			rsi.atActionType = 1;
			rsi.tooltip = option;
		}

		rsi.atActionType = 1;
		if (spriteId != -1) {
			rsi.disabledSprite = SpriteCache.get(spriteId);
			if (rsi.disabledSprite != null) {
				rsi.width = rsi.disabledSprite.myWidth;
				rsi.height = rsi.disabledSprite.myHeight;
			}
		}

		if (hoverSpriteId != -1) {
			final Sprite sprite = SpriteCache.get(hoverSpriteId);
			rsi.onMouseOver = new MouseEvent() {
				@Override
				public void run() {
					rsi.disabledSprite = sprite;
				}
			};

			final Sprite revertSprite = spriteId == -1 ? null : SpriteCache.get(spriteId);
			rsi.onMouseLeave = new MouseEvent() {

				@Override
				public void run() {
					rsi.disabledSprite = revertSprite;
				}
			};
		}

		return rsi;
	}
	
	public static void addHoverOnly(int id, final int sid, final int hoveredSpriteId, int bID2, int width, int height,
			int configID, int aT, int configFrame) {
		final RSInterface tab = addInterface(id);
		tab.id = id;
		tab.interfaceType = 5;
		tab.atActionType = aT;
		tab.disabledSprite = SpriteCache.get(sid);
		tab.valueCompareType = new byte[1];
		tab.requiredValues = new int[1];
		tab.valueCompareType[0] = 1;
		tab.requiredValues[0] = configID;
		tab.valueIndexArray = new int[1][3];
		tab.valueIndexArray[0][0] = 5;
		tab.valueIndexArray[0][1] = configFrame;
		tab.valueIndexArray[0][2] = 0;

		if (bID2 != -1) {
			tab.enabledSprite = SpriteCache.get(bID2);
		}
		tab.onMouseOver = new MouseEvent() {
			@Override
			public void run() {
				tab.disabledSprite = SpriteCache.get(hoveredSpriteId);
			}
		};
		tab.onMouseLeave = new MouseEvent() {
			@Override
			public void run() {
				tab.disabledSprite = SpriteCache.get(sid);
			}
		};
		if (tab.disabledSprite != null) {
			tab.width = width;
			tab.height = height;
		}
	}

	public static void addRadioButton(int ID, int pID, int bID, int bID2, int width, int height, String tT,
			int configID, int aT, int configFrame) {
		RSInterface Tab = addInterface(ID);
		Tab.parentID = pID;
		Tab.id = ID;
		Tab.interfaceType = 5;
		Tab.atActionType = aT;
		Tab.contentType = 0;
		Tab.width = width;
		Tab.height = height;
		Tab.hoverType = -1;
		Tab.valueCompareType = new byte[1];
		Tab.requiredValues = new int[1];
		Tab.valueCompareType[0] = 1;
		Tab.requiredValues[0] = configID;
		Tab.valueIndexArray = new int[1][3];
		Tab.valueIndexArray[0][0] = 5;
		Tab.valueIndexArray[0][1] = configFrame;
		Tab.valueIndexArray[0][2] = 0;

		if (bID != -1) {
			Tab.disabledSprite = SpriteCache.get(bID);
		}

		if (bID2 != -1) {
			Tab.enabledSprite = SpriteCache.get(bID2);
		}
		Tab.tooltip = tT;
	}

	public static void setRadioButtonVarp(int id, int varpId, int varpValue, int spriteId) {
		RSInterface Tab = interfaceCache[id];
		Tab.valueCompareType = new byte[1];
		Tab.requiredValues = new int[1];
		Tab.valueCompareType[0] = 1;
		Tab.requiredValues[0] = varpValue;
		Tab.valueIndexArray = new int[1][3];
		Tab.valueIndexArray[0][0] = 5;
		Tab.valueIndexArray[0][1] = varpId;
		Tab.valueIndexArray[0][2] = 0;
		Tab.enabledSprite = SpriteCache.get(spriteId);
	}

	public static RSInterface addScreenInterface(int id) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.id = id;
		tab.parentID = id;
		tab.interfaceType = 0;
		tab.atActionType = 0;
		tab.contentType = 0;
		tab.width = 512;
		tab.height = 334;
		tab.hoverType = 0;
		return tab;
	}

	public static void addHead2(int i, int j, int k, int l) { // summon tab only
		RSInterface rsinterface = addTab(i);
		rsinterface.parentID = 17011;
		rsinterface.interfaceType = 6;
		rsinterface.modelZoom = l;
		rsinterface.modelRotationY = 40;
		rsinterface.modelRotationX = 1900;
		rsinterface.height = k;
		rsinterface.width = j;
		rsinterface.hoverType = -1;
	}
	
	public static void addModel(int id, int width, int height, int rotationX, int rotationY, int zoom, int modelId, int modelType, int animId, int clientCode) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.id = id;
		tab.parentID = id;
		tab.interfaceType = 6;
		tab.width = width;
		tab.disabledAnimation = animId;
		tab.height = height;
		tab.modelRotationX = rotationX;
		tab.modelRotationY = rotationY;
		tab.contentType = clientCode;
		tab.modelZoom = zoom;
		tab.disabledMediaType = modelType;
		tab.disabledMediaID = modelId;
	}
	
	public static void addAPlayerHead(int interfaceID) {
		RSInterface rsi = addTabInterface(interfaceID);
		rsi.interfaceType = 6;
		rsi.disabledMediaType = 11;
		rsi.enabledMediaType = 11;//(a.getIDKHead() <= 0 ? 11 : 10); //lol
		int anim = /*headAnims[Client.getRandom(headAnims.length - 1, false)]*/9846;
		//rsi.enabledAnimationId = rsi.disabledAnimationId = anim;
		rsi.disabledMediaID = 6;
		rsi.enabledMediaID = 6;//a.appearance[0];//rsi.mediaID = (a.getIDKHead() <= 0 ? a.getHelmet() : a.getIDKHead());
		rsi.modelZoom = 2000;
		rsi.modelRotationY = 40;
		rsi.modelRotationX = 1800;
		rsi.height = 150;
		rsi.width = 150;
	}

	public static RSInterface addTab(int i) {
		RSInterface rsinterface = interfaceCache[i] = new RSInterface();
		RSInterface.checkTaken(i);
		rsinterface.id = i;
		rsinterface.parentID = i;
		rsinterface.interfaceType = 0;
		rsinterface.atActionType = 0;
		rsinterface.contentType = 0;
		rsinterface.width = 512;
		rsinterface.height = 334;
		rsinterface.hoverType = 0;
		return rsinterface;
	}
	
	public static void addBankHover1(int interfaceID, int actionType,
			int hoverid, int spriteId, int Width, int Height,
			String Tooltip, int hoverId2, int hoverSpriteId,
			String hoverSpriteName, int hoverId3, String hoverDisabledText,
			int X, int Y) {
		RSInterface hover = addInterface(interfaceID);
		hover.id = interfaceID;
		hover.parentID = interfaceID;
		hover.interfaceType = 5;
		hover.atActionType = actionType;
		hover.contentType = 0;
		hover.hoverType = hoverid;
		hover.disabledSprite = SpriteCache.get(spriteId);
		hover.width = Width;
		hover.tooltip = Tooltip;
		hover.height = Height;
		hover = addInterface(hoverid);
		hover.parentID = hoverid;
		hover.id = hoverid;
		hover.interfaceType = 0;
		hover.atActionType = 0;
		hover.width = 550;
		hover.height = 334;
		hover.isMouseoverTriggered = true;
		hover.hoverType = -1;
		addVarSprite(hoverId2, hoverSpriteId, hoverSpriteId, 0, 0);
		addHoverBox(hoverId3, interfaceID, hoverDisabledText,
				hoverDisabledText, 0, 0);
		hover.totalChildren(2);
		hover.child(0, hoverId2, 15, 60);
		hover.child(1, hoverId3, X, Y);
	}
	
	public static void addBankHover(int interfaceID, int actionType,
			int hoverid, int spriteId, int spriteId2, int Width,
			int Height, int configFrame, int configId, String Tooltip,
			int hoverId2, int hoverSpriteId, int hoverSpriteId2,
			String hoverSpriteName, int hoverId3, String hoverDisabledText,
			String hoverEnabledText, int X, int Y) {
		RSInterface hover = addInterface(interfaceID);
		hover.id = interfaceID;
		hover.parentID = interfaceID;
		hover.interfaceType = 5;
		hover.atActionType = actionType;
		hover.contentType = 0;
		hover.hoverType = hoverid;
		hover.disabledSprite = SpriteCache.get(spriteId);
		hover.enabledSprite = SpriteCache.get(spriteId2);
		hover.width = Width;
		hover.tooltip = Tooltip;
		hover.height = Height;
		hover.valueCompareType = new byte[1];
		hover.requiredValues = new int[1];
		hover.valueCompareType[0] = 1;
		hover.requiredValues[0] = configId;
		hover.valueIndexArray = new int[1][3];
		hover.valueIndexArray[0][0] = 5;
		hover.valueIndexArray[0][1] = configFrame;
		hover.valueIndexArray[0][2] = 0;
		hover = addInterface(hoverid);
		hover.parentID = hoverid;
		hover.id = hoverid;
		hover.interfaceType = 0;
		hover.atActionType = 0;
		hover.width = 550;
		hover.height = 334;
		hover.isMouseoverTriggered = true;
		hover.hoverType = -1;
		addVarSprite(hoverId2, hoverSpriteId, hoverSpriteId2,
				configId, configFrame);
		addHoverBox(hoverId3, interfaceID, hoverDisabledText, hoverEnabledText,
				configId, configFrame);
		hover.totalChildren(2);
		hover.child(0, hoverId2, 15, 60);
		hover.child(1, hoverId3, X, Y);
	}
	
	public static void addVarSprite(int ID, int i, int i2,
			int configId, int configFrame) {
		RSInterface Tab = addInterface(ID);
		Tab.id = ID;
		Tab.parentID = ID;
		Tab.interfaceType = 5;
		Tab.atActionType = 0;
		Tab.contentType = 0;
		Tab.width = 512;
		Tab.height = 334;
		Tab.valueCompareType = new byte[1];
		Tab.requiredValues = new int[1];
		Tab.valueCompareType[0] = 1;
		Tab.requiredValues[0] = configId;
		Tab.valueIndexArray = new int[1][3];
		Tab.valueIndexArray[0][0] = 5;
		Tab.valueIndexArray[0][1] = configFrame;
		Tab.valueIndexArray[0][2] = 0;
		Tab.disabledSprite = SpriteCache.get(i);
		Tab.enabledSprite = SpriteCache.get(i2);
	}
	
	public static RSInterface addTabInterface(int id) {
		RSInterface tab = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		tab.id = id;// 250
		tab.parentID = id;// 236
		tab.interfaceType = 0;// 262
		tab.atActionType = 0;// 217
		tab.contentType = 0;
		tab.width = 512;// 220
		tab.height = 700;// 267
		tab.hoverType = -1;// Int 230
		return tab;
	}
	
	public static void addBankItem(int index, Boolean hasOption) {
		RSInterface rsi = interfaceCache[index] = new RSInterface();
		RSInterface.checkTaken(index);
		//rsi.actions = new String[5];
		rsi.spritesX = new short[1];
		rsi.invStackSizes = new int[1];
		rsi.inv = new int[1];
		rsi.spritesY = new short[1];

		rsi.children = new int[0];
		rsi.childX = new short[0];
		rsi.childY = new short[0];

		rsi.isInventoryInterface = false;
		rsi.deletesTargetSlot = false;
		rsi.usableItemInterface = false;
		rsi.invStackSizes[0] = 0;
		rsi.invSpritePadX = 24;
		rsi.invSpritePadY = 24;
		rsi.height = 1;
		rsi.width = 1;
		rsi.parentID = 5292;
		rsi.id = index;
		rsi.interfaceType = 2;
		
		rsi.atActionType = 0;
		rsi.contentType = 0;
	}
	
	public static void addInventory(int id, int rows, int columns, int offX, int offY, boolean draggable, String... ops) {
		RSInterface rsi = interfaceCache[id] = new RSInterface();
		RSInterface.checkTaken(id);
		rsi.inv = new int[columns * rows];
		rsi.invStackSizes = new int[columns * rows];
		rsi.spritesX = new short[20];
		rsi.spritesY = new short[20];
		rsi.sprites = new Sprite[20];
		rsi.invSpritePadX = offX;
		rsi.invSpritePadY = offY;
		rsi.height = columns;
		rsi.width = rows;
		rsi.parentID = id;
		rsi.id = id;
		rsi.interfaceType = 2;
		rsi.aBoolean259 = draggable;

		int length = ops.length;
		if (length > 0) {
			rsi.actions = new String[RIGHT_CLICK_ACTIONS_SIZE];
			for (int i = 0; i < length; i++) {
				rsi.actions[i] = ops[i];
			}
		}
	}
	
	public static void addHoverBox(int id, int ParentID, String text,
			String text2, int configId, int configFrame) {
		addTooltipBox(id, text);
		RSInterface rsi = RSInterface.interfaceCache[id];
		rsi.enabledMessage = text2;
		rsi.valueCompareType = new byte[1];
		rsi.requiredValues = new int[1];
		rsi.valueCompareType[0] = 1;
		rsi.requiredValues[0] = configId;
		rsi.valueIndexArray = new int[1][3];
		rsi.valueIndexArray[0][0] = 5;
		rsi.valueIndexArray[0][1] = configFrame;
		rsi.valueIndexArray[0][2] = 0;
	}
	
	public static void addClickableText
	(int i, String s, String[] actionsss, int k, boolean l, boolean m, int j, int w, int h) {
		RSInterface rsinterface = addInterface(i);
		rsinterface.parentID = i;
		rsinterface.id = i;
		rsinterface.interfaceType = 4;
		rsinterface.atActionType = 1;
		rsinterface.width = w;
		rsinterface.height = h;
		rsinterface.contentType = 0;
		rsinterface.hoverType = -1;
		rsinterface.centerText = l;
		rsinterface.textShadow = m;
		rsinterface.textDrawingAreas = j;
		rsinterface.message = s;
		rsinterface.disabledColor = 0xFF981F;
		rsinterface.actions = actionsss;
	}
	
	public static void addText(int id, String text, int idx,
			int color) {
		RSInterface rsinterface = addInterface(id);
		rsinterface.id = id;
		rsinterface.parentID = id;
		rsinterface.interfaceType = 4;
		rsinterface.atActionType = 0;
		rsinterface.width = 174;
		rsinterface.height = 11;
		rsinterface.contentType = 0;
		rsinterface.hoverType = -1;
		rsinterface.centerText = false;
		rsinterface.textShadow = true;
		rsinterface.textDrawingAreas = idx;
		rsinterface.message = text;
		rsinterface.disabledColor = color;
	}
	
	public static void addPrayer(int i, int configId, int configFrame, int requiredValues, int prayerSpriteID, String PrayerName, int Hover) {
		RSInterface Interface = addTabInterface(i);
		Interface.id = i;
		Interface.parentID = 22500;
		Interface.interfaceType = 5;
		Interface.atActionType = 4;
		Interface.contentType = 0;
		Interface.hoverType = Hover;
		Interface.disabledSprite = SpriteCache.get(332);
		Interface.enabledSprite = null;
		Interface.width = 34;
		Interface.height = 34;
		Interface.valueCompareType = new byte[1];
		Interface.requiredValues = new int[1];
		Interface.valueCompareType[0] = 1;
		Interface.requiredValues[0] = configId;
		Interface.valueIndexArray = new int[1][3];
		Interface.valueIndexArray[0][0] = 5;
		Interface.valueIndexArray[0][1] = configFrame;
		Interface.valueIndexArray[0][2] = 0;
		Interface.tooltip = "Activate<col=ff9040> " + PrayerName;
		Interface = addTabInterface(i + 1);
		Interface.id = i + 1;
		Interface.parentID = 22500;
		Interface.interfaceType = 5;
		Interface.atActionType = 0;
		Interface.contentType  = 0;
		Interface.disabledSprite = SpriteCache.get(379 + prayerSpriteID);
		Interface.enabledSprite =  SpriteCache.get(359 + prayerSpriteID);
		Interface.width = 34;
		Interface.height = 34;
		Interface.valueCompareType = new byte[1];
		Interface.requiredValues = new int[1];
		Interface.valueCompareType[0] = 2;
		Interface.requiredValues[0] = requiredValues + 1;
		Interface.valueIndexArray = new int[1][3];
		Interface.valueIndexArray[0][0] = 2;
		Interface.valueIndexArray[0][1] = 5;
		Interface.valueIndexArray[0][2] = 0;
	}
	
	public static void addPrayer1(int i, int configId, int configFrame, int requiredValues, int disabledPrayerSpriteID, int enabledPrayerSpriteID, String PrayerName, int Hover) {
		RSInterface Interface = addTabInterface(i);
		Interface.id = i;
		Interface.parentID = 22500;
		Interface.interfaceType = 5;
		Interface.atActionType = 4;
		Interface.contentType = 0;
		Interface.hoverType = Hover;
		Interface.disabledSprite = SpriteCache.get(332);
		Interface.enabledSprite = null;
		Interface.width = 34;
		Interface.height = 34;
		Interface.valueCompareType = new byte[1];
		Interface.requiredValues = new int[1];
		Interface.valueCompareType[0] = 1;
		Interface.requiredValues[0] = configId;
		Interface.valueIndexArray = new int[1][3];
		Interface.valueIndexArray[0][0] = 5;
		Interface.valueIndexArray[0][1] = configFrame;
		Interface.valueIndexArray[0][2] = 0;
		Interface.tooltip = "Activate<col=ff9040> " + PrayerName;
		Interface = addTabInterface(i + 1);
		Interface.id = i + 1;
		Interface.parentID = 22500;
		Interface.interfaceType = 5;
		Interface.atActionType = 0;
		Interface.contentType  = 0;
		Interface.disabledSprite = SpriteCache.get(disabledPrayerSpriteID);
		Interface.enabledSprite =  SpriteCache.get(enabledPrayerSpriteID);
		Interface.width = 34;
		Interface.height = 34;
		Interface.valueCompareType = new byte[1];
		Interface.requiredValues = new int[1];
		Interface.valueCompareType[0] = 2;
		Interface.requiredValues[0] = requiredValues + 1;
		Interface.valueIndexArray = new int[1][3];
		Interface.valueIndexArray[0][0] = 2;
		Interface.valueIndexArray[0][1] = 5;
		Interface.valueIndexArray[0][2] = 0;
	}

	public static void addPrayer(int i, int configId, int configFrame,
			int requiredValues, int prayOnSpriteId, int prayOffSpriteId, String prayerName) {
		RSInterface tab = addInterface(i, InterfaceType.TAB);
		tab.id = i;
		tab.parentID = 5608;
		tab.interfaceType = 5;
		tab.atActionType = 4;
		tab.contentType = 0;
		tab.hoverType = -1;
		tab.disabledSprite = SpriteCache.get(332);
		tab.enabledSprite = null;
		tab.width = 34;
		tab.height = 34;
		tab.valueCompareType = new byte[1];
		tab.requiredValues = new int[1];
		tab.valueCompareType[0] = 1;
		tab.requiredValues[0] = configId;
		tab.valueIndexArray = new int[1][3];
		tab.valueIndexArray[0][0] = 5;
		tab.valueIndexArray[0][1] = configFrame;
		tab.valueIndexArray[0][2] = 0;
		tab.tooltip = "Activate<col=ff7000> " + prayerName;
//		tab.tooltip = "Select";
		RSInterface tab2 = addInterface(i + 1, InterfaceType.TAB);
		tab2.id = i + 1;
		tab2.parentID = 5608;
		tab2.interfaceType = 5;
		tab2.atActionType = 0;
		tab2.contentType = 0;
		tab2.hoverType = -1;
		tab2.disabledSprite = SpriteCache.get(prayOnSpriteId); //imageLoader(spriteID, "Prayer/PRAYON");
		tab2.enabledSprite = SpriteCache.get(prayOffSpriteId); //imageLoader(spriteID, "Prayer/PRAYOFF");
		tab2.width = 34;
		tab2.height = 34;
		tab2.valueCompareType = new byte[1];
		tab2.requiredValues = new int[1];
		tab2.valueCompareType[0] = 2;
		tab2.requiredValues[0] = requiredValues + 1;
		tab2.valueIndexArray = new int[1][3];
		tab2.valueIndexArray[0][0] = 2;
		tab2.valueIndexArray[0][1] = 5;
		tab2.valueIndexArray[0][2] = 0;
	}
	
	/**
	 * Adds your current character to an interface.
	 **/

	protected static void addOldPrayer(int id, String prayerName) {
		RSInterface rsi = interfaceCache[id];
		rsi.tooltip = "Activate<col=ff7000> " + prayerName;
	}
	
	public static void addPrayerHover(int i, int hoverID, int prayerSpriteID,
			String hoverText) {
		RSInterface Interface = addInterface(i, InterfaceType.TAB);
		Interface.id = i;
		Interface.parentID = 5608;
		Interface.interfaceType = 5;
		Interface.atActionType = 0;
		Interface.contentType = 0;
		Interface.hoverType = hoverID;
		Interface.enabledSprite = null; // imageLoader(0, "tabs/prayer/hover/PRAYERH");
		Interface.disabledSprite = null; // imageLoader(0, "tabs/prayer/hover/PRAYERH");
		Interface.width = 34;
		Interface.height = 34;

		Interface = addInterface(hoverID, InterfaceType.TAB);
		Interface.id = hoverID;
		Interface.parentID = 5608;
		Interface.interfaceType = 0;
		Interface.atActionType = 0;
		Interface.contentType = 0;
		Interface.hoverType = -1;
		Interface.width = 512;
		Interface.height = 334;
		Interface.isMouseoverTriggered = true;
		addTooltipBox(hoverID + 1, hoverText);
		Interface.totalChildren(1);
		Interface.child(0, hoverID + 1, 0, 0);
	}
	
	public static void removeInterface(int id) {
		interfaceCache[id] = new RSInterface();
	}
	
	public static void printUnusedInterfaces() {
		final PrintWriter writer;
		try {
			writer = new PrintWriter(new File("unused_interfaces.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		try {
			for (int index = 0; index < interfaceCache.length; index++) {
				RSInterface rsi = interfaceCache[index];
				
				if (rsi == null) {
					writer.println(index);
				}
			}
		} finally {
			writer.close();
		}
	}

	@Override
	public String toString() {
		return "RSInterface [disabledSprite=" + disabledSprite + ", anInt208=" + anInt208 + ", sprites=" + Arrays.toString(sprites) + ", requiredValues=" + Arrays.toString(requiredValues) + ", contentType=" + contentType + ", spritesX=" + Arrays.toString(spritesX) + ", anInt216="
				+ mouseHoverTextRecolor + ", atActionType=" + atActionType + ", spellName=" + targetName + ", enabledColor=" + enabledColor + ", width=" + width + ", tooltip=" + tooltip + ", selectedActionName=" + targetVerb + ", centerText=" + centerText + ", scrollPosition=" + scrollPosition + ", actions=" + Arrays.toString(actions) + ", valueIndexArray=" + ObjectDef.twoDArrayToString(valueIndexArray)
				+ ", filled=" + filled + ", enabledMessage=" + enabledMessage + ", hoverType=" + hoverType + ", invSpritePadX=" + invSpritePadX + ", disabledColor=" + disabledColor + ", disabledMediaType=" + disabledMediaType + ", disabledMediaID=" + disabledMediaID + ", deletesTargetSlot=" + deletesTargetSlot + ", parentID=" + parentID + ", targetMask=" + targetMask.getMask() + ", anInt239="
				+ anInt239 + ", children=" + Arrays.toString(children) + ", childX=" + Arrays.toString(childX) + ", usableItemInterface=" + usableItemInterface + ", textDrawingAreas=" + textDrawingAreas + ", invSpritePadY=" + invSpritePadY + ", valueCompareType=" + Arrays.toString(valueCompareType) + ", anInt246=" + anInt246 + ", spritesY=" + Arrays.toString(spritesY) + ", message=" + message
				+ ", isInventoryInterface=" + isInventoryInterface + ", id=" + id + ", invStackSizes=" + Arrays.toString(invStackSizes) + ", inv=" + Arrays.toString(inv) + ", opacity=" + transparency + ", enabledMediaType=" + enabledMediaType + ", enabledMediaID=" + enabledMediaID + ", disabledAnimation=" + disabledAnimation + ", enabledAnimation=" + enabledAnimation + ", aBoolean259=" + aBoolean259
				+ ", enabledSprite=" + enabledSprite + ", scrollMax=" + scrollMax + ", interfaceType=" + interfaceType + ", xOffset=" + xOffset + ", aBoolean251=" + aBoolean251 + ", yOffset=" + yOffset + ", isMouseoverTriggered=" + isMouseoverTriggered + ", height=" + height + ", textShadow=" + textShadow + ", modelZoom=" + modelZoom + ", modelRotationY=" + modelRotationY + ", modelRotationX="
				+ modelRotationX + ", childY=" + Arrays.toString(childY) + "]";
	}

	public static void addLunar3RunesSmallBox2(int interfaceId, int r1, int r2, int r3,
											  int ra1, int ra2, int ra3, int rune1, int rune2, int lvl,
											  String name, String descr, int esid, int dsid, int suo, int type) {
		RSInterface rsInterface = addInterface(interfaceId);
		rsInterface.id = interfaceId;
		rsInterface.parentID = 1151;
		rsInterface.interfaceType = 5;
		rsInterface.atActionType = type;
		rsInterface.contentType = 0;
		rsInterface.hoverType = interfaceId + 1;
		rsInterface.targetMask = new InterfaceTargetMask(suo);
		rsInterface.targetVerb = "Cast on";
		rsInterface.width = 20;
		rsInterface.height = 20;
		rsInterface.tooltip = "Cast <col=ff00>" + name;
		rsInterface.targetName = name;
		rsInterface.valueCompareType = new byte[4];
		rsInterface.requiredValues = new int[4];
		rsInterface.valueCompareType[0] = 3;
		rsInterface.requiredValues[0] = ra1;
		rsInterface.valueCompareType[1] = 3;
		rsInterface.requiredValues[1] = ra2;
		rsInterface.valueCompareType[2] = 3;
		rsInterface.requiredValues[2] = ra3;
		rsInterface.valueCompareType[3] = 3;
		rsInterface.requiredValues[3] = lvl;
		rsInterface.valueIndexArray = new int[4][];
		rsInterface.valueIndexArray[0] = new int[4];
		rsInterface.valueIndexArray[0][0] = 4;
		rsInterface.valueIndexArray[0][1] = 3214;
		rsInterface.valueIndexArray[0][2] = r1;
		rsInterface.valueIndexArray[0][3] = 0;
		rsInterface.valueIndexArray[1] = new int[4];
		rsInterface.valueIndexArray[1][0] = 4;
		rsInterface.valueIndexArray[1][1] = 3214;
		rsInterface.valueIndexArray[1][2] = r2;
		rsInterface.valueIndexArray[1][3] = 0;
		rsInterface.valueIndexArray[2] = new int[4];
		rsInterface.valueIndexArray[2][0] = 4;
		rsInterface.valueIndexArray[2][1] = 3214;
		rsInterface.valueIndexArray[2][2] = r3;
		rsInterface.valueIndexArray[2][3] = 0;
		rsInterface.valueIndexArray[3] = new int[3];
		rsInterface.valueIndexArray[3][0] = 1;
		rsInterface.valueIndexArray[3][1] = 6;
		rsInterface.valueIndexArray[3][2] = 0;
		rsInterface.enabledSprite = SpriteCache.get(esid);
		rsInterface.disabledSprite = SpriteCache.get(dsid);
		RSInterface hover = addInterface(interfaceId + 1);
		hover.hoverType = -1;
		hover.isMouseoverTriggered = true;
		hover.totalChildren(9);
		RSInterface.addLunarSprite(interfaceId + 2, 400 + 2);
		hover.child(0, interfaceId + 2, 0, 0);
		addText2(interfaceId + 3, "Level " + (lvl + 1) + ": " + name, 0xFF981F, true,
				true, 52, 1);
		hover.child(1, interfaceId + 3, 90, 4);
		addText2(interfaceId + 4, descr, 0xAF6A1A, true, true, 52, 0);
		hover.child(2, interfaceId + 4, 90, 19);
		hover.child(3, 30016, 10, 30); // astral
		hover.child(4, rune1, 70, 30);
		hover.child(5, rune2, 124, 30);
		addRuneText(interfaceId + 5, ra1 + 1, r1);
		hover.child(6, interfaceId + 5, 26, 66);
		addRuneText(interfaceId + 6, ra2 + 1, r2);
		hover.child(7, interfaceId + 6, 87, 66);
		addRuneText(interfaceId + 7, ra3 + 1, r3);
		hover.child(8, interfaceId + 7, 142, 66);
	}

	public static void addLunar2RunesSmallBox2(int ID, int r1, int r2, int ra1,
											  int ra2, int rune1, int lvl, String name, String descr,
											  int esid, int dsid, int suo, int type) {
		RSInterface rsInterface = addInterface(ID);
		rsInterface.id = ID;
		rsInterface.parentID = 1151;
		rsInterface.interfaceType = 5;
		rsInterface.atActionType = type;
		rsInterface.contentType = 0;
		rsInterface.hoverType = ID + 1;
		rsInterface.targetMask = new InterfaceTargetMask(suo);
		rsInterface.targetVerb = "Cast on";
		rsInterface.width = 20;
		rsInterface.height = 20;
		rsInterface.tooltip = "Cast <col=ff00>" + name;
		rsInterface.targetName = name;
		rsInterface.valueCompareType = new byte[3];
		rsInterface.requiredValues = new int[3];
		rsInterface.valueCompareType[0] = 3;
		rsInterface.requiredValues[0] = ra1;
		rsInterface.valueCompareType[1] = 3;
		rsInterface.requiredValues[1] = ra2;
		rsInterface.valueCompareType[2] = 3;
		rsInterface.requiredValues[2] = lvl;
		rsInterface.valueIndexArray = new int[3][];
		rsInterface.valueIndexArray[0] = new int[4];
		rsInterface.valueIndexArray[0][0] = 4;
		rsInterface.valueIndexArray[0][1] = 3214;
		rsInterface.valueIndexArray[0][2] = r1;
		rsInterface.valueIndexArray[0][3] = 0;
		rsInterface.valueIndexArray[1] = new int[4];
		rsInterface.valueIndexArray[1][0] = 4;
		rsInterface.valueIndexArray[1][1] = 3214;
		rsInterface.valueIndexArray[1][2] = r2;
		rsInterface.valueIndexArray[1][3] = 0;
		rsInterface.valueIndexArray[2] = new int[3];
		rsInterface.valueIndexArray[2][0] = 1;
		rsInterface.valueIndexArray[2][1] = 6;
		rsInterface.valueIndexArray[2][2] = 0;
		rsInterface.enabledSprite = SpriteCache.get(esid);
		rsInterface.disabledSprite = SpriteCache.get(dsid);
		RSInterface hover = addInterface(ID + 1);
		hover.hoverType = -1;
		hover.isMouseoverTriggered = true;
		hover.totalChildren(7);
		RSInterface.addLunarSprite(ID + 2, 400 + 2);
		hover.child(0, ID + 2, 0, 0);
		addText2(ID + 3, "Level " + (lvl + 1) + ": " + name, 0xFF981F, true,
				true, 52, 1);
		hover.child(1, ID + 3, 90, 4);
		addText2(ID + 4, descr, 0xAF6A1A, true, true, 52, 0);
		hover.child(2, ID + 4, 90, 19);
		hover.child(3, 30016, 33, 30);// Rune
		hover.child(4, rune1, 109, 30);// Rune
		addRuneText(ID + 5, ra1 + 1, r1);
		hover.child(5, ID + 5, 50, 66);
		addRuneText(ID + 6, ra2 + 1, r2);
		hover.child(6, ID + 6, 123, 66);
	}

	public static void addLunarSprite(int i, int spriteId) {
		RSInterface rsInterface = addInterface(i);
		rsInterface.id = i;
		rsInterface.parentID = i;
		rsInterface.interfaceType = 5;
		rsInterface.atActionType = 0;
		rsInterface.contentType = 0;
		rsInterface.hoverType = 52;
		rsInterface.disabledSprite = SpriteCache.get(spriteId);
		rsInterface.width = 00;
		rsInterface.height = 00;
		rsInterface.tooltip = "";
	}

	public static void addRuneText(int ID, int runeAmount, int RuneID) {
		RSInterface rsInterface = addInterface(ID);
		rsInterface.id = ID;
		rsInterface.parentID = 1151;
		rsInterface.interfaceType = 4;
		rsInterface.atActionType = 0;
		rsInterface.contentType = 0;
		rsInterface.width = 0;
		rsInterface.height = 14;
		rsInterface.hoverType = -1;
		rsInterface.valueCompareType = new byte[1];
		rsInterface.requiredValues = new int[1];
		rsInterface.valueCompareType[0] = 3;
		rsInterface.requiredValues[0] = runeAmount-1;
		rsInterface.valueIndexArray = new int[1][4];
		rsInterface.valueIndexArray[0][0] = 4;
		rsInterface.valueIndexArray[0][1] = 3214;
		rsInterface.valueIndexArray[0][2] = RuneID;
		rsInterface.valueIndexArray[0][3] = 0;
		rsInterface.centerText = true;
		rsInterface.textDrawingAreas = 0;
		rsInterface.textShadow = true;
		rsInterface.message = "%1/" + runeAmount + "";
		rsInterface.disabledColor = 12582912;
		rsInterface.enabledColor = 49152;
	}

	public static void addLunar3RunesLargeBox(int ID, int r1, int r2, int r3,
											  int ra1, int ra2, int ra3, int rune1, int rune2, int lvl,
											  String name, String descr, int esid, int dsid, int suo, int type) {
		RSInterface rsInterface = addInterface(ID);
		rsInterface.id = ID;
		rsInterface.parentID = 1151;
		rsInterface.interfaceType = 5;
		rsInterface.atActionType = type;
		rsInterface.contentType = 0;
		rsInterface.hoverType = ID + 1;
		rsInterface.targetMask = new InterfaceTargetMask(suo);
		rsInterface.targetVerb = "Cast on";
		rsInterface.width = 20;
		rsInterface.height = 20;
		rsInterface.tooltip = "Cast <col=ff00>" + name;
		rsInterface.targetName = name;
		rsInterface.valueCompareType = new byte[4];
		rsInterface.requiredValues = new int[4];
		rsInterface.valueCompareType[0] = 3;
		rsInterface.requiredValues[0] = ra1;
		rsInterface.valueCompareType[1] = 3;
		rsInterface.requiredValues[1] = ra2;
		rsInterface.valueCompareType[2] = 3;
		rsInterface.requiredValues[2] = ra3;
		rsInterface.valueCompareType[3] = 3;
		rsInterface.requiredValues[3] = lvl;
		rsInterface.valueIndexArray = new int[4][];
		rsInterface.valueIndexArray[0] = new int[4];
		rsInterface.valueIndexArray[0][0] = 4;
		rsInterface.valueIndexArray[0][1] = 3214;
		rsInterface.valueIndexArray[0][2] = r1;
		rsInterface.valueIndexArray[0][3] = 0;
		rsInterface.valueIndexArray[1] = new int[4];
		rsInterface.valueIndexArray[1][0] = 4;
		rsInterface.valueIndexArray[1][1] = 3214;
		rsInterface.valueIndexArray[1][2] = r2;
		rsInterface.valueIndexArray[1][3] = 0;
		rsInterface.valueIndexArray[2] = new int[4];
		rsInterface.valueIndexArray[2][0] = 4;
		rsInterface.valueIndexArray[2][1] = 3214;
		rsInterface.valueIndexArray[2][2] = r3;
		rsInterface.valueIndexArray[2][3] = 0;
		rsInterface.valueIndexArray[3] = new int[3];
		rsInterface.valueIndexArray[3][0] = 1;
		rsInterface.valueIndexArray[3][1] = 6;
		rsInterface.valueIndexArray[3][2] = 0;
		rsInterface.enabledSprite = SpriteCache.get(esid);
		rsInterface.disabledSprite = SpriteCache.get(dsid);
		RSInterface hover = addInterface(ID + 1);
		hover.isMouseoverTriggered = true;
		hover.hoverType = -1;
		hover.totalChildren(9);
		RSInterface.addLunarSprite(ID + 2, 400 + 2);
		hover.child(0, ID + 2, 0, 0);
		addText2(ID + 3, "Level " + (lvl + 1) + ": " + name, 0xFF981F, true,
				true, 52, 1);
		hover.child(1, ID + 3, 90, 4);
		addText2(ID + 4, descr, 0xAF6A1A, true, true, 52, 0);
		hover.child(2, ID + 4, 90, 34);
		hover.child(3, 30016, 14, 61);
		hover.child(4, rune1, 74, 61);
		hover.child(5, rune2, 130, 61);
		RSInterface.addRuneText(ID + 5, ra1 + 1, r1);
		hover.child(6, ID + 5, 26, 92);
		RSInterface.addRuneText(ID + 6, ra2 + 1, r2);
		hover.child(7, ID + 6, 87, 92);
		RSInterface.addRuneText(ID + 7, ra3 + 1, r3);
		hover.child(8, ID + 7, 142, 92);
	}

	public static void addLunar3RunesBigBox2(int ID, int r1, int r2, int r3,
											int ra1, int ra2, int ra3, int rune1, int rune2, int lvl,
											String name, String descr, int esid, int dsid, int suo, int type) {
		RSInterface rsInterface = addInterface(ID);
		rsInterface.id = ID;
		rsInterface.parentID = 1151;
		rsInterface.interfaceType = 5;
		rsInterface.atActionType = type;
		rsInterface.contentType = 0;
		rsInterface.hoverType = ID + 1;
		rsInterface.targetMask = new InterfaceTargetMask(suo);
		rsInterface.targetVerb = "Cast on";
		rsInterface.width = 20;
		rsInterface.height = 20;
		rsInterface.tooltip = "Cast <col=ff00>" + name;
		rsInterface.targetName = name;
		rsInterface.valueCompareType = new byte[4];
		rsInterface.requiredValues = new int[4];
		rsInterface.valueCompareType[0] = 3;
		rsInterface.requiredValues[0] = ra1;
		rsInterface.valueCompareType[1] = 3;
		rsInterface.requiredValues[1] = ra2;
		rsInterface.valueCompareType[2] = 3;
		rsInterface.requiredValues[2] = ra3;
		rsInterface.valueCompareType[3] = 3;
		rsInterface.requiredValues[3] = lvl;
		rsInterface.valueIndexArray = new int[4][];
		rsInterface.valueIndexArray[0] = new int[4];
		rsInterface.valueIndexArray[0][0] = 4;
		rsInterface.valueIndexArray[0][1] = 3214;
		rsInterface.valueIndexArray[0][2] = r1;
		rsInterface.valueIndexArray[0][3] = 0;
		rsInterface.valueIndexArray[1] = new int[4];
		rsInterface.valueIndexArray[1][0] = 4;
		rsInterface.valueIndexArray[1][1] = 3214;
		rsInterface.valueIndexArray[1][2] = r2;
		rsInterface.valueIndexArray[1][3] = 0;
		rsInterface.valueIndexArray[2] = new int[4];
		rsInterface.valueIndexArray[2][0] = 4;
		rsInterface.valueIndexArray[2][1] = 3214;
		rsInterface.valueIndexArray[2][2] = r3;
		rsInterface.valueIndexArray[2][3] = 0;
		rsInterface.valueIndexArray[3] = new int[3];
		rsInterface.valueIndexArray[3][0] = 1;
		rsInterface.valueIndexArray[3][1] = 6;
		rsInterface.valueIndexArray[3][2] = 0;
		rsInterface.enabledSprite = SpriteCache.get(esid);
		rsInterface.disabledSprite = SpriteCache.get(dsid);
		RSInterface hover = addInterface(ID + 1);
		hover.hoverType = -1;
		hover.isMouseoverTriggered = true;
		hover.totalChildren(9);
		RSInterface.addLunarSprite(ID + 2, 402 + 2);
		hover.child(0, ID + 2, 0, 0);
		addText2(ID + 3, "Level " + (lvl + 1) + ": " + name, 0xFF981F, true,
				true, 52, 1);
		hover.child(1, ID + 3, 90, 4);
		addText2(ID + 4, descr, 0xAF6A1A, true, true, 52, 0);
		hover.child(2, ID + 4, 90, 21);
		hover.child(3, 30016, 14, 48);
		hover.child(4, rune1, 74, 48);
		hover.child(5, rune2, 130, 48);
		RSInterface.addRuneText(ID + 5, ra1 + 1, r1);
		hover.child(6, ID + 5, 26, 79);
		RSInterface.addRuneText(ID + 6, ra2 + 1, r2);
		hover.child(7, ID + 6, 87, 79);
		RSInterface.addRuneText(ID + 7, ra3 + 1, r3);
		hover.child(8, ID + 7, 142, 79);
	}

	public static void addSmallBoxTeleport(int ID, String name, String descr, String cacheName, int spriteId) {
			RSInterface rsInterface = addInterface(ID, InterfaceType.TAB);
			rsInterface.id = ID;
			rsInterface.parentID = 1151;
			rsInterface.interfaceType = 5;
			rsInterface.atActionType = 5;
			rsInterface.contentType = 0;
			rsInterface.hoverType = ID + 1;
			rsInterface.width = 20;
			rsInterface.height = 20;
			rsInterface.tooltip = "Cast <col=ff00>" + name;
			rsInterface.targetName = name;
	//		rsInterface.valueCompareType = new byte[4];
	//		rsInterface.requiredValues = new int[4];
			rsInterface.disabledSprite = getSprite(spriteId, aClass44, cacheName, mediaIndex);
			RSInterface hover = addInterface(ID + 1);
			hover.hoverType = -1;
			hover.isMouseoverTriggered = true;
			hover.totalChildren(3);
			RSInterface.addLunarSprite(ID + 2, 400 + 2);
			hover.child(0, ID + 2, 0, 0);
			addText(ID + 3, name, 0xFF981F, true, true, 52, 1);
			hover.child(1, ID + 3, 90, 4);
			addText(ID + 4, descr, 0xAF6A1A, true, true, 52, 0);
			hover.child(2, ID + 4, 90, 19);
			
		}

	public static void addSpriteCache(int i, String sprite, int index) {
		RSInterface rsInterface = addInterface(i);
		rsInterface.id = i;
		rsInterface.interfaceType = 5;
		rsInterface.disabledSprite = getSprite(index, aClass44, sprite, mediaIndex);
		rsInterface.width = 24;
		rsInterface.height = 24;
	}

	public static RSInterface addSprite(int i, int width, int height, final String sprite) {
		RSInterface rsInterface = addLayer(i, width, height);
		rsInterface.interfaceType = 5;
		if (!sprite.isEmpty()) {
			int comma = sprite.lastIndexOf(",");
			rsInterface.disabledSprite = RSInterface.getSprite(Integer.parseInt(sprite.substring(comma + 1)), aClass44, sprite.substring(0, comma), mediaIndex);
		}

		return rsInterface;
	}

	public static RSInterface addButton(int x, int y, int spriteId, int hoverId, String text) {
		final RSInterface inter = addSprite(x, y, 0, 0, spriteId, hoverId, text);
		if (inter.disabledSprite != null) {
			inter.width = inter.disabledSprite.myWidth;
			inter.height = inter.disabledSprite.myHeight;
		}

		return inter;
	}

	public static RSInterface addSprite(int x, int y, int width, int height, final String sprite) {
		final RSInterface rsi = new RSInterface();
		rsi.x = x;
		rsi.y = y;
		rsi.interfaceType = 5;
		rsi.width = width;
		rsi.height = height;
		if (sprite != null && !sprite.isEmpty()) {
			int comma = sprite.lastIndexOf(",");
			rsi.disabledSprite = RSInterface.getSprite(Integer.parseInt(sprite.substring(comma + 1)), aClass44, sprite.substring(0, comma), mediaIndex);
		}

		return rsi;
	}

	public static RSInterface addSprite(int x, int y, int width, int height, final int spriteId, int hoverId, String text) {
		final RSInterface rsi = new RSInterface();
		rsi.x = x;
		rsi.y = y;
		rsi.interfaceType = 5;
		rsi.width = width;
		rsi.height = height;
		if (text != null && !text.isEmpty()) {
			rsi.atActionType = 1;
			rsi.tooltip = text;
		}

		if (spriteId != -1) {
			rsi.disabledSprite = SpriteCache.get(spriteId);
		}

		if (hoverId != -1) {
			final Sprite sprite = SpriteCache.get(hoverId);
			rsi.onMouseOver = new MouseEvent() {
				@Override
				public void run() {
					rsi.disabledSprite = sprite;
				}
			};

			final Sprite revertSprite = spriteId == -1 ? null : SpriteCache.get(spriteId);
			rsi.onMouseLeave = new MouseEvent() {

				@Override
				public void run() {
					rsi.disabledSprite = revertSprite;
				}
			};
		}

		return rsi;
	}

	public static RSInterface addRectangle(int x, int y, int width, int height, int color, int opacity, boolean filled) {
		RSInterface tab = new RSInterface();
		tab.x = x;
		tab.y = y;
		tab.disabledColor = color;
		tab.filled = filled;
		tab.interfaceType = 3;
		tab.transparency = opacity;
		tab.width = width;
		tab.height = height;
		tab.setFlag(RECT_IGNORE_ATLAS);
		return tab;
	}

	public static RSInterface addItemSprite(int x, int y, int itemId) {
		RSInterface rsi = new RSInterface();
		rsi.x = x;
		rsi.y = y;
		rsi.interfaceType = 5;
		rsi.width = 36;
		rsi.height = 32;
		rsi.itemId = itemId;
		rsi.setFlag(DONT_DRAW_INVENTORY_COUNT_BIT);
		return rsi;
	}
	
	public static RSInterface addNewText(String text, int x, int y, int width, int height, int font, int color, int hoverColor, boolean shadow, int horizontalAlign, int verticalAlign, int verticalSpacing) {
		RSInterface rsi = new RSInterface();
		rsi.x = x;
		rsi.y = y;
		rsi.interfaceType = 10;
		rsi.width = width;
		rsi.height = height;
		rsi.textShadow = shadow;
		rsi.textDrawingAreas = font;
		rsi.message = text;
		rsi.disabledColor = color;
		rsi.horizontalAlignment = horizontalAlign;
		rsi.verticalAlignment = verticalAlign;
		rsi.verticalSpacing = verticalSpacing;
		rsi.mouseHoverTextRecolor = hoverColor;
		return rsi;
	}

	public static RSInterface addModelDynamic(int x, int y, int width, int height, int rotationX, int rotationY, int zoom, int modelId, int modelType, int animId) {
		RSInterface tab = new RSInterface();
		tab.x = x;
		tab.y = y;
		tab.interfaceType = 6;
		tab.width = width;
		tab.disabledAnimation = animId;
		tab.height = height;
		tab.modelRotationX = rotationX;
		tab.modelRotationY = rotationY;
		tab.modelZoom = zoom;
		tab.disabledMediaType = modelType;
		tab.disabledMediaID = modelId;
		return tab;
	}
	
	public RSInterface addOnSkillUpdate(int skillId, Runnable runnable) {
		if (onSkillUpdate == null) {
			onSkillUpdate = new HashMap<Integer, List<Runnable>>();
		}

		List<Runnable> list = onSkillUpdate.get(skillId);
		if (list == null) {
			list = new LinkedList<Runnable>();
			onSkillUpdate.put(skillId, list);
		}

		list.add(runnable);
		return this;
	}

	public RSInterface addOnInvetoryUpdate(int inventoryId, Runnable runnable) {
		if (onInventoryUpdate == null) {
			onInventoryUpdate = new HashMap<Integer, List<Runnable>>();
		}

		List<Runnable> list = onInventoryUpdate.get(inventoryId);
		if (list == null) {
			list = new LinkedList<Runnable>();
			onInventoryUpdate.put(inventoryId, list);
		}

		list.add(runnable);
		return this;
	}

	public RSInterface addOnVarpUpdate(int varpId, Runnable runnable) {
		if (onVarpUpdate == null) {
			onVarpUpdate = new HashMap<Integer, List<Runnable>>();
		}

		List<Runnable> list = onVarpUpdate.get(varpId);
		if (list == null) {
			list = new LinkedList<Runnable>();
			onVarpUpdate.put(varpId, list);
		}

		list.add(runnable);
		return this;
	}
}
