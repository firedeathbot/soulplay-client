package com.soulplayps.client.interfaces;

import static com.soulplayps.client.interfaces.RSInterface.CANT_CLICK_THROUGH;
import static com.soulplayps.client.interfaces.RSInterface.addNewText;
import static com.soulplayps.client.interfaces.RSInterface.addRectangle;
import static com.soulplayps.client.interfaces.RSInterface.addSprite;

import com.soulplayps.client.Client;
import com.soulplayps.client.EntityAttackOptionSetting;
import com.soulplayps.client.interfaces.impl.powers.PowerUpInterface;
import com.soulplayps.client.interfaces.impl.powers.PowerUpSortType;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.settings.Settings;

public class Dropdown {

	// Make sure to add optionLayer on 0,0 or else it will not work!
	public static void makeDropdown(final RSInterface menuLayer, final RSInterface optionLayer, final String[] options,
			int fontId, final int dropDownId, int shownId) {
		int menuWidth = menuLayer.width;
		int menuHeight = menuLayer.height;

		menuLayer.dynamicComponents = null;
		menuLayer.createDynamic(0, addRectangle(0, 0, menuWidth, menuHeight, 0, 0, false));
		menuLayer.createDynamic(1, addRectangle(1, 1, menuWidth - 2, menuHeight - 2, 4671301, 0, true));
		menuLayer.dynamicComponents[1].setFlag(RSInterface.RECT_IGNORE_ATLAS);
		menuLayer.createDynamic(2, addSprite(menuWidth - 16 - 2, menuHeight / 2 - 16 / 2, 16, 16, 848, -1, null));
		final RSInterface text = addNewText(options[shownId], 1, 1, menuWidth - 16, menuHeight - 2, fontId, 16750623, 0, true, 1, 1, 0);
		text.onMouseOverEnter = new MouseEvent() {
			@Override
			public void run() {
				text.disabledColor = 16758847;
			}
		};
		text.onMouseLeave = new MouseEvent() {
			@Override
			public void run() {
				text.disabledColor = 16750623;
			}
		};
		menuLayer.createDynamic(3, text);
		menuLayer.onMouseClick = new MouseEvent() {
			@Override
			public void run() {
				if (optionLayer.hidden) {
					menuLayer.dynamicComponents[2].disabledSprite = SpriteCache.get(847);
					optionLayer.hidden = false;
				} else {
					menuLayer.dynamicComponents[2].disabledSprite = SpriteCache.get(848);
					optionLayer.hidden = true;
				}
			}
		};

		int fontHeight = Client.instance.chooseFont(fontId).baseCharacterHeight + 4;
		int optionLayerX = menuLayer.x;
		int optionLayerY = menuLayer.y + menuLayer.height - 1;
		int optionLayerHeight = options.length * fontHeight + 2;

		optionLayer.dynamicComponents = null;
		optionLayer.createDynamic(0, addRectangle(optionLayerX, optionLayerY, menuWidth, optionLayerHeight, 0, 0, false));
		RSInterface background = addRectangle(optionLayerX + 1, optionLayerY + 1, menuWidth - 2, optionLayerHeight - 2, 4671301,
				0, true);
		background.setFlag(CANT_CLICK_THROUGH);
		optionLayer.createDynamic(1, background);
		final RSInterface hoverRectangle = addRectangle(optionLayerX + 1, optionLayerY + 1, menuWidth - 2, fontHeight, 0xffffff,
				170, true);
		hoverRectangle.hidden = true;
		optionLayer.createDynamic(2, hoverRectangle);
		for (int i = 0; i < options.length; i++) {
			final int optionIndex = i;
			final RSInterface option = addNewText(options[i], optionLayerX + 1, optionLayerY + 1 + i * fontHeight, menuWidth,
					fontHeight, fontId, 16750623, 0, true, 1, 1, 0);
			option.onMouseOver = new MouseEvent() {
				@Override
				public void run() {
					hoverRectangle.hidden = false;
					hoverRectangle.y = option.y;
					option.disabledColor = 16758847;
				}
			};
			option.onMouseLeave = new MouseEvent() {
				@Override
				public void run() {
					hoverRectangle.hidden = true;
					option.disabledColor = 16750623;
				}
			};
			option.onMouseClick = new MouseEvent() {
				public void run() {
					optionLayer.hidden = true;
					hoverRectangle.hidden = true;
					menuLayer.dynamicComponents[2].disabledSprite = SpriteCache.get(848);
					menuLayer.dynamicComponents[3].message = options[optionIndex];
					dropDownClicked(dropDownId, optionIndex);
					Client.instance.sendOptionClick(optionLayer.id, optionIndex, -1);
				}
			};
			optionLayer.createDynamic(3 + i, option);
		}
	}

	public static void dropDownClicked(int id, int index) {
		switch (id) {
			case 0:
				Client.playerAttackOptionSetting = EntityAttackOptionSetting.find(index);
				Settings.changeSetting("player_attack_option", index);
				return;
			case 1:
				Client.npcAttackOptionSetting = EntityAttackOptionSetting.find(index);
				Settings.changeSetting("npc_attack_option", index);
				return;
			case 2:
				PowerUpInterface.selectedSort = PowerUpSortType.list(index);
				PowerUpInterface.updateSorted();
				return;
		}
	}

}
