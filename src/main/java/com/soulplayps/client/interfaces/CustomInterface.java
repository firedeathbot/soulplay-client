package com.soulplayps.client.interfaces;

public abstract class CustomInterface extends InterfaceBuilder {

	public CustomInterface(int id, int startingChild) {
		super(id, startingChild);
	}

	public CustomInterface(int id) {
		this(id, id + 1);
	}
	
	public void init() {
		create();
		build();
	}

	public abstract void create();

}
