package com.soulplayps.client.interfaces;

public class LinkedComponentBuilder extends ComponentBuilder {
	
	private final ComponentBuilder linked;

	public LinkedComponentBuilder(ComponentBuilder linked, int childID) {
		super(linked.getInterfaceBuilder(), childID);
		this.linked = linked;
	}
	
	@Override
	public Component at(int x, int y) {
		Component linkedComponent = linked.at(x, y);
		super.at(x, y);
		return linkedComponent;
	}

}
