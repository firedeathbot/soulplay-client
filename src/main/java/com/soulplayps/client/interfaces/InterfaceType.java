package com.soulplayps.client.interfaces;

public enum InterfaceType {
	
	MAIN(516, 338),
	
	TAB(250, 335);
	
	private final int width, height;
	
	InterfaceType(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}	

}
