package com.soulplayps.client.interfaces.widget;

import com.soulplayps.client.Client;
import com.soulplayps.client.WorldType;
import com.soulplayps.client.node.animable.item.ItemDef;
import com.soulplayps.client.node.raster.Raster;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.settings.Settings;
import com.soulplayps.client.util.Misc;

public class Widget {

    private static final Widget[] widgets = new Widget[10];

    private final int spriteId;
    private int seconds, dx, dy;
    private boolean disableAlpha;
    private boolean itemImage;
    private String text;

    private Widget(int seconds, int spriteId, boolean itemImage) {
    	this(seconds, spriteId);
    	this.itemImage = itemImage;
    }

    private Widget(int seconds, int spriteId) {
        if (seconds < 0)
            seconds = 0;
        this.setSeconds(seconds);
        this.spriteId = spriteId;
    }

    private void decrement() {
    	int next = seconds - 1;
        if (next < 0) {
            next = 0;
        }

        setSeconds(next);
    }

    public void setSeconds(int seconds) {
		this.seconds = seconds;
		this.text = Misc.getFormattedTime(seconds);
	}

    private boolean terminated() {
        return seconds <= 0;
    }

    public static void draw() {
        if (Client.worldType != WorldType.SEASONAL && !Settings.getOrDefault("display_timers", true)) {
            return;
        }

        int x = 445;
        int y = 225;

        if (Client.isResizeable()) {
        	int width = Client.clientWidth;
        	int height = Client.clientHeight;

        	x = width - 275;
        	y = height;
        	y -= width < 1000 ? 115 : 75;
        }

        for (int index = 0; index < widgets.length - 1; index++) {
            Widget widget = widgets[index];
            if (widget == null) {
                continue;
            }

            int xOff;
            int height;
            Sprite sprite;
            if (widget.itemImage) {
            	sprite = ItemDef.getSprite(widget.spriteId, 1, 1, 3153952, false, false);
            	height = 34;
            	xOff = 1;
            } else {
            	sprite = SpriteCache.get(widget.spriteId);
            	height = 22;
            	xOff = 3;
            }

            if (sprite == null) {
                continue;
            }

            double ratio = Math.abs(widget.dy / 24.0);
            int alpha = widget.disableAlpha ? 200 : (int) (200 - 200 * ratio);
            int textAlpha = widget.disableAlpha ? 256 : (int) (256 - 256 * ratio);

            if (widget.dx > 0) {
                ratio = Math.abs(widget.dx / 24.0);
                alpha = (int) (200 * ratio);
                textAlpha = (int) (256 * ratio);
            }

            int posY = y - widget.dy;
            Raster.fillRect(x, posY, 65, height, 0x473F35, alpha, true);
            Raster.drawRect(x, posY, 65, height, 0x000000, (alpha - 80) > 0 ? alpha - 80 : 0);
            sprite.drawTransparentSprite(x + xOff, posY + (height / 2 - sprite.myHeight / 2), alpha);

            Client.newSmallFont.drawInterfaceText(widget.text, x, posY, 60, height, 0xFF8C00, 0, textAlpha, 2, 1, 0);

            int offset = height;
            Widget next = widgets[index + 1];
            if (next != null) {
            	offset = next.itemImage ? 34 : 22;
            }

            y -= offset + 2;
        }
    }

    private static void add(Widget widget) {
        if (widget.seconds == 0) {
            return;
        }

        int index = 0;
        for (int i = 0; i < widgets.length; i++) {
            if (widgets[i] == null || widgets[i].spriteId == widget.spriteId) {
                index = i;
                break;
            }
        }

        widget.dy = -24;
        widgets[index] = widget;
    }

    public static void tick() {
        for (int index = 0; index < widgets.length; index++) {
            Widget widget = widgets[index];

            if (widget == null) {
                continue;
            }

            widget.decrement();

            if (widget.terminated() && widget.dx == 0) {
                remove(widget);
                continue;
            }

            if (widget.dx != 0 || widget.dy != 0) {
                if (widget.dx != 0) {
                    if (widget.dx > 0) {
                        widget.dx--;
                    }

                    if (widget.dx == 0)
                        remove2(index);
                } else {
                    if (widget.dy > 0) {
                        widget.dy--;
                    } else if (widget.dy < 0) {
                        widget.dy++;
                    }

                    if (widget.dy == 0)
                        widget.disableAlpha = true;
                }
            }
        }
    }

	public static void clear() {
		for (int i = 0, length = widgets.length; i < length; i++) {
			widgets[i] = null;
		}
	}

    private static void remove(Widget widget) {
        for (Widget next : widgets) {
            if (next == null) {
                return;
            }

            if (next.spriteId == widget.spriteId) {
                next.dx = 24;
                break;
            }
        }
    }

    private static void remove2(int index) {
        widgets[index] = null;

        for (int next = index; next < widgets.length - 1; next++) {
            Widget temp = widgets[next];

            widgets[next] = widgets[next + 1];
            widgets[next + 1] = temp;

            if (widgets[next] != null)
                widgets[next].dy = 18;
            if (widgets[next + 1] != null)
                widgets[next + 1].dy = 18;
        }
    }

    public static void place(int type, int time) {
        switch (type) {
            case 1:
                submit(new Widget(time, 1211));
                break;
            case 2:
                submit(new Widget(time, 1212));
                break;
            case 3:
                submit(new Widget(time, 1213));
                break;
            case 4:
                submit(new Widget(time, 1214));
                break;
            case 5:
                submit(new Widget(time, 1215));
                break;
            case 6:
                submit(new Widget(time, 507));
                break;
            case 7:
                submit(new Widget(time, 157));
                break;
            case 8:
            	submit(new Widget(time, 9656, true));
            	break;
            case 9:
            	submit(new Widget(time, 9657, true));
            	break;
            case 10:
            	submit(new Widget(time, 9658, true));
            	break;
        }
    }

    private static void submit(Widget widget) {
        if (widget.seconds <= 0) {
            remove(widget);
        } else {
            add(widget);
        }
    }

    @Override
    public String toString() {
        return String.format("[%s, %s]", seconds, spriteId);
    }

}

