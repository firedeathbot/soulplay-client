package com.soulplayps.client.interfaces.impl;

import static com.soulplayps.client.interfaces.RSInterface.TILING_FLAG_BIT;
import static com.soulplayps.client.interfaces.RSInterface.addButton;
import static com.soulplayps.client.interfaces.RSInterface.addInterface;
import static com.soulplayps.client.interfaces.RSInterface.addLayer;
import static com.soulplayps.client.interfaces.RSInterface.addNewText;
import static com.soulplayps.client.interfaces.RSInterface.addRectangle;
import static com.soulplayps.client.interfaces.RSInterface.addSprite;

import com.soulplayps.client.ConfigCodes;
import com.soulplayps.client.interfaces.MouseEvent;
import com.soulplayps.client.interfaces.RSInterface;
import com.soulplayps.client.node.io.RSBuffer;

public class InstanceInterface {

	public static void init() {
		int width = 500;
		int height = 320;
		int settingsLayerWidth = 500 - 21 - 165;
		int settingsLayerHeight = 320 - 22 - 7;
		int bossLayerWidth = 166;
		int bossLayerHeight = 320 - 22 - 7;
		
		RSInterface inter = addInterface(70200);
		addSprite(70201, 1288);
		addNewText(70202, "Instance Setup", width, 22, 2, 16750623, 0, true, 1, 1, 0);
		addSprite(70203, 854, 6, settingsLayerHeight); 
		RSInterface.interfaceCache[70203].setFlag(TILING_FLAG_BIT);
		RSInterface.interfaceCache[70203].flipHorizontal();
		addButton(70204, 300, 301, "Close");
		RSInterface.interfaceCache[70204].atActionType = 3;

		inter.totalChildren(6);
		inter.child(0, 70201, 6, 6);
		inter.child(1, 70202, 6, 6);
		inter.child(2, 70203, bossLayerWidth + 13, 6 + 22);
		inter.child(3, 70220, 6 + 14 + 165, 6 + 22);
		inter.child(4, 70400, 6 + 7, 6 + 22);
		inter.child(5, 70204, 484, 10);
		
		addLayer(70220, settingsLayerWidth, settingsLayerHeight);
		addLayer(70400, bossLayerWidth, bossLayerHeight);
	
		RSInterface settingsLayer = RSInterface.interfaceCache[70220];
		
		addNewText(70221, "Here you can create your own instance for multiple bosses by clicking "
				+ "on the 'create' button. Players may join other instances by clicking on the 'join' "
				+ "button, if they have the right information to do so. Keep in mind, creating or "
				+ "joining an instance will cost you a instance token.", settingsLayerWidth - 10, settingsLayerHeight - 235, 0, 0xffffff, 0, true, 1, 0, 0);
		addRectangle(70222, 0, 0x32292e/*0x40362B*/, false, settingsLayerWidth - 10, 175, 0);//0x544E40
		addRectangle(70223, 0, 0x554C3F, false, settingsLayerWidth - 12, 173, 0);//0x544E40
		addButton(70224, 584, 583, "Create instance");
		addNewText(70225, "Create", 74, 30, 1, 16750623, 0, true, 1, 1, 0);
		addButton(70226, 584, 583, "Join existing instance");
		addNewText(70227, "Join", 74, 30, 1, 16750623, 0, true, 1, 1, 0);
		addButton(70228, 584, 583, "Buy Tokens");
		addNewText(70229, "Buy Tokens", 74, 30, 1, 16750623, 0, true, 1, 1, 0);
		
		addLayer(70250, settingsLayerWidth - 14, 171);
		
		settingsLayer.totalChildren(10);
		settingsLayer.child(0, 70221, 5, 5);
		settingsLayer.child(1, 70222, 5, settingsLayerHeight - 225);
		settingsLayer.child(2, 70223, 6, settingsLayerHeight - 224);
		
		settingsLayer.child(3, 70224, 10, settingsLayerHeight - 30 - 10);
		settingsLayer.child(4, 70225, 10, settingsLayerHeight - 30 - 10);
		settingsLayer.child(5, 70226, 94, settingsLayerHeight - 30 - 10);
		settingsLayer.child(6, 70227, 94, settingsLayerHeight - 30 - 10);
		settingsLayer.child(7, 70228, settingsLayerWidth - 74 - 10, settingsLayerHeight - 30 - 10);
		settingsLayer.child(8, 70229, settingsLayerWidth - 74 - 10, settingsLayerHeight - 30 - 10);
		settingsLayer.child(9, 70250, 7, settingsLayerHeight - 223);
		
		RSInterface checkLayer = RSInterface.interfaceCache[70250];
		
		addNewText(70251, "", settingsLayerWidth - 14, 22, 1, 16750623, 0, true, 1, 1, 0);
		addSprite(70252, 980, 981, ConfigCodes.INSTANCE_DEATH_SAFETY, 1, -1, 15, 15, "Select", false);
		RSInterface.interfaceCache[70252].atActionType = 4;
		addNewText(70253, "Instance insurance (<col=ffffff>150 Million GP</col>)", 280, 15, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(70254, "Instances are <col=ff0000>NOT</col> safe by default. Buy insurance to keep your items on death.", 280 - 14, 30, 0, 0xffffff, 0, true, 0, 0, 0);
		
		addSprite(70255, 980, 981, ConfigCodes.INSTANCE_HEALTH_GENERATION, 1, -1, 15, 15, "Select", false);
		RSInterface.interfaceCache[70255].atActionType = 4;
		addNewText(70256, "Health generation (<col=ffffff>50 Million GP</col>)", 280, 15, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(70257, "Generates health over time while in the instance.", 280 - 14, 30, 0, 0xffffff, 0, true, 0, 0, 0);
		
		addSprite(70258, 980, 981, ConfigCodes.INSTANCE_PRAYER_DRAINING, 1, -1, 15, 15, "Select", false);
		RSInterface.interfaceCache[70258].atActionType = 4;
		addNewText(70259, "Prayer draining (<col=ffffff>50 Million GP</col>)", 280, 15, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(70260, "Reduces the prayer draining by 100% while in the instance.", 280 - 14, 30, 0, 0xffffff, 0, true, 0, 0, 0);

		addLayer(70500, 248, 135);
		
		checkLayer.totalChildren(11);
		checkLayer.child(0, 70251, 0, 0);
		checkLayer.child(1, 70252, 5, 27);
		checkLayer.child(2, 70253, 26, 27);
		checkLayer.child(3, 70254, 26, 45);
		
		checkLayer.child(4, 70255, 5, 77);
		checkLayer.child(5, 70256, 26, 77);
		checkLayer.child(6, 70257, 26, 95);
		
		checkLayer.child(7, 70258, 5, 127);
		checkLayer.child(8, 70259, 26, 127);
		checkLayer.child(9, 70260, 26, 145);
		checkLayer.child(10, 70500, 26, 18);
		
		RSInterface confirmLayer = RSInterface.interfaceCache[70500];
		confirmLayer.hidden = true;
		
		addSprite(70501, 972, 248, 135);
		addNewText(70502, "Confirm Setup", 248, 22, 1, 16750623, 0, true, 1, 1, 0);
		
		addNewText(70503, "Creating/joining an instance costs <col=ffffff>1</col> instance token. You may set a key if you wish to allow others to join your instance. Key may contain <col=ffffff>numbers only</col>.", 238, 50, 0, 16750623, 0, true, 1, 0, 0);
		addSprite(70504, 988, -1, -1, 0, -1, 100, 20, "Search", true);
		addNewText(70505, "Key...", 100, 20, 0, 0xA9A9A9, 0, false, 0, 1, 0);
		RSInterface.interfaceCache[70505].contentType = 1360;
		addNewText(70506, "Key:", 100, 20, 1, 0xffffff, 0, false, 0, 1, 0);
		addButton(70507, 556, 557, "Confirm");
		addNewText(70508, "Confirm", 61, 20, 1, 16750623, 0, true, 1, 1, 0);
		addButton(70509, 556, 557, "Cancel");
		addNewText(70510, "Cancel", 61, 20, 1, 16750623, 0, true, 1, 1, 0);

		confirmLayer.totalChildren(10);
		confirmLayer.child(0, 70501, 0, 0);
		confirmLayer.child(1, 70502, 0, 0);
		confirmLayer.child(2, 70503, 5, 25);
		confirmLayer.child(3, 70504, 108, 70);
		confirmLayer.child(4, 70505, 113, 71);
		confirmLayer.child(5, 70506, 42, 71);
		confirmLayer.child(6, 70507, 42, 100);
		confirmLayer.child(7, 70508, 42, 100);
		confirmLayer.child(8, 70509, 145, 100);
		confirmLayer.child(9, 70510, 145, 100);
		
		RSInterface bossLayer = RSInterface.interfaceCache[70400];

		addRectangle(70401, 0, 0, false, bossLayerWidth, 1);
		addNewText(70402, "Instances", bossLayerWidth, 26, 1, 16750623, 0, true, 1, 1, 0);
		addLayer(70403, bossLayerWidth - 16, bossLayerHeight - 28);

		bossLayer.totalChildren(3);
		bossLayer.child(0, 70402, 0, 0);
		bossLayer.child(1, 70401, 0, 26);
		bossLayer.child(2, 70403, 0, 27);
	}

	public static void refreshInstancesFromServer(RSBuffer buffer) {
		RSInterface scrollLayer = RSInterface.interfaceCache[70403];
		scrollLayer.dynamicComponents = null;

		int height = 20;
		int length = buffer.readUnsignedByte();
		int selectedIndex = buffer.readUnsignedByte();
		if (selectedIndex == 255) {
			selectedIndex = -1;
		}

		int index = 0;
		for (int i = 0; i < length; i++) {
			int y = i * height;
			final int origTrans = i % 2 != 0 ? 235 : 255;
			final RSInterface rectangle = RSInterface.addRectangle(0, y, scrollLayer.width, height, 0xffffff, origTrans, true);
			rectangle.atActionType = 1;
			rectangle.tooltip = "Select";

			if (i != selectedIndex) {
				rectangle.onMouseOverEnter = new MouseEvent() {
					@Override
					public void run() {
						rectangle.transparency = 225;
					}
				};

				rectangle.onMouseLeave = new MouseEvent() {
					@Override
					public void run() {
						rectangle.transparency = origTrans;
					}
				};
			} else {
				rectangle.transparency = 200;
			}

			scrollLayer.createDynamic(index++, rectangle);
			scrollLayer.createDynamic(index++, RSInterface.addNewText(buffer.readString(), 0, y, scrollLayer.width, height, 0, 0xffffff, 0, true, 0, 1, 0));
		}

		int scrollHeight = length * height;
		if (scrollHeight <= scrollLayer.height) {
			scrollLayer.scrollMax = scrollLayer.height + 1;
		} else {
			scrollLayer.scrollMax = scrollHeight;
		}
	}

}
