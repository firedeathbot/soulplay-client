package com.soulplayps.client.interfaces.impl;

import com.soulplayps.client.interfaces.CustomInterface;

public final class PetInfo extends CustomInterface {

	public PetInfo() {
		super(40262);
	}

	@Override
	public void create() {
		sprite(702).at(14, 17);
		hover("Close Window", 300, 301, 3, 0).at(475, 30);
		button(1038, "Pets", 28646, 1, 68, 20).at(99, 30);
		button(1037, "Pet Info", 28647, 1, 68, 20).at(25, 30);
		text("Pet Info", 0, 0xffb000, true, true).at(58, 35);
		text("Pets", 0, 0x695f28, true, true).at(128, 35);
		sprite(30).at(20, 80);
		sprite(484).at(205, 55);
		sprite(484).at(205, 140);
		sprite(484).at(205, 207);
		sprite(486).at(210, 260);
		sprite(486).at(298, 260);
		text("Pet Statistics", 2, 0xffb000, false, true).at(65, 65);
		text("None", 157, 1, 0xffb000, true, true).at(273, 62);
		hover("Recall pet", 2, 3, 1, 0).at(250, 275);
		hover("Feed pet", 2, 3, 1, 0).at(380, 275);
		text("Recall", 2, 0xffb000, false, true).at(265, 284);
		text("Feed", 2, 0xffb000, false, true).at(400, 284);
		sprite(488).at(220, 230);
		rectangle(254, 12, 0xff, true, 170, 1344).at(224, 234);
		text("Exp: 0/0", 0, 0xffb000, true, true).at(350, 234);
		sprite(120).at(273, 85);
		rectangle(148, 12, 0xff0000, true, 170, 0).at(278, 89);
		rectangle(148, 12, 0xff00, true, 120, 1343).at(278, 89);
		text("HP: 0/0", 0, 0xffb000, true, true).at(352, 89);
		text("Level: 0", 1, 0xffb000, false, true).at(80, 100);
		text("Hitpoints: 0", 1, 0xffb000, false, true).at(30, 125);
		text("Strength: 0", 1, 0xffb000, false, true).at(30, 140);
		text("Magic: 0", 1, 0xffb000, false, true).at(30, 155);
		text("Range: 0", 1, 0xffb000, false, true).at(30, 170);
		text("Stat points: 0", 1, 0xffb000, false, true).at(30, 300);
		hover("Level-up Hitpoints", 295, 296, 1, 0).at(120, 125);
		hover("Level-up Strength", 295, 296, 1, 0).at(120, 140);
		hover("Level-up Magic", 295, 296, 1, 0).at(120, 155);
		hover("Level-up Range", 295, 296, 1, 0).at(120, 170);
		npcModel(0, 84, 89 * 2, 1000, 0, 0, 0, 1342).at(310, 130);
		sprite(84, -1, 8100, 1, 85, 40, 40, "Toggle 70% Taunt").at(442, 73);
		sprite(1201).at(444, 75);
		//1030
	}

}
