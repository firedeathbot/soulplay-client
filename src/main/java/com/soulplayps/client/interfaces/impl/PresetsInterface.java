package com.soulplayps.client.interfaces.impl;

import static com.soulplayps.client.interfaces.RSInterface.RECT_IGNORE_ATLAS;
import static com.soulplayps.client.interfaces.RSInterface.TILING_FLAG_BIT;
import static com.soulplayps.client.interfaces.RSInterface.addButton;
import static com.soulplayps.client.interfaces.RSInterface.addInterface;
import static com.soulplayps.client.interfaces.RSInterface.addLayer;
import static com.soulplayps.client.interfaces.RSInterface.addNewText;
import static com.soulplayps.client.interfaces.RSInterface.addRectangle;
import static com.soulplayps.client.interfaces.RSInterface.addSprite;

import com.soulplayps.client.interfaces.MouseEvent;
import com.soulplayps.client.interfaces.RSInterface;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.raster.sprite.Sprite;

public class PresetsInterface {

	public static void init() {
		int width = 500;
		int height = 291;
		int inventoryWidth = 173;
		int inventoryHeight = 291 - 35 - 5;
		RSInterface inter = addInterface(70600);
		addSprite(70601, 1288);
		addNewText(70602, "Presets", width, 22, 2, 0xe2b800, 0, true, 1, 1, 0);
		addSprite(70603, 854, 6, height);
		RSInterface.interfaceCache[70603].setFlag(TILING_FLAG_BIT);
		addSprite(70604, 853, 143, 6);
		RSInterface.interfaceCache[70604].setFlag(TILING_FLAG_BIT);
		addSprite(70605, 853, 158, 6);
		RSInterface.interfaceCache[70605].setFlag(TILING_FLAG_BIT);
		addSprite(70606, 853, 173, 6);
		RSInterface.interfaceCache[70606].setFlag(TILING_FLAG_BIT);
		addButton(70607, 104, 105, "Load this preset");
		addNewText(70608, "Load this preset", 150, 25, 1, 0xe2b800, 0, true, 1, 1, 0);
		addButton(70609, 104, 105, "Save this preset");
		addNewText(70610, "Save this preset", 150, 25, 1, 0xe2b800, 0, true, 1, 1, 0);
		addNewText(70611, "Presets", 143, 22, 1, 0xe2b800, 0, true, 1, 1, 0);
		addNewText(70612, "", 158, 22, 1, 0xe2b800, 0, true, 1, 1, 0);
		addRectangle(70613, 0, 0x312921, true, 143 + 6 + 158, 22, 0);// 0x332417
		RSInterface.interfaceCache[70613].setFlag(RECT_IGNORE_ATLAS);
		addRectangle(70614, 0, 0x312921, true, 158, 57, 0);// 0x332417
		RSInterface.interfaceCache[70614].setFlag(RECT_IGNORE_ATLAS);
		addRectangle(70615, 0, 0x312921, true, 173, 35, 0);// 0x332417
		RSInterface.interfaceCache[70615].setFlag(RECT_IGNORE_ATLAS);
		addSprite(70616, 6, 192, "miscgraphics,2");
		RSInterface.interfaceCache[70616].setFlag(TILING_FLAG_BIT);
		addSprite(70617, 110, 6, "miscgraphics,3");
		RSInterface.interfaceCache[70617].setFlag(TILING_FLAG_BIT);
		addSprite(70618, 140, 6, "miscgraphics,3");
		RSInterface.interfaceCache[70618].setFlag(TILING_FLAG_BIT);
		addSprite(70619, 6, 80, "miscgraphics,2");
		RSInterface.interfaceCache[70619].setFlag(TILING_FLAG_BIT);
		addSprite(70620, 6, 80, "miscgraphics,2");
		RSInterface.interfaceCache[70620].setFlag(TILING_FLAG_BIT);
		addSprite(70621, 36, 36, "miscgraphics,0");
		addSprite(70622, 36, 36, "miscgraphics,0");
		addSprite(70623, 36, 36, "miscgraphics,0");
		addSprite(70624, 36, 36, "miscgraphics,0");
		addSprite(70625, 36, 36, "miscgraphics,0");
		addSprite(70626, 36, 36, "miscgraphics,0");
		addSprite(70627, 36, 36, "miscgraphics,0");
		addSprite(70628, 36, 36, "miscgraphics,0");
		addSprite(70629, 36, 36, "miscgraphics,0");
		addSprite(70630, 36, 36, "miscgraphics,0");
		addSprite(70631, 36, 36, "miscgraphics,0");
		
		addSprite(70632, 36, 36, "wornicons,0");
		addSprite(70633, 36, 36, "wornicons,1");
		addSprite(70634, 36, 36, "wornicons,2");
		addSprite(70635, 36, 36, "wornicons,3");
		addSprite(70636, 36, 36, "wornicons,5");
		addSprite(70637, 36, 36, "wornicons,6");
		addSprite(70638, 36, 36, "wornicons,7");
		addSprite(70639, 36, 36, "wornicons,8");
		addSprite(70640, 36, 36, "wornicons,9");
		addSprite(70641, 36, 36, "wornicons,4");
		addSprite(70642, 36, 36, "wornicons,10");
		addButton(70643, 300, 301, "Close");
		RSInterface.interfaceCache[70643].atActionType = 3;

		inter.totalChildren(50);
		inter.child(0, 70601, 6, 6);
		inter.child(1, 70602, 6, 6);
		inter.child(2, 70613, 13, 28);
		inter.child(3, 70614, 13 + 149, 28 + 228 + 6);
		inter.child(4, 70615, 13 + 149 + 164, 28 + 250 + 6);
		inter.child(5, 70603, width - 174 - 6, 28);
		inter.child(6, 70603, width - 174 - 6 - 164, 28);
		inter.child(7, 70604, 13, 28 + 22);
		inter.child(8, 70605, 13 + 149, 28 + 22);
		inter.child(9, 70605, 13 + 149, 28 + 228);
		inter.child(10, 70606, 13 + 149 + 164, 28 + 250);
		inter.child(11, 70607, 13 + 149 + 4, 28 + 227 + 10);
		inter.child(12, 70608, 13 + 149 + 4, 28 + 227 + 10);
		inter.child(13, 70609, 13 + 149 + 4, 28 + 227 + 10 + 26);
		inter.child(14, 70610, 13 + 149 + 4, 28 + 227 + 10 + 26);
		inter.child(15, 70611, 13, 28);
		inter.child(16, 70612, 13 + 149, 28);

		// equipment
		inter.child(17, 70616, 13 + 149 + 79 - 16 - 1, 28 + 28 + 4);
		inter.child(18, 70617, 13 + 149 + 25, 28 + 28 + 3 + 36 + 3 + 18 - 16);
		inter.child(19, 70618, 13 + 149 + 10, 28 + 28 + 3 + 36 + 3 + 36 + 3 + 18 - 16);
		inter.child(20, 70619, 13 + 149 + 79 - 3 - 20 - 36 - 16, 28 + 28 + 3 + 36 + 3 + 36 + 3 + 18);
		inter.child(21, 70620, 13 + 149 + 79 - 3 + 20 + 36 - 16, 28 + 28 + 3 + 36 + 3 + 36 + 3 + 18);
		
		inter.child(22, 70621, 13 + 149 + 79 - 18, 28 + 28 + 4);
		inter.child(23, 70622, 13 + 149 + 79 - 18 - 5 - 36, 28 + 28 + 4 + 36 + 3);
		inter.child(24, 70623, 13 + 149 + 79 - 18, 28 + 28 + 4 + 36 + 3);
		inter.child(25, 70624, 13 + 149 + 79 - 18 + 36 + 5, 28 + 28 + 4 + 36 + 3);
		inter.child(26, 70625, 13 + 149 + 79 - 18 - 36 - 20, 28 + 28 + 4 + 36 + 3 + 36 + 3);
		inter.child(27, 70626, 13 + 149 + 79 - 18, 28 + 28 + 4 + 36 + 3 + 36 + 3);
		inter.child(28, 70627, 13 + 149 + 79 - 18 + 36 + 20, 28 + 28 + 4 + 36 + 3 + 36 + 3);
		inter.child(29, 70628, 13 + 149 + 79 - 18, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3);
		inter.child(30, 70629, 13 + 149 + 79 - 18 - 36 - 20, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3 + 36 + 3);
		inter.child(31, 70630, 13 + 149 + 79 - 18, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3 + 36 + 3);
		inter.child(32, 70631, 13 + 149 + 79 - 18 + 36 + 20, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3 + 36 + 3);
		
		inter.child(33, 70632, 13 + 149 + 79 - 18 + 2, 28 + 28 + 4 + 2);
		inter.child(34, 70633, 13 + 149 + 79 - 18 - 5 - 36 + 2, 28 + 28 + 4 + 36 + 3 + 2);
		inter.child(35, 70634, 13 + 149 + 79 - 18 + 2, 28 + 28 + 4 + 36 + 3 + 2);
		inter.child(36, 70642, 13 + 149 + 79 - 18 + 36 + 5 + 2, 28 + 28 + 4 + 36 + 3 + 2);
		inter.child(37, 70635, 13 + 149 + 79 - 18 - 36 - 20 + 2, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 2);
		inter.child(38, 70636, 13 + 149 + 79 - 18 + 2, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 2);
		inter.child(39, 70637, 13 + 149 + 79 - 18 + 36 + 20 + 2, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 2);
		inter.child(40, 70638, 13 + 149 + 79 - 18 + 2, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3 + 2);
		inter.child(41, 70639, 13 + 149 + 79 - 18 - 36 - 20 + 2, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3 + 36 + 3 + 2);
		inter.child(42, 70640, 13 + 149 + 79 - 18 + 2, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3 + 36 + 3 + 2);
		inter.child(43, 70641, 13 + 149 + 79 - 18 + 36 + 20 + 2, 28 + 28 + 4 + 36 + 3 + 36 + 3 + 36 + 3 + 36 + 3 + 2);
		
		inter.child(44, 70650, 13 + 149 + 164, 28);
		inter.child(45, 70651, 8 + 6, 51 + 6);
		inter.child(46, 70643, 484, 10);
		inter.child(47, 70652, 7 + 6, 292 + 6);
		inter.child(48, 70653, 7 + 6, 292 + 6);
		inter.child(49, 70654, 8 + 6, 51 + 6);

		addLayer(70650, inventoryWidth, inventoryHeight);
		addLayer(70651, 142, 242);
		addSprite(70652, 1322, 1323, 142, 20, "Select");
		addNewText(70653, "View shared presets", 143, 20, 0, 0xe2b800, 0, true, 1, 1, 0);
		RSInterface sentPresets = addLayer(70654, 142, 242);
		sentPresets.hidden = true;
		sentPresets.totalChildren(5);
		sentPresets.child(0, 70655, 0, 242 - 20 - 2);
		sentPresets.child(1, 70656, 72, 242 - 20 - 2);
		sentPresets.child(2, 70657, 0, 0);
		sentPresets.child(3, 70658, 0, 242 - 20 - 2);
		sentPresets.child(4, 70659, 72, 242 - 20 - 2);

		addSprite(70655, 1324, 1325, 69, 20, "Select");
		addSprite(70656, 1324, 1325, 69, 20, "Select");
		addLayer(70657, 142, 221);
		addNewText(70658, "Save", 69, 20, 0, 0xe2b800, 0, true, 1, 1, 0);
		addNewText(70659, "Delete", 69, 20, 0, 0xe2b800, 0, true, 1, 1, 0);
	}

	public static void decodeFromServer(RSBuffer buffer) {
		int type = buffer.readUnsignedByte();
		switch (type) {
			case 0: {
				int[] ids = new int[28];
				int[] amounts = new int[28];
				for (int i = 0; i < 28; i++) {
					int itemId = buffer.read3Bytes();
					if (itemId == 16777215) {
						itemId = -1;
					}

					ids[i] = itemId;
					amounts[i] = buffer.readDWord();
				}

				updateItems(ids, amounts);
				break;
			}
			case 1: {
				for (int i = 0; i < 14; i++) {
					int itemId = buffer.read3Bytes();
					if (itemId == 16777215) {
						itemId = -1;
					}

					int amount = buffer.readDWord();
					switch (i) {
						case 0:
							updateEquipment(RSInterface.interfaceCache[70632], itemId, amount, "wornicons,0");
							break;
						case 1:
							updateEquipment(RSInterface.interfaceCache[70633], itemId, amount, "wornicons,1");
							break;
						case 2:
							updateEquipment(RSInterface.interfaceCache[70634], itemId, amount, "wornicons,2");
							break;
						case 3:
							updateEquipment(RSInterface.interfaceCache[70635], itemId, amount, "wornicons,3");
							break;
						case 4:
							updateEquipment(RSInterface.interfaceCache[70636], itemId, amount, "wornicons,5");
							break;
						case 5:
							updateEquipment(RSInterface.interfaceCache[70637], itemId, amount, "wornicons,6");
							break;
						case 7:
							updateEquipment(RSInterface.interfaceCache[70638], itemId, amount, "wornicons,7");
							break;
						case 9:
							updateEquipment(RSInterface.interfaceCache[70639], itemId, amount, "wornicons,8");
							break;
						case 10:
							updateEquipment(RSInterface.interfaceCache[70640], itemId, amount, "wornicons,9");
							break;
						case 12:
							updateEquipment(RSInterface.interfaceCache[70641], itemId, amount, "wornicons,4");
							break;
						case 13:
							updateEquipment(RSInterface.interfaceCache[70642], itemId, amount, "wornicons,10");
							break;
					}
				}
				break;
			}
			case 2: {
				updatePresetsList(buffer);
				break;
			}
			case 3: {
				updateSharedPresetsList(buffer);
				break;
			}
		}
	}

	private static void updatePresetsList(RSBuffer buffer) {
		RSInterface parent = RSInterface.interfaceCache[70651];
		parent.dynamicComponents = null;

		int height = 20;
		int index = 0;
		int amount = buffer.readUnsignedByte();
		int selectedIndex = buffer.readSignedByte();

		for (int i = 0; i < amount; i++) {
			final String presetName = buffer.readString();
			int y = i * height;
			final int origTrans = i % 2 != 0 ? 235 : 255;
			final RSInterface rectangle = RSInterface.addRectangle(0, y, parent.width, height, 0xffffff, origTrans, true);
			rectangle.actions = new String[3];
			rectangle.actions[0] = "Select";
			rectangle.actions[1] = "Remove";
			rectangle.actions[2] = "Share";
			rectangle.targetVerb = "<col=ff9040>" + presetName;

			if (i != selectedIndex) {
				rectangle.onMouseOverEnter = new MouseEvent() {
					@Override
					public void run() {
						rectangle.transparency = 225;
					}
				};

				rectangle.onMouseLeave = new MouseEvent() {
					@Override
					public void run() {
						rectangle.transparency = origTrans;
					}
				};
			} else {
				rectangle.transparency = 200;
			}

			parent.createDynamic(index++, rectangle);
			parent.createDynamic(index++, RSInterface.addNewText(presetName, 0, y, parent.width, height, 0, 0xffffff, 0, true, 1, 1, 0));
		}

		int y = amount * height;
		final int origTrans = 180;
		final RSInterface rectangle = RSInterface.addRectangle(0, y, parent.width, height, 3089433, origTrans, true);
		rectangle.actions = new String[1];
		rectangle.actions[0] = "Select";

		parent.createDynamic(index++, rectangle);
		parent.createDynamic(index++, RSInterface.addRectangle(0, y, parent.width, height, 0, 0, false));
		parent.createDynamic(index++, RSInterface.addNewText("Add new preset", 0, y, parent.width, height, 0, 0xffffff, 0, true, 1, 1, 0));
	}

	private static void updateSharedPresetsList(RSBuffer buffer) {
		RSInterface parent = RSInterface.interfaceCache[70657];
		parent.dynamicComponents = null;

		int height = 20;
		int index = 0;
		int amount = buffer.readUnsignedByte();
		int selectedIndex = buffer.readSignedByte();

		for (int i = 0; i < amount; i++) {
			final String presetName = buffer.readString();
			int y = i * height;
			final int origTrans = i % 2 != 0 ? 235 : 255;
			final RSInterface rectangle = RSInterface.addRectangle(0, y, parent.width, height, 0xffffff, origTrans, true);
			rectangle.actions = new String[1];
			rectangle.actions[0] = "Select";
			rectangle.targetVerb = "<col=ff9040>" + presetName;

			if (i != selectedIndex) {
				rectangle.onMouseOverEnter = new MouseEvent() {
					@Override
					public void run() {
						rectangle.transparency = 225;
					}
				};

				rectangle.onMouseLeave = new MouseEvent() {
					@Override
					public void run() {
						rectangle.transparency = origTrans;
					}
				};
			} else {
				rectangle.transparency = 200;
			}

			parent.createDynamic(index++, rectangle);
			parent.createDynamic(index++, RSInterface.addNewText(presetName, 0, y, parent.width, height, 0, 0xffffff, 0, true, 1, 1, 0));
		}
	}

	private static void updateEquipment(RSInterface child, int itemId, int amount, String sprite) {
		if (itemId == -1) {
			int comma = sprite.lastIndexOf(",");
			child.disabledSprite = new Sprite(RSInterface.aClass44, sprite.substring(0, comma), Integer.parseInt(sprite.substring(comma + 1)), RSInterface.mediaIndex);
			child.itemId = -1;
		} else {
			child.disabledSprite = null;
			child.itemId = itemId;
			child.itemAmount = amount;
		}
	}

	private static void updateItems(int[] ids, int[] amounts) {
		RSInterface inventoryLayer = RSInterface.interfaceCache[70650];
		inventoryLayer.dynamicComponents = null;

		for (int i = 0; i < 28; i++) {
			int startX = (i % 4) * 40;
			int startY = (i / 4) * 35;

			RSInterface itemSprite = RSInterface.addSprite(startX + 10, startY + 5, 36, 32, null);
			itemSprite.itemId = ids[i];
			itemSprite.itemAmount = amounts[i];
			inventoryLayer.createDynamic(i, itemSprite);
		}
	}

}
