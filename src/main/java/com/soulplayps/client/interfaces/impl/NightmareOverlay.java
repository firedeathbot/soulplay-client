package com.soulplayps.client.interfaces.impl;

import static com.soulplayps.client.interfaces.RSInterface.TILING_FLAG_BIT;
import static com.soulplayps.client.interfaces.RSInterface.addInterface;
import static com.soulplayps.client.interfaces.RSInterface.addLayer;
import static com.soulplayps.client.interfaces.RSInterface.addRectangle;

import com.soulplayps.client.interfaces.InterfaceEvent;
import com.soulplayps.client.interfaces.RSInterface;
import com.soulplayps.client.node.io.RSBuffer;

public class NightmareOverlay {

	public static void init() {
		RSInterface inter = addInterface(50200);
		inter.x = 5;
		inter.y = 14;

		int width = 139;
		int height = 64;
		final RSInterface content = addLayer(50201, width, height);
		content.onLoad = new InterfaceEvent() {
			@Override
			public void run() {
				editTransMass(255);
				editTextMass(true);
				content.onTimer = fadeIn(255, content);
			}
		};

		addRectangle(50202, 255, 1643793, false, width - 2, height - 2);
		RSInterface background = RSInterface.addSprite(50203, width - 4, height - 4, "tradebacking,0");
		background.transparency = 255;
		background.setFlag(TILING_FLAG_BIT);

		int boxWidth = 65;
		int boxHeight = 30;
		RSInterface box1 = addLayer(50204, boxWidth, boxHeight);
		buildProgress(box1, "North West");
		RSInterface box2 = addLayer(50205, boxWidth, boxHeight);
		buildProgress(box2, "South West");
		RSInterface box3 = addLayer(50206, boxWidth, boxHeight);
		buildProgress(box3, "North East");
		RSInterface box4 = addLayer(50207, boxWidth, boxHeight);
		buildProgress(box4, "South East");

		final RSInterface fadingHook = addLayer(50208, width, height);
		fadingHook.onTimer = fadingOut(fadingHook);

		inter.totalChildren(1);
		inter.child(0, 50201, 0, 0);

		content.totalChildren(7);
		content.child(0, 50202, width / 2 - (width - 2) / 2, 1);
		content.child(1, 50203, width / 2 - background.width / 2, height / 2 - background.height / 2);
		content.child(2, 50204, 4, 2);
		content.child(3, 50205, 4, 32);
		content.child(4, 50206, width - boxWidth - 4, 2);
		content.child(5, 50207, width - boxWidth - 4, 32);
		content.child(6, 50208, 0, 0);
	}

	public static void decodeFromServer(RSBuffer buffer) {
		int type = buffer.readUnsignedByte();
		switch (type) {
			case 0:
				setBars(buffer.readUnsignedWord(), buffer.readUnsignedWord(), buffer.readUnsignedWord(), buffer.readUnsignedWord(), buffer.readUnsignedWord(), buffer.readUnsignedWord(), buffer.readUnsignedWord(), buffer.readUnsignedWord(), buffer.readUnsignedByte() == 1);
				break;
			case 1:
				final RSInterface content = RSInterface.interfaceCache[50201];
				content.onTimer = fadeOut(0, content);
				break;
		}
	}

	private static InterfaceEvent fadeIn(final int transStart, final RSInterface main) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				final int trans = Math.max(0, transStart - 3);

				editTransMass(trans);

				if (trans <= 0) {
					editTextMass(false);
					main.onTimer = null;
					return;
				}

				main.onTimer = fadeIn(trans, main);
			}
		};

		return event;
	}

	public static InterfaceEvent fadeOut(final int transStart, final RSInterface main) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				if (transStart <= 0) {
					editTextMass(true);
				}

				final int trans = Math.min(255, transStart + 3);

				editTransMass(trans);

				main.onTimer = fadeOut(trans, main);
			}
		};

		return event;
	}

	private static void editTransMass(int trans) {
		RSInterface.interfaceCache[50202].transparency = trans;
		RSInterface.interfaceCache[50203].transparency = trans;
		editTransBox(RSInterface.interfaceCache[50204], trans);
		editTransBox(RSInterface.interfaceCache[50205], trans);
		editTransBox(RSInterface.interfaceCache[50206], trans);
		editTransBox(RSInterface.interfaceCache[50207], trans);
	}

	private static void editTextMass(boolean hidden) {
		editTextVisibility(RSInterface.interfaceCache[50204], hidden);
		editTextVisibility(RSInterface.interfaceCache[50205], hidden);
		editTextVisibility(RSInterface.interfaceCache[50206], hidden);
		editTextVisibility(RSInterface.interfaceCache[50207], hidden);
	}

	public static void setBars(int swMax, int swLp, int seMax, int seLp, int nwMax, int nwLp, int neMax, int neLp, boolean instant) {
		setBar(RSInterface.interfaceCache[50205], swMax, swLp, instant);
		setBar(RSInterface.interfaceCache[50207], seMax, seLp, instant);
		setBar(RSInterface.interfaceCache[50204], nwMax, nwLp, instant);
		setBar(RSInterface.interfaceCache[50206], neMax, neLp, instant);
	}

	private static void setBar(RSInterface box, int max, int lp, boolean instant) {
		if (box.dynamicComponents == null) {
			return;
		}

		RSInterface rectangles = box.dynamicComponents[1];
		if (rectangles.dynamicComponents == null) {
			return;
		}

		setBarInner(rectangles.dynamicComponents[2], rectangles.dynamicComponents[1], max, lp, instant);
	}

	private static RSInterface getComponent(RSInterface box, int index) {
		if (box.dynamicComponents == null) {
			return null;
		}

		RSInterface rectangles = box.dynamicComponents[1];
		if (rectangles.dynamicComponents == null) {
			return null;
		}

		return rectangles.dynamicComponents[index];
	}

	private static void setBarInner(RSInterface bar1, RSInterface bar2, int max, int lp, boolean instant) {
		if (lp == -1 || max <= 0) {
			return;
		}

		int baseWidth = bar2.width;

		int newWidth = lp * baseWidth / max;
		if (newWidth == 0 && lp > 0) {
			newWidth = 1;
		}

		if (newWidth == baseWidth && lp < max) {
			newWidth--;
		}

		if (instant) {
			bar1.width = newWidth;
			if (bar1.width == baseWidth) {
				bar1.disabledColor = 16777215;
			} else {
				bar1.disabledColor = 14406400;
			}
		} else {
			bar2.onTimer = setBarSmooth(bar1, bar2, newWidth);
		}
	}

	private static InterfaceEvent setBarSmooth(final RSInterface bar1, final RSInterface bar2, final int newWidth) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				int width = bar1.width;
				if (width == newWidth) {
					bar1.width = newWidth;
					bar2.onTimer = null;
					if (bar2.width == width) {
						bar1.disabledColor = 16777215;
					} else {
						bar1.disabledColor = 14406400;
					}
					return;
				}

				if (width < newWidth) {
					bar1.width++;
				} else if (width > newWidth) {
					bar1.width--;
				}
			}
		};

		return event;
	}

	private static InterfaceEvent fadingOut(final RSInterface parent) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				RSInterface box1 = getComponent(RSInterface.interfaceCache[50204], 2);
				RSInterface box2 = getComponent(RSInterface.interfaceCache[50205], 2);
				RSInterface box3 = getComponent(RSInterface.interfaceCache[50206], 2);
				RSInterface box4 = getComponent(RSInterface.interfaceCache[50207], 2);
				RSInterface box1Base = getComponent(RSInterface.interfaceCache[50204], 1);
				RSInterface box2Base = getComponent(RSInterface.interfaceCache[50205], 1);
				RSInterface box3Base = getComponent(RSInterface.interfaceCache[50206], 1);
				RSInterface box4Base = getComponent(RSInterface.interfaceCache[50207], 1);

				int trans = 0;
				int boxTrans = box1.transparency;
				if (boxTrans != 0) {
					trans = boxTrans;
				} else {
					boxTrans = box2.transparency;
					if (boxTrans != 0) {
						trans = boxTrans;
					} else {
						boxTrans = box3.transparency;
						if (boxTrans != 0) {
							trans = boxTrans;
						} else {
							boxTrans = box4.transparency;
							if (boxTrans != 0) {
								trans = boxTrans;
							}
						}
					}
				}

				boolean setBox1 = false;
				boolean setBox2 = false;
				boolean setBox3 = false;
				boolean setBox4 = false;
				if (box1.width == box1Base.width) {
					setBox1 = true;
				}
				if (box2.width == box2Base.width) {
					setBox2 = true;
				}
				if (box3.width == box3Base.width) {
					setBox3 = true;
				}
				if (box4.width == box4Base.width) {
					setBox4 = true;
				}

				if (!setBox1 && !setBox2 && !setBox3 && !setBox4) {
					return;
				}

				if (trans >= 210) {
					if (parent != null) {
						parent.onTimer = fadingIn(parent);
					}
					return;
				}

				trans = Math.min(210, trans + 3);
				if (setBox1) {
					box1.transparency = trans;
				}
				if (setBox2) {
					box2.transparency = trans;
				}
				if (setBox3) {
					box3.transparency = trans;
				}
				if (setBox4) {
					box4.transparency = trans;
				}
			}
		};

		return event;
	}

	private static InterfaceEvent fadingIn(final RSInterface parent) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				RSInterface box1 = getComponent(RSInterface.interfaceCache[50204], 2);
				RSInterface box2 = getComponent(RSInterface.interfaceCache[50205], 2);
				RSInterface box3 = getComponent(RSInterface.interfaceCache[50206], 2);
				RSInterface box4 = getComponent(RSInterface.interfaceCache[50207], 2);
				RSInterface box1Base = getComponent(RSInterface.interfaceCache[50204], 1);
				RSInterface box2Base = getComponent(RSInterface.interfaceCache[50205], 1);
				RSInterface box3Base = getComponent(RSInterface.interfaceCache[50206], 1);
				RSInterface box4Base = getComponent(RSInterface.interfaceCache[50207], 1);

				int trans = 0;
				int boxTrans = box1.transparency;
				if (boxTrans != 0) {
					trans = boxTrans;
				} else {
					boxTrans = box2.transparency;
					if (boxTrans != 0) {
						trans = boxTrans;
					} else {
						boxTrans = box3.transparency;
						if (boxTrans != 0) {
							trans = boxTrans;
						} else {
							boxTrans = box4.transparency;
							if (boxTrans != 0) {
								trans = boxTrans;
							}
						}
					}
				}

				if (trans == 0) {
					if (parent != null) {
						parent.onTimer = fadingOut(parent);
					}
					return;
				}

				trans = Math.max(0, trans - 3);
				if (box1.width == box1Base.width) {
					box1.transparency = trans;
				}
				if (box2.width == box2Base.width) {
					box2.transparency = trans;
				}
				if (box3.width == box3Base.width) {
					box3.transparency = trans;
				}
				if (box4.width == box4Base.width) {
					box4.transparency = trans;
				}
			}
		};

		return event;
	}

	private static void editTextVisibility(RSInterface box, boolean hidden) {
		if (box.dynamicComponents == null) {
			return;
		}

		box.dynamicComponents[0].hidden = hidden;
	}

	private static void editTransBox(RSInterface box, int trans) {
		if (box.dynamicComponents == null) {
			return;
		}

		RSInterface rectangles = box.dynamicComponents[1];
		if (rectangles.dynamicComponents == null) {
			return;
		}

		for (int i = 0, length = rectangles.dynamicComponents.length; i < length; i++) {
			rectangles.dynamicComponents[i].transparency = trans;
		}
	}

	private static void buildProgress(RSInterface layer, String text) {
		layer.dynamicComponents = null;
		RSInterface textComponent = RSInterface.addNewText(text, layer.width / 2 - layer.width / 2, 2, layer.width, 15, 0,
				16750623, 0, true, 1, 1, 0);
		textComponent.hidden = true;
		layer.createDynamic(0, textComponent);

		RSInterface rectangles = RSInterface.addLayer(0, 15, layer.width, 13);
		layer.createDynamic(1, rectangles);

		int width = rectangles.width;
		int height = rectangles.height;
		rectangles.dynamicComponents = null;

		rectangles.createDynamic(0,
				RSInterface.addRectangle(width / 2 - (width - 2) / 2, 1, width - 2, height - 2, 1643793, 255, false));
		rectangles.createDynamic(1, RSInterface.addRectangle(width / 2 - (width - 4) / 2, 13 / 2 - (height - 4) / 2,
				width - 4, height - 4, 2960128, 255, true));
		rectangles.createDynamic(2, RSInterface.addRectangle(width / 2 - (width - 4) / 2, 13 / 2 - (height - 4) / 2,
				width - 4, height - 4, 14406400, 255, true));
	}

}
