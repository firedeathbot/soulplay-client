package com.soulplayps.client.interfaces.impl.powers;

public class PowerUpData {

	private final String name;
	private final String desc;
	private final int amount, maxAmount, rgb, imageId, id, rarityType;
	private boolean hidden;

	public PowerUpData(String name, String desc, int amount, int maxAmount, int rgb, int imageId, int id,
			int rarityType) {
		this.name = name;
		this.desc = desc;
		this.amount = amount;
		this.maxAmount = maxAmount;
		this.rgb = rgb;
		this.imageId = imageId;
		this.id = id;
		this.rarityType = rarityType;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public int getAmount() {
		return amount;
	}

	public int getMaxAmount() {
		return maxAmount;
	}

	public int getRgb() {
		return rgb;
	}

	public int getImageId() {
		return imageId;
	}

	public int getId() {
		return id;
	}

	public int getRarityType() {
		return rarityType;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public boolean isHidden() {
		return hidden;
	}

}
