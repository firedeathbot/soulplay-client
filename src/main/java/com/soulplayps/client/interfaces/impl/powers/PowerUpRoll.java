package com.soulplayps.client.interfaces.impl.powers;

import static com.soulplayps.client.interfaces.RSInterface.addInterface;
import static com.soulplayps.client.interfaces.RSInterface.addLayer;
import static com.soulplayps.client.interfaces.RSInterface.addNewText;
import static com.soulplayps.client.interfaces.RSInterface.addSprite;

import com.soulplayps.client.Client;
import com.soulplayps.client.interfaces.InterfaceEvent;
import com.soulplayps.client.interfaces.RSInterface;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.raster.sprite.SpriteCache;

public class PowerUpRoll {

	public static final int ID = 54700;
	public static final int IMAGE_LENGTH = 9;
	public static final int IMAGE_LOOPS = 20;
	public static int[] imageIds = new int[IMAGE_LENGTH];
	public static int imageAmount;
	public static String name;
	public static final int imageStepX = 37;
	public static final int imageOffsetX = 3;
	public static int speed;

	public static void open(RSBuffer buffer) {
		name = buffer.readString();
		for (int i = 0; i < IMAGE_LENGTH; i++) {
			imageIds[i] = buffer.readUnsignedWord();
		}

		imageAmount = IMAGE_LENGTH * IMAGE_LOOPS;
		speed = 8;
		Client.openOverlayInterface(ID);
	}

	public static void init() {
		int width = 264;
		final RSInterface inter = addInterface(ID);
		addSprite(54701, 1399);
		addSprite(54702, 1400);
		addNewText(54703, "New Power Unlocked", 150, 22, 1, 0xFFFFFF, 0, true, 1, 1, 0);
		addNewText(54704, "", 0, 22, 2, 0xadd8e6, 0, true, 1, 1, 0);
		addSprite(54705, 1361);
		addSprite(54707, 1398);
		RSInterface.interfaceCache[54703].transparency = 255;
		RSInterface.interfaceCache[54704].transparency = 255;
		RSInterface.interfaceCache[54705].transparency = 255;
		RSInterface.interfaceCache[54707].transparency = 255;
		addLayer(54706, width - 4, 38);

		inter.totalChildren(7);
		inter.child(0, 54701, 150, 80);
		inter.child(1, 54706, 152, 82);
		inter.child(2, 54702, 264, 81);
		inter.child(3, 54703, 205, 128);
		inter.child(4, 54704, 202 + 36, 150);
		inter.child(5, 54707, 202, 147);
		inter.child(6, 54705, 202 + imageOffsetX, 147 + imageOffsetX);

		final RSInterface rollLayer = RSInterface.interfaceCache[54706];
		rollLayer.onLoad = new InterfaceEvent() {
			@Override
			public void run() {
				int index = 0;
				rollLayer.dynamicComponents = null;
				for (int i = 0; i < imageAmount; i++) {
					//needs to change to 1398 when it lands on the power
					RSInterface border = RSInterface.addSprite(i * imageStepX, 1, 30, 30, 1397, -1, null);
					border.transparency = 255;
					rollLayer.createDynamic(index++, border);
					RSInterface image = RSInterface.addSprite(i * imageStepX + imageOffsetX, 1 + imageOffsetX, 30, 30, imageIds[i % IMAGE_LENGTH], -1, null);
					image.transparency = 255;
					rollLayer.createDynamic(index++, image);
				}

				for (int i = 0; i < inter.children.length; i++) {
					RSInterface.interfaceCache[inter.children[i]].transparency = 255;
				}

				RSInterface.interfaceCache[54703].hidden = true;
				RSInterface.interfaceCache[54704].hidden = true;
				RSInterface.interfaceCache[54704].message = '[' + name + ']';
				RSInterface.interfaceCache[54704].width = Client.newBoldFont.getBasicWidth(RSInterface.interfaceCache[54704].message) + 15;
				RSInterface.interfaceCache[54705].hidden = true;
				RSInterface.interfaceCache[54705].disabledSprite = SpriteCache.get(imageIds[0]);
				RSInterface.interfaceCache[54707].hidden = true;

				rollLayer.onTimer = fadeIn(255, inter, rollLayer);
			}
		};
	}

	private static InterfaceEvent roll(final int loadTick, final RSInterface rollLayer) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				if (rollLayer.dynamicComponents == null) {
					return;
				}

				int speed = Client.interpolateSafe(10, 1, loadTick, loadTick + 250, Client.loopCycle);
				for (int i = 0; i < imageAmount; i++) {
					int child = i * 2;
					RSInterface border = rollLayer.dynamicComponents[child];
					RSInterface image = rollLayer.dynamicComponents[child + 1];

					if (speed == 1 && image.disabledSprite == SpriteCache.get(imageIds[0]) && border.x == 3 * imageStepX) {
						rollLayer.onTimer = fadeInRolled(255, rollLayer);
						return;
					}
				}

				for (int i = 0; i < imageAmount; i++) {
					int child = i * 2;
					RSInterface border = rollLayer.dynamicComponents[child];
					RSInterface image = rollLayer.dynamicComponents[child + 1];
					border.x -= speed;
					image.x -= speed;
				}
			}
		};

		return event;
	}

	private static InterfaceEvent fadeIn(final int transStart, final RSInterface main, final RSInterface holder) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				final int trans = Math.max(0, transStart - 4);

				for (int i = 0; i < main.children.length; i++) {
					RSInterface.interfaceCache[main.children[i]].transparency = trans;
				}

				for (int i = 0; i < holder.dynamicComponents.length; i++) {
					holder.dynamicComponents[i].transparency = trans;
				}
				
				if (trans <= 0) {
					holder.onTimer = roll(Client.loopCycle, holder);
					return;
				}

				holder.onTimer = fadeIn(trans, main, holder);
			}
		};

		return event;
	}

	private static InterfaceEvent fadeOut(final int transStart, final RSInterface main, final RSInterface holder) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				final int trans = Math.min(255, transStart + 6);

				for (int i = 0; i < main.children.length; i++) {
					RSInterface.interfaceCache[main.children[i]].transparency = trans;
				}

				for (int i = 0; i < holder.dynamicComponents.length; i++) {
					holder.dynamicComponents[i].transparency = trans;
				}

				if (trans <= 0) {
					holder.onTimer = null;
					Client.openOverlayInterface(-1);
					return;
				}

				holder.onTimer = fadeOut(trans, main, holder);
			}
		};

		return event;
	}

	private static InterfaceEvent fadeInRolled(final int transStart, final RSInterface holder) {
		InterfaceEvent event = new InterfaceEvent() {
			@Override
			public void run() {
				final int trans = Math.max(0, transStart - 4);

				RSInterface.interfaceCache[54703].transparency = trans;
				RSInterface.interfaceCache[54704].transparency = trans;
				RSInterface.interfaceCache[54705].transparency = trans;
				RSInterface.interfaceCache[54707].transparency = trans;
				RSInterface.interfaceCache[54703].hidden = false;
				RSInterface.interfaceCache[54704].hidden = false;
				RSInterface.interfaceCache[54705].hidden = false;
				RSInterface.interfaceCache[54707].hidden = false;

				if (trans <= 0) {
					final int startClock = Client.loopCycle + 100;
					holder.onTimer = new InterfaceEvent() {
						@Override
						public void run() {
							if (Client.loopCycle < startClock) {
								return;
							}

							holder.onTimer = fadeOut(0, RSInterface.interfaceCache[54700], holder);
						}
					};
					return;
				}

				holder.onTimer = fadeInRolled(trans, holder);
			}
		};

		return event;
	}

}
