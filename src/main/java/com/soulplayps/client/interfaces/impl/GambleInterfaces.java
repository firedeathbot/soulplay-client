package com.soulplayps.client.interfaces.impl;

import static com.soulplayps.client.interfaces.RSInterface.*;

import com.soulplayps.client.Client;
import com.soulplayps.client.ConfigCodes;
import com.soulplayps.client.interfaces.InterfaceEvent;
import com.soulplayps.client.interfaces.MouseEvent;
import com.soulplayps.client.interfaces.RSInterface;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.node.raster.sprite.SpriteCache;

public class GambleInterfaces {

	public static void initGambleInventory() {
		RSInterface inter = addInterface(73033);
		addInventory(73034, 4, 7, 10, 4, false, "Offer 1", "Offer 5", "Offer 10", null, "Offer X", null, "Offer All");
		RSInterface.interfaceCache[73034].parentID = 73033;
		inter.totalChildren(1);
		inter.child(0, 73034, 16, 8);
	}

	public static void initConfirm() {
		int width = 500;
		RSInterface inter = addInterface(73500);
		addSprite(73501, 1288);
		addNewText(73502, "Gamble Confirmation <col=F71910>Check Before Accepting</col>", width, 22, 2, 16750623, 0, true, 1, 1, 0);
		addSprite(73503, 854, 6, 292); 
		RSInterface.interfaceCache[73503].setFlag(TILING_FLAG_BIT);
		addSprite(73504, 854, 6, 292); 
		RSInterface.interfaceCache[73504].setFlag(TILING_FLAG_BIT);
		addSprite(73505, 853, 174, 6); 
		RSInterface.interfaceCache[73505].setFlag(TILING_FLAG_BIT);

		addButton(73506, 1320, 1319, "Accept");
		addButton(73507, 1320, 1319, "Decline");
		addNewText(73508, "Accept", 80, 25, 1, 0x3DCD01, 0, true, 1, 1, 0);
		addNewText(73509, "Decline", 80, 25, 1, 0x7C0704, 0, true, 1, 1, 0);

		addRectangle(73510, 0, 0, false, 146, 1, 0);//0x332417
		addRectangle(73511, 0, 0, false, 146, 1, 0);//0x332417
		addRectangle(73512, 0, 0, false, 146, 1, 0);//0x332417
		addRectangle(73513, 0, 0, false, 146, 1, 0);//0x332417

		addNewText(73514, "Your stake:", 150, 20, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(73515, "Player name stake:", 150, 20, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(73516, "0 gp", 145, 20, 1, 16750623, 0, true, 2, 1, 0);
		addNewText(73517, "0 gp", 145, 20, 1, 16750623, 0, true, 2, 1, 0);

		RSInterface inv1 = addLayer(73518, 147, 250);
		addInventory(73520, 4, 7, 6, 4, false);
		inv1.totalChildren(1);
		inv1.child(0, 73520, 0, 0);

		RSInterface inv2 = addLayer(73519, 147, 250);
		inv2.totalChildren(1);
		inv2.child(0, 73521, 0, 0);
		addInventory(73521, 4, 7, 6, 4, false);
		/*for (int i = 0; i < 28; i++) {
			RSInterface.interfaceCache[73520].inv[i] = 4152;
			RSInterface.interfaceCache[73521].inv[i] = 4152;
		}*/
		
		addButton(73522, 300, 301, "Close");
		RSInterface.interfaceCache[73522].atActionType = 3;

		inter.totalChildren(21);
		inter.child(0, 73501, 6, 6);
		inter.child(1, 73502, 6, 6);
		inter.child(2, 73503, 500 - 150 - 8, 27);
		inter.child(3, 73504, 12 + 150, 27);
		inter.child(4, 73505, 12 + 150 + 6, 27 + 238);
		
		inter.child(5, 73506, 12 + 150 + 6 + 7, 290);
		inter.child(6, 73507, 12 + 150 + 6 + 7 + 82, 290);
		inter.child(7, 73508, 12 + 150 + 6 + 7, 290);
		inter.child(8, 73509, 12 + 150 + 6 + 7 + 82, 290);
		
		inter.child(9, 73510, 12 + 2, 27 + 20);
		inter.child(10, 73511, 500 - 150, 27 + 20);	
		inter.child(11, 73510, 12 + 2, 27 + 272);
		inter.child(12, 73511, 500 - 150, 27 + 272);	
		
		inter.child(13, 73514, 12 + 2, 27);
		inter.child(14, 73515, 500 - 150, 27);
		inter.child(15, 73516, 12 + 2, 27 + 272);
		inter.child(16, 73517, 500 - 150, 27 + 272);
		inter.child(17, 73550, 12 + 150 + 6, 27);
		inter.child(18, 73518, 9 + 5, 43 + 9);
		inter.child(19, 73519, 345 + 5, 43 + 9);
		inter.child(20, 73522, 484, 10);
		
		addLayer(73550, 174, 240);
	
		
		RSInterface infoLayer = RSInterface.interfaceCache[73550];
		
		//more info and shit will prob be added into the categories so other categories should automatically change height to not collide
		addNewText(73551, "General Information:", 150, 20, 2, 0x3CBBDC, 0, true, 0, 1, 0);
		addNewText(73552, "Game Mode: <col=BFFF09>Flower Poker", 150, 20, 1, 0x47FF33/*0xBFFF09*/, 0, true, 0, 1, 0);
		addNewText(73553, "Opponent:", 145, 20, 2, 0X3AA6DC, 0, true, 0, 1, 0);
		addNewText(73554, "Name: <col=BFFF09>Michael", 145, 20, 1, 0x47FF33, 0, true, 0, 1, 0);
		
		//"Host:"
		addNewText(73555, "", 150, 20, 2, 0x3790DC, 0, true, 0, 1, 0);
		//"Name: <col=BFFF09>Julius"
		addNewText(73556, "", 145, 20, 1, 0x47FF33/*0x25D000*/, 0, true, 0, 1, 0);
		//"Comission: <col=BFFF09>8%"
		addNewText(73557, "", 145, 20, 1, 0x47FF33/*0x25D000*/, 0, true, 0, 1, 0);
		addNewText(73558, "", 174, 15, 0, 16750623, 0, true, 1, 1, 0);
		
		infoLayer.totalChildren(8);
		infoLayer.child(0, 73551, 10, 10);
		infoLayer.child(1, 73552, 10, 30);
		infoLayer.child(2, 73553, 10, 60);
		infoLayer.child(3, 73554, 10, 80);
		infoLayer.child(4, 73555, 10, 110);
		infoLayer.child(5, 73556, 10, 130);
		infoLayer.child(6, 73557, 10, 145);
		infoLayer.child(7, 73558, 0, 220);
	}

	public static void init() {
		int width = 500;
		RSInterface inter = addInterface(73000);
		addSprite(73001, 1288);
		addNewText(73002, "Gamble", width, 22, 2, 16750623, 0, true, 1, 1, 0);
		addSprite(73003, 854, 6, 228);
		RSInterface.interfaceCache[73003].setFlag(TILING_FLAG_BIT);
		addSprite(73004, 854, 6, 292);
		RSInterface.interfaceCache[73004].setFlag(TILING_FLAG_BIT);
		addSprite(73005, 853, 307, 6);
		RSInterface.interfaceCache[73005].setFlag(TILING_FLAG_BIT);
		addRectangle(73006, 0, 0, false, 174, 1);//0x332417
		addRectangle(73007, 0, 0, false, 146, 1);//0x332417
		addRectangle(73008, 0, 0, false, 146, 1);//0x332417
		addRectangle(73009, 0, 0, false, 146, 1);//0x332417
		addRectangle(73010, 0, 0, false, 146, 1);//0x332417
		addNewText(73011, "Your stake:", 150, 20, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(73012, "", 150, 20, 1, 16750623, 0, true, 0, 1, 0);//Player name stake:
		addNewText(73013, "0 gp", 145, 20, 1, 16750623, 0, true, 2, 1, 0);
		addNewText(73014, "0 gp", 145, 20, 1, 16750623, 0, true, 2, 1, 0);
		addButton(73015, 275, 276, "All in");
		addButton(73016, 1316, -1, "Load previous stake");

		final RSInterface gameModeButton = addButton(73017, 1318, -1, "Game Mode");
		final RSInterface hostsButton = addButton(73018, 1317, -1, "Hosts");
		final RSInterface inventoryButton = addButton(73019, 1317, -1, "Inventory");
		gameModeButton.onButtonClick = new InterfaceEvent() {
			@Override
			public void run() {
				Sprite clicked = SpriteCache.get(1318);
				Sprite notClicked = SpriteCache.get(1317);
				gameModeButton.disabledSprite = clicked;
				hostsButton.disabledSprite = notClicked;
				inventoryButton.disabledSprite = notClicked;

				RSInterface.interfaceCache[73050].hidden = false;
				RSInterface.interfaceCache[73080].hidden = true;
				RSInterface.interfaceCache[73100].hidden = true;
			}
		};
		hostsButton.onButtonClick = new InterfaceEvent() {
			@Override
			public void run() {
				Sprite clicked = SpriteCache.get(1318);
				Sprite notClicked = SpriteCache.get(1317);
				gameModeButton.disabledSprite = notClicked;
				hostsButton.disabledSprite = clicked;
				inventoryButton.disabledSprite = notClicked;

				RSInterface.interfaceCache[73050].hidden = true;
				RSInterface.interfaceCache[73080].hidden = false;
				RSInterface.interfaceCache[73100].hidden = true;
			}
		};
		inventoryButton.onButtonClick = new InterfaceEvent() {
			@Override
			public void run() {
				Sprite clicked = SpriteCache.get(1318);
				Sprite notClicked = SpriteCache.get(1317);
				gameModeButton.disabledSprite = notClicked;
				hostsButton.disabledSprite = notClicked;
				inventoryButton.disabledSprite = clicked;

				RSInterface.interfaceCache[73050].hidden = true;
				RSInterface.interfaceCache[73080].hidden = true;
				RSInterface.interfaceCache[73100].hidden = false;
			}
		};

		addButton(73020, 1320, 1319, "Accept");
		addButton(73021, 1320, 1319, "Decline");
		addNewText(73022, "Accept", 80, 25, 1, 0x3DCD01, 0, true, 1, 1, 0);
		addNewText(73023, "Decline", 80, 25, 1, 0x7C0704, 0, true, 1, 1, 0);
		addNewText(73024, "", 185, 56, 2, 0xBA2516, 0, true, 1, 1, 0);
		addSprite(73025, 844, 32, 32);
		addSprite(73026, 1321, 27, 28);

		addItemSprite(73027, 2468, 32, 32);
		addItemSprite(73028, 15098, 32, 32);
		RSInterface inv1 = addLayer(73029, 128, 187);
		addInventory(73031, 4, 7, 0, 3, false, "Take 1", "Take 5", "Take 10", null, "Take X", null, "Take All");
		inv1.totalChildren(1);
		inv1.child(0, 73031, 0, 0);
		inv1.scrollMax = 7 * (32 + 2);

		RSInterface inv2 = addLayer(73030, 128, 187);
		inv2.totalChildren(1);
		inv2.child(0, 73032, 0, 0);
		inv2.scrollMax = 7 * (32 + 2);
		addInventory(73032, 4, 7, 0, 3, false);
		/*for (int i = 0; i < 28; i++) {
			RSInterface.interfaceCache[73031].inv[i] = 4152;
			RSInterface.interfaceCache[73032].inv[i] = 4152;
		}*/

		addButton(73035, 300, 301, "Close");
		RSInterface.interfaceCache[73035].atActionType = 3;

		inter.totalChildren(34);
		inter.child(0, 73001, 6, 6);
		inter.child(1, 73002, 6, 6);
		inter.child(2, 73003, 500 - 150 - 8, 27);
		inter.child(3, 73004, 500 - 150 - 150 - 8 - 6, 27);
		inter.child(4, 73005, 500 - 150 - 150 - 8, 27 + 228);
		inter.child(5, 73006, 12, 27 + 42);
		inter.child(6, 73007, 12 + 6 + 174 + 2, 27 + 20);
		inter.child(7, 73008, 12 + 6 + 174 + 2, 320 - 64 - 20);
		inter.child(8, 73009, 12 + 6 + 174 + 6 + 2 + 150, 27 + 20);
		inter.child(9, 73010, 12 + 6 + 174 + 6 + 2 + 150, 320 - 64 - 20);
		inter.child(10, 73011, 12 + 6 + 174 + 3, 27);
		inter.child(11, 73012, 12 + 6 + 174 + 6 + 3 + 150, 27);
		inter.child(12, 73013, 12 + 6 + 174 + 2, 320 - 64 - 20);
		inter.child(13, 73014, 12 + 6 + 174 + 6 + 2 + 150, 320 - 64 - 20);
		inter.child(14, 73015, 12 + 6 + 174 + 1, 320 - 58);
		inter.child(15, 73016, 12 + 6 + 174 + 130, 29);
		inter.child(16, 73017, 12 + 1, 27 + 1);
		inter.child(17, 73018, 12 + 4 + 38, 27 + 1);
		inter.child(18, 73019, 12 + 7 + 38 + 38, 27 + 1);	
		inter.child(19, 73020, 500 - 6 - 80, 320 - 56);
		inter.child(20, 73021, 500 - 6 - 80, 320 - 30);
		inter.child(21, 73022, 500 - 6 - 80, 320 - 56);
		inter.child(22, 73023, 500 - 6 - 80, 320 - 30);
		inter.child(23, 73024, 12 + 6 + 174 + 1 + 35, 320 - 58);	
		inter.child(24, 73025, 12 + 4 + 38 + 13, 27 + 1 + 4);
		inter.child(25, 73026, 12 + 7 + 38 + 38 + 6, 27 + 1 + 6);
		inter.child(26, 73027, 12 + 1 + 10, 27 + 1 + 1);
		inter.child(27, 73028, 10, 27 + 1 + 7);
		inter.child(28, 73050, 12, 27 + 43);
		inter.child(29, 73080, 12, 27 + 43);
		inter.child(30, 73029, 190 + 6, 43 + 6);
		inter.child(31, 73030, 345 + 6, 43 + 6);
		inter.child(32, 73100, 12, 27 + 43);
		inter.child(33, 73035, 484, 10);

		RSInterface gameModeLayer = addLayer(73050, 174, 249);
		RSInterface hostLayer = addLayer(73080, 174, 249);
		RSInterface inventoryLayer = addLayer(73100, 174, 249);

		addRadioButton(73051, 73050, 980, 981, 100, 20, "Flower Poker", 0, 5, ConfigCodes.GAMBLE_GAME_MODE);
		addRadioButton(73052, 73050, 980, 981, 100, 20, "Hot & Cold", 1, 5, ConfigCodes.GAMBLE_GAME_MODE);
		RSInterface.interfaceCache[73052].atActionType = 0;
		addRadioButton(73053, 73050, 980, 981, 100, 20, "55x2", 2, 5, ConfigCodes.GAMBLE_GAME_MODE);
		RSInterface.interfaceCache[73053].atActionType = 0;
		addRadioButton(73054, 73050, 980, 981, 100, 20, "21", 3, 5, ConfigCodes.GAMBLE_GAME_MODE);
		RSInterface.interfaceCache[73054].atActionType = 0;
		addRadioButton(73055, 73050, 980, 981, 100, 20, "Dice Duel", 4, 5, ConfigCodes.GAMBLE_GAME_MODE);
		RSInterface.interfaceCache[73055].atActionType = 0;

		addNewText(73056, "Flower Poker", 80, 20, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(73057, "<str>Hot & Cold", 80, 20, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(73058, "<str>55x2", 80, 20, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(73059, "<str>21 Blackjack", 80, 20, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(73060, "<str>Dice Duel", 80, 20, 1, 16750623, 0, true, 0, 1, 0);
		
		/*addNewText(73061, "The goal with this game mode is to get as close to 21 as possible,"
				+ " host will roll the dice until you say stop. You will <col=BA2516>lose</col> the game if "
				+ "your total rolls are higher than 21. The player with number closest to"
				+ " 21 <col=3DCD01>wins</col> the game.", 160, 118, 1, 0xCCCCCC, 0, true, 1, 0, 0);*/
		addNewText(73061, "Two players plant 5 seeds, who ever gets the best plant combination <col=3DCD01>wins</col> the game.<br>Combinations in a winning order: <col=ffffff>5 of a kind</col>, <col=ffffff>4 of a kind</col>, <col=ffffff>full house(3 of a kind and 1 pair)</col>, <col=ffffff>3 of a kind</col>, <col=ffffff>2 pair</col>, <col=ffffff>1 pair</col>.", 160, 118, 1, 0xCCCCCC, 0, true, 1, 0, 0);

		addRectangle(73062, 0, 0, false, 165, 130, 0);//0x332417
		
		gameModeLayer.totalChildren(12);
		gameModeLayer.child(0, 73051, 10, 10);
		gameModeLayer.child(1, 73052, 10, 30);
		gameModeLayer.child(2, 73053, 10, 50);
		gameModeLayer.child(3, 73054, 10, 70);
		gameModeLayer.child(4, 73055, 10, 90);
		gameModeLayer.child(5, 73056, 30, 10);
		gameModeLayer.child(6, 73057, 30, 30);
		gameModeLayer.child(7, 73058, 30, 50);
		gameModeLayer.child(8, 73059, 30, 70);
		gameModeLayer.child(9, 73060, 30, 90);
		gameModeLayer.child(10, 73061, 7, 120);
		gameModeLayer.child(11, 73062, 5, 115);

		addNewText(73081, "You can find available <col=3DCD01>hosts</col> here, using another person for <col=3DCD01>host</col>"
				+ " can cost you comission, you can see the comission for each host above.", 160, 100, 1, 0xCCCCCC, 0, true, 1, 0, 0);
		addRectangle(73082, 0, 0, false, 170, 1, 0);//0x332417
		addRectangle(73083, 0, 0, false, 165, 110, 0);//0x332417
		
		hostLayer.totalChildren(4);
		hostLayer.child(0, 73081, 7, 140);
		hostLayer.child(1, 73082, 2, 125);
		hostLayer.child(2, 73083, 5, 135);
		hostLayer.child(3, 73090, 0, 0);

		addLayer(73090, 157, 125);
		
		addInventory(73101, 4, 7, 8, 4, false);
		inventoryLayer.totalChildren(1);
		inventoryLayer.child(0, 73101, 12, 3);
		/*for (int i = 0; i < 28; i++) {
			RSInterface.interfaceCache[73101].inv[i] = 4152;
		}*/
		
		gameModeLayer.hidden = false;
		hostLayer.hidden = true;
		inventoryLayer.hidden = true;
	}

	public static void decodeFromServer(RSBuffer buffer) {
		int type = buffer.readUnsignedByte();
		switch (type) {
			case 0: {
				int buttonType = buffer.readUnsignedByte();
				if (buttonType == 1) {
					waitButton(73506, 73508);
				} else {
					waitButton(73020, 73022);
				}
				break;
			}
		}
	}

	public static void waitButton(int spriteId, int textId) {
		final RSInterface sprite = RSInterface.interfaceCache[spriteId];
		if (sprite.onTimer != null) {
			return;
		}

		final RSInterface text = RSInterface.interfaceCache[textId];
		final String oldText = text.message;
		final int oldColor = text.disabledColor;
		final MouseEvent oldEvent = sprite.onMouseOver;
		final int oldButtonType = sprite.atActionType;
		text.message = "Wait...";
		text.disabledColor = 16750623;
		sprite.onMouseOver = null;
		sprite.atActionType = 0;

		final int endCycle = Client.loopCycle + 50;
		sprite.onTimer = new InterfaceEvent() {
			@Override
			public void run() {
				if (Client.loopCycle < endCycle) {
					return;
				}

				text.message = oldText;
				text.disabledColor = oldColor;
				sprite.onMouseOver = oldEvent;
				sprite.atActionType = oldButtonType;
				sprite.onTimer = null;
			}
		};
	}

}
