package com.soulplayps.client.interfaces.impl;

import com.soulplayps.client.interfaces.CustomInterface;
import com.soulplayps.client.node.animable.Animation;

public class PetCollection extends CustomInterface {

	public PetCollection() {
		super(49262);
	}

	@Override
	public void create() {
		final Object[][] data = {
			//	NPc name, npc id, zoom, animation
				{"Chicken", 1017, 1200, 5386}, //lvl 2
				{"Giant Rat", 86, 1100, 14858}, 
				{"Cow", 81, 1400, 5854},
				{"Goblin", 4261, 1300, 6181},
				{"Giant Spider", 59, 1100, 5326},
				{"Crawling Hand", 1648, 1100, 9128}, // 8
				{"Minotaur", 4404, 1700, 4264}, //12	
				{"Rock Crab", 1265, 1300, 1310}, // 13
				{"Giant Mosquito", 4347, 1500, 2395}, // 13
				{"Scorpion", 107, 1300, 6252},// 14
				{"Unicorn", 89, 1900, 6374}, // 15
				{"Ghost", 103 , 1600, 5538}, // 19
				{"Grizzly Bear", 105, 1500, 4920}, // 21
				{"Yak", 5529, 1500, 544}, // 22
				{"Banshee", 1612, 1900, 9452}, //23
				{"Cave Crawler", 1600, 1200, 268}, // 23
				{"Cave Slime", 1831, 1200, 1790}, // 23
				{"Zombie", 75, 1700, 5572}, // 24
				{"White Wolf", 96, 1300, 6580},//25
				{"Skeleton", 92, 1700, 5483}, 
				{"Giant Bat", 78, 1700, 4913}, // 27
				
				{"Hill Giant", 117, 2600, 4650}, // 28
				{"Kalphite Worker", 1153, 1900, 6218}, // 28
				{"Rockslug", 1631, 900, 9509}, // 29
				{"Red Spider", 63, 1100, 5326}, // 34
				{"Fire Elemental", 1019, 1600, 1027}, // 35
				{"Cockatrice", 1620, 1800, 7760}, // 37
				{"Jungle Spider", 62, 1100, 5326}, // 40
				{"Moss Giant", 112, 2700, 4656}, // 42
				{"Hobgoblin", 2688, 1500, 165}, // 42
				{"Pyrefiend", 1633, 1700, 8077}, // 43
				{"Baby Blue Drag", 52, 1800, 14267}, // 48
				{"Baby Red Drag", 1589, 1800, 14267}, //48
				{"Bloodworm", 2031, 900, 2071}, // 52
				{"Ice Giant", 111, 2600, 4670}, // 53
				{"Ice Warrior", 125, 1700, 842}, // 57
				{"Giant ant", 6380 , 700, 7236}, // 58
				{"Basilisk", 1616, 1500, 262}, // 61
				{"Poison Spider", 134, 1100, 5326}, // 64
				{"Infernal Mage", 1643, 1900, 7188}, // 66
				{"Giant wasp", 6381, 1700, 7241}, // 66
				{"Jungle Horror", 4350, 1700, 4231}, // 70
				
				
				
				{"Bloodveld", 1618, 1500, 9133}, // 76
				{"Jelly", 1637, 1000, 8572}, // 78
				{"Green Dragon", 941, 2500, 12248}, // 79
				{"Aviansie", 6233, 3200, 6949}, // 79
				{"Baby black drag", 3376, 1800, 14267}, // 83
				{"Kalphite Soldier", 1154, 1800, 6218}, // 85
				{"Fire Giant", 110, 2600, 4662}, // 86
				{"Dire Wolf", 1198, 1200, 6580}, // 88
				{"Turoth", 1623, 1300, 9474}, // 88
				{"Greater Demon", 83 , 3200, 66}, // 92
				{"Dagannoth", 1343, 1900, 1338}, // 92
				{"Werewolf", 6212, 1500, 6539}, // 93
				{"Aberrant spectre", 1604, 1950, 9469}, // 96
				{"Cave Bug", 5750, 1500, 6078}, // 96
				{"Wallasalki", 2884, 1500, 2364}, // 98
				{"Mummy", 1961, 1500, 5548}, // 103
				{"Kurask", 1608, 1900, 9442}, // 106
				{"Ork", 6274, 1600, 13887}, // 107
				{"Jungle worm", 9467, 2900, 12790}, // 110
				{"Gargoyle", 1610, 2000, 9456}, // 111
				{"Blue Dragon", 55 , 2500, 12248}, // 111
				
				
				{"Aquanite", 9172, 1800, 12038}, //114	
				{"Nechryael", 1613, 1800, 9490}, // 115 
				{"Waterfiend", 5361, 1500, 302}, // 115
		    	{"Rev Imp", 13465, 1000, 7405}, // 7
				{"Rev Goblin", 13469, 1800, 7451}, // 15
				{"Rev Vampyre", 13473, 1800, 7458}, // 68
				{"Rev Pyrefiend", 13471, 1600, 7486}, // 52
				{"Rev Hobgoblin", 13472, 1500, 7421}, // 60
				{"Rev Werewolf", 13474, 1700, 7400}, // 75
				{"Rev Cyclop", 13475, 3100, 7458}, // 82
				{"Rev Demon", 13477, 3500, 7479}, // 98
				{"Rev Ork", 13478, 2100, 7414}, // 105
				{"Rev Knight", 13480, 1700, 7444}, // 128
				{"Rev Beast", 13479, 1900, 7472}, // 120
				{"Rev Hellhound", 13476, 1700, 7465}, // 90
				{"Rev Dragon", 13481, 1400, 14262}, // 135			
				{"Ahrim", 2025, 1800, 813}, // 98
				{"Karil", 2028, 1800, 808}, // 98
				{"Guthan", 2027, 1800, 813}, // 115
				{"Torag", 2029, 1800, 808}, // 115
				{"Dharok", 2026, 1800, 2065}, // 115
				
				
				{"Verac", 2030, 1800, 2061}, // 115
				{"Abyssal Demon", 1615, 2600, 1536}, // 124
				{"Rock Lobster", 2889, 1600, 2858}, // 127
				{"Desert Worm", 9465, 2800, 12790}, // 130
				{"Bronze Dragon", 1590, 2500, 14251}, // 131
				{"Tzhaar-Xil", 2605, 1200, 9281}, // 133
				{"Tok-Xil", 2631, 3300, 9241}, // 90
				{"Yt-MejKot", 2741, 4300, 9250}, // 180
				{"Ket-Zek", 2743, 2500, 9267}, //380
				{"Skeletal Wyvern", 3068, 2500, 1588}, // 140
				{"Kalphite Guard", 1155, 2900, 6219}, // 141
				{"Strongstack", 6261, 1800, 6153}, // 141
				{"Grimspike", 6265, 1800, 6153}, // 142
				{"Steelwill", 6263, 1800, 6153}, // 142
				{"Growler", 6250, 1200, 7014}, // 139
				{"Starlight", 6248, 1800, 6374}, // 149
				{"Bree", 6252, 2500, 4311}, // 146
				{"Kreeyath", 6208, 3400, 66}, // 151
				{"Karlak", 6204, 3400, 66}, // 145
				{"Gritch", 6206, 3400, 66}, // 142
				{"Geerin", 6225, 3300, 6949}, // 149
				
				
				{"Skree", 6223, 3300, 6949}, //
				{"Kilisa", 6227, 3300, 6949}, //
				{"Jadink Guard", 13821, 1900, 3165}, // 145
				{"Gorak", 6218, 2100, 4299}, // 149
				{"Red Dragon", 53, 2500, 12248}, // 152
				{"Frost Dragon", 51, 2500, 13156}, // 166
				{"Barrelchest", 5666, 2500, 5893}, // 170
				{"Black Demon", 84, 3200, 66}, // 172
				{"Dark Beast", 2783, 1800, 2730}, // 182
				{"Iron Dragon", 1591, 2500, 14251}, // 189
				{"Jadinko Male", 13822, 1900, 3165}, // 201
				{"Ice Strykewyrm", 9463, 2900, 12790}, // 210
				{"Brutal dragon", 5362, 2500, 12248}, // 227
				{"Giant Mole", 3340, 2500, 3309}, // 230
				{"Steel Dragon", 1592, 2500, 14251}, // 246
				{"Lava Dragon", 2011, 2100, 90}, // 252
				{"Demonic gorilla", 7151, 3000, Animation.getOsrsAnimId(7230)}, // 275
				{"Bandos Avatar", 4540, 2300, 11242}, // 125
				{"KBD", 50, 2300, 90}, // 276
				{"Chaos Ele", 3200, 2200, 5248}, //302
				{"Dag Supreme", 2881, 2000, 2850}, // 303
				{"Dag Prime", 2882, 2000, 2850}, // 303
				
				
				//button id changes
				{"Dag Rex", 2883, 2000, 2850}, // 303
				{"Mithril Dragon", 5363, 2500, 14251}, // 304
				{"Skeletal Horror", 9176, 3300, 12074}, // 320
				{"Torm Demon", 8349, 3400,  10921}, // 450
				{"Scorpia", 4172, 1700, 6252}, // 464
				{"Venenatis", 4173, 2450, 5326}, // 464
				{"Callisto", 4174, 2000, 4919}, // 470
				{"Glacor", 1382, 1700, 10867}, // 475
     			{"Avatar of Des", 8596, 5000, 11195}, // 525
				{"Avatar of Crea", 8597, 5200, 11200}, // 525
				{"Kree'arra", 6222, 5300, 6972}, // 580
				{"Zilyana", 6247, 2200, 6963}, // 650 
				{"Graardor", 6260, 4000, 7059}, // 624
				{"Kr'il Tsutsaroth", 6203, 4200, 14967}, // 650 
				{"TzTok-Jad", 2745, 4800, 9274}, // 702
				{"Zulrah", 2045, 1080, Animation.getOsrsAnimId(1721)}, // 725
				{"Corp Beast", 8133, 4500, 10056}, // 785
				{"Nex", 13447, 2300, 6320}, // 1001
				{"Gluttonous", 9948, 3050, 13712}, // 0
				{"Astea Frostweb", 9965, 1550, 808}, //0
				{"Icy Bones", 10040, 2000, 10814}, // 0
				{"Luminescent", 9912, 1500, 13333}, // 0
				{"Lakhrahnaz", 9929, 2500, 13767}, // 0
				{"To'Kash", 10024, 4000, 14381}, // 0
				{"Skinweaver", 10058, 1700, 13672}, // 0
				{"Geomancer", 10059, 1650, 12984}, // 0
				{"Bulwark beast", 10073, 3050, 12998}, // 0
				{"Cursebearer", 10111, 1800, 13167}, // 0
				{"Rammernaut", 9767, 1900, 13696}, // 0
				{"Har'Lakk", 9898, 4000, 14381}, // 0
				{"Lexicus", 9842, 1550, 13465}, // 0
				{"Sagittare", 9753, 1550, 808}, // 0
				
				{"Bal'lak", 10128, 4000, 14381}, // 0
				{"Runebound", 11752, 3000, 14420}, // 0
				{"Gravecreeper", 11708, 1700, 14499}, // 0
				{"Necrolord", 11737, 1550, 813}, // 0
				{"Haasghenahk", 11895, 3300, 14470}, // 0
				{"Yk'Lagor", 11872, 4000, 14381}, // 0
				{"Blink", 12865, 1500, 14948}, // 0
				
				{"Warped Gulega", 12737, 2700, 15015}, // 0
				{"Dreadnaut", 12848, 2300, 14977}, // 0
				{"Hope devourer", 12886, 3300, 14454}, // 0
				{"Shukarhazh", 12478, 4700, 14887}, // 0
				{"Kal'Ger", 12841, 4000, 14967}, // 0
				{"Vorkath", 8970, 5500, 27948}, // 0
				
				{"Terror dog", 11365, 1500, 5623}, // 0
				{"Kruk", 1441, 1000, 1386}, // 0
				{"Phoenix", 8548, 2000, 11092}, // 0
				{"Angry goblin", 3648, 1500, 6181}, // 0
				{"Harold", 14181, 1800, 9687}, // 0
				{"Ayuni", 13220, 2000, 15038}, // 0
				{"Chaos dwogre", 8771, 2000, 12148}, // 0
				{"Kebbit", 5137, 1300, 5302}, // 0
				{"A doubt", 5906, 1500, 6311}, // 0
				{"Ourg statue", 7086, 4000, 11407}, // 0
				{"Ele creature", 9432, 1300, 11935}, // 0
				{"Ice titan", 14257, 2100, 8186}, // 0
				{"The shaikahan", 1172, 1300, 311}, // 0
				{"Sharathteerk", 8165, 2100, 10428}, // 0
				{"Giant lobster", 7828, 1400, 6263}, // 0
				{"Gnoeals", 7392, 2000, 11488}, // 0
				{"TokTZ ket dill", 7771, 2400, 9334}, // 0
				{"Treus dayth", 1540, 1600, 5538}, // 0
				{"The untouchable", 5904, 1700, 6328}, // 0
				{"Nail beast", 1521, 2000, 5986}, // 0
				{"Mutant tarn", 5421, 3500, 5616}, // 0
				{"Ice demon", 9052, 2400, 2978}, // 0
				
				
				{"Black dragon", 54, 2500, 12248}, // 0
				{"Brutal blue dragon", 107273, 2500, 20090}, // 0
				{"Brutal red dragon", 107274, 2500, 20090}, // 0
				{"Brutal black dragon", 107275, 2500, 20090}, // 0
				{"Adamant dragon", 108030, 2500, 20090}, // 0
				{"Rune dragon", 108027, 2500, 20090}, // 0
				{"Cave horror", 4353, 2000, 4231}, // 0
				{"Albino bat", 4345, 1800, 4914}, // 0
				{"Living rock striker", 8833, 2400, 12202}, // 0
				{"Living rock protector", 8832, 2400, 12193}, // 0
				{"Taloned wyvern", 107793, 2400, 27650}, // 0
				{"Long-tailed wyvern", 107792, 2400, 27650}, // 0
				{"Spitting wyvern", 107794, 2400, 27650}, // 0
				{"Ancient wyvern", 107795, 2400, 27650}, // 0
				{"Lizardman shaman", 107745, 3500, 27191}, // 0
				{"Sand crab", 107206, 1700, 21310}, // 0
				{"King sand crab", 107266, 1300, 21310}, // 0
				
				{"Sulphur lizard", 108614, 2000, 28282}, // 0
				{"Wyrm", 108611, 3300, 28267}, // 0
				{"Drake", 108612, 2100, 28274}, // 0
				{"Hydra", 108609, 7500, 28260}, // 0
				{"Alchemical hydra", 108615, 6000, 28233}, // 0
				{"Basilisk knight", 109293, 2700, 28497}, // 0
				{"Mutated bloodveld", 7642, 1900, 9133}, // 0
				{"Greater nechryael", 107278, 2200, 21527}, // 0
				{"Shade", 105633, 1900, 21283}, // 0
				{"Deviant spectre", 107279, 3100, 21506}, // 0
				{"Twisted banshee", 107272, 2300, 21522}, // 0
				{"Ankou", 4381, 2000, 808}, // 0
				{"Warped jelly", 107277, 2200, 21584}, // 0
				{"Dust devil", 1624, 2100, 1556}, // 0
				{"Olmlet", 107519, 2400, 27396}, // 0
				{"Lil'Zik", 108336, 4500, 28120}, // 0
				{"Little nightmare", 109398, 2800, 28593}, // 0
				
				
				//	{"", , , }, //
				
			
				
		};
		sprite(702).at(14, 17);
		hover("Close Window", 300, 301, 3, 0).at(475, 30);
		button(1038, "Pet Info", 28647, 1, 68, 20).at(25, 30);
		button(1037, "Pets", 28646, 1, 68, 20).at(99, 30);
		text("Pet Info", 0, 0x695f28, true, true).at(55, 34);
		text("Pets", 0, 0xffb000, true, true).at(130, 34);
		text("", 2, 0xff981f, false, true).at(268, 30);
		sprite(988).at(330, 27);
		text("Pets 25/114", 0, 0xffb000, false, true).at(348, 31);
		hover("Spawn Pet", 2, 3, 1, 0).at(80, 277);
		hover("Pick-up pet", 2, 3, 1, 0).at(345, 277);
		text("Put-Down", 2, 0xffb000, false, true).at(85, 284);
		text("Pick-Up", 2, 0xffb000, false, true).at(355, 284);

		new ChildInterface(this) {
			@Override
			public void create() {
				int length = data.length;
				rsInterface.width = 430;
				rsInterface.height = 210;
				
				int lastY = 0;
				for (int i = 0; i < length; i++) {
					String name = (String) data[i][0];
					int npcId = (Integer) data[i][1];
					int zoom = (Integer) data[i][2];
					int animId = (Integer) data[i][3];
					int startX = (i % 4) * 110;
					int startY = (i / 4) * 100;
					
					sprite(506, 508, 8005, i + 1, 510, 86, 91, "Select").at(startX, startY);
					text(name, 84, 0, 0xffb000, true, true).at(startX, startY + 5);
					npcModel(npcId, 84, 89 * 2, zoom, 0, 0, animId, 1000 + i).at(startX, startY - 2);
					if (lastY < startY) {
						lastY = startY;
					}
				}

				rsInterface.scrollMax = lastY + 100;
			}
		}.at(30, 60);
		
		sprite(30).at(270, 267);
		sprite(30).at(80, 267);
		sprite(30).at(20, 267);
		hover("Revive pet", 2, 3, 1, 0).at(215, 277);
		text("Revive", 2, 0xffb000, false, true).at(228, 284);
	}

}
