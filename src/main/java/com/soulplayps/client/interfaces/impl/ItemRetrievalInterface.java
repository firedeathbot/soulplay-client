package com.soulplayps.client.interfaces.impl;

import static com.soulplayps.client.interfaces.RSInterface.addInterface;
import static com.soulplayps.client.interfaces.RSInterface.addInventory;
import static com.soulplayps.client.interfaces.RSInterface.addNewText;
import static com.soulplayps.client.interfaces.RSInterface.addRectangle;
import static com.soulplayps.client.interfaces.RSInterface.addSprite;

import com.soulplayps.client.Client;
import com.soulplayps.client.ConfigCodes;

import static com.soulplayps.client.interfaces.CustomInterfaces.addBorder;

import com.soulplayps.client.interfaces.RSInterface;

public class ItemRetrievalInterface {

	public static void init() {
		RSInterface inter = addInterface(54800);
		
		RSInterface background = addInterface(54801);
		background.width = 398;
		background.height = 300;
		addBorder(background, "Item Retrieval Service", true);

		RSInterface main = addInterface(54802);
		int positionWidth = background.width - 20;
		main.width = background.width - 20 - 16;
		main.height = background.height - 90;
		main.scrollMax = main.height + 1;

		addInventory(54803, 8, 10, 14, 8, false, "Take-All");
		//for (int i = 0; i < 80; i++) {
		//	RSInterface.interfaceCache[54803].inv[i] = 4152;
		//}

		addRectangle(54804, 0, 921100, false, main.width, main.height);
		addRectangle(54805, 0, 4671301, false, main.width - 2, main.height - 2);

		main.totalChildren(1);
		main.child(0, 54803, 3, 7);
		//main.scrollMax = 1000;

		RSInterface bottom = addInterface(54806);
		bottom.width = background.width - 20;
		bottom.height = 36;

		bottom.totalChildren(3);
		bottom.child(0, 54812, bottom.width - 74, (36 / 2) - (29 / 2));
		bottom.child(1, 54813, 0, (36 / 2) - (29 / 2));
		bottom.child(2, 54811, (bottom.width / 2) - ((bottom.width - (80 + 40 + 34)) / 2), 0);

		RSInterface unlockLayer = addInterface(54812);
		unlockLayer.width = 74;
		unlockLayer.height = 29;
		addSprite(54807, 584, -1, 74, 29, "Select");
		addNewText(54808, "Unlock", 74, 29, 1, 16750623, 0xffffff, true, 1, 1, 0);
		RSInterface.interfaceCache[54808].addOnVarpUpdate(ConfigCodes.ITEM_RETRIEVAL_UNLOCKED, new Runnable() {
			@Override
			public void run() {
				int value = Client.variousSettings[ConfigCodes.ITEM_RETRIEVAL_UNLOCKED];
				if (value == 1) {
					RSInterface.interfaceCache[54808].message = "Take-All";
				} else {
					RSInterface.interfaceCache[54808].message = "Unlock";
				}
			}
		});

		unlockLayer.totalChildren(2);
		unlockLayer.child(0, 54807, 0, 0);
		unlockLayer.child(1, 54808, 0, 0);
		unlockLayer.hidden = true;

		RSInterface discardLayer = addInterface(54813);
		discardLayer.width = 74;
		discardLayer.height = 29;
		addSprite(54809, 584, -1, 74, 29, "Select");
		addNewText(54810, "Discard", 74, 29, 1, 16750623, 0xffffff, true, 1, 1, 0);
		discardLayer.totalChildren(2);
		discardLayer.child(0, 54809, 0, 0);
		discardLayer.child(1, 54810, 0, 0);
		discardLayer.hidden = true;

		addNewText(54811, "", bottom.width - 120, bottom.height, 1, 16750623, 0, true, 1, 1, 15);

		inter.totalChildren(5);
		inter.child(0, 54801, (inter.width - background.width) / 2, (inter.height - background.height) / 2);
		inter.child(1, 54804, (inter.width - positionWidth) / 2, (inter.height - background.height) / 2 + 40);
		inter.child(2, 54805, (inter.width - positionWidth) / 2 + 1, (inter.height - background.height) / 2 + 41);
		inter.child(3, 54802, (inter.width - positionWidth) / 2, (inter.height - background.height) / 2 + 40);
		inter.child(4, 54806, (inter.width - bottom.width) / 2, (inter.height - background.height) / 2 + background.height - bottom.height - 10);
	}
	
}
