package com.soulplayps.client.interfaces.impl.powers;

import java.util.Comparator;

public enum PowerUpSortType {

	RARITY(0, "Rarity", new Comparator<PowerUpData>() {

		@Override
		public int compare(PowerUpData o1, PowerUpData o2) {
			int rarity = o2.getRarityType() - o1.getRarityType();
			if (rarity == 0) {
				int name = o2.getName().compareTo(o1.getName());
				return name == 0 ? rarity : name;
			}

			return rarity;
		}

	}),
	AVAILABLE(1, "Availability", new Comparator<PowerUpData>() {

		@Override
		public int compare(PowerUpData o1, PowerUpData o2) {
			int amount = o1.getAmount() - o2.getAmount();
			if (amount == 0) {
				int rarity = o2.getRarityType() - o1.getRarityType();
				if (rarity == 0) {
					int name = o2.getName().compareTo(o1.getName());
					return name == 0 ? rarity : name;
				}

				return rarity;
			}

			return amount;
		}

	}),
	AGE(2, "Release date", new Comparator<PowerUpData>() {

		@Override
		public int compare(PowerUpData o1, PowerUpData o2) {
			return o2.getId() - o1.getId();
		}

	});

	public static final PowerUpSortType[] values = values();
	private final int id;
	private final String name;
	private final Comparator<PowerUpData> comparator;

	PowerUpSortType(int id, String name, Comparator<PowerUpData> comparator) {
		this.id = id;
		this.name = name;
		this.comparator = comparator;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Comparator<PowerUpData> getComparator() {
		return comparator;
	}

	public static PowerUpSortType list(int id) {
		for (PowerUpSortType type : values) {
			if (type.id == id) {
				return type;
			}
		}

		return RARITY;
	}

}
