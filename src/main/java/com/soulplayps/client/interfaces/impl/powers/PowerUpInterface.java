package com.soulplayps.client.interfaces.impl.powers;

import static com.soulplayps.client.interfaces.RSInterface.CANT_CLICK_THROUGH;
import static com.soulplayps.client.interfaces.RSInterface.addButton;
import static com.soulplayps.client.interfaces.RSInterface.addInterface;
import static com.soulplayps.client.interfaces.RSInterface.addNewText;
import static com.soulplayps.client.interfaces.RSInterface.addRectangle;
import static com.soulplayps.client.interfaces.RSInterface.addSprite;

import java.util.Comparator;

import com.soulplayps.client.Client;
import com.soulplayps.client.ConfigCodes;
import com.soulplayps.client.interfaces.Dropdown;
import com.soulplayps.client.interfaces.InterfaceEvent;
import com.soulplayps.client.interfaces.MouseEvent;
import com.soulplayps.client.interfaces.RSInterface;
import com.soulplayps.client.interfaces.Tooltip;
import com.soulplayps.client.node.io.RSBuffer;

public class PowerUpInterface {

	private static int selectedPowerUp = -1;
	public static PowerUpSortType selectedSort;
	public static PowerUpData[] powers;
	public static String oldSearchInput;

	public static void init() {
		int width = 500;
		int height = 291;
		RSInterface inter = addInterface(72000);
		addSprite(72001, 1288);
		addNewText(72002, "Power Ups", width, 22, 2, 16750623, 0, true, 1, 1, 0);
		addRectangle(72003, 0, 0, false, width - 20, height - 32, 0);// 0x544E40
		addRectangle(72004, 0, 0, false, 150, 22, 0);
		addNewText(72005, "", 150, 22, 1, 16750623, 0, true, 1, 1, 0);
		addRectangle(72006, 0, 0, false, 150, 22, 0);
		addRectangle(72014, 0, 4671301, true, 148, 20, 0);
		addRectangle(72015, 0, 4671301, true, 148, 20, 0);
		RSInterface searchComponent = addNewText(72007, "Search...", 150, 22, 1, 16750623, 0, true, 1, 1, 0);
		searchComponent.atActionType = 1;
		searchComponent.tooltip = "Search";
		searchComponent.contentType = 1361;
		searchComponent.onButtonClick = new InterfaceEvent() {
			@Override
			public void run() {
				if (powers == null) {
					return;
				}

				Client.instance.setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, Client.POWER_UP_INPUT);
			}
		};

		searchComponent.onTimer = new InterfaceEvent() {
			@Override
			public void run() {
				if (Client.getInputDialogState() != Client.POWER_UP_INPUT || Client.loopCycle % 40 != 0 || powers == null) {
					return;
				}

				if (!Client.instance.amountOrNameInput.equals(oldSearchInput)) {
					oldSearchInput = Client.instance.amountOrNameInput;

					showAllPowers();
					for (int i = 0, length = powers.length; i < length; i++) {
						PowerUpData power = powers[i];
						if (power != null && (!power.getName().toLowerCase().contains(oldSearchInput) && !power.getDesc().toLowerCase().contains(oldSearchInput))) {
							power.setHidden(true);
						}
					}

					buildPowers();
				}
			}
		};

		RSInterface.addLayer(72010, width - 20 - 16 - 2, height - 34);
		RSInterface.addLayer(72011, width - 20 - 16 - 2, height - 34);
		RSInterface.addLayer(72012, 0, 0);
		addButton(72013, 300, 301, "Close");
		RSInterface.interfaceCache[72013].atActionType = 3;

		RSInterface.addLayer(72008, width, height);
		RSInterface.addLayer(72009, 150, 22);

		inter.totalChildren(15);
		inter.child(0, 72001, 9, 9);
		inter.child(1, 72002, 9, 9);
		inter.child(2, 72003, 19, 60);
		inter.child(3, 72004, 19, 35);
		inter.child(4, 72015, 20, 36);
		inter.child(5, 72005, 19, 35);
		inter.child(6, 72006, 185, 35);
		inter.child(7, 72014, 186, 36);
		inter.child(8, 72007, 185, 35);

		inter.child(9, 72010, 20, 61);
		inter.child(10, 72011, 20, 61);
		inter.child(11, 72012, 0, 0);

		inter.child(12, 72008, 0, 0);
		inter.child(13, 72009, 349, 35);
		inter.child(14, 72013, 487, 13);
	}

	public static void showAllPowers() {
		if (powers == null) {
			return;
		}

		for (int i = 0, length = powers.length; i < length; i++) {
			PowerUpData power = powers[i];
			if (power == null) {
				continue;
			}

			power.setHidden(false);
		}
	}

	public static void decodeFromServer(RSBuffer buffer) {
		selectedSort = PowerUpSortType.list(buffer.readUnsignedByte());
		buildSortButton();

		int length = buffer.readUnsignedByte();
		powers = new PowerUpData[length];
		for (int i = 0; i < length; i++) {
			powers[i] = new PowerUpData(buffer.readString(), buffer.readString(), buffer.readUnsignedByte(), buffer.readUnsignedByte(), buffer.readDWord(), buffer.readUnsignedWord(), i, buffer.readUnsignedByte());
		}

		updateSorted();
	}

	public static void buildSortButton() {
		RSInterface optionLayer = RSInterface.interfaceCache[72008];
		optionLayer.hidden = true;

		int length = PowerUpSortType.values.length;
		String[] ops = new String[length];
		for (int i = 0; i < length; i++) {
			ops[i] = PowerUpSortType.values[i].getName();
		}

		RSInterface menuLayer = RSInterface.interfaceCache[72009];
		Dropdown.makeDropdown(menuLayer, optionLayer, ops, 0, 2, selectedSort.getId());
	}

	public static void buildPowers() {
		final RSInterface optionLayer = RSInterface.interfaceCache[72011];
		optionLayer.hidden = true;

		final RSInterface content = RSInterface.interfaceCache[72010];
		content.dynamicComponents = null;
		content.onMouseWheel = new MouseEvent() {
			@Override
			public void run() {
				optionLayer.hidden = true;
				selectedPowerUp = -1;
			}
		};

		int length = powers.length;
		final int rows = (length / 3) + (length % 3 == 0 ? 0 : 1);
		int index = 0;
		int posIndex = 0;
		for (int i = 0; i < length; i++) {
			final PowerUpData power = powers[i];
			if (power.isHidden()) {
				continue;
			}

			final String name = power.getName();
			final String desc = power.getDesc();
			final int amount = power.getAmount();
			final int maxAmount = power.getMaxAmount();
			final boolean unlocked = amount > 0;
			final int rgb = power.getRgb();
			final int imageId = power.getImageId();
			final int currentI = i;
			int x = (posIndex % 3) * 151 + 10;
			int y = (posIndex / 3) * 40 + 10;
			final RSInterface border = RSInterface.addRectangle(x, y, 142, 32, rgb, 32, false);
			border.onMouseClick = new MouseEvent() {
				@Override
				public void run() {
					RSInterface.interfaceCache[72012].dynamicComponents = null;

					if (currentI == selectedPowerUp) {
						optionLayer.hidden = true;
						optionLayer.dynamicComponents = null;
						selectedPowerUp = -1;
					} else {
						selectedPowerUp = currentI;
						createDropdown(border, optionLayer, content, power.getId(), rows, currentI / 3);
					}
				}
			};

			Tooltip.build(desc, RSInterface.interfaceCache[72012], border, 25, 140);

			content.createDynamic(index++, border);
			content.createDynamic(index++, RSInterface.addRectangle(x + 1, y + 1, 140, 30, 0x1e1e1e, 30, true));
			content.createDynamic(index++, RSInterface.addSprite(x + 1, y + 1, 30, 30, imageId, -1, null));
			content.createDynamic(index++, RSInterface.addNewText(name, x + 1 + 30, y, 142 - 31, 28, 1, 0xe2b800, 0, true, 1, 1, 0));
			content.createDynamic(index++, RSInterface.addNewText(amount + "/" + maxAmount, x + 1 + 30, y, 142 - 34, 32, 0, 0xe2b800, 0, true, 2, 2, 0));
			RSInterface hideRect = RSInterface.addRectangle(x, y, 142, 32, 0x545454, 128, true);
			hideRect.hidden = unlocked;
			content.createDynamic(index++, hideRect);
			posIndex++;
		}

		int bottomY = rows * 40 + 11;
		if (bottomY > content.height) {
			content.scrollMax = bottomY;
		} else {
			content.scrollMax = content.height + 1;
		}
	}

	private static void createDropdown(final RSInterface menuLayer, final RSInterface optionLayer,
			final RSInterface parent, final int componentId, int rows, int currentRow) {
		final String[] options = { "Learn", "Forget" };
		int fontId = 0;
		int fontHeight = Client.instance.chooseFont(fontId).baseCharacterHeight + 4;
		int optionLayerHeight = options.length * fontHeight + 2;
		int width = 142;
		int optionLayerX = menuLayer.x;
		int optionLayerY;
		if (rows - 1 == currentRow) {
			optionLayerY = menuLayer.y - 1 - parent.scrollPosition - optionLayerHeight;
		} else {
			optionLayerY = menuLayer.y + menuLayer.height - 1 - parent.scrollPosition;
		}

		optionLayer.hidden = false;
		optionLayer.dynamicComponents = null;
		optionLayer.createDynamic(0,
				addRectangle(optionLayerX, optionLayerY, width, optionLayerHeight, 0x5E5E5E, 32, false));
		RSInterface background = addRectangle(optionLayerX + 1, optionLayerY + 1, width - 2, optionLayerHeight - 2,
				0x1e1e1e, 30, true);
		background.setFlag(CANT_CLICK_THROUGH);
		optionLayer.createDynamic(1, background);
		final RSInterface hoverRectangle = addRectangle(optionLayerX + 1, optionLayerY + 1, width - 2, fontHeight,
				0xffffff, 170, true);
		hoverRectangle.hidden = true;
		optionLayer.createDynamic(2, hoverRectangle);
		for (int i = 0; i < options.length; i++) {
			final int optionIndex = i;
			final RSInterface option = addNewText(options[i], optionLayerX + 1, optionLayerY + 1 + i * fontHeight,
					width, fontHeight, fontId, 16750623, 0, true, 1, 1, 0);
			option.onMouseOver = new MouseEvent() {
				@Override
				public void run() {
					hoverRectangle.hidden = false;
					hoverRectangle.y = option.y;
					option.disabledColor = 16758847;
				}
			};
			option.onMouseLeave = new MouseEvent() {
				@Override
				public void run() {
					hoverRectangle.hidden = true;
					option.disabledColor = 16750623;
				}
			};
			option.onMouseClick = new MouseEvent() {
				public void run() {
					optionLayer.hidden = true;
					hoverRectangle.hidden = true;
					optionLayer.dynamicComponents = null;
					selectedPowerUp = -1;
					Client.instance.sendOptionClick(parent.id, optionIndex, componentId);
				}
			};
			optionLayer.createDynamic(3 + i, option);
		}
	}

	public static void updateSorted() {
		quicksort(powers, selectedSort.getComparator());
		buildPowers();
	}

	private static void quicksort(PowerUpData[] objects, Comparator<PowerUpData> comparator) {
		quicksort(objects, 0, objects.length - 1, comparator);
	}

	private static void quicksort(PowerUpData[] values, int off, int len, Comparator<PowerUpData> comparator) {
		if (len > off) {
			int pivot = (off + len) / 2;
			PowerUpData oldValue = values[pivot];
			values[pivot] = values[len];
			values[len] = oldValue;
			int i = off;
			for (int j = off; j < len; j++) {
				if (comparator.compare(values[j], oldValue) > 0) {
					PowerUpData tmpVal = values[j];
					values[j] = values[i];
					values[i++] = tmpVal;
				}
			}
			values[len] = values[i];
			values[i] = oldValue;
			quicksort(values, off, i - 1, comparator);
			quicksort(values, i + 1, len, comparator);
		}
	}

}