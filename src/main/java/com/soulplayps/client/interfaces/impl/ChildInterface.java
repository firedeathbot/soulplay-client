package com.soulplayps.client.interfaces.impl;

import com.soulplayps.client.interfaces.Component;
import com.soulplayps.client.interfaces.CustomInterface;

public abstract class ChildInterface extends CustomInterface {

	private final CustomInterface parent;

	public ChildInterface(CustomInterface parent) {
		super(parent.nextChild++);
		this.parent = parent;
	}

	public void at(int x, int y) {
		init();
		parent.add(new Component(id, x, y));
		parent.nextChild = nextChild;
	}

}
