package com.soulplayps.client.interfaces.enums;

public enum DungRewardData {
	
	EXPERIENCE_LAMP(15389, "This option allows you to trade dungeoneering tokens for dungeoneering skill XP at a rate of 1:1.", 10_000),
	BONECRUSHER(18337, "This item requires a Prayer level of 21 to purchase. If you have a bonecrusher in your inventory, dropped bones from monster kills will automatically be converted into Prayer XP.", 34_000),
	HERBICIDE(19675, "You may choose for this item, if held in the inventory, to corrode and destroy herbs instead of being dropped. This provides you with 2x the usual cleaning XP in Herblore.", 34_000),
	CHARMING_IMP(9952, "As well as bein good company, a charming imp held in the inventory will automatically pick up dropped charms for you.", 100_000),
	SPIRIT_CAPE(19893, "This magical item, when worn, gives 17 strength bonus to each style, with the reduction to the cost of any scroll abilities that your combat familiars have.", 45_000),
	ARCANE_PULSE_NECK(18333, "This item can be worn by mages with a Magic level of 30 or higher and provides +13 magic bonus.", 6_500),
	TWISTED_BIRD_SKULL_NECK(19886, "This item can be worn by mages with a Magic level of 30 or higher and provides +13 magic bonus.", 8_500),
	LAW_STAFF(18342, "This item requires a Magic level of 45 and an Attack level of 40 to wield. You can store up to 1000 law runes in the staff. When casting spells that use law runes, there is a 1/10 chance that a law rune will not be used.", 10_000),
	NATURE_STAFF(18341, "This item requires a Magic level of 53 and an Attack level of 40 to wield. You can store up to 1000 nature runes in the staff. When casting spells that use nature runes, there is a 1/10 chance that a nature rune will not be used.", 12_500),
	GRAVITE_RAPIER(18365, "This item requires an Attack level of 55 to wield and will last forever and does not need recharging.", 40_000),
	GRAVITE_LONGSWORD(18367, "This item requires an Attack level of 55 to wield and will last forever and does not need recharging.", 40_000),
	GRAVITE_2H_SWORD(18369, "This item requires an Attack level of 55 to wield and will last forever and does not need recharging.", 40_000),
	GRAVITE_STAFF(18371, "This item requires a Magic level of 55 to wield and will last forever and does not need recharging.", 40_000),
	GRAVITE_SHORTBOW(18373, "This item requires a Ranged level of 55 to wield and will last forever and does not need recharging.", 40_000),
	TOME_OF_FROST(18346, "This item requires a Magic level of 48 to wield. This item acts as an infinite number of water runes and can be equipped in the offhand slot.", 43_000),
	AMULET_OF_ZEALOTS(19892, "This item, when wielded, improves the effect of any prayer that boosts only one skill. It comes with a -5 penalty to your prayer points.", 40_000),
	SCROLL_OF_CLEANSING(19890, "Reading this scroll premanently unlocks the wasteless herblore ability: when concocting a potion, there is a chance you will complete the poiton slightly quicker, and that you will save the secondary item completely.", 20_000),
	ARCANE_BLAST_NECK(18334, "This item can be worn by mages with a Magic level of 50 or higher and provides +24 magic bonus.", 15_000),
	SPLIT_DRAGONTOOTH_NECK(19887, "This item can be worn by mages with a Magic level of 50 or higher and provides +24 magic bonus.", 17_000),
	ANTI_POISON_TOTEM(18340, "This item requires 70 Herblore to wield. While wearing the totem, you will be immune to all poisons. (The totem does not remove any existing poison effects.).", 44_000),
	RING_OF_VIGOUR(19669, "If the ring of vigour is worn, after an ultimate ability is used, the user will coninue combat from 10% adrenaline instead of 0.", 44_000),
	SCROLL_OF_RENEWAL(18343, "Reading this scroll premanantly unlocks rapid renewal, a level 65 Prayer, This prayer, when activiated, will resore life points at 5 times the normal restore rate.", 38_000),
	MERCENARY_GLOVES(18347, "This item requires a Ranged level of 73 to wear. When worn, these gloves provide a +13 ranged attack bous.", 48_500),
	ARCANE_STEAM_NECK(18335, "This item can be worn by mages with a Magic level of 70 or higher and provides +14 magic attack and +15% magical damage.", 150_000),
	CHAOTIC_RAPIER(18349, "This item requires an Attack level of 80 to wield and will last forever and does not need recharging.", 200_000),
	CHAOTIC_LONGSWORD(18351, "This item requires an Attack level of 80 to wield and will last forever and does not need recharging.", 200_000),
	CHAOTIC_MAUL(18353, "This item requires an Attack level of 80 to wield and will last forever and does not need recharging.", 200_000),
	CHAOTIC_STAFF(18355, "This item requires a Magic level of 80 to wield and will last forever and does not need recharging.", 200_000),
	CHAOTIC_CROSSBOW(18357, "This item requires a Ranged level of 80 to wield and will last forever and does not need recharging.", 200_000),
	CHAOTIC_KITESHIELD(18359, "This item requires a Defence level of 80 to wield and will last forever and does not need recharging.", 200_000),
	EAGLE_EYE_KITESHIELD(18361, "This item requires a Defence level of 80 to wield and will last forever and does not need recharging.", 200_000),
	FARSEER_KITESHIELD(18363, "This item requires a Defence level of 80 to wield and will last forever and does not need recharging.", 200_000),
	DEMON_HORN_NECK(19888, "This item restores 5 prayer points for burying normal bones, 10 prayer points for burying big, baby dragon, wyvern bones, and 15 prayer points for burying dragon, our dagannoth and frost dragon bones (Requires 90 prayer).", 35_000),
	SNEAKERPEEPER_SPAWN(19894, "This is a pet stalker from Daemonheim.", 85_000),
	
	GLUTTONOUS_BEHEMOTH(9948, "This is a dungeoneering boss pet that will fight with you and follow you around.", 475_000, 6000, 13712),
	ASTEA_FROSTWEB(9965, "This is a dungeoneering boss pet that will fight with you and follow you around.", 475_000, 3500, 808),
	ICY_BONES(10040, "This is a dungeoneering boss pet that will fight with you and follow you around.", 500_000, 4500, 10814),
	LUMINESCENT(9912, "This is a dungeoneering boss pet that will fight with you and follow you around.", 500_000, 3500, 13333),
	LAKHRAHNAZ(9929, "This is a dungeoneering boss pet that will fight with you and follow you around.", 500_000, 5000, 13767),
	TO_KASH(10024, "This is a dungeoneering boss pet that will fight with you and follow you around.", 600_000, 9000, 14381),
	DIVING_SKINWEAVER(10058, "This is a dungeoneering boss pet that will fight with you and follow you around.", 600_000, 3500, 13672),
	GEOMANCER(10059, "This is a dungeoneering boss pet that will fight with you and follow you around.", 500_000, 3500, 12984),
	BULWARK_BEAST(10073, "This is a dungeoneering boss pet that will fight with you and follow you around.", 600_000, 6000, 12998),
	UNHOLY_CURSEBEARER(10111, "This is a dungeoneering boss pet that will fight with you and follow you around.", 600_000, 4000, 13167),
	RAMMERNAUT(9767, "This is a dungeoneering boss pet that will fight with you and follow you around.", 600_000, 4300, 13696),
	HAR_LAKK(9898, "This is a dungeoneering boss pet that will fight with you and follow you around.", 650_000, 9000, 14381),
	LEXICUS(9842, "This is a dungeoneering boss pet that will fight with you and follow you around.", 550_000, 3500, 13465),
	SAGITTARE(9753, "This is a dungeoneering boss pet that will fight with you and follow you around.", 550_000, 3500, 808),
	BAL_LAK(10128, "This is a dungeoneering boss pet that will fight with you and follow you around.", 700_000, 9000, 14381),
	RUNEBOUND(11752, "This is a dungeoneering boss pet that will fight with you and follow you around.", 700_000, 6000, 14420),
	GRAVECREEPER(11708, "This is a dungeoneering boss pet that will fight with you and follow you around.", 600_000, 3600, 14499),
	NECROLORD(11737, "This is a dungeoneering boss pet that will fight with you and follow you around.", 700_000, 3500, 813),
	HAASGHENAHK(11895, "This is a dungeoneering boss pet that will fight with you and follow you around.", 850_000, 6700, 14470),
	YK_LAGOR(11872, "This is a dungeoneering boss pet that will fight with you and follow you around.", 850_000, 9000, 14381),
	BLINK(12865, "This is a dungeoneering boss pet that will fight with you and follow you around.", 850_000, 3500, 14948),
	WARPED_GULEGA(12737, "This is a dungeoneering boss pet that will fight with you and follow you around.", 900_000, 6000, 15015),
	DREADNAUT(12848, "This is a dungeoneering boss pet that will fight with you and follow you around.", 900_000, 5300, 14977),
	HOPE_DEVOURER(12886, "This is a dungeoneering boss pet that will fight with you and follow you around.", 900_000, 6000, 14454),
	SHUKARHAZH(12478, "This is a dungeoneering boss pet that will fight with you and follow you around.", 1_000_000, 10500, 14887),
	KAL_GER(12841, "This is a dungeoneering boss pet that will fight with you and follow you around.", 1_000_000, 9000, 14967);
	
	

	public static final DungRewardData[] values = values();
	private final int productId;
	private final String description;
	private final int price;
	private final int zoom;
	private final int animId;
	private final boolean model;

	private DungRewardData(int productId, String description, int price) {
		this(productId, description, price, false, 0, 0);
	}

	private DungRewardData(int productId, String description, int price, int zoom, int animId) {
		this(productId, description, price, true, zoom, animId);
	}

	private DungRewardData(int productId, String description, int price, boolean model, int zoom, int animId) {
		this.productId = productId;
		this.description = description;
		this.price = price;
		this.model = model;
		this.zoom = zoom;
		this.animId = animId;
	}

	public int getProductId() {
		return productId;
	}

	public String getDescription() {
		return description;
	}

	public int getPrice() {
		return price;
	}

	public boolean isModel() {
		return model;
	}
	
	public int getZoom() {
		return zoom;
	}
	
	public int getAnimId() {
		return animId;
	}

}
