package com.soulplayps.client.interfaces.enums;

import java.util.HashMap;
import java.util.Map;

public enum ChatCrownType {

	IRONMAN(13, false, true),
	HC_IRONMAN(25, false, true),
	REGULAR(0, false, true),
	PK_BRONZE(15, false, true),
	PK_STEEL(16, false, true),
	PK_ADAMANT(17, false, true),
	PK_RUNITE(18, false, true),
	PK_DRAGON(19, false, true),
	RED_DONOR(4, false, true),
	BLUE_DONOR(5, false, true),
	GREEN_DONOR(6, false, true),
	PURPLE_DONOR(11, false, true),
	GOLD_DONOR(12, false, true),
	INSANE(23, false, true),
	TRUSTED(24, false, true),
	YOUTUBE_RANK(14, false, true),
	VETERAN(9, false, true),
	INFORMER(7, false, true),
	MOD_CROWN(1, true, true),
	ADMIN_CROWN(2, true, false),
	PRESTIGE_1(20, false, true),
	PRESTIGE_2(21, false, true),
	PRESTIGE_3(22, false, true);

	public static final Map<Integer, ChatCrownType> values = new HashMap<>();
	private final int id;
	private final boolean isStaff;
	private final boolean isIgnorable;

	private ChatCrownType(int id, boolean isStaff, boolean isIgnorable) {
		this.id = id;
		this.isStaff = isStaff;
		this.isIgnorable = isIgnorable;
	}

	public int getId() {
		return id;
	}

	public boolean isStaff() {
		return isStaff;
	}

	public boolean isIgnorable() {
		return isIgnorable;
	}

	public String toCrown() {
		if (id <= 0) {
			return "";
		}

		return "<img=" + (id - 1) + ">";
	}

	public static ChatCrownType get(int id) {
		ChatCrownType chatCrownType = values.get(id);
		if (chatCrownType == null) {
			return REGULAR;
		}

		return chatCrownType;
	}

	static {
		for (ChatCrownType value : values()) {
			values.put(value.getId(), value);
		}
	}

}
