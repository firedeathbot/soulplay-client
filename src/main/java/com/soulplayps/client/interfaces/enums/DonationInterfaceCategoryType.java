package com.soulplayps.client.interfaces.enums;

public enum DonationInterfaceCategoryType {

	FOUR_TO_FOUR(0, new int[][] {
		{1292, 1298, 1299}, {1292, 1298, 1299}, {1292, 1298, 1299}, {1292, 1298, 1299},
		{1292, 1298, 1299}, {1292, 1298, 1299}, {1292, 1298, 1299}, {1292, 1298, 1299}
	}, new int[][] {
		{0 % 4 * (83 + 4), 0 / 4 * (116 + 5)}, {1 % 4 * (83 + 4), 1 / 4 * (116 + 5)}, {2 % 4 * (83 + 4), 2 / 4 * (116 + 5)}, {3 % 4 * (83 + 4), 3 / 4 * (116 + 5)},
		{4 % 4 * (83 + 4), 4 / 4 * (116 + 5)}, {5 % 4 * (83 + 4), 5 / 4 * (116 + 5)}, {6 % 4 * (83 + 4), 6 / 4 * (116 + 5)}, {7 % 4 * (83 + 4), 7 / 4 * (116 + 5)}
	}, new int[][] {
		{83, 116}, {83, 116}, {83, 116}, {83, 116},
		{83, 116}, {83, 116}, {83, 116}, {83, 116}
	}),
	TWO_TO_TWO(1, new int[][] {
		{1303, 1303}, {1303, 1303},
		{1303, 1303}, {1303, 1303}
	}, new int[][] {
		{0 % 2 * (170 + 4), 0 / 2 * (116 + 5)}, {1 % 2 * (170 + 4), 1 / 2 * (116 + 5)},
		{2 % 2 * (170 + 4), 2 / 2 * (116 + 5)}, {3 % 2 * (170 + 4), 3 / 2 * (116 + 5)}
	}, new int[][] {
		{170, 116}, {170, 116},
		{170, 116}, {170, 116}
	}),
	ONE_TO_TWO(2, new int[][] {
		{1305, 1305},
		{1303, 1303}, {1303, 1303}
	}, new int[][] {
		{0, 0},
		{0, (116 + 5)}, {(170 + 4), (116 + 5)}
	}, new int[][] {
		{344, 116},
		{170, 116}, {170, 116}
	}),
	ONE_TO_ONE(3, new int[][] {
		{1302, 1302}, {1302, 1302}
	}, new int[][] {
		{0 % 2 * (170 + 4), 0 / 2 * (237 + 5)}, {1 % 2 * (170 + 4), 1 / 2 * (237 + 5)}
	}, new int[][] {
		{170, 237}, {170, 237},
	}),
	ONE(4, new int[][] {
		{1304, 1304}
	}, new int[][] {
		{0, 0}
	}, new int[][] {
		{344, 237}
	});

	private static DonationInterfaceCategoryType[] values = values();
	private final int id;
	private final int[][] tilePositions, tileSizes, tileSprites;

	DonationInterfaceCategoryType(int id, int tileSprites[][], int[][] tilePositions, int[][] tileSizes) {
		this.id = id;
		this.tileSprites = tileSprites;
		this.tilePositions = tilePositions;
		this.tileSizes = tileSizes;
	}

	public int getId() {
		return id;
	}

	public int[][] getTileSprites() {
		return tileSprites;
	}

	public int[][] getTilePositions() {
		return tilePositions;
	}

	public int[][] getTileSizes() {
		return tileSizes;
	}

	public static DonationInterfaceCategoryType list(int id) {
		for (DonationInterfaceCategoryType value : values) {
			if (value.id == id) {
				return value;
			}
		}

		return FOUR_TO_FOUR;
	}

}
