package com.soulplayps.client.interfaces.enums;	

public enum SpawnInfo {
	//RED_ICON("Pk Savage", 526),
	MELEE("Melee Setup", 1127),
	HYBRID("Hybrid Setup", 4091),
	ZERKER("Zerker Setup", 10828),
	PURE_NH("Pure NH Setup", 6109),
	PURE("Pure", 2930),
	BARRAGE_RUNES("Barrage Runes", 565),
	VENG_RUNES("Veng Runes", 9075),
	TB_RUNES("TB Runes", 563),
	SUPER_SET("Super Set", 2440),
	FOOD("Food", 15272),
    ;
	
	
	private String setName;
	private int spriteId;
	private SpawnInfo(String rankName, int spriteId) {
		this.setSpawnName(rankName);
		this.setSpriteId(spriteId);
	}
	public String getSetName() {
		return setName;
	}
	public void setSpawnName(String setName) {
		this.setName = setName;
	}
	public int getItemId() {
		return spriteId;
	}
	public void setSpriteId(int spriteId) {
		this.spriteId = spriteId;
	}
	
}
