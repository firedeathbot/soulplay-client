package com.soulplayps.client.interfaces.enums;

public enum VoteStoreData {

	RARE_DROP_BOOSTER(607, "Increases the chance of a rare item drop by 20%", 35),
	DOUBLE_EXPERIENCE(761, "Increases your experience by 100%", 30),
	PURE_PK_POINT_BOOST(608, "Increases your PK points when killing your opponent as a pure.", 25),
	MAIN_PK_POINT_BOOST(2403, "Increases your PK points when killing your opponent as a main.", 25),
	OVERLOAD(15332, "Potion that increases all your stats to 125.", 15),
	FROZEN_KEY(20120, "Frozen key to access godwars bosses without killcount.", 15),
	FIGHTER_TORSO(10551, "Very strong torso (untradeable)", 65),
	DRAGON_DEFENDER(20072, "An item that goes in your shield slot (untradeable)", 70),
	BARROWS_GLOVES(7462, "One of the strongest gloves in-game.", 20),
	FIRE_CAPE(6570, "The third best cape in-game.", 100),
	TOKHAAR_KAL(23639, "The second best cape in-game.", 1000),
	BONECRUSHER(18337, "If you have a bonecrusher in your inventory, dropped bones from monster kills will automatically be converted into Prayer XP.", 200),
	RUNE_POUCH(30047, "Used to store 3 type of runes in.", 300),
	PET_STAT_RESET_SCROLL(21821, "Resets all stats from one of your pets.", 50),
	PET_DOUBLE_EXPERIENCE(15360, "Grants your pet double experience for 1 hour.", 15),
	PET_BOOST(21403, "Increases your pets size and becomes stronger for 2 minutes.", 10),
	STRONG_TAMING_ROPE(19668, "Increases your chance of catching a monster as a pet by 50%", 40),
	BOUNTY_TELEPORT_SCROLL(30267, "Bounty hunter scroll, unlocks the ability to teleport to your target.", 400),
	SLAYER_HELM(13263, "Slayer helm increases your damage towards slayer task monsters.", 80),
	ABYSSAL_HEAD(7979, "Upgrades your slayer helm and changes its colors to red.", 100),
	KBD_HEADS(7980, "Upgrades your slayer helm and changes its color to black.", 100),
	KQ_HEAD(7981, "Upgrades your slayer helm and changes its color to green.", 100),
	VORKATH_HEAD(30329, "Upgrades your slayer helm and changes its color to blue.", 100),
	BLOOD_HOUND(30210, "A pet that follows you and picks-up all coins that it sees on the floor.", 1500),
	RING_OF_COINS(30192, "When equipping this ring you will turn into a gold stack.", 1000),
	RING_OF_NATURE(30193, "When equipping this ring you will turn into a bush.", 1000),
	VOID_TOP(8839, "Void knight top, one of the pieces for the void set that increases your damage.", 50),
	VOID_ROBE(8840, "Void knight robe, one of the pieces for the void set that increases your damage.", 50),
	VOID_GLOVES(8842, "Void knight gloves, one of the pieces for the void set that increases your damage.", 50),
	VOID_MAGE_HELM(11663, "Void mage helm, one of the pieces for the void set that increases your magic damage.", 75),
	VOID_RANGE_HELM(11664, "Void ranger helm, one of the pieces for the void set that increases your range damage.", 75),
	VOID_MELEE_HELM(11665, "Void melee helm, one of the pieces for the void set that increases your melee damage.", 75),
	ROYAL_SEED_POD(119564, "Royal seed pod can teleport from level 30 wilderness.", 200),
	IMBUED_GUTHIX_CAPE(123603, "A cape that boosts your magic offense and defence.", 350),
	IMBUED_ZAMORAK_CAPE(123605, "A cape that boosts your magic offense and defence.", 350),
	IMBUED_SARADOMIN_CAPE(123607, "A cape that boosts your magic offense and defence.", 350),
	
	
	TERROR_DOG(11365, "Unique and rare pet", 1000, 2600, 5623),
	ANGRY_GOBLIN(3648, "Unique and rare pet", 1500, 2500, 6181),
	CHAOS_DWOGRE(8771, "Unique and rare pet", 2500, 4300, 12148),
	PHOENIX(8548, "Unique and rare pet", 5000, 4200, 11092),
	NAIL_BEAST(1521, "Unique and rare pet", 5000, 2700, 5986),
	;

	public static final VoteStoreData[] values = values();
	private final int productId;
	private final String description;
	private final int price;
	private final int zoom;
	private final int animId;
	private final boolean model;

	private VoteStoreData(int productId, String description, int price) {
		this(productId, description, price, false, 0, 0);
	}

	private VoteStoreData(int productId, String description, int price, int zoom, int animId) {
		this(productId, description, price, true, zoom, animId);
	}

	private VoteStoreData(int productId, String description, int price, boolean model, int zoom, int animId) {
		this.productId = productId;
		this.description = description;
		this.price = price;
		this.model = model;
		this.zoom = zoom;
		this.animId = animId;
	}

	public int getProductId() {
		return productId;
	}

	public String getDescription() {
		return description;
	}

	public int getPrice() {
		return price;
	}

	public boolean isModel() {
		return model;
	}

	public int getZoom() {
		return zoom;
	}

	public int getAnimId() {
		return animId;
	}

}
