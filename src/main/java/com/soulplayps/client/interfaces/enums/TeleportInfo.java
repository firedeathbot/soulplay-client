package com.soulplayps.client.interfaces.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TeleportInfo {
	
	EDGEVILLE(0, "Edgeville", "All levels", "Safe", "Edgeville home of soulplay"),
	GRAND_EXCHANGE(0, "Grand Exchange", "All levels", "Safe", "Grand exchange soulplay market"),
	LUMBRIDGE(0, "Lumbridge", "All levels", "Safe", "lumbridge"),
	VARROCK(0, "Varrock", "All levels", "Safe", "varrock"),
	CAMELOT(0, "Camelot", "All levels", "Safe", "camelot"),
	FALADOR(0, "Falador", "All levels", "Safe", "falador"),
	ARDOUGNE(0, "Ardougne", "All levels", "Safe", "ardougne"),
	DRAYNOR(0, "Draynor", "All levels", "Safe", "draynor"),
	NEITIZNOT(0, "Neitiznot", "All levels", "Safe", "neitiznot"),
	SHILO_VILLAGE(0, "Shilo Village", "All levels", "Safe", "shilo village"),
	KARAMJA(0, "Karamja", "All levels", "Safe", "karamja"),
	YANILLE(0, "Yanille", "All levels", "Safe", "yanille"),
	LUNAR_ISLE(0, "Lunar Isle", "All levels", "Safe", "lunar isle"),
	TAI_BWO_WANNAI(0, "Tai Bwo Wannai", "All levels", "Safe", "Tai Bwo Wannai, jiminua"),
	KELDAGRIM(0, "Keldagrim", "All levels", "Safe", "keldagrim city"),
	APE_ATOLL(0, "Ape Atoll", "All levels", "Safe", "Ape Atoll, monkeys, apes"),
    LAND_OF_SERENITY(0, "Land of Serenity", "All levels", "Safe", "Land of Serenity, donate, donor, world boss"),
	
	
	ROCK_CRAB(1, "Rock crabs", "13", "Safe", "rock crabs start training here"),
	YAKS(1, "Yaks", "22", "Safe", "yaks good for training"),
	EXPERIMENT(1, "Experiments", "25-51", "Safe", "experiements good for training"),
	SLAYER_TOWER(1, "Slayer Tower", "8-124", "Safe", "slayer tower, crawling hand, banshee, infernal mage, bloodveld, abyssal demon, gargoyle"),
	
	TAVERLY_DUNG(2, "Taverly Dungeon", "13-227", "Safe", "Baby black dragon, Baby blue dragon, Black demon, Black dragon, Black knight, Blue dragon, Chaos druid, Chaos dwarf, Hellhound, Hill giant, Giant bat, Lesser demon, Poison spider, Skeleton and Ghost, taverly dungeon"),
	BRIMHAVEN_DUNG(2, "Brimhaven Dungeon", "42-141", "Safe", "Monsters: Wild dog, Moss giant, Greater demon, Fire giant, Baby red dragon, Red dragon, Black demon, Bronze dragon, Iron dragon and Steel dragon, brimhaven dungeon"),
	FREMENNIK_DUNG(2, "Fremennik Dungeon", "10-70", "Safe", "Monsters: Cave crawlers, Rockslugs, Cockatrices, Pyrefiends, Basilisks, Jellies, Turoths and Kurasks, fremennik dungeon"),
	LUMBRIDGE_DUNG(2, "Lumbridge Dungeon", "3-99", "Safe", "Monsters: Cave goblins, Cave bug, Cave crawler, Cave slime, Big frog, Rockslug, Wall beast and Giant Frog, lumbridge dungeon"),
	EDGEVILLE_DUNG(2, "Edgeville Dungeon", "3-172", "Safe", "Monsters: Giant rats, Giant spiders, Zombies, Skeletons, Hobgolbin, Hill giants, thugs, Chaos druids, Deadly red spiders, Earth warriors, Poison spiders, Chronozon and Black demons, edgeville dungeon"),
	VARROCK_DUNG(2, "Varrock Dungeon", "3-42", "Safe", "Monsters: Giant rats, Giant spiders, Zombies, Scorpions, Ghosts, Skeletons, Grizzly bear, Deadly Red Spiders and Moss giants, varrock dungeon"),
	KALPHITE_LAIR(2, "Kalphite Lair", "28-141", "Safe", "Monsters: Kalphite workers, Kalphite soldiers, Kalphite guardians, kalphite queen."),
	ASGARNIA_DUNG(2, "Ice Dungeon", "6-140", "Safe", "Monsters: Frost dragons, Ice giants, Ice warriors, Pirates, Hobgoblins, Muggers, asgarnia dungeon"),
	DWARVEN_MINES(2, "Dwarven Mines", "1-70", "Safe", "Monsters: hand cannoneers, dwarven mines"),
	GLACOR_LAIR(2, "Glacor Lair", "475", "Safe", "Monsters: glacors lair"),
	FORBIDDEN_DUNG(2, "Forbidden Dungeon", "140", "Safe", "Monsters: skeletal wyverns, mutated jadinko guard, mutated jadinko male, forbidden dungeon"),
	JUNGLE_DUNG(2, "Jungle Dungeon", "3-42", "Safe", "giant mosquitos, jungle horrors, jungle spiders, giant wasps, jungle strykewyrms, jungle savages."),
	ANCIENT_CAVERN(2, "Ancient Cavern", "115-304", "Safe", "mithril dragons, green brutal dragons, ancient cavern"),
	CHAOS_TUNNEL(2, "Chaos Tunnel", "115-304", "Safe", "placeholder, chaos tunnel"),
	SMOKE_DUNG(2, "Smoke Dungeon", "12-137", "Safe", "Monsters: Fire elementals, Pyrefiend, Fire giants, Dust devils, smoke dungeon"),
	WATERBIRTH(2, "Waterbirth Dungeon", "90-137", "Safe", "Monsters: Dagannoths, Wallasalkis, giant rock crabs and Rock lobsters, Waterbirth Dungeon"),
	REV_CAVE(2, "Revenant Dungeon", "120-135", "Wildy (39)", "revenants, revs"),
	CATACOMB_OF_KOURAND(2, "Catacombs of Kourend", "19-200", "Safe", "catacomb, dragon, ghost, lesser demon, black demon, abyssal demon, bronze dragon, iron dragon, steel dragon, hill giant, moss giant, magic axe, fire giant, cyclops, dust devil, warped jelly, king sand crab, sand crab, jelly, dagannoth, brutal black drags, brutal red drags, fiyre shade, deveant spectre, Twisted Banshee, ankou, Greater Nechryael, hellhound, Mutated Bloodveld"),
	NIEVES_CAVE(2, "Stronghold Slayer Cave", "19-100", "Safe", "nieve, Stronghold Slayer Cave, ankou, bloodveld, aberrant spectre, hellhound, fire giant"),
	LITHKREN_VAULT(2, "Lithkren Vault", "338-380", "Safe", "lithkren vault, rune dragon, addy dragon, adamant dragon, dragon"),
	JORMUNGANDS_PRISON(2, "Jormungand's Prison", "204", "Safe", "jormungands prison, basilisk, dagannoth, basilisk knight"),
	KARUULM_SLAYER_DUNGEON(2, "Karuulm Slayer Dungeon", "70 - 426", "Safe", "karuulm slayer dungeon, wyrms, hydras, drakes, sulphur lizards, greater demons, hellhounds, fire giants"),
	CRABCRAWL_CAVES(2, "Crabcrawl Caves", "15 - 107", "Safe", "sand crabs"),
	LIZARDMAN_CAVES(2, "Lizardman Caves", "150", "Safe", "lizardman shaman"),
	WYVERN_CAVE(2, "Wyvern Cave", "200", "Safe", "wyvern cave, ancient wyvern, long tailed wyvern, spitting wyvern, taloned wyver"),
	LIVING_ROCK_CAVERNS(2, "Living Rock Caverns", "120-200", "Safe", "living rock striker, living rock protector, living rock patriarch, living rock caverns, living minerals"),
	SMOKE_DEVIL_DUNGEON(2, "Smoke Devil Dungeon", "160-301", "Safe", "smoke devil, thermonuclear smoke devil"),
	KALPHITE_CAVE(2, "Kalphite Cave", "28-141", "Safe", "kalphite cave, kalphite worker, kalphite soldier, kalphite guardian"),
	CAVE_OF_HORRORS(2, "Cave Of Horrors", "80", "Safe", "Mos Le'Harmless Cave, cave horrors, cave of horrors"),
	
	
	BOSS_INSTANCE(3, "Instanced bosses", "170-1001", "Safe", "vorkath, boss, zulrah, kree, arnadyl, bandos, zamorak, saradomin, general graardor, kr'il, zilyana, kraken, corporeal beast, dagannoth king, nex, bandos avatar, nomad, giant mole, barrelchest, kbd, chaos elemental, skele horror, scorpia, callisto, venenatis, tormented demon, crazy archeologist, Chaos Fanatic"),
	VORKATH(3, "Vorkath", "750", "Safe", "vorkath boss"),
	ZULRAH(3, "Zulrah", "725", "Safe", "Zulrah boss"),
	ARMADYL(3, "Kree'arra", "580", "Safe", "armadyl godwars boss"),
	BANDOS(3, "General Graardor", "624", "Safe", "bandos godwars boss"),
	ZAMORAK(3, "K'ril Tsutsaroth", "650", "Safe", "zamorak godwars boss"),
	SARADOMIN(3, "Commander Zilyana", "596", "Safe", "saradomin godwars boss"),
	KRAKEN(3, "Kraken", "291", "Safe", "kraken, trident"),
	CORPOREAL_BEAST(3, "Corporeal Beast", "785", "Safe", "corporeal beast, corp"),
	DAGG_KINGS(3, "Dagannoth Kings", "303", "Safe", "dagannoth kings, dagg"),
	NEX(3, "Nex", "1001", "Safe", "nex, torva"),
	BANDOS_AVATAR(3, "Bandos Avatar", "125", "Safe", "bandos avatar"),
	NOMAD(3, "Nomad", "699", "Safe", "nomad"),
	GIANT_MOLE(3, "Giant Mole", "230", "Safe", "giant mole"),
	BARRELCHEST(3, "Barrelchest", "170", "Safe", "barrelchest"),
	KBD(3, "KBD", "276", "Wildy (41)", "king black dragon, kbd"),
	CHAOS_ELEMENTAL(3, "Chaos Elemental", "305", "Wildy (52)", "chaos elemental"),
	SKELE_HORROR(3, "Skeletal Horror", "320", "Wildy (22)", "skeletal horror"),
	SCORPIA(3, "Scorpia", "464", "Wildy (59)", "scorpia"),
	CALLISTO(3, "Callisto", "470", "Wildy (45)", "callisto"),
	VENENANTIS(3, "Venenatis", "464", "Wildy (30)", "venenatis"),
	TORMTENTED_DEMON(3, "Tormented Demons", "450", "Safe", "tormented demon, tds, dragon claws"),
	CHAOS_FANATIC(3, "Chaos Fanatic", "202", "Wildy (40)", "chaos fanatic"),
	CRAZY_ARCHEOLOGIST(3, "Crazy Archeologist", "204", "wildy (22)", "crazy archeologist"),
	DERANGED_ARCHEOLOGIST(3, "Deranged Archeologist", "276", "Safe", "deranged archeologist"),
	ALCHEMICAL_HYDRA(3, "Alchemical Hydra", "426", "Safe", "alchemical hydra, karuulm dungeon"),
	NIGHTMARE_OF_ASHIHAMA(3, "Nightmare of Ashihama", "814", "Safe", "nightmare of ashihama boss"),
	VETION(3, "Vet'ion", "454", "Wildy (40)", "vetion, boss"),
	KALPHITE_QUEEN(3, "Kalphite Queen", "333", "Safe", "Kalphite queen, boss"),
	LIZARD_SHAMAN(3, "Lizard Shamans", "150", "Safe", "lizard shaman, boss"),
	
	
	GAMBLE(4, "Gambling", "All levels", "Safe", "gamble, flower poker, dice"),
	LAST_MAN_STANDING(4, "Last Man Standing", "All levels", "Safe", "last man standing, lms, staking, stake, gamble"),
	DUEL_ARENA(4, "Duel Arena", "All levels", "Safe", "duel arena, stake, staking, gamble, gambling"),
	BARROWS(4, "Barrows", "All levels", "Safe", "dharocks, veracs, ahrims, guthans, karils, barrows, minigame"),
	TZHAAR(4, "Tzhaar", "All levels", "Safe", "tzhaar, firecape, minigame"),
	WARRIORS_GUILD(4, "Warrior's guild", "All levels", "Safe", "warriors guild, minigame"),
	CASTLE_WARS(4, "Castle Wars", "All levels", "Safe", "castle wars, minigame, cw"),
	PEST_CONTROL(4, "Pest Control", "All levels", "Safe", "Pest Control, minigame"),
	SOULWARS(4, "Soulwars", "All levels", "Safe", "soulwars, sw, minigame"),
	WILDERNESS_TREASURE(4, "Wilderness Treasure", "All levels", "Safe", "wilderness treasure, minigame"),
	FISHING_CONTEST(4, "Fishing Contest", "All levels", "Safe", "fishing contest, minigame"),
	BARBARIAN_ASSAULT(4, "Barbarian Assault", "All levels", "Safe", "barbarian assault, minigame"),
	FIGHT_PITS(4, "Fight Pits", "All levels", "Safe", "fight pits, minigame"),
	FUNPK(4, "FunPK", "All levels", "Safe", "funpk "),
	TAI_BWO_WANNAI_CLEANUP(4, "Bwo Wannai Cleanup", "All levels", "Safe", "tai bwo wannai cleanup, village, tribal, karamja"),
	PENGUIN_HUNT(4, "Penguin Hunt", "All levels", "Safe", "Penguin hunt, minigame"),
	CHAMBER_OF_XERIC(4, "Chamber Of Xeric (raids 1)", "All levels", "Safe", "chamber of xeric, raids 1, great olm"),
    INFERNO(4, "Inferno", "All levels", "Safe", "inferno"),
    THEATRE_OF_BLOOD(4, "Theatre of Blood (raids 2)", "All levels", "Safe", "theatre of blood, raids 2, verzik, tob"),
	
	
	
	EDGE_WILDY(5, "Edgeville Wilderness", "All levels", "Safe", "wildy, edgeville wilderness, edge wildy"),
	MAGE_BANK(5, "Mage Bank", "All levels", "Safe", "mage bank"),
	CLAN_WARS(5, "Clan Wars", "All levels", "Safe", "clan wars"),
	WEST_DRAG(5, "West Dragons", "79", "Wildy (14)", "wests dragons wildy, wilderness"),
	HILL_GIANTS(5, "Hill Giants", "28", "Wildy (17)", "hill giants wildy, wilderness"),
	DEMONIC_GORILLA(5, "Demonic Gorillas", "275", "Safe", "demonic gorillas, gorilla"),
	LAVA_DRAG(5, "Lava Dragons", "252", "Wildy (47)", "lava dragons wildy, wilderness"),
	ELDER_CHAOS_DRUIDS(5, "Elder Chaos druids", "129", "Wildy (18)", "elder chaos druids, chaos temple"),
	
	
	TURAEL(6, "Turael", "1", "Safe", "turael slayer master"),
	KRYSTILIA(6, "Krystilia", "1", "Safe", "Krystilia slayer master"),
	MAZCHNA(6, "Mazchna", "20", "Safe", "mazchna slayer master"),
	VANNAKA(6, "Vannaka", "40", "Safe", "vannaka slayer master"),
	CAHELDAR(6, "Chaeldar", "75", "Safe", "caheldar slayer master"),
	KONAR_QUO_MATEN(6, "Konar quo Maten", "75", "Safe", "Konar quo Maten slayer master"),
	NIEVE(6, "Nieve", "85", "Safe", "Nieve slayer master"),
	SUMMMONA(6, "Sumona", "95", "Safe", "summona slayer master"),
	DURADEL(6, "Duradel", "100", "Safe", "duradel slayer master"),
	KURADAL(6, "Kuradal", "110", "Safe", "kuradal slayer master"),
	MORVARN(6, "Morvran", "120", "Safe", "morvran slayer master"),
	
	CATH_FISHING(7, "Catherby", "All levels", "Safe", "fishing"),
	FISHING_GUILD(7, "Fishing Guild", "60", "Safe", "fishing guild"),
	KARAMBWANS(7, "Karambwans", "65", "Safe", "karambwans fishing"),
	RESOURCE_AREA_FISHING(7, "Resource Area Fishing", "76", "Wildy (58)", "fishing, shark, rocktail, manta ray, resource area"),
	PORT_PISCARILIUS_FISHING(7, "Port Piscarilius", "82", "safe", "fishing, anglerfish, port piscarilius"),
	
	
	FALADOR_MINING(8, "Dwarven Mine", "All levels", "Safe", "mining, dwarven mine, gold, mithril, coal, tin, copper, iron"),
	NEITIZNOT_MINING(8, "Neitiznot Mine", "All levels", "Safe", "mining, mine, mithril, adamant, coal, iron"),
	CRAFTING_GUILD_MINING(8, "Crafting Guild Mine", "All levels", "Safe", "mining, mine, clay, tin, gold"),
	LIMESTONE_MINE(8, "Limestone Mine", "All levels", "Safe", "mining, mine, limestone"),
	GEM_MINE(8, "Gem Mine", "All levels", "Safe", "mining, mine, gems"),
	DORGESHUUN_MINE(8, "Dorgeshuun Mine", "All levels", "Safe", "mining, dorgesh kaan mine, dorgeshuun, bone"),
	
	DRAYNOR_WC(9, "Draynor", "All levels", "Safe", "woodcutting"),
	CAMELOT_WC(9, "Camelot", "All levels", "Safe", "woodcutting"),
	MAHOGNAY_WC(9, "Tai Bwo Wannai (WC)", "35", "Safe", "woodcutting, mahogany, teak"),
	RESOUCE_AREA_WC(9, "Resource Area (WC)", "60", "Wildy (58)", "woodcutting, ivy, resource area"),
	IVY_WC(9, "Ivy Vines", "68", "Safe", "woodcutting, ivy"),
	WOODCUTTING_GUILD(9, "Woodcutting Guild", "60-90", "Safe", "woodcutting guild, yew tree, willow tree, magic tree, redwood"),
	
	
	GNOME_AGILITY(10, "Gnome Stronghold", "All levels", "Safe", "gnome agility course"),
	BARB_AGILITY(10, "Barbarian Outpost", "35", "Safe", "barbarian agility, barb agility course"),
	WILDY_AGILITY(10, "Wilderness", "52", "Wildy (53)", "wildy agility, wilderness agility course"),
	
	
	EDGEVILLE_THIEV(11, "Edgeville Stalls", "All levels", "Safe", "thieving stalls,thieving"),
	VARROCK_THIEV(11, "Varrock Stalls", "All levels", "Safe", "thieving stalls,thieving"),
	ARDOUGNE_THIEV(11, "Ardougne", "All levels", "Safe", "thieving stalls,thieving"),
	
	IMPLINGS(12, "Implings", "All levels", "Safe", "hunter, implings"),
	GRAY_CHINS(12, "Gray Chinchompas", "53", "Safe", "hunter, gray chinchompa, box trap"),
	RED_CHINS(12, "Red Chinchompas", "63", "Safe", "hunter, red chinchompa, box trap"),
	BLACK_CHINS(12, "Black Chinchompas", "73", "Wildy (33)", "hunter, black chinchompa, box trap"),
	
	CATHERBY_FARM(13, "Catherby Farm", "All levels", "Safe", "farming, farm"),
	FALADOR_FARM(13, "Falador Farm", "All levels", "Safe", "farming, farm"),
	FARMING_CANIFIS(13, "Canifis Farm", "All levels", "Safe", "farming, farm"),
	FARMING_ARDOUGNE(13, "Ardougne Farm", "All levels", "Safe", "farming, farm"),
	VARROCK_TREE(13, "Varrock Tree Farm", "All levels", "Safe", "tree farm, farming"),
	LUMBRIDGE_TREE(13, "Lumbridge Tree Farm", "All levels", "Safe", "tree farm, farming"),
	FALADOR_TREE(13, "Falador Tree Farm", "All levels", "Safe", "tree farm, farming"),
	TAVERLY_TREE(13, "Taverly Tree Farm", "All levels", "Safe", "tree farm, farming"),
	
	EDGE_RUNECRAFT(14, "Edgeville Zamorak", "All levels", "Safe", "runecrafting"),
	
	SUMMONING_OBELISK(15, "Summoning Obelisk", "All levels", "Safe", "summoning, obelisk"),
	
	DAEMONHEIM(16, "Daemonheim", "All levels", "Safe", "dungeoneering, daemonheim"),
	
	CONSTRUCTION(17, "Edgeville Portal", "All levels", "Safe", "construction");
	
	
	public int type;
	public String name;
	public String level;
	public String danger;
	public String[] searchKeywords;

	private TeleportInfo(int type, String name, String level, String danger, String keywords) {
		this.type = type;
		this.name = name;
		this.level = level;
		this.danger = danger;
		
		try {
			String[] keywordsArray = keywords.split(",");
			int length = keywordsArray.length;

			this.searchKeywords = new String[length];

			for(int i = 0; i < length; i++) {
				this.searchKeywords[i] = keywordsArray[i].toLowerCase();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static Map<Integer, List<TeleportInfo>> values = new HashMap<Integer, List<TeleportInfo>>();
	
	static {
		TeleportInfo[] valuez = values();
		for (TeleportInfo info : valuez) {
			List<TeleportInfo> list = values.get(info.type);
			if (list == null) {
				list = new ArrayList<TeleportInfo>();
				values.put(info.type, list);
			}
			
			list.add(info);
		}
	}
}
