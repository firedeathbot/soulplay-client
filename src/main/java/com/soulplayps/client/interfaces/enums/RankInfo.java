package com.soulplayps.client.interfaces.enums;

public enum RankInfo {
	//RED_ICON("Pk Savage", 526),
	NO_ICON("None Rank", 458),
	PK_ICON_1("Killer Rank", 468),
	PK_ICON_2("Crusader Rank", 470),
	PK_ICON_3("Psychic Rank", 472),
	PK_ICON_4("Destroyer Rank", 474),
	PK_ICON_5("Leader Rank", 476),
	DONATOR_ICON("Donator Rank", 3),
	SUPER_ICON("Super Rank", 4),
	EXTREME_RANK("Supreme Rank", 5),
	LEGENDARY_ICON("Legendary Rank", 10),
	UBER_ICON("Uber Rank", 11),
	INSANE("Insane Rank", 1285),
	TRUSTED("Trusted Rank", 1284),
	YOUTUBER_ICON("Youtube Rank", 13),
	VET_ICON("Veteran Rank", 8),
	HELPER_ICON("Helper Rank", 6),
	MOD_ICON("Moderator Rank", 0),
	AMIN_ICON("Admin Rank", 1),
	PRESTIGE_1("Lord", 1247),
	PRESTIGE_2("Legend", 1248),
	PRESTIGE_3("Extreme", 1249)
    ;
	
	public static final RankInfo[] values = values();
	private String rankName;
	private int spriteId;
	private RankInfo(String rankName, int spriteId) {
		this.setRankName(rankName);
		this.setSpriteId(spriteId);
	}
	public String getRankName() {
		return rankName;
	}
	public void setRankName(String rankName) {
		this.rankName = rankName;
	}
	public int getSpriteId() {
		return spriteId;
	}
	public void setSpriteId(int spriteId) {
		this.spriteId = spriteId;
	}
	
}
