package com.soulplayps.client.interfaces;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class InterfaceWriter {
	
	public static void writeInterfaces() throws IOException {
		  DataOutputStream data = new DataOutputStream(new FileOutputStream("interfaces"));
          data.writeShort(RSInterface.interfaceCache.length);
          for (int i = 0; i < RSInterface.interfaceCache.length; i++) {
                  RSInterface inte = RSInterface.interfaceCache[i];
                  if (inte.parentID != -1) {
                          data.writeShort(65535);
                          data.writeShort(inte.parentID);
                          data.writeShort(i);
                  } else {
                          data.writeShort(i);
                  }
                  data.writeByte(inte.interfaceType);
                  data.writeByte(inte.atActionType);
                  data.writeShort(inte.contentType);
                  data.writeShort(inte.width);
                  data.writeShort(inte.height);
                  data.writeByte(inte.transparency);
                  if (inte.hoverType != -1) {
                          writeShort2(data, inte.hoverType);
                  } else {
                          data.writeByte(0);
                  }
                 
                  if(inte.valueCompareType != null) {
                          data.writeByte(inte.valueCompareType.length);
                          if (inte.valueCompareType.length > 0) {
                                  for (int ii = 0; ii < inte.valueCompareType.length; ii++) {
                                          data.writeByte(inte.valueCompareType[ii]);
                                          data.writeShort(inte.requiredValues[ii]);
                                  }
                          }
                  } else {
                          data.writeByte(0);
                  }
                  if(inte.valueIndexArray != null) {
                          data.writeByte(inte.valueIndexArray.length);
                          if (inte.valueIndexArray.length > 0) {
                                  for (int ii = 0; ii < inte.valueIndexArray.length; ii++) {
                                          data.writeShort(inte.valueIndexArray[ii].length);
                                          for (int iii = 0; iii < inte.valueIndexArray[ii].length; iii++) {
                                                  data.writeShort(inte.valueIndexArray[ii][iii]);
                                          }
                                  }
                          }
                  } else {
                          data.writeByte(0);
                  }
                  if (inte.interfaceType == 0) {
                          data.writeShort(inte.scrollMax);
                          data.writeBoolean(inte.isMouseoverTriggered);
                          int childAmount = 0;
                          if (inte.children != null) {
                                  childAmount = inte.children.length;
                          }
                          data.writeShort(childAmount);
                          for (int ii = 0; ii < childAmount; ii++) {
                                  data.writeShort(inte.children[ii]);
                                  data.writeShort(inte.childX[ii]);
                                  data.writeShort(inte.childY[ii]);
                          }
                  }
                  if (inte.interfaceType == 1) {
                          data.writeShort(0/*inte.anInt211*/); // anInt211 // anInt211 is nothing? o.O
                          data.writeBoolean(inte.aBoolean251);
                  }
                  if (inte.interfaceType == 2) {
                          data.writeBoolean(inte.aBoolean259);
                          data.writeBoolean(inte.isInventoryInterface);
                          data.writeBoolean(inte.usableItemInterface);
                          data.writeBoolean(inte.deletesTargetSlot);
                          data.writeByte(inte.invSpritePadX);
                          data.writeByte(inte.invSpritePadY);
                          for (int ii = 0; ii < 20; ii++) {
                                  boolean type = false;
                                  if (inte.sprites != null) {
                                          type = inte.sprites[ii] != null;
                                  }
                                  data.writeBoolean(type);
                                  if (type) {
                                          data.writeShort(inte.spritesX[ii]);
                                          data.writeShort(inte.spritesY[ii]);
                                          writeString(data, inte.sprites[ii].interfaceString);
                                  }
                          }
                          for (int ii = 0; ii < RSInterface.RIGHT_CLICK_ACTIONS_SIZE; ii++) { // used to be 5, but i added more to interfaces like Prev X amount
                                  if (inte.actions[ii] == null) {
                                          writeString(data, "");
                                  } else {
                                          writeString(data, inte.actions[ii]);
                                  }
                          }
                  }
                  if (inte.interfaceType == 3) {
                          data.writeBoolean(inte.filled);
                  }
                  if (inte.interfaceType == 4 || inte.interfaceType == 1) {
                          data.writeBoolean(inte.centerText);
                          data.writeByte(inte.textDrawingAreas);
                          data.writeBoolean(inte.textShadow);
                  }
                  if (inte.interfaceType == 4) {
                          writeString(data, inte.message);
                          writeString(data, inte.enabledMessage);
                  }
                  if (inte.interfaceType == 1 || inte.interfaceType == 3 || inte.interfaceType == 4) {
                          writeHex(data, inte.disabledColor);
                  }
                  if (inte.interfaceType == 3 || inte.interfaceType == 4) {
                          writeHex(data, inte.enabledColor);
                          writeHex(data, inte.mouseHoverTextRecolor);
                          writeHex(data, inte.anInt239);
                  }
                  if (inte.interfaceType == 5) {
                          String s;
                          if(inte.disabledSprite != null) { //TODO: add loading of sprites for sprites.dat and add hoveredDisabled/enabled sprites
                                  s = inte.disabledSprite.interfaceString;
                                  if (s == null || s.startsWith("null")) {
                                          writeString(data, "");
                                  } else {
                                          writeString(data, s);
                                  }
                          } else {
                                  writeString(data, "");
                          }
                          if(inte.enabledSprite != null) {
                                  s = inte.enabledSprite.interfaceString;
                                  if (s == null || s.startsWith("null")) {
                                          writeString(data, "");
                                  } else {
                                          writeString(data, s);
                                  }
                          } else {
                                  writeString(data, "");
                          }
                  }
                  if (inte.interfaceType == 6) {
                          if (inte.disabledMediaID != -1) {
                                  writeShort2(data, inte.disabledMediaID);
                          } else {
                                  data.writeByte(0);
                          }
                          if (inte.enabledMediaID != -1) {
                                  writeShort2(data, inte.enabledMediaID);
                          } else {
                                  data.writeByte(0);
                          }
                          if (inte.disabledAnimation != -1) {
                                  writeShort2(data, inte.disabledAnimation);
                          } else {
                                  data.writeByte(0);
                          }
                          if (inte.enabledAnimation != -1) {
                                  writeShort2(data, inte.enabledAnimation);
                          } else {
                                  data.writeByte(0);
                          }
                          data.writeShort(inte.modelZoom);
                          data.writeShort(inte.modelZoom);
                          data.writeShort(inte.modelRotationX);
                  }
                  if (inte.interfaceType == 7) {
                          data.writeBoolean(inte.centerText);
                          data.writeByte(inte.textDrawingAreas);
                          data.writeBoolean(inte.textShadow);
                          writeHex(data, inte.disabledColor);
                          data.writeShort(inte.invSpritePadX);
                          data.writeShort(inte.invSpritePadY);
                          data.writeBoolean(inte.isInventoryInterface);
                          for (int ii = 0; ii < 5; ii++) {
                                  if (inte.actions[ii] == null) {
                                          writeString(data, "");
                                  } else {
                                          writeString(data, inte.actions[ii]);
                                  }
                          }
                  }
                  if (inte.atActionType == 2 || inte.interfaceType == 2) {
                          writeString(data, inte.targetVerb);
                          writeString(data, inte.targetName);
                          data.writeShort(inte.targetMask.mask);
                  }
                  if (inte.atActionType == 1 || inte.atActionType == 4 || inte.atActionType == 5 || inte.atActionType == 6) {
                          if (inte.tooltip.equals("Ok") || inte.tooltip.equals("Select") || inte.tooltip.equals("Continue")) {
                                  writeString(data, "");
                          } else {
                                  writeString(data, inte.tooltip);
                          }
                  }
          }
          data.close();
  }

  private static void writeShort2(DataOutputStream dos, int input) throws IOException {
          dos.writeByte((input >> 8) + 1);
          dos.writeByte(input);
  }

  private static void writeHex(DataOutputStream dos, int input) throws IOException {
          dos.writeByte(input >> 24);
          dos.writeByte(input >> 16);
          dos.writeByte(input >> 8);
          dos.writeByte(input);
  }

  private static void writeString(DataOutputStream dos, String input) throws IOException {
          dos.write(input.getBytes());
          dos.writeByte(10);
  }

}
