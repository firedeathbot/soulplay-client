package com.soulplayps.client.interfaces;

public class ComponentBuilder {
	
	private final InterfaceBuilder interfaceBuilder;
	private final int childID;
	
	public ComponentBuilder(InterfaceBuilder interfaceBuilder, int childID) {
		this.interfaceBuilder = interfaceBuilder;
		this.childID = childID;
	}
	
	public InterfaceBuilder getInterfaceBuilder() {
		return interfaceBuilder;
	}
	
	public Component at(int x, int y) {
		Component component = new Component(childID, x, y);
		interfaceBuilder.add(component);
		return component;
	}

}
