package com.soulplayps.client.interfaces;

import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.util.Strings;

import java.util.HashSet;

import com.soulplayps.client.Client;
import com.soulplayps.client.node.ObjectCache;
import com.soulplayps.client.node.io.RSBuffer;
import com.soulplayps.client.fs.RSArchive;

public final class InterfaceDecoder {
	
	private InterfaceDecoder() {
		
	}
	
	public static void decode(RSArchive interfaceArchive, RSArchive mediaArchive, byte[] mediaIndex) {
		RSInterface.aMRUNodes_238 = new ObjectCache(500);
		RSBuffer stream = new RSBuffer(interfaceArchive.readFile("data"));
		int i = -1;
		int j = stream.readUnsignedWord();
		RSInterface.interfaceCache = new RSInterface[j + 100_000];
		while (stream.currentOffset < stream.buffer.length) {
			int k = stream.readUnsignedWord();
			if (k == 65535) {
				i = stream.readUnsignedWord();
				k = stream.readUnsignedWord();
			}
			RSInterface rsInterface = RSInterface.interfaceCache[k] = new RSInterface();
//			RSInterface.checkTaken(k);
			rsInterface.id = k;
			rsInterface.parentID = i;
			rsInterface.interfaceType = stream.readUnsignedByte();
			rsInterface.atActionType = stream.readUnsignedByte();
			rsInterface.contentType = stream.readUnsignedWord();
			rsInterface.width = stream.readUnsignedWord();
			rsInterface.height = stream.readUnsignedWord();
			rsInterface.transparency = stream.readUnsignedByte();
			rsInterface.hoverType = stream.readUnsignedByte();
			if (rsInterface.hoverType != 0)
				rsInterface.hoverType = (rsInterface.hoverType - 1 << 8) + stream.readUnsignedByte();
			else
				rsInterface.hoverType = -1;
			int i1 = stream.readUnsignedByte();
			if (i1 > 0) {
				rsInterface.valueCompareType = new byte[i1];
				rsInterface.requiredValues = new int[i1];
				for (int j1 = 0; j1 < i1; j1++) {
					rsInterface.valueCompareType[j1] = (byte) stream.readUnsignedByte();
					rsInterface.requiredValues[j1] = stream.readUnsignedWord();
				}

			}
			int k1 = stream.readUnsignedByte();
			if (k1 > 0) {
				rsInterface.valueIndexArray = new int[k1][];
				for (int l1 = 0; l1 < k1; l1++) {
					int i3 = stream.readUnsignedWord();
					rsInterface.valueIndexArray[l1] = new int[i3];
					for (int l4 = 0; l4 < i3; l4++)
						rsInterface.valueIndexArray[l1][l4] = stream.readUnsignedWord();

				}

			}
			if (rsInterface.interfaceType == 0) {
				rsInterface.scrollMax = stream.readUnsignedWord();
				rsInterface.isMouseoverTriggered = stream.readUnsignedByte() == 1;
				int i2 = stream.readUnsignedWord();
				rsInterface.children = new int[i2];
				rsInterface.childX = new short[i2];
				rsInterface.childY = new short[i2];
				for (int j3 = 0; j3 < i2; j3++) {
					rsInterface.children[j3] = stream.readUnsignedWord();
					rsInterface.childX[j3] = (short) stream.readSignedWord();
					rsInterface.childY[j3] = (short) stream.readSignedWord();
				}
			}
			if (rsInterface.interfaceType == 1) {
				stream.readUnsignedWord();
				stream.readUnsignedByte();
			}
			if (rsInterface.interfaceType == 2) {
				rsInterface.inv = new int[rsInterface.width * rsInterface.height];
				rsInterface.invStackSizes = new int[rsInterface.width * rsInterface.height];
				rsInterface.aBoolean259 = stream.readUnsignedByte() == 1;
				rsInterface.isInventoryInterface = stream.readUnsignedByte() == 1;
				rsInterface.usableItemInterface = stream.readUnsignedByte() == 1;
				rsInterface.deletesTargetSlot = stream.readUnsignedByte() == 1;
				rsInterface.invSpritePadX = stream.readUnsignedByte();
				rsInterface.invSpritePadY = stream.readUnsignedByte();
				rsInterface.spritesX = new short[20];
				rsInterface.spritesY = new short[20];
				rsInterface.sprites = new Sprite[20];
				for (int j2 = 0; j2 < 20; j2++) {
					int k3 = stream.readUnsignedByte();
					if (k3 == 1) {
						rsInterface.spritesX[j2] = (short) stream.readSignedWord();
						rsInterface.spritesY[j2] = (short) stream.readSignedWord();
						String s1 = stream.readString();
						if (mediaArchive != null && s1.length() > 0) {
							int i5 = s1.lastIndexOf(",");
							rsInterface.sprites[j2] = RSInterface.getSprite(Integer.parseInt(s1.substring(i5 + 1)), mediaArchive,
									s1.substring(0, i5), mediaIndex);
						}
					}
				}
				rsInterface.actions = new String[RSInterface.RIGHT_CLICK_ACTIONS_SIZE];
				for (int l3 = 0; l3 < 5; l3++) { //Needs change here if we write the interfaces to cache
					rsInterface.actions[l3] = stream.readString();
					if (rsInterface.actions[l3].length() == 0) {
						rsInterface.actions[l3] = null;
					} else {
						rsInterface.actions[l3] = Strings.handleOldSyntax(rsInterface.actions[l3]);
					}
				}
				
				switch (rsInterface.parentID) {
				case 5292:
					rsInterface.actions[5] = "Withdraw " + Client.prevXAmount;
					rsInterface.actions[6] = "PlaceHolder";
					break;
				case 5063:
					rsInterface.actions[5] = "Bank " + Client.prevXAmount;
					break;
				case 3824:
					rsInterface.actions[4] = "Buy X";
					rsInterface.actions[5] = "Buy " + Client.prevXAmount;
					break;
				case 3822:
					rsInterface.actions[4] = "Sell X";
					rsInterface.actions[5] = "Sell " + Client.prevXAmount;
					break;
				case 1644:
					rsInterface.actions[2] = "Operate";
					break;
				}
			}
			if (rsInterface.interfaceType == 3)
				rsInterface.filled = stream.readUnsignedByte() == 1;
			if (rsInterface.interfaceType == 4 || rsInterface.interfaceType == 1) {
				rsInterface.centerText = stream.readUnsignedByte() == 1;
				rsInterface.textDrawingAreas = stream.readUnsignedByte();
				rsInterface.textShadow = stream.readUnsignedByte() == 1;
			}
			if (rsInterface.interfaceType == 4) {
				rsInterface.message = stream.readString();
				rsInterface.message = Strings.handleOldSyntax(rsInterface.message);
				if (RSInterface.showIds)
					rsInterface.message = Integer.toString(rsInterface.id);
				rsInterface.enabledMessage = stream.readString();
				rsInterface.enabledMessage = Strings.handleOldSyntax(rsInterface.enabledMessage);
			}
			if (rsInterface.interfaceType == 1 || rsInterface.interfaceType == 3 || rsInterface.interfaceType == 4)
				rsInterface.disabledColor = stream.readDWord();
			if (rsInterface.interfaceType == 3 || rsInterface.interfaceType == 4) {
				rsInterface.enabledColor = stream.readDWord();
				rsInterface.mouseHoverTextRecolor = stream.readDWord();
				rsInterface.anInt239 = stream.readDWord();
			}
			if (rsInterface.interfaceType == 5) {
				String s = stream.readString();
				if (mediaArchive != null && s.length() > 0) {
					int i4 = s.lastIndexOf(",");
					rsInterface.disabledSprite = RSInterface.getSprite(Integer.parseInt(s.substring(i4 + 1)), mediaArchive,
							s.substring(0, i4), mediaIndex);
				}
				s = stream.readString();
				if (mediaArchive != null && s.length() > 0) {
					int j4 = s.lastIndexOf(",");
					rsInterface.enabledSprite = RSInterface.getSprite(Integer.parseInt(s.substring(j4 + 1)), mediaArchive,
							s.substring(0, j4), mediaIndex);
				}
			}
			if (rsInterface.interfaceType == 6) {
				int l = stream.readUnsignedByte();
				if (l != 0) {
					rsInterface.disabledMediaType = 1;
					rsInterface.disabledMediaID = (l - 1 << 8) + stream.readUnsignedByte();
				}
				l = stream.readUnsignedByte();
				if (l != 0) {
					rsInterface.enabledMediaType = 1;
					rsInterface.enabledMediaID = (l - 1 << 8) + stream.readUnsignedByte();
				}
				l = stream.readUnsignedByte();
				if (l != 0)
					rsInterface.disabledAnimation = (l - 1 << 8) + stream.readUnsignedByte();
				else
					rsInterface.disabledAnimation = -1;
				l = stream.readUnsignedByte();
				if (l != 0)
					rsInterface.enabledAnimation = (l - 1 << 8) + stream.readUnsignedByte();
				else
					rsInterface.enabledAnimation = -1;
				rsInterface.modelZoom = stream.readUnsignedWord();
				rsInterface.modelRotationY = stream.readUnsignedWord();
				rsInterface.modelRotationX = stream.readUnsignedWord();
			}
			if (rsInterface.interfaceType == 7) {
				rsInterface.inv = new int[rsInterface.width * rsInterface.height];
				rsInterface.invStackSizes = new int[rsInterface.width * rsInterface.height];
				rsInterface.centerText = stream.readUnsignedByte() == 1;
				rsInterface.textDrawingAreas = stream.readUnsignedByte();
				rsInterface.textShadow = stream.readUnsignedByte() == 1;
				rsInterface.disabledColor = stream.readDWord();
				rsInterface.invSpritePadX = stream.readSignedWord();
				rsInterface.invSpritePadY = stream.readSignedWord();
				rsInterface.isInventoryInterface = stream.readUnsignedByte() == 1;
				rsInterface.actions = new String[5];
				for (int k4 = 0; k4 < 5; k4++) {
					rsInterface.actions[k4] = stream.readString();
					if (rsInterface.actions[k4].length() == 0)
						rsInterface.actions[k4] = null;
				}

			}
			if (rsInterface.atActionType == 2 || rsInterface.interfaceType == 2) {
				rsInterface.targetVerb = stream.readString();
				rsInterface.targetName = stream.readString();
				rsInterface.targetMask = new InterfaceTargetMask(stream.readUnsignedWord());
			}

			if (rsInterface.interfaceType == 8) {
				rsInterface.message = stream.readString();
			}

			if (rsInterface.atActionType == 1 || rsInterface.atActionType == 4 || rsInterface.atActionType == 5
					|| rsInterface.atActionType == 6) {
				rsInterface.tooltip = stream.readString();
				if (rsInterface.tooltip.length() == 0) {
					if (rsInterface.atActionType == 1)
						rsInterface.tooltip = "Ok";
					if (rsInterface.atActionType == 4)
						rsInterface.tooltip = "Select";
					if (rsInterface.atActionType == 5)
						rsInterface.tooltip = "Select";
					if (rsInterface.atActionType == 6)
						rsInterface.tooltip = "Continue";
				} else {
					rsInterface.tooltip = Strings.handleOldSyntax(rsInterface.tooltip);
				}
			}
		}

		RSInterface.aClass44 = mediaArchive;
		RSInterface.mediaIndex = mediaIndex;

		loadInterfaces(false);
	}

	public static void loadInterfaces(boolean initCache) {
		if (initCache) {
			RSInterface.aMRUNodes_238 = new ObjectCache(1000);
		}

		RSInterface.list = new HashSet<Integer>();
		CustomInterfaces.loadInterfaces();
		RSInterface.aMRUNodes_238 = null;
		RSInterface.list.clear();
	}
}
