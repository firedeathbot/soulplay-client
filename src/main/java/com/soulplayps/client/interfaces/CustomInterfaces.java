package com.soulplayps.client.interfaces;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.ConfigCodes;
import com.soulplayps.client.interfaces.enums.DungRewardData;
import com.soulplayps.client.interfaces.enums.RankInfo;
import com.soulplayps.client.interfaces.enums.SpawnInfo;
import com.soulplayps.client.interfaces.enums.TeleportInfo;
import com.soulplayps.client.interfaces.enums.VoteStoreData;
import com.soulplayps.client.interfaces.impl.GambleInterfaces;
import com.soulplayps.client.interfaces.impl.InstanceInterface;
import com.soulplayps.client.interfaces.impl.ItemRetrievalInterface;
import com.soulplayps.client.interfaces.impl.NightmareOverlay;
import com.soulplayps.client.interfaces.impl.PetCollection;
import com.soulplayps.client.interfaces.impl.PetInfo;
import com.soulplayps.client.interfaces.impl.PresetsInterface;
import com.soulplayps.client.interfaces.impl.powers.PowerUpInterface;
import com.soulplayps.client.interfaces.impl.powers.PowerUpRoll;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.summoning.Creation.CreationData;
import com.soulplayps.client.util.Misc;
import com.soulplayps.client.util.Skills;
import static com.soulplayps.client.interfaces.RSInterface.*;

public class CustomInterfaces {

	public static volatile boolean autoRefresh = false;
	public static final Set<CustomInterface> customInterfaces = new HashSet<CustomInterface>();

	public static void refresh() {
		for (CustomInterface ci : customInterfaces) {
			ci.init();
		}
	}

	public static void add(CustomInterface customInterface) {
		customInterface.init();
		customInterfaces.add(customInterface);
	}

	public static void customInterfaces() {
		add(new PetInfo());
		add(new PetCollection());
	}

	public static void loadInterfaces() {
		emoteTab();
		prayerTab();
		statisticTab();
		questTab();
		achievement();
		equipmentScreen();
		friendsTab();
		ignoreTab();
		skillTab602();
		logout();
		equipmentTab();
		titles();
		settings();
		soulplayToggles();
		settingsTab();
		barrowText();
		//magicTab();
		//ancientMagicTab();
		summonTab();
		bank();
		clanChatTab();
		clanChatSetup();
		itemsOnDeath();
		godWars();
		Pestpanel();
		Pestpanel2();
		Sidebar0();
		Curses();
		market();
		testScroll();
		runePouch();
		pouchesLesik();
		scrollsProgress();
		capeColor();
		quickPrayers();
		quickCurses();
		BOB();
		playerIcons();
		spawnTab();

		roomChooser();
		constructionWaiting();
		constructionFurnitureChooser();

		clanGame();
		clanGameWait();

		barbarianAssault();
		barbarianAssaultLobby();

		soulWarsGameInterface();
		soulWarsExpReward();
		soulWarsCharmsReward();
		soulWarsOtherReward();

		marketOfferUpInterface();
		buyOffersFromAll();
		viewMyOwnBuyOffers();

		compCustColor();
		compCust();

		teleportInterface();

		dungeoneeringTab();
		dungeoneeringInvite();
		dungeoneeringRewards();
		dungeoneeringbindings();
		dungeoneeringFloors();
		dungeoneeringComplexity();
		dungeoneeringMap();
		dungSmelting();
		dungRewardShop();
		dungSpellBook();
		dungScrollingShop();
		dungeoneeringProgress();

		houseOptions();

		displayName();
		votingPoll();
		hotkeys();

		customInterfaces();
		lunarSpellBook();
		normalSpellBook();
		ancientSpellBook();
		popularHouses();
		
		cleanupEdit();
		LMSInfo();
		taskTab();
		taskInterface();
		blackList();
		addSmithingOption();
		playerInfoTab();
		scrollingShop();
		overlay();
		coxPartySiderbar();
		coxHud();
		coxRewards();
		hpBarHud();
		randomInterfaces();
		seasonPass();
		voteInterface();
		donorInterface();
		toolBelt();
		InstanceInterface.init();
		PresetsInterface.init();
		NightmareOverlay.init();
		GambleInterfaces.init();
		GambleInterfaces.initConfirm();
		GambleInterfaces.initGambleInventory();
		PowerUpInterface.init();
		PowerUpRoll.init();
		clanInterface();
		ItemRetrievalInterface.init();
		tobRewards();

//		 Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new
//		 Runnable() {
//		 @Override
//		 public void run() {
//		 if (autoRefresh)
//		 equipmentScreen();//CustomInterfaces.refresh();
//		 }
//		
//		 }, 600, 600, TimeUnit.MILLISECONDS);

	}

	private static void randomInterfaces() {
		RSInterface.interfaceCache[8631].disabledMediaID = 2541;
		RSInterface.interfaceCache[8631].enabledMediaID = 2541;
		RSInterface.interfaceCache[8631].recolorOriginal = new int[] { 43059 };
		RSInterface.interfaceCache[8631].recolorModified = new int[] { 8596 };

		//Glass blowing
		RSInterface.interfaceCache[11463].setFlag(RECT_IGNORE_ATLAS);
		RSInterface.interfaceCache[11463].transparency = 255;
		RSInterface.interfaceCache[15349].disabledColor = 16777215;
	}

	private static void tobRewards() {
		RSInterface inter = addInterface(54900);

		RSInterface background = addInterface(54901);
		background.width = 240;
		background.height = 180;
		addBorder(background, "Theatre of Blood", true);

		addModel(54902, 125, 130, 826, 512, 700, 1070, 1, -1, 0);
		RSInterface.interfaceCache[54902].setFlag(OSRS_MODEL);

		addModel(54903, 125, 130, 451, 512, 600, 1048, 1, -1, 0);
		RSInterface.interfaceCache[54903].setFlag(OSRS_MODEL);
		
		addModel(54904, 130, 130, 267, 135, 1550, 35414, 1, -1, 0);
		RSInterface.interfaceCache[54904].setFlag(OSRS_MODEL);

		addInventory(54905, 2, 2, 14, 11, false, "Take");
		//for (int i = 0; i < 4; i++) {
		//	RSInterface.interfaceCache[54905].inv[i] = 4152;
		//}
		
		inter.totalChildren(5);
		inter.child(0, 54901, (inter.width - 240) / 2, (inter.height - 180) / 2);
		inter.child(1, 54902, (inter.width - 240) / 2, ((inter.height - 180) / 2 - 130 / 2) + 105);
		inter.child(2, 54903, (inter.width - 240) / 2 + 25, ((inter.height - 180) / 2 - 130 / 2) + 105 - 22);
		inter.child(3, 54904, (inter.width - 240) / 2 + 97, (inter.height - 180) / 2 + 80);
		inter.child(4, 54905, (inter.width - 240) / 2 + 15, (inter.height - 180) / 2 + 40);
	}

	public static void clanInterface() {
		int width = 500;
		int height = 299;
		int layerWidth = 486;
		int layerHeight = 271 - 5;
		RSInterface inter = addInterface(71000);
		addSprite(71001, 1288);
		addNewText(71002, "Clan - name", width, 22, 2, 0xe2b800, 0, true, 1, 1, 0);
		addSprite(71003, 1038, 1037, ConfigCodes.CLAN_CATEGORIES, 0, -1, 79, 20, "Clan", false);
		RSInterface.interfaceCache[71003].atActionType = 5;
		addSprite(71004, 1038, 1037, ConfigCodes.CLAN_CATEGORIES, 1, -1, 79, 20, "Perks", false);
		RSInterface.interfaceCache[71004].atActionType = 5;
		addSprite(71005, 1038, 1037, ConfigCodes.CLAN_CATEGORIES, 2, -1, 79, 20, "Settings", false);
		RSInterface.interfaceCache[71005].atActionType = 5;
		addSprite(71006, 1038, 1037, ConfigCodes.CLAN_CATEGORIES, 3, -1, 79, 20, "Info", false);
		RSInterface.interfaceCache[71006].atActionType = 5;
		addNewText(71007, "Clan", 65, 20, 1, 0xe2b800, 0xffd416, true, 1, 1, 0);
		addNewText(71008, "Perks", 65, 20, 1, 0xe2b800, 0xffd416, true, 1, 1, 0);
		addNewText(71009, "Settings", 65, 20, 1, 0xe2b800, 0xffd416, true, 1, 1, 0);
		addNewText(71010, "Info", 65, 20, 1, 0xe2b800, 0xffd416, true, 1, 1, 0);
	
		
		inter.totalChildren(11);
		inter.child(0, 71001, 6, 6);
		inter.child(1, 71002, 6, 6);
		inter.child(2, 71003, 20, height);
		inter.child(3, 71004, 99, height);
		inter.child(4, 71005, 178, height);
		inter.child(5, 71006, 257, height);
		inter.child(6, 71007, 20, height);
		inter.child(7, 71008, 99, height);
		inter.child(8, 71009, 178, height);
		inter.child(9, 71010, 257, height);
		inter.child(10, 71020, 13, 28);
		//inter.child(11, 71200, 13, 28);
		
		
		addLayer(71020, layerWidth, layerHeight);
		addLayer(71100, 305, 230);
		addLayer(71200, 305, 210);
		addLayer(71220, 305, 210);
		
		RSInterface clanLayer = RSInterface.interfaceCache[71020];
		
		addSprite(71021, 854, 6, layerHeight); 
		RSInterface.interfaceCache[71021].setFlag(TILING_FLAG_BIT);
		addNewText(71022, "Clan Level 17", 100, 20, 1, 0xe2b800, 0, true, 1, 1, 0);
		addSprite(71023, 576);
		addSprite(71024, 783, 20, 14);
		RSInterface.interfaceCache[71024].setFlag(CLIP_BIT);
		RSInterface.interfaceCache[71024].width = 100;
		addSprite(71025, 784, 7, 14);
		addSprite(71026, 785, 7, 14);
		addRectangle(71027, 0, 0x000000, false, 300, 1, 0);//0x332417
		
		
		clanLayer.totalChildren(9);
		clanLayer.child(0, 71021, width - 174 - 6, 0);
		clanLayer.child(1, 71022, 10, 10);
		clanLayer.child(2, 71023, 110, 11);
		clanLayer.child(3, 71024, 115, 12);
		clanLayer.child(4, 71025, 111, 12);
		clanLayer.child(5, 71026, 288, 12);
		clanLayer.child(6, 71027, 10, 36);
		clanLayer.child(7, 71100, 0, 36);
		clanLayer.child(8, 71200, 0, 37);
		/*clanLayer.child(7, 71028, 10, 36 + 15);
		clanLayer.child(8, 71029, 10 + 34, 36 + 15);
		clanLayer.child(9, 71030, 10 + 35, 36 + 16);
		clanLayer.child(10, 71031, 10 + 36, 36 + 17);
		
		clanLayer.child(11, 71032, 10 + 155, 36 + 15);
		clanLayer.child(12, 71033, 10 + 34 + 155, 36 + 15);
		clanLayer.child(13, 71034, 10 + 35 + 155, 36 + 16);
		clanLayer.child(14, 71035, 10 + 36 + 155, 36 + 17);
		clanLayer.child(15, 71036, 10 + 36, 36 + 17);
		clanLayer.child(16, 71037, 10 + 36 + 155, 36 + 17);
		clanLayer.child(17, 71038, 10 + 155, 36 + 15);*/
		
		int endY = 0;
		RSInterface perkLayer = RSInterface.interfaceCache[71100];
		perkLayer.dynamicComponents = null;
		perkLayer.totalChildren(15 * 6);
		
		for (int i = 0; i < 15; i++) {

			int childOffset = i * 6;
			int startX = (i % 2) * 150;
			int startY = (i / 2) * 45;
			if (endY < startY) {
				endY = startY;
			}

			addRectangle(71101 + childOffset, 0, 0x7f7760, false, 136, 34, 0);//0x332417
			addRectangle(71101 + childOffset + 1, 0, 0x000000, false, 134, 32, 0);//0x332417 5e5847
			addRectangle(71101 + childOffset + 2, 200, 0x000000, true, 132, 30, 0);//0x332417 5e5847
			addSprite(71101 + childOffset + 3, 1314, 34, 34);
			addNewText(71101 + childOffset + 4, "Cash Flow", 95, 17, 0, 0xffffff, 0, true, 0, 1, 0);
			addNewText(71101 + childOffset + 5, "LvL 115", 40, 16, 0, 0xe2b800, 0, true, 1, 1, 0);

			perkLayer.child(childOffset, 71101 + childOffset,  startX + 10, startY + 5);
			perkLayer.child(childOffset + 1, 71101 + childOffset + 1,  startX + 11, startY + 6);
			perkLayer.child(childOffset + 2, 71101 + childOffset + 2,  startX + 12, startY + 7);
			perkLayer.child(childOffset + 3, 71101 + childOffset + 3, startX + 10, startY + 5);
			perkLayer.child(childOffset + 4, 71101 + childOffset + 4,  startX + 50, startY + 7);
			perkLayer.child(childOffset + 5, 71101 + childOffset + 5, startX + 105, startY + 23);
		}
		
		perkLayer.scrollMax = 500;
		perkLayer.height = 230;
		perkLayer.width = 305;
		perkLayer.hidden = true;
		
		RSInterface listLayer = RSInterface.interfaceCache[71200];
		listLayer.dynamicComponents = null;
		
		addRectangle(71201, 0, 0x7f7760, false, 286, 24, 0);//0x332417
		addRectangle(71202, 0, 0x000000, false, 284, 22, 0);//0x332417 5e5847
		addRectangle(71203 , 200, 0x000000, true, 282, 20, 0);//0x332417 5e5847
		addNewText(71204, "Cash Flow", 95, 17, 0, 0xffffff, 0, true, 0, 1, 0);
		addNewText(71205, "LvL 125", 40, 16, 0, 0xe2b800, 0, true, 1, 1, 0);

		
		listLayer.totalChildren(2);
		listLayer.child(0, 71201, 0, 0);
		listLayer.child(1, 71220, 0, 24);
		
		RSInterface listScrollLayer = RSInterface.interfaceCache[71220];
		listScrollLayer.dynamicComponents = null;
		listScrollLayer.totalChildren(6 * 5);
		
		for (int i = 0; i < 6; i++) {

			int childOffset = i * 5;
			int startX = 0;
			int startY = (i) * 25;
			if (endY < startY) {
				endY = startY;
			}

			addRectangle(71221 + childOffset, 0, 0x7f7760, false, 286, 24, 0);//0x332417
			addRectangle(71221 + childOffset + 1, 0, 0x000000, false, 284, 22, 0);//0x332417 5e5847
			addRectangle(71221 + childOffset + 2, 200, 0x000000, true, 282, 20, 0);//0x332417 5e5847
			addNewText(71221 + childOffset + 3, "Cash Flow", 95, 17, 0, 0xffffff, 0, true, 0, 1, 0);
			addNewText(71221 + childOffset + 4, "LvL 125", 40, 16, 0, 0xe2b800, 0, true, 1, 1, 0);

			listScrollLayer.child(childOffset, 71221 + childOffset,  startX + 10, startY + 5);
			listScrollLayer.child(childOffset + 1, 71221 + childOffset + 1,  startX + 11, startY + 6);
			listScrollLayer.child(childOffset + 2, 71221 + childOffset + 2,  startX + 12, startY + 7);
			listScrollLayer.child(childOffset + 3, 71221 + childOffset + 3,  startX + 50, startY + 7);
			listScrollLayer.child(childOffset + 4, 71221 + childOffset + 4, startX + 105, startY + 23);
		}
		
		listScrollLayer.scrollMax = 500;
		listScrollLayer.height = 210;
		listScrollLayer.width = 305;
		listScrollLayer.hidden = false;
	}

	public static void toolBelt() {
		int width = 500;
		int height = 320;
		int mainLayerWidth = 486;
		int mainLayerHeight = 291;
		RSInterface inter = addInterface(70000);
		addSprite(70001, 1288);
		addNewText(70002, "Tool Belt - 50/50", width, 22, 2, 0xe2b800, 0, true, 1, 1, 0);
		
		inter.totalChildren(3);
		inter.child(0, 70001, 6, 6);
		inter.child(1, 70002, 6, 6);
		inter.child(2, 70003, 6 + 7, 6 + 22);
		
		addLayer(70003, mainLayerWidth - 16, mainLayerHeight);
		
		RSInterface scrollLayer = RSInterface.interfaceCache[70003];
		scrollLayer.dynamicComponents = null;
		
		scrollLayer.scrollMax = mainLayerHeight + 1;
		scrollLayer.height = mainLayerHeight;
		scrollLayer.width = mainLayerWidth - 16;
		
		int toolBoxHeight = 100;
		addLayer(70004, mainLayerWidth, toolBoxHeight);
		
		scrollLayer.totalChildren(1);
		scrollLayer.child(0, 70004, 0, 0);
		
		RSInterface generalLayer = RSInterface.interfaceCache[70004];
		generalLayer.dynamicComponents = null;
		
		addNewText(70005, "General tools - 0/10", mainLayerWidth, 22, 1, 0xe2b800, 0, true, 0, 1, 0);
		addRectangle(70006, 0, 0x32292e/*0x40362B*/, false, mainLayerWidth - 36, 52, 0);//0x544E40
		addRectangle(70007, 0, 0x554C3F, true, mainLayerWidth - 38, 50, 0);//0x544E40
		int Id = 70008;
		int button = 1;
		for (int i = 0; i < 10; i++) {
			addSprite(Id++, 720, 34, 34);
			//TODO add item models inside boxes.
		}
		
		Id = 70008;
		int childID = 0;
		generalLayer.totalChildren(10 + 3);
		generalLayer.child(childID++, 70005, 10, 10);
		generalLayer.child(childID++, 70006, 10, 10 + 22);
		generalLayer.child(childID++, 70007, 11, 11 + 22);
		int x = 20;
		for (int i = 0; i < 10; i++) {
			generalLayer.child(childID++, Id++, x, 41);
			x += 44;
		}	
		
		//TODO: ADD MORE TOOL LAYERS FISHING, CRAFTING AND FARMING
		
	}
	
	public static void donorInterface() {
		int width = 500;
		int height = 320;
		RSInterface inter = addInterface(69000);
		addSprite(69001, 1288);
		addNewText(69002, "Soulplay Store", width, 25, 1, 0xe2b800, 0, true, 1, 1, 0);
		addRectangle(69003, 0, 0x1e1e1e, true, width - 15, height - 30, 0);//0x544E40
		RSInterface.interfaceCache[69003].setFlag(RECT_IGNORE_ATLAS);
		addRectangle(69004, 0, 0x828282, false, width - 13, height - 28, 0);//0x544E40
		RSInterface.interfaceCache[69004].setFlag(RECT_IGNORE_ATLAS);
		addSprite(69005, 854, 6, height - 30); //45
		RSInterface.interfaceCache[69005].setFlag(TILING_FLAG_BIT);
		addButton(69006, 852, 851, "Claim");
		addNewText(69007, "Buy Points", 114, 25, 1, 0xe2b800, 0, true, 1, 1, 0);
		addSprite(69008, 853, 352, 6); //45
		RSInterface.interfaceCache[69008].setFlag(TILING_FLAG_BIT);
		addButton(69009, 852, 851, "Claim");
		addNewText(69010, "Purchase", 114, 25, 1, 0xe2b800, 0, true, 1, 1, 0);
		addNewText(69011, "", 118, 25, 1, 0xe2b800, 0, true, 1, 1, 0);
		addSprite(69012, 1296, 1295, 20, 20, "Page Up");
		addSprite(69013, 1294, 1293, 20, 20, "Page Down");
		addNewText(69014, "Page 1/17", 50, 25, 0, 0xb3b3b3, 0, true, 1, 1, 15);
		addButton(69016, 300, 301, "Close");
		RSInterface.interfaceCache[69016].atActionType = 3;
		
		addLayer(69100, 4 * (83 + 4), (116 + 5) * 2);
		addLayer(69015, 115, 25 * 6);
		
		int childID = 0;
		inter.totalChildren(17);
		inter.child(childID++, 69001, 6, 6);
		inter.child(childID++, 69002, 6, 6);
		inter.child(childID++, 69003, 13, 28);
		inter.child(childID++, 69004, 12, 27);
		inter.child(childID++, 69005, 140, 28); //36
		inter.child(childID++, 69006, 19, height - 34);
		inter.child(childID++, 69007, 19, height - 34);
		inter.child(childID++, 69008, 146,  height - 47);
		inter.child(childID++, 69009, 265, height - 34);
		inter.child(childID++, 69010, 265, height - 34);
		inter.child(childID++, 69011, 146, height - 32);
		inter.child(childID++, 69012, 446, height - 32);
		inter.child(childID++, 69013, 472, height - 32);
		inter.child(childID++, 69014, 393, height - 32);
		
		inter.child(childID++, 69100, 150, 32);
		inter.child(childID++, 69015, 20, 35);
		inter.child(childID++, 69016, 484, 10); // close button
	}
	
	public static void voteInterface() {
		int width = 473;
		int height = 190;
		RSInterface inter = addInterface(67300);
		addSprite(67301, 702);
		
		addRadioButton(67302, 67300, 1038, 1037, 75, 20, "Vote Rewards", 0, 5, 6460);
		addRadioButton(67303, 67300, 1038, 1037, 75, 20, "Vote Store", 1, 5, 6460);
		addRadioButton(67304, 67300, 1038, 1037, 75, 20, "Vote Luck", 2, 5, 6460);
		addRectangle(67305, 0, 0x5B5345, false, width, height, 0);//0x544E40
		addNewText(67306, "Vote Management", width, 25, 1, 16750623, 0, true, 1, 1, 0);
		addNewText(67307, "Rewards", 70, 22, 1, 16750623, 0, true, 1, 1, 0);
		addNewText(67308, "Store", 70, 22, 1, 16750623, 0, true, 1, 1, 0);
		addNewText(67309, "Luck", 70, 22, 1, 16750623, 0, true, 1, 1, 0);
		addSprite(67310, 858, width, 5);
		RSInterface.interfaceCache[67310].setFlag(RSInterface.TILING_FLAG_BIT);	
		addButton(67311, 551, 552, "Claim");
		addNewText(67312, "Claim", 90, 32, 1, 0xff9933, 0, true, 1, 1, 0);		
		addButton(67313, 2, 3, "Vote");
		addNewText(67314, "Vote", 72, 32, 1, 0xff9933, 0, true, 1, 1, 0);
		addNewText(67315, "Vote points: 2873", 463, 44, 1, 0xff9933, 0, true, 0, 1, 0);
		addButton(67316, 300, 301, "Close");
		RSInterface.interfaceCache[67316].atActionType = 3;

		inter.totalChildren(29);
		inter.child(0, 67301, 15, 15);
		inter.child(1, 67302, 25, 55);
		inter.child(2, 67303, 105, 55);
		inter.child(3, 67304, 185, 55);
		inter.child(4, 67306, 22, 23);
		inter.child(5, 67320, 25, 75);
		inter.child(6, 67420, 25, 80);
		inter.child(7, 67900, 32, 85);
		inter.child(8, 67305, 22, 75);
		inter.child(9, 67307, 25, 55);
		inter.child(10, 67308, 105, 55);
		inter.child(11, 67309, 185, 55);
		inter.child(12, 67310, 22, height + 75);
		inter.child(13, 67311, 210, height + 86);
		inter.child(14, 67312, 210, height + 86);
		inter.child(15, 67313, 415, height + 86);
		inter.child(16, 67314, 415, height + 86);
		inter.child(17, 67315, 30, height + 80);
		inter.child(18, 67800, 340, 40);
		inter.child(19, 67802, 340, 35);
		inter.child(20, 67810, 340, 200);
		inter.child(21, 67815, 340, 200);
		inter.child(22, 67316, 474, 27);
		inter.child(23, 67404, 25 + 1, 75 + 127);
		inter.child(24, 67405, 25 + 1, 75 + 127 + 60);
		inter.child(25, 67407, 25 + 1, 75 + 127 - 64);
		inter.child(26, 67408, 25 + 1, 75 + 127 + 60 - 64);
		inter.child(27, 67410, 25 + 1, 75 + 127 - 64 - 64);
		inter.child(28, 67411, 25 + 1, 75 + 127 + 60 - 64 - 64);

		addRectangle(67404, 255, 0, true, 298, 60);
		RSInterface.interfaceCache[67404].hoverType = 67405;
		addTooltip(67405, "If applicable Advanced section offers you a Mystery box, Double experience,\nExperience lamp, 10 Voting points and a Drop rate boost.\n\nRequirements: 10 days play time and 200 votes count");

		addRectangle(67407, 255, 0, true, 298, 60);
		RSInterface.interfaceCache[67407].hoverType = 67408;
		addTooltip(67408, "If applicable Intermediate section offers you a Mystery box, Double experience,\nExperience lamp and 10 Voting points.\n\nRequirements: 3 days play time and 50 votes count");

		addRectangle(67410, 255, 0, true, 298, 60);
		RSInterface.interfaceCache[67410].hoverType = 67411;
		addTooltip(67411, "Beginner section offers you a Mystery box, Double experience and an Experience lamp.");
		
		RSInterface rewards = addInterface(67320);
		rewards.width = width;
		rewards.height = height;
		
		addRectangle(67321, 0, 0x5B5345, false, 300, 62, 0);//0x5B5345
		addRectangle(67322, 0, 0x5B5345, false, 300, 62, 0);
		addRectangle(67323, 0, 0x5B5345, false, 300, 62, 0);
		addRectangle(67324, 0, 0x5B5345, false, 169, 188, 0);
		addRectangle(67325, 0, 0x5B5345, false, 169, 20, 0);
		addNewText(67326, "Beginner", 295, 62, 1, 0xff9933, 0, true, 0, 1, 0);
		addNewText(67327, "Intermediate", 295, 62, 1, 0xff9933, 0, true, 0, 1, 0);
		addNewText(67328, "Advanced", 295, 62, 1, 0xff9933, 0, true, 0, 1, 0);
		addNewText(67329, "Vote streaks", 169, 20, 1, 0xff9933, 0, true, 1, 1, 0);
		addRectangle(67330, 0, 0x5B5345, false, 1, 168, 0);

		addSprite(67331, 720, 34, 34);
		addSprite(67332, 720, 34, 34);
		addSprite(67333, 720, 34, 34);
		addItemSprite(67334, 6199, 32, 32);
		addItemSprite(67335, 4447, 32, 32);
		addSprite(67336, 1326, 20, 20);
		addNewText(67400, "XP", 34, 34, 0, 0xFFDF00, 0, true, 1, 2, 0);

		addSprite(67337, 722, 34, 34);
		addSprite(67338, 722, 34, 34);
		addSprite(67339, 722, 34, 34);
		addSprite(67340, 722, 34, 34);
		addItemSprite(67341, 6199, 32, 32);
		addSprite(67342, 1326, 20, 20);
		addItemSprite(67343, 4447, 32, 32);
		addItemSprite(67344, 2996, 32, 32);
		RSInterface.interfaceCache[67344].itemAmount = 10;
		addNewText(67401, "XP", 34, 34, 0, 0xFFDF00, 0, true, 1, 2, 0);
		
		addSprite(67345, 724, 34, 34);
		addSprite(67346, 724, 34, 34);
		addSprite(67347, 724, 34, 34);
		addSprite(67348, 724, 34, 34);
		addSprite(67349, 724, 34, 34);
		addItemSprite(67350, 6199, 32, 32);
		addSprite(67351, 1326, 20, 20);
		addItemSprite(67352, 4447, 32, 32);
		addItemSprite(67353, 2996, 32, 32);
		RSInterface.interfaceCache[67353].itemAmount = 10;
		addSprite(67354, 1326, 20, 20);
		addNewText(67402, "XP", 34, 34, 0, 0xFFDF00, 0, true, 1, 2, 0);
		addNewText(67403, "Drops", 34, 34, 0, 0xFFDF00, 0, true, 1, 2, 0);
		
		//store.child(3, 39702, 476, 28); // close button
		//store.child(4, 39703, 476, 28); // close hover button
		
		int Id = 67355;
		for (int i = 0; i < 10; i++) {
			final int origTrans = i % 2 != 0 ? 235 : 255;
			addRectangle(Id++, origTrans, 0xffffff, true, 169, 17, 0);
			addNewText(Id++, "1x Dragon Claws", 169, 17, 0, 0xffffff, 0, true, 1, 1, 0);
			addNewText(Id++, "28", 20, 17, 0, 0xffffff, 0, true, 1, 1, 0);
		}

		Id = 67355;
		int childID = 0;
		int y = 20;
		
		rewards.totalChildren(38 + (10 * 3));

		for (int i = 0; i < 10; i++) {
			rewards.child(childID, Id++, 301, y);
			rewards.child(childID+1, Id++, 301, y + 1);
			rewards.child(childID+2, Id++, 301, y + 1);
			childID += 3;
			y += 17;
		}
		rewards.child(childID, 67321, 0, 1);
		rewards.child(childID+ 1, 67322, 0, 64);
		rewards.child(childID+ 2, 67323, 0, 127);
		rewards.child(childID+ 3, 67324, 301, 1);
		rewards.child(childID+ 4, 67325, 301, 1);
		rewards.child(childID+ 5, 67326, 5, 1);
		rewards.child(childID+ 6, 67327, 5, 64);
		rewards.child(childID+ 7, 67328, 5, 127);
		rewards.child(childID+ 8, 67329, 301, 1);
		rewards.child(childID+ 9, 67330, 321, 21);
		
		rewards.child(childID+ 10, 67331, 90, 16);
		rewards.child(childID+ 11, 67332, 131, 16);
		rewards.child(childID+ 12, 67333, 170, 16);
		rewards.child(childID+ 13, 67334, 91, 17);
		rewards.child(childID+ 14, 67335, 171, 17);
		rewards.child(childID+ 15, 67336, 132 + 6, 17);

		rewards.child(childID+ 16, 67337, 90, 79);
		rewards.child(childID+ 17, 67338, 131, 79);
		rewards.child(childID+ 18, 67339, 170, 79);
		rewards.child(childID+ 19, 67340, 209, 79);
		rewards.child(childID+ 20, 67341, 91, 80);
		rewards.child(childID+ 21, 67342, 132 + 6, 80);
		rewards.child(childID+ 22, 67343, 171, 80);
		rewards.child(childID+ 23, 67344, 210, 80);
		
		rewards.child(childID+ 24, 67345, 90, 142);
		rewards.child(childID+ 25, 67346, 131, 142);
		rewards.child(childID+ 26, 67347, 170, 142);
		rewards.child(childID+ 27, 67348, 209, 142);
		rewards.child(childID+ 28, 67349, 248, 142);
		rewards.child(childID+ 29, 67350, 91, 143);
		rewards.child(childID+ 30, 67351, 132 + 6, 143);
		rewards.child(childID+ 31, 67352, 171, 143);
		rewards.child(childID+ 32, 67353, 210, 143);
		rewards.child(childID+ 33, 67354, 249 + 6, 143);
		rewards.child(childID+ 34, 67400, 131, 16);
		rewards.child(childID+ 35, 67401, 131, 79);
		rewards.child(childID+ 36, 67402, 131, 142);
		rewards.child(childID+ 37, 67403, 248, 142);
		
		RSInterface store = addInterface(67420);
		store.width = width;
		store.height = height;

		int totalItems = VoteStoreData.values.length;
		int endY = 0;
		store.totalChildren(totalItems * 3);
		for (int i = 0; i < totalItems; i++) {
			VoteStoreData data = VoteStoreData.values[i];

			int childOffset = i * 3;
			int startX = (i % 5) * 60;
			int startY = (i / 5) * 68;
			if (endY < startY) {
				endY = startY;
			}

			int startPriceX = startX + 5;
			int startPriceY = startY + 39;

			addSprite(67421 + childOffset, 500, -1, -1, 0, 502, 46, 50, "Select", true);
			addNewText(67421 + childOffset + 1, Client.methodR(data.getPrice()), 40, 10, 0, 0xe2e2a2, 0, true, 0, 0, 0);

			if (!data.isModel()) {
				addItemSprite(67421 + childOffset + 2, data.getProductId());
				RSInterface.interfaceCache[67421 + childOffset + 2].setFlag(RSInterface.IGNORE_BOUNDS_BIT);
			} else {
				addModel(67421 + childOffset + 2, 33, 65, 0, 0, data.getZoom(), data.getProductId(), 5, data.getAnimId(), 0);
			}

			store.child(childOffset, 67421 + childOffset, startX, startY);
			//store.child(childOffset + 1, 67421 + childOffset + 1, startTokenX, startTokenY);
			store.child(childOffset + 1, 67421 + childOffset + 1, startPriceX, startPriceY);
			store.child(childOffset + 2, 67421 + childOffset + 2, startX + 7, startY + 4);
		}

		store.scrollMax = endY + 68;
		store.width = 300;
		store.height = 188;
		if (store.scrollMax < store.height) {
			store.scrollMax = store.height + 1;
		}
		store.hidden = true;
		
		RSInterface empty = addInterface(67800, InterfaceType.TAB);
		addNewText(67801, "Select an item to view<br>more<br>information/purchase.", 152, 80, 2, 0x7592a0, 0, true, 1, 1, 0);

		empty.totalChildren(1);
		empty.child(0, 67801, 2, 70);

		empty.hidden = true;

		RSInterface buy = addInterface(67802, InterfaceType.TAB);

		addSprite(67804, 100);
		RSInterface.interfaceCache[67804].itemId = 18337;
		addNewText(67805,"None", 152, 90, 1, 0xfbd6a9, 0, true, 1, 1, 0);
		addModel(67806, 37, 170, 0, 0, 0, 0, 5, -1, 0);
		RSInterface.interfaceCache[67806].hidden = true;

		buy.totalChildren(3);
		buy.child(0, 67804, 63, 45);
		buy.child(1, 67805, 2, 85);
		buy.child(2, 67806, 65, 45);

		buy.hidden = true;

		RSInterface buyButton = addInterface(67810, InterfaceType.TAB);
		addButton(67811, 799, 800, "Buy");
		addNewText(67812, "Buy", 128, 25, 2, 0xd2d2d2, 0xFFFFFF, true, 1, 1, 0);
		//addSprite(67813, 795);
		//addItemSprite(67813, 7478);
		addNewText(67814, "200,000", 160, 15, 2, 0xff9b00, 0, true, 1, 1, 0);
		buyButton.totalChildren(3);
		buyButton.child(0, 67811, 13, 30);
		buyButton.child(1, 67812, 13, 30);
		//buyButton.child(2, 67813, 30, 8);
		buyButton.child(2, 67814, 0, 6);
		buyButton.hidden = true;

		RSInterface buyConfirm = addInterface(67815, InterfaceType.TAB); 
		addButton(67816, 801, 802, "Yes");
		addNewText(67817, "Yes", 68, 25, 2, 0xd2d2d2, 0xFFFFFF, true, 1, 1, 0);
		addButton(67818, 801, 802, "No");
		addNewText(67819, "No", 68, 25, 2, 0xd2d2d2, 0xFFFFFF, true, 1, 1, 0);
		addNewText(67820, "Confirm purchase", 160, 15, 2, 0xff9b00, 0, true, 1, 1, 0);
		buyConfirm.totalChildren(5);
		buyConfirm.child(0, 67816, 1, 30);
		buyConfirm.child(1, 67817, 1, 30);
		buyConfirm.child(2, 67818, 85, 30);
		buyConfirm.child(3, 67819, 85, 30);
		buyConfirm.child(4, 67820, 0, 6);
		buyConfirm.hidden = true;
		
		RSInterface luck = addInterface(67900);
		luck.width = width - 7;
		luck.height = height - 10;
		luck.totalChildren(44 * 3 + 1);
		int endBoxY = 0;
		for (int i = 0; i < 44; i++) {
			int childOffset = i * 3;
			int startX = (i % 11) * 42;
			int startY = (i / 11) * 45;
			if (endBoxY < startY) {
				endBoxY = startY;
			}
			//int startTokenX = startX + 4;
			//int startTokenY = startY + 39;
			//int startPriceX = startX + 5;
			//int startPriceY = startY + 39;

			addSprite(67901 + childOffset, 721, -1, -1, 0, 720, 34, 34, "Flip", true);
			addNewText(67901 + childOffset + 1, "?", 34, 34, 2, 0xffffff, 0, true, 1, 1, 0);
			addSprite(67901 + childOffset + 2, -1, 36, 32);
			
			luck.child(childOffset, 67901 + childOffset, startX, startY);
			luck.child(childOffset + 1, 67901 + childOffset + 1, startX, startY + 2);
			luck.child(childOffset + 2, 67901 + childOffset + 2, startX + 2, startY + 2);
		}

		luck.child(44 * 3, 68500, -9, -9);
		luck.hidden = true;

		RSInterface luckLock = addInterface(68500, InterfaceType.TAB);
		luckLock.width = width;
		luckLock.height = height;
		luckLock.setFlag(CANT_CLICK_THROUGH);

		addRectangle(68501, 80, 0, true, width, height, 0);
		addNewText(68502, "By accepting this you understand that once a box is clicked, a game of flip boxes will start and you will be charged 10 vote points.", width - 100, height - 140, 1, 0xffffff, 0, true, 1, 1, 0);
		addButton(68503, 799, 800, "Accept");
		addNewText(68504, "Accept", 128, 25, 2, 0xd2d2d2, 0xFFFFFF, true, 1, 1, 0);
		
		luckLock.totalChildren(4);
		luckLock.child(0, 68501, 0, 0);
		luckLock.child(1, 68502, 50, 50);
		luckLock.child(2, 68503, width / 2 - 64, 100);
		luckLock.child(3, 68504, width / 2 - 64, 100);
	}
	
	
	private static void hpBarHud() {
		int width = 300;

		RSInterface inter = addInterface(67240);

		RSInterface panel = addInterface(67241);
		panel.width = width;
		panel.height = 38;

		RSInterface header = addInterface(67242);
		header.width = 80;
		header.height = 18;

		header.createDynamic(0, addRectangle(0, 0, 80, 18, 921100, 0, false));
		header.createDynamic(1, addRectangle(1, 1, 78, 16, 4671301, 0, false));
		RSInterface bg = addSprite(1, 1, 77, 15, "tradebacking,0");
		bg.setFlag(CLIP_BIT);
		header.createDynamic(2, bg);
		header.createDynamic(3, addNewText("test", 0, 0, 80, 18, 0, 16750623, 0, true, 1, 1, 0));

		RSInterface footer = addInterface(67243);
		footer.width = width;
		footer.height = 20;
		addRectangle(67244, 0, 921100, false, width, 20, 0);
		addRectangle(67245, 0, 4671301, false, width - 2, 18, 0);
		addRectangle(67246, 0, 13369344, true, width - 4, 16, 0);
		addRectangle(67247, 0, 52224, true, width - 4, 16, 0);
		addNewText(67248, "- / -", width, 20, 0, 0, 0, false, 1, 1, 0);

		footer.totalChildren(5);
		footer.child(0, 67244, 0, 0);
		footer.child(1, 67245, 1, 1);
		footer.child(2, 67246, 2, 2);
		footer.child(3, 67247, 2, 2);
		footer.child(4, 67248, 0, 0);

		panel.totalChildren(2);
		panel.child(0, 67242, width / 2 - 80 / 2, 0);
		panel.child(1, 67243, 0, 18);

		inter.totalChildren(1);
		inter.child(0, 67241, 5, 20);
	}

	private static void coxRewards() {
		RSInterface inter = addInterface(67230);

		RSInterface background = addInterface(67231);
		background.width = 260;
		background.height = 180;
		addBorder(background, "Rewards", true);

		addModel(67232, 0, 0, 1536, 0, 1700, 32506, 1, -1, 0);
		RSInterface.interfaceCache[67232].setFlag(OSRS_MODEL);

		addModel(67233, 0, 0, 1891, 105, 600, 32756, 1, -1, 0);
		RSInterface.interfaceCache[67233].setFlag(OSRS_MODEL);

		addInventory(67234, 1, 3, 0, 4, false, "Take");

		inter.totalChildren(4);
		inter.child(0, 67231, (inter.width - 260) / 2, (inter.height - 180) / 2);
		inter.child(1, 67232, (inter.width - 260) / 2 + 75, (inter.height - 180) / 2 + 163);
		inter.child(2, 67233, (inter.width - 260) / 2 + 197, (inter.height - 180) / 2 + 147);
		inter.child(3, 67234, (inter.width - 260) / 2 + 135, (inter.height - 180) / 2 + 40);
	}

	public static void addBorder(RSInterface inter, String title, boolean closable) {
		RSInterface background = RSInterface.addSprite(0, 0, inter.width, inter.height, "tradebacking,0");
		background.setFlag(TILING_FLAG_BIT);
		inter.createDynamic(0, background);
		inter.createDynamic(1, RSInterface.addNewText(title, 6, 6, inter.width - 12, 24, 2, 16750623, 0, true, 1, 1, 0));
		inter.createDynamic(2, RSInterface.addSprite(0, 0, 25, 30, "steelborder,0"));
		inter.createDynamic(3, RSInterface.addSprite(inter.width - 25, 0, 25, 30, "steelborder,1"));
		inter.createDynamic(4, RSInterface.addSprite(0, inter.height - 30, 25, 30, "steelborder,2"));
		inter.createDynamic(5, RSInterface.addSprite(inter.width - 25, inter.height - 30, 25, 30, "steelborder,3"));
		RSInterface titleFrame = RSInterface.addSprite(6, 14, inter.width - 12, 6, "steelborder2,0");
		titleFrame.setFlag(TILING_FLAG_BIT);
		inter.createDynamic(6, titleFrame);
		RSInterface leftFrame = RSInterface.addSprite(-15, 30, 6, inter.height - 60, "miscgraphics,2");
		leftFrame.setFlag(TILING_FLAG_BIT);
		inter.createDynamic(7, leftFrame);
		RSInterface rightFrame = RSInterface.addSprite(inter.width - 15 - 6, 30, 6, inter.height - 60, "steelborder2,1");
		rightFrame.setFlag(TILING_FLAG_BIT);
		inter.createDynamic(8, rightFrame);
		RSInterface topFrame = RSInterface.addSprite(25, -15, inter.width - 50, 6, "steelborder2,0");
		topFrame.setFlag(TILING_FLAG_BIT);
		topFrame.setFlag(IGNORE_BOUNDS_BIT);
		inter.createDynamic(9, topFrame);
		RSInterface bottomFrame = RSInterface.addSprite(25, inter.height - 15 - 6, inter.width - 50, 6, "miscgraphics,3");
		bottomFrame.setFlag(TILING_FLAG_BIT);
		bottomFrame.setFlag(IGNORE_BOUNDS_BIT);
		inter.createDynamic(10, bottomFrame);

		if (closable) {
			RSInterface closeButton = RSInterface.addSprite(inter.width - 3 - 26, 6, 26, 23, 910, 911, "");
			closeButton.atActionType = 3;
			inter.createDynamic(11, closeButton);
		}
	}

	private static void coxHud() {
		RSInterface main = addInterface(67220);

		main.totalChildren(1);
		main.child(0, 67221, 5, 20);

		RSInterface inter = addInterface(67221);
		inter.width = 200;
		inter.height = 49;

		addRectangle(67222, 0, 4800564, true, inter.width, inter.height, 0);
		addRectangle(67223, 0, 3682339, false, inter.width, inter.height, 0);
		addRectangle(67224, 0, 5919301, false, inter.width - 2, inter.height - 2, 0);
		addNewText(67225, "Total:<br>Leanbow:<br>Time:", inter.width - 16, inter.height, 1, 16750623, 0, true, 0, 1, 0);
		addNewText(67226, "0<br>0<br>0", inter.width - 16, inter.height, 1, 16777215, 0, true, 2, 1, 0);

		inter.totalChildren(5);
		inter.child(0, 67222, 0, 0);
		inter.child(1, 67223, 0, 0);
		inter.child(2, 67224, 1, 1);
		inter.child(3, 67225, 8, 0);
		inter.child(4, 67226, 8, 0);
	}

	public static void coxPartySiderbar() {
		RSInterface tab = addInterface(67200, InterfaceType.TAB);
		
		addNewText(67201, "Raiding Party", 186, 16, 2, 0xff9933, 0, true, 1, 1, 0);
		addRectangle(67202, 0, 0x473C32, true, 181, 55, 0);
		RSInterface.interfaceCache[67202].setFlag(RECT_IGNORE_ATLAS);
		addRectangle(67203, 0, 0x3F342C, true, 181, 175, 0);
		RSInterface.interfaceCache[67203].setFlag(RECT_IGNORE_ATLAS);
		addRectangle(67204, 0, 0x000000, true, 182, 1, 0);
		addRectangle(67205, 0, 0x000000, true, 1, 177, 0);
		addRectangle(67206, 0, 0x000000, true, 1, 57, 0);
		addNewText(67207, "Party Size: <col=ffffff>1</col>", 72, 16, 0, 0xff9933, 0, true, 1, 1, 0);
		//addNewText(67208, "1", 15, 16, 0, 0xffffff, 0, true, 1, 1, 0);
		
		tab.totalChildren(15);
		tab.child(0, 67201, 0, 1);
		tab.child(1, 67202, 3, 200);
		tab.child(2, 67203, 3, 21);
		tab.child(3, 67204, 2, 20);
		tab.child(4, 67205, 2, 20);
		tab.child(5, 67204, 2, 196);
		tab.child(6, 67205, 183, 20);
		tab.child(7, 67204, 2, 199);
		tab.child(8, 67206, 2, 199);
		tab.child(9, 67204, 2, 256);
		tab.child(10, 67206, 183, 199);
		tab.child(11, 67207, 60, 202);
		tab.child(12, 67209, 0, 220);
		tab.child(13, 67212, 0, 220);
		tab.child(14, 67214, 0, 22);
		
		RSInterface leader = addInterface(67209, InterfaceType.TAB);
		
		addButton(67210, 2, 3, "Start Raid");
		addNewText(67211, "Start Raid", 72, 32, 1, 0xff9933, 0, true, 1, 1, 0);
		
		leader.totalChildren(2);
		leader.child(0, 67210, 60, 0);
		leader.child(1, 67211, 60, 0);
		leader.hidden = false;
		
        RSInterface player = addInterface(67212, InterfaceType.TAB);
		
		addNewText(67213, "Waiting for your leader to begin the raid...", 160, 26, 1, 0xff9933, 0, true, 1, 1, 0);
		
		player.totalChildren(1);
		player.child(0, 67213, 13, 0);
		player.hidden = true;
		
		RSInterface scroll = addInterface(67214, InterfaceType.TAB);
		
		scroll.scrollMax = 190;
		scroll.width = 166;
		scroll.height = 173;
	}

	public static void overlay() {
		RSInterface inter = RSInterface.interfaceCache[6673];

		addNewText(6570, "", 100, 20, 1, 0xffff00, 0, true, 1, 1, 0);
		addNewText(6572, "", 100, 20, 1, 0xffff00, 0, true, 1, 1, 0);
		addNewText(6664, "", 100, 20, 1, 0xffff00, 0, true, 1, 1, 0);

		inter.totalChildren(3);
		inter.child(0, 6570, 200, 7);
		inter.child(1, 6572, 200, 26);
		inter.child(2, 6664, 200, 45);
	}
	
	public static void scrollingShop() {
		RSInterface tab = addInterface(67100, InterfaceType.MAIN);
		final RSInterface scroll = addInterface(67110, InterfaceType.MAIN);

		addSprite(67101, 880);
		addText(67102, "Shop", 2, 0xff9900, true, true, 0);
		addText(67103, "Right-click on an item in the shop to see the available options.", 0, 0xff9900, true, true, 0);

		tab.totalChildren(6);
		tab.child(0, 67101, 9, 20);
		tab.child(1, 67102, 256, 28);
		tab.child(2, 67103, 256, 296);
		tab.child(3, 43667, 484, 26); // close button
		tab.child(4, 43668, 484, 26); // close hover button
		tab.child(5, 67110, 32, 63);

		final int rows = 10;
		final int yOff = 5;
		final int itemHeight = 32;
		addInventory(67111, rows, 20, 10, yOff, false, "Value", "Buy 1", "Buy 5", "Buy 10", "Buy 50");
		RSInterface.interfaceCache[67111].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		RSInterface.interfaceCache[67111].addOnInvetoryUpdate(67111, new Runnable() {
			@Override
			public void run() {
				int itemCount = -60;
				for (int i = 0; i < 200; i++) {
					int id = RSInterface.interfaceCache[67111].inv[i];
					if (id > 0) {
						itemCount++;
					}
				}

				int extraScroll;
				if (itemCount > 0) {
					extraScroll = ((itemCount / rows) + 1) * (itemHeight + yOff);
				} else {
					extraScroll = 0;
				}

				scroll.scrollMax = (scroll.height + 1) + extraScroll;
			}
		});

		scroll.totalChildren(1);
		scroll.child(0, 67111, 8, 7);
		scroll.width = 443;
		scroll.height = 225;
	}
	
	public static void playerInfoTab() {
		RSInterface tab = addInterface(67050, InterfaceType.TAB);
		addNewText(67051, "Donator rank: None", 186, 28, 1, 0xff9933, 0, true, 1, 1, 0);
		//addSprite(67051, 1265, 156, 28);//big na
		addButton(67052, 1265, "Change Your In-Game Name");//name changer
		addNewText(67053, "Open Store", 50, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addSprite(67054, 1266, 20, 20);//store icon
		addNewText(67055, "Spinning Wheel", 50, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addSprite(67056, 1267, 20, 20);//wheel icon
		addNewText(67057, "Total Votes: 29", 186, 28, 1, 0xff9933, 0, true, 1, 1, 0);
		//addSprite(63448, 1268, 76, 28);//small
		addButton(67058, 1268, "Open Store");//open store
		addNewText(67059, "Vote For Rewards", 76, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addNewText(67060, "Claim Vote Rewards", 76, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addNewText(67061, "Name: Julius", 186, 28, 1, 0xff9933, 0, true, 1, 1, 0);
		addNewText(67062, "Name Changer", 156, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addSprite(67063, 1269, 20, 20);//name change icon
		addNewText(67064, "Claim", 50, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addNewText(67065, "Claim", 50, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addNewText(67066, "Soulplay Website", 156, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addNewText(67067, "Official Discord", 156, 28, 0, 0xffffff, 0xC0C0C0, true, 1, 1, 0);
		addSprite(67068, 1271, 20, 20);//inbox icon
		addSprite(67069, 1270, 20, 20);//discord icon
		addButton(67070, 1265, "Open Soulplay Website");//soulplay website
		addButton(67071, 1265, "Open Discord");//official discord
		addButton(67072, 1268, "Claim Store Items");//claim store items
		addButton(67073, 1268, "Open Spinning Wheel");//open spinning wheel
		addButton(67074, 1268, "Claim Spin Items");//claim spin items
		addButton(67075, 1268, "Vote For Rewards");//vote for rewards
		addButton(67076, 1268, "Claim Vote Rewards");//claim vote rewards
		tab.totalChildren(31);
		tab.child(0, 67051, 0, 0);
		tab.child(1, 67058, 15, 24);
		tab.child(2, 67053, 41, 24);
		tab.child(3, 67054, 21, 28);
		tab.child(4, 67073, 15, 56);
		tab.child(5, 67055, 41, 56);
		tab.child(6, 67056, 21, 62);
		tab.child(7, 67057, 0, 84);
		tab.child(8, 67075, 15, 108);
		tab.child(9, 67076, 97, 108);
		tab.child(10, 67059, 15, 108);
		tab.child(11, 67060, 97, 108);
		tab.child(12, 67061, 0, 136);
		tab.child(13, 67052, 15, 160);
		tab.child(14, 67062, 15, 160);
		tab.child(15, 67063, 21, 164);
		tab.child(16, 67063, 145, 164);
		tab.child(17, 67072, 97, 24);
		tab.child(18, 67064, 97, 24);
		tab.child(19, 67054, 145, 28);
		tab.child(20, 67074, 97, 56);
		tab.child(21, 67065, 97, 56);
		tab.child(22, 67056, 145, 62);
		tab.child(23, 67070, 15, 192);
		tab.child(24, 67066, 15, 192);
		tab.child(25, 67068, 21, 196);
		tab.child(26, 67068, 145, 196);
		tab.child(27, 67071, 15, 224);
		tab.child(28, 67067, 15, 224);
		tab.child(29, 67069, 21, 228);
		tab.child(30, 67069, 145, 228);
		

	}

	private static void addSmithingOption() {
		String[] options = { "Make", "Make 5", "Make 10", "Make All", null, null };
		RSInterface.interfaceCache[1119].actions = options;
		RSInterface.interfaceCache[1120].actions = options;
		RSInterface.interfaceCache[1121].actions = options;
		RSInterface.interfaceCache[1122].actions = options;
		RSInterface.interfaceCache[1123].actions = options;
	}

	public static void blackList() {
		RSInterface rsi = addInterface(67000, InterfaceType.MAIN);
		
		addButton(67001, 300, 301, "Close");
		RSInterface.interfaceCache[67001].atActionType = 3;
		addSprite(67002, 854, 6, 110);
		RSInterface.interfaceCache[67002].setFlag(TILING_FLAG_BIT);
		RSInterface.interfaceCache[67002].flipHorizontal();
		addSprite(67003, 854, 6, 110);
		RSInterface.interfaceCache[67003].setFlag(TILING_FLAG_BIT);
		addSprite(67004, 853, 400, 6);
		RSInterface.interfaceCache[67004].setFlag(TILING_FLAG_BIT);
		RSInterface.interfaceCache[67004].flipVertical();
		addSprite(67005, 853, 400, 6);
		RSInterface.interfaceCache[67005].setFlag(TILING_FLAG_BIT);
		addRectangle(67006, 0, 0x544B40, true, 398, 99, 0);
		RSInterface.interfaceCache[67006].setFlag(RECT_IGNORE_ATLAS);
		addRectangle(67007, 0, 0x312B25, true, 192, 89, 0);
		RSInterface.interfaceCache[67007].setFlag(RECT_IGNORE_ATLAS);
		addButton(67008, 549, 550, "Add Player");
		addSprite(67009, 844);
		addNewText(67010, "Add Player", 145, 32, 2, 0xeb981f, 0, true, 1, 1, 0);
		addNewText(67011, "The black list allows you to block players from attacking you below 10 Wilderness.", 180, 42, 1, 0xeb981f, 0, true, 1, 1, 0);
		addRectangle(67012, 0, 0x000000, true, 192, 1, 0);
		addRectangle(67013, 0, 0x000000, true, 1, 91, 0);
		RSInterface layer = addInterface(67014, InterfaceType.MAIN);
		layer.width = 192;
		layer.height = 89;

		int boxOffX = 5;
		int boxOffY = 2;
		int extraX = 50;
		int extraY = 180;
		rsi.totalChildren(20);
		rsi.child(0, 67002, 5 + extraX, 23 + extraY);
		rsi.child(1, 67003, 409 + extraX, 23 + extraY);
		rsi.child(2, 67004, 10 + extraX, 23 + extraY);
		rsi.child(3, 67005, 10 + extraX, 128 + extraY);
		rsi.child(4, 67006, 11 + extraX, 29 + extraY);
		rsi.child(5, 67007, 210 + extraX - boxOffX, 36 + extraY - boxOffY);
		rsi.child(6, 67008, 18 + extraX, 90 + extraY);
		rsi.child(7, 67009, 35 + extraX, 90 + extraY);
		rsi.child(8, 67010, 53 + extraX, 90 + extraY);
		rsi.child(9, 67011, 18 + extraX, 40 + extraY);
		rsi.child(10, 67012, 210 + extraX - boxOffX, 35 + extraY - boxOffY);
		rsi.child(11, 67013, 209 + extraX - boxOffX, 35 + extraY - boxOffY);
		rsi.child(12, 67012, 210 + extraX - boxOffX, 125 + extraY - boxOffY);
		rsi.child(13, 67013, 401 + extraX - boxOffX, 35 + extraY - boxOffY);
		rsi.child(14, 67012, 210 + extraX - boxOffX, 53 + extraY - boxOffY);
		rsi.child(15, 67012, 210 + extraX - boxOffX, 71 + extraY - boxOffY);
		rsi.child(16, 67012, 210 + extraX - boxOffX, 89 + extraY - boxOffY);
		rsi.child(17, 67012, 210 + extraX - boxOffX, 107 + extraY - boxOffY);
		rsi.child(18, 67001, 400 + extraX, 22 + extraY);
		rsi.child(19, 67014, 210 + extraX - boxOffX, 36 + extraY - boxOffY);
	}

	public static void taskInterface() {
		RSInterface rsi = addInterface(66300, InterfaceType.MAIN);
		
		addSprite(66301, 702);
		
		addNewText(66302, "Daily Tasks", 475, 25, 2, 0xeb981f, 0, true, 1, 1, 0);
		addSprite(66303, 1241, 474, 2);
		RSInterface.interfaceCache[66303].setFlag(TILING_FLAG_BIT);
		addSprite(66304, 1243, 1244, ConfigCodes.DAILY_TASK_TABS, 0 ,1242, 40, 40, "Category", false);
		RSInterface category = RSInterface.interfaceCache[66304];
		category.atActionType = 5;
		category.onMouseClick = new MouseEvent() {
			@Override
			public void run() {
				for (int id : Client.CATEGORY_LAYERS) {
					RSInterface.interfaceCache[id].hidden = true;
				}

				RSInterface.interfaceCache[Client.CATEGORY_LAYERS[0]].hidden = false;
			}
		};
		addSprite(66305, 1243, 1244, ConfigCodes.DAILY_TASK_TABS, 1, 1242, 40, 40, "Category", false);
		category = RSInterface.interfaceCache[66305];
		category.atActionType = 5;
		category.onMouseClick = new MouseEvent() {
			@Override
			public void run() {
				for (int id : Client.CATEGORY_LAYERS) {
					RSInterface.interfaceCache[id].hidden = true;
				}

				RSInterface.interfaceCache[Client.CATEGORY_LAYERS[1]].hidden = false;
			}
		};
		addSprite(66306, 1243, 1244, ConfigCodes.DAILY_TASK_TABS, 2 ,1242, 40, 40, "Category", false);
		category = RSInterface.interfaceCache[66306];
		category.atActionType = 5;
		category.onMouseClick = new MouseEvent() {
			@Override
			public void run() {
				for (int id : Client.CATEGORY_LAYERS) {
					RSInterface.interfaceCache[id].hidden = true;
				}

				RSInterface.interfaceCache[Client.CATEGORY_LAYERS[2]].hidden = false;
			}
		};
		addSprite(66307, 1243, 1244, ConfigCodes.DAILY_TASK_TABS, 3, 1242, 40, 40, "Category", false);
		category = RSInterface.interfaceCache[66307];
		category.atActionType = 5;
		category.onMouseClick = new MouseEvent() {
			@Override
			public void run() {
				for (int id : Client.CATEGORY_LAYERS) {
					RSInterface.interfaceCache[id].hidden = true;
				}

				RSInterface.interfaceCache[Client.CATEGORY_LAYERS[3]].hidden = false;
			}
		};
		addSprite(66308, 1243, 1244, ConfigCodes.DAILY_TASK_TABS, 4, 1242, 40, 40, "Category", false);
		category = RSInterface.interfaceCache[66308];
		category.atActionType = 5;
		category.onMouseClick = new MouseEvent() {
			@Override
			public void run() {
				for (int id : Client.CATEGORY_LAYERS) {
					RSInterface.interfaceCache[id].hidden = true;
				}

				RSInterface.interfaceCache[Client.CATEGORY_LAYERS[4]].hidden = false;
			}
		};
		
		addNewText(66309, "Total Points: 285", 135, 14, 0, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66310, "Tasks Completed: 23", 135, 14, 0, 0xeb981f, 0, true, 0, 1, 0);
		addButton(66311, 584, 583, "Open Shop");
		addNewText(66312, "Shop", 74, 30, 2, 0xeb981f, 0, true, 1, 1, 0);
		
		addNewText(66314, "Daily Tasks", 70, 15, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66316, "Progress", 60, 15, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66317, "Time left", 60, 15, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66328, "Bonus Task: ", 60, 15, 0, 0xeb981f, 0, true, 0, 1, 0);
		
		rsi.totalChildren(27);
		rsi.child(0, 66301, 15, 15);
		rsi.child(1, 66302, 20, 25);
		rsi.child(2, 48052, 475, 27);
		rsi.child(3, 66303, 21, 98);
		rsi.child(4, 66304, 25, 60);
		rsi.child(5, 66305, 66, 60);
		rsi.child(6, 66306, 107, 60);
		rsi.child(7, 66307, 148, 60);
		rsi.child(8, 66308, 189, 60);
		rsi.child(9, 66309, 240, 55);
		rsi.child(10, 66310, 240, 70);
		rsi.child(11, 66311, 400, 60);
		rsi.child(12, 66312, 400, 60);
		rsi.child(13, 66313, 21, 130);
		
		/*Icons*/
		rsi.child(14, 66236, 33, 67);
		rsi.child(15, 66237, 74, 67);
		rsi.child(16, 66238, 115, 67);
		rsi.child(17, 66239, 156, 67);
		rsi.child(18, 66240, 197, 67);
		
		rsi.child(19, 66314, 15 + 21, 110);
		rsi.child(20, 66316, 300 + 21, 110);
		rsi.child(21, 66317, 410 + 21, 110);
		rsi.child(22, 66324, 21, 130);
		rsi.child(23, 66325, 21, 130);
		rsi.child(24, 66326, 21, 130);
		rsi.child(25, 66327, 21, 130);
		rsi.child(26, 66328, 240, 85);
		
		RSInterface Category1 = addInterface(66313, InterfaceType.MAIN);

		/*First Task*/
		addNewText(66318, "Monday:", 45, 15, 0, 0xffffff, 0, true, 0, 0, 0);
		addNewText(66319, "Fish 100 rocktails and also must go and fish michael in edgeville.", 200, 22, 0, 0xeb981f, 0, true, 0, 0, 0);
		addSprite(66320, 989);
		addRectangle(66321, 100, 0x20C000, true, 87, 13, 0); //Bar length = 137
		addNewText(66322, "0 / 100", 143, 20, 0, 0xeb981f, 0, true, 1, 1, 0);
		addNewText(66323, "23 Hours", 45, 15, 0, 0xeb981f, 0, true, 0, 0, 0);
		
		RSInterface Category2 = addInterface(66324, InterfaceType.MAIN);
		Category2.hidden = true;
		RSInterface Category3 = addInterface(66325, InterfaceType.MAIN);
		Category3.hidden = true;
		RSInterface Category4 = addInterface(66326, InterfaceType.MAIN);
		Category4.hidden = true;
		RSInterface Category5 = addInterface(66327, InterfaceType.MAIN);
		Category5.hidden = true;
	
		Category1.totalChildren(6);
		/*First Task*/
		Category1.child(0, 66318, 5, 15);
		Category1.child(1, 66319, 50, 15);
		Category1.child(2, 66320, 260, 12);
		Category1.child(3, 66321, 263, 15);
		Category1.child(4, 66322, 260, 12);
		Category1.child(5, 66323, 418, 11);
	}
	
	public static void taskTab() {
		RSInterface tab = addInterface(66200, InterfaceType.TAB);
		addText(66201, "Daily Tasks", 2, 0xeb981f, false, true);
		
		addSprite(66202, 854, 6, 200);
		RSInterface.interfaceCache[66202].setFlag(TILING_FLAG_BIT);
		RSInterface.interfaceCache[66202].flipHorizontal();
		addSprite(66203, 854, 6, 200);
		RSInterface.interfaceCache[66203].setFlag(TILING_FLAG_BIT);
		addSprite(66204, 853, 166, 6);
		RSInterface.interfaceCache[66204].setFlag(TILING_FLAG_BIT);
		RSInterface.interfaceCache[66204].flipVertical();
		addSprite(66205, 853, 166, 6);
		RSInterface.interfaceCache[66205].setFlag(TILING_FLAG_BIT);
		addRectangle(66206, 0, 0x544B40, true, 164, 188, 0);
		RSInterface.interfaceCache[66206].setFlag(RECT_IGNORE_ATLAS);
		addRectangle(66207, 0, 0x312B25, true, 118, 13, 0);
		/*Blue box*/
		addRectangle(66208, 0, 0x2000C8, true, 120, 1, 0);
		addRectangle(66209, 0, 0x2000C8, true, 1, 15, 0);
		addRectangle(66210, 0, 0x2000C8, true, /*bar %*/ 100, 11, 0);
		addNewText(66211, "Category", 75, 10, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66212, "0.0%", 40, 15, 0, 0xeb981f, 0, true, 1, 0, 0);
		/*Green box*/
		addRectangle(66213, 0, 0x20C000, true, 120, 1, 0);
		addRectangle(66214, 0, 0x20C000, true, 1, 15, 0);
		addRectangle(66215, 0, 0x20C000, true, /*bar %*/ 45, 11, 0);
		addNewText(66216, "Category", 75, 10, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66217, "0.0%", 40, 15, 0, 0xeb981f, 0, true, 1, 0, 0);
		/*Orange box*/
		addRectangle(66218, 0, 0xC05008, true, 120, 1, 0);
		addRectangle(66219, 0, 0xC05008, true, 1, 15, 0);
		addRectangle(66220, 0, 0xC05008, true, /*bar %*/ 74, 11, 0);
		addNewText(66221, "Category", 75, 10, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66222, "0.0%", 40, 15, 0, 0xeb981f, 0, true, 1, 0, 0);
		/*Light blue box*/
		addRectangle(66223, 0, 0x0098A0, true, 120, 1, 0);
		addRectangle(66224, 0, 0x0098A0, true, 1, 15, 0);
		addRectangle(66225, 0, 0x0098A0, true, /*bar %*/ 10, 11, 0);
		addNewText(66226, "Category", 75, 10, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66227, "0.0%", 40, 15, 0, 0xeb981f, 0, true, 1, 0, 0);
		/*Red box*/
		addRectangle(66228, 0, 0x980810, true, 120, 1, 0);
		addRectangle(66229, 0, 0x980810, true, 1, 15, 0);
		addRectangle(66230, 0, 0x980810, true, /*bar %*/ 27, 11, 0);
		addNewText(66231, "Category", 75, 10, 1, 0xeb981f, 0, true, 0, 1, 0);
		addNewText(66232, "0.0%", 40, 15, 0, 0xeb981f, 0, true, 1, 0, 0);
		//End of boxes
		
		addButton(66233, 796, 797, "View Daily Tasks");
		addNewText(66234, "View Tasks", 83, 20, 1, 0xeb981f, 0, true, 1, 1, 0);
		addNewText(66235, "Tasks reset in: 4 Days", 190, 15, 0, 0xeb981f, 0, true, 1, 1, 0);
		
		/*Icons*/
		addSprite(66236, 1216, 25, 25);
		addSprite(66237, 1217, 25, 25);
		addSprite(66238, 1218, 25, 25);
		addSprite(66239, 1219, 25, 25);
		addSprite(66240, 1220, 25, 25);

		/*Lock stuff*/
		tab.addOnVarpUpdate(ConfigCodes.TASKS_LOCKED, new Runnable() {
			@Override
			public void run() {
				Client.updateDailyTasksState();
			}
		});

		addRectangle(66241, 60, 0, true, 190, 260, 0);
		addNewText(66242, "You need at least 750 total level or prestige 1 or higher to access daily tasks.", 190, 260, 1, 0xffffff, 0, true, 1, 1, 0);

		tab.totalChildren(59);
		tab.child(0, 66201, 5, 5);
		tab.child(1, 62102, 150, 2);
		tab.child(2, 62103, 168, 2);
		tab.child(3, 62105, 132, 2);
	    tab.child(4, 66206, 11, 29);
		tab.child(5, 66202, 5, 23);
		tab.child(6, 66203, 175, 23);
		tab.child(7, 66204, 10, 23);
		tab.child(8, 66205, 10, 217);
		/*Blue box*/
		tab.child(9, 66208, 48, 49);
		tab.child(10, 66208, 48, 63);
		tab.child(11, 66209, 48, 49);
		tab.child(12, 66209, 167, 49);
		tab.child(13, 66207, 49, 50);
		tab.child(14, 66210, 50, 51);
		tab.child(15, 66211, 49, 36);
		tab.child(16, 66212, 134, 36);
		/*Green box*/
		tab.child(17, 66213, 48, 84);
		tab.child(18, 66213, 48, 98);
		tab.child(19, 66214, 48, 84);
		tab.child(20, 66214, 167, 84);
		tab.child(21, 66207, 49, 85);
		tab.child(22, 66215, 50, 86);
		tab.child(23, 66216, 49, 71);
		tab.child(24, 66217, 134, 71);
		/*Orange box*/
		tab.child(25, 66218, 48, 119);
		tab.child(26, 66218, 48, 133);
		tab.child(27, 66219, 48, 119);
		tab.child(28, 66219, 167, 119);
		tab.child(29, 66207, 49, 120);
		tab.child(30, 66220, 50, 121);
		tab.child(31, 66221, 49, 106);
		tab.child(32, 66222, 134, 106);
		/*Light blue box*/
		tab.child(33, 66223, 48, 154);
		tab.child(34, 66223, 48, 168);
		tab.child(35, 66224, 48, 154);
		tab.child(36, 66224, 167, 154);
		tab.child(37, 66207, 49, 155);
		tab.child(38, 66225, 50, 156);
		tab.child(39, 66226, 49, 141);
		tab.child(40, 66227, 134, 141);
		/*Red box*/
		tab.child(41, 66228, 48, 189);
		tab.child(42, 66228, 48, 203);
		tab.child(43, 66229, 48, 189);
		tab.child(44, 66229, 167, 189);
		tab.child(45, 66207, 49, 190);
		tab.child(46, 66230, 50, 191);
		tab.child(47, 66231, 49, 176);
		tab.child(48, 66232, 134, 176);
		//End of boxes
		
		tab.child(49, 66233, 50, 226);
		tab.child(50, 66234, 50, 226);
		tab.child(51, 66235, 0, 246);

		/*Icons*/
		tab.child(52, 66236, 17, 42);
		tab.child(53, 66237, 17, 77);
		tab.child(54, 66238, 17, 112);
		tab.child(55, 66239, 17, 147);
		tab.child(56, 66240, 17, 182);

		/*Lock stuff*/
		tab.child(57, 66241, 0, 0);
		tab.child(58, 66242, 0, 0);
	}
	
	public static void LMSInfo() {
		final RSInterface tab = addInterface(66110, InterfaceType.MAIN);
		
		int resizeX = 0; // 0 = BIG 37 = SMALL
		int moveFirst = 0; // 0 = BIG 59 = SMALL
		int moveSecond = 0; // 0 = BIG 24 = SMALL
		int moveThird = 0; // 0 = BIG 28 = SMALL

		tab.addOnVarpUpdate(ConfigCodes.LMS_BOX, new Runnable() {
			@Override
			public void run() {
				int resizeX = 0; // 0 = BIG 37 = SMALL
				int moveFirst = 0; // 0 = BIG 59 = SMALL
				int moveSecond = 0; // 0 = BIG 24 = SMALL
				int moveThird = 0; // 0 = BIG 28 = SMALL
				if (Client.variousSettings[ConfigCodes.LMS_BOX] == 1) {
					resizeX = 37;
					moveFirst = 65;
					moveSecond = 24;
					moveThird = 28;
				}

				RSInterface.interfaceCache[66112].width = 151 - resizeX;
				RSInterface.interfaceCache[66113].width = 151 - resizeX;

				tab.child(0, 66111, 347 + resizeX, 5);
				tab.child(1, 66111, 500, 5);
				tab.child(2, 66112, 349 + resizeX, 5);
				tab.child(3, 66112, 349 + resizeX, 63);
				tab.child(4, 66113, 349 + resizeX, 7);
				tab.child(5, 66114, 355 + resizeX, 14);
				tab.child(6, 66115, 355 + resizeX, 30);
				tab.child(7, 66116, 355 + resizeX, 46);
				tab.child(8, 66117, 384 + moveFirst, 14);
				tab.child(9, 66118, 394 + moveSecond, 30);
				tab.child(10, 66119, 386 + moveThird, 46);
			}
		});
		
		addRectangle(66111, 100, 0x969696, true, 2, 60, 0);
		addRectangle(66112, 100, 0x969696, true, 151 - resizeX, 2, 0);
		addRectangle(66113, 50, 0x464628, true, 151 - resizeX, 56, 0);
		addNewText(66114, "Rank:", 40, 10, 0, 0xffffff, 0, true, 0, 1, 0);
		addNewText(66115, "Coffer:", 40, 10, 0, 0xffffff, 0, true, 0, 1, 0);
		addNewText(66116, "Mode:", 40, 10, 0, 0xffffff, 0, true, 0, 1, 0);
		addNewText(66117, "Noob", 40, 10, 0, 0xffffff, 0, true, 0, 1, 0);
		addNewText(66118, "Empty", 40, 10, 0, 0xffffff, 0, true, 0, 1, 0);
		addNewText(66119, "None", 40, 10, 0, 0xffffff, 0, true, 0, 1, 0);
		addNewText(66120, "mins", 40, 10, 0, 0xffffff, 0, true, 1, 1, 0);
		addNewText(66121, "0", 40, 10, 0, 0xffffff, 0, true, 2, 1, 0);
		
		tab.totalChildren(13);
		tab.child(0, 66111, 347 + resizeX, 5);
		tab.child(1, 66111, 500, 5);
		tab.child(2, 66112, 349 + resizeX, 5);
		tab.child(3, 66112, 349 + resizeX, 63);
		tab.child(4, 66113, 349 + resizeX, 7);
		tab.child(5, 66114, 355 + resizeX, 14);
		tab.child(6, 66115, 355 + resizeX, 30);
		tab.child(7, 66116, 355 + resizeX, 46);
		tab.child(8, 66117, 384 + moveFirst, 14);
		tab.child(9, 66118, 394 + moveSecond, 30);
		tab.child(10, 66119, 386 + moveThird, 46);
		tab.child(11, 66120, 465, 320);
		tab.child(12, 66121, 431, 320);
	}

	public static void cleanupEdit() {
		addSprite(66105, 1208);

		RSInterface tab = RSInterface.interfaceCache[14210];
		tab.width = 512;
		tab.height = 334;
		tab.totalChildren(3);
		tab.child(0, 66105, 316, 0);
		tab.child(1, 14211, 343, 15);
		tab.child(2, 14209, 458, 15);
	}

	public static void popularHouses() {
		RSInterface tab = addInterface(66000, InterfaceType.MAIN);
		RSInterface scroll = addInterface(66050, InterfaceType.MAIN);

		addSprite(66001, 702);
		addSprite(66002, 853, 250, 6);
		RSInterface.interfaceCache[66002].setFlag(TILING_FLAG_BIT);
		RSInterface.interfaceCache[66002].flipVertical();
		addNewText(66003, "Popular Houses", 475, 20, 2, 0xffffff, 0, false, 1, 1, 0);
		addButton(66004, 563, 564, "Refresh List");
		addNewText(66005, "Refresh", 71, 31, 1, 0xffffff, 0, false, 1, 1, 0);
		addButton(66006, 551, 552, "Change Title");
		addNewText(66007, "Change Title", 91, 31, 1, 0xffffff, 0, false, 1, 1, 0);

		tab.totalChildren(10);
		tab.child(0, 66001, 15, 15);
		tab.child(1, 66002, 22, 271);
		tab.child(2, 66002, 238, 271);
		tab.child(3, 66003, 20, 25);
		tab.child(4, 66004, 405, 279);
		tab.child(5, 66005, 405, 279);
		tab.child(6, 66006, 175, 279);
		tab.child(7, 66007, 175, 279);
		tab.child(8, 48052, 475, 27);
		tab.child(9, 66050, 23, 54);

		int loops = 11;
		int multiplier = 2;
		int toggleId = 64562;
		int configId = ConfigCodes.HIGHER_HITMARKS;
		int childs = loops * multiplier;

		int Id = 66051;
		for (int i = 0; i < 10; i++) {
			int spriteId = i % 2 == 0 ? 1199 : 1200;
			addButton(Id++, spriteId, 1198, "Enter House");
			addRectangle(Id++, 100, 0x3f542c, true, 90, 26, 0);// 0xaf, 0xaf0000
																// red //
																// 0xFF8C00
																// orange //
																// 0x999900
																// yellow
																// //0x3f542c
																// green
			addSprite(Id++, 844);
			addNewText(Id++, "26", 20, 30, 3, 0xffffff, 0, false, 0, 1, 0);// 0xff9b00
			addNewText(Id++, "Julius's House | Gilded Altar | Box Gamble | Chilling!", 365, 30, 1,
					0xffffff, 0, false, 0, 1, 0);
		}

		Id = 66051;
		int childID = 0;
		scroll.totalChildren(10 * 5);
		int y = 0;
		for (int i = 0; i < 10; i++) {
			scroll.child(childID++, Id++, 0, y);
			scroll.child(childID++, Id++, 380, y + 2);
			scroll.child(childID++, Id++, 393, y - 1);
			scroll.child(childID++, Id++, 420, y);
			scroll.child(childID++, Id++, 5, y);
			y += 30;
		}

		scroll.scrollMax = 10 * 30;
		scroll.height = 216;
		scroll.width = 455;
	}

	public static void displayName() {
		RSInterface tab = addInterface(64800, InterfaceType.TAB);

		addNewText(64801, "Display name", 177, 20, 1, 0xff9900, 0, true, 1, 1, 0);

		// corners
		addSprite(64802, 856);
		addSprite(64803, 856);
		RSInterface.interfaceCache[64803].flipHorizontal();
		addSprite(64804, 855);
		addSprite(64805, 855);
		RSInterface.interfaceCache[64805].flipHorizontal();

		// lines
		addSprite(64806, 854, 6, 170);
		RSInterface.interfaceCache[64806].setFlag(TILING_FLAG_BIT);
		addSprite(64807, 854, 6, 170);
		RSInterface.interfaceCache[64807].flipHorizontal();
		RSInterface.interfaceCache[64807].setFlag(TILING_FLAG_BIT);
		addSprite(64808, 853, 150, 6);
		RSInterface.interfaceCache[64808].setFlag(TILING_FLAG_BIT);
		addSprite(64809, 853, 160, 6);
		RSInterface.interfaceCache[64809].setFlag(TILING_FLAG_BIT);
		RSInterface.interfaceCache[64809].flipVertical();

		addNewText(64810, "Next free change:", 100, 15, 0, 0xff9900, 0, true, 0, 0, 0);
		addNewText(64811, "30+ days", 170, 20, 1, 0xffb83f, 0, true, 1, 0, 0);
		addNewText(64812, "Extra changes:", 77, 15, 0, 0xff9900, 0, true, 0, 0, 0);
		addNewText(64813, "0", 25, 15, 0, 0xffb83f, 0, true, 0, 0, 0);
		addNewText(64814, "Name change limit is 2 days.", 150, 15, 0, 0x808080, 0, true, 0, 0, 0);
		addNewText(64815, "Status:", 60, 20, 1, 0xff9900, 0, true, 0, 0, 0);
		addSprite(64816, 857, 155, 20);
		addNewText(64817,
				"You can always change your display name back to your regular username if you have a freechange or a extra change.",
				160, 50, 0, 0x808080, 0, true, 0, 0, 0);
		addButton(64818, 852, 851, "Look up name");
		addSprite(64819, 851);
		addButton(64820, 849, 850, "Redeem extra change");
		addNewText(64821, "Look up name", 114, 25, 1, 0xff9900, 0, true, 1, 1, 0);
		addNewText(64822, "---", 114, 25, 1, 0x808080, 0, true, 1, 1, 0);
		addNewText(64823, "Already taken"/* Not taken / Name rejected */, 157, 20, 0, 0x808080/* 0xffffff */, 0, true,
				1, 1, 0);
		addItemSprite(64824, 786, 0, 0);

		tab.totalChildren(26);
		tab.child(0, 64801, 3, 2);
		tab.child(1, 64802, 3, 24);
		tab.child(2, 64803, 155, 24);
		tab.child(3, 64804, 3, 223);// 223
		tab.child(4, 64805, 155, 223);
		tab.child(5, 64506, 171, 5); // close button
		tab.child(6, 64806, 3, 24 + 32);
		tab.child(7, 64807, 181, 24 + 32);
		tab.child(8, 64808, 10, 24);
		tab.child(9, 64809, 14, 249);
		tab.child(10, 64810, 15, 35);
		tab.child(11, 64811, 9, 50);
		tab.child(12, 64812, 15, 70);
		tab.child(13, 64813, 92, 71);
		tab.child(14, 64814, 15, 82);
		tab.child(15, 64815, 15, 96);
		tab.child(16, 64816, 15, 116);
		tab.child(17, 64817, 15, 141);
		tab.child(18, 64818, 13, 191);
		tab.child(19, 64819, 13, 219);
		tab.child(20, 64820, 130, 191);
		tab.child(21, 64821, 13, 191);
		tab.child(22, 64822, 13, 219);
		tab.child(23, 64823, 15, 116);
		tab.child(24, 64824, 138, 201);
		tab.child(25, 64830, 13, 219);

		RSInterface nameChange = addInterface(64830, InterfaceType.TAB);

		addButton(64831, 852, 851, "Confirm name");
		addNewText(64832, "Michael"/* username */, 114, 25, 1, 0x00ff00, 0, true, 1, 1, 0);

		nameChange.totalChildren(2);
		nameChange.child(0, 64831, 0, 0);
		nameChange.child(1, 64832, 0, 0);

		nameChange.hidden = true;
	}

	public static void soulplayToggles() {
		RSInterface tab = addInterface(64500, InterfaceType.TAB);
		RSInterface scroll = addInterface(64550, InterfaceType.TAB);

		addNewText(64505, "SoulPlay Toggles", 167, 20, 2, 0xff9900, 0, true, 1, 1, 0);
		addButton(64506, 300, 301, "Close");

		tab.totalChildren(11);
		tab.child(0, 64505, 3, 2);
		tab.child(1, 64802, 3, 24);
		tab.child(2, 64803, 140, 24);
		tab.child(3, 64804, 3, 223);
		tab.child(4, 64805, 140, 223);
		tab.child(5, 64806, 3, 24 + 32);
		tab.child(6, 64807, 166, 24 + 32);
		tab.child(7, 64808, 8, 24);
		tab.child(8, 64809, 8, 249);
		tab.child(9, 64506, 171, 5); // close button
		tab.child(10, 64550, 9, 30);

		addNewText(64551, "562 Hitmarks", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64552, "562 HP Bar", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64553, "X10 Hits", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64554, "Display Timers", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64555, "Render All Floors", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64556, "Particles", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64557, "HD Fog", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64558, "Hover Cursors", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64559, "HD Textures", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64560, "Display FPS", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64561, "Display Ping", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64562, "Display Timeplayed", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64563, "Ground Decorations", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64564, "Combatbox", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);
		addNewText(64565, "Smooth Animations", 132, 20, 1, 0xff9900, 0xffffff, true, 0, 1, 0);

		int loops = 15;
		int multiplier = 2;
		int toggleId = 64566;
		int configId = ConfigCodes.HIGHER_HITMARKS;
		int childs = loops * multiplier;

		scroll.totalChildren(loops * multiplier + 15);
		for (int i = 0; i < loops; i++) {

			addSprite(toggleId + i * multiplier, 857, 157, 20);
			mouseOverHover(toggleId + i * multiplier);
			addToggleButton(toggleId + i * multiplier + 1, 980, 981, configId, 150, 18, "Toggle");
			configId++;

			scroll.child(i * multiplier, toggleId + i * multiplier, 0, 2 + (i * 20));
			scroll.child(i * multiplier + 1, toggleId + i * multiplier + 1, 5, 4 + (i * 20));
		}

		scroll.child(childs, 64551, 30, 2);
		scroll.child(childs + 1, 64552, 30, 24);
		scroll.child(childs + 2, 64553, 30, 44);
		scroll.child(childs + 3, 64554, 30, 64);
		scroll.child(childs + 4, 64555, 30, 84);
		scroll.child(childs + 5, 64556, 30, 104);
		scroll.child(childs + 6, 64557, 30, 124);
		scroll.child(childs + 7, 64558, 30, 144);
		scroll.child(childs + 8, 64559, 30, 164);
		scroll.child(childs + 9, 64560, 30, 184);
		scroll.child(childs + 10, 64561, 30, 204);
		scroll.child(childs + 11, 64562, 30, 224);
		scroll.child(childs + 12, 64563, 30, 244);
		scroll.child(childs + 13, 64564, 30, 264);
		scroll.child(childs + 14, 64565, 30, 284);

		scroll.scrollMax = loops * 20 + 2;
		scroll.width = 162;
		scroll.height = 219;
	}

	public static RSInterface scrollLayer;

	public static void settingsTab() {
		RSInterface tab = addInterface(64000, InterfaceType.TAB);
		RSInterface energy = interfaceCache[149];
		energy.disabledColor = 0xff9933;

		addRadioButton(64001, 64000, 84, 85, 40, 40, "Display", 0, 5, 6450);
		addRadioButton(64002, 64000, 84, 85, 40, 40, "Audio", 1, 5, 6450);
		addRadioButton(64003, 64000, 84, 85, 40, 40, "Chat", 2, 5, 6450);
		addRadioButton(64004, 64000, 84, 85, 40, 40, "Controls", 3, 5, 6450);
		addButton(64005, 84, "SoulPlay Toggles");
		addToggleButton(64006, 84, 85, 173, 40, 40, "Toggle Run");
		addButton(64007, 84, "View House Options");
		addToggleButton(64008, 84, 85, ConfigCodes.OPEN_GL_ENABLED, 40, 40, "Switch To OpenGL");
		addSprite(64009, 339, 40, 40);
		mouseOverHover(64009);
		addSprite(64010, 78, 40, 40);
		mouseOverHover(64010);
		addSprite(64011, 828, 40, 40);
		mouseOverHover(64011);
		addSprite(64012, 829, 40, 40);
		mouseOverHover(64012);
		addSprite(64013, 842);
		addSprite(64014, 82);
		addSprite(64015, 707);
		addNewText(64016, "HD", 40, 10, 0, 0xffffff, 0, true, 1, 0, 0);
		addSprite(64017, 484);
		RSInterface.interfaceCache[64017].flipHorizontal();
		addSprite(64018, 484);
		addSprite(64019, 858);
		RSInterface.interfaceCache[64019].flipVertical();
		addSprite(64020, 858);

		tab.totalChildren(29);
		tab.child(0, 64001, 6, 2);
		tab.child(1, 64002, 51, 2);
		tab.child(2, 64003, 96, 2);
		tab.child(3, 64004, 141, 2);
		tab.child(4, 64005, 6, 220);
		tab.child(5, 64006, 51, 220);
		tab.child(6, 64007, 96, 220);
		tab.child(7, 64008, 141, 220);
		tab.child(8, 64009, 6, 2);
		tab.child(9, 64010, 51, 2);
		tab.child(10, 64011, 96, 2);
		tab.child(11, 64012, 141, 2);
		tab.child(12, 64013, 10, 223);
		tab.child(13, 64014, 63, 224);
		tab.child(14, 149, 56, 242);
		tab.child(15, 64015, 100, 224);
		tab.child(16, 64016, 141, 234);
		tab.child(17, 64017, 0, 45);
		tab.child(18, 64017, 0, 108);
		tab.child(19, 64018, 180, 45);
		tab.child(20, 64018, 180, 108);
		tab.child(21, 64019, 5, 44);
		tab.child(22, 64019, 80, 44);
		tab.child(23, 64020, 5, 212);
		tab.child(24, 64020, 80, 212);
		tab.child(25, 64030, 5, 49);
		tab.child(26, 64100, 5, 49);
		tab.child(27, 64200, 5, 49);
		tab.child(28, 64300, 5, 49);

		RSInterface screen = addInterface(64030, InterfaceType.TAB);

		addRadioButton(64031, 64030, 830, 831, 62, 55, "Fixed mode", 1, 5, 6454);
		addRadioButton(64032, 64030, 830, 831, 62, 55, "Resizeable mode", 2, 5, 6454);
		addHoverOnly(64033, 973, 974, 974, 62, 55, 1, 0, 6454);
		addHoverOnly(64034, 975, 976, 976, 62, 55, 2, 0, 6454);
		addButton(64035, 104, 105, "Advanced Options");
		addNewText(64036, "Advanced Options", 150, 25, 1, 0xb6b6b6, 0xffffff, true, 1, 1, 1);
		addSprite(64037, 832, 30, 30);
		addToggleButton(64038, 832, 833, ConfigCodes.WHEEL_SCROLLING, 30, 30, "Disable Zooming");
		addSprite(64039, 76, 30, 30);
		int screenId = 64040;
		int screenButtons = 4;
		int screenSprite = 86;
		screen.totalChildren(screenButtons + 10);
		for (int i = 0; i < screenButtons; i++) {
			addRadioButton(screenId + i, 64030, screenSprite + (i * 2), screenSprite + ((i * 2) + 1), 32, 16,
					"Adjust Screen Brightness", i + 1, 5, 166);
			screen.child(i + 9, screenId + i, 9 + ((i + 1) * 32), 46);
		}
		scrollLayer = addLayer(screenId + screenButtons, 128, 16);
		scrollLayer.createDynamic(0, RSInterface.addSprite(0, 0, 32, 16, 86, -1, null));
		scrollLayer.createDynamic(1, RSInterface.addSprite(32, 0, 32, 16, 88, -1, null));
		scrollLayer.createDynamic(2, RSInterface.addSprite(64, 0, 32, 16, 90, -1, null));
		scrollLayer.createDynamic(3, RSInterface.addSprite(96, 0, 32, 16, 92, -1, null));
		scrollLayer.createDynamic(4, RSInterface.addSprite(0, 0, 16, 16, 834, -1, null));
		scrollLayer.onMouseClickRepeat = new MouseEvent() {
			@Override
			public void run() {
				if (scrollLayer == null) {
					return;
				}

				if (mouseX == -1) {
					return;
				}

				RSInterface component = scrollLayer.dynamicComponents[4];
				if (mouseX + component.width > scrollLayer.width) {
					mouseX = scrollLayer.width - component.width;
				}

				scrollLayer.dynamicComponents[4].x = mouseX;
				int interpolant = Misc.interpolate(201, 2100, 0, scrollLayer.width - component.width, mouseX);
				Client.cameraZoom = interpolant;
				Client.cameraZoom = Math.min(Math.max(Client.cameraZoom, 251), 2000);
				// System.out.println(mouseX+":"+interpolant);
			}
		};

		screen.child(0, 64031, 17, 73);
		screen.child(1, 64032, 95, 73);
		screen.child(2, 64033, 16, 73);
		screen.child(3, 64034, 96, 73);
		screen.child(4, 64035, 13, 132);
		screen.child(5, 64036, 13, 132);
		screen.child(6, 64037, 5, 5);
		screen.child(7, 64038, 5, 5);
		screen.child(8, 64039, 5, 38);
		screen.child(screenButtons + 9, screenId + screenButtons, 40, 10);
		screen.hidden = false;

		RSInterface audio = addInterface(64100, InterfaceType.TAB);

		addSprite(64101, 77, 30, 30);
		addToggleButton(64102, 77, 833, 6456, 30, 30, "Disable Music");
		addSprite(64103, 78, 30, 30);
		addToggleButton(64104, 78, 833, 6457, 30, 30, "Disable Sound Effects");

		int audioId = 64105;
		int audioButtons = 5;
		int audiSprite = 94;
		int idMultiplier = 2;

		audio.totalChildren(audioButtons * idMultiplier + 4);
		for (int i = 0; i < audioButtons; i++) {

			addRadioButton(audioId + i * idMultiplier, 64100, audiSprite + (i * 2), audiSprite + ((i * 2) + 1), 32, 16,
					"Adjust Music Volume", 4 - i, 5, 168);
			addRadioButton(audioId + i * idMultiplier + 1, 64100, audiSprite + (i * 2), audiSprite + ((i * 2) + 1), 32,
					16, "Adjust Sound Effect Volume", 4 - i, 5, 169);

			audio.child(4 + i * idMultiplier, audioId + i * 2, 15 + ((i + 1) * 26), 39);
			audio.child(4 + i * idMultiplier + 1, audioId + i * 2 + 1, 15 + ((i + 1) * 26), 103);
		}

		audio.child(0, 64101, 5, 30);
		audio.child(1, 64102, 5, 30);
		audio.child(2, 64103, 2, 90);
		audio.child(3, 64104, 2, 90);
		audio.hidden = true;

		RSInterface chat = addInterface(64200, InterfaceType.TAB);

		addToggleButton(64201, 85, 84, 171, 40, 40, "Toggle Chat Effects");
		addToggleButton(64202, 84, 85, 287, 40, 40, "Toggle Split Private Chat");
		addToggleButton(64203, 84, 85, 6462, 40, 40, "Toggle Hide Private Chat");
		addToggleButton(64204, 84, 85, ConfigCodes.PROFAINTY_FILTER, 40, 40, "Toggle Profainty Filter");
		addButton(64205, 84, "Notifications");
		addToggleButton(64206, 84, 85, 6464, 40, 40, "Toggle Login/Logout Notification Timeout");
		addButton(64207, 705, "Display Name");
		addSprite(64208, 80);
		addSprite(64209, 79);
		addSprite(64210, 837);
		addSprite(64211, 836);
		addSprite(64212, 838);
		addSprite(64213, 839);
		addNewText(64214, "Change Display Name", 166, 32, 1, 0xff9900, 0xffffff, true, 1, 1, 1);

		chat.totalChildren(14);
		chat.child(0, 64201, 13, 15);
		chat.child(1, 64202, 66, 15);
		chat.child(2, 64203, 120, 15);
		chat.child(3, 64204, 13, 65);
		chat.child(4, 64205, 66, 65);
		chat.child(5, 64206, 120, 65);
		chat.child(6, 64207, 5, 115);
		chat.child(7, 64208, 17, 22);
		chat.child(8, 64209, 70, 22);
		chat.child(9, 64210, 124, 19);
		chat.child(10, 64211, 17, 69);
		chat.child(11, 64212, 71, 68);
		chat.child(12, 64213, 125, 69);
		chat.child(13, 64214, 5, 115);
		chat.hidden = true;

		RSInterface controls = addInterface(64300, InterfaceType.TAB);

		addButton(64301, 84, "Keybinding");
		addToggleButton(64302, 84, 85, ConfigCodes.SHIFT_DROP, 40, 40, "Toggle Shift Click Drop");
		addToggleButton(64303, 85, 84, ConfigCodes.MIDDLE_MOUSE_MOVE, 40, 40, "Toggle Mouse Camera");
		addToggleButton(64304, 84, 85, ConfigCodes.PET_OPTION_PRIORITY, 40, 40, "Toggle Follower Options Priority");
		addSprite(64305, 83);
		addSprite(64306, 835);
		addSprite(64307, 840);
		addSprite(64308, 841);
		addNewText(64309, "Player 'Attack' options:", 166, 32, 1, 0xff9900, 0, true, 0, 0, 1);
		addNewText(64310, "NPC 'Attack' options:", 166, 32, 1, 0xff9900, 0, true, 0, 0, 1);
		final RSInterface optionLayerPlayer = addLayer(64314, 250, 335);
		final RSInterface optionLayerNPC = addLayer(64312, 250, 335);
		final RSInterface menuLayerPlayer = addLayer(64311, 166, 20);
		final RSInterface menuLayerNPC = addLayer(64313, 166, 20);
		optionLayerPlayer.hidden = true;
		optionLayerNPC.hidden = true;

		controls.totalChildren(14);
		controls.child(0, 64301, 5, 5);
		controls.child(1, 64302, 47, 5);
		controls.child(2, 64303, 89, 5);
		controls.child(3, 64304, 131, 5);
		controls.child(4, 64305, 12, 13);
		controls.child(5, 64306, 51, 9);
		controls.child(6, 64307, 93, 8);
		controls.child(7, 64308, 135, 9);
		controls.child(8, 64309, 5, 55);
		controls.child(9, 64310, 5, 110);
		controls.child(10, 64311, 5, 74);
		controls.child(11, 64313, 5, 129);
		controls.child(12, 64312, 0, 0);
		controls.child(13, 64314, 0, 0);
		controls.hidden = true;

		String[] playerOps = { "Depends on combat levels", "Always right-click", "Left-click where available", "Hidden", "Left-click opponent only" };
		String[] npcOps = { "Depends on combat levels", "Always right-click", "Left-click where available", "Hidden" };
		Dropdown.makeDropdown(menuLayerPlayer, optionLayerPlayer, playerOps, 0, 0, Client.playerAttackOptionSetting.getId());
		Dropdown.makeDropdown(menuLayerNPC, optionLayerNPC, npcOps, 0, 1, Client.npcAttackOptionSetting.getId());
	}

	public static void mouseOverHover(final int interfaceId) {
		RSInterface.interfaceCache[interfaceId].onMouseOverEnter = new MouseEvent() {
			@Override
			public void run() {
				RSInterface.interfaceCache[interfaceId].transparency = 100;
			}
		};
		RSInterface.interfaceCache[interfaceId].onMouseLeave = new MouseEvent() {
			@Override
			public void run() {
				RSInterface.interfaceCache[interfaceId].transparency = 0;
			}
		};
	}

	public static void hotkeys() {
		RSInterface tab = addInterface(50000, InterfaceType.MAIN);

		addSprite(50001, 880);
		addSprite(50002, 996);
		RSInterface.interfaceCache[50002].atActionType = 3;

		int[] spriteIds = { 881, 898, 896, 906, 878, 908, 883, 876, 900, 904, 891, 990, 1310, 994 };
		int length = spriteIds.length;
		for (int i = 0; i < length; i++) {
			addSprite(50003 + i, spriteIds[i]);
		}

		addSprite(50003 + length, 902);
		addSprite(50004 + length, 980);
		addNewText(50005 + length, "Esc closes current interface", 300, 30, 1, 0xFF8A1F, 0, true, 0, 0, 0);

		tab.totalChildren(5 + length);
		tab.child(0, 50001, 0, 0);
		tab.child(1, 50002, 474, 7);
		int x = 15;
		int y = 38;
		int i = 0;
		while (i < length) {
			tab.child(2 + i, 50003 + i, x, y);
			y += 44;
			i++;
			if (i % 5 == 0) {
				x += 160;
				y = 38;
			}
		}
		tab.child(2 + length, 50003 + length, 335, 255);
		tab.child(3 + length, 50004 + length, 20, 265);
		tab.child(4 + length, 50005 + length, 45, 265);
	}

	public static void dungSpellBook() {
		int air = 17780;
		int mind = 17784;
		int water = 17781;
		int earth = 17782;
		int fire = 17783;
		int body = 17788;
		int cosmic = 17789;
		int chaos = 17785;
		int astral = 17790;
		int nature = 17791;
		int law = 17792;
		int death = 17786;
		int blood = 17787;
		int soul = 17793;

		InterfaceTargetMask useOnNpcMask = new InterfaceTargetMask().useOnNpc();
		InterfaceTargetMask useOnPlayerMask = new InterfaceTargetMask().useOnPlayer();
		InterfaceTargetMask useOnInvMask = new InterfaceTargetMask().useOnInventory();

		RSInterface tab = addInterface(62000, InterfaceType.TAB);

		addSprite(62001, 109);
		addSpellData(62001, 62047, SpriteCache.get(109), SpriteCache.get(109), 1, "Dungeon Home Teleport",
				"Teleport to the starting<br>room of the dungeon.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(62002, "magicon", 0);
		addSpellData(62002, 62047, getSprite(0, aClass44, "magicoff", mediaIndex), getSprite(0, aClass44, "magicon", mediaIndex), 1,
				"Wind Strike", "A basic Air missile.", mind, 1, air, 1, -1, -1, -1, -1, useOnNpcMask, true);
		addSpriteCache(62003, "magicon", 1);
		addSpellData(62003, 62047, getSprite(1, aClass44, "magicoff", mediaIndex), getSprite(1, aClass44, "magicon", mediaIndex), 3, "Confuse",
				"Reduces the target's chance to hit by 5% for 1 minute.", body, 1, earth, 2, water, 3, -1, -1,
				useOnNpcMask, false);
		addSpriteCache(62004, "magicon", 2);
		addSpellData(62004, 62047, getSprite(2, aClass44, "magicoff", mediaIndex), getSprite(2, aClass44, "magicon", mediaIndex), 5,
				"Water Strike", "A basic Water missile.", mind, 1, water, 1, air, 1, -1, -1, useOnNpcMask, true);
		addSpriteCache(62005, "magicon", 4);
		addSpellData(62005, 62047, getSprite(4, aClass44, "magicoff", mediaIndex), getSprite(4, aClass44, "magicon", mediaIndex), 9,
				"Earth Strike", "A basic Earth missile.", mind, 1, earth, 2, air, 1, -1, -1, useOnNpcMask, true);
		addSpriteCache(62006, "magicon", 5);
		addSpellData(62006, 62047, getSprite(5, aClass44, "magicoff", mediaIndex), getSprite(5, aClass44, "magicon", mediaIndex), 11, "Weaken",
				"Reduces the target's damage dealt by 5% for 1 minute.", body, 1, earth, 2, water, 3, -1, -1,
				useOnNpcMask, false);
		addSpriteCache(62007, "magicon", 6);
		addSpellData(62007, 62047, getSprite(6, aClass44, "magicoff", mediaIndex), getSprite(6, aClass44, "magicon", mediaIndex), 13,
				"Fire Strike", "A basic Earth missile.", mind, 1, fire, 3, air, 2, -1, -1, useOnNpcMask, true);
		addSpriteCache(62008, "magicon", 7);
		addSpellData(62008, 62047, getSprite(7, aClass44, "magicoff", mediaIndex), getSprite(7, aClass44, "magicon", mediaIndex), 15,
				"Bones To Bananas", "Converts all bones in the inventory into bananas.", nature, 1, earth, 2, water, 2,
				-1, -1, useOnInvMask, false);
		addSpriteCache(62009, "magicon", 8);
		addSpellData(62009, 62047, getSprite(8, aClass44, "magicoff", mediaIndex), getSprite(8, aClass44, "magicon", mediaIndex), 17,
				"Wind Bolt", "A low level air missile.", chaos, 1, air, 2, -1, -1, -1, -1, useOnNpcMask, true);
		addSpriteCache(62010, "magicon", 9);
		addSpellData(62010, 62047, getSprite(9, aClass44, "magicoff", mediaIndex), getSprite(9, aClass44, "magicon", mediaIndex), 19, "Curse",
				"Increases the target's damage received by 5% for 1 minute.", body, 1, earth, 2, water, 3, -1, -1,
				useOnNpcMask, false);
		addSpriteCache(62011, "magicon2", 0);// bind
		addSpellData(62011, 62047, getSprite(0, aClass44, "magicoff2", mediaIndex), getSprite(0, aClass44, "magicon2", mediaIndex), 20, "Bind",
				"Prevents creatures from moving for 12 seconds, or players for 6 seconds.", nature, 2, earth, 3, water,
				2, -1, -1, useOnNpcMask, false);
		addSpriteCache(62012, "magicon", 10);// low level alch
		addSpellData(62012, 62047, getSprite(10, aClass44, "magicoff", mediaIndex), getSprite(10, aClass44, "magicon", mediaIndex), 21,
				"Low Level Alchemy", "Converts the targeted item into coins.", nature, 1, fire, 3, -1, -1, -1, -1,
				useOnInvMask, false);
		addSpriteCache(62013, "magicon", 11);// water bolt
		addSpellData(62013, 62047, getSprite(11, aClass44, "magicoff", mediaIndex), getSprite(11, aClass44, "magicon", mediaIndex), 23,
				"Water Bolt", "A low level water missile.", chaos, 1, water, 2, air, 2, -1, -1, useOnNpcMask, true);
		addSpriteCache(62014, "magicon", 14);// earth bolt
		addSpellData(62014, 62047, getSprite(14, aClass44, "magicoff", mediaIndex), getSprite(14, aClass44, "magicon", mediaIndex), 29,
				"Earth Bolt", "A low level earth missile.", chaos, 1, earth, 3, air, 2, -1, -1, useOnNpcMask, true);
		addSprite(62015, 811);// Create a one-use green gatestone
		addSpellData(62015, 62047, SpriteCache.get(812), SpriteCache.get(811), 32, "Create Gatestone",
				"Create a one-use green gatestone that can be dropped on the ground, providing the destination of the Gatestone Teleport spell.",
				cosmic, 3, -1, -1, -1, -1, -1, -1, null, false);
		addSprite(62016, 809);// Teleport the caster to the location of the
								// green gatestone from Create Gatestone,
								// destroying it.
		addSpellData(62016, 62047, SpriteCache.get(810), SpriteCache.get(809), 32, "Gatestone Teleport",
				"Teleport the caster to the location of the green gatestone from Create Gatestone, destroying it.", -1,
				-1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(62017, "magicon", 17);// fire bolt
		addSpellData(62017, 62047, getSprite(17, aClass44, "magicoff", mediaIndex), getSprite(17, aClass44, "magicon", mediaIndex), 35,
				"Fire Bolt", "A low level fire missile.", chaos, 1, fire, 3, air, 3, -1, -1, useOnNpcMask, true);
		addSpriteCache(62018, "magicon", 20);// air blast
		addSpellData(62018, 62047, getSprite(20, aClass44, "magicoff", mediaIndex), getSprite(20, aClass44, "magicon", mediaIndex), 41,
				"Wind Blast", "A medium level air missile.", death, 1, air, 3, -1, -1, -1, -1, useOnNpcMask, true);
		addSpriteCache(62019, "magicon", 23);// water blast
		addSpellData(62019, 62047, getSprite(23, aClass44, "magicoff", mediaIndex), getSprite(23, aClass44, "magicon", mediaIndex), 47,
				"Water Blast", "A medium level water missile.", death, 1, water, 3, air, 3, -1, -1, useOnNpcMask, true);
		addSpriteCache(62020, "magicon2", 1);// snare
		addSpellData(62020, 62047, getSprite(1, aClass44, "magicoff2", mediaIndex), getSprite(1, aClass44, "magicon2", mediaIndex), 50, "Snare",
				"Prevents creatures from moving for 18 seconds, or players for 9 seconds.", nature, 3, earth, 4, water,
				4, -1, -1, useOnNpcMask, false);
		addSpriteCache(62021, "magicon", 25);// earth blast
		addSpellData(62021, 62047, getSprite(25, aClass44, "magicoff", mediaIndex), getSprite(25, aClass44, "magicon", mediaIndex), 53,
				"Earth Blast", "A medium level earth missile.", death, 1, earth, 4, air, 3, -1, -1, useOnNpcMask, true);
		addSpriteCache(62022, "magicon", 26);// high level alch
		addSpellData(62022, 62047, getSprite(26, aClass44, "magicoff", mediaIndex), getSprite(26, aClass44, "magicon", mediaIndex), 55,
				"High Level Alchemy", "Converts the targeted item into even more coins", nature, 1, fire, 5, -1, -1, -1,
				-1, useOnInvMask, false);
		addSpriteCache(62023, "magicon", 29);// fire blast
		addSpellData(62023, 62047, getSprite(29, aClass44, "magicoff", mediaIndex), getSprite(29, aClass44, "magicon", mediaIndex), 59,
				"Fire Blast", "A medium level fire missile.", death, 1, fire, 5, air, 4, -1, -1, useOnNpcMask, true);
		addSpriteCache(62024, "magicon", 31);// air wave
		addSpellData(62024, 62047, getSprite(31, aClass44, "magicoff", mediaIndex), getSprite(31, aClass44, "magicon", mediaIndex), 62,
				"Wind Wave", "A high level air missile.", blood, 1, air, 5, -1, -1, -1, -1, useOnNpcMask, true);
		addSprite(62025, 809);// group gatestone teleport
		addSpellData(62025, 62047, SpriteCache.get(814), SpriteCache.get(813), 64, "Group Gatestone Teleport",
				"Teleports to the party gatestone.", law, 3, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(62026, "magicon", 33);// water wave
		addSpellData(62026, 62047, getSprite(33, aClass44, "magicoff", mediaIndex), getSprite(33, aClass44, "magicon", mediaIndex), 65,
				"Water Wave", "A high level water missile.", blood, 1, water, 7, air, 5, -1, -1, useOnNpcMask, true);
		addSpriteCache(62027, "magicon", 41);// vuln
		addSpellData(62027, 62047, getSprite(41, aClass44, "magicoff", mediaIndex), getSprite(41, aClass44, "magicon", mediaIndex), 66,
				"Vulnerability", "Increases the target's damage received by 10% for 1 minute.", soul, 1, earth, 5,
				water, 5, -1, -1, useOnNpcMask, false);
		addSprite(62028, 481);// examine monster
		addSpellData(62028, 62047, SpriteCache.get(417), SpriteCache.get(481), 66, "Monster Examine",
				"Examine the stats of a target monster.", cosmic, 1, astral, 1, mind, 1, -1, -1, useOnNpcMask, false);
		addSprite(62029, 485);// cure other
		addSpellData(62029, 62047, SpriteCache.get(421), SpriteCache.get(485), 68, "Cure Other",
				"Cure a target of poison.", law, 1, astral, 1, earth, 10, -1, -1, useOnPlayerMask, false);
		addSprite(62030, 487);// humidify
		addSpellData(62030, 62047, SpriteCache.get(423), SpriteCache.get(487), 68, "Humidify",
				"Fill containers with water.", astral, 1, fire, 1, water, 3, -1, -1, null, false);
		addSpriteCache(62031, "magicon", 36);// earth wave
		addSpellData(62031, 62047, getSprite(36, aClass44, "magicoff", mediaIndex), getSprite(36, aClass44, "magicon", mediaIndex), 70,
				"Earth Wave", "A high level earth missile.", blood, 1, earth, 7, air, 5, -1, -1, useOnNpcMask, true);
		addSprite(62032, 489);// cure me
		addSpellData(62032, 62047, SpriteCache.get(425), SpriteCache.get(489), 71, "Cure Me",
				"Cure the caster of poison.", law, 1, cosmic, 2, astral, 2, -1, -1, null, false);
		addSpriteCache(62033, "magicon", 42);// enfeeble
		addSpellData(62033, 62047, getSprite(42, aClass44, "magicoff", mediaIndex), getSprite(42, aClass44, "magicon", mediaIndex), 73,
				"Enfeeble", "Reduces the target's damage dealt by 10% for 1 minute.", soul, 1, earth, 8, water, 8, -1,
				-1, useOnNpcMask, false);
		addSprite(62034, 493);// cure group
		addSpellData(62034, 62047, SpriteCache.get(429), SpriteCache.get(493), 74, "Cure Group",
				"Cures everyone around the caster standing in a 3-by-3 square of poison.", law, 2, cosmic, 2, astral, 2,
				-1, -1, null, false);
		addSpriteCache(62035, "magicon", 37);// fire wave
		addSpellData(62035, 62047, getSprite(37, aClass44, "magicoff", mediaIndex), getSprite(37, aClass44, "magicon", mediaIndex), 75,
				"Fire Wave", "A high level fire missile.", blood, 1, fire, 7, air, 5, -1, -1, useOnNpcMask, true);
		addSpriteCache(62036, "magicon2", 2);// entangle
		addSpellData(62036, 62047, getSprite(2, aClass44, "magicoff2", mediaIndex), getSprite(2, aClass44, "magicon2", mediaIndex), 79,
				"Entangle", "Prevents creatures from moving for 24 seconds, or players for 12 seconds.", nature, 4,
				earth, 5, water, 5, -1, -1, useOnNpcMask, false);
		addSpriteCache(62037, "magicon", 43);// stagger
		addSpellData(62037, 62047, getSprite(43, aClass44, "magicoff", mediaIndex), getSprite(43, aClass44, "magicon", mediaIndex), 80, "Stun",
				"Reduces the target's chance to hit by 10% for 1 minute.", soul, 1, earth, 12, water, 12, -1, -1,
				useOnNpcMask, false);
		addSprite(62038, 804);// air surge
		addSpellData(62038, 62047, SpriteCache.get(821), SpriteCache.get(804), 81, "Wind Surge",
				"A very powerful air spell.", blood, 1, death, 1, air, 7, -1, -1, useOnNpcMask, true);
		addSprite(62039, 805);// water surge
		addSpellData(62039, 62047, SpriteCache.get(822), SpriteCache.get(805), 85, "Water Surge",
				"A very powerful water spell.", blood, 1, death, 1, water, 10, air, 7, useOnNpcMask, true);
		addSprite(62040, 806);// earth surge
		addSpellData(62040, 62047, SpriteCache.get(823), SpriteCache.get(806), 90, "Earth Surge",
				"A very powerful earth spell.", blood, 1, death, 1, earth, 10, air, 7, useOnNpcMask, true);
		addSprite(62041, 817);// create gatestone 2
		addSpellData(62041, 62047, SpriteCache.get(818), SpriteCache.get(817), 90, "Create Gatestone 2",
				"Create a one-use red gatestone that can be dropped on the ground, providing the destination of the Gatestone Teleport 2 spell.",
				-1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSprite(62042, 819);// teleport to gatestone 2
		addSpellData(62042, 62047, SpriteCache.get(820), SpriteCache.get(819), 90, "Gatestone Teleport 2",
				"Teleport the caster to the location of the red gatestone from Create Gatestone 2, destroying it.", -1,
				-1, -1, -1, -1, -1, -1, -1, null, false);
		addSprite(62043, 525);// vengeance other
		addSpellData(62043, 62047, SpriteCache.get(469), SpriteCache.get(525), 93, "Vengeance Other",
				"Allow the target to gain power of inflicting opponent a large percentage of damage dealt to the target.",
				death, 2, astral, 3, earth, 10, -1, -1, useOnPlayerMask, false);
		addSprite(62044, 526);// vengeance
		addSpellData(62044, 62047, SpriteCache.get(471), SpriteCache.get(526), 94, "Vengeance",
				"Rebounds 75% of the damage of the next hit back to the caster's opponent.", death, 2, astral, 4, earth,
				10, -1, -1, null, false);
		addSprite(62045, 524);// vengeance group
		addSpellData(62045, 62047, SpriteCache.get(816), SpriteCache.get(815), 95, "Vengeance Group",
				"	Allows everyone in the 7x7 square around the caster to rebound 75% of the next hit's damage.",
				death, 3, astral, 4, earth, 11, -1, -1, null, false);
		addSprite(62046, 807);// fire surge
		addSpellData(62046, 62047, SpriteCache.get(808), SpriteCache.get(807), 95, "Fire Surge",
				"A very powerful fire spell.", blood, 1, death, 1, fire, 10, air, 7, useOnNpcMask, true);

		addInterface(62047);// Dynamic layer

		tab.totalChildren(47);
		tab.child(0, 62001, 13, 24);
		tab.child(1, 62002, 36, 24);
		tab.child(2, 62003, 60, 24);
		tab.child(3, 62004, 84, 24);
		tab.child(4, 62005, 108, 24);
		tab.child(5, 62006, 132, 24);
		tab.child(6, 62007, 156, 24);

		tab.child(7, 62008, 13, 48);
		tab.child(8, 62009, 36, 48);
		tab.child(9, 62010, 60, 48);
		tab.child(10, 62011, 84, 48);
		tab.child(11, 62012, 108, 48);
		tab.child(12, 62013, 132, 48);
		tab.child(13, 62014, 156, 48);

		tab.child(14, 62015, 13, 72);
		tab.child(15, 62016, 36, 72);
		tab.child(16, 62017, 60, 72);
		tab.child(17, 62018, 84, 72);
		tab.child(18, 62019, 108, 72);
		tab.child(19, 62020, 132, 72);
		tab.child(20, 62021, 156, 72);

		tab.child(21, 62022, 13, 96);
		tab.child(22, 62023, 36, 96);
		tab.child(23, 62024, 60, 96);
		tab.child(24, 62025, 84, 96);
		tab.child(25, 62026, 108, 96);
		tab.child(26, 62027, 132, 96);
		tab.child(27, 62028, 156, 96);

		tab.child(28, 62029, 13, 120);
		tab.child(29, 62030, 36, 120);
		tab.child(30, 62031, 60, 120);
		tab.child(31, 62032, 84, 120);
		tab.child(32, 62033, 108, 120);
		tab.child(33, 62034, 132, 120);
		tab.child(34, 62035, 156, 120);

		tab.child(35, 62036, 13, 144);
		tab.child(36, 62037, 36, 144);
		tab.child(37, 62038, 60, 144);
		tab.child(38, 62039, 84, 144);
		tab.child(39, 62040, 108, 144);
		tab.child(40, 62041, 132, 144);
		tab.child(41, 62042, 156, 144);

		tab.child(42, 62043, 13, 168);
		tab.child(43, 62044, 36, 168);
		tab.child(44, 62045, 60, 168);
		tab.child(45, 62046, 84, 168);
		tab.child(46, 62047, 0, 0);
	}

	private static void addSpellData(final int interfaceId, final int hoverId, final Sprite off, final Sprite on,
			final int level, final String spellName, final String desc, final int item1Id, final int item1Amount,
			final int item2Id, final int item2Amount, final int item3Id, final int item3Amount, final int item4Id,
			final int item4Amount, InterfaceTargetMask mask, boolean autocast) {
		final RSInterface component = RSInterface.interfaceCache[interfaceId];
		component.disabledSprite = off;
		if (mask == null) {
			component.atActionType = 1;
			component.tooltip = "Cast <col=00ff00>" + spellName;
		} else {
			component.targetMask = mask;
			component.atActionType = 2;
			component.targetName = spellName;
			component.targetVerb = "Cast on";
		}

		if (autocast) {
			component.setFlag(CAN_AUTOCAST);
		}

		component.onMouseOver = new MouseEvent() {
			@Override
			public void run() {
				buildSpellHover(interfaceId, hoverId, level, spellName, desc, item1Id, item1Amount, item2Id,
						item2Amount, item3Id, item3Amount, item4Id, item4Amount);
			}
		};
		component.onMouseLeave = new MouseEvent() {
			@Override
			public void run() {
				RSInterface.interfaceCache[hoverId].dynamicComponents = null;
			}
		};
		Runnable requirementsCheck = new Runnable() {
			@Override
			public void run() {
				switch (interfaceId) {
					case 62016:
						if (Client.variousSettings[ConfigCodes.HAS_GATESTONE_DROPPED] == 0) {
							component.disabledSprite = off;
						} else {
							component.disabledSprite = on;
						}
						return;
					case 30028:
					case 1215:
					case 12875:
						if (Client.variousSettings[ConfigCodes.BOUNTY_TELEPORT_UNLOCKED] == 0) {
							component.disabledSprite = off;
							return;
						}

						break;
				}

				if (Client.instance.currentStats[Skills.MAGIC] < level) {
					component.disabledSprite = off;
					return;
				}
				if (item1Id != -1 && checkItems(item1Id) < item1Amount) {
					component.disabledSprite = off;
					return;
				}
				if (item2Id != -1 && checkItems(item2Id) < item2Amount) {
					component.disabledSprite = off;
					return;
				}
				if (item3Id != -1 && checkItems(item3Id) < item3Amount) {
					component.disabledSprite = off;
					return;
				}
				if (item4Id != -1 && checkItems(item4Id) < item4Amount) {
					component.disabledSprite = off;
					return;
				}

				component.disabledSprite = on;
			}
		};
		component.addOnSkillUpdate(Skills.MAGIC, requirementsCheck);
		component.addOnInvetoryUpdate(3214, requirementsCheck);
		component.addOnVarpUpdate(ConfigCodes.INF_RUNES, requirementsCheck);
		switch (interfaceId) {
			case 62016:
				component.addOnVarpUpdate(ConfigCodes.HAS_GATESTONE_DROPPED, requirementsCheck);
				break;
			case 30028:
			case 1215:
			case 12875:
				component.addOnVarpUpdate(ConfigCodes.BOUNTY_TELEPORT_UNLOCKED, requirementsCheck);
				break;
		}
	}

	private static void buildSpellHover(int id, int hoverId, int level, String spellName, String desc, int item1Id,
			int item1Amount, int item2Id, int item2Amount, int item3Id, int item3Amount, int item4Id, int item4Amount) {
		String levelText = "Level " + level + ": " + spellName;
		int heightLevelText = Client.instance.newRegularFont.paragraphHeigth(levelText, 177) * 13 + 2;
		int descHeight = Client.instance.newSmallFont.paragraphHeigth(desc, 177) * 13;
		int finalHeight = heightLevelText + 2 + descHeight + 2;
		if (item1Id != -1) {
			finalHeight += 32 + 14;
		}
		int yPos = 5;
		if (RSInterface.interfaceCache[id].y < 130) {
			yPos = 261 - finalHeight - 5;
		}

		RSInterface background = addRectangle(5, yPos, 180, finalHeight, 0, 42, true);
		background.setFlag(RSInterface.RECT_IGNORE_ATLAS);
		RSInterface.interfaceCache[hoverId].createDynamic(0, background);
		RSInterface.interfaceCache[hoverId].createDynamic(1,
				addRectangle(6, yPos + 1, 179, finalHeight - 1, 3025699, 0, false));
		RSInterface.interfaceCache[hoverId].createDynamic(2,
				addRectangle(5, yPos, 179, finalHeight - 1, 7496785, 0, false));
		RSInterface.interfaceCache[hoverId].createDynamic(3,
				addNewText(levelText, 7, yPos + 2, 177, heightLevelText, 1, 16750623, 0, true, 1, 1, 0));
		RSInterface.interfaceCache[hoverId].createDynamic(4,
				addNewText(desc, 7, yPos + 2 + heightLevelText, 177, descHeight, 0, 11495962, 0, true, 1, 1, 0));

		int itemCount = 1;
		if (item2Id != -1) {
			itemCount = 2;
		}
		if (item3Id != -1) {
			itemCount = 3;
		}
		if (item4Id != -1) {
			itemCount = 4;
		}
		int posX = (190 - itemCount * 35) / (itemCount + 1);
		if (item1Id != -1) {
			int itemY = yPos + 2 + heightLevelText + descHeight;
			RSInterface.interfaceCache[hoverId].createDynamic(5, addItemSprite(posX, itemY, item1Id));

			int runeCount = checkItems(item1Id);
			RSInterface.interfaceCache[hoverId].createDynamic(6,
					addNewText(Client.methodR(runeCount) + "/" + item1Amount, posX, itemY + 32, 35, 14, 0,
							runeCount >= item1Amount ? 65280 : 16711680, 0, true, 1, 1, 0));
		}

		if (item2Id != -1) {
			int itemX = posX * 2 + 35;
			int itemY = yPos + 2 + heightLevelText + descHeight;
			RSInterface.interfaceCache[hoverId].createDynamic(7, addItemSprite(itemX, itemY, item2Id));

			int runeCount = checkItems(item2Id);
			RSInterface.interfaceCache[hoverId].createDynamic(8,
					addNewText(Client.methodR(runeCount) + "/" + item2Amount, itemX, itemY + 32, 35, 14, 0,
							runeCount >= item2Amount ? 65280 : 16711680, 0, true, 1, 1, 0));
		}

		if (item3Id != -1) {
			int itemX = posX * 3 + 70;
			int itemY = yPos + 2 + heightLevelText + descHeight;
			RSInterface.interfaceCache[hoverId].createDynamic(9, addItemSprite(itemX, itemY, item3Id));

			int runeCount = checkItems(item3Id);
			RSInterface.interfaceCache[hoverId].createDynamic(10,
					addNewText(Client.methodR(runeCount) + "/" + item3Amount, itemX, itemY + 32, 35, 14, 0,
							runeCount >= item3Amount ? 65280 : 16711680, 0, true, 1, 1, 0));
		}

		if (item4Id != -1) {
			int itemX = posX * 4 + 105;
			int itemY = yPos + 2 + heightLevelText + descHeight;
			RSInterface.interfaceCache[hoverId].createDynamic(11, addItemSprite(itemX, itemY, item4Id));

			int runeCount = checkItems(item4Id);
			RSInterface.interfaceCache[hoverId].createDynamic(12,
					addNewText(Client.methodR(runeCount) + "/" + item4Amount, itemX, itemY + 32, 35, 14, 0,
							runeCount >= item4Amount ? 65280 : 16711680, 0, true, 1, 1, 0));
		}
	}

	private static int checkItems(int id) {
		int pouchOne = -1;
		int pouchTwo = -1;
		int pouchThird = -1;
		if (Client.itemCount(3214, 30047) > 0) {
			RSInterface pouch = RSInterface.interfaceCache[43671];
			pouchOne = pouch.inv[0] - 1;
			pouchTwo = pouch.inv[1] - 1;
			pouchThird = pouch.inv[2] - 1;
		}
		switch (id) {
		case 556: {
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1381) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1397) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1405) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 21777) > 0) {
				return 999999999;
			}
			int extra = 0;
			if (id == pouchOne) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[0];
			} else if (id == pouchTwo) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[1];
			} else if (id == pouchThird) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[2];
			}
			return Client.itemCount(3214, 556) + Client.itemCount(3214, 4697) + Client.itemCount(3214, 4695)
					+ Client.itemCount(3214, 4696) + extra;
		}
		case 555: {
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 30089) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 18346) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1383) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1395) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1403) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 6562) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 6563) > 0) {
				return 999999999;
			}
			int extra = 0;
			if (id == pouchOne) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[0];
			} else if (id == pouchTwo) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[1];
			} else if (id == pouchThird) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[2];
			}
			return Client.itemCount(3214, 555) + Client.itemCount(3214, 4694) + Client.itemCount(3214, 4695)
					+ Client.itemCount(3214, 4698) + extra;
		}
		case 561: {//Nature rune
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			int extra = 0;
			if (Client.itemCount(1688, 18341) > 0) {
				extra = Client.variousSettings[ConfigCodes.NATURE_STAFF_CHARGE_COUNT];
			}

			if (id == pouchOne) {
				extra += RSInterface.interfaceCache[43671].invStackSizes[0];
			} else if (id == pouchTwo) {
				extra += RSInterface.interfaceCache[43671].invStackSizes[1];
			} else if (id == pouchThird) {
				extra += RSInterface.interfaceCache[43671].invStackSizes[2];
			}

			return Client.itemCount(3214, 561) + extra;
		}
		case 563: {//Law rune
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			int extra = 0;
			if (Client.itemCount(1688, 18342) > 0) {
				extra = Client.variousSettings[ConfigCodes.LAW_STAFF_CHARGE_COUNT];
			}

			if (id == pouchOne) {
				extra += RSInterface.interfaceCache[43671].invStackSizes[0];
			} else if (id == pouchTwo) {
				extra += RSInterface.interfaceCache[43671].invStackSizes[1];
			} else if (id == pouchThird) {
				extra += RSInterface.interfaceCache[43671].invStackSizes[2];
			}

			return Client.itemCount(3214, 563) + extra;
		}
		case 557: {
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1385) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1399) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1407) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 3053) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 3054) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 6562) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 6563) > 0) {
				return 999999999;
			}
			int extra = 0;
			if (id == pouchOne) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[0];
			} else if (id == pouchTwo) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[1];
			} else if (id == pouchThird) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[2];
			}
			return Client.itemCount(3214, 557) + Client.itemCount(3214, 4696) + Client.itemCount(3214, 4699)
					+ Client.itemCount(3214, 4698) + extra;
		}
		case 554: {
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1387) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1393) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 1401) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 3053) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 3054) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 120714) > 0) {
				return 999999999;
			}
			int extra = 0;
			if (id == pouchOne) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[0];
			} else if (id == pouchTwo) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[1];
			} else if (id == pouchThird) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[2];
			}
			return Client.itemCount(3214, 554) + Client.itemCount(3214, 4694) + Client.itemCount(3214, 4697)
					+ Client.itemCount(3214, 4699) + extra;
		}
		case 13867:
			return Client.itemCount(1688, 13867) + Client.itemCount(1688, 13869) + Client.itemCount(1688, 13941) + Client.itemCount(1688, 13943);
		case 8843:
			return Client.itemCount(1688, 2416) + Client.itemCount(1688, 8841) + Client.itemCount(1688, 124144);
		case 1409:
		case 4170:
		case 2415:
		case 2417:
			return Client.itemCount(1688, id);
		case 17780:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16169) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16170) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 17009) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 17011) > 0) {
				return 999999999;
			}

			return Client.itemCount(3214, id) + Client.itemCount(3214, 16091);
		case 17784:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16095);
		case 17781:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16163) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16164) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16997) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16999) > 0) {
				return 999999999;
			}

			return Client.itemCount(3214, id) + Client.itemCount(3214, 16092);
		case 17782:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16165) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16166) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 17001) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 17003) > 0) {
				return 999999999;
			}

			return Client.itemCount(3214, id) + Client.itemCount(3214, 16093);
		case 17783:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16167) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 16168) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 17005) > 0) {
				return 999999999;
			}
			if (Client.itemCount(1688, 17007) > 0) {
				return 999999999;
			}

			return Client.itemCount(3214, id) + Client.itemCount(3214, 16094);
		case 17788:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16099);
		case 17789:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16100);
		case 17785:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16096);
		case 17790:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16101);
		case 17791:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16102);
		case 17792:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16103);
		case 17786:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16097);
		case 17787:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16098);
		case 17793:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			return Client.itemCount(3214, id) + Client.itemCount(3214, 16104);
		default:
			if (Client.variousSettings[ConfigCodes.INF_RUNES] > 0) {
				return 999999999;
			}
			
			int extra = 0;
			if (id == pouchOne) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[0];
			} else if (id == pouchTwo) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[1];
			} else if (id == pouchThird) {
				extra = RSInterface.interfaceCache[43671].invStackSizes[2];
			}
			return Client.itemCount(3214, id) + extra;
		}
	}

	public static void votingPoll() {
		int tabId = 59000;
		int scrollId = 59010;
		RSInterface tab = addInterface(tabId, InterfaceType.MAIN);
		addInterface(scrollId, InterfaceType.MAIN);

		addSprite(59001, 793);
		addNewText(59002, "Quality of SoulPlay - Voting poll 2018", 40, 10, 2, 0xff9b00, 0, true, 1, 0, 0);
		addNewText(59003, "Voting requires a total level of 1500 & 2 hours play-time.", 40, 10, 0, 0xffffff, 0, true,
				0, 0, 0);
		addButton(59004, 796, 797, "History");
		addNewText(59005, "History", 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
		addButton(59006, 796, 797, "Vote");
		addNewText(59007, "Vote", 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
		addButton(59008, 300, 301, "Close");

		tab.totalChildren(9);
		tab.child(0, 59001, 2, 2);
		tab.child(1, 59002, 240, 12);
		tab.child(2, 59003, 17, 45);
		tab.child(3, 59004, 418, 42);
		tab.child(4, 59005, 440, 44);
		tab.child(5, 59006, 330, 42);
		tab.child(6, 59007, 352, 44);
		tab.child(7, 59008, 487, 14);
		tab.child(8, 59010, 15, 65);

		addNewText(59011,
				"Throughout February, we're running a series of polls of your popular quality-of-<br>"
						+ "life suggestions, like we did last year.<br><br>"
						+ "For this week, we've collated the most popular overall player suggestions<br>"
						+ "for you to vote on!<br><br>" + "This poll will close on Saturday 24th February.",
				460, 100, 1, 0xff9b00, 0, true, 0, 0, 0);
		addNewText(59012, "Total Votes: 79,380", 70, 10, 2, 0xf5f5f5, 0, true, 1, 0, 0);
	}
	
	public static String seasonPassDetailString = "Level up your Season Pass<br>by completing Daily Tasks"
			+"<br><br>Some of these items cannot<br>be obtained anywhere<br>else but here."
			+ "<br><br>Expires: ";
	
	public static void seasonPass() {
		int tabId = 60000;
		int scrollId = 60010;
		RSInterface tab = addInterface(tabId, InterfaceType.MAIN);
		RSInterface scroll = addInterface(scrollId, InterfaceType.MAIN);

		addSprite(60001, 793);
		addNewText(60002, "Season Pass", 40, 10, 2, 0xff9b00, 0, true, 1, 0, 0);
		addNewText(60003, "Lvl:", 40, 10, 0, 0xff9b00, 0, true,
				0, 0, 0);
		addButton(60004, 796, 797, "Daily Tasks");
		addNewText(60005, "Daily Tasks", 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
		addButton(60006, 796, 797, "Buy Pass");
		addNewText(60007, "Buy Pass", 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
		//addButton(60008, 300, 301, "Close");
		addNewText(60009, "Exp:", 40, 10, 0, 0xff9b00, 0, true, 0, 0, 0);
		addNewText(60008, "Level up your Season Pass<br>by completing Daily Tasks"
				+"<br><br>Some of these items cannot<br>be obtained anywhere<br>else but here."
				+ "<br><br>Expires: Expired!", 40, 10, 0, 0xeb981f, 0, true, 0, 0, 0);

		tab.totalChildren(11);
		tab.child(0, 60001, 2, 2);
		tab.child(1, 60002, 240, 12);
		tab.child(2, 60003, 17, 45);
		tab.child(3, 60004, 418, 42);
		tab.child(4, 60005, 440, 44);
		tab.child(5, 60006, 330, 42);
		tab.child(6, 60007, 352, 44);
		tab.child(7, 59008/*60008*/, 487, 14); //close button from vote poll
		tab.child(8, 60010, 15, 65);
		tab.child(9, 60009, 60, 45);
		tab.child(10, 60008, 16, 60);

		
//		addRectangle(60011, 0, 0x777775, false, 150, 100, 0); // outline around the box
//		RSInterface.addItemSprite(60012, 14484);
//		addNewText(60013, "Level Req: 1", 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
//		addButton(60014, 796, 797, "Claim");
//		addNewText(60015, "Claim", 40, 10, 1, 0xff9b00, 0, true, 1, 0, 0);
//		
//		scroll.totalChildren(5);
//		scroll.child(0, 60011, 160, 10);
//		scroll.child(1, 60012, 215, 20);
//		scroll.child(2, 60013, 215, 60);
//		scroll.child(3, 60014, 195, 83);
//		scroll.child(4, 60015, 215, 85);
		
	}

	public static void dungSmelting() {
		RSInterface tab = addInterface(51000, InterfaceType.MAIN);

		addInventory(51001, 10, 1, 17, 0, false, "Smelt 1", "Smelt 5", "Smelt 10", "Smelt X");
		for (int i = 0; i < 10 * 1; i++) {
			RSInterface.interfaceCache[51001].inv[i] = 17663;
			RSInterface.interfaceCache[51001].invStackSizes[i] = 1;
		}

		addNewText(51002, "Novite", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51003, "Bathus", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51004, "Marm-<br>aros", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51005, "Krat-<br>onite", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51006, "Fra-<br>ctite", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51007, "Zep-<br>hyrium", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51008, "Arg-<br>onite", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51009, "Kat-<br>agon", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51010, "Gorg-<br>onite", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);
		addNewText(51011, "Prom-<br>ethium", 40, 10, 1, 0x000000, 0, false, 1, 0, 0);

		addNewText(51012, "What would you like to smelt?", 40, 10, 1, 0x800000, 0, false, 1, 0, 0);

		tab.totalChildren(12);
		tab.child(0, 51001, 2, 32);
		tab.child(1, 51002, 2, 75);
		tab.child(2, 51003, 50, 75);// 75
		tab.child(3, 51004, 99, 75);
		tab.child(4, 51005, 148, 75);
		tab.child(5, 51006, 197, 75);
		tab.child(6, 51007, 246, 75);
		tab.child(7, 51008, 295, 75);
		tab.child(8, 51009, 344, 75);
		tab.child(9, 51010, 393, 75);
		tab.child(10, 51011, 442, 75);
		tab.child(11, 51012, 220, 5);

	}

	public static void dungRewardShop() {
		RSInterface tab = addInterface(46500, InterfaceType.MAIN);
		RSInterface scroll = addInterface(46520, InterfaceType.MAIN);

		addSprite(46501, 793);
		addSprite(46502, 803);
		addNewText(46503, "Daemonheim Rewards", 40, 10, 2, 0xff9900, 0, true, 1, 0, 0);
		addSprite(46504, 794);
		addSprite(46505, 795);
		addNewText(46506, "Your tokens:", 40, 10, 2, 0xffffff, 0, true, 0, 0, 0);
		addNewText(46507, "200,000", 40, 10, 2, 0xfbd6a9, 0, true, 0, 0, 0);

		tab.totalChildren(14);
		tab.child(0, 46501, 2, 2);
		tab.child(1, 46502, 15, 43);
		tab.child(2, 39702, 485, 14); // close button
		tab.child(3, 39703, 485, 14); // close hover button
		tab.child(4, 46503, 250, 14);
		tab.child(5, 46520, 18, 46);
		tab.child(6, 46504, 340, 35);
		tab.child(7, 46505, 15, 312);
		tab.child(8, 46506, 35, 310);
		tab.child(9, 46507, 125, 310);
		tab.child(10, 47000, 340, 35);
		tab.child(11, 47002, 340, 35);
		tab.child(12, 47010, 340, 259);
		tab.child(13, 47015, 340, 259);

		int totalItems = DungRewardData.values.length;
		int endY = 0;
		scroll.totalChildren(totalItems * 4);
		for (int i = 0; i < totalItems; i++) {
			DungRewardData data = DungRewardData.values[i];

			int childOffset = i * 4;
			int startX = (i % 5) * 60;
			int startY = (i / 5) * 68;
			if (endY < startY) {
				endY = startY;
			}
			int startTokenX = startX + 4;
			int startTokenY = startY + 39;
			int startPriceX = startX + 5;
			int startPriceY = startY + 39;

			addSprite(46522 + childOffset, 500, -1, -1, 0, 502, 46, 50, "Select", true);
			addSprite(46522 + childOffset + 1, 795, 0, 0);
			addNewText(46522 + childOffset + 2, Client.methodR(data.getPrice()), 40, 10, 0, 0xe2e2a2, 0, true, 2, 0, 0);
			if (!data.isModel()) {
				addItemSprite(46522 + childOffset + 3, data.getProductId());
				RSInterface.interfaceCache[46522 + childOffset + 3].setFlag(RSInterface.IGNORE_BOUNDS_BIT);
			} else {
				addModel(46522 + childOffset + 3, 33, 65, 0, 0, data.getZoom(), data.getProductId(), 5, data.getAnimId(), 0);
			}

			scroll.child(childOffset, 46522 + childOffset, startX, startY);
			scroll.child(childOffset + 1, 46522 + childOffset + 1, startTokenX, startTokenY);
			scroll.child(childOffset + 2, 46522 + childOffset + 2, startPriceX, startPriceY);
			scroll.child(childOffset + 3, 46522 + childOffset + 3, startX + 7, startY + 4);
		}

		scroll.scrollMax = endY + 68;
		scroll.width = 300;
		scroll.height = 257;
		if (scroll.scrollMax < scroll.height) {
			scroll.scrollMax = scroll.height + 1;
		}

		RSInterface empty = addInterface(47000, InterfaceType.TAB);
		addNewText(47001, "Select an item to view<br>more<br>information/purchase.", 152, 150, 2, 0x7592a0, 0, true, 1,
				1, 0);

		empty.totalChildren(1);
		empty.child(0, 47001, 4, 70);

		empty.hidden = false;

		RSInterface buy = addInterface(47002, InterfaceType.TAB);

		addNewText(47003, "None", 160, 15, 1, 0xff9b00, 0, true, 1, 0, 0);
		addSprite(47004, 100);
		RSInterface.interfaceCache[47004].itemId = 18337;
		addNewText(47005,"None", 152, 150, 1, 0xfbd6a9, 0, true, 1, 1, 0);
		addModel(47006, 37, 170, 0, 0, 0, 0, 5, -1, 0);
		RSInterface.interfaceCache[47006].hidden = true;

		buy.totalChildren(4);
		buy.child(0, 47003, 0, 8);// 340, 35
		buy.child(1, 47004, 63, 30);
		buy.child(2, 47005, 4, 70);
		buy.child(3, 47006, 65, 35);

		buy.hidden = true;

		RSInterface buyButton = addInterface(47010, InterfaceType.TAB);
		addButton(47011, 799, 800, "Buy");
		addNewText(47012, "Buy", 128, 25, 2, 0xd2d2d2, 0xFFFFFF, true, 1, 1, 0);
		addSprite(47013, 795);
		addNewText(47014, "200,000", 160, 15, 2, 0xff9b00, 0, true, 1, 1, 0);
		buyButton.totalChildren(4);
		buyButton.child(0, 47011, 16, 30);
		buyButton.child(1, 47012, 16, 30);
		buyButton.child(2, 47013, 30, 8);
		buyButton.child(3, 47014, 0, 6);
		buyButton.hidden = true;

		RSInterface buyConfirm = addInterface(47015, InterfaceType.TAB); // when
																			// pressing
																			// yes
																			// should
																			// go
																			// to
																			// empty
																			// layer
		addButton(47016, 801, 802, "Yes");
		addNewText(47017, "Yes", 68, 25, 2, 0xd2d2d2, 0xFFFFFF, true, 1, 1, 0);
		addButton(47018, 801, 802, "No");
		addNewText(47019, "No", 68, 25, 2, 0xd2d2d2, 0xFFFFFF, true, 1, 1, 0);
		addNewText(47020, "Confirm purchase", 160, 15, 2, 0xff9b00, 0, true, 1, 1, 0);
		buyConfirm.totalChildren(5);
		buyConfirm.child(0, 47016, 4, 30);
		buyConfirm.child(1, 47017, 4, 30);
		buyConfirm.child(2, 47018, 88, 30);
		buyConfirm.child(3, 47019, 88, 30);
		buyConfirm.child(4, 47020, 0, 6);
		buyConfirm.hidden = true;

	}

	public static void dungScrollingShop() {
		RSInterface tab = addInterface(44000, InterfaceType.MAIN);
		RSInterface scroll = addInterface(44010, InterfaceType.MAIN);

		addSprite(44001, 702);
		addText(44002, "Shop", 2, 0xff9900, true, true, 0);
		addText(44003, "Right-click on an item in the shop to see the available options.", 0, 0xFFD700, true, true, 0);

		tab.totalChildren(6);
		tab.child(0, 44001, 16, 16);
		tab.child(1, 44002, 256, 28);
		tab.child(2, 44003, 256, 296);
		tab.child(3, 39702, 476, 28); // close button
		tab.child(4, 39703, 476, 28); // close hover button
		tab.child(5, 44010, 32, 63);

		addInventory(44011, 8, 20, 22, 24, false, "Info", "Buy 1", "Buy 5", "Buy 10", "Buy 50");
		RSInterface.interfaceCache[44011].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		RSInterface.interfaceCache[44011].x = 8;
		RSInterface.interfaceCache[44011].y = 7;

		scroll.width = 443;
		scroll.height = 225;
	}

	public static void dungeoneeringMap() {
		RSInterface tab = addInterface(43974, InterfaceType.MAIN);

		addSprite(43975, 772);
		addSprite(43976, 772);
		RSInterface.interfaceCache[43976].flipHorizontal();
		RSInterface components = addInterface(43977, InterfaceType.MAIN);
		components.width = 252;
		components.height = 252;
		components.totalChildren(0);

		tab.totalChildren(5);
		tab.child(0, 43975, 100, 10);
		tab.child(1, 43976, 260, 10);
		tab.child(2, 39702, 393, 20); // close button
		tab.child(3, 39703, 393, 20); // close hover button
		tab.child(4, 43977, 135, 50);
	}

	public static void dungeoneeringbindings() {
		RSInterface tab = addInterface(43933, InterfaceType.MAIN);

		addInventory(29991, 5, 1, 8, 8, false, "Clear");
		addInventory(29975, 5, 1, 8, 8, false, "Clear");
		addInventory(29974, 5, 1, 8, 8, false, "Clear");
		addInventory(29395, 6, 2, 8, 8, true);
		addSprite(43934, 708);
		addText(43935, "Bound Items Setup", 2, 0xFFFFFF, false, true, 0);
		addText(43936, "Bound Items", 1, 0xFFFFFF, true, true, 0);
		addButton(43937, 710, 709, "Close");
		RSInterface.interfaceCache[43937].atActionType = 3;
		addSprite(43938, 721, 720, 34, 34);
		addSprite(43939, 721, 720, 34, 34);
		addSprite(43940, 721, 720, 34, 34);
		addSprite(43941, 721, 720, 34, 34);
		addSprite(43942, 721, 720, 34, 34);
		addSprite(43943, 721, 720, 34, 34);
		addSprite(43944, 721, 720, 34, 34);
		addSprite(43945, 721, 720, 34, 34);
		addSprite(43946, 721, 720, 34, 34);
		addSprite(43947, 721, 720, 34, 34);
		addSprite(43948, 721, 720, 34, 34);
		addSprite(43949, 721, 720, 34, 34);
		addButton(43950, 712, 711, "Done");
		RSInterface.interfaceCache[43950].atActionType = 3;
		addText(43951, "Loadouts", 1, 0xFFFFFF, true, true, 0);
		addButton(43952, 719, 718, "Unbind");
		RSInterface.interfaceCache[43952].setFlag(RSInterface.CAN_DRAG_ON_TO_BIT);
		addButton(43953, 716, 717, "Start-with");
		addButton(43954, 716, 717, "Start-with");
		addButton(43955, 716, 717, "Start-with");
		setRadioButtonVarp(43953, ConfigCodes.SELECTED_LOADOUT, 0, 715);
		setRadioButtonVarp(43954, ConfigCodes.SELECTED_LOADOUT, 1, 715);
		setRadioButtonVarp(43955, ConfigCodes.SELECTED_LOADOUT, 2, 715);
		addSprite(43956, 723, 722, 34, 34);
		addSprite(43957, 723, 722, 34, 34);
		addSprite(43958, 723, 722, 34, 34);
		addSprite(43959, 723, 722, 34, 34);
		addSprite(43960, 723, 722, 34, 34);

		addSprite(43961, 727, 726, 34, 34);
		addSprite(43962, 727, 726, 34, 34);
		addSprite(43963, 727, 726, 34, 34);
		addSprite(43964, 727, 726, 34, 34);
		addSprite(43965, 727, 726, 34, 34);

		addSprite(43966, 725, 724, 34, 34);
		addSprite(43967, 725, 724, 34, 34);
		addSprite(43968, 725, 724, 34, 34);
		addSprite(43969, 725, 724, 34, 34);
		addSprite(43970, 725, 724, 34, 34);
		addButton(43971, 714, 713, "Use");
		addButton(43972, 714, 713, "Use");
		addButton(43973, 714, 713, "Use");

		tab.totalChildren(44);
		tab.child(0, 43934, 85, 7);
		tab.child(1, 43935, 195, 10);
		tab.child(2, 43936, 252, 52);
		tab.child(3, 43937, 413, 21);

		// bound item boxes
		tab.child(4, 43938, 121, 71);
		tab.child(5, 43939, 161, 71);
		tab.child(6, 43940, 201, 71);
		tab.child(7, 43941, 241, 71);
		tab.child(8, 43942, 281, 71);
		tab.child(9, 43943, 321, 71);
		tab.child(10, 43944, 121, 111);
		tab.child(11, 43945, 161, 111);
		tab.child(12, 43946, 201, 111);
		tab.child(13, 43947, 241, 111);
		tab.child(14, 43948, 281, 111);
		tab.child(15, 43949, 321, 111);

		tab.child(16, 43950, 210, 294);
		tab.child(17, 43951, 252, 155);
		tab.child(18, 43952, 377, 98);
		// check boxes
		tab.child(19, 43953, 110, 184);
		tab.child(20, 43954, 110, 224);
		tab.child(21, 43955, 110, 264);

		// green boxes
		tab.child(22, 43956, 140, 173);
		tab.child(23, 43957, 180, 173);
		tab.child(24, 43958, 220, 173);
		tab.child(25, 43959, 260, 173);
		tab.child(26, 43960, 300, 173);
		// yellow boxes
		tab.child(27, 43961, 140, 213);
		tab.child(28, 43962, 180, 213);
		tab.child(29, 43963, 220, 213);
		tab.child(30, 43964, 260, 213);
		tab.child(31, 43965, 300, 213);
		// red boxes
		tab.child(32, 43966, 140, 253);
		tab.child(33, 43967, 180, 253);
		tab.child(34, 43968, 220, 253);
		tab.child(35, 43969, 260, 253);
		tab.child(36, 43970, 300, 253);
		// load arrows
		tab.child(37, 43971, 351, 183);
		tab.child(38, 43972, 351, 223);
		tab.child(39, 43973, 351, 263);
		// inventories
		tab.child(40, 29395, 122, 74);
		tab.child(41, 29991, 140, 177);
		tab.child(42, 29975, 140, 217);
		tab.child(43, 29974, 140, 257);
	}

	public static void dungeoneeringComplexity() {
		RSInterface tab = addInterface(43672, InterfaceType.MAIN);

		addNewText(29996, "0% XP Penalty", 60, 16, 0, 0xe2e2a2, 0, true, 1, 1, 0);
		addSprite(43673, 581);
		addSprite(43674, 582);
		addButton(43675, 584, 583, "Confirm");
		addText(43676, "Select Complexity", 2, 0xc69b01, false, true, 0);
		addText(43677, "Complexity", 2, 0xc69b01, false, true, 0);
		addText(43678, "Confirm", 1, 0xc69b01, false, true, 0);
		addText(43679, "8", 2, 0xe2e2a2, true, true, 0);
		addNewText(43680,
				"The complexity of a dungeon deremines,<br>" + "how many of your skills and abilities will<br>"
						+ "be tested in a raid, choosing a lower so<br>" + "complexity will make the dungeon more<br>"
						+ "straight-forward to complete, but it will<br>" + "also lower the Dungeoneering XP reward<br>"
						+ "you receive at the end.<br><br>" + "When you first start Dungeoneering, only<br>"
						+ "complexity 1 will be unlocked. You must<br>" + "complete at least one raid at that<br>"
						+ "complexity before you can unlock the<br>" + "next.",
				240, 300, 1, 0xc69b01, 0, true, 0, 0, 0);

		addSprite(43681, 670);

		addSprite(25423, 666, -1, -1, 0, -1, 20, 20, "Select-complexity", true);
		addSprite(25424, 669);
		addSprite(25425, 666, -1, -1, 0, -1, 20, 20, "Select-complexity", true);
		addSprite(25426, 669);
		RSInterface.interfaceCache[25426].hidden = true;
		addSprite(25427, 666, -1, -1, 0, -1, 20, 20, "Select-complexity", true);
		addSprite(25428, 669);
		RSInterface.interfaceCache[25428].hidden = true;
		addSprite(25429, 666, -1, -1, 0, -1, 20, 20, "Select-complexity", true);
		addSprite(25430, 669);
		RSInterface.interfaceCache[25430].hidden = true;
		addSprite(25431, 666, -1, -1, 0, -1, 20, 20, "Select-complexity", true);
		addSprite(25432, 669);
		RSInterface.interfaceCache[25432].hidden = true;
		addSprite(25433, 666, -1, -1, 0, -1, 20, 20, "Select-complexity", true);
		addSprite(25434, 669);
		RSInterface.interfaceCache[25434].hidden = true;

		addNewText(25435, "1", 22, 22, 2, 0, 0xffffff, false, 1, 1, 0);
		addNewText(25436, "2", 22, 22, 2, 0, 0xffffff, false, 1, 1, 0);
		addNewText(25437, "3", 22, 22, 2, 0, 0xffffff, false, 1, 1, 0);
		addNewText(25438, "4", 22, 22, 2, 0, 0xffffff, false, 1, 1, 0);
		addNewText(25439, "5", 22, 22, 2, 0, 0xffffff, false, 1, 1, 0);
		addNewText(25440, "6", 22, 22, 2, 0, 0xffffff, false, 1, 1, 0);

		tab.totalChildren(35);
		tab.child(0, 43673, 2, 2);
		tab.child(1, 43674, 127, 270);
		tab.child(2, 43675, 169, 266);
		tab.child(3, 39702, 490, 5); // close button
		tab.child(4, 39703, 490, 5); // close hover button
		tab.child(5, 43676, 200, 5);
		tab.child(6, 43677, 36, 274);
		tab.child(7, 43678, 184, 274);
		tab.child(8, 43679, 139, 275);
		tab.child(9, 43680, 264, 30);
		tab.child(10, 43681, 33, 67);
		tab.child(11, 43682, 10, 30);
		tab.child(12, 43715, 10, 30);
		tab.child(13, 43748, 10, 30);
		tab.child(14, 43781, 10, 30);
		tab.child(15, 43814, 10, 30);

		tab.child(16, 25423, 27, 37);
		tab.child(17, 25424, 26, 36);
		tab.child(18, 25425, 66, 37);
		tab.child(19, 25426, 65, 36);
		tab.child(20, 25427, 104, 37);
		tab.child(21, 25428, 103, 36);
		tab.child(22, 25429, 142, 37);
		tab.child(23, 25430, 141, 36);
		tab.child(24, 25431, 180, 37);
		tab.child(25, 25432, 179, 36);
		tab.child(26, 25433, 218, 37);
		tab.child(27, 25434, 217, 36);
		tab.child(28, 25435, 26, 36);
		tab.child(29, 25436, 65, 36);
		tab.child(30, 25437, 103, 36);
		tab.child(31, 25438, 141, 36);
		tab.child(32, 25439, 179, 36);
		tab.child(33, 25440, 217, 36);
		tab.child(34, 29996, 100, 213);

		RSInterface complexityOne = addInterface(43682, InterfaceType.TAB);
		addSprite(43683, 671);
		addText(43684, "Combat", 0, 0xe2e2a2, false, true, 0);
		addSprite(43685, 687);
		addText(43686, "Cooking", 0, 0x808080, false, true, 0);
		addSprite(43687, 700);
		addText(43688, "Firemaking", 0, 0x808080, false, true, 0);
		addSprite(43689, 688);
		addText(43690, "Woodcutting", 0, 0x808080, false, true, 0);
		addSprite(43691, 689);
		addText(43692, "Fishing", 0, 0x808080, false, true, 0);
		addSprite(43693, 690);
		addText(43694, "Creating Weapons", 0, 0x808080, false, true, 0);
		addSprite(43695, 691);
		addText(43696, "Mining", 0, 0x808080, false, true, 0);
		addSprite(43697, 692);
		addText(43698, "Runecrafting", 0, 0x808080, false, true, 0);

		addSprite(43699, 693);
		addText(43700, "Farming Textiles", 0, 0x808080, false, true, 0);
		addSprite(43701, 694);
		addText(43702, "Hunting", 0, 0x808080, false, true, 0);
		addSprite(43703, 695);
		addText(43704, "Creating Armour", 0, 0x808080, false, true, 0);
		addSprite(43705, 696);
		addText(43706, "Farming seeds", 0, 0x808080, false, true, 0);
		addSprite(43707, 697);
		addText(43708, "Herblore", 0, 0x808080, false, true, 0);
		addSprite(43709, 699);
		addText(43710, "Thieving", 0, 0x808080, false, true, 0);
		addSprite(43711, 698);
		addText(43712, "Summoning", 0, 0x808080, false, true, 0);
		addSprite(43713, 695);
		addText(43714, "Construction", 0, 0x808080, false, true, 0);

		complexityOne.totalChildren(32);
		complexityOne.child(0, 43683, 5, 60);
		complexityOne.child(1, 43684, 27, 62);
		complexityOne.child(2, 43685, 5, 75);
		complexityOne.child(3, 43686, 27, 77);
		complexityOne.child(4, 43687, 5, 90);
		complexityOne.child(5, 43688, 27, 92);
		complexityOne.child(6, 43689, 5, 105);
		complexityOne.child(7, 43690, 27, 107);
		complexityOne.child(8, 43691, 5, 120);
		complexityOne.child(9, 43692, 27, 122);
		complexityOne.child(10, 43693, 5, 135);
		complexityOne.child(11, 43694, 27, 137);
		complexityOne.child(12, 43695, 5, 150);
		complexityOne.child(13, 43696, 27, 152);
		complexityOne.child(14, 43697, 5, 165);
		complexityOne.child(15, 43698, 27, 167);

		complexityOne.child(16, 43699, 125, 60);
		complexityOne.child(17, 43700, 147, 62);
		complexityOne.child(18, 43701, 125, 75);
		complexityOne.child(19, 43702, 147, 77);
		complexityOne.child(20, 43703, 125, 90);
		complexityOne.child(21, 43704, 147, 92);
		complexityOne.child(22, 43705, 125, 105);
		complexityOne.child(23, 43706, 147, 107);
		complexityOne.child(24, 43707, 125, 120);
		complexityOne.child(25, 43708, 147, 122);
		complexityOne.child(26, 43709, 125, 138);
		complexityOne.child(27, 43710, 147, 137);
		complexityOne.child(28, 43711, 125, 150);
		complexityOne.child(29, 43712, 147, 152);
		complexityOne.child(30, 43713, 125, 165);
		complexityOne.child(31, 43714, 147, 167);

		complexityOne.hidden = true;

		RSInterface complexityTwo = addInterface(43715, InterfaceType.TAB);
		addSprite(43716, 671);
		addText(43717, "Combat", 0, 0xe2e2a2, false, true, 0);
		addSprite(43718, 672);
		addText(43719, "Cooking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43720, 673);
		addText(43721, "Firemaking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43722, 674);
		addText(43723, "Woodcutting", 0, 0xe2e2a2, false, true, 0);
		addSprite(43724, 675);
		addText(43725, "Fishing", 0, 0xe2e2a2, false, true, 0);
		addSprite(43726, 690);
		addText(43727, "Creating Weapons", 0, 0x808080, false, true, 0);
		addSprite(43728, 691);
		addText(43729, "Mining", 0, 0x808080, false, true, 0);
		addSprite(43730, 692);
		addText(43731, "Runecrafting", 0, 0x808080, false, true, 0);

		addSprite(43732, 693);
		addText(43733, "Farming Textiles", 0, 0x808080, false, true, 0);
		addSprite(43734, 694);
		addText(43735, "Hunting", 0, 0x808080, false, true, 0);
		addSprite(43736, 695);
		addText(43737, "Creating Armour", 0, 0x808080, false, true, 0);
		addSprite(43738, 696);
		addText(43739, "Farming seeds", 0, 0x808080, false, true, 0);
		addSprite(43740, 697);
		addText(43741, "Herblore", 0, 0x808080, false, true, 0);
		addSprite(43742, 699);
		addText(43743, "Thieving", 0, 0x808080, false, true, 0);
		addSprite(43744, 698);
		addText(43745, "Summoning", 0, 0x808080, false, true, 0);
		addSprite(43746, 695);
		addText(43747, "Construction", 0, 0x808080, false, true, 0);

		complexityTwo.totalChildren(32);
		complexityTwo.child(0, 43716, 5, 60);
		complexityTwo.child(1, 43717, 27, 62);
		complexityTwo.child(2, 43718, 5, 75);
		complexityTwo.child(3, 43719, 27, 77);
		complexityTwo.child(4, 43720, 5, 90);
		complexityTwo.child(5, 43721, 27, 92);
		complexityTwo.child(6, 43722, 5, 105);
		complexityTwo.child(7, 43723, 27, 107);
		complexityTwo.child(8, 43724, 5, 120);
		complexityTwo.child(9, 43725, 27, 122);
		complexityTwo.child(10, 43726, 5, 135);
		complexityTwo.child(11, 43727, 27, 137);
		complexityTwo.child(12, 43728, 5, 150);
		complexityTwo.child(13, 43729, 27, 152);
		complexityTwo.child(14, 43730, 5, 165);
		complexityTwo.child(15, 43731, 27, 167);

		complexityTwo.child(16, 43732, 125, 60);
		complexityTwo.child(17, 43733, 147, 62);
		complexityTwo.child(18, 43734, 125, 75);
		complexityTwo.child(19, 43735, 147, 77);
		complexityTwo.child(20, 43736, 125, 90);
		complexityTwo.child(21, 43737, 147, 92);
		complexityTwo.child(22, 43738, 125, 105);
		complexityTwo.child(23, 43739, 147, 107);
		complexityTwo.child(24, 43740, 125, 120);
		complexityTwo.child(25, 43741, 147, 122);
		complexityTwo.child(26, 43742, 125, 138);
		complexityTwo.child(27, 43743, 147, 137);
		complexityTwo.child(28, 43744, 125, 150);
		complexityTwo.child(29, 43745, 147, 152);
		complexityTwo.child(30, 43746, 125, 165);
		complexityTwo.child(31, 43747, 147, 167);

		complexityTwo.hidden = true;

		RSInterface complexityThree = addInterface(43748, InterfaceType.TAB);
		addSprite(43749, 671);
		addText(43750, "Combat", 0, 0xe2e2a2, false, true, 0);
		addSprite(43751, 672);
		addText(43752, "Cooking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43753, 673);
		addText(43754, "Firemaking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43755, 674);
		addText(43756, "Woodcutting", 0, 0xe2e2a2, false, true, 0);
		addSprite(43757, 675);
		addText(43758, "Fishing", 0, 0xe2e2a2, false, true, 0);
		addSprite(43759, 676);
		addText(43760, "Creating Weapons", 0, 0xe2e2a2, false, true, 0);
		addSprite(43761, 677);
		addText(43762, "Mining", 0, 0xe2e2a2, false, true, 0);
		addSprite(43763, 678);
		addText(43764, "Runecrafting", 0, 0xe2e2a2, false, true, 0);

		addSprite(43765, 693);
		addText(43766, "Farming Textiles", 0, 0x808080, false, true, 0);
		addSprite(43767, 694);
		addText(43768, "Hunting", 0, 0x808080, false, true, 0);
		addSprite(43769, 695);
		addText(43770, "Creating Armour", 0, 0x808080, false, true, 0);
		addSprite(43771, 696);
		addText(43772, "Farming seeds", 0, 0x808080, false, true, 0);
		addSprite(43773, 697);
		addText(43774, "Herblore", 0, 0x808080, false, true, 0);
		addSprite(43775, 699);
		addText(43776, "Thieving", 0, 0x808080, false, true, 0);
		addSprite(43777, 698);
		addText(43778, "Summoning", 0, 0x808080, false, true, 0);
		addSprite(43779, 695);
		addText(43780, "Construction", 0, 0x808080, false, true, 0);

		complexityThree.totalChildren(32);
		complexityThree.child(0, 43749, 5, 60);
		complexityThree.child(1, 43750, 27, 62);
		complexityThree.child(2, 43751, 5, 75);
		complexityThree.child(3, 43752, 27, 77);
		complexityThree.child(4, 43753, 5, 90);
		complexityThree.child(5, 43754, 27, 92);
		complexityThree.child(6, 43755, 5, 105);
		complexityThree.child(7, 43756, 27, 107);
		complexityThree.child(8, 43757, 5, 120);
		complexityThree.child(9, 43758, 27, 122);
		complexityThree.child(10, 43759, 5, 135);
		complexityThree.child(11, 43760, 27, 137);
		complexityThree.child(12, 43761, 5, 150);
		complexityThree.child(13, 43762, 27, 152);
		complexityThree.child(14, 43763, 5, 165);
		complexityThree.child(15, 43764, 27, 167);

		complexityThree.child(16, 43765, 125, 60);
		complexityThree.child(17, 43766, 147, 62);
		complexityThree.child(18, 43767, 125, 75);
		complexityThree.child(19, 43768, 147, 77);
		complexityThree.child(20, 43769, 125, 90);
		complexityThree.child(21, 43770, 147, 92);
		complexityThree.child(22, 43771, 125, 105);
		complexityThree.child(23, 43772, 147, 107);
		complexityThree.child(24, 43773, 125, 120);
		complexityThree.child(25, 43774, 147, 122);
		complexityThree.child(26, 43775, 125, 138);
		complexityThree.child(27, 43776, 147, 137);
		complexityThree.child(28, 43777, 125, 150);
		complexityThree.child(29, 43778, 147, 152);
		complexityThree.child(30, 43779, 125, 165);
		complexityThree.child(31, 43780, 147, 167);

		complexityThree.hidden = true;

		RSInterface complexityFour = addInterface(43781, InterfaceType.TAB);
		addSprite(43782, 671);
		addText(43783, "Combat", 0, 0xe2e2a2, false, true, 0);
		addSprite(43784, 672);
		addText(43785, "Cooking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43786, 673);
		addText(43787, "Firemaking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43788, 674);
		addText(43789, "Woodcutting", 0, 0xe2e2a2, false, true, 0);
		addSprite(43790, 675);
		addText(43791, "Fishing", 0, 0xe2e2a2, false, true, 0);
		addSprite(43792, 676);
		addText(43793, "Creating Weapons", 0, 0xe2e2a2, false, true, 0);
		addSprite(43794, 677);
		addText(43795, "Mining", 0, 0xe2e2a2, false, true, 0);
		addSprite(43796, 678);
		addText(43797, "Runecrafting", 0, 0xe2e2a2, false, true, 0);

		addSprite(43798, 679);
		addText(43799, "Farming Textiles", 0, 0xe2e2a2, false, true, 0);
		addSprite(43800, 680);
		addText(43801, "Hunting", 0, 0xe2e2a2, false, true, 0);
		addSprite(43802, 681);
		addText(43803, "Creating Armour", 0, 0xe2e2a2, false, true, 0);
		addSprite(43804, 696);
		addText(43805, "Farming seeds", 0, 0x808080, false, true, 0);
		addSprite(43806, 697);
		addText(43807, "Herblore", 0, 0x808080, false, true, 0);
		addSprite(43808, 699);
		addText(43809, "Thieving", 0, 0x808080, false, true, 0);
		addSprite(43810, 698);
		addText(43811, "Summoning", 0, 0x808080, false, true, 0);
		addSprite(43812, 695);
		addText(43813, "Construction", 0, 0x808080, false, true, 0);

		complexityFour.totalChildren(32);
		complexityFour.child(0, 43782, 5, 60);
		complexityFour.child(1, 43783, 27, 62);
		complexityFour.child(2, 43784, 5, 75);
		complexityFour.child(3, 43785, 27, 77);
		complexityFour.child(4, 43786, 5, 90);
		complexityFour.child(5, 43787, 27, 92);
		complexityFour.child(6, 43788, 5, 105);
		complexityFour.child(7, 43789, 27, 107);
		complexityFour.child(8, 43790, 5, 120);
		complexityFour.child(9, 43791, 27, 122);
		complexityFour.child(10, 43792, 5, 135);
		complexityFour.child(11, 43793, 27, 137);
		complexityFour.child(12, 43794, 5, 150);
		complexityFour.child(13, 43795, 27, 152);
		complexityFour.child(14, 43796, 5, 165);
		complexityFour.child(15, 43797, 27, 167);

		complexityFour.child(16, 43798, 125, 60);
		complexityFour.child(17, 43799, 147, 62);
		complexityFour.child(18, 43800, 125, 75);
		complexityFour.child(19, 43801, 147, 77);
		complexityFour.child(20, 43802, 125, 90);
		complexityFour.child(21, 43803, 147, 92);
		complexityFour.child(22, 43804, 125, 105);
		complexityFour.child(23, 43805, 147, 107);
		complexityFour.child(24, 43806, 125, 120);
		complexityFour.child(25, 43807, 147, 122);
		complexityFour.child(26, 43808, 125, 138);
		complexityFour.child(27, 43809, 147, 137);
		complexityFour.child(28, 43810, 125, 150);
		complexityFour.child(29, 43811, 147, 152);
		complexityFour.child(30, 43812, 125, 165);
		complexityFour.child(31, 43813, 147, 167);

		complexityFour.hidden = true;

		RSInterface complexityFive = addInterface(43814, InterfaceType.TAB);
		addSprite(43815, 671);
		addText(43816, "Combat", 0, 0xe2e2a2, false, true, 0);
		addSprite(43817, 672);
		addText(43818, "Cooking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43819, 673);
		addText(43820, "Firemaking", 0, 0xe2e2a2, false, true, 0);
		addSprite(43821, 674);
		addText(43822, "Woodcutting", 0, 0xe2e2a2, false, true, 0);
		addSprite(43823, 675);
		addText(43824, "Fishing", 0, 0xe2e2a2, false, true, 0);
		addSprite(43825, 676);
		addText(43826, "Creating Weapons", 0, 0xe2e2a2, false, true, 0);
		addSprite(43827, 677);
		addText(43828, "Mining", 0, 0xe2e2a2, false, true, 0);
		addSprite(43829, 678);
		addText(43830, "Runecrafting", 0, 0xe2e2a2, false, true, 0);

		addSprite(43831, 679);
		addText(43832, "Farming Textiles", 0, 0xe2e2a2, false, true, 0);
		addSprite(43833, 680);
		addText(43834, "Hunting", 0, 0xe2e2a2, false, true, 0);
		addSprite(43835, 681);
		addText(43836, "Creating Armour", 0, 0xe2e2a2, false, true, 0);
		addSprite(43837, 682);
		addText(43838, "Farming seeds", 0, 0xe2e2a2, false, true, 0);
		addSprite(43839, 683);
		addText(43840, "Herblore", 0, 0xe2e2a2, false, true, 0);
		addSprite(43841, 685);
		addText(43842, "Thieving", 0, 0xe2e2a2, false, true, 0);
		addSprite(43843, 684);
		addText(43844, "Summoning", 0, 0xe2e2a2, false, true, 0);
		addSprite(43845, 695);
		addText(43846, "Construction", 0, 0xe2e2a2, false, true, 0);

		complexityFive.totalChildren(32);
		complexityFive.child(0, 43815, 5, 60);
		complexityFive.child(1, 43816, 27, 62);
		complexityFive.child(2, 43817, 5, 75);
		complexityFive.child(3, 43818, 27, 77);
		complexityFive.child(4, 43819, 5, 90);
		complexityFive.child(5, 43820, 27, 92);
		complexityFive.child(6, 43821, 5, 105);
		complexityFive.child(7, 43822, 27, 107);
		complexityFive.child(8, 43823, 5, 120);
		complexityFive.child(9, 43824, 27, 122);
		complexityFive.child(10, 43825, 5, 135);
		complexityFive.child(11, 43826, 27, 137);
		complexityFive.child(12, 43827, 5, 150);
		complexityFive.child(13, 43828, 27, 152);
		complexityFive.child(14, 43829, 5, 165);
		complexityFive.child(15, 43830, 27, 167);

		complexityFive.child(16, 43831, 125, 60);
		complexityFive.child(17, 43832, 147, 62);
		complexityFive.child(18, 43833, 125, 75);
		complexityFive.child(19, 43834, 147, 77);
		complexityFive.child(20, 43835, 125, 90);
		complexityFive.child(21, 43836, 147, 92);
		complexityFive.child(22, 43837, 125, 106);
		complexityFive.child(23, 43838, 147, 107);
		complexityFive.child(24, 43839, 125, 120);
		complexityFive.child(25, 43840, 147, 122);
		complexityFive.child(26, 43841, 125, 138);
		complexityFive.child(27, 43842, 147, 137);
		complexityFive.child(28, 43843, 125, 150);
		complexityFive.child(29, 43844, 147, 152);
		complexityFive.child(30, 43845, 125, 165);
		complexityFive.child(31, 43846, 147, 167);

		complexityFive.hidden = false;

	}

	private static void alterFloors(int unlockedFloors, int startId) {
		for (int i = 0; i < unlockedFloors; i++) {
			int off = i * 2;
			RSInterface.interfaceCache[off + startId].hidden = true;
			RSInterface.interfaceCache[off + 1 + startId].hidden = false;
		}
		for (int i = unlockedFloors; i < 60; i++) {
			int off = i * 2;
			RSInterface.interfaceCache[off + startId].hidden = true;
			RSInterface.interfaceCache[off + 1 + startId].hidden = true;
		}
	}

	private static Runnable alterUnlocked(final int baseId, final int unlockedFloorsId, final int start) {
		return new Runnable() {
			@Override
			public void run() {
				int unlockedFloors = Client.variousSettings[unlockedFloorsId];
				for (int i = 0; i < unlockedFloors; i++) {
					RSInterface.interfaceCache[start + i * 2].hidden = !Client.normalizeBitToggled(baseId, i);
				}
			}
		};
	}

	public static void dungeoneeringFloors() {
		RSInterface tab = addInterface(46040, InterfaceType.MAIN);

		addSprite(46041, 581);
		addSprite(46042, 582);
		addButton(46043, 584, 583, "Confirm");
		addText(46044, "Select Floor", 2, 0xc69b01, false, true, 0);
		addText(46045, "Floor", 2, 0xc69b01, false, true, 0);
		addText(46046, "Confirm", 1, 0xc69b01, false, true, 0);
		addText(46047, "8", 2, 0xe2e2a2, true, true, 0);
		addNewText(46048, "The deeper you go down into Daemonheim,<br>" + "the more powerful the creatures become.<br>"
				+ "As the difficulty increases with depth, so<br>" + "does the Dungeoneering XP reward you<br>"
				+ "receive at the end of a raid.<br><br>" + "When you first start Dungeoneering, only<br>"
				+ "floor 1 will be unlocked. You must<br>" + "coplete a raid of a floor before you can<br>"
				+ "unlock the next one, and you may only<br>" + "access a floor if you have the necessary<br>"
				+ "Dungeoneering skill level.<br><br>" + "All members of your party must have the<br>"
				+ "floor unlocked for you to raid it. To see<br>" + "which floors are unlocked by all players,<br>"
				+ "look at the columns that run vertically<br>" + "down the screen to the left - there's a<br>"
				+ "column for each member of your group.", 240, 300, 1, 0xc69b01, 0, true, 0, 0, 0);
		addSprite(46055, 586);
		RSInterface.interfaceCache[46055].hidden = true;// Black bg

		tab.totalChildren(12);
		tab.child(0, 46041, 2, 2);
		tab.child(1, 46042, 127, 270);
		tab.child(2, 46043, 169, 266);
		tab.child(3, 39702, 490, 5); // close button
		tab.child(4, 39703, 490, 5); // close hover button
		tab.child(5, 46044, 200, 5);
		tab.child(6, 46045, 77, 274);
		tab.child(7, 46046, 184, 274);
		tab.child(8, 46047, 139, 275);
		tab.child(9, 46048, 264, 30);
		tab.child(10, 46056, 16, 31);
		tab.child(11, 46055, 10, 170);

		RSInterface scroll = addInterface(46056);

		addSprite(46057, 585);
		RSInterface.interfaceCache[46057].setFlag(RSInterface.IGNORE_BOUNDS_BIT);

		// First
		int width = 15 * 3;
		addSprite(27768, 592, 15, 15);
		addSprite(27769, 593, width, 15);
		RSInterface.interfaceCache[27769].setFlag(RSInterface.TILING_FLAG_BIT);
		addSprite(27770, 592, 15, 15);
		RSInterface.interfaceCache[27770].flipHorizontal();

		// Body
		int height = 106 * 5 + 65;
		addSprite(27771, 599, 15, height);
		RSInterface.interfaceCache[27771].setFlag(RSInterface.TILING_FLAG_BIT);
		addSprite(27772, 600, width, height);
		RSInterface.interfaceCache[27772].setFlag(RSInterface.TILING_FLAG_BIT);
		addSprite(27773, 599, 15, height);
		RSInterface.interfaceCache[27773].flipHorizontal();
		RSInterface.interfaceCache[27773].setFlag(RSInterface.TILING_FLAG_BIT);

		// Last
		addSprite(27774, 595, 15, 15);
		addSprite(27775, 596, width, 15);
		RSInterface.interfaceCache[27775].setFlag(RSInterface.TILING_FLAG_BIT);
		addSprite(27776, 595, 15, 15);
		RSInterface.interfaceCache[27776].flipHorizontal();

		// floor display
		addSprite(27777, 664, 8, 600);

		final int start = 23821;
		final int childCount = 120;
		addFloors(start, 1352);
		final int start2 = start + childCount;
		addFloors(start2, 1353);
		final int start3 = start2 + childCount;
		addFloors(start3, 1354);
		final int start4 = start3 + childCount;
		addFloors(start4, 1355);
		final int start5 = start4 + childCount;
		addFloors(start5, 1356);

		final int clickboxStart = 24446;
		for (int i = 0; i < 60; i++) {
			addRectangleClickable(clickboxStart + i, 255, 0xff0000, false, 210, 10, "Select-floor", 1357);
		}

		Runnable updateFloors = new Runnable() {
			@Override
			public void run() {
				int floor1 = Client.variousSettings[8200];
				int floor2 = Client.variousSettings[8201];
				int floor3 = Client.variousSettings[8202];
				int floor4 = Client.variousSettings[8203];
				int floor5 = Client.variousSettings[8204];
				int floors = Math.max(floor1, Math.max(floor2, Math.max(floor3, Math.max(floor4, floor5))));

				for (int i = 0; i < floors; i++) {
					RSInterface.interfaceCache[i + clickboxStart].hidden = false;
				}

				for (int i = floors; i < 60; i++) {
					RSInterface.interfaceCache[i + clickboxStart].hidden = true;
				}

				alterFloors(floor1, start);
				alterFloors(floor2, start2);
				alterFloors(floor3, start3);
				alterFloors(floor4, start4);
				alterFloors(floor5, start5);
			}
		};

		Runnable checkComplete1 = alterUnlocked(8205, 8200, start);
		Runnable checkComplete2 = alterUnlocked(8207, 8201, start2);
		Runnable checkComplete3 = alterUnlocked(8209, 8202, start3);
		Runnable checkComplete4 = alterUnlocked(8211, 8203, start4);
		Runnable checkComplete5 = alterUnlocked(8213, 8204, start5);

		scroll.addOnVarpUpdate(8200, updateFloors);
		scroll.addOnVarpUpdate(8201, updateFloors);
		scroll.addOnVarpUpdate(8202, updateFloors);
		scroll.addOnVarpUpdate(8203, updateFloors);
		scroll.addOnVarpUpdate(8204, updateFloors);
		scroll.addOnVarpUpdate(8205, checkComplete1);
		scroll.addOnVarpUpdate(8206, checkComplete1);
		scroll.addOnVarpUpdate(8207, checkComplete2);
		scroll.addOnVarpUpdate(8208, checkComplete2);
		scroll.addOnVarpUpdate(8209, checkComplete3);
		scroll.addOnVarpUpdate(8210, checkComplete3);
		scroll.addOnVarpUpdate(8211, checkComplete4);
		scroll.addOnVarpUpdate(8212, checkComplete4);
		scroll.addOnVarpUpdate(8213, checkComplete5);
		scroll.addOnVarpUpdate(8214, checkComplete5);

		int pointer = clickboxStart + 60;
		addSprite(pointer, 662, 87, 12);
		addSprite(pointer + 1, 663, 124, 20);

		int text = pointer + 2;
		addText(text, "1", 2, 0xe2e2a2, true, true, 0);
		addText(text + 1, "2", 2, 0xe2e2a2, true, true, 0);
		addText(text + 2, "3", 2, 0xe2e2a2, true, true, 0);
		addText(text + 3, "4", 2, 0xe2e2a2, true, true, 0);
		addText(text + 4, "5", 2, 0xe2e2a2, true, true, 0);

		addNewText(text + 5, "Floor", 20, 20, 1, 0xc69b01, 0, true, 0, 0, 0);
		addNewText(text + 6, "1", 15, 20, 1, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(text + 7, "Frozen", 70, 20, 1, 0xc69b01, 0, true, 1, 1, 0);

		scroll.totalChildren(15 + (childCount * 5) + 60 + 2 + 8);
		scroll.child(0, 46057, -40, -15);
		int x = 3;
		int initialY = 47;
		int y = initialY;
		scroll.child(1, 27768, x, y);
		x += 15;
		scroll.child(2, 27769, 18, y);
		x += width;
		scroll.child(3, 27770, 62, y);
		y += 15;
		scroll.child(4, 27771, 3, y);
		scroll.child(5, 27772, 18, y);
		scroll.child(6, 27773, 62, y);
		y += height;
		scroll.child(7, 27774, 3, y);
		scroll.child(8, 27775, 18, y);
		scroll.child(9, 27776, 62, y);

		y = initialY + 15;
		scroll.child(10, 27777, 12, y);
		scroll.child(11, 27777, 24, y);
		scroll.child(12, 27777, 36, y);
		scroll.child(13, 27777, 48, y);
		scroll.child(14, 27777, 60, y);
		appendFloors(scroll, 12, y, start, 15);
		appendFloors(scroll, 24, y, start2, 15 + 120);
		appendFloors(scroll, 36, y, start3, 15 + 120 + 120);
		appendFloors(scroll, 48, y, start4, 15 + 120 + 120 + 120);
		appendFloors(scroll, 60, y, start5, 15 + 120 + 120 + 120 + 120);

		int index = 15 + 120 + 120 + 120 + 120 + 120;
		y = initialY + 15;
		for (int i = 0; i < 60; i++) {
			scroll.child(index++, clickboxStart + i, 3, y);
			y += 10;
		}

		y = initialY + 15;
		scroll.child(index++, pointer, 10, y);
		scroll.child(index++, pointer + 1, 10 + 87, y - 4);

		scroll.child(index++, text, 15, initialY);
		scroll.child(index++, text + 1, 27, initialY);
		scroll.child(index++, text + 2, 40, initialY);
		scroll.child(index++, text + 3, 52, initialY);
		scroll.child(index++, text + 4, 65, initialY);
		scroll.child(index++, text + 5, 105, initialY + 13);
		scroll.child(index++, text + 6, 135, initialY + 11);
		scroll.child(index++, text + 7, 150, initialY + 11);

		scroll.width = 221;
		scroll.height = 200;
		scroll.scrollMax = 700;
	}

	private static void appendFloors(RSInterface scroll, int x, int y, int start, int startIndex) {
		int progressY = y;
		for (int i = 1; i < 121; i++) {
			int real = i - 1;
			boolean check = i % 2 == 0;
			scroll.child(startIndex + real, start + real, x, !check ? progressY + 1 : progressY);
			if (check) {
				progressY += 10;
			}
		}
	}

	private static void addFloors(int start, int clientCodeCheck) {
		int childId = start;
		for (int i = 0; i < 60; i++) {
			addSprite(childId++, 661, 8, 9);
			addSprite(childId, 601 + i, 8, 10);
			RSInterface.interfaceCache[childId++].transparency = 100;
		}
	}

	public static void houseOptions() {
		int tabId = 46000;
		RSInterface tab = addInterface(tabId, InterfaceType.TAB);

		addButton(45999, 72, 73, "Close");
		addText(46001, "House Options", 2, 0xff9900, false, true, 0);
		addText(46002, "Building Mode", 1, 0xff9900, false, true, 0);
		addText(46003, "Render doors open", 1, 0xff9900, false, true, 0);
		addText(46004, "Teleport Inside", 1, 0xff9900, false, true, 0);
		addText(46005, "Number of rooms: 1", 1, 0xff9900, true, true, 0);

		addText(46006, "On", 0, 0xff9900, false, true, 0);
		addText(46007, "Off", 0, 0xff9900, false, true, 0);
		addRadioButton(46008, tabId, 980, 981, 15, 15, "Select", 1, 5, 8100);
		addRadioButton(46010, tabId, 980, 981, 15, 15, "Select", 2, 5, 8100);

		addRadioButton(46012, tabId, 980, 981, 15, 15, "Select", 1, 5, 8101);
		addRadioButton(46014, tabId, 980, 981, 15, 15, "Select", 2, 5, 8101);

		addRadioButton(46016, tabId, 980, 981, 15, 15, "Select", 1, 5, 8102);
		addRadioButton(46018, tabId, 980, 981, 15, 15, "Select", 2, 5, 8102);

		addButton(46020, 706, 706, "View");
		addButton(46021, 705, 705, "Select"); // these are the sprites

		addButton(46009, 705, 705, "Select");
		addButton(46011, 705, 705, "Select");
		addButton(46013, 705, 705, "Select");

		addNewText(46022, "View     ", 105, 40, 1, 0xff9900, 0xffffff, true, 2, 1, 0);
		addNewText(46023, "Expel Guests", 166, 32, 1, 0xff9900, 0xffffff, true, 1, 1, 0);
		addNewText(46024, "Leave House", 166, 32, 1, 0xff9900, 0xffffff, true, 1, 1, 0);
		addNewText(46025, "Call Servant", 166, 32, 1, 0xff9900, 0xffffff, true, 1, 1, 0);

		tab.totalChildren(22);
		tab.child(0, 45999, 162, 7); // close button
		tab.child(1, 46001, 40, 4);
		tab.child(2, 46002, 8, 69);
		tab.child(3, 46003, 8, 89);
		tab.child(4, 46004, 8, 109);
		tab.child(5, 46005, 92, 240);
		tab.child(6, 46006, 137, 54);
		tab.child(7, 46007, 160, 54);
		tab.child(8, 46008, 136, 69);
		tab.child(9, 46010, 162, 69);
		tab.child(10, 46012, 136, 89);
		tab.child(11, 46014, 162, 89);
		tab.child(12, 46016, 136, 109);
		tab.child(13, 46018, 162, 109);
		tab.child(14, 46020, 8, 25);
		tab.child(15, 46009, 8, 130); // expel guests
		tab.child(16, 46011, 8, 166);
		tab.child(17, 46013, 8, 203);
		tab.child(18, 46022, 8, 25);
		tab.child(19, 46023, 8, 130);
		tab.child(20, 46024, 8, 166);
		tab.child(21, 46025, 8, 203);
	}

	public static void dungeoneeringRewards() {
		RSInterface tab = addInterface(44200, InterfaceType.MAIN);
		tab.addOnVarpUpdate(ConfigCodes.AMOUNT_OF_PLAYERS, new Runnable() {
			@Override
			public void run() {
				int count = Client.variousSettings[ConfigCodes.AMOUNT_OF_PLAYERS];
				if (count > 5) {
					return;
				}

				int baseId = 50020;
				int baseIdName = 50040;
				for (int i = 0; i < 5; i++) {
					RSInterface.interfaceCache[baseId + i].hidden = true;
					RSInterface.interfaceCache[baseIdName + i].hidden = true;
				}

				for (int i = 0; i < count; i++) {
					RSInterface.interfaceCache[baseId + i].hidden = false;
					RSInterface.interfaceCache[baseIdName + i].hidden = false;
				}
			}
		});
		tab.addOnVarpUpdate(ConfigCodes.PLAYERS_STATES, new Runnable() {
			@Override
			public void run() {
				int data = Client.variousSettings[ConfigCodes.PLAYERS_STATES];
				
				int state1 = data & 0x3;
				int color1 = 16757760;
				if (state1 == 1) {
					color1 = 3928883;
				} else if (state1 == 2) {
					color1 = 16713013;
				}
				RSInterface.interfaceCache[50026].disabledColor = color1;
				
				int state2 = (data >> 2) & 0x3;
				int color2 = 16757760;
				if (state2 == 1) {
					color2 = 3928883;
				} else if (state2 == 2) {
					color2 = 16713013;
				}
				RSInterface.interfaceCache[50029].disabledColor = color2;
				
				int state3 = data >> 4 & 0x3;
				int color3 = 16757760;
				if (state3 == 1) {
					color3 = 3928883;
				} else if (state3 == 2) {
					color3 = 16713013;
				}
				RSInterface.interfaceCache[50032].disabledColor = color3;
				
				int state4 = data >> 6 & 0x3;
				int color4 = 16757760;
				if (state4 == 1) {
					color4 = 3928883;
				} else if (state4 == 2) {
					color4 = 16713013;
				}
				RSInterface.interfaceCache[50035].disabledColor = color4;

				int state5 = data >> 8 & 0x3;
				int color5 = 16757760;
				if (state5 == 1) {
					color5 = 3928883;
				} else if (state5 == 2) {
					color5 = 16713013;
				}
				RSInterface.interfaceCache[50038].disabledColor = color5;
			}
		});

		String[] ops = { "Ready", "Leave" };

		addSprite(50025, 1202, 15, 16);
		RSInterface.interfaceCache[50025].actions = ops;
		addRectangle(50026, 0, 16757760, false, 13, 14, 0);
		addNewText(50027, "1", 13, 15, 1, 0xc69b01, 0, true, 1, 1, 0);
		RSInterface click1 = addInterface(50020, InterfaceType.MAIN);
		click1.totalChildren(3);
		click1.child(0, 50025, 0, 0);
		click1.child(1, 50026, 1, 1);
		click1.child(2, 50027, 1, 1);
		click1.width = 16;
		click1.height = 16;

		addSprite(50028, 1202, 15, 16);
		RSInterface.interfaceCache[50028].actions = ops;
		addRectangle(50029, 0, 16757760, false, 13, 14, 0);
		addNewText(50030, "2", 13, 15, 1, 0xc69b01, 0, true, 1, 1, 0);
		RSInterface click2 = addInterface(50021, InterfaceType.MAIN);
		click2.totalChildren(3);
		click2.child(0, 50028, 0, 0);
		click2.child(1, 50029, 1, 1);
		click2.child(2, 50030, 1, 1);
		click2.width = 16;
		click2.height = 16;

		addSprite(50031, 1202, 15, 16);
		RSInterface.interfaceCache[50031].actions = ops;
		addRectangle(50032, 0, 16757760, false, 13, 14, 0);
		addNewText(50033, "3", 13, 15, 1, 0xc69b01, 0, true, 1, 1, 0);
		RSInterface click3 = addInterface(50022, InterfaceType.MAIN);
		click3.totalChildren(3);
		click3.child(0, 50031, 0, 0);
		click3.child(1, 50032, 1, 1);
		click3.child(2, 50033, 1, 1);
		click3.width = 16;
		click3.height = 16;
		
		addSprite(50034, 1202, 15, 16);
		RSInterface.interfaceCache[50034].actions = ops;
		addRectangle(50035, 0, 16757760, false, 13, 14, 0);
		addNewText(50036, "4", 13, 15, 1, 0xc69b01, 0, true, 1, 1, 0);
		RSInterface click4 = addInterface(50023, InterfaceType.MAIN);
		click4.totalChildren(3);
		click4.child(0, 50034, 0, 0);
		click4.child(1, 50035, 1, 1);
		click4.child(2, 50036, 1, 1);
		click4.width = 16;
		click4.height = 16;
		
		addSprite(50037, 1202, 15, 16);
		RSInterface.interfaceCache[50037].actions = ops;
		addRectangle(50038, 0, 16757760, false, 13, 14, 0);
		addNewText(50039, "5", 13, 15, 1, 0xc69b01, 0, true, 1, 1, 0);
		RSInterface click5 = addInterface(50024, InterfaceType.MAIN);
		click5.totalChildren(3);
		click5.child(0, 50037, 0, 0);
		click5.child(1, 50038, 1, 1);
		click5.child(2, 50039, 1, 1);
		click5.width = 16;
		click5.height = 16;
		
		// Deahts
		for (int i = 0; i < 15; i++) {
			addSprite(29976 + i, 777, 19, 19);
		}

		tab.addOnVarpUpdate(ConfigCodes.DUNGEONEERING_DEATHS, new Runnable() {
			@Override
			public void run() {
				int value = Client.variousSettings[ConfigCodes.DUNGEONEERING_DEATHS];
				int start = 29976;
				int length = 15;
				for (int i = 0; i < length; i++) {
					RSInterface.interfaceCache[start + i].hidden = true;
				}

				if (value <= 0 || value > 15) {
					return;
				}

				Sprite redSkull = SpriteCache.get(776);
				Sprite brownSkull = SpriteCache.get(777);

				if (value == 1) {
					RSInterface.interfaceCache[start].hidden = false;
					RSInterface.interfaceCache[start].disabledSprite = redSkull;
				} else {
					value--;
					for (int i = 0; i < value; i++) {
						int index = start + i;
						RSInterface.interfaceCache[index].hidden = false;
						RSInterface.interfaceCache[index].disabledSprite = brownSkull;
					}

					RSInterface.interfaceCache[start + value].hidden = false;
					RSInterface.interfaceCache[start + value].disabledSprite = redSkull;
				}
			}
		});

		tab.addOnVarpUpdate(ConfigCodes.BONUS_ROOMS, new Runnable() {
			@Override
			public void run() {
				int value = Client.variousSettings[ConfigCodes.BONUS_ROOMS];
				int max = 13;
				int width = 176;

				int scale = value * width / max;
				RSInterface.interfaceCache[29992].width = scale;

				if (value > 0) {
					RSInterface.interfaceCache[44200].childX[39] = (short) (129 + scale);
					RSInterface.interfaceCache[29994].hidden = false;
				} else {
					RSInterface.interfaceCache[29994].hidden = true;
				}
			}
		});

		tab.addOnVarpUpdate(ConfigCodes.TOTAL_MODIFIER, new Runnable() {
			@Override
			public void run() {
				int value = Client.variousSettings[ConfigCodes.TOTAL_MODIFIER];
				int max = 200;
				int width = 176;

				int scale = value * width / max;
				RSInterface.interfaceCache[23116].width = scale;

				if (value > 0) {
					RSInterface.interfaceCache[44200].childX[57] = (short) (129 + scale);
					RSInterface.interfaceCache[23118].hidden = false;
				} else {
					RSInterface.interfaceCache[23118].hidden = true;
				}
			}
		});

		// Bonus rooms
		addSprite(29992, 783, 20, 14);
		RSInterface.interfaceCache[29992].setFlag(CLIP_BIT);
		addSprite(29993, 784, 7, 14);
		addSprite(29994, 785, 7, 14);

		// Total modifier
		addSprite(23116, 786, 20, 20);
		RSInterface.interfaceCache[23116].setFlag(CLIP_BIT);
		addSprite(23117, 787, 7, 20);
		addSprite(23118, 788, 7, 20);

		addNewText(29995, "00:00:00", 50, 22, 0, 0xffffff, 0, true, 0, 0, 0);
		addSprite(44201, 569);
		addText(44202, "Congratulations! You have completed a Dungeon!", 2, 0xc69b01, false, true, 0);
		addText(44203, "Base XP", 2, 0xc69b01, false, true, 0);
		addText(44204, "Floor 1:", 1, 0xc69b01, false, true, 0);
		addText(44205, "Prestige 35", 1, 0xc69b01, false, true, 0);
		addText(44206, "Average:", 1, 0xc69b01, false, true, 0);
		addText(44207, "176", 1, 0xe2e2a2, true, true, 0);
		addText(44208, "2556", 1, 0xe2e2a2, true, true, 0);
		addText(44209, "1366", 1, 0xe2e2a2, true, true, 0);
		addText(44210, "Modifier", 2, 0xc69b01, false, true, 0);
		addText(44211, "Dungeon Size:", 1, 0xc69b01, false, true, 0);
		addText(44212, "Bonus Rooms:", 1, 0xc69b01, false, true, 0);
		addText(44213, "Difficulty:", 1, 0xc69b01, false, true, 0);
		addText(44214, "Complexity:", 1, 0xc69b01, false, true, 0);
		addText(44215, "Deaths:", 1, 0xc69b01, false, true, 0);
		addText(44216, "Total Modifier:", 1, 0xc69b01, false, true, 0);
		addText(44217, "Level Mod:", 1, 0xc69b01, false, true, 0);
		addText(44218, "Guide Mode:", 1, 0xc69b01, false, true, 0);
		addSprite(44219, 570, 571, 1997, 0, -1, 62, 20, "", true);
		addSprite(44221, 572, 573, 1997, 1, -1, 62, 20, "", true);
		addSprite(44223, 574, 575, 1997, 2, -1, 62, 20, "", true);
		addSprite(44225, 576);
		addSprite(44226, 577);
		addSprite(44227, 578);
		addSprite(44228, 579);
		addText(44229, "1 : 1", 1, 0xe2e2a2, true, true, 0);
		addText(44230, "1", 1, 0xe2e2a2, true, true, 0);
		addText(44231, "+0%", 0, 0xe2e2a2, false, true, 0);
		addText(44232, "-43%", 0, 0xe2e2a2, false, true, 0);

		addText(44233, "+0%", 0, 0xe2e2a2, false, true, 0);
		addText(44234, "+0%", 0, 0xe2e2a2, false, true, 0);
		addText(44235, "-12%", 0, 0xe2e2a2, false, true, 0);
		addText(44236, "-1%", 0, 0xe2e2a2, false, true, 0);
		addText(44237, "+0%", 0, 0xe2e2a2, false, true, 0);
		addText(44238, "41%", 1, 0xe2e2a2, false, true, 0);
		
		addNewText(44239, "Total XP:", 72, 30, 2, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44240, "159", 150, 30, 3, 0xe2e2a2, 0, true, 0, 1, 0);
		addNewText(44241, "Tokens:", 60, 30, 2, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44242, "15", 72, 30, 3, 0xe2e2a2, 0, true, 0, 1, 0);

		addNewText(50040, "Julius", 72, 20, 2, 0xc69b01, 0, true, 0, 1, 0);
		addNewText(50041, "Julius dog", 150, 20, 2, 0xe2e2a2, 0, true, 0, 1, 0);
		addNewText(50042, "Julius cat", 60, 20, 2, 0xc69b01, 0, true, 0, 1, 0);
		addNewText(50043, "Julius croc", 72, 20, 2, 0xe2e2a2, 0, true, 0, 1, 0);
		addNewText(50044, "Julius horse", 72, 20, 2, 0xe2e2a2, 0, true, 0, 1, 0);

		tab.totalChildren(40 + 15 + 3 + 5 + 5 + 5);
		tab.child(0, 44201, 14, 14);
		tab.child(1, 44202, 97, 19);
		tab.child(2, 44203, 67, 40);
		tab.child(3, 44204, 65, 65);
		tab.child(4, 44205, 156, 65);
		tab.child(5, 44206, 267, 65);
		tab.child(6, 44207, 85, 82);
		tab.child(7, 44208, 189, 82);
		tab.child(8, 44209, 291, 82);
		tab.child(9, 44210, 67, 109);
		tab.child(10, 44211, 39, 132);
		tab.child(11, 44212, 40, 154);
		tab.child(12, 44213, 62, 176);
		tab.child(13, 44214, 53, 198);
		tab.child(14, 44215, 75, 222);
		tab.child(15, 44216, 38, 250);
		tab.child(16, 44217, 252, 176);
		tab.child(17, 44218, 243, 198);
		tab.child(18, 44219, 130, 128);
		tab.child(19, 44221, 192, 128);
		tab.child(20, 44223, 254, 128);
		tab.child(21, 44225, 127, 154);
		tab.child(22, 44226, 127, 216);
		tab.child(23, 44227, 127, 245);
		tab.child(24, 44228, 156, 178);
		tab.child(25, 44228, 156, 200);
		tab.child(26, 44229, 138, 177);
		tab.child(27, 44230, 138, 199);
		tab.child(28, 44231, 178, 178);
		tab.child(29, 44232, 178, 200);

		tab.child(30, 44233, 320, 133);
		tab.child(31, 44234, 320, 155);
		tab.child(32, 44235, 320, 177);
		tab.child(33, 44236, 320, 199);
		tab.child(34, 44237, 320, 223);
		tab.child(35, 44238, 320, 249);
		tab.child(36, 29995, 38, 21);
		tab.child(37, 29992, 132, 155);
		tab.child(38, 29993, 128, 155);
		tab.child(39, 29994, 305, 155);

		for (int i = 0; i < 15; i++) {
			tab.child(40 + i, 29976 + i, 130 + i * 11, 219);
		}

		tab.child(55, 23116, 132, 247);
		tab.child(56, 23117, 128, 247);
		tab.child(57, 23118, 305, 247);
		tab.child(58, 50020, 423, 19);
		tab.child(59, 50021, 423+14, 19);
		tab.child(60, 50022, 423+14+14, 19);
		tab.child(61, 50023, 423+14+14+14, 19);
		tab.child(62, 50024, 423+14+14+14+14, 19);
		tab.child(63, 44239, 53, 278);
		tab.child(64, 44240, 158, 278);
		tab.child(65, 44241, 225, 278);
		tab.child(66, 44242, 295, 278);
		tab.child(67, 44243, 127, 227);
		tab.child(68, 50040, 365, 43);
		tab.child(69, 50041, 365, 103);
		tab.child(70, 50042, 365, 163);
		tab.child(71, 50043, 365, 223);
		tab.child(72, 50044, 365, 283);
		
		RSInterface unbalance = addInterface(44243, InterfaceType.TAB);
		
		addSprite(44244, 1203, 188, 18);
		addNewText(44245, "Unbalanced party penalty:", 130, 18, 0, 0xff3333, 0, true, 0, 1, 0);
		addNewText(44246, "x48%", 30, 18, 0, 0xff3333, 0, true, 0, 1, 0);
		
		
		unbalance.totalChildren(3);
		unbalance.child(0, 44244, 0, 0);
		unbalance.child(1, 44245, 12, 1);
		unbalance.child(2, 44246, 145, 1);
		
		unbalance.hidden = true;
		
	}

	public static void dungeoneeringInvite() {
		RSInterface tab = addInterface(44100, InterfaceType.MAIN);

		addSprite(44101, 562);
		addButton(44102, 563, 564, "Accept");
		addButton(44103, 565, 566, "Decline");
		addButton(44104, 567, 568, "Decline forever");
		addNewText(44105, "Accept", 72, 33, 1, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44106, "Decline", 72, 33, 1, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44107, "Decline forever", 72, 33, 1, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44108, "You Are Invited to a Dungeon Party", 475, 20, 2, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44109, "Name", 160, 25, 0, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44110, "Dungeoneering level", 76, 25, 0, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44111, "Combat level", 62, 25, 0, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44112, "Highest<br>skill level", 68, 25, 0, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44113, "Total<br>skill level", 90, 25, 0, 0xc69b01, 0, true, 1, 1, 0);
		addNewText(44114, "Floor", 40, 20, 2, 0xc69b01, 0, true, 0, 1, 0);
		addNewText(44115, "Complexity", 80, 20, 2, 0xc69b01, 0, true, 0, 1, 0);
		addNewText(44116, "0", 20, 20, 2, 0xe2e2a2, 0, true, 1, 1, 0);
		addNewText(44117, "0", 20, 20, 2, 0xe2e2a2, 0, true, 1, 1, 0);
		addNewText(44118, "Do you accept the invitation?", 460, 20, 2, 0xc69b01, 0, true, 1, 1, 0);
		
		int players = 5;
		int baseId = 44119;
		int boxSizeY = 19;
		tab.totalChildren(25 + 20);
		for (int i = 0; i < players; i++) {
			addNewText(baseId + i * players, "Playername", 140, boxSizeY, 2, 0xc69b01, 0, true, 0, 1, 0);
			addNewText(baseId + i * players + 1, "1", 76, boxSizeY, 2, 0xc69b01, 0, true, 1, 1, 0);
			addNewText(baseId + i * players + 2, "1", 62, boxSizeY, 2, 0xc69b01, 0, true, 1, 1, 0);
			addNewText(baseId + i * players + 3, "1", 68, boxSizeY, 2, 0xc69b01, 0, true, 1, 1, 0);
			addNewText(baseId + i * players + 4, "1", 90, boxSizeY, 2, 0xc69b01, 0, true, 1, 1, 0);
			
			tab.child(20 + i * players, baseId + i * players, 36, 81 + i * boxSizeY);
			tab.child(20 + i * players + 1, baseId + i * players + 1, 186, 81 + i * boxSizeY);
			tab.child(20 + i * players + 2, baseId + i * players + 2, 263, 81 + i * boxSizeY);
			tab.child(20 + i * players + 3, baseId + i * players + 3, 326, 81 + i * boxSizeY);
			tab.child(20 + i * players + 4, baseId + i * players + 4, 395, 81 + i * boxSizeY);
			
		}

		tab.child(0, 44101, 14, 28);
		tab.child(1, 39702, 467, 30); // close button
		tab.child(2, 39703, 467, 30); // close hover button
		tab.child(3, 44102, 127, 254);
		tab.child(4, 44103, 217, 254);
		tab.child(5, 44104, 307, 254);
		tab.child(6, 44105, 127, 254);
		tab.child(7, 44106, 217, 254);
		tab.child(8, 44107, 307, 254);
		tab.child(9, 44108, 15, 30);
		tab.child(10, 44109, 25, 53);
		tab.child(11, 44110, 186, 53);
		tab.child(12, 44111, 263, 53);
		tab.child(13, 44112, 326, 53);
		tab.child(14, 44113, 395, 53);
		tab.child(15, 44114, 215, 178);
		tab.child(16, 44115, 176, 203);
		tab.child(17, 44116, 279, 178);
		tab.child(18, 44117, 279, 203);
		tab.child(19, 44118, 22, 232);
		//tab.child(20, 44121, 36, 80);
	}

	public static void dungeoneeringProgress() {
		RSInterface tab = addInterface(50100, InterfaceType.MAIN);

		addSprite(50101, 1209);
		Sprite sprite = addSprite(50102, 1210);
		RSInterface.interfaceCache[50102].setFlag(CLIP_BIT);
		RSInterface.interfaceCache[50102].addOnVarpUpdate(ConfigCodes.DUNG_PROGRESS_BAR, new Runnable() {
			@Override
			public void run() {
				RSInterface.interfaceCache[50102].width = Client.variousSettings[ConfigCodes.DUNG_PROGRESS_BAR];
			}
		});
		addNewText(50103, "Bulwark Beast's Armor", 199, 13, 0, 0xff9b00, 0, false, 1, 1, 0);
		tab.totalChildren(3);
		int offsetX = tab.width / 2 - sprite.myWidth / 2;
		int offsetY = 10;
		tab.child(0, 50101, offsetX, offsetY);
		tab.child(1, 50102, offsetX + 4, offsetY + 15);
		tab.child(2, 50103, offsetX + 4, offsetY + 4);
	}

	public static void dungeoneeringTab() {
		RSInterface tab = addInterface(44026, InterfaceType.TAB);

		addSprite(44027, 546);
		addButton(44028, 547, 548, "Reset");
		addText(44029, "Reset", 1, 0xcc9901, false, true, 0);
		addText(44030, "Current Progress", 0, 0xc69b01, false, true, 0);
		addText(44031, "Previous Progress", 0, 0xc69b01, false, true, 0);
		addText(44032, "0", 0, 0xe2e2a2, true, true, 0);
		addText(44033, "0", 0, 0xe2e2a2, true, true, 0);
		addText(44034, "Floor", 2, 0xc69b01, false, true, 0);
		addText(44035, "Complexity", 2, 0xc69b01, false, true, 0);
		addText(44036, "Guide Mode", 2, 0xc69b01, false, true, 0);
		addText(44037, "0", 2, 0xe2e2a2, true, true, 0);
		addText(44038, "0", 2, 0xe2e2a2, true, true, 0);
		addText(44039, "Party Details", 2, 0xc69b01, false, true, 0);
		addSprite(44040, 559);
		addText(44041, "", 0, 0xd23500, true, true, 0);
		addText(44042, "", 0, 0x008985, true, true, 0);
		addText(44043, "", 0, 0x488100, true, true, 0);
		addText(44044, "", 0, 0x919600, true, true, 0);
		addText(44045, "", 0, 0x6d865f, true, true, 0);
		String[] ops = { "Inspect", "Kick", "Promote" };
		RSInterface.interfaceCache[44041].actions = ops;
		RSInterface.interfaceCache[44042].actions = ops;
		RSInterface.interfaceCache[44043].actions = ops;
		RSInterface.interfaceCache[44044].actions = ops;
		RSInterface.interfaceCache[44045].actions = ops;
		addButton(44046, 300, 301, "Close");

		tab.totalChildren(19);
		tab.child(0, 44027, -2, 0); // Background
		tab.child(1, 44030, 3, 231);
		tab.child(2, 44031, 3, 245);
		tab.child(3, 44032, 114, 231);
		tab.child(4, 44033, 114, 245);
		tab.child(5, 44034, 5, 155);
		tab.child(6, 44035, 5, 180);
		tab.child(7, 44036, 5, 207);
		tab.child(8, 44037, 102, 155);
		tab.child(9, 44038, 102, 183);
		tab.child(10, 44039, 50, 1);
		tab.child(11, 44051, 0, 0);
		tab.child(12, 44071, 0, 0);
		tab.child(13, 44041, 90, 31);
		tab.child(14, 44042, 90, 46);
		tab.child(15, 44043, 90, 61);
		tab.child(16, 44044, 90, 76);
		tab.child(17, 44045, 90, 91);
		tab.child(18, 44046, 169, 1); // close button

		RSInterface single = addInterface(44051, InterfaceType.TAB);
		addButton(44052, 549, 550, "Form a party");
		addNewText(44053, "Form party", 180, 32, 1, 0xcc9901, 0, true, 1, 1, 0);
		addSprite(44054, 555);
		addText(44055, "Change", 0, 0x382b01, false, true, 0);
		addSprite(44056, 558);
		addSprite(44057, 1204, 1205, 20, 19);

		single.totalChildren(12);
		single.child(0, 44052, 3, 111);
		single.child(1, 44053, 3, 111);
		single.child(2, 44054, 120, 152);
		single.child(3, 44055, 132, 156);
		single.child(4, 44054, 120, 180);
		single.child(5, 44055, 132, 184);
		single.child(6, 44056, 8, 30);
		single.child(7, 44056, 30, 30);
		single.child(8, 44040, 5, 29);
		single.child(9, 44028, 130, 230);
		single.child(10, 44029, 140, 235);
		single.child(11, 44057, 160, 205);

		single.hidden = false;

		RSInterface team = addInterface(44071, InterfaceType.TAB);
		addButton(44072, 553, 554, "Leave party");
		addText(44073, "Leave party", 1, 0xcc9901, false, true, 0);
		addButton(44074, 551, 552, "Invite player");
		addText(44075, "Invite player", 1, 0xcc9901, false, true, 0);

		addButton(44076, 556, 557, "Change");
		addButton(44077, 556, 557, "Change");
		addText(44078, "Change", 0, 0xcc9901, false, true, 0);
		addSprite(44079, 1205, 1206, 20, 19);
		addSprite(44080, -1, 1207, ConfigCodes.DUNG_GUIDE_MODE, 1, -1, 20, 19, "Toggle Guide mode", true);
		
		team.totalChildren(14);
		team.child(0, 44072, 3, 111);
		team.child(1, 44073, 14, 119);
		team.child(2, 44074, 93, 111);
		team.child(3, 44075, 104, 119);
		team.child(4, 44076, 120, 152);
		team.child(5, 44078, 132, 156);
		team.child(6, 44077, 120, 180);
		team.child(7, 44078, 132, 184);
		team.child(8, 44056, 8, 30);
		team.child(9, 44040, 5, 29);
		team.child(10, 44028, 130, 230);
		team.child(11, 44029, 140, 235);
		team.child(12, 44079, 160, 205);
		team.child(13, 44080, 160, 205);

		team.hidden = true;

	}

	public static void teleportInterface() {
		RSInterface tab = addInterface(25411, InterfaceType.MAIN);

		addSprite(25412, 985);
		addSprite(25413, 486);
		addSprite(25414, 486);
		addSprite(25415, 484);
		addSprite(25416, 484);
		addSprite(25417, 484);
		addSprite(25418, 988, -1, -1, 0, -1, 100, 20, "Search", true);
		addText(29997, "Search...", 1, 0xffffff, false, true, 0);
		RSInterface.interfaceCache[29997].contentType = 1350;
		addText(25419, "Teleports", 2, 0xff9b00, true, true, 0);
		addText(25420, "Name", 2, 0xff9b00, true, true, 0);
		addText(25421, "Level", 2, 0xff9b00, true, true, 0);
		addText(25422, "Danger", 2, 0xff9b00, true, true, 0);

		tab.totalChildren(35);
		tab.child(0, 39701, 14, 14); // Background
		tab.child(1, 25453, 21, 85); // Categories
		tab.child(2, 25653, 152, 87); // Cities
		tab.child(3, 39702, 474, 26); // close
		tab.child(4, 39703, 474, 26); // close
		tab.child(5, 25412, 20, 82);
		tab.child(6, 25413, 298, 81);
		tab.child(7, 25414, 146, 81);
		tab.child(8, 25415, 146, 53);
		tab.child(9, 25416, 146, 160);
		tab.child(10, 25417, 146, 205);
		tab.child(11, 25418, 31, 57);
		tab.child(12, 25419, 270, 25);
		tab.child(13, 25420, 225, 59);
		tab.child(14, 25421, 335, 59);
		tab.child(15, 25422, 430, 59);
		tab.child(16, 25853, 152, 87); // Monsters
		tab.child(17, 26053, 152, 87); // Dungeons
		tab.child(18, 26253, 152, 87); // Bosses
		tab.child(19, 26453, 152, 87); // Minigames
		tab.child(20, 26653, 152, 87); // Wilderness
		tab.child(21, 26853, 152, 87); // Slayer
		tab.child(22, 27053, 152, 87); // Fishing
		tab.child(23, 27253, 152, 87); // Mining
		tab.child(24, 27453, 152, 87); // Woodcutting
		tab.child(25, 27753, 152, 87); // Agility
		tab.child(26, 28253, 152, 87); // sTheiving
		tab.child(27, 28453, 152, 87); // Hunter
		tab.child(28, 28753, 152, 87); // Farming
		tab.child(29, 28953, 152, 87); // Runecrafting
		tab.child(30, 29153, 152, 87); // Summoning
		tab.child(31, 29253, 152, 87); // Dungeoneering
		tab.child(32, 29453, 152, 87); // Construction
		tab.child(33, 29997, 36, 60); // Search text
		tab.child(34, 29998, 152, 87); // Search layer

		String[] array = { "Cities", "Monsters", "Dungeons", "Bosses", "Minigames", "Wilderness", "Slayer", "Fishing",
				"Mining", "Woodcutting", "Agility", "Thieving", "Hunter", "Farming", "Runecrafting", "Summoning",
				"Dungeoneering", "Construction" };

		int id = 25454;
		for (int i = 0; i < array.length; i++) {
			addRadioButton(id++, 25411, 490, 492, 108, 25, "Select", i + 1, 5, 1991);
			addNewText(id++, array[i], 108, 25, 1, 0xffffff, 0, false, 0, 1, 0);
		}

		// Category scroll
		RSInterface categories = addInterface(25453, InterfaceType.MAIN);

		categories.totalChildren(array.length * 2);
		id = 25454;
		int y = 0;
		int childID = 0;
		for (int i = 0; i < array.length; i++) {
			categories.child(childID++, id++, 0, y);
			categories.child(childID++, id++, 8, y);
			y += 25;
		}

		categories.width = 108;
		categories.height = 228;
		categories.scrollMax = 450;

		// Teleport scrolls
		RSInterface cities = addInterface(25653, InterfaceType.MAIN);

		addTeleport(cities, 25654, TeleportInfo.values.get(0)); // Cities
		cities.hidden = false;

		addTeleport(addInterface(25853, InterfaceType.MAIN), 25854, TeleportInfo.values.get(1)); // Monsters

		addTeleport(addInterface(26053, InterfaceType.MAIN), 26054, TeleportInfo.values.get(2)); // Dungeons

		addTeleport(addInterface(26253, InterfaceType.MAIN), 26254, TeleportInfo.values.get(3)); // Bosses

		addTeleport(addInterface(26453, InterfaceType.MAIN), 26454, TeleportInfo.values.get(4)); // Minigames

		addTeleport(addInterface(26653, InterfaceType.MAIN), 26654, TeleportInfo.values.get(5)); // Wilderness

		addTeleport(addInterface(26853, InterfaceType.MAIN), 26854, TeleportInfo.values.get(6)); // Slayer

		addTeleport(addInterface(27053, InterfaceType.MAIN), 27054, TeleportInfo.values.get(7)); // Fishing

		addTeleport(addInterface(27253, InterfaceType.MAIN), 27254, TeleportInfo.values.get(8)); // Mining

		addTeleport(addInterface(27453, InterfaceType.MAIN), 27454, TeleportInfo.values.get(9)); // Woodcutting

		addTeleport(addInterface(27753, InterfaceType.MAIN), 27754, TeleportInfo.values.get(10)); // Agility

		addTeleport(addInterface(28253, InterfaceType.MAIN), 28254, TeleportInfo.values.get(11)); // Theiving

		addTeleport(addInterface(28453, InterfaceType.MAIN), 28454, TeleportInfo.values.get(12)); // Hunter

		addTeleport(addInterface(28753, InterfaceType.MAIN), 28754, TeleportInfo.values.get(13)); // Farming

		addTeleport(addInterface(28953, InterfaceType.MAIN), 28954, TeleportInfo.values.get(14)); // Runecrafting

		addTeleport(addInterface(29153, InterfaceType.MAIN), 29154, TeleportInfo.values.get(15)); // Summoning

		addTeleport(addInterface(29253, InterfaceType.MAIN), 29254, TeleportInfo.values.get(16)); // Dungeoneering

		addTeleport(addInterface(29453, InterfaceType.MAIN), 29454, TeleportInfo.values.get(17)); // Construction

		addInterface(29998, InterfaceType.MAIN);
		RSInterface.interfaceCache[29998].contentType = 1351;
		RSInterface.interfaceCache[29998].hidden = true;
		RSInterface.interfaceCache[29998].width = 326;
		RSInterface.interfaceCache[29998].height = 226;
	}

	private static void addTeleport(RSInterface teleports, int id, List<TeleportInfo> values) {
		int tempId = id;
		int size = values.size();
		for (int i = 0; i < size; i++) {
			TeleportInfo info = values.get(i);
			int spriteId = i % 2 == 0 ? 1059 : 1060;
			addButton(tempId++, spriteId, 494, "Teleport");
			addNewText(tempId++, info.name, 100, 30, 1, 0xff9b00, 0, false, 0, 1, 0);
			addNewText(tempId++, info.level, 80, 30, 1, 0xff9b00, 0, false, 1, 1, 0);
			addNewText(tempId++, info.danger, 80, 30, 1, 0xff9b00, 0, false, 1, 1, 0);
		}

		tempId = id;
		int childID = 0;
		teleports.totalChildren(size * 4);
		int y = 0;
		for (int i = 0; i < size; i++) {
			teleports.child(childID++, tempId++, 0, y);
			teleports.child(childID++, tempId++, 4, y);
			teleports.child(childID++, tempId++, 146, y);
			teleports.child(childID++, tempId++, 240, y);
			y += 30;
		}

		teleports.width = 326;
		teleports.height = 226;

		if (y <= teleports.height) {
			y = teleports.height + 1;
		}

		teleports.scrollMax = y;
		teleports.hidden = true;
	}

	public static void compCustColor() {
		RSInterface tab = addInterface(25400);
		addText(25402, "Select Color...", 2, 0xff9b00, true, true);

		addButton(25406, 292, 293, "Accept color change");
		addText(25407, "Accept", 2, 0xffffff, true, true);

		addButton(25408, 292, 293, "Decline");
		addText(25409, "Decline", 2, 0xffffff, true, true);

		addContainer(25410, 512, 700, 1345);

		tab.totalChildren(9);
		tab.child(0, 39701, 14, 17); // background
		tab.child(1, 25402, 258, 28); // Title text
		tab.child(2, 39702, 475, 30); // close button
		tab.child(3, 39703, 475, 30); // close hover button
		tab.child(4, 25406, 80, 260); // accept button
		tab.child(5, 25407, 150, 268); // accept text
		tab.child(6, 25408, 270, 260); // decline button
		tab.child(7, 25409, 341, 268); // decline text
		tab.child(8, 25410, 130, 20); // the color overlay
	}

	public static void compCust() {
		RSInterface tab = addInterface(25300);
		addText(25302, "Cape Customizer", 2, 0xff9b00, true, true, 0);
		addText(25303, "First Color", 1, 0xff9b00, true, true, 0);
		addText(25304, "Second Color", 1, 0xff9b00, true, true, 0);
		addText(25305, "Third Color", 1, 0xff9b00, true, true, 0);
		addText(25306, "Fourth Color", 1, 0xff9b00, true, true, 0);
		addText(25307, "Preview Colors", 1, 0xff9b00, true, true, 0);

		addContainer(25312, 65, 63, 1346);
		addContainer(25313, 65, 63, 1347);
		addContainer(25314, 65, 63, 1348);
		addContainer(25315, 65, 63, 1349);
		addRecoloredModel(25316, 450);

		tab.totalChildren(14);
		tab.child(0, 39701, 14, 0); // Background
		tab.child(1, 25302, 258, 12); // Title text
		tab.child(2, 25303, 95, 68); // color1 text
		tab.child(3, 25304, 245, 68); // color2 text
		tab.child(4, 25305, 95, 198); // color3 text
		tab.child(5, 25306, 245, 198); // color4 text
		tab.child(6, 25307, 417, 48); // preview color text
		tab.child(7, 39702, 480, 12); // close
		tab.child(8, 39703, 480, 12); // close
		tab.child(9, 25312, 63, 84); // Preview 1
		tab.child(10, 25313, 213, 84); // Preview 2
		tab.child(11, 25314, 63, 214); // Preview 3
		tab.child(12, 25315, 213, 214); // Preview 4
		tab.child(13, 25316, 342, 200); // Character
	}

	public static void buyOffersFromAll() {
		RSInterface rsi = addInterface(19600, InterfaceType.MAIN);
		RSInterface scroll = addInterface(19615, InterfaceType.MAIN);// don't go
																		// above
																		// 19619!
																		// it
																		// will
																		// overwrite
																		// other
																		// thing

		addNewText(19601, "Buy Offers from Players", 473, 26, 2, 0xff981f, 0xff981f, true, 1, 1, 0);
		addRectangle(19602, 0, 0x30251F, false, 250, 50, 0); // box around item
																// offered
		addNewText(19603, "Filter:", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);

		addButton(19604, 292, 293, "View My Offers");
		addNewText(19605, "My Offers", 150, 35, 2, 0xffffff, 0xffffff, true, 1, 1, 0);

		addNewText(19606, "Item Name", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);
		addNewText(19607, "Amount", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);
		addNewText(19608, "Price", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);

		addNewText(19609, "Loading...", 22, 22, 2, 0xffffff, 0xffffff, true, 0, 0, 0);

		int searchX = 50, searchY = 60;
		int listX = 40, listY = 130;

		rsi.totalChildren(17);
		rsi.child(0, 39701, 14, 17); // main screen
		rsi.child(1, 39702, 475, 30); // close button
		rsi.child(2, 39703, 475, 30); // close hover button
		rsi.child(3, 19601, 21, 24); // Title of the box
		rsi.child(4, 19602, 40, 56); // outline around filter
		rsi.child(5, 19502, searchX, searchY); // search button sprite box
		rsi.child(6, 19503, searchX + 1, searchY); // search item sprite
		rsi.child(7, 19504, searchX, searchY); // search button
		rsi.child(8, 19603, 95, 80); // search filter
		rsi.child(9, 19604, 300, 70);// create offer button
		rsi.child(10, 19605, 300, 70);// create offer button text
		rsi.child(11, 45041, listX, listY); // buy offer items
		rsi.child(12, 19606, listX + 30, listY + 10); // item name text
		rsi.child(13, 19607, listX + 160, listY + 10); // item name text
		rsi.child(14, 19608, listX + 300, listY + 10); // item name text
		rsi.child(15, 19609, listX + 170, listY + 165); // Loading text
		rsi.child(16, scroll.id, listX + 6, listY + 31);

		final int TOTAL_SEARCH_RESULTS = 20;
		int totalChildren = TOTAL_SEARCH_RESULTS * 4;
		final int LIST_SPRITE_HEIGHT = 21;

		scroll.totalChildren(totalChildren);

		int startInterfaceId = scroll.id;

		int interfaceId = startInterfaceId + 1;
		int yOffset = 0;

		// int count = 0;

		for (int i = 0; i < totalChildren; i++) {
			addButton(interfaceId, 967, 966, "Sell to this offer");
			addNewText(interfaceId + 1, "Itemname:", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);
			addNewText(interfaceId + 2, "Amount:", 130, 20, 0, 0xff981f, 0xff981f, true, 2, 1, 0);
			addNewText(interfaceId + 3, "Price:", 110, 20, 0, 0xff981f, 0xff981f, true, 2, 1, 0);

			scroll.child(i, interfaceId, 0, yOffset);
			scroll.child(i + 1, interfaceId + 1, 5, yOffset);
			scroll.child(i + 2, interfaceId + 2, listX + 103, yOffset);
			scroll.child(i + 3, interfaceId + 3, listX + 250, yOffset);
			i += 3;
			interfaceId = interfaceId + 4;

			yOffset += LIST_SPRITE_HEIGHT; // how stretched down it is
			// count++;
		}

		scroll.scrollMax = TOTAL_SEARCH_RESULTS * LIST_SPRITE_HEIGHT;
		scroll.width = 408;
		scroll.height = 121;

	}

	public static void viewMyOwnBuyOffers() {
		RSInterface rsi = addInterface(19700, InterfaceType.MAIN);
		RSInterface scroll = addInterface(19750, InterfaceType.MAIN);

		addNewText(19701, "My Buy Orders", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);

		addButton(19702, 292, 293, "Back");
		addNewText(19703, "Back", 22, 22, 2, 0xffffff, 0xffffff, true, 0, 0, 0);

		addNewText(19704, "Item Name", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);
		addNewText(19705, "Amount", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);
		addNewText(19706, "Price", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);

		addNewText(19707, "Loading...", 22, 22, 2, 0xffffff, 0xffffff, true, 0, 0, 0);

		int listX = 40, listY = 90;
		int buttonX = 190, buttonY = 260;

		rsi.totalChildren(12);
		rsi.child(0, 39701, 14, 17); // main screen
		rsi.child(1, 39702, 475, 30); // close button
		rsi.child(2, 39703, 475, 30); // close hover button
		rsi.child(3, 19701, 195, 30); // Title of the box
		rsi.child(4, 19702, buttonX, buttonY);// create offer button
		rsi.child(5, 19703, buttonX + 55, buttonY + 10);// create offer button
														// text
		rsi.child(6, 45041, listX, listY); // buy offer items
		rsi.child(7, 19704, listX + 30, listY + 10); // item name text
		rsi.child(8, 19705, listX + 160, listY + 10); // item name text
		rsi.child(9, 19706, listX + 300, listY + 10); // item name text
		rsi.child(10, 19707, 235, 65); // loading text
		rsi.child(11, scroll.id, listX + 6, listY + 31);

		final int TOTAL_SEARCH_RESULTS = 20;
		int totalChildren = TOTAL_SEARCH_RESULTS * 4;
		final int LIST_SPRITE_HEIGHT = 21;

		scroll.totalChildren(totalChildren);

		int startInterfaceId = scroll.id;

		int interfaceId = startInterfaceId + 1;
		int yOffset = 0;

		// int count = 0;

		for (int i = 0; i < totalChildren; i++) {
			addButton(interfaceId, 967, 966, "Manage this offer");
			addNewText(interfaceId + 1, "Dragon Claws", 22, 22, 2, 0xff981f, 0xff981f, true, 0, 0, 0);
			addNewText(interfaceId + 2, "1,000", 130, 17, 0, 0xff981f, 0xff981f, true, 2, 1, 0);
			addNewText(interfaceId + 3, "1,000,000,000,000", 110, 17, 0, 0xff981f, 0xff981f, true, 2, 1, 0);

			scroll.child(i, interfaceId, 0, yOffset);
			scroll.child(i + 1, interfaceId + 1, 5, yOffset + 3);
			scroll.child(i + 2, interfaceId + 2, listX + 103, yOffset + 3);
			scroll.child(i + 3, interfaceId + 3, listX + 250, yOffset + 3);
			i += 3;
			interfaceId = interfaceId + 4;

			yOffset += LIST_SPRITE_HEIGHT; // how stretched down it is
			// count++;
		}

		scroll.scrollMax = TOTAL_SEARCH_RESULTS * LIST_SPRITE_HEIGHT;
		scroll.width = 408;
		scroll.height = 121;

	}

	public static void marketOfferUpInterface() {
		RSInterface rsi = addInterface(19500, InterfaceType.MAIN);

		addText(19501, "Create a Market Offer", 2, 0xff981f, false, true);
		addSprite(19502, 971);
		addItemSprite(19503, 14484, 0, 0);
		addSprite(19504, -1, 970, 40, 36, "Search Item");
		addText(19505, "Offer Item", 2, 0xff981f, false, true);
		addButton(19506, 292, 293, "Confirm");
		addText(19507, "Confirm", 2, 0xffffff, false, true);
		int cnfBttnX = 290, cnfBckBttnY = 267;
		addButton(19508, 292, 293, "Back");
		addText(19509, "Back", 2, 0xffffff, false, true);
		int bckBttnX = 70;
		addSprite(19510, 1); // border
		addRectangle(19511, 0, 0x30251F, false, 150, 100, 0); // box around item
																// offered
		addRectangle(19512, 0, 0x30251F, false, 286, 100, 0); // box right of
																// item offered
		addRectangle(19513, 0, 0x30251F, false, 218, 70, 0); // other two boxes
																// under it
		addButton(19514, 107, "Change Quantity");
		addText(19515, "Quantity:", 2, 0xff981f, false, true);
		addText(19516, "100", 0, 0xffffff, true, false);
		int qntPriceY = 200;
		int qntX = 60;
		addButton(19517, 107, "Change Price");
		addText(19518, "Price per item:", 2, 0xff981f, false, true);
		addText(19519, "100,000,000", 0, 0xffffff, true, false);
		int priceX = 275;

		addItemSprite(19520, 996, 0, 0);
		addText(19521, "100,000,000", 0, 0xC0985A, false, true);

		addText(19522, "Total Price:", 2, 0xff981f, false, true);
		addText(19523, "100,000,000,000", 0, 0xC0985A, false, true);

		rsi.totalChildren(29);
		rsi.child(0, 39701, 14, 17); // main screen
		rsi.child(1, 39702, 475, 30); // close button
		rsi.child(2, 39703, 475, 30); // close hover button
		rsi.child(3, 19501, 195, 30); // Title of the box
		rsi.child(4, 19502, 80, 90); // search box sprite
		rsi.child(5, 19503, 83, 91); // item sprite container
		rsi.child(6, 19504, 80, 90); // search hover click button
		rsi.child(7, 19505, 70, 70); // offer item text
		rsi.child(8, 19506, cnfBttnX, cnfBckBttnY); // confirm button
		rsi.child(9, 19507, cnfBttnX + 45, cnfBckBttnY + 10); // confirm text
		rsi.child(10, 19508, bckBttnX, cnfBckBttnY); // back button
		rsi.child(11, 19509, bckBttnX + 55, cnfBckBttnY + 10); // back button
																// text
		rsi.child(12, 19510, 20, 250); // bottom border
		rsi.child(13, 19510, 210, 250); // bottom border
		rsi.child(14, 19510, 304, 250); // bottom border
		rsi.child(15, 19511, 40, 65); // box around item offered
		rsi.child(16, 19512, 189, 65);// box right of item offered
		rsi.child(17, 19513, 40, 164); // box of amount to offer
		rsi.child(18, 19513, 257, 164); // box of price to offer
		rsi.child(19, 19514, qntX, qntPriceY); // quantity text box
		rsi.child(20, 19515, qntX + 50, qntPriceY - 18); // quantity title
		rsi.child(21, 19516, qntX + 90, qntPriceY + 4); // quantity number
		rsi.child(22, 19517, priceX, qntPriceY); // price text box
		rsi.child(23, 19518, priceX + 45, qntPriceY - 18); // price title text
		rsi.child(24, 19519, priceX + 90, qntPriceY + 4); // price number

		rsi.child(25, 19520, 60, 125); // average price sprite
		rsi.child(26, 19521, 90, 140); // average price text

		rsi.child(27, 19522, 205, 105); // total price title
		rsi.child(28, 19523, 285, 108); // total price title
	}

	public static void soulWarsExpReward() {
		RSInterface tab = addInterface(29293);

		addSprite(29294, 1034);
		addHoverButton(29295, 1035, "Buy-1 Zeals <col=ffb000>Attack XP", -1, 29296, 5);
		addHoveredButton(29296, 1036, 29297);
		addHoverButton(29298, 1035, "Buy-1 Zeals <col=ffb000>Strength XP", -1, 29299, 5);
		addHoveredButton(29299, 1036, 29300);
		addHoverButton(29301, 1035, "Buy-1 Zeals <col=ffb000>Defence XP", -1, 29302, 5);
		addHoveredButton(29302, 1036, 29303);
		addHoverButton(29304, 1035, "Buy-1 Zeals <col=ffb000>Hitpoint XP", -1, 29305, 5);
		addHoveredButton(29305, 1036, 29306);
		addHoverButton(29307, 1035, "Buy-1 Zeals <col=ffb000>Range XP", -1, 29308, 5);
		addHoveredButton(29308, 1036, 29309);
		addHoverButton(29310, 1035, "Buy-1 Zeals <col=ffb000>Magic XP", -1, 29311, 5);
		addHoveredButton(29311, 1036, 29312);
		addHoverButton(29313, 1035, "Buy-1 Zeals <col=ffb000>Prayer XP", -1, 29314, 5);
		addHoveredButton(29314, 1036, 29315);
		addHoverButton(29316, 1035, "Buy-1 Zeals <col=ffb000>Slayer XP", -1, 29317, 5);
		addHoveredButton(29317, 1036, 29318);
		addSprite(29319, 1039);
		addSprite(29320, 1055);
		addSprite(29321, 1044);
		addSprite(29322, 1048);
		addSprite(29323, 1053);
		addSprite(29324, 1049);
		addSprite(29325, 1051);
		addSprite(29326, 1054);
		addButton(29327, 1038, "Charms", 28641, 1, 68, 20);
		addButton(29328, 1038, "Other", 28642, 1, 68, 20);
		addButton(29329, 1037, "Experience", 28643, 1, 68, 20);
		addText(29330, "Experience", 0, 0xffb000, true, true);
		addText(29331, "Charms", 0, 0x695f28, true, true);
		addText(29332, "Other", 0, 0x695f28, true, true);
		addText(29333, "Zeal: 0", 0, 0xffb000, true, true);
		addHover(29334, 3, 0, 29335, 300, "Close window");
		addHovered(29335, 301, 29336);

		tab.totalChildren(34);
		tab.child(0, 29294, 75, 50);
		tab.child(1, 29295, 100, 110);
		tab.child(2, 29296, 100, 110);
		tab.child(3, 29298, 185, 110);
		tab.child(4, 29299, 185, 110);
		tab.child(5, 29301, 270, 110);
		tab.child(6, 29302, 270, 110);
		tab.child(7, 29304, 355, 110);
		tab.child(8, 29305, 355, 110);
		tab.child(9, 29307, 100, 192);
		tab.child(10, 29308, 100, 192);
		tab.child(11, 29310, 185, 192);
		tab.child(12, 29311, 185, 192);
		tab.child(13, 29313, 270, 192);
		tab.child(14, 29314, 270, 192);
		tab.child(15, 29316, 355, 192);
		tab.child(16, 29317, 355, 192);
		tab.child(17, 29319, 110, 120);
		tab.child(18, 29320, 200, 120);
		tab.child(19, 29321, 286, 123);
		tab.child(20, 29322, 365, 123);
		tab.child(21, 29323, 110, 200);
		tab.child(22, 29324, 192, 200);
		tab.child(23, 29325, 277, 199);
		tab.child(24, 29326, 363, 200);
		tab.child(25, 29327, 160, 65);
		tab.child(26, 29328, 235, 65);
		tab.child(27, 29329, 85, 65);
		tab.child(28, 29330, 120, 70);
		tab.child(29, 29331, 195, 70);
		tab.child(30, 29332, 270, 70);
		tab.child(31, 29333, 363, 65);
		tab.child(32, 29334, 418, 62);
		tab.child(33, 29335, 418, 62);
	}

	public static void soulWarsCharmsReward() {
		RSInterface tab = addInterface(29337);

		addSprite(29338, 1034);
		addHoverButton(29339, 1035, "Buy-1 Zeals <col=ffb000>Gold Charms", -1, 29340, 5);
		addHoveredButton(29340, 1036, 29341);
		addHoverButton(29342, 1035, "Buy-1 Zeals <col=ffb000>Green Charms", -1, 29343, 5);
		addHoveredButton(29343, 1036, 29344);
		addHoverButton(29345, 1035, "Buy-1 Zeals <col=ffb000>Crimson Charms", -1, 29346, 5);
		addHoveredButton(29346, 1036, 29347);
		addHoverButton(29348, 1035, "Buy-1 Zeals <col=ffb000>Blue Charms", -1, 29349, 5);
		addHoveredButton(29349, 1036, 29350);

		addSprite(29351, 1040);
		addSprite(29352, 1042);
		addSprite(29353, 1041);
		addSprite(29354, 1043);

		addButton(29357, 1037, "Charms", 28644, 1, 68, 20);
		addButton(29358, 1038, "Experience", 28645, 1, 68, 20);
		addText(29359, "Charms", 0, 0xffb000, true, true);
		addText(29360, "Experience", 0, 0x695f28, true, true);

		tab.totalChildren(22);
		tab.child(0, 29338, 75, 50);
		tab.child(1, 29339, 100, 145);
		tab.child(2, 29340, 100, 145);
		tab.child(3, 29342, 185, 145);
		tab.child(4, 29343, 185, 145);
		tab.child(5, 29345, 270, 145);
		tab.child(6, 29346, 270, 145);
		tab.child(7, 29348, 355, 145);
		tab.child(8, 29349, 355, 145);
		tab.child(9, 29351, 113, 158);
		tab.child(10, 29352, 199, 158);
		tab.child(11, 29353, 283, 158);
		tab.child(12, 29354, 369, 158);
		tab.child(13, 29358, 85, 65);
		tab.child(14, 29328, 235, 65);
		tab.child(15, 29357, 160, 65);
		tab.child(16, 29332, 270, 70);
		tab.child(17, 29359, 195, 70);
		tab.child(18, 29360, 120, 70);
		tab.child(19, 29333, 363, 65);
		tab.child(20, 29334, 418, 62);
		tab.child(21, 29335, 418, 62);
	}

	public static void soulWarsOtherReward() {
		RSInterface tab = addInterface(29361);

		addSprite(29362, 1034);

		addHoverButton(29363, 1035, "Buy-5 Zeals <col=ffb000>Crawling hand", -1, 29364, 5);
		addHoveredButton(29364, 1036, 29365);
		addHoverButton(29366, 1035, "Buy-25 Zeals <col=ffb000>Minitrice", -1, 29367, 5);
		addHoveredButton(29367, 1036, 29368);
		addHoverButton(29369, 1035, "Buy-40 Zeals <col=ffb000>Baby basilisk", -1, 29370, 5);
		addHoveredButton(29370, 1036, 29371);

		addHoverButton(29372, 1035, "Buy-70 Zeals <col=ffb000>Baby Kurask", -1, 29373, 5);
		addHoveredButton(29373, 1036, 29374);
		addHoverButton(29375, 1035, "Buy-85 Zeals <col=ffb000>Abyssal minion", -1, 29376, 5);
		addHoveredButton(29376, 1036, 29377);
		addHoverButton(29378, 1035, "Buy-100 Zeals <col=ffb000>TzRek-Jad", -1, 29379, 5);
		addHoveredButton(29379, 1036, 29380);

		addHoverButton(29381, 1035, "Buy-3 Zeals <col=ffb000>Try my luck", -1, 29382, 5);
		addHoveredButton(29382, 1036, 29383);

		addSprite(29384, 1047);
		addSprite(29385, 1045);
		addSprite(29386, 1046);
		addSprite(29387, 1050);
		addSprite(29388, 1056);
		addSprite(29389, 1057);
		addSprite(29390, 1052);

		addButton(29391, 1037, "Other", 28642, 1, 68, 20);
		addButton(29392, 1038, "Experience", 28643, 1, 68, 20);
		addText(29393, "Other", 0, 0xffb000, true, true);
		addText(29394, "Experience", 0, 0x695f28, true, true);

		tab.totalChildren(31);
		tab.child(0, 29362, 75, 50);
		tab.child(1, 29363, 100, 110);
		tab.child(2, 29364, 100, 110);
		tab.child(3, 29366, 185, 110);
		tab.child(4, 29367, 185, 110);
		tab.child(5, 29369, 270, 110);
		tab.child(6, 29370, 270, 110);
		tab.child(7, 29372, 100, 192);
		tab.child(8, 29373, 100, 192);
		tab.child(9, 29375, 185, 192);
		tab.child(10, 29376, 185, 192);
		tab.child(11, 29378, 270, 192);
		tab.child(12, 29379, 270, 192);
		tab.child(13, 29381, 355, 150);
		tab.child(14, 29382, 355, 150);
		tab.child(15, 29384, 108, 117);
		tab.child(16, 29385, 196, 117);
		tab.child(17, 29386, 280, 117);
		tab.child(18, 29387, 110, 202);
		tab.child(19, 29388, 197, 201);
		tab.child(20, 29389, 280, 200);
		tab.child(21, 29390, 375, 163);

		tab.child(22, 29327, 160, 65);
		tab.child(23, 29391, 235, 65);
		tab.child(24, 29392, 85, 65);
		tab.child(25, 29394, 120, 70);
		tab.child(26, 29331, 195, 70);
		tab.child(27, 29393, 270, 70);
		tab.child(28, 29333, 363, 65);
		tab.child(29, 29334, 418, 62);
		tab.child(30, 29335, 418, 62);
	}

	public static void soulWarsGameInterface() {
		RSInterface tab = addScreenInterface(29266);

		addSprite(29267, 1032);
		addSprite(29268, 1033);
		addSprite(29269, 1033);
		addSprite(29270, 1033);
		addSprite(29271, 1030);
		addSprite(29272, 1031);
		addSprite(29273, 1027);
		addSprite(29274, 1028);
		addSprite(29275, 1029);
		addText(29276, "<col=ff>Blue     <col=ff0000>Red", 0, 0xff981f, true, true);
		addText(29277, "Avatar deaths", 0, 0xffb000, true, true);
		addText(29278, "0", 0, 0xff, true, true);
		addText(29279, "0", 0, 0xff0000, true, true);
		addText(29280, "Avatar level", 0, 0xffb000, true, true);
		addText(29281, "100", 0, 0xff, true, true);
		addText(29282, "100", 0, 0xff0000, true, true);
		addText(29283, "Avatar health", 0, 0xffb000, true, true);
		addText(29284, "100%", 0, 0xff, true, true);
		addText(29285, "100%", 0, 0xff0000, true, true);
		addText(29286, "Time left", 0, 0xffb000, true, true);
		addText(29287, "N/A", 0, 0xffb000, true, true);
		addRectangle(29288, 128, 0xafafaf, true, 35, 40, 1337);// 0xaf, 0xaf0000
		addRectangle(29289, 128, 0xaf, true, 35, 40, 1338);
		addRectangle(29290, 125, 0xaf0000, true, 35, 40, 1339);
		addRectangle(29291, 128, 0xaf00, true, 0, 13, 1340);// 116
		addRectangle(29292, 128, 0xaf00, true, 18, 0, 1341);// 149

		tab.totalChildren(26);
		tab.child(0, 29288, 306, 5);
		tab.child(1, 29289, 265, 5);
		tab.child(2, 29290, 348, 5);
		tab.child(3, 29291, 266, 53);
		tab.child(4, 29292, 486, 8);
		tab.child(5, 29267, 389, 5);
		tab.child(6, 29268, 347, 5);
		tab.child(7, 29269, 306, 5);
		tab.child(8, 29270, 265, 5);
		tab.child(9, 29271, 266, 53);
		tab.child(10, 29272, 486, 6);
		tab.child(11, 29273, 265, 5);
		tab.child(12, 29274, 346, 5);
		tab.child(13, 29275, 306, 5);
		tab.child(14, 29276, 435, 14);
		tab.child(15, 29277, 437, 34);
		tab.child(16, 29278, 420, 46);
		tab.child(17, 29279, 455, 46);
		tab.child(18, 29280, 435, 62);
		tab.child(19, 29281, 420, 76);
		tab.child(20, 29282, 455, 76);
		tab.child(21, 29283, 437, 94);
		tab.child(22, 29284, 420, 108);
		tab.child(23, 29285, 455, 108);
		tab.child(24, 29286, 435, 124);
		tab.child(25, 29287, 435, 139);
	}

	public static void playerIcons() {
		RSInterface tab = addInterface(15271, InterfaceType.TAB);
		RSInterface list = addInterface(15288, InterfaceType.TAB);
		addText(15281, "Change Display Rank", 2, 0xeb981f, false, true);
		addSprite(15282, 30);
		addText(15283, "Ranks with a lock are unavailable", 0, 0xeb981f, false, true);
		addSprite(15284, 31);
		addHover(15285, 3, 0, 15286, 300, "Close Rank Changer");
		addHovered(15286, 301, 15287);
		tab.totalChildren(8);
		tab.child(0, 15281, 23, 5);
		tab.child(1, 15284, 0, 25);
		tab.child(2, 15282, 0, 22);
		tab.child(3, 15282, 0, 249);
		tab.child(4, 15283, 4, 251);
		tab.child(5, 15285, 168, 4);
		tab.child(6, 15286, 168, 4);
		tab.child(7, 15288, 6, 24); // list

		int iconsAmount = RankInfo.values.length;

		int totalChildren = iconsAmount * 5;

		list.totalChildren(totalChildren);

		int startInterfaceId = 19002;

		int configCode = 1960;
		int lockConfigCode = 1961;

		int interfaceId = startInterfaceId;
		int yOffset = 0;

		int count = 0;

		for (int i = 0; i < totalChildren; i++) {
			addRadioButton(interfaceId, 15288, -1, 466, 180, 37, "Toggle", count, 5, configCode);
			addText(interfaceId + 1, RankInfo.values[count].getRankName(), 0xffb000, false, true, -1, 1);
			if (RankInfo.values[count].getSpriteId() < 14)
				addSpriteModIcon(interfaceId + 2, RankInfo.values[count].getSpriteId());
			else
				addSprite(interfaceId + 2, RankInfo.values[count].getSpriteId()); // TODO:
																					// loop
																					// through
																					// correct
																					// sprite
																					// ids
			addRadioButton(interfaceId + 3, 15288, 108, -1, 0, 0, "Unlock Information", 1, 4, lockConfigCode++);

			list.child(i, 15282, -10, yOffset + 37);
			list.child(i + 1, interfaceId, 0, yOffset);
			list.child(i + 2, interfaceId + 1, 32, yOffset + 10);
			list.child(i + 3, interfaceId + 2, 8, yOffset + 9);
			list.child(i + 4, interfaceId + 3, 155, yOffset + 10);
			i += 4;
			interfaceId = interfaceId + 4;

			yOffset += 40; // how stretched down it is
			count++;
		}

		list.scrollMax = iconsAmount * 40;
		list.width = 168;
		list.height = 225;
	}

	public static void spawnTab() {
		RSInterface tab = addInterface(15891, InterfaceType.TAB);
		RSInterface list = addInterface(18793, InterfaceType.TAB);
		addText(18786, "Setup Tab", 2, 0xeb981f, false, true);
		addSprite(18787, 30);
		addText(18788, "Pick your Setup and supplies", 0, 0xeb981f, false, true);
		addSprite(18789, 31);
		addHover(18790, 3, 0, 18791, 300, "Close Setup Tab");
		addHovered(18791, 301, 18792);
		tab.totalChildren(8);
		tab.child(0, 18786, 23, 5);
		tab.child(1, 18789, 0, 25);
		tab.child(2, 18787, 0, 22);
		tab.child(3, 18787, 0, 249);
		tab.child(4, 18788, 4, 251);
		tab.child(5, 18790, 168, 4);
		tab.child(6, 18791, 168, 4);
		tab.child(7, 18793, 6, 24); // list

		int iconsAmount = SpawnInfo.values().length;

		int totalChildren = iconsAmount * 4;

		list.totalChildren(totalChildren);

		int startInterfaceId = 18794;

		int interfaceId = startInterfaceId;
		int yOffset = 0;

		int count = 0;

		for (int i = 0; i < totalChildren; i++) {
			// addButton(interfaceId, 465, 466, 180, 37, "Spawn");
			addSprite(interfaceId, -1, 466, 168, 37, "Spawn");
			// addConfigButton(interfaceId, 18793, -1, 466, 180, 37, "Toggle",
			// 1, 4, configCode++);
			addText(interfaceId + 1, SpawnInfo.values()[count].getSetName(), 0xffb000, false, true, -1, 1);
			// addSprite(interfaceId+2,
			// SpawnInfo.values()[count].getSpriteId());
			addItemSprite(interfaceId + 2, SpawnInfo.values()[count].getItemId());

			list.child(i, 18787, -10, yOffset + 37);
			list.child(i + 1, interfaceId, 0, yOffset);
			list.child(i + 2, interfaceId + 1, 38, yOffset + 10);
			list.child(i + 3, interfaceId + 2, 0, yOffset + 0);
			i += 3;
			interfaceId = interfaceId + 3;

			yOffset += 40; // how stretched down it is
			count++;
		}

		list.scrollMax = iconsAmount * 40;
		list.width = 168;
		list.height = 225;
	}

	public static void constructionFurnitureChooser() {
		RSInterface rsi = addInterface(39550);

		rsi.totalChildren(67);

		addSprite(39551, 456); // background
		rsi.child(0, 39551, 0, 0);

		addText(39552, "Name1", 0, 0xff981f, false, false);
		rsi.child(1, 39552, 140, 56);
		addText(39553, "Req1.1", 0, 0xccccff, false, false);
		rsi.child(2, 39553, 140, 67);
		addText(39554, "Req1.2", 0, 0xccccff, false, false);
		rsi.child(3, 39554, 140, 76);
		addText(39555, "Req1.3", 0, 0xccccff, false, false);
		rsi.child(4, 39555, 140, 85);
		addText(39556, "Req1.4", 0, 0xccccff, false, false);
		rsi.child(5, 39556, 140, 94);
		addText(39557, "Level: 1", 0, 0xff981f, false, false);
		rsi.child(6, 39557, 88, 89);

		addText(39558, "Name2", 0, 0xff981f, false, false);
		rsi.child(7, 39558, 140, 126);
		addText(39559, "req2.1", 0, 0xccccff, false, false);
		rsi.child(8, 39559, 140, 135);
		addText(39560, "req2.2", 0, 0xccccff, false, false);
		rsi.child(9, 39560, 140, 144);
		addText(39561, "req2.3", 0, 0xccccff, false, false);
		rsi.child(10, 39561, 140, 153);
		addText(39562, "req2.4", 0, 0xccccff, false, false);
		rsi.child(11, 39562, 140, 162);
		addText(39563, "Level: 2", 0, 0xff981f, false, false);
		rsi.child(12, 39563, 88, 158);

		addText(39564, "Name3", 0, 0xff981f, false, false);
		rsi.child(13, 39564, 140, 196);
		addText(39565, "Req3.1", 0, 0xccccff, false, false);
		rsi.child(14, 39565, 140, 205);
		addText(39566, "Req3.2", 0, 0xccccff, false, false);
		rsi.child(15, 39566, 140, 214);
		addText(39567, "Req3.3", 0, 0xccccff, false, false);
		rsi.child(16, 39567, 140, 223);
		addText(39568, "Req3.4", 0, 0xccccff, false, false);
		rsi.child(17, 39568, 140, 232);
		addText(39569, "Level: 3", 0, 0xff981f, false, false);
		rsi.child(18, 39569, 89, 228);

		addText(39570, "Name4", 0, 0xff981f, false, false);
		rsi.child(19, 39570, 140, 265);
		addText(39571, "Req4.1", 0, 0xccccff, false, false);
		rsi.child(20, 39571, 140, 274);
		addText(39572, "Req4.2", 0, 0xccccff, false, false);
		rsi.child(21, 39572, 140, 283);
		addText(39573, "Req4.3", 0, 0xccccff, false, false);
		rsi.child(22, 39573, 140, 292);
		addText(39574, "Req4.4", 0, 0xccccff, false, false);
		rsi.child(23, 39574, 140, 301);
		addText(39575, "Level: 4", 0, 0xff981f, false, false);
		rsi.child(24, 39575, 89, 297);

		addText(39576, "Name5", 0, 0xff981f, false, false);
		rsi.child(25, 39576, 336, 56);
		addText(39577, "req5.1", 0, 0xccccff, false, false);
		rsi.child(26, 39577, 336, 67);
		addText(39578, "req5.2", 0, 0xccccff, false, false);
		rsi.child(27, 39578, 336, 76);
		addText(39579, "req5.3", 0, 0xccccff, false, false);
		rsi.child(28, 39579, 336, 85);
		addText(39580, "req5.4", 0, 0xccccff, false, false);
		rsi.child(29, 39580, 336, 94);
		addText(39581, "Level: 5", 0, 0xff981f, false, false);
		rsi.child(30, 39581, 284, 89);

		addText(39582, "Name6", 0, 0xff981f, false, false);
		rsi.child(31, 39582, 336, 126);
		addText(39583, "req6.1", 0, 0xccccff, false, false);
		rsi.child(32, 39583, 336, 135);
		addText(39584, "req6.2", 0, 0xccccff, false, false);
		rsi.child(33, 39584, 336, 144);
		addText(39585, "req6.3", 0, 0xccccff, false, false);
		rsi.child(34, 39585, 336, 153);
		addText(39586, "req6.4", 0, 0xccccff, false, false);
		rsi.child(35, 39586, 336, 162);
		addText(39587, "Level: 6", 0, 0xff981f, false, false);
		rsi.child(36, 39587, 284, 158);

		addText(39588, "Name7", 0, 0xff981f, false, false);
		rsi.child(37, 39588, 336, 196);
		addText(39589, "req7.1", 0, 0xccccff, false, false);
		rsi.child(38, 39589, 336, 205);
		addText(39590, "req7.2", 0, 0xccccff, false, false);
		rsi.child(39, 39590, 336, 214);
		addText(39591, "req7.3", 0, 0xccccff, false, false);
		rsi.child(40, 39591, 336, 223);
		addText(39592, "req7.4", 0, 0xccccff, false, false);
		rsi.child(41, 39592, 336, 232);
		addText(39593, "Level: 7", 0, 0xff981f, false, false);
		rsi.child(42, 39593, 284, 228);

		addText(39594, "Name8", 0, 0xff981f, false, false);
		rsi.child(43, 39594, 336, 265);
		addText(39595, "req8.1", 0, 0xccccff, false, false);
		rsi.child(44, 39595, 336, 274);
		addText(39596, "req8.2", 0, 0xccccff, false, false);
		rsi.child(45, 39596, 336, 283);
		addText(39597, "req8.3", 0, 0xccccff, false, false);
		rsi.child(46, 39597, 336, 292);
		addText(39598, "req8.4", 0, 0xccccff, false, false);
		rsi.child(47, 39598, 336, 301);
		addText(39599, "Level: 8", 0, 0xff981f, false, false);
		rsi.child(48, 39599, 284, 297);

		// int configStart = 1000;
		for (int i = 0; i < 4; i++) {
			RSInterface group = RSInterface.addInterface(39600 + i);
			RSInterface.addToItemGroup(group, 1, 28, 0, 0, new String[] { "Build" });
			rsi.child((49 + i), group.id, 85, 58 + (i * 69));

			// addFurnitureXMark(39608+i, configStart++);
			// rsi.child(39608+i, 85, 58 + (i * 69), (59 + i));
		}

		for (int i = 0; i < 4; i++) {
			RSInterface group = RSInterface.addInterface(39604 + i);
			RSInterface.addToItemGroup(group, 1, 28, 0, 0, new String[] { "Build" });
			rsi.child((53 + i), 39604 + i, 282, 58 + (i * 69));

			// addFurnitureXMark(39612+i, configStart++);
			// rsi.child(39612+i, 282, 58 + (i * 69), (63 + i));
		}

		rsi.child(57, 39702, 460, 25); // close option
		rsi.child(58, 39703, 460, 25); // close hover

		// config codes for level requirements
		addFurnitureXMark(39609, ConfigCodes.CONS_LVL_REQ1);
		rsi.child(59, 39609, 85, 58 + (0 * 69));

		addFurnitureXMark(39610, ConfigCodes.CONS_LVL_REQ2);
		rsi.child(60, 39610, 85, 58 + (1 * 69));

		addFurnitureXMark(39611, ConfigCodes.CONS_LVL_REQ3);
		rsi.child(61, 39611, 85, 58 + (2 * 69));

		addFurnitureXMark(39612, ConfigCodes.CONS_LVL_REQ4);
		rsi.child(62, 39612, 85, 58 + (3 * 69));

		addFurnitureXMark(39613, ConfigCodes.CONS_LVL_REQ5);
		rsi.child(63, 39613, 282, 58 + (0 * 69));

		addFurnitureXMark(39614, ConfigCodes.CONS_LVL_REQ6);
		rsi.child(64, 39614, 282, 58 + (1 * 69));

		addFurnitureXMark(39615, ConfigCodes.CONS_LVL_REQ7);
		rsi.child(65, 39615, 282, 58 + (2 * 69));

		addFurnitureXMark(39616, ConfigCodes.CONS_LVL_REQ8);
		rsi.child(66, 39616, 282, 58 + (3 * 69));

	}

	public static void addFurnitureXMark(int i, int config) {
		RSInterface rsi = interfaceCache[i] = new RSInterface();
		RSInterface.checkTaken(i);
		rsi.id = i;
		rsi.parentID = i;
		rsi.interfaceType = 5;
		rsi.atActionType = 0;
		rsi.contentType = 0;
		rsi.enabledSprite = Client.instance.crosses[5];
		rsi.width = 512;
		rsi.height = 334;
		setSelectableValues(i, config, 1);
	}

	public static void constructionWaiting() {
		RSInterface tab = addInterface(38400);
		addSprite(38401, 454);
		tab.totalChildren(1);
		tab.child(0, 38401, 0, 0);
	}

	public static void roomChooser() {
		RSInterface tab = addInterface(28643);
		addSprite(28644, 460);
		tab.totalChildren(4);
		int BASEX = 10, BASEY = 10;
		tab.child(0, 28644, BASEX + 0, BASEY + 0);
		tab.child(1, 39702, BASEX + 430, BASEY + 4);
		tab.child(2, 39703, BASEX + 430, BASEY + 4);
		tab.child(3, 28646, BASEX + 51, BASEY + 60);

		tab = addInterface(28646);
		int totalRooms = 23;
		tab.scrollMax = totalRooms * 65;
		tab.width = 330;
		tab.height = 220;
		String[] names = new String[] { "Parlour: Lvl 1", "Garden: Lvl 1", "Kitchen: lvl 5", "Dining room: lvl 10",
				"Workshop: lvl 15", "Bedroom: Lvl 20", "Hall - Skill Trophies: Lvl 25", "Games Room: Lvl 30",
				"Combat room: Lvl 32", "Hall - Quest trophies: Lvl 35", "Menagarie: Lvl 37", "Study: Lvl 40",
				"Costume room: Lvl 42", "Chapel: Lvl 45", "Portal chamber: Lvl 50", "Formal garden: Lvl 55",
				"Throne room: Lvl 60", "Oubliette: Lvl 65", "Dungeon - corridor: Lvl 70", "Dungeon - junction: Lvl 70",
				"Dungeon - stairs: Lvl 70", "Dungeon - pit: Lvl 70", "Treasure room: Lvl 75" };
		int[] money = new int[] { 1000, 1000, 5000, 5000, 10000, 10000, 15000, 25000, 25000, 25000, 30000, 50000, 50000,
				50000, 100000, 75000, 150000, 150000, 7500, 7500, 7500, 10000, 250000 };
		int y = 8, x = 4, bounds = 0, id = 28647;
		tab.totalChildren(totalRooms * 4);
		for (int i = 0; i < names.length; i++) {
			addRectangleClickable(id, 0, 0x333333, false, 238, 61);
			tab.child(bounds++, id++, x, y);
			addSprite(id, 1003 + i);
			tab.child(bounds++, id++, x + 13, y + 5);

			addText(id, names[i], 0xCCCCFF, false, true, 0, 1);
			tab.child(bounds++, id++, x + 78, y + 22);
			addText(id, money[i] + " Coins", 0xC80000, false, true, 0, 1);
			interfaceCache[id].valueIndexArray = new int[1][];
			interfaceCache[id].requiredValues = new int[1];
			interfaceCache[id].valueCompareType = new byte[1];
			interfaceCache[id].valueIndexArray[0] = new int[4];
			interfaceCache[id].valueIndexArray[0][0] = 4;
			interfaceCache[id].valueIndexArray[0][1] = 3214;
			interfaceCache[id].valueIndexArray[0][2] = 995;
			interfaceCache[id].valueIndexArray[0][3] = 0;
			interfaceCache[id].requiredValues[0] = money[i];
			interfaceCache[id].valueCompareType[0] = 10;
			tab.child(bounds++, id++, x + 240, y + 25);
			y += 64;

		}
	}

	/**
	 * In Game Countdown interface
	 */
	public static void clanGameWait() {
		int id = 31500;
		RSInterface rsi = addInterface(id);
		addSprite(id + 1, 462);
		addText(id + 2, "SoulPlay:", 1, 0xff9b00, false, true);
		addText(id + 3, "Opposing:", 1, 0xff9b00, false, true);
		addText(id + 4, "Countdown to Battle:", 1, 0xff9b00, false, true);
		addText(id + 5, "Players: 55", 1, 0xff9b00, false, true);
		addText(id + 6, "Players: 48", 1, 0xff9b00, false, true);
		addText(id + 7, "2:00", 1, 0xff9b00, false, true);

		int[][] children = { { id + 1, 341, 3 }, { id + 2, 350, 8 }, { id + 3, 436, 8 }, { id + 4, 370, 47 },
				{ id + 5, 350, 22 }, { id + 6, 436, 22 }, { id + 7, 410, 60 } };

		rsi.totalChildren(children.length);
		for (int i = 0; i < children.length; i++) {
			rsi.child(i, children[i][0], children[i][1], children[i][2]);
		}
	}

	/**
	 * In Game Interface
	 */
	public static void clanGame() {
		int id = 32000;
		RSInterface rsi = addInterface(id);
		addTransparentSprite(id + 1, 464);
		addText(id + 2, "SoulPlay:", 1, 0xff9b00, false, true);
		addText(id + 3, "Opposing:", 1, 0xff9b00, false, true);
		addText(id + 4, "Kills: 5", 1, 0xff9b00, false, true);
		addText(id + 5, "Players: 55", 1, 0xff9b00, false, true);
		addText(id + 6, "Players: 48", 1, 0xff9b00, false, true);
		addText(id + 7, "Kills: 30", 1, 0xff9b00, false, true);

		int[][] children = { { id + 1, 341, 3 }, { id + 2, 350, 8 }, { id + 3, 436, 8 }, { id + 4, 350, 36 },
				{ id + 5, 350, 22 }, { id + 6, 436, 22 }, { id + 7, 436, 36 } };

		rsi.totalChildren(children.length);
		for (int i = 0; i < children.length; i++) {
			rsi.child(i, children[i][0], children[i][1], children[i][2]);
		}
	}

	public static void scrollsProgress() { // these pouches
											// have proper
											// hovering, but
											// needs to be
											// redone
											// completely
		RSInterface rsinterface = addInterface(38700);
		addButton(38701, 544, "Pouches", 27640, 1, 116, 20);
		addButton(38702, 545, "Scrolls", 27640, 1, 116, 20);
		rsinterface.totalChildren(7);
		rsinterface.child(0, 39701, 14, 17);
		rsinterface.child(1, 39702, 475, 30);
		rsinterface.child(2, 39703, 475, 30);
		rsinterface.child(3, 38701, 25, 30);
		rsinterface.child(4, 38702, 128, 30);
		rsinterface.child(5, 39707, 268, 30);
		rsinterface.child(6, 38710, 35, 65);
		RSInterface scroll = addInterface(38710);
		scroll.width = 430;
		scroll.height = 230;
		scroll.scrollMax = 550;
		int lastId = 38710;

		int startInterfaceId = lastId + 1;

		int totalScrolls = CreationData.values().length;

		scroll.totalChildren(totalScrolls * 3); // times 3 cuz we put hover
												// inside of addPouchLesik
												// function

		ArrayList<Integer> list = new ArrayList<Integer>();

		addText(39992, "This item requires", 0xaf6a1a, true, true, 52, 0);

		int lastX = -52;
		int lastY = 0;
		int lastChildIndex = 0;
		for (int i = 0; i < totalScrolls; i++) {
			int hoverId = 45131 + i * 6;
			list.add(hoverId);
			CreationData data = CreationData.values()[i];
			// first the background image box with hoverbox as child of it
			addScrollLesik(startInterfaceId + i, hoverId, 500, 504, 502, data);
			scroll.child(0 + i * 2, startInterfaceId + i, lastX += 52, lastY);
			// then add the sprite of the item
			startInterfaceId += 1;
			addItemSprite(startInterfaceId + i, data.getScrollId());
			scroll.child(1 + i * 2, startInterfaceId + i, lastX + 6, lastY + 6);
			lastChildIndex = 1 + i * 2;
			startInterfaceId += 1;

			if (lastX == 364) {
				lastX = -52;
				lastY += 55;
			}
		}

		int xPadding = 33;

		lastChildIndex++;

		int drawY = 0;

		for (int i = 0; i < list.size(); i++) {
			int drawX = 5 + (i % 8) * xPadding;
			if (drawX > 292)
				drawX -= 90;

			drawY = 55 * ((i / 8) + 1);
			if (i >= 64) {
				drawY = 360;
				if (i >= 72)
					drawY = 415;
			}
			scroll.child(lastChildIndex + i, list.get(i), drawX, drawY);
		}

		list = null;

	}

	public static void pouchesLesik() { // these pouches
										// have proper
										// hovering, but
										// needs to be
										// redone
										// completely
		RSInterface rsinterface = addInterface(39700);
		addSprite(39701, 702);
		addHover(39702, 3, 0, 39703, 300, "Close Window");
		addHovered(39703, 301, 39704);
		addButton(39705, 703, "Pouches", 27640, 1, 116, 20);
		addButton(39706, 704, "Scrolls", 27640, 1, 116, 20);
		addText(39707, "Summoning Pouch Creation", 2, 0xff981f, false, true);

		rsinterface.totalChildren(7);
		rsinterface.child(0, 39701, 14, 17);
		rsinterface.child(1, 39702, 475, 30);
		rsinterface.child(2, 39703, 475, 30);
		rsinterface.child(3, 39705, 25, 30);
		rsinterface.child(4, 39706, 128, 30);
		rsinterface.child(5, 39707, 268, 30);
		rsinterface.child(6, 39710, 35, 65);

		RSInterface scroll = addInterface(39710);
		scroll.width = 430;
		scroll.height = 250;
		scroll.scrollMax = 550;
		int lastId = 39710;

		int startInterfaceId = lastId + 1;

		int totalPouches = CreationData.values().length;

		scroll.totalChildren(totalPouches * 3); // times 3 cuz we put hover
												// inside of addPouchLesik
												// function

		ArrayList<Integer> list = new ArrayList<Integer>();

		addSprite(39990, 408);
		addText(39991, "This item requires", 0xaf6a1a, true, true, 52, 0);

		int lastX = -52;
		int lastY = 0;
		int lastChildIndex = 0;
		for (int i = 0; i < totalPouches; i++) {
			int hoverId = 40900 + i * 12;
			CreationData data = CreationData.values()[i];
			list.add(hoverId);
			// first the background image box with hoverbox as child of it
			addPouchLesik(startInterfaceId + i, hoverId, 500, 504, 502, data);
			scroll.child(0 + i * 2, startInterfaceId + i, lastX += 52, lastY);
			// hoverbounds
			// setBounds(startInterfaceId + i + 1, 10, 10, 2 + i * 3, scroll);
			// then add the sprite of the item
			startInterfaceId += 1;// startInterfaceId += 13;
			addItemSprite(startInterfaceId + i, data.getPouchId(), 0, 0);
			scroll.child(1 + i * 2, startInterfaceId + i, lastX + 6, lastY + 6);
			lastChildIndex = 1 + i * 2;
			startInterfaceId += 1;

			if (lastX == 364) {
				lastX = -52;
				lastY += 55;
			}
		}

		int xPadding = 33;
		// int yPadding = 33;

		lastChildIndex++;

		int drawY = 0;

		for (int i = 0; i < list.size(); i++) {
			int drawX = 5 + (i % 8) * xPadding;
			if (drawX > 292)
				drawX -= 90;
			// int drawY = 55 + (i / 8) * yPadding;
			// if(drawY > 160)
			// drawY -= 80;

			drawY = 55 * ((i / 8) + 1);
			if (i >= 64) {
				drawY = 360;
				if (i >= 72)
					drawY = 415;
			}
			scroll.child(lastChildIndex + i, list.get(i), drawX, drawY);
		}

		list = null;

	}

	public static void BOB() {
		RSInterface tab = addInterface(39500);
		addSprite(39501, 430);
		addHoverButton(39505, 432, "Withdraw", -1, 39506, 1);
		addHoveredButton(39506, 434, 39507);

		RSInterface bobStorage = addInterface(39502);
		addToItemGroup(bobStorage, 6, 5, 22, 21, new String[] { "Take 1", "Take 5", "Take 10", "Take All", "Take X" });

		tab.totalChildren(6);
		tab.child(0, 39501, 0, 0);
		tab.child(1, 39502, 90, 76 - 14);
		tab.child(2, 39505, 410, 306 - 14);
		tab.child(3, 39506, 410, 306 - 14);
		tab.child(4, 39702, 416, 45 - 14);
		tab.child(5, 39703, 416, 45 - 14);
	}

	public static void runePouch() {
		RSInterface tab = addInterface(43660);
		addSprite(43661, 914);
		addSprite(43665, 913);
		addSprite(43666, 912);
		addText(43662, "Rune pouch", 2, 0xFFA500, true, true);
		addText(43663, "Pouch", 2, 0xFFA500, true, true);
		addText(43664, "Inventory", 2, 0xFFA500, true, true);
		addHover(43667, 3, 0, 43668, 910, "Close Window");
		addHovered(43668, 911, 43669);
		RSInterface add = addInterface(43670);
		addToItemGroup(add, 7, 4, 14, 2, new String[] { "Store-1", "Store-5", "Store-All", "Store-X" });
		add = addInterface(43671);
		addToItemGroup(add, 3, 1, 24, 0, new String[] { "Withdraw-1", "Withdraw-5", "Withdraw-All", "Withdraw-X" });
		tab.totalChildren(10);
		tab.child(0, 43661, 0, 0);
		tab.child(1, 43662, 253, 29);
		tab.child(2, 43663, 253, 62);
		tab.child(3, 43664, 253, 137);
		tab.child(4, 43665, 105, 57);
		tab.child(5, 43666, 342, 57);
		tab.child(6, 43667, 406, 26);
		tab.child(7, 43668, 406, 26);
		tab.child(8, 43670, 106, 153);
		tab.child(9, 43671, 186, 82);
	}

	public static void capeColor() {
		RSInterface Interface = addInterface(14000); // Interface ID
		// Background here
		addSprite(14001, 922);
		// Buttons Begin
		addHoverButton(14002, 984, 1, 21, 22, "Choose color", -1, 14002, 1);
		addHoverButton(14003, 984, 1, 21, 22, "Choose color", -1, 14003, 1);
		addHoverButton(14004, 984, 1, 21, 22, "Choose color", -1, 14004, 1);
		addHoverButton(14005, 984, 1, 21, 22, "Choose color", -1, 14005, 1);
		addHoverButton(14006, 984, 1, 21, 22, "Choose color", -1, 14006, 1);
		addHoverButton(14007, 984, 1, 21, 22, "Choose color", -1, 14007, 1);
		addHoverButton(14008, 984, 1, 21, 22, "Choose color", -1, 14008, 1);
		addHoverButton(14009, 984, 1, 21, 22, "Choose color", -1, 14009, 1);
		addHoverButton(14010, 984, 1, 21, 22, "Choose color", -1, 14010, 1);
		addHoverButton(14011, 984, 1, 21, 22, "Choose color", -1, 14011, 1);
		addHoverButton(14012, 984, 1, 21, 22, "Choose color", -1, 14012, 1);
		addHoverButton(14013, 984, 1, 21, 22, "Choose color", -1, 14013, 1);
		addHoverButton(14014, 984, 1, 21, 22, "Choose color", -1, 14014, 1);
		addHoverButton(14015, 984, 1, 21, 22, "Choose color", -1, 14015, 1);
		addHoverButton(14016, 984, 1, 21, 22, "Choose color", -1, 14016, 1);
		addHoverButton(14017, 984, 1, 21, 22, "Choose color", -1, 14017, 1);
		addHoverButton(14018, 984, 1, 21, 22, "Choose color", -1, 14018, 1);
		addHoverButton(14019, 984, 1, 21, 22, "Choose color", -1, 14019, 1);
		addHoverButton(14020, 984, 1, 21, 22, "Choose color", -1, 14020, 1);
		addHoverButton(14021, 984, 1, 21, 22, "Choose color", -1, 14021, 1);
		addHoverButton(14022, 984, 1, 21, 22, "Choose color", -1, 14022, 1);
		addHoverButton(14023, 984, 1, 21, 22, "Choose color", -1, 14023, 1);
		addHoverButton(14024, 984, 1, 21, 22, "Choose color", -1, 14024, 1);
		addHoverButton(14025, 984, 1, 21, 22, "Choose color", -1, 14025, 1);
		addHoverButton(14026, 984, 1, 21, 22, "Close", -1, 14026, 1);

		// Buttons End

		// Set Bounds Begin
		Interface.totalChildren(26);
		Interface.child(0, 14001, 109, 97);
		Interface.child(1, 14002, 155, 161);
		Interface.child(2, 14003, 183, 161);
		Interface.child(3, 14004, 213, 161);
		Interface.child(4, 14005, 242, 161);
		Interface.child(5, 14006, 271, 161);
		Interface.child(6, 14007, 300, 161);
		Interface.child(7, 14008, 329, 161);
		Interface.child(8, 14009, 358, 161);
		Interface.child(9, 14010, 155, 195);
		Interface.child(10, 14011, 183, 195);
		Interface.child(11, 14012, 213, 195);
		Interface.child(12, 14013, 242, 195);
		Interface.child(13, 14014, 271, 195);
		Interface.child(14, 14015, 300, 195);
		Interface.child(15, 14016, 329, 195);
		Interface.child(16, 14017, 358, 195);
		Interface.child(17, 14018, 155, 229);
		Interface.child(18, 14019, 183, 229);
		Interface.child(19, 14020, 213, 229);
		Interface.child(20, 14021, 242, 229);
		Interface.child(21, 14022, 271, 229);
		Interface.child(22, 14023, 300, 229);
		Interface.child(23, 14024, 329, 229);
		Interface.child(24, 14025, 358, 229);
		Interface.child(25, 14026, 398, 98);
		// Set Bounds End
	}

	public static void lunarSpellBook() {
		int air = 556;
		int mind = 558;
		int water = 555;
		int earth = 557;
		int fire = 554;
		int body = 559;
		int cosmic = 564;
		int chaos = 562;
		int astral = 9075;
		int nature = 561;
		int law = 563;
		int death = 560;
		int blood = 565;
		int soul = 566;

		InterfaceTargetMask useOnNpcMask = new InterfaceTargetMask().useOnNpc();
		InterfaceTargetMask useOnPlayerMask = new InterfaceTargetMask().useOnPlayer();
		InterfaceTargetMask useOnInvMask = new InterfaceTargetMask().useOnInventory();
		InterfaceTargetMask useOnObjMask = new InterfaceTargetMask().useOnObject();

		RSInterface tab = addInterface(29999, InterfaceType.TAB);

		addSprite(30000, 109);// home teleport
		addSpellData(30000, 30041, SpriteCache.get(109), SpriteCache.get(109), 1, "Home Teleport",
				"A teleport which requires no runes and no required level that teleports you to the main land.", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		addSprite(30001, 804);// bake pie
		addSpellData(30001, 30041, SpriteCache.get(413), SpriteCache.get(477), 65, "Bake Pie",
				"A very powerful air spell.", astral, 1, fire, 5, water, 4, -1, -1, useOnInvMask, false);
		addSprite(30002, 479);// cure plant
		addSpellData(30002, 30041, SpriteCache.get(415), SpriteCache.get(479), 66, "Cure plant",
				"cure disease on farming patch.", astral, 1, earth, 8, -1, -1, -1, -1, useOnObjMask, false);
		addSprite(30003, 481);// monster examine
		addSpellData(30003, 30041, SpriteCache.get(417), SpriteCache.get(481), 66, "Monster Examine",
				"Detect the combat statistics of a monster.", astral, 1, cosmic, 1, mind, 1, -1, -1, useOnNpcMask, false);
		addSprite(30004, 483);// npc contact
		addSpellData(30004, 30041, SpriteCache.get(419), SpriteCache.get(483), 67, "NPC Contact",
				"Speak with varied NPCs.", astral, 1, cosmic, 1, air, 2, -1, -1, useOnNpcMask, false);
		addSprite(30005, 485);// cure other
		addSpellData(30005, 30041, SpriteCache.get(421), SpriteCache.get(485), 68, "Cure Other",
				"Cure poisoned players.", astral, 1, law, 1, earth, 10, -1, -1, useOnPlayerMask, false);
		addSprite(30006, 487);// humidify
		addSpellData(30006, 30041, SpriteCache.get(423), SpriteCache.get(487), 68, "Humidify",
				"Fills certain vessels with water.", astral, 1, water, 3, fire, 1, -1, -1, null, false);
		addSpriteCache(30007, "magicon", 12);// monster Teleport
		addSpellData(30007, 30041, getSprite(12, aClass44, "magicon", mediaIndex), getSprite(12, aClass44, "magicon", mediaIndex), 1,
				"Monster Teleport", "Teleports you to monster locations.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(30008, "magicon", 15);// minigame Teleport
		addSpellData(30008, 30041, getSprite(15, aClass44, "magicon", mediaIndex), getSprite(15, aClass44, "magicon", mediaIndex), 1,
				"Minigame Teleport", "Teleports you to minigames.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(30009, "magicon", 18);// boss Teleport
		addSpellData(30009, 30041, getSprite(18, aClass44, "magicon", mediaIndex), getSprite(18, aClass44, "magicon", mediaIndex), 1,
				"Boss Teleport", "Teleports you to bosses.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSprite(30010, 489);// cure me
		addSpellData(30010, 30041, SpriteCache.get(425), SpriteCache.get(489), 71, "Cure me",
				"Cure the caster of poison.", law, 1, cosmic, 2, astral, 2, -1, -1, null, false);
		addSprite(30011, 491);// hunter kit
		addSpellData(30011, 30041, SpriteCache.get(427), SpriteCache.get(491), 95, "Hunter Kit",
				"Gives the player a hunter kit containing various hunter supplies.", astral, 2, earth, 2, -1, -1, -1,
				-1, null, false);
		addSpriteCache(30012, "magicon", 22);// PKing teleport
		addSpellData(30012, 30041, getSprite(22, aClass44, "magicon", mediaIndex), getSprite(22, aClass44, "magicon", mediaIndex), 1,
				"PKing teleport", "Teleports you to pking locations", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(30013, "magicon", 39);// Skilling teleport
		addSpellData(30013, 30041, getSprite(39, aClass44, "magicon", mediaIndex), getSprite(39, aClass44, "magicon", mediaIndex), 1,
				"Skilling teleport", "Teleports you to skilling locations", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSprite(30014, 493);// cure group
		addSpellData(30014, 30041, SpriteCache.get(429), SpriteCache.get(493), 74, "Cure Group",
				"Cures all players within a 3x3 square area of the player casting it.", law, 2, cosmic, 2, astral, 2,
				-1, -1, null, false);
		addSprite(30015, 495);// Stat Spy
		addSpellData(30015, 30041, SpriteCache.get(431), SpriteCache.get(495), 75, "Stat Spy",
				"Can be cast on other players to see their combat stats as well as non-combat stats.", cosmic, 2,
				astral, 2, body, 5, -1, -1, useOnPlayerMask, false);
		addSpriteCache(30016, "magicon", 40);// City teleport
		addSpellData(30016, 30041, getSprite(40, aClass44, "magicon", mediaIndex), getSprite(40, aClass44, "magicon", mediaIndex), 1,
				"City teleport", "Teleports you to cities around the world.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(30017, "magicon2", 28);// PKing teleport
		addSpellData(30017, 30041, getSprite(28, aClass44, "magicon2", mediaIndex), getSprite(28, aClass44, "magicon2", mediaIndex), 1,
				"Dungeon teleport", "Teleports you to various dungeons around the world.", -1, -1, -1, -1, -1, -1, -1,
				-1, null, false);
		addSprite(30018, 497);// superglass make
		addSpellData(30018, 30041, SpriteCache.get(433), SpriteCache.get(497), 77, "Superglass Make",
				"Make glass without a furnace.", astral, 2, fire, 6, air, 10, -1, -1, null, false);
		addSprite(30019, 499);// khazard teleport
		addSpellData(30019, 30041, SpriteCache.get(435), SpriteCache.get(499), 78, "Khazard Teleport",
				"Teleports you to Port Khazard.", law, 2, astral, 2, water, 4, -1, -1, null, false);
		addSprite(30020, 501);// khazard teleport
		addSpellData(30020, 30041, SpriteCache.get(437), SpriteCache.get(501), 79, "Khazard Tele Group",
				"Teleports the player along with anyone within a 3x3 square area.", law, 2, astral, 2, water, 8, -1, -1,
				null, false);
		addSprite(30021, 503);// dream
		addSpellData(30021, 30041, SpriteCache.get(439), SpriteCache.get(503), 79, "Dream",
				"Take a rest and restore hitpoints 3 times faster.", cosmic, 1, astral, 2, body, 5, -1, -1, null, false);
		addSprite(30022, 505);// string jewellery
		addSpellData(30022, 30041, SpriteCache.get(441), SpriteCache.get(505), 80, "String Jewellery",
				"Strings all unstrung amulets in the player's inventory without the need of a ball of wool.", astral, 2,
				earth, 10, water, 5, -1, -1, null, false);
		addSprite(30023, 507);// share stat restore
		addSpellData(30023, 30041, SpriteCache.get(443), SpriteCache.get(507), 81, "Share Stat Restore",
				"Casting this on a Stat restore potion in the player's inventory causes the player to administer 1 dose to each player in a 3x3 square area",
				astral, 2, earth, 10, water, 10, -1, -1, useOnInvMask, false);
		addSprite(30024, 509);// magic imbue
		addSpellData(30024, 30041, SpriteCache.get(445), SpriteCache.get(509), 82, "Magic Imbue",
				"Combine runes without a talisman.", astral, 2, fire, 7, water, 7, -1, -1, null, false);
		addSprite(30025, 511);// fertile soil
		addSpellData(30025, 30041, SpriteCache.get(447), SpriteCache.get(511), 83, "Fertile Soil",
				"This spell treats the targeted Farming patch with super compost.", nature, 2, astral, 3, earth, 15, -1,
				-1, useOnObjMask, false);
		addSprite(30026, 512);// boost potion share
		addSpellData(30026, 30041, SpriteCache.get(449), SpriteCache.get(512), 84, "Boost Potion Share",
				"Casting this on almost any potion in the player's inventory causes the player to administer 1 dose to each player in a 3x3 square area ",
				astral, 3, earth, 12, water, 10, -1, -1, useOnInvMask, false);
		addSprite(30027, 513);// fishing guild teleport
		addSpellData(30027, 30041, SpriteCache.get(451), SpriteCache.get(513), 85, "Fishing Guild Teleport",
				"Teleports the player outside the door of the Fishing Guild.", law, 3, astral, 3, water, 10, -1, -1,
				null, false);
		addSprite(30028, 1255, 24, 24);//Teleport to bounty target
		addSpellData(30028, 30041, SpriteCache.get(1254), SpriteCache.get(1255), 85,
				"Teleport to Bounty Target", "Teleports you near your Bounty Hunter target", law, 1, death, 1, chaos, 1, -1, -1, null, false);
		
		addSprite(30029, 514);// fishing guild tele group
		addSpellData(30029, 30041, SpriteCache.get(453), SpriteCache.get(514), 86, "Fishing Guild Tele Group",
				"Teleports the player along with anyone within a 3x3 square area outside the door to the Fishing.", law,
				3, astral, 3, water, 14, -1, -1, null, false);
		addSprite(30030, 515);// plank make
		addSpellData(30030, 30041, SpriteCache.get(455), SpriteCache.get(515), 86, "Plank Make",
				"Turns one log into a plank.", nature, 1, astral, 2, earth, 15, -1, -1, null, false);
		addSprite(30031, 516);// catherby teleport
		addSpellData(30031, 30041, SpriteCache.get(457), SpriteCache.get(516), 87, "Catherby Teleport",
				"Teleports you to catherby.", law, 3, astral, 3, water, 10, -1, -1, null, false);
		addSprite(30032, 517);// catherby tele group
		addSpellData(30032, 30041, SpriteCache.get(459), SpriteCache.get(517), 88, "Catherby Tele Group",
				"Teleports the player along with anyone within a 3x3 square area to Catherby.", law, 3, astral, 3,
				water, 15, -1, -1, null, false);
		addSprite(30033, 518);// ice plateau teleport
		addSpellData(30033, 30041, SpriteCache.get(461), SpriteCache.get(518), 89, "Ice Plateau Teleport",
				"Teleports the player to the members half of the Ice Plateau in level 53 Wilderness.", law, 3, astral,
				3, water, 8, -1, -1, null, false);
		addSprite(30034, 520);// Ice Plateau Tele Group
		addSpellData(30034, 30041, SpriteCache.get(463), SpriteCache.get(520), 90, "Ice Plateau Tele Group",
				"Teleports the player along with anyone within a 3x3 square area to the members half of the Ice Plateau.",
				law, 3, astral, 3, water, 16, -1, -1, null, false);
		addSprite(30035, 522);// energy transfer
		addSpellData(30035, 30041, SpriteCache.get(465), SpriteCache.get(522), 91, "Energy Transfer",
				"Send HP and SA enerty to another players HP and run energy. ", law, 2, nature, 1, astral, 3, -1, -1,
				useOnPlayerMask, false);
		addSprite(30036, 524);// heal other
		addSpellData(30036, 30041, SpriteCache.get(467), SpriteCache.get(524), 92, "Heal other",
				"Transfer up to 75% of hitpoints to another player.", blood, 1, law, 3, astral, 3, -1, -1,
				useOnPlayerMask, false);
		addSprite(30037, 525);// vengeance other
		addSpellData(30037, 30041, SpriteCache.get(469), SpriteCache.get(525), 93, "Vengeance Other",
				"Allows another player to rebound damage to an opponent.", death, 2, astral, 3, earth, 10, -1, -1,
				useOnPlayerMask, false);
		addSprite(30038, 526);// vengeance
		addSpellData(30038, 30041, SpriteCache.get(471), SpriteCache.get(526), 94, "Vengeance",
				"Rebound damage to an opponent.", death, 2, astral, 4, earth, 10, -1, -1, null, false);
		addSprite(30039, 527);// heal group
		addSpellData(30039, 30041, SpriteCache.get(473), SpriteCache.get(527), 95, "Heal Group",
				"Transfer up to 75% of hitpoints to a group in 3x3 square area.", blood, 3, law, 6, astral, 4, -1, -1,
				null, false);

		addText(30040, "", 0xFFFFFF, false, true, -1, 2); // veng timer

		addInterface(30041);// Dynamic layer

		tab.totalChildren(42);
		tab.child(0, 30000, 11, 10);
		tab.child(1, 30001, 40, 9);
		tab.child(2, 30002, 71, 12);
		tab.child(3, 30003, 103, 10);
		tab.child(4, 30004, 135, 12);
		tab.child(5, 30005, 165, 10);
		tab.child(6, 30006, 8, 38);
		tab.child(7, 30007, 39, 39);
		tab.child(8, 30008, 71, 39);
		tab.child(9, 30009, 103, 39);
		tab.child(10, 30010, 135, 39);
		tab.child(11, 30011, 165, 37);
		tab.child(12, 30012, 12, 68);
		tab.child(13, 30013, 42, 68);
		tab.child(14, 30014, 71, 68);
		tab.child(15, 30015, 103, 68);
		tab.child(16, 30016, 135, 68);
		tab.child(17, 30017, 165, 68);
		tab.child(18, 30018, 14, 97);
		tab.child(19, 30019, 42, 97);
		tab.child(20, 30020, 71, 97);
		tab.child(21, 30021, 101, 97);
		tab.child(22, 30022, 135, 98);
		tab.child(23, 30023, 168, 98);
		tab.child(24, 30024, 11, 125);
		tab.child(25, 30025, 42, 124);
		tab.child(26, 30026, 74, 125);
		tab.child(27, 30027, 103, 125);
		tab.child(28, 30028, 135, 125);
		tab.child(29, 30029, 164, 126);
		tab.child(30, 30030, 10, 155);
		tab.child(31, 30031, 42, 155);
		tab.child(32, 30032, 71, 155);
		tab.child(33, 30033, 103, 155);
		tab.child(34, 30034, 136, 155);
		tab.child(35, 30035, 165, 155);
		tab.child(36, 30036, 13, 185);
		tab.child(37, 30037, 42, 185);
		tab.child(38, 30038, 71, 184);
		tab.child(39, 30039, 103, 184);
		tab.child(40, 30040, 42, 184);
		tab.child(41, 30041, 0, 0);
	}

	public static void Sidebar0() {
		Sidebar0a(1698, 1701, 7499, "Chop", "Hack", "Smash", "Block", 42, 75, 127, 75, 39, 128, 125, 128, 122, 103, 40,
				50, 122, 50, 40, 103, "Accurate\nSlash\nAttack XP", "Aggressive\nSlash\nStrength XP",
				"Aggressive\nCrush\nStrength XP", "Defensive\nSlash\nDefence XP", 40132, 40136, 40140, 40144); // OK

		Sidebar0a(2276, 2279, 7574, "Stab", "Lunge", "Slash", "Block", 43, 75, 124, 75, 41, 128, 125, 128, 122, 103, 40,
				50, 122, 50, 40, 103, "Accurate\nStab\nAttack XP", "Aggressive\nStab\nStrength XP",
				"Aggressive\nSlash\nStrength XP", "Defensive\nStab\nDefence XP", 40020, 40024, 40028, 40032); // OK

		Sidebar0a(2423, 2426, 7599, "Chop", "Slash", "Lunge", "Block", 42, 75, 125, 75, 40, 128, 125, 128, 122, 103, 40,
				50, 122, 50, 40, 103, "Accurate\nSlash\nAttack XP", "Aggressive\nSlash\nStrength XP",
				"Controlled\nStab\nShared XP", "Defensive\nSlash\nDefence XP", 40036, 40040, 40044, 40048); // OK

		Sidebar0a(3796, 3799, 7624, "Pound", "Pummel", "Spike", "Block", 39, 75, 121, 75, 41, 128, 125, 128, 122, 103,
				40, 50, 122, 50, 40, 103, "Accurate\nCrush\nAttack XP", "Aggressive\nCrush\nStrength XP",
				"Controlled\nStab\nShared XP", "Defensive\nCrush\nDefence XP", 40052, 40056, 40060, 40064); // WTF
																											// IS
		// THIS?!

		Sidebar0a(4679, 4682, 7674, "Lunge", "Swipe", "Pound", "Block", 40, 75, 124, 75, 39, 128, 125, 128, 122, 103,
				40, 50, 122, 50, 40, 103, "Controlled\nStab\nShared XP", "Controlled\nSlash\nShared XP",
				"Controlled\nCrush\nShared XP", "Defensive\nStab\nDefence XP", 40068, 40072, 40076, 40080); // OK

		Sidebar0a(4705, 4708, 7699, "Chop", "Slash", "Smash", "Block", 42, 75, 125, 75, 39, 128, 125, 128, 122, 103, 40,
				50, 122, 50, 40, 103, "Accurate\nSlash\nAttack XP", "Aggressive\nSlash\nStrength XP",
				"Aggressive\nCrush\nStrength XP", "Defensive\nSlash\nDefence XP", 40084, 40088, 40092, 40096); // ???

		Sidebar0a(5570, 5573, 7724, "Spike", "Impale", "Smash", "Block", 41, 75, 123, 75, 39, 128, 125, 128, 122, 103,
				40, 50, 122, 50, 40, 103, "Accurate\nStab\nAttack XP", "Aggressive\nStab\nStrength XP",
				"Aggressive\nCrush\nStrength XP", "Defensive\nStab\nDefence XP", 40010, 40104, 40108, 40112); // OK

		Sidebar0a(7762, 7765, 7800, "Chop", "Slash", "Lunge", "Block", 42, 75, 125, 75, 40, 128, 125, 128, 122, 103, 40,
				50, 122, 50, 40, 103, "Accurate\nSlash\nAttack XP", "Aggressive\nSlash\nStrength XP",
				"Controlled\nStab\nShared XP", "Defensive\nSlash\nDefence XP", 40116, 40120, 40124, 40128); // OK

		Sidebar0b(776, 779, "Reap", "Chop", "Jab", "Block", 42, 75, 126, 75, 46, 128, 125, 128, 122, 103, 122, 50, 40,
				103, 40, 50, "Accurate\nSlash\nAttack XP", "Aggressive\nStab\nStrength XP",
				"Aggressive\nCrush\nStrength XP", "Defensive\nSlash\nDefence XP", 40236, 40240, 40244, 40248); // ???

		Sidebar0c(425, 428, 7474, "Pound", "Pummel", "Block", 39, 75, 121, 75, 42, 128, 40, 103, 40, 50, 122, 50,
				"Accurate\nCrush\nAttack XP", "Aggressive\nCrush\nStrength XP", "Defensive\nCrush\nDefence XP", 40148,
				40152, 40156); // OK

		Sidebar0c(1749, 1752, 7524, "Accurate", "Rapid", "Longrange", 33, 75, 125, 75, 29, 128, 40, 103, 40, 50, 122,
				50, "Accurate\nRanged XP", "Rapid\nRanged XP", "Long range\nRanged XP\nDefence XP", 40160, 40164,
				40168); // OK

		Sidebar0c(1764, 1767, 7549, "Accurate", "Rapid", "Longrange", 33, 75, 125, 75, 29, 128, 40, 103, 40, 50, 122,
				50, "Accurate\nRanged XP", "Rapid\nRanged XP", "Long range\nRanged XP\nDefence XP", 40172, 40176,
				40180); // OK

		Sidebar0c(4446, 4449, 7649, "Accurate", "Rapid", "Longrange", 33, 75, 125, 75, 29, 128, 40, 103, 40, 50, 122,
				50, "Accurate\nRanged XP", "Rapid\nRanged XP", "Long range\nRanged XP\nDefence XP", 40184, 40188,
				40192); // OK

		Sidebar0c(5855, 5857, 7749, "Punch", "Kick", "Block", 40, 75, 129, 75, 42, 128, 40, 50, 122, 50, 40, 103,
				"Accurate\nCrush\nAttack XP", "Aggressive\nCrush\nStrength XP", "Defensive\nCrush\nDefence XP", 40196,
				40200, 40204); // OK

		Sidebar0c(6103, 6132, 6117, "Bash", "Pound", "Block", 43, 75, 124, 75, 42, 128, 40, 103, 40, 50, 122, 50,
				"Accurate\nCrush\nAttack XP", "Aggressive\nCrush\nStrength XP", "Defensive\nCrush\nDefence XP", 40208,
				40212, 40216); // ???

		Sidebar0c(8460, 8463, 8493, "Jab", "Swipe", "Fend", 46, 75, 124, 75, 43, 128, 40, 103, 40, 50, 122, 50,
				"Controlled\nStabbed\nShared XP", "Aggressive\nSlash\nStrength XP", "Defensive\nStab\nDefence XP",
				40224, 40228, 40232); // OK

		Sidebar0c(12290, 12293, 12323, "Flick", "Lash", "Deflect", 44, 75, 127, 75, 36, 128, 40, 50, 40, 103, 122, 50,
				"Accurate\nSlash\nAttack XP", "Controlled\nSlash\nShared XP", "Defensive\nSlash\nDefence XP", 40252,
				40256, 40260); // OK

		Sidebar0d(328, 331, "Bash", "Pound", "Focus", 42, 66, 39, 101, 41, 136, 40, 120, 40, 50, 40, 85);

		RSInterface rsi = addInterface(19300, InterfaceType.TAB);
		textSize(3983, 0);
		addAttackStyleButton2(150, 340, 341, 172, 150, 44, "Auto Retaliate", 40000, 154, 42,
				"When active, you will\nautomatically fight back if\nattacked.");

		rsi.totalChildren(3);
		rsi.child(0, 3983, 52, 25); // combat level
		rsi.child(1, 150, 21, 153); // auto retaliate
		rsi.child(2, 40000, 26, 200); // auto retaliate hover box

		rsi = interfaceCache[3983];
		rsi.centerText = true;
		rsi.disabledColor = 0xff981f;
	}

	public static void Sidebar0a(int id, int id2, int id3, String text1, String text2, String text3, String text4,
			int str1x, int str1y, int str2x, int str2y, int str3x, int str3y, int str4x, int str4y, int img1x,
			int img1y, int img2x, int img2y, int img3x, int img3y, int img4x, int img4y, String popupString1,
			String popupString2, String popupString3, String popupString4, int hoverID1, int hoverID2, int hoverID3,
			int hoverID4) // 4button
	// spec
	{
		RSInterface rsi = addInterface(id, InterfaceType.TAB); // 2423
		addAttackText(id2, "-2", 3, 0xff981f, true); // 2426
		addAttackText(id2 + 11, text1, 0, 0xff981f, false);
		addAttackText(id2 + 12, text2, 0, 0xff981f, false);
		addAttackText(id2 + 13, text3, 0, 0xff981f, false);
		addAttackText(id2 + 14, text4, 0, 0xff981f, false);

		rsi.specialBar(id3); // 7599

		addAttackHover(id2 + 3, hoverID1, popupString1);
		addAttackHover(id2 + 6, hoverID2, popupString2);
		addAttackHover(id2 + 5, hoverID3, popupString3);
		addAttackHover(id2 + 4, hoverID4, popupString4);

		rsi.width = 190;
		rsi.height = 261;

		int frame = 0;
		rsi.totalChildren(20);

		rsi.child(frame, id2 + 3, 21, 46);
		frame++; // 2429
		rsi.child(frame, id2 + 4, 104, 99);
		frame++; // 24301
		rsi.child(frame, id2 + 5, 21, 99);
		frame++; // 2431
		rsi.child(frame, id2 + 6, 105, 46);
		frame++; // 2432

		rsi.child(frame, id2 + 7, img1x, img1y);
		frame++; // bottomright 2433
		rsi.child(frame, id2 + 8, img2x, img2y);
		frame++; // topleft 2434
		rsi.child(frame, id2 + 9, img3x, img3y);
		frame++; // bottomleft 2435
		rsi.child(frame, id2 + 10, img4x, img4y);
		frame++; // topright 2436

		rsi.child(frame, id2 + 11, str1x, str1y);
		frame++; // chop 2437
		rsi.child(frame, id2 + 12, str2x, str2y);
		frame++; // slash 2438
		rsi.child(frame, id2 + 13, str3x, str3y);
		frame++; // lunge 2439
		rsi.child(frame, id2 + 14, str4x, str4y);
		frame++; // block 2440

		rsi.child(frame, id3, 21, 205);
		frame++; // special attack 7599
		rsi.child(frame, 19300, 0, 0);
		frame++; // stuffs
		rsi.child(frame, id2, 94, 4);
		frame++; // weapon 2426
		rsi.child(frame, hoverID1, 25, 96);
		frame++;
		rsi.child(frame, hoverID2, 108, 96);
		frame++;
		rsi.child(frame, hoverID3, 25, 149);
		frame++;
		rsi.child(frame, hoverID4, 108, 149);
		frame++;
		rsi.child(frame, 40005, 28, 149);
		frame++; // special bar tooltip

		for (int i = id2 + 3; i < id2 + 7; i++) { // 2429 - 2433
			rsi = interfaceCache[i];
			rsi.disabledSprite = SpriteCache.get(358);
			rsi.enabledSprite = SpriteCache.get(359);
			rsi.width = 68;
			rsi.height = 44;
		}
	}

	public static void Sidebar0b(int id, int id2, String text1, String text2, String text3, String text4, int str1x,
			int str1y, int str2x, int str2y, int str3x, int str3y, int str4x, int str4y, int img1x, int img1y,
			int img2x, int img2y, int img3x, int img3y, int img4x, int img4y, String popupString1, String popupString2,
			String popupString3, String popupString4, int hoverID1, int hoverID2, int hoverID3, int hoverID4) // 4button
	// nospec
	{
		RSInterface rsi = addInterface(id, InterfaceType.TAB); // 2423
		addAttackText(id2, "-2", 3, 0xff981f, true); // 2426
		addAttackText(id2 + 11, text1, 0, 0xff981f, false);
		addAttackText(id2 + 12, text2, 0, 0xff981f, false);
		addAttackText(id2 + 13, text3, 0, 0xff981f, false);
		addAttackText(id2 + 14, text4, 0, 0xff981f, false);

		addAttackHover(id2 + 3, hoverID1, popupString1);
		addAttackHover(id2 + 6, hoverID2, popupString2);
		addAttackHover(id2 + 5, hoverID3, popupString3);
		addAttackHover(id2 + 4, hoverID4, popupString4);

		rsi.width = 190;
		rsi.height = 261;

		int frame = 0;
		rsi.totalChildren(18);

		rsi.child(frame, id2 + 3, 21, 46);
		frame++; // 2429
		rsi.child(frame, id2 + 4, 104, 99);
		frame++; // 2430
		rsi.child(frame, id2 + 5, 21, 99);
		frame++; // 2431
		rsi.child(frame, id2 + 6, 105, 46);
		frame++; // 2432

		rsi.child(frame, id2 + 7, img1x, img1y);
		frame++; // bottomright 2433
		rsi.child(frame, id2 + 8, img2x, img2y);
		frame++; // topleft 2434
		rsi.child(frame, id2 + 9, img3x, img3y);
		frame++; // bottomleft 2435
		rsi.child(frame, id2 + 10, img4x, img4y);
		frame++; // topright 2436

		rsi.child(frame, id2 + 11, str1x, str1y);
		frame++; // chop 2437
		rsi.child(frame, id2 + 12, str2x, str2y);
		frame++; // slash 2438
		rsi.child(frame, id2 + 13, str3x, str3y);
		frame++; // lunge 2439
		rsi.child(frame, id2 + 14, str4x, str4y);
		frame++; // block 2440

		rsi.child(frame, 19300, 0, 0);
		frame++; // stuffs
		rsi.child(frame, id2, 94, 4);
		frame++; // weapon 2426
		rsi.child(frame, hoverID1, 25, 96);
		frame++;
		rsi.child(frame, hoverID2, 108, 96);
		frame++;
		rsi.child(frame, hoverID3, 25, 149);
		frame++;
		rsi.child(frame, hoverID4, 108, 149);
		frame++;

		for (int i = id2 + 3; i < id2 + 7; i++) { // 2429 - 2433
			rsi = interfaceCache[i];
			rsi.disabledSprite = SpriteCache.get(358);
			rsi.enabledSprite = SpriteCache.get(359);
			rsi.width = 68;
			rsi.height = 44;
		}
	}

	public static void Sidebar0c(int id, int id2, int id3, String text1, String text2, String text3, int str1x,
			int str1y, int str2x, int str2y, int str3x, int str3y, int img1x, int img1y, int img2x, int img2y,
			int img3x, int img3y, String popupString1, String popupString2, String popupString3, int hoverID1,
			int hoverID2, int hoverID3) // 3button spec
	{
		RSInterface rsi = addInterface(id, InterfaceType.TAB); // 2423
		addAttackText(id2, "-2", 3, 0xff981f, true); // 2426
		addAttackText(id2 + 9, text1, 0, 0xff981f, false);
		addAttackText(id2 + 10, text2, 0, 0xff981f, false);
		addAttackText(id2 + 11, text3, 0, 0xff981f, false);

		rsi.specialBar(id3); // 7599

		addAttackHover(id2 + 5, hoverID1, popupString1);
		addAttackHover(id2 + 4, hoverID2, popupString2);
		addAttackHover(id2 + 3, hoverID3, popupString3);

		rsi.width = 190;
		rsi.height = 261;

		int frame = 0;
		rsi.totalChildren(16);

		rsi.child(frame, id2 + 3, 21, 99);
		frame++;
		rsi.child(frame, id2 + 4, 105, 46);
		frame++;
		rsi.child(frame, id2 + 5, 21, 46);
		frame++;

		rsi.child(frame, id2 + 6, img1x, img1y);
		frame++; // topleft
		rsi.child(frame, id2 + 7, img2x, img2y);
		frame++; // bottomleft
		rsi.child(frame, id2 + 8, img3x, img3y);
		frame++; // topright

		rsi.child(frame, id2 + 9, str1x, str1y);
		frame++; // chop
		rsi.child(frame, id2 + 10, str2x, str2y);
		frame++; // slash
		rsi.child(frame, id2 + 11, str3x, str3y);
		frame++; // lunge

		rsi.child(frame, id3, 21, 205);
		frame++; // special attack 7599
		rsi.child(frame, 19300, 0, 0);
		frame++; // stuffs
		rsi.child(frame, id2, 94, 4);
		frame++; // weapon

		rsi.child(frame, hoverID1, 25, 96);
		frame++;
		rsi.child(frame, hoverID2, 108, 96);
		frame++;
		rsi.child(frame, hoverID3, 25, 149);
		frame++;
		rsi.child(frame, 40005, 28, 149);
		frame++; // special bar tooltip

		for (int i = id2 + 3; i < id2 + 6; i++) {
			rsi = interfaceCache[i];
			rsi.disabledSprite = SpriteCache.get(358);
			rsi.enabledSprite = SpriteCache.get(359);
			rsi.width = 68;
			rsi.height = 44;
		}
	}

	public static void Sidebar0d(int id, int id2, String text1, String text2, String text3, int str1x, int str1y,
			int str2x, int str2y, int str3x, int str3y, int img1x, int img1y, int img2x, int img2y, int img3x,
			int img3y) // 3button nospec
	// (magic intf)
	{
		RSInterface rsi = addInterface(id, InterfaceType.TAB); // 2423
		addAttackText(id2, "-2", 3, 0xff981f, true); // 2426
		addAttackText(id2 + 9, text1, 0, 0xff981f, false);
		addAttackText(id2 + 10, text2, 0, 0xff981f, false);
		addAttackText(id2 + 11, text3, 0, 0xff981f, false);

		addAttackText(353, "Spell", 0, 0xff981f, false);
		addAttackText(354, "Spell", 0, 0xff981f, false);

		if (!Client.doneLoading) {
			addCacheSprite(337, 19, "combaticons");
			addCacheSprite(338, 13, "combaticons2");
			addCacheSprite(339, 14, "combaticons2");
		}

		addToggleButton(349, 342, 343, 108, 68, 44, "Select");
		addToggleButton(350, 344, 345, 108, 68, 44, "Select");

		rsi.width = 190;
		rsi.height = 261;

		int last = 15;
		int frame = 0;
		rsi.totalChildren(last);

		rsi.child(frame, id2 + 3, 20, 115);
		frame++;
		rsi.child(frame, id2 + 4, 20, 80);
		frame++;
		rsi.child(frame, id2 + 5, 20, 45);
		frame++;

		rsi.child(frame, id2 + 6, img1x, img1y);
		frame++; // topleft
		rsi.child(frame, id2 + 7, img2x, img2y);
		frame++; // bottomleft
		rsi.child(frame, id2 + 8, img3x, img3y);
		frame++; // topright

		rsi.child(frame, id2 + 9, str1x, str1y);
		frame++; // bash
		rsi.child(frame, id2 + 10, str2x, str2y);
		frame++; // pound
		rsi.child(frame, id2 + 11, str3x, str3y);
		frame++; // focus

		rsi.child(frame, 349, 105, 46);
		frame++; // spell1
		rsi.child(frame, 350, 104, 106);
		frame++; // spell2

		rsi.child(frame, 353, 125, 74);
		frame++; // spell
		rsi.child(frame, 354, 125, 134);
		frame++; // spell

		rsi.child(frame, 19300, 0, 0);
		frame++; // stuffs
		rsi.child(frame, id2, 94, 4);
		frame++; // weapon
	}

	public static void Pestpanel() {
		RSInterface main = addInterface(21119, InterfaceType.MAIN);
		addText(21120, "Next Departure:", 0xCCCBCB, false, true, 52, 1);
		addText(21121, "Players Ready:", 0x5BD230, false, true, 52, 1);
		addText(21122, "(Need 5 to 25 players)", 0xDED36A, false, true, 52, 1);
		addText(21123, "Pest Points:", 0x99FFFF, false, true, 52, 1);
		main.totalChildren(4);
		main.child(0, 21120, 15, 12);
		main.child(1, 21121, 15, 30);
		main.child(2, 21122, 15, 48);
		main.child(3, 21123, 15, 66);

		RSInterface rsi = interfaceCache[6114] = new RSInterface();
		RSInterface.checkTaken(6114);
		rsi.interfaceType = 4;
		rsi.width = 390;
		rsi.centerText = true;
	}

	public static void Pestpanel2() {
		RSInterface RSinterface = addInterface(21100);
		addSprite(21101, 333);
		addSprite(21102, 334);
		addSprite(21103, 335);
		addSprite(21104, 336);
		addSprite(21105, 337);
		addSprite(21106, 338);
		addText(21107, "", 0xCC00CC, false, true, 52, 1);
		addText(21108, "", 0x0000FF, false, true, 52, 1);
		addText(21109, "", 0xFFFF44, false, true, 52, 1);
		addText(21110, "", 0xCC0000, false, true, 52, 1);
		addText(21111, "", 0x99FF33, false, true, 52, 1);// w purp
		addText(21112, "", 0x99FF33, false, true, 52, 1);// e blue
		addText(21113, "", 0x99FF33, false, true, 52, 1);// se yel
		addText(21114, "", 0x99FF33, false, true, 52, 1);// sw red
		addText(21115, "200", 0x99FF33, false, true, 52, 1);// attacks
		addText(21116, "", 0x99FF33, false, true, 52, 1);// knights hp
		addText(21117, "Time Remaining:", 0xFFFFFF, false, true, 52, 0);
		addText(21118, "", 0xFFFFFF, false, true, 52, 0);
		RSinterface.totalChildren(18);
		RSinterface.child(0, 21101, 361, 26);
		RSinterface.child(1, 21102, 396, 26);
		RSinterface.child(2, 21103, 436, 26);
		RSinterface.child(3, 21104, 474, 26);
		RSinterface.child(4, 21105, 3, 21);
		RSinterface.child(5, 21106, 3, 50);
		RSinterface.child(6, 21107, 371, 60);
		RSinterface.child(7, 21108, 409, 60);
		RSinterface.child(8, 21109, 443, 60);
		RSinterface.child(9, 21110, 479, 60);
		RSinterface.child(10, 21111, 362, 10);
		RSinterface.child(11, 21112, 398, 10);
		RSinterface.child(12, 21113, 436, 10);
		RSinterface.child(13, 21114, 475, 10);
		RSinterface.child(14, 21115, 32, 32);
		RSinterface.child(15, 21116, 32, 62);
		RSinterface.child(16, 21117, 8, 88);
		RSinterface.child(17, 21118, 87, 88);
	}

	public static void godWars() {
		RSInterface rsinterface = addInterface(16210, InterfaceType.MAIN);
		addText(16211, "NPC killcount", 0, 0xff9040, true, true);
		addText(16212, "Armadyl kills", 0, 0xff9040, true, true);
		addText(16213, "Bandos kills", 0, 0xff9040, true, true);
		addText(16214, "Saradomin kills", 0, 0xff9040, true, true);
		addText(16215, "Zamorak kills", 0, 0xff9040, true, true);
		addText(16220, "Zaros kills", 0, 0xff9040, true, true);
		addText(16216, "0", 0, 0x66FFFF, true, true);// armadyl
		addText(16217, "0", 0, 0x66FFFF, true, true);// bandos
		addText(16218, "0", 0, 0x66FFFF, true, true);// saradomin
		addText(16219, "0", 0, 0x66FFFF, true, true);// zamorak
		addText(16221, "0", 0, 0x66FFFF, true, true);// zaros
		rsinterface.scrollMax = 0;
		rsinterface.totalChildren(11);
		rsinterface.children[0] = 16211;
		rsinterface.childX[0] = -52 + 375 + 30;
		rsinterface.childY[0] = 7;
		rsinterface.children[1] = 16212;
		rsinterface.childX[1] = -52 + 375 + 30;
		rsinterface.childY[1] = 30;
		rsinterface.children[2] = 16213;
		rsinterface.childX[2] = -52 + 375 + 30;
		rsinterface.childY[2] = 44;
		rsinterface.children[3] = 16214;
		rsinterface.childX[3] = -52 + 375 + 30;
		rsinterface.childY[3] = 58;
		rsinterface.children[4] = 16215;
		rsinterface.childX[4] = -52 + 375 + 30;
		rsinterface.childY[4] = 73;
		rsinterface.children[5] = 16216;
		rsinterface.childX[5] = -52 + 460 + 60;
		rsinterface.childY[5] = 31;
		rsinterface.children[6] = 16217;
		rsinterface.childX[6] = -52 + 460 + 60;
		rsinterface.childY[6] = 45;
		rsinterface.children[7] = 16218;
		rsinterface.childX[7] = -52 + 460 + 60;
		rsinterface.childY[7] = 59;
		rsinterface.children[8] = 16219;
		rsinterface.childX[8] = -52 + 460 + 60;
		rsinterface.childY[8] = 74;
		rsinterface.children[9] = 16220;
		rsinterface.childX[9] = -52 + 375 + 30;
		rsinterface.childY[9] = 87;
		rsinterface.children[10] = 16221;
		rsinterface.childX[10] = -52 + 460 + 60;
		rsinterface.childY[10] = 88;
	}

	public static void prayerTab() { // changed image
										// locations of
										// protect from summ
										// and rapid renewal
		RSInterface tab = addInterface(5608, InterfaceType.TAB);
		RSInterface currentPray = interfaceCache[687];
		addSprite(5651, 325);
		currentPray.disabledColor = 0xFF981F;
		currentPray.textShadow = true;
		currentPray.message = "%1/%2";

		int[] ID1 = { 18016, 18017, 18018, 18019, 18020, 18021, 18022, 18023, 18024, 18025, 18026, 18027, 18028, 18029,
				18030, 18031, 18032, 18033, 18034, 18035, 18036, 18037, 18038, 18039, 18040, 18041, 18042, 18043, 18044,
				18045 };
		// mouse hover location
		int[] X = { 8, 44, 80, 114, 150, 8, 44, 80, 116, 152, 8, 42, 78, 116, 152, 8, 44, 80, 116, 150, 6, 44, 80, 116,
				150, 6, 44, 80, 116, 150 };
		int[] Y = { 6, 6, 6, 4, 4, 42, 42, 42, 42, 42, 79, 76, 76, 78, 78, 114, 114, 114, 114, 112, 148, 150, 150, 150,
				148, 184, 184, 184, 184, 184 };

		int[] hoverIDs = { 18050, 18052, 18054, 18056, 18058, 18060, 18062, 18064, 18066, 18068, 18070, 18072, 18074,
				18076, 18078, 18080, 18082, 18084, 18086, 18088, 18090, 18092, 18094, 18096, 18098, 18100, 18102, 18104,
				18106, 18108 };
		// box hover location
		int[] hoverX = { 12, 8, 20, 12, 24, 2, 2, 6, 6, 50, 6, 6, 10, 6, 6, 5, 5, 5, 5, 5, 5, 10, 23, 25, 30, 1, 1, 1,
				5, 15, 10 };
		int[] hoverY = { 42, 42, 42, 42, 42, 80, 80, 80, 80, 80, 118, 118, 118, 118, 118, 150, 150, 150, 150, 150, 95,
				80, 65, 65, 95, 100, 100, 115, 115, 115, 100 };
		String[] hoverStrings = { "Level 01\nThick Skin\nIncreases your Defence by 5%",
				"Level 04\nBurst of Strength\nIncreases your Strength by 5%",
				"Level 07\nCharity of Thought\nIncreases your Attack by 5%",
				"Level 08\nSharp Eye\nIncreases your Ranged by 5%", "Level 09\nMystic Will\nIncreases your Magic by 5%",
				"Level 10\nRock Skin\nIncreases your Defence by 10%",
				"Level 13\nSuperhuman Strength\nIncreases your Strength by 10%",
				"Level 16\nImproved Reflexes\nIncreases your Attack by 10%",
				"Level 19\nRapid Restore\n2x restore rate for all stats\nexcept Hitpoints and Prayer",
				"Level 22\nRapid Heal\n2x restore rate for the\nHitpoints stat",
				"Level 25\nProtect Item\nKeep one extra item if you die",
				"Level 26\nHawk Eye\nIncreases your Ranged by 10%",
				"Level 27\nMystic Lore\nIncreases your Magic by 10%",
				"Level 28\nSteel Skin\nIncreases your Defence by 15%",
				"Level 31\nUltimate Strength\nIncreases your Strength by 15%",
				"Level 34\nIncredible Reflexes\nIncreases your Attack by 15%",
				"Level 37\nProtect from Magic\nProtection from magical attacks",
				"Level 40\nProtect from Missiles\nProtection from ranged attacks",
				"Level 43\nProtect from Melee\nProtection from close attacks",
				"Level 44\nEagle Eye\nIncreases your Ranged by 15%",
				"Level 45\nMystic Might\nIncreases your Magic by 15%",
				"Level 46\nRetribution\nInflicts damage to nearby\ntargets if you die",
				"Level 49\nRedemption\nHeals you when damaged\nand Hitpoints falls\nbelow 10%",
				"Level 52\nSmite\n1/4 of damage dealt is\nalso removed from\nopponents Prayer",
				"Level 65\nRapid Renewal\nIncrease your healing rate.",
				"Level 60\nChivalry\nIncreases your Defence by 20%,\nStrength by 18% and Attack by\n15%",
				"Level 70\nPiety\nIncreases your Defence by 25%,\nStrength by 23% and Attack by\n20%",
				"Level 74\nRigour\nIncreases your Ranged by 20%\nand Defence by 25%.",
				"Level 77\nAugury\nIncreases your Magic by 20%\nand Defence by 25%.",
				"Level 35\nProtect from Summoning\nProtect against damage from\nSummoning attacks."
				 };

		int ID2[] = { 5609, 5610, 5611, 5612, 5613, 5614, 5615, 5616, 5617, 5618, 5619, 5620, 5621, 5622, 5623, 683,
				684, 685, 5632, 5633, 5634, 5635, 5636, 5637, 5638, 5639, 5640, 5641, 5642, 5643, 5644, 686, 5645, 5649,
				5647, 5648, 18000, 18001, 18002, 18003, 18004, 18005, 18006, 18007, 18008, 18009, 18010, 18011, 18012,
				18013, 18014, 18015, 5651, 687 };

		String[] oldPrayerNames = { "Thick Skin", "Burst of Strength", "Charity of Thought", "Rock Skin",
				"Superhuman Strength", "Improved Reflexes", "Rapid Restore", "Rapid Heal", "Protect Item", "Steel Skin",
				"Ultimate Strength", "Incredible Reflexes", "Protect from Magic", "Protect from Missiles",
				"Protect from Melee", "Retribution", "Redemption", "Smite" };
		for (int i = 0; i < oldPrayerNames.length; i++) {
			addOldPrayer(ID2[i], oldPrayerNames[i]);
		}
		addPrayer(18000, 0, 601, 7, 310, 309, "Sharp Eye");
		addPrayer(18002, 0, 602, 8, 312, 311, "Mystic Will");
		addPrayer(18004, 0, 603, 25, 314, 313, "Hawk Eye");
		addPrayer(18006, 0, 604, 26, 316, 315, "Mystic Lore");
		addPrayer(18008, 0, 605, 43, 318, 317, "Eagle Eye");
		addPrayer(18010, 0, 606, 44, 320, 319, "Mystic Might");
		addPrayer(18012, 0, 607, 59, 322, 321, "Chivalry");
		addPrayer(18014, 0, 608, 69, 324, 323, "Piety");
		addPrayer(17900, 0, 609, 34, 331, 330, "Protect from Summoning"); // summoning
		addPrayer(17902, 0, 610, 73, 968, 969, "Rigour"); // rigour
		addPrayer(17904, 0, 611, 76, 329, 328, "Augury"); // augury
		addPrayer(17906, 0, 612, 65, 327, 326, "Rapid Recovery"); // rapid
																	// recovery

		for (int i = 0; i < ID1.length; i++) {
			addPrayerHover(ID1[i], hoverIDs[i], i, hoverStrings[i]);
		}

		int spritesAmount = 62;
		tab.totalChildren(spritesAmount + ID1.length + hoverIDs.length);
		/* Buttons/glows */
		tab.child(0, 5609, 6, 4);
		tab.child(1, 5610, 42, 4);
		tab.child(2, 5611, 78, 4);
		tab.child(3, 5612, 6, 40);
		tab.child(4, 5613, 42, 40);
		tab.child(5, 5614, 78, 40);
		tab.child(6, 5615, 114, 40);
		tab.child(7, 5616, 150, 40);
		tab.child(8, 5617, 6, 76);
		tab.child(9, 5618, 114, 76);
		tab.child(10, 5619, 150, 76);
		tab.child(11, 5620, 6, 112);
		tab.child(13, 5622, 78, 112); // 3rd click area
		tab.child(14, 5623, 114, 112); // 4th click area
		/* Sprites */
		tab.child(18, 5632, 8, 6);
		tab.child(19, 5633, 44, 6);
		tab.child(20, 5634, 80, 6);
		tab.child(21, 5635, 8, 42);
		tab.child(22, 5636, 44, 42);
		tab.child(23, 5637, 80, 42);
		tab.child(24, 5638, 116, 42);
		tab.child(25, 5639, 152, 42);
		tab.child(26, 5640, 8, 79);
		tab.child(27, 5641, 116, 78);
		tab.child(28, 5642, 152, 78);// ult str
		tab.child(29, 5643, 8, 114); // incredible reflexes
		tab.child(12, 5621, 42, 112); // 2nd click area 60, 17900
		tab.child(30, 5644, 45, 114); // 2nd image 61, 17901

		tab.child(31, 686, 80, 115); // 3rd image
		tab.child(32, 5645, 115, 115); // 4th image

		tab.child(60, 18008, 150, 112); // 5th click
		tab.child(61, 18009, 152, 117); // 5th image

		tab.child(44, 18010, 6, 148);
		tab.child(45, 18011, 9, 153);

		tab.child(46, 683, 42, 148);
		tab.child(47, 5649, 45, 152);

		tab.child(15, 684, 78, 148);
		tab.child(33, 5647, 80, 150);

		tab.child(16, 685, 114, 148);
		tab.child(34, 5648, 116, 150);

		tab.child(17, 17906, 150, 148);
		tab.child(35, 17907, 152, 153);

		/* New prayers */
		tab.child(36, 18000, 114, 4);
		tab.child(37, 18001, 117, 8);
		tab.child(38, 18002, 150, 4);
		tab.child(39, 18003, 153, 7);
		tab.child(40, 18004, 42, 76);
		tab.child(41, 18005, 45, 80);
		tab.child(42, 18006, 78, 76);
		tab.child(43, 18007, 81, 79);

		tab.child(48, 18012, 6, 184);
		tab.child(49, 18013, 13, 187);

		tab.child(50, 18014, 42, 184); // used to be rapid renewal 17906 // the
										// activated image
		tab.child(51, 18015, 44, 194); // image of the 2nd column bottom row

		/* Prayer ICONS & text */
		tab.child(52, 5651, 65, 239);// icon
		tab.child(53, 687, 85, 239);// text

		tab.child(54, 17902, 78, 184);
		tab.child(55, 17903, 80, 188);

		tab.child(56, 17904, 114, 184);
		tab.child(57, 17905, 117, 188);

		tab.child(58, 17900, 150, 184); // 5th activated pray image of last row
		tab.child(59, 17901, 153, 186); // 5th image of last row

		
		
		
		int frame = spritesAmount;
		int frame2 = 0;
		for (int i : ID1) {
			tab.child(frame, i, X[frame2], Y[frame2]);
			frame++;
			frame2++;
		}

		int frame3 = 0;
		for (int i : hoverIDs) {
			tab.child(frame, i, hoverX[frame3], hoverY[frame3]);
			frame++;
			frame3++;
		}

	}

	// public static void prayerTab(TextDrawingArea[] tda) { // prayers with
	// proper order by level
	// RSInterface tab = addInterface(5608, InterfaceType.TAB);
	// RSInterface currentPray = interfaceCache[687];
	// addSprite(5651, 325);
	// currentPray.disabledColor = 0xFF981F;
	// currentPray.textShadow = true;
	// currentPray.message = "%1/%2";
	//
	// int[] ID1 = {18016, 18017, 18018, 18019, 18020, 18021, 18022, 18023,
	// 18024, 18025, 18026, 18027, 18028, 18029, 18030, 18031, 18032,
	// 18033, 18034, 18035, 18036, 18037, 18038, 18039, 18040, 18041,
	// 18042, 18043, 18044, 18045};
	// //mouse hover location
	// int[] X = {8, 44, 80, 114, 150, 8, 44, 80, 116, 152, 8, 42, 78, 116,
	// 152, 8, 44, 80, 116, 150, 6, 44, 80, 116, 150, 6,
	// 44, 80, 116, 150};
	// int[] Y = {6, 6, 6, 4, 4, 42, 42, 42, 42, 42, 79, 76, 76, 78, 78, 114,
	// 114, 114, 114, 112, 148, 150, 150, 150, 148, 184,
	// 184, 184, 184, 184};
	//
	// int[] hoverIDs = {18050, 18052, 18054, 18056, 18058, 18060, 18062,
	// 18064, 18066, 18068, 18070, 18072, 18074, 18076, 18078, 18080,
	// 18082, 18084, 18086, 18088, 18090, 18092, 18094, 18096, 18098,
	// 18100,
	// 18102, 18104, 18106, 18108};
	// //box hover location
	// int[] hoverX = {12, 8, 20, 12, 24, 2, 2, 6, 6, 50, 6, 6, 10, 6, 6, 5,
	// 5, 5, 5, 5, 5, 10, 23, 25, 30, 1, 17,
	// 1, 4, 10, 10
	// };
	// int[] hoverY = {42, 42, 42, 42, 42, 80, 80, 80, 80, 80, 118, 118, 118,
	// 118, 118, 150, 150, 150, 150, 150, 95, 95, 80, 65, 65, 100, 130,
	// 100, 115, 115, 100
	// };
	// String[] hoverStrings = {
	// "Level 01\nThick Skin\nIncreases your Defence by 5%",
	// "Level 04\nBurst of Strength\nIncreases your Strength by 5%",
	// "Level 07\nCharity of Thought\nIncreases your Attack by 5%",
	// "Level 08\nSharp Eye\nIncreases your Ranged by 5%",
	// "Level 09\nMystic Will\nIncreases your Magic by 5%",
	// "Level 10\nRock Skin\nIncreases your Defence by 10%",
	// "Level 13\nSuperhuman Strength\nIncreases your Strength by 10%",
	// "Level 16\nImproved Reflexes\nIncreases your Attack by 10%",
	// "Level 19\nRapid Restore\n2x restore rate for all stats\nexcept Hitpoints
	// and Prayer",
	// "Level 22\nRapid Heal\n2x restore rate for the\nHitpoints stat",
	// "Level 25\nProtect Item\nKeep one extra item if you die",
	// "Level 26\nHawk Eye\nIncreases your Ranged by 10%",
	// "Level 27\nMystic Lore\nIncreases your Magic by 10%",
	// "Level 28\nSteel Skin\nIncreases your Defence by 15%",
	// "Level 31\nUltimate Strength\nIncreases your Strength by 15%",
	// "Level 34\nIncredible Reflexes\nIncreases your Attack by 15%",
	// "Level 35\nProtect from Summoning\nProtect against damage from\nSummoning
	// attacks.",
	// "Level 37\nProtect from Magic\nProtection from magical attacks",
	// "Level 40\nProtect from Missiles\nProtection from ranged attacks",
	// "Level 43\nProtect from Melee\nProtection from close attacks",
	// "Level 44\nEagle Eye\nIncreases your Ranged by 15%",
	// "Level 45\nMystic Might\nIncreases your Magic by 15%",
	// "Level 46\nRetribution\nInflicts damage to nearby\ntargets if you die",
	// "Level 49\nRedemption\nHeals you when damaged\nand Hitpoints falls\nbelow
	// 10%",
	// "Level 52\nSmite\n1/4 of damage dealt is\nalso removed from\nopponents
	// Prayer",
	// "Level 60\nChivalry\nIncreases your Defence by 20%,\nStrength by 18% and
	// Attack by\n15%",
	// "Level 65\nRapid Renewal\nIncrease your healing rate.",
	// "Level 70\nPiety\nIncreases your Defence by 25%,\nStrength by 23% and
	// Attack by\n20%",
	// "Level 74\nRigour\nIncreases your Ranged by 20%\nand Defence by 20%.",
	// "Level 77\nAugury\nIncreases your Magic by 20%\nand Defence by 20%."
	// };
	//
	// int ID2[] = {5609, 5610, 5611, 5612, 5613, 5614, 5615, 5616, 5617,
	// 5618, 5619, 5620, 5621, 5622, 5623, 683, 684, 685, 5632, 5633,
	// 5634, 5635, 5636, 5637, 5638, 5639, 5640, 5641, 5642, 5643,
	// 5644, 686, 5645, 5649, 5647, 5648, 18000, 18001, 18002, 18003,
	// 18004, 18005, 18006, 18007, 18008, 18009, 18010, 18011, 18012,
	// 18013, 18014, 18015, 5651, 687};
	//
	// String[] oldPrayerNames = {"Thick Skin", "Burst of Strength",
	// "Charity of Thought", "Rock Skin", "Superhuman Strength",
	// "Improved Reflexes", "Rapid Restore", "Rapid Heal",
	// "Protect Item", "Steel Skin", "Ultimate Strength",
	// "Incredible Reflexes", "Protect from Magic",
	// "Protect from Missiles", "Protect from Melee", "Retribution",
	// "Redemption", "Smite"};
	// for (int i = 0; i < oldPrayerNames.length; i++) {
	// addOldPrayer(ID2[i], oldPrayerNames[i]);
	// }
	// addPrayer(18000, 0, 601, 7, 310, 309, "Sharp Eye");
	// addPrayer(18002, 0, 602, 8, 312, 311, "Mystic Will");
	// addPrayer(18004, 0, 603, 25, 314, 313, "Hawk Eye");
	// addPrayer(18006, 0, 604, 26, 316, 315, "Mystic Lore");
	// addPrayer(18008, 0, 605, 43, 318, 317, "Eagle Eye");
	// addPrayer(18010, 0, 606, 44, 320, 319, "Mystic Might");
	// addPrayer(18012, 0, 607, 59, 322, 321, "Chivalry");
	// addPrayer(18014, 0, 608, 69, 324, 323, "Piety");
	// addPrayer(17900, 0, 609, 34, 331, 330, "Protect from Summoning"); //
	// summoning
	// addPrayer(17902, 0, 610, 76, 968, 969, "Rigour"); // rigour
	// addPrayer(17904, 0, 611, 79, 329, 328, "Augury"); // augury
	// addPrayer(17906, 0, 612, 79, 327, 326, "Rapid Recovery"); // rapid
	// recovery
	//
	// //System.out.println(ID2.length + ", " + X2.length + ", " + Y2.length);
	// //System.out.println(ID1.length + ", " + X.length + ", " + Y.length + ",
	// " + hoverIDs.length + ", " + hoverStrings.length);
	//
	//
	// for (int i = 0; i < ID1.length; i++) {
	// addPrayerHover(ID1[i], hoverIDs[i], i, hoverStrings[i]);
	// }
	//
	//
	// int spritesAmount = 62;
	// tab.totalChildren(spritesAmount + ID1.length + hoverIDs.length);
	// /* Buttons/glows */
	// tab.child(0, 5609, 6, 4);
	// tab.child(1, 5610, 42, 4);
	// tab.child(2, 5611, 78, 4);
	// tab.child(3, 5612, 6, 40);
	// tab.child(4, 5613, 42, 40);
	// tab.child(5, 5614, 78, 40);
	// tab.child(6, 5615, 114, 40);
	// tab.child(7, 5616, 150, 40);
	// tab.child(8, 5617, 6, 76);
	// tab.child(9, 5618, 114, 76);
	// tab.child(10, 5619, 150, 76);
	// tab.child(11, 5620, 6, 112);
	// tab.child(12, 5621, 78, 112);
	// tab.child(13, 5622, 114, 112);
	// /* Sprites */
	// tab.child(18, 5632, 8, 6);
	// tab.child(19, 5633, 44, 6);
	// tab.child(20, 5634, 80, 6);
	// tab.child(21, 5635, 8, 42);
	// tab.child(22, 5636, 44, 42);
	// tab.child(23, 5637, 80, 42);
	// tab.child(24, 5638, 116, 42);
	// tab.child(25, 5639, 152, 42);
	// tab.child(26, 5640, 8, 79);
	// tab.child(27, 5641, 116, 78);
	// tab.child(28, 5642, 152, 78);
	// tab.child(29, 5643, 8, 114);
	// tab.child(60, 17900, 42, 112);
	// tab.child(61, 17901, 45, 114);
	//
	// tab.child(30, 5644, 80, 115);
	//
	// tab.child(31, 686, 115, 115);
	//
	// tab.child(14, 5623, 150, 112);
	// tab.child(32, 5645, 152, 114);
	//
	// tab.child(44, 18008, 6, 148);
	// tab.child(45, 18009, 9, 153);
	//
	// tab.child(46, 18010, 42, 148);
	// tab.child(47, 18011, 45, 152);
	//
	// tab.child(15, 683, 78, 148);
	// tab.child(33, 5649, 80, 150);
	//
	// tab.child(16, 684, 114, 148);
	// tab.child(34, 5647, 116, 150);
	//
	// tab.child(17, 685, 150, 148);
	// tab.child(35, 5648, 152, 150);
	//
	//
	// /* Prayer ICONS & text */
	// tab.child(52, 5651, 65, 239);// icon
	// tab.child(53, 687, 85, 239);// text
	//
	// /* New prayers */
	// tab.child(36, 18000, 114, 4);
	// tab.child(37, 18001, 117, 8);
	// tab.child(38, 18002, 150, 4);
	// tab.child(39, 18003, 153, 7);
	// tab.child(40, 18004, 42, 76);
	// tab.child(41, 18005, 45, 80);
	// tab.child(42, 18006, 78, 76);
	// tab.child(43, 18007, 81, 79);
	//
	// tab.child(48, 18012, 6, 184);
	// tab.child(49, 18013, 13, 187);
	//
	// tab.child(50, 17906, 42, 184);
	// tab.child(51, 17907, 45, 189);
	//
	// tab.child(54, 18014, 78, 184);
	// tab.child(55, 18015, 80, 194);
	//
	// tab.child(56, 17902, 114, 184);
	// tab.child(57, 17903, 117, 188);
	//
	// tab.child(58, 17904, 150, 184);
	// tab.child(59, 17905, 153, 187);
	//
	// int frame = spritesAmount;
	// int frame2 = 0;
	// for (int i : ID1) {
	// tab.child(frame, i, X[frame2], Y[frame2]);
	// frame++;
	// frame2++;
	// }
	//
	// int frame3 = 0;
	// for (int i : hoverIDs) {
	// tab.child(frame, i, hoverX[frame3], hoverY[frame3]);
	// frame++;
	// frame3++;
	// }
	//
	// }

	public static void Curses() {
		RSInterface Interface = addInterface(22500, InterfaceType.TAB);
		int index = 0;
		addText(687, "99/99", 0xFF981F, false, false, -1, 1);
		addSprite(22502, 325/* 0, "Interfaces/CurseTab/ICON" */);
		addPrayer(22503, 0, 83, 49, 7, "Protect Item", 22582);
		addPrayer(22505, 0, 84, 49, 4, "Sap Warrior", 22544);
		addPrayer(22507, 0, 85, 51, 5, "Sap Ranger", 22546);
		addPrayer(22509, 0, 101, 53, 3, "Sap Mage", 22548);
		addPrayer(22511, 0, 102, 55, 2, "Sap Spirit", 22550);
		addPrayer(22513, 0, 86, 58, 18, "Berserker", 22552);
		addPrayer(22515, 0, 87, 61, 15, "Deflect Summoning", 22554);
		addPrayer(22517, 0, 88, 64, 17, "Deflect Magic", 22556);
		addPrayer(22519, 0, 89, 67, 16, "Deflect Missiles", 22558);
		addPrayer(22521, 0, 90, 70, 6, "Deflect Melee", 22560);
		addPrayer(22523, 0, 91, 73, 9, "Leech Attack", 22562);
		addPrayer(22525, 0, 103, 75, 10, "Leech Ranged", 22564);
		addPrayer(22527, 0, 104, 77, 11, "Leech Magic", 22566);
		addPrayer(22529, 0, 92, 79, 12, "Leech Defence", 22568);
		addPrayer(22531, 0, 93, 81, 13, "Leech Strength", 22570);
		addPrayer(22533, 0, 94, 83, 14, "Leech Energy", 22572);
		addPrayer(22535, 0, 95, 85, 19, "Leech Special Attack", 22574);
		addPrayer(22537, 0, 96, 88, 1, "Wrath", 22576);
		addPrayer(22539, 0, 97, 91, 8, "Soul Split", 22578);
		addPrayer(22541, 0, 105, 94, 20, "Turmoil", 22580);
		addPrayer1(22584, 0, 106, 94, 1307, 1306, "Anguish", 22586);
		addPrayer1(22588, 0, 107, 94, 1309, 1308, "Torment", 22590);
		//addPrayer(int i, int configId, int configFrame, int requiredValues, int prayerSpriteID, String PrayerName, int Hover
		drawTooltip(22582, "Level 50\nProtect Item\nKeep 1 extra item if you die.");
		drawTooltip(22544,
				"Level 50\nSap Warrior\nDrains 10% of enemy Attack,\nStrength and Defence,\nincreasing to 20% over time.");
		drawTooltip(22546,
				"Level 52\nSap Ranger\nDrains 10% of enemy Ranged\nand Defence, increasing to 20%\nover time.");
		drawTooltip(22548, "Level 54\nSap Mage\nDrains 10% of enemy Magic\nand Defence, increasing to 20%\nover time.");
		drawTooltip(22550, "Level 56\nSap Spirit\nDrains enemy special attack\nenergy.");
		drawTooltip(22552, "Level 59\nBerserker\nBoosted stats last 15% longer.");
		drawTooltip(22554,
				"Level 62\nDeflect Summoning\nReduces damage dealt from\nSummoning scrolls, prevents the\nuse of a familiar's special\nattack, and can deflect some of\ndamage back to the attacker.");
		drawTooltip(22556,
				"Level 65\nDeflect Magic\nProtects against magical attacks\nand can deflect some of the\ndamage back to the attacker.");
		drawTooltip(22558,
				"Level 68\nDeflect Missiles\nProtects against ranged attacks\nand can deflect some of the\ndamage back to the attacker.");
		drawTooltip(22560,
				"Level 71\nDeflect Melee\nProtects against melee attacks\nand can deflect some of the\ndamage back to the attacker.");
		drawTooltip(22562,
				"Level 74\nLeech Attack\nBoosts Attack by 5%, increasing\nto 10% over time, while draining\nenemy Attack by 10%, increasing\nto 25% over time.");
		drawTooltip(22564,
				"Level 76\nLeech Ranged\nBoosts Ranged by 5%, increasing\nto 10% over time, while draining\nenemy Ranged by 10%,\nincreasing to 25% over time.");
		drawTooltip(22566,
				"Level 78\nLeech Magic\nBoosts Magic by 5%, increasing\nto 10% over time, while draining\nenemy Magic by 10%, increasing\nto 25% over time.");
		drawTooltip(22568,
				"Level 80\nLeech Defence\nBoosts Defence by 5%, increasing\nto 10% over time, while draining\nenemy Defence by10%,\nincreasing to 25% over time.");
		drawTooltip(22570,
				"Level 82\nLeech Strength\nBoosts Strength by 5%, increasing\nto 10% over time, while draining\nenemy Strength by 10%, increasing\nto 25% over time.");
		drawTooltip(22572, "Level 84\nLeech Energy\nDrains enemy run energy, while\nincreasing your own.");
		drawTooltip(22574,
				"Level 86\nLeech Special Attack\nDrains enemy special attack\nenergy, while increasing your\nown.");
		drawTooltip(22576, "Level 89\nWrath\nInflicts damage to nearby\ntargets if you die.");
		drawTooltip(22578,
				"Level 92\nSoul Split\nYou gain 1 life point for every 5\ndamage you do to your\nopponent, and they lose 1\nprayer point for every 5\ndamage.");
		drawTooltip(22580,
				"Level 95\nTurmoil\nIncreases Attack and Defence\nby 15%, plus 15% of enemy's\nlevel, and Strength by 23% plus\n10% of enemy's level.");
		drawTooltip(22586,
				"Level 95\nAnguish\nIncreases Defence by 15%,\nplus 15% of enemy's level,\nand Ranged by 23% plus 10%\nof enemy's level.");
		drawTooltip(22590,
				"Level 95\nTorment\nIncreases Defence by 15%,\nplus 15% of enemy's level,\nand Magic by 23% plus 10%\nof enemy's level.");
		
		Interface.totalChildren(68);

		Interface.child(index, 687, 85, 241);
		index++;
		Interface.child(index, 22502, 65, 241);
		index++;
		Interface.child(index, 22503, 2, 5);
		index++;
		Interface.child(index, 22504, 8, 8);
		index++;
		Interface.child(index, 22505, 40, 5);
		index++;
		Interface.child(index, 22506, 47, 12);
		index++;
		Interface.child(index, 22507, 76, 5);
		index++;
		Interface.child(index, 22508, 82, 11);
		index++;
		Interface.child(index, 22509, 113, 5);
		index++;
		Interface.child(index, 22510, 116, 8);
		index++;
		Interface.child(index, 22511, 150, 5);
		index++;
		Interface.child(index, 22512, 155, 10);
		index++;
		Interface.child(index, 22513, 2, 45);
		index++;
		Interface.child(index, 22514, 9, 48);
		index++;
		Interface.child(index, 22515, 39, 45);
		index++;
		Interface.child(index, 22516, 42, 47);
		index++;
		Interface.child(index, 22517, 76, 45);
		index++;
		Interface.child(index, 22518, 79, 48);
		index++;
		Interface.child(index, 22519, 113, 45);
		index++;
		Interface.child(index, 22520, 116, 48);
		index++;
		Interface.child(index, 22521, 151, 45);
		index++;
		Interface.child(index, 22522, 154, 48);
		index++;
		Interface.child(index, 22523, 2, 82);
		index++;
		Interface.child(index, 22524, 6, 86);
		index++;
		Interface.child(index, 22525, 40, 82);
		index++;
		Interface.child(index, 22526, 42, 86);
		index++;
		Interface.child(index, 22527, 77, 82);
		index++;
		Interface.child(index, 22528, 79, 86);
		index++;
		Interface.child(index, 22529, 114, 83);
		index++;
		Interface.child(index, 22530, 119, 87);
		index++;
		Interface.child(index, 22531, 153, 83);
		index++;
		Interface.child(index, 22532, 156, 86);
		index++;
		Interface.child(index, 22533, 2, 120);
		index++;
		Interface.child(index, 22534, 7, 125);
		index++;
		Interface.child(index, 22535, 40, 120);
		index++;
		Interface.child(index, 22536, 45, 124);
		index++;
		Interface.child(index, 22537, 78, 120);
		index++;
		Interface.child(index, 22538, 86, 124);
		index++;
		Interface.child(index, 22539, 114, 120);
		index++;
		Interface.child(index, 22540, 120, 125);
		index++;
		Interface.child(index, 22541, 151, 120);
		index++;
		Interface.child(index, 22542, 153, 127);
		index++;
		Interface.child(index, 22584, 2, 158);
		index++;
		Interface.child(index, 22585, 6, 163);
		index++;
		Interface.child(index, 22588, 40, 158);
		index++;
		Interface.child(index, 22589, 42, 161);
		index++;
		Interface.child(index, 22582, 10, 40);
		index++;
		Interface.child(index, 22544, 20, 40);
		index++;
		Interface.child(index, 22546, 20, 40);
		index++;
		Interface.child(index, 22548, 20, 40);
		index++;
		Interface.child(index, 22550, 20, 40);
		index++;
		Interface.child(index, 22552, 10, 80);
		index++;
		Interface.child(index, 22554, 10, 80);
		index++;
		Interface.child(index, 22556, 10, 80);
		index++;
		Interface.child(index, 22558, 10, 80);
		index++;
		Interface.child(index, 22560, 10, 80);
		index++;
		Interface.child(index, 22562, 10, 120);
		index++;
		Interface.child(index, 22564, 10, 120);
		index++;
		Interface.child(index, 22566, 10, 120);
		index++;
		Interface.child(index, 22568, 5, 120);
		index++;
		Interface.child(index, 22570, 5, 120);
		index++;
		Interface.child(index, 22572, 10, 160);
		index++;
		Interface.child(index, 22574, 10, 160);
		index++;
		Interface.child(index, 22576, 10, 160);
		index++;
		Interface.child(index, 22578, 10, 160);
		index++;
		Interface.child(index, 22580, 10, 160);
		index++;
		Interface.child(index, 22586, 10, 200);
		index++;
		Interface.child(index, 22590, 10, 200);
		index++;
	}

	public static void itemsOnDeath() {
		RSInterface mainRsi = addInterface(17100);
		addSprite(17101, 302);
		addHover(17102, 3, 0, 10601, 300, "Close Window");
		addHovered(10601, 301, 10602);
		addText(17103, "Items kept on death", 2, 0xff981f);
		addText(17104, "Items I will keep...", 1, 0xff981f);
		addText(17105, "Items I will lose...", 1, 0xff981f);
		addText(17106, "Items Lost to Grim Reaper", 1, 0xff981f);
		addText(17107, Config.SERVER_NAME, 1, 0xffcc33);
		addText(17108, "", 1, 0xffcc33);
		mainRsi.scrollMax = 0;
		mainRsi.isMouseoverTriggered = false;
		mainRsi.totalChildren(13);
		mainRsi.setFlag(TILING_FLAG_BIT);

		mainRsi.children[0] = 17101;
		mainRsi.childX[0] = 7;
		mainRsi.childY[0] = 8;
		mainRsi.children[1] = 17102;
		mainRsi.childX[1] = 480;
		mainRsi.childY[1] = 17;
		mainRsi.children[2] = 17103;
		mainRsi.childX[2] = 185;
		mainRsi.childY[2] = 18;
		mainRsi.children[3] = 17104;
		mainRsi.childX[3] = 22;
		mainRsi.childY[3] = 50;
		mainRsi.children[4] = 17105;
		mainRsi.childX[4] = 22;
		mainRsi.childY[4] = 110;
		mainRsi.children[5] = 17106;
		mainRsi.childX[5] = 347;
		mainRsi.childY[5] = 47;
		mainRsi.children[6] = 17107;
		mainRsi.childX[6] = 349;
		mainRsi.childY[6] = 270;
		mainRsi.children[7] = 17108;
		mainRsi.childX[7] = 398;
		mainRsi.childY[7] = 298;
		mainRsi.children[8] = 17115;
		mainRsi.childX[8] = 348;
		mainRsi.childY[8] = 200;
		mainRsi.children[9] = 10494;
		mainRsi.childX[9] = 26;
		mainRsi.childY[9] = 74;
		mainRsi.children[10] = 10600;
		mainRsi.childX[10] = 26;
		mainRsi.childY[10] = 133;
		mainRsi.children[11] = 10601;
		mainRsi.childX[11] = 480;
		mainRsi.childY[11] = 17;
		mainRsi.children[12] = 15289;
		mainRsi.childX[12] = 345;
		mainRsi.childY[12] = 70;
		
		RSInterface reaper = addInterface(15289, InterfaceType.MAIN);
		reaper.totalChildren(1);
		reaper.child(0, 15290, 0, 0);
		reaper.height = 150;
		reaper.width = 130;
		reaper.scrollMax = 320;
		
		RSInterface rItems = addInterface(15290, InterfaceType.MAIN);
		addToItemGroup(rItems, 4, 10, 0, 0, new String[] { "Examine" });

		RSInterface dataRsi = addInterface(17115);
		addText(17109, "a", 0, 0xff981f);
		addText(17110, "b", 0, 0xff981f);
		addText(17111, "c", 0, 0xff981f);
		addText(17112, "d", 0, 0xff981f);
		addText(17113, "e", 0, 0xff981f);
		addText(17114, "f", 0, 0xff981f);
		addText(17117, "g", 0, 0xff981f);
		addText(17118, "h", 0, 0xff981f);
		addText(17119, "i", 0, 0xff981f);
		addText(17120, "j", 0, 0xff981f);
		addText(17121, "k", 0, 0xff981f);
		addText(17122, "l", 0, 0xff981f);
		addText(17123, "m", 0, 0xff981f);
		addText(17124, "n", 0, 0xff981f);
		addText(17125, "o", 0, 0xff981f);
		addText(17126, "p", 0, 0xff981f);
		addText(17127, "q", 0, 0xff981f);
		addText(17128, "r", 0, 0xff981f);
		addText(17129, "s", 0, 0xff981f);
		addText(17130, "t", 0, 0xff981f);
		dataRsi.parentID = 17115;
		dataRsi.id = 17115;
		dataRsi.interfaceType = 0;
		dataRsi.atActionType = 0;
		dataRsi.contentType = 0;
		dataRsi.width = 130;
		dataRsi.height = 80;
		dataRsi.hoverType = -1;
		dataRsi.scrollMax = 50;
		dataRsi.totalChildren(20);
		dataRsi.children[0] = 17109;
		dataRsi.childX[0] = 0;
		dataRsi.childY[0] = 0;
		dataRsi.children[1] = 17110;
		dataRsi.childX[1] = 0;
		dataRsi.childY[1] = 12;
		dataRsi.children[2] = 17111;
		dataRsi.childX[2] = 0;
		dataRsi.childY[2] = 24;
		dataRsi.children[3] = 17112;
		dataRsi.childX[3] = 0;
		dataRsi.childY[3] = 36;
		dataRsi.children[4] = 17113;
		dataRsi.childX[4] = 0;
		dataRsi.childY[4] = 48;
		dataRsi.children[5] = 17114;
		dataRsi.childX[5] = 0;
		dataRsi.childY[5] = 60;
		dataRsi.children[6] = 17117;
		dataRsi.childX[6] = 0;
		dataRsi.childY[6] = 72;
		dataRsi.children[7] = 17118;
		dataRsi.childX[7] = 0;
		dataRsi.childY[7] = 84;
		dataRsi.children[8] = 17119;
		dataRsi.childX[8] = 0;
		dataRsi.childY[8] = 96;
		dataRsi.children[9] = 17120;
		dataRsi.childX[9] = 0;
		dataRsi.childY[9] = 108;
		dataRsi.children[10] = 17121;
		dataRsi.childX[10] = 0;
		dataRsi.childY[10] = 120;
		dataRsi.children[11] = 17122;
		dataRsi.childX[11] = 0;
		dataRsi.childY[11] = 132;
		dataRsi.children[12] = 17123;
		dataRsi.childX[12] = 0;
		dataRsi.childY[12] = 144;
		dataRsi.children[13] = 17124;
		dataRsi.childX[13] = 0;
		dataRsi.childY[13] = 156;
		dataRsi.children[14] = 17125;
		dataRsi.childX[14] = 0;
		dataRsi.childY[14] = 168;
		dataRsi.children[15] = 17126;
		dataRsi.childX[15] = 0;
		dataRsi.childY[15] = 180;
		dataRsi.children[16] = 17127;
		dataRsi.childX[16] = 0;
		dataRsi.childY[16] = 192;
		dataRsi.children[17] = 17128;
		dataRsi.childX[17] = 0;
		dataRsi.childY[17] = 204;
		dataRsi.children[18] = 17129;
		dataRsi.childX[18] = 0;
		dataRsi.childY[18] = 216;
		dataRsi.children[19] = 17130;
		dataRsi.childX[19] = 0;
		dataRsi.childY[19] = 228;
	}

	public static int getFreeInterfaceId() {
		for (int index = 0; index < RSInterface.interfaceCache.length; index++) {
			if (RSInterface.interfaceCache[index] == null) {
				return index;
			}
		}

		return -1;
	}

	public static void clanChatSetup() {
		RSInterface rsi = addInterface(18300, InterfaceType.MAIN);
		rsi.totalChildren(12 + 15 + 3);
		int count = 0;
		/* Background */
		addSprite(18301, 289);
		rsi.child(count++, 18301, 14, 18);
		/* Close button */
		addButton(18302, 290, 291, "Close");
		interfaceCache[18302].atActionType = 3;
		rsi.child(count++, 18302, 475, 26);
		/* Clan Setup title */
		addText(18303, "Clan Setup", 2, 0xFF981F, true, true);
		rsi.child(count++, 18303, 256, 26);
		/* Setup buttons */
		String[] titles = { "Clan name:", "Who can enter chat?", "Who can talk on chat?", "Who can kick on chat?",
				"Who can ban on chat?", "Who can share loot?" };
		String[] defaults = { "Chat Disabled", "Anyone", "Anyone", "Anyone", "Anyone", "Anyone" };
		String[] whoCan = { "Anyone", "Recruit", "Corporal", "Sergeant", "Lieutenant", "Captain", "General",
				"Only Me" };

		for (int index = 0, id = 18304, y = 50; index < titles.length; index++, id += 3, y += 40) {
			if (titles[index].equals("Who can share loot?")) {

				id = 18580;
				addSprite(id, 292, 293, 150, 35);
				interfaceCache[id].atActionType = 0;
				interfaceCache[id].actions = whoCan;
				addText(id + 1, titles[index], 0, 0xFF981F, true, true);
				addText(id + 2, defaults[index], 1, 0xFFFFFF, true, true);
				rsi.child(count++, id, 25, y);
				rsi.child(count++, id + 1, 100, y + 4);
				rsi.child(count++, id + 2, 100, y + 17);
				continue;
			}
			addSprite(id, 292, 293, 150, 35);
			interfaceCache[id].atActionType = 0;
			if (index > 0) {
				interfaceCache[id].actions = whoCan;
			} else {
				interfaceCache[id].actions = new String[] { "Change title", "Delete clan" };
			}
			addText(id + 1, titles[index], 0, 0xFF981F, true, true);
			addText(id + 2, defaults[index], 1, 0xFFFFFF, true, true);
			rsi.child(count++, id, 25, y);
			rsi.child(count++, id + 1, 100, y + 4);
			rsi.child(count++, id + 2, 100, y + 17);
		}
		/* Table */
		addSprite(18319, 294);
		rsi.child(count++, 18319, 197, 70);
		/* Labels */
		int id = 18320;
		int y = 74;
		addText(id, "Ranked Members", 2, 0xFF981F, false, true);
		rsi.child(count++, id++, 202, y);
		addText(id, "Banned Members", 2, 0xFF981F, false, true);
		rsi.child(count++, id++, 339, y);
		/* Ranked members list */
		RSInterface list = addInterface(id++);
		list.contentType = 1359;
		int lines = 100;
		list.totalChildren(lines);
		String[] ranks = { "Demote", "Recruit", "Corporal", "Sergeant", "Lieutenant", "Captain", "General", "Owner" };
		list.childY[0] = 2;
		// System.out.println(id);
		int height = Client.instance.chooseFont(1).baseCharacterHeight;
		for (int index = id; index < id + lines; index++) {
			// addText(index, "", tda, 1, 0xffffff, false, true);
			addClickableText(index, "", ranks, 0xffffff, false, true, 1, 70, height);
			interfaceCache[index].actions = ranks;
			list.children[index - id] = index;
			list.childX[index - id] = 2;
			list.childY[index - id] = (short) (index - id > 0 ? list.childY[index - id - 1] + 14 : 0);
		}
		id += lines;
		list.width = 119;
		list.height = 210;
		list.scrollMax = (lines * 14) + 2;
		rsi.child(count++, list.id, 199, 92);
		/* Banned members list */
		list = addInterface(id++);
		list.contentType = 1359;
		list.totalChildren(lines);
		list.childY[0] = 2;
		// System.out.println(id);
		for (int index = id; index < id + lines; index++) {
			// addText(index, "", tda, 1, 0xffffff, false, true);
			String[] kksgsd = { "Unban" };
			addClickableText(index, "", kksgsd, 0xffffff, false, true, 1, 70, height);
			interfaceCache[index].actions = kksgsd;
			list.children[index - id] = index;
			list.childY[index - id] = (short) (index - id > 0 ? list.childY[index - id - 1] + 14 : 0);
		}
		id += lines;
		list.width = 119;
		list.height = 210;
		list.scrollMax = (lines * 14) + 2;
		rsi.child(count++, list.id, 339, 92);
		/* Table info text */
		y = 47;
		addText(id, "You can manage both ranked and banned members here.", 0, 0xFF981F, true, true);
		rsi.child(count++, id++, 337, y);
		addText(id, "Right click on a name to edit the member.", 0, 0xFF981F, true, true);
		rsi.child(count++, id++, 337, y + 11);
		/* Add ranked member button */
		y = 75;
		addButton(id, 295, "Add ranked member");
		interfaceCache[id].atActionType = 5;
		rsi.child(count++, id++, 319, y);
		/* Add banned member button */
		addButton(id, 295, "Add banned member");
		interfaceCache[id].atActionType = 5;
		rsi.child(count++, id++, 459, y);
	}

	public static void clanChatTab() {
		RSInterface tab = addInterface(18128, InterfaceType.TAB);
		addHoverButton(18129, 282, "Join Clan", 550, 18130, 5);
		addHoveredButton(18130, 283, 18131);
		addHoverButton(18132, 282, "Clan Setup", -1, 18133, 5);
		addHoveredButton(18133, 283, 18134);
		addText(18135, "Join Clan", 0, 0xff9b00, true, true);
		addText(18136, "Clan Setup", 0, 0xff9b00, true, true);
		addSprite(18137, 284);
		addText(18138, "Clan Chat", 1, 0xff9b00, true, true);
		addText(18139, "Talking in: Not in clan", 0, 0xff9b00, false, true);
		addText(18140, "Owner: None", 0, 0xff9b00, false, true);
		// addSprite(16126, 285);
		AddInterfaceButtons(18254, 288, "Toggle LS/CS", 18255, 1);
		addText(18250, "Off", 0, 0xff9b00, false, true);
		tab.totalChildren(15);
		tab.child(0, 16126, 0, 221);
		tab.child(1, 16126, 0, 59);
		tab.child(2, 18137, 0, 62);
		tab.child(3, 18143, 0, 62);
		tab.child(4, 18129, 15, 226);
		tab.child(5, 18130, 15, 226);
		tab.child(6, 18132, 103, 226);
		tab.child(7, 18133, 103, 226);
		tab.child(8, 18135, 51, 237);
		tab.child(9, 18136, 139, 237);
		tab.child(10, 18138, 95, 1);
		tab.child(11, 18139, 10, 23);
		tab.child(12, 18140, 25, 38);
		tab.child(13, 18254, 148, 24);
		tab.child(14, 18250, 150, 46);

		/* Text area */
		RSInterface list = addInterface(18143, InterfaceType.TAB);
		list.contentType = 1358;
		list.totalChildren(100);
		String[] opti0nzzz = { "Edit Rank", "Kick", "Ban" };
		int height = Client.instance.chooseFont(0).baseCharacterHeight;
		for (int i = 18144; i <= 18244; i++) {
			addClickableText(i, "", opti0nzzz, 0xffffff, false, true, 0, 70, height);
		}
		for (int id = 18144, i = 0; id <= 18243 && i <= 99; id++, i++) {
			list.children[i] = id;
			list.childX[i] = 5;
			for (int id2 = 18144, i2 = 1; id2 <= 18243 && i2 <= 99; id2++, i2++) {
				list.childY[0] = 2;
				list.childY[i2] = (short) (list.childY[i2 - 1] + 14);
			}
		}
		list.height = 158;
		list.width = 174;
		list.scrollMax = 1405;
	}

	public static void bank() {
		RSInterface Tab = addInterface(23000);
		Tab.totalChildren(1);
		Tab.children[0] = 5292;
		Tab.childX[0] = 0;
		Tab.childY[0] = 0;
		RSInterface rsi = addInterface(5292);
		rsi.totalChildren(40);
		addSprite(5293, 262);
		rsi.child(0, 5293, 13, 13);
		addHover(5384, 3, 0, 5380, 263, "Close Window");
		addHovered(5380, 264, 5379);
		rsi.child(3, 5384, 476, 16);
		rsi.child(4, 5380, 476, 16);
		addHover(5294, 4, 0, 5295, 265, "Set A Bank PIN");
		addHovered(5295, 266, 5296);
		rsi.child(5, 5294, 110, 285);
		rsi.child(6, 5295, 110, 285);
		addBankHover(22000, 4, 22001, 269, 303, 35, 25, 304, 1, "Swap Withdraw Mode", 22002, 270, 308, "Bank/BANK",
				22003, "Switch to insert items \nmode", "Switch to swap items \nmode.", 12, 20);
		rsi.child(7, 22000, 25, 285);
		rsi.child(8, 22001, 10, 225);
		// old search toggle button addBankHover(22004, 4, 22005, 271, 306, 35,
		// 25, 0, 1, "Search", 22006, 272, 307, "Bank/BANK", 22007, "Click here
		// to search your \nbank", "Click here to search your \nbank", 12, 20);
		addBankHover1(22004, 5, 22005, 271, 35, 25, "Search", 22006, 272, "Bank/BANK", 22007,
				"Click here to search your \nbank", 12, 20);
		rsi.child(9, 22004, 65, 285);
		rsi.child(10, 22005, 50, 225);
		addBankHover(22008, 4, 22009, 273, 304, 35, 25, 115, 1, "Withdraw", 22010, 274, 305, "Bank/BANK", 22011,
				"Switch to note withdrawal \nmode", "Switch to item withdrawal \nmode", 12, 20);
		rsi.child(11, 22008, 240, 285);
		rsi.child(12, 22009, 225, 225);
		addBankHover1(22012, 5, 22013, 275, 35, 25, "Deposit carried items.", 22014, 276, "Bank/BANK", 22015,
				"Empty your backpack into\nyour bank", 0, 20);
		rsi.child(13, 22012, 375, 285);
		rsi.child(14, 22013, 360, 225);
		addBankHover1(22016, 5, 22017, 277, 35, 25, "Deposit worn items.", 22018, 278, "Bank/BANK", 22019,
				"Empty the items your are\nwearing into your bank", 0, 20);
		rsi.child(15, 22016, 415, 285);
		rsi.child(16, 22017, 400, 225);
		addBankHover1(22020, 5, 22021, 279, 35, 25, "Deposit money in Vault.", 22022, 280, "Bank/BANK", 22023,
				"Empty your BoB's inventory\ninto your bank", 0, 20);
		rsi.child(17, 22020, 455, 285);
		rsi.child(18, 22021, 440, 225);
		addBankHover(22043, 4, 22044, 1250, 1252, 35, 25, ConfigCodes.PLACEHOLDERS, 1, "Toggle Placeholders", 22045, 1251, 1253, "Bank/BANK",
				22046, "Enable\nPlaceHolders", "Disable\nPlaceHolders", 12, 20);
		rsi.child(38, 22043, 280, 285);
		rsi.child(39, 22044, 265, 225);
		
		rsi.child(1, 5383, 170, 15);
		rsi.child(2, 5385, -4, 74);
		addButton(22024, 267, "Click here to view the full contents of your bank");
		rsi.child(19, 22024, 22, 36);
		addButton(22025, 268, "Drag an item here to create a new tab");
		rsi.child(20, 22025, 70, 36);
		addButton(22026, 268, "Drag an item here to create a new tab");
		rsi.child(21, 22026, 118, 36);
		addButton(22027, 268, "Drag an item here to create a new tab");
		rsi.child(22, 22027, 166, 36);
		addButton(22028, 268, "Drag an item here to create a new tab");
		rsi.child(23, 22028, 214, 36);
		addButton(22029, 268, "Drag an item here to create a new tab");
		rsi.child(24, 22029, 262, 36);
		addButton(22030, 268, "Drag an item here to create a new tab");
		rsi.child(25, 22030, 310, 36);
		addButton(22031, 268, "Drag an item here to create a new tab");
		rsi.child(26, 22031, 358, 36);
		addButton(22032, 268, "Drag an item here to create a new tab");
		rsi.child(27, 22032, 406, 36);
		addText(22033, "134", 0, 0xB4965A, true, false);
		rsi.child(28, 22033, 473, 42);
		addText(22034, "496", 0, 0xB4965A, true, false);
		rsi.child(29, 22034, 473, 57);
		addBankItem(22035, false);
		RSInterface.interfaceCache[22035].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(30, 22035, 77, 39);
		addBankItem(22036, false);
		RSInterface.interfaceCache[22036].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(31, 22036, 125, 39);
		addBankItem(22037, false);
		RSInterface.interfaceCache[22037].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(32, 22037, 173, 39);
		addBankItem(22038, false);
		RSInterface.interfaceCache[22038].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(33, 22038, 221, 39);
		addBankItem(22039, false);
		RSInterface.interfaceCache[22039].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(34, 22039, 269, 39);
		addBankItem(22040, false);
		RSInterface.interfaceCache[22040].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(35, 22040, 317, 39);
		addBankItem(22041, false);
		RSInterface.interfaceCache[22041].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(36, 22041, 365, 39);
		addBankItem(22042, false);
		RSInterface.interfaceCache[22042].setFlag(RSInterface.DONT_DRAW_INVENTORY_COUNT_BIT);
		rsi.child(37, 22042, 413, 39);

		addText(27000, "0", 0xFF981F, false, true, 52, 1);
		addText(27001, "0", 0xFF981F, false, true, 52, 1);
		addText(27002, "0", 0xFF981F, false, true, 52, 1);

		rsi = interfaceCache[5385];
		rsi.height = 206;
		rsi.width = 480;
		rsi.scrollMax = 1400;
		rsi = interfaceCache[5382];
		rsi.width = 10; // 10 columns
		rsi.invSpritePadX = 12;
		rsi.height = 35; // 35 rows of items, so 10*35 = 350 items
		rsi.setFlag(TRANSPARENT_ON_ZERO_COUNT);

		// interfaceCache[5382].invStackSizes = interfaceCache[5382].inv = new
		// int[500]; // change bank size here
	}

	public static void summonTab() {
		RSInterface Tab = addInterface(17011, InterfaceType.TAB);
		addSprite(17012, 110);
		addButton(17013, 111, "Activate Special Attack");
		addSprite(17014, 110);
		addRadioButton(17015, 17032, 112, 113, 20, 30, "Familiar Special", 1, 5, 300);
		addHoverButton(17018, 114, "Beast of burden Inventory", -1, 17028, 1);
		addHoveredButton(17028, 115, 17029);
		addHoverButton(17022, 116, "Call Familiar", -1, 17030, 1);
		addHoveredButton(17030, 117, 17031);
		addHoverButton(17023, 118, "Dismiss Familiar", -1, 17033, 1);
		addHoveredButton(17033, 119, 17034);
		addHoverButton(17035, 116, "Pet Controller", -1, 17036, 1);
		addHoveredButton(17036, 117, 17037);
		addSprite(17016, 120);
		addText(17017, "", 2, 0xDAA520, false, true);
		addSprite(17019, 121);
		addText(17021, "6.00", 0, 0xFFA500, false, true);
		addSprite(17020, 122);
		addSprite(17024, 123);
		addText(17025, "49/50", 0, 0xFFA500, false, true);
		addText(17026, "10", 0, 0xFFA500, false, true);
		addHead2(17027, 75, 55, 800);
		Tab.totalChildren(21);
		Tab.child(0, 17012, 10, 25);
		Tab.child(1, 17013, 24, 7);
		Tab.child(2, 17014, 10, 25);
		Tab.child(3, 17015, 11, 25);
		Tab.child(4, 17016, 15, 140);
		Tab.child(5, 17017, 45, 143);
		Tab.child(6, 17018, 20, 170);
		Tab.child(7, 17019, 115, 167);
		Tab.child(8, 17020, 143, 170);
		Tab.child(9, 17021, 135, 197);
		Tab.child(10, 17022, 20, 213);
		Tab.child(11, 17023, 67, 213);
		Tab.child(12, 17024, 135, 214);
		Tab.child(13, 17025, 135, 240);
		Tab.child(14, 17026, 21, 59);
		Tab.child(15, 17027, 75, 55);
		Tab.child(16, 17028, 20, 170);
		Tab.child(17, 17030, 20, 213);
		Tab.child(18, 17033, 67, 213);
		Tab.child(19, 17035, 67, 170);
		Tab.child(20, 17036, 67, 170);
	}
	
	public static void normalSpellBook() {
		int air = 556;
		int mind = 558;
		int water = 555;
		int earth = 557;
		int fire = 554;
		int body = 559;
		int cosmic = 564;
		int chaos = 562;
		int astral = 9075;
		int nature = 561;
		int law = 563;
		int death = 560;
		int blood = 565;
		int soul = 566;
		int armadyl = 21773;

		InterfaceTargetMask useOnNpcMask = new InterfaceTargetMask().useOnNpc();
		InterfaceTargetMask useOnPlayerMask = new InterfaceTargetMask().useOnPlayer();
		InterfaceTargetMask useOnInvMask = new InterfaceTargetMask().useOnInventory();
		InterfaceTargetMask useOnGroundItem = new InterfaceTargetMask().useOnGroundItem();
		InterfaceTargetMask useOnObjMask = new InterfaceTargetMask().useOnObject();
		InterfaceTargetMask useOnNpcPlayer = new InterfaceTargetMask().useOnNpc().useOnPlayer();

		RSInterface tab = addInterface(1151, InterfaceType.TAB);

		//1st line
		addSprite(1152, 109, 24, 24);// home teleport
		addSpellData(1152, 1221, SpriteCache.get(109), SpriteCache.get(109), 1, "Home Teleport",
				"A teleport which requires no runes and no required level that teleports you to the main land.", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1153, "magicon", 0);
		addSpellData(1153, 1221, getSprite(0, aClass44, "magicoff", mediaIndex), getSprite(0, aClass44, "magicon", mediaIndex), 1,
				"Wind Strike", "A basic Air missile.", mind, 1, air, 1, -1, -1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1154, "magicon", 1);
		addSpellData(1154, 1221, getSprite(1, aClass44, "magicoff", mediaIndex), getSprite(1, aClass44, "magicon", mediaIndex), 3, "Confuse",
				"Reduces the target's chance to hit by 5% for 1 minute.", body, 1, earth, 2, water, 3, -1, -1,
				useOnNpcPlayer, false);
		addSpriteCache(1155, "magicon", 2);
		addSpellData(1155, 1221, getSprite(2, aClass44, "magicoff", mediaIndex), getSprite(2, aClass44, "magicon", mediaIndex), 5,
				"Water Strike", "A basic Water missile.", mind, 1, water, 1, air, 1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1156, "magicon", 3);
		addSpellData(1156, 1221, getSprite(3, aClass44, "magicoff", mediaIndex), getSprite(3, aClass44, "magicon", mediaIndex), 7,
				"Lvl-1 Enchant", "For use on sapphire and opal jewellrey.", water, 1, cosmic, 1, -1, -1, -1, -1, useOnInvMask, false);
		addSpriteCache(1157, "magicon", 4);
		addSpellData(1157, 1221, getSprite(4, aClass44, "magicoff", mediaIndex), getSprite(4, aClass44, "magicon", mediaIndex), 9,
				"Earth Strike", "A basic Earth missile.", mind, 1, earth, 2, air, 1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1158, "magicon", 5);
		addSpellData(1158, 1221, getSprite(5, aClass44, "magicoff", mediaIndex), getSprite(5, aClass44, "magicon", mediaIndex), 11, "Weaken",
				"Reduces the target's damage dealt by 5% for 1 minute.", body, 1, earth, 2, water, 3, -1, -1,
				useOnNpcPlayer, false);
		//2nd line
		addSpriteCache(1159, "magicon", 6);
		addSpellData(1159, 1221, getSprite(6, aClass44, "magicoff", mediaIndex), getSprite(6, aClass44, "magicon", mediaIndex), 13,
				"Fire Strike", "A basic Earth missile.", mind, 1, fire, 3, air, 2, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1160, "magicon", 7);
		addSpellData(1160, 1221, getSprite(7, aClass44, "magicoff", mediaIndex), getSprite(7, aClass44, "magicon", mediaIndex), 15,
				"Bones To Bananas", "Converts all bones in the inventory into bananas.", nature, 1, earth, 2, water, 2,
				-1, -1, useOnInvMask, false);
		addSpriteCache(1161, "magicon", 8);
		addSpellData(1161, 1221, getSprite(8, aClass44, "magicoff", mediaIndex), getSprite(8, aClass44, "magicon", mediaIndex), 17,
				"Wind Bolt", "A low level air missile.", chaos, 1, air, 2, -1, -1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1162, "magicon", 9);
		addSpellData(1162, 1221, getSprite(9, aClass44, "magicoff", mediaIndex), getSprite(9, aClass44, "magicon", mediaIndex), 19, "Curse",
				"Reduces your opponent's defence by 5%.", body, 1, earth, 3, water, 2, -1, -1,
				useOnNpcPlayer, false);
		addSpriteCache(1163, "magicon2", 0);// bind
		addSpellData(1163, 1221, getSprite(0, aClass44, "magicoff2", mediaIndex), getSprite(0, aClass44, "magicon2", mediaIndex), 20, "Bind",
				"Holds your opponent for 5 seconds", nature, 2, earth, 3, water, 3, -1, -1, useOnNpcPlayer, false);
		addSpriteCache(1164, "magicon", 10);// low level alch
		addSpellData(1164, 1221, getSprite(10, aClass44, "magicoff", mediaIndex), getSprite(10, aClass44, "magicon", mediaIndex), 21,
				"Low Level Alchemy", "Converts the targeted item into coins.", nature, 1, fire, 3, -1, -1, -1, -1,
				useOnInvMask, false);
		addSpriteCache(1165, "magicon", 11);// water bolt
		addSpellData(1165, 1221, getSprite(11, aClass44, "magicoff", mediaIndex), getSprite(11, aClass44, "magicon", mediaIndex), 23,
				"Water Bolt", "A low level water missile.", chaos, 1, water, 2, air, 2, -1, -1, useOnNpcPlayer, true);
		//3rd line
		addSpriteCache(1166, "magicon", 12);// monster Teleport
		addSpellData(1166, 1221, getSprite(12, aClass44, "magicon", mediaIndex), getSprite(12, aClass44, "magicon", mediaIndex), 1,
				"Monster Teleport", "Teleports you to monster locations.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1167, "magicon", 13);
		addSpellData(1167, 1221, getSprite(13, aClass44, "magicoff", mediaIndex), getSprite(13, aClass44, "magicon", mediaIndex), 27,
				"Lvl-2 Enchant", "For use on emerald and jade jewellrey.", air, 3, cosmic, 1, -1, -1, -1, -1, useOnInvMask, false);
		addSpriteCache(1168, "magicon", 14);// earth bolt
		addSpellData(1168, 1221, getSprite(14, aClass44, "magicoff", mediaIndex), getSprite(14, aClass44, "magicon", mediaIndex), 29,
				"Earth Bolt", "A low level earth missile.", chaos, 1, earth, 3, air, 2, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1169, "magicon", 15);// minigame Teleport
		addSpellData(1169, 1221, getSprite(15, aClass44, "magicon", mediaIndex), getSprite(15, aClass44, "magicon", mediaIndex), 1,
				"Minigame Teleport", "Teleports you to minigames.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1170, "magicon", 16);// telekinetic grab
		addSpellData(1170, 1221, getSprite(16, aClass44, "magicoff", mediaIndex), getSprite(16, aClass44, "magicon", mediaIndex), 33,
				"Telekinetic Grab", "Take an item you can see but can't reach", air, 1, law, 1, -1, -1, -1, -1, useOnGroundItem, false);
		addSpriteCache(1171, "magicon", 17);// fire blast
		addSpellData(1171, 1221, getSprite(17, aClass44, "magicoff", mediaIndex), getSprite(17, aClass44, "magicon", mediaIndex), 35,
				"Fire Bolt", "A low level fire missile.", chaos, 1, fire, 3, air, 3, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1172, "magicon", 18);// boss Teleport
		addSpellData(1172, 1221, getSprite(18, aClass44, "magicon", mediaIndex), getSprite(18, aClass44, "magicon", mediaIndex), 1,
				"Boss Teleport", "Teleports you to bosses.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		//4th line
		addSpriteCache(1173, "magicon", 19);// Crumble Undead
		addSpellData(1173, 1221, getSprite(19, aClass44, "magicoff", mediaIndex), getSprite(19, aClass44, "magicon", mediaIndex), 39,
				"Crumble Undead", "Hits skeletons, ghosts, shades & zombies hard", earth, 2, air, 2, chaos, 1, -1, -1, useOnNpcMask, true);
		addSpriteCache(1174, "magicon2", 36);// House teleport
		addSpellData(1174, 1221, getSprite(36, aClass44, "magicon2", mediaIndex), getSprite(36, aClass44, "magicon2", mediaIndex), 1,
				"Teleport to House", "Teleports you to your house", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1175, "magicon", 20);// wind blast
		addSpellData(1175, 1221, getSprite(20, aClass44, "magicoff", mediaIndex), getSprite(20, aClass44, "magicon", mediaIndex), 41,
				"Wind Blast", "A medium level air missile.", death, 1, air, 3, -1, -1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1176, "magicon", 21);
		addSpellData(1176, 1221, getSprite(21, aClass44, "magicoff", mediaIndex), getSprite(21, aClass44, "magicon", mediaIndex), 43,
				"Superheat Item", "Smelt ore without a furnance", fire, 4, nature, 1, -1, -1, -1, -1, useOnInvMask, false);
		addSpriteCache(1177, "magicon", 22);// PKing teleport
		addSpellData(1177, 1221, getSprite(22, aClass44, "magicon", mediaIndex), getSprite(22, aClass44, "magicon", mediaIndex), 1,
				"PKing teleport", "Teleports you to pking locations", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1178, "magicon", 23);// water blast
		addSpellData(1178, 1221, getSprite(23, aClass44, "magicoff", mediaIndex), getSprite(23, aClass44, "magicon", mediaIndex), 47,
				"Water Blast", "A medium level water missile.", death, 1, water, 3, air, 3, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1179, "magicon", 24);
		addSpellData(1179, 1221, getSprite(24, aClass44, "magicoff", mediaIndex), getSprite(24, aClass44, "magicon", mediaIndex), 49,
				"Lvl-3 Enchant", "For use on ruby and topaz jewellrey.", fire, 5, cosmic, 1, -1, -1, -1, -1, useOnInvMask, false);
		//5th line
		addSpriteCache(1180, "magicon", 38);// Iban blast
		addSpellData(1180, 1221, getSprite(38, aClass44, "magicoff", mediaIndex), getSprite(38, aClass44, "magicon", mediaIndex), 47,
				"Iban Blast", "A medium level water missile.", fire, 5, death, 1, 1409, 1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1181, "magicon2", 1);// snare
		addSpellData(1181, 1221, getSprite(1, aClass44, "magicoff2", mediaIndex), getSprite(1, aClass44, "magicon2", mediaIndex), 50, "Snare",
				"Holds your opponent for 10 seconds", nature, 3, earth, 4, water,
				4, -1, -1, useOnNpcPlayer, false);
		addSpriteCache(1182, "magicon2", 5);// magic dart
		addSpellData(1182, 1221, getSprite(5, aClass44, "magicoff2", mediaIndex), getSprite(5, aClass44, "magicon2", mediaIndex), 50,
				"Magic Dart", "A magic dart of slaying", 4170, 1, death, 1, mind, 4, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1183, "magicon", 39);// Skilling teleport
		addSpellData(1183, 1221, getSprite(39, aClass44, "magicon", mediaIndex), getSprite(39, aClass44, "magicon", mediaIndex), 1,
				"Skilling teleport", "Teleports you to skilling locations", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1184, "magicon", 25);// earth blast
		addSpellData(1184, 1221, getSprite(25, aClass44, "magicoff", mediaIndex), getSprite(25, aClass44, "magicon", mediaIndex), 53,
				"Earth Blast", "A medium level earth missile.", death, 1, earth, 4, air, 3, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1185, "magicon", 26);// high level alch
		addSpellData(1185, 1221, getSprite(26, aClass44, "magicoff", mediaIndex), getSprite(26, aClass44, "magicon", mediaIndex), 55,
				"High Level Alchemy", "Converts the targeted item into even more coins", nature, 1, fire, 5, -1, -1, -1,
				-1, useOnInvMask, false);
		addSpriteCache(1186, "magicon", 27);// charge water orb 
		addSpellData(1186, 1221, getSprite(27, aClass44, "magicoff", mediaIndex), getSprite(27, aClass44, "magicon", mediaIndex), 56,
				"Charge Water Orb", "Needs to be cast on a water obelisk", water, 30, cosmic, 3, 567, 1, -1,
				-1, useOnObjMask, false);
		//6th line
		addSpriteCache(1187, "magicon", 28);
		addSpellData(1187, 1221, getSprite(28, aClass44, "magicoff", mediaIndex), getSprite(28, aClass44, "magicon", mediaIndex), 57,
				"Lvl-4 Enchant", "For use on diamond jewellrey.", earth, 10, cosmic, 1, -1, -1, -1, -1, useOnInvMask, false);
		addSpriteCache(1188, "magicon", 40);// City teleport
		addSpellData(1188, 1221, getSprite(40, aClass44, "magicon", mediaIndex), getSprite(40, aClass44, "magicon", mediaIndex), 1,
				"City teleport", "Teleports you to cities around the world.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1189, "magicon", 29);// fire blast
		addSpellData(1189, 1221, getSprite(29, aClass44, "magicoff", mediaIndex), getSprite(29, aClass44, "magicon", mediaIndex), 59,
				"Fire Blast", "A medium level fire missile.", death, 1, fire, 5, air, 4, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1190, "magicon", 30);// charge earth orb 
		addSpellData(1190, 1221, getSprite(30, aClass44, "magicoff", mediaIndex), getSprite(30, aClass44, "magicon", mediaIndex), 60,
				"Charge Earth Orb", "Needs to be cast on an earth obelisk", earth, 30, cosmic, 3, 567, 1, -1,
				-1, useOnObjMask, false);
		addSpriteCache(1191, "magicon2", 35); //bones to peaches
		addSpellData(1191, 1221, getSprite(35, aClass44, "magicoff2", mediaIndex), getSprite(35, aClass44, "magicon2", mediaIndex), 60,
				"Bones to Peaches", "For use on diamond jewellrey.", nature, 2, water, 4, earth, 4, -1, -1, useOnInvMask, false);
		addSpriteCache(1192, "magicon", 46);// Saradomin strike
		addSpellData(1192, 1221, getSprite(46, aClass44, "magicoff", mediaIndex), getSprite(46, aClass44, "magicon", mediaIndex), 60,
				"Saradomin strike", "Summons the power of Saradomin", fire, 2, blood, 2, air, 4, 2415, 1, useOnNpcPlayer, true);
		addSpriteCache(1193, "magicon", 45);// claws of guthix
		addSpellData(1193, 1221, getSprite(45, aClass44, "magicoff", mediaIndex), getSprite(45, aClass44, "magicon", mediaIndex), 60,
				"Claws of Guthix", "Summons the power of Guthix", fire, 1, blood, 2, air, 4, 8843, 1, useOnNpcPlayer, true);
		//line 7
		addSpriteCache(1194, "magicon", 44);// flames of zamorak
		addSpellData(1194, 1221, getSprite(44, aClass44, "magicoff", mediaIndex), getSprite(44, aClass44, "magicon", mediaIndex), 60,
				"Flames of Zamorak", "Summons the power of Zamorak", fire, 4, blood, 2, air, 1, 2417, 1, useOnNpcPlayer, true);
		addSpriteCache(1195, "magicon2", 28);// PKing teleport
		addSpellData(1195, 1221, getSprite(28, aClass44, "magicon2", mediaIndex), getSprite(28, aClass44, "magicon2", mediaIndex), 1,
				"Dungeon teleport", "Teleports you to various dungeons around the world.", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1196, "magicon", 31);// wind wave
		addSpellData(1196, 1221, getSprite(31, aClass44, "magicoff", mediaIndex), getSprite(31, aClass44, "magicon", mediaIndex), 62,
				"Wind Wave", "A high level air missile.", blood, 1, air, 5, -1, -1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1197, "magicon", 32);// charge fire orb 
		addSpellData(1197, 1221, getSprite(32, aClass44, "magicoff", mediaIndex), getSprite(32, aClass44, "magicon", mediaIndex), 63,
				"Charge Fire Orb", "Needs to be cast on an fire obelisk", fire, 30, cosmic, 3, 567, 1, -1,
				-1, useOnObjMask, false);
		addSpriteCache(1198, "magicon", 39);// Slayer teleport
		addSpellData(1198, 1221, getSprite(39, aClass44, "magicon", mediaIndex), getSprite(39, aClass44, "magicon", mediaIndex), 1,
				"Slayer teleport", "Teleports you to slayer masters", -1, -1, -1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(1199, "magicon", 33);// water wave
		addSpellData(1199, 1221, getSprite(33, aClass44, "magicoff", mediaIndex), getSprite(33, aClass44, "magicon", mediaIndex), 65,
				"Water Wave", "A high level water missile.", blood, 1, water, 7, air, 5, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1200, "magicon", 34);// charge air orb 
		addSpellData(1200, 1221, getSprite(34, aClass44, "magicoff", mediaIndex), getSprite(34, aClass44, "magicon", mediaIndex), 66,
				"Charge Air Orb", "Needs to be cast on an air obelisk", air, 30, cosmic, 3, 567, 1, -1,
				-1, useOnObjMask, false);
		//line 8
		addSpriteCache(1201, "magicon", 41);// vuln
		addSpellData(1201, 1221, getSprite(41, aClass44, "magicoff", mediaIndex), getSprite(41, aClass44, "magicon", mediaIndex), 66,
				"Vulnerability", "Increases the target's damage received by 10% for 1 minute.", soul, 1, earth, 5,
				water, 5, -1, -1, useOnNpcPlayer, false);
		addSpriteCache(1202, "magicon", 35);//lvl-5 enchant
		addSpellData(1202, 1221, getSprite(35, aClass44, "magicoff", mediaIndex), getSprite(35, aClass44, "magicon", mediaIndex), 68,
				"Lvl-5 Enchant", "For use on dragonstone jewellrey.", water, 15, earth, 15, cosmic, 1, -1, -1, useOnInvMask, false);
		addSpriteCache(1203, "magicon", 36);// earth wave
		addSpellData(1203, 1221, getSprite(36, aClass44, "magicoff", mediaIndex), getSprite(36, aClass44, "magicon", mediaIndex), 70,
				"Earth Wave", "A high level earth missile.", blood, 1, earth, 7, air, 5, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1204, "magicon", 42);// enfeeble
		addSpellData(1204, 1221, getSprite(42, aClass44, "magicoff", mediaIndex), getSprite(42, aClass44, "magicon", mediaIndex), 73,
				"Enfeeble", "Reduces the target's damage dealt by 10% for 1 minute.", soul, 1, earth, 8, water, 8, -1,
				-1, useOnNpcPlayer, false);
		addSpriteCache(1205, "magicon2", 30);//teleother lumbridge
		addSpellData(1205, 1221, getSprite(30, aClass44, "magicoff2", mediaIndex), getSprite(30, aClass44, "magicon2", mediaIndex), 74,
				"Teleother Lumbridge", "Teleports target to Lumbridge", soul, 1, law, 1, earth, 1, -1, -1, useOnPlayerMask, false);
		addSpriteCache(1206, "magicon", 37);// fire wave
		addSpellData(1206, 1221, getSprite(37, aClass44, "magicoff", mediaIndex), getSprite(37, aClass44, "magicon", mediaIndex), 75,
				"Fire Wave", "A high level fire missile.", blood, 1, fire, 7, air, 5, -1, -1, useOnNpcPlayer, true);
		addSprite(1207, 1257, 24, 24);// Storm of Armadyl
		addSpellData(1207, 1221, SpriteCache.get(1256), SpriteCache.get(1257), 77,
				"Storm of Armadyl", "A very strong armadyl spell", armadyl, 1, -1, -1, -1, -1, -1, -1, useOnNpcPlayer, true);
		//line 9
		addSpriteCache(1208, "magicon2", 2);// entangle
		addSpellData(1208, 1221, getSprite(2, aClass44, "magicoff2", mediaIndex), getSprite(2, aClass44, "magicon2", mediaIndex), 79,
				"Entangle", "Prevents creatures from moving for 24 seconds, or players for 12 seconds.", nature, 4,
				earth, 5, water, 5, -1, -1, useOnNpcPlayer, false);
		addSpriteCache(1209, "magicon", 43);// stagger
		addSpellData(1209, 1221, getSprite(43, aClass44, "magicoff", mediaIndex), getSprite(43, aClass44, "magicon", mediaIndex), 80, "Stun",
				"Reduces the target's chance to hit by 10% for 1 minute.", soul, 1, earth, 12, water, 12, -1, -1,
				useOnNpcPlayer, false);
		addSpriteCache(1210, "magicon2", 3);// charge
		addSpellData(1210, 1221, getSprite(3, aClass44, "magicoff2", mediaIndex), getSprite(3, aClass44, "magicon2", mediaIndex), 80,
				"Charge", "Temporarily increases the power of the three arena spells when wearing Mage Arena capes", fire, 3, blood, 3, air, 3, -1, -1, null, false);
		addSprite(1211, 804, 24, 24);// air surge
		addSpellData(1211, 1221, SpriteCache.get(821), SpriteCache.get(804), 81, "Wind Surge",
				"A very high level Air missile", air, 7, blood, 2, -1, -1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1212, "magicon2", 31);//teleother falador
		addSpellData(1212, 1221, getSprite(31, aClass44, "magicoff2", mediaIndex), getSprite(31, aClass44, "magicon2", mediaIndex), 82,
				"Teleother Falador", "Teleports target to Falador", soul, 1, law, 1, earth, 1, -1, -1, useOnPlayerMask, false);
		addSprite(1213, 805, 24, 24);// water surge
		addSpellData(1213, 1221, SpriteCache.get(822), SpriteCache.get(805), 85, "Water Surge",
				"A very high level Water missile", water, 10, air, 7, blood, 2, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1214, "magicon2", 33);//tele block
		addSpellData(1214, 1221, getSprite(33, aClass44, "magicoff2", mediaIndex), getSprite(33, aClass44, "magicon2", mediaIndex), 85,
				"Tele Block", "Stops your target from teleporting", law, 1, chaos, 1, death, 1, -1, -1, useOnPlayerMask, false);
		addSprite(1215, 1255, 24, 24);//Teleport to bounty target
		addSpellData(1215, 1221, SpriteCache.get(1254), SpriteCache.get(1255), 85,
				"Teleport to Bounty Target", "Teleports you near your Bounty Hunter target", law, 1, death, 1, chaos, 1, -1, -1, null, false);
		//line 10
		addSpriteCache(1216, "magicon2", 34);//lvl-6 enchant
		addSpellData(1216, 1221, getSprite(34, aClass44, "magicoff2", mediaIndex), getSprite(34, aClass44, "magicon2", mediaIndex), 87,
				"Lvl-6 Enchant", "For use on onyx jewellrey.", earth, 20, fire, 20, cosmic, 1, -1, -1, useOnInvMask, false);
		addSpriteCache(1217, "magicon2", 32);//teleother camelot
		addSpellData(1217, 1221, getSprite(32, aClass44, "magicoff2", mediaIndex), getSprite(32, aClass44, "magicon2", mediaIndex), 90,
				"Teleother Camelot", "Teleports target to Camelot", soul, 2, law, 1, -1, -1, -1, -1, useOnPlayerMask, false);
		addSprite(1218, 806, 24, 24);// earth surge
		addSpellData(1218, 1221, SpriteCache.get(823), SpriteCache.get(806), 90, "Earth Surge",
				"A very high level Earth missile", earth, 10, air, 7, blood, 2, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(1219, "magicon2", 34);//lvl-6 enchant
		addSpellData(1219, 1221, getSprite(34, aClass44, "magicoff2", mediaIndex), getSprite(34, aClass44, "magicon2", mediaIndex), 93,
				"Lvl-7 Enchant", "For use on zenyte jewellrey.", blood, 20, soul, 20, cosmic, 1, -1, -1, useOnInvMask, false);
		addSprite(1220, 807, 24, 24);// fire surge
		addSpellData(1220, 1221, SpriteCache.get(808), SpriteCache.get(807), 95, "Fire Surge",
				"A very high level Fire missile", fire, 10, air, 7, blood, 2, -1, -1, useOnNpcPlayer, true);
		
		addInterface(1221);// Dynamic layer

		tab.totalChildren(70);
		int y = 12;
		tab.child(0, 1152, 13, y);
		tab.child(1, 1153, 36, y);
		tab.child(2, 1154, 60, y);
		tab.child(3, 1155, 84, y);
		tab.child(4, 1156, 108, y);
		tab.child(5, 1157, 132, y);
		tab.child(6, 1158, 156, y);
		y += 24;

		tab.child(7, 1159, 13, y);
		tab.child(8, 1160, 36, y);
		tab.child(9, 1161, 60, y);
		tab.child(10, 1162, 84, y);
		tab.child(11, 1163, 108, y);
		tab.child(12, 1164, 132, y);
		tab.child(13, 1165, 156, y);
		y += 24;

		tab.child(14, 1166, 13, y);
		tab.child(15, 1167, 36, y);
		tab.child(16, 1168, 60, y);
		tab.child(17, 1169, 84, y);
		tab.child(18, 1170, 108, y);
		tab.child(19, 1171, 132, y);
		tab.child(20, 1172, 156, y);
		y += 24;

		tab.child(21, 1173, 13, y);
		tab.child(22, 1174, 36, y);
		tab.child(23, 1175, 60, y);
		tab.child(24, 1176, 84, y);
		tab.child(25, 1177, 108, y);
		tab.child(26, 1178, 132, y);
		tab.child(27, 1179, 156, y);
		y += 24;

		tab.child(28, 1180, 13, y);
		tab.child(29, 1181, 36, y);
		tab.child(30, 1182, 60, y);
		tab.child(31, 1183, 84, y);
		tab.child(32, 1184, 108, y);
		tab.child(33, 1185, 132, y);
		tab.child(34, 1186, 156, y);
		y += 24;

		tab.child(35, 1187, 13, y);
		tab.child(36, 1188, 36, y);
		tab.child(37, 1189, 60, y);
		tab.child(38, 1190, 84, y);
		tab.child(39, 1191, 108, y);
		tab.child(40, 1192, 132, y);
		tab.child(41, 1193, 156, y);
		y += 24;

		tab.child(42, 1194, 13, y);
		tab.child(43, 1195, 36, y);
		tab.child(44, 1196, 60, y);
		tab.child(45, 1197, 84, y);
		tab.child(46, 1198, 108, y);
		tab.child(47, 1199, 132, y);
		tab.child(48, 1200, 156, y);
		y += 24;
		
		tab.child(49, 1201, 13, y);
		tab.child(50, 1202, 36, y);
		tab.child(51, 1203, 60, y);
		tab.child(52, 1204, 84, y);
		tab.child(53, 1205, 108, y);
		tab.child(54, 1206, 132, y);
		tab.child(55, 1207, 156, y);
		y += 24;
		
		tab.child(56, 1208, 13, y);
		tab.child(57, 1209, 36, y);
		tab.child(58, 1210, 60, y);
		tab.child(59, 1211, 84, y);
		tab.child(60, 1212, 108, y);
		tab.child(61, 1213, 132, y);
		tab.child(62, 1214, 156, y);
		y += 24;
		
		tab.child(63, 1215, 13, y);
		tab.child(64, 1216, 36, y);
		tab.child(65, 1217, 60, y);
		tab.child(66, 1218, 84, y);
		tab.child(67, 1219, 108, y);
		tab.child(68, 1220, 132, y);
		//tab.child(69, 1221, 156, 198);
		
		tab.child(69, 1221, 0, 0);
	}

	/*public static void magicTab() {
		RSInterface tab = addInterface(1151, InterfaceType.TAB);
		RSInterface homeHover = addInterface(1196, InterfaceType.TAB);
		RSInterface spellButtons = interfaceCache[12424];
		spellButtons.scrollMax = 0;
		spellButtons.height = 260;
		spellButtons.width = 190;

		addButton(1195, 109, "Cast <col=ff00>Home Teleport");
		RSInterface homeButton = interfaceCache[1195];
		homeButton.hoverType = 1196;

		int[] spellButton = { 1196, 1199, 1206, 1215, 1224, 1231, 1240, 1249, 1258, 1267, 1274, 1283, 1573, 1290, 1299,
				1308, 1315, 1324, 1333, 1340, 1349, 1358, 1367, 1374, 1381, 1388, 1397, 1404, 1583, 12038, 1414, 1421,
				1430, 1437, 1446, 1453, 1460, 1469, 15878, 1602, 1613, 1624, 7456, 1478, 1485, 1494, 1503, 1512, 1521,
				1530, 1544, 1553, 1563, 1593, 1635, 12426, 12436, 12446, 12456, 6004 };
		tab.totalChildren(63 + 1); // 2 for new teleport, the hover and the
									// button
		tab.child(0, 12424, 13, 24);
		tab.child(1, 1195, 13, 24);
		int lastChild = 0;
		for (int i1 = 0; i1 < spellButton.length; i1++) {
			int yPos = i1 > 34 ? 8 : 183;
			tab.child(i1 + 2, spellButton[i1], 5, yPos);
			lastChild = i1 + 2;
		}
		addSmallBoxTeleport(15276, "Monkey Nuts", "You can get monkey nuts here.\\nLots of Monkey Nuts.", "magicon2",
				38);
		tab.child(++lastChild, 15276, 84, 168); // add the button location
		tab.child(++lastChild, 15277, 3, 4);// add the hover location

		for (int i2 = 0; i2 < spellButton.length; i2++) {
			if (i2 < 60)
				spellButtons.childX[i2] = (short) (spellButtons.childX[i2] + 24);
			if (i2 == 6 || i2 == 12 || i2 == 19 || i2 == 35 || i2 == 41 || i2 == 44 || i2 == 49 || i2 == 51)
				spellButtons.childX[i2] = 0;
			spellButtons.childY[6] = 24;
			spellButtons.childY[12] = 48;
			spellButtons.childY[19] = 72;
			spellButtons.childY[49] = 96;
			spellButtons.childY[44] = 120;
			spellButtons.childY[51] = 144;
			spellButtons.childY[35] = 170;
			spellButtons.childY[41] = 192;
		}
		homeHover.isMouseoverTriggered = true;
		homeHover.width = 182;
		homeHover.height = 76;
		homeHover.hoverType = -1;
		addText(1197, "Level 0: Home Teleport", 1, 0xFE981F, true, true);
		RSInterface homeLevel = interfaceCache[1197];
		homeLevel.width = 174;
		homeLevel.height = 68;
		addText(1198, "A teleport which requires no", 0, 0xAF6A1A, true, true);
		addText(18998, "runes and no required level that", 0, 0xAF6A1A, true, true);
		addText(18999, "teleports you to the main land.", 0, 0xAF6A1A, true, true);
		homeHover.totalChildren(4);
		homeHover.child(0, 1197, 3, 4);
		homeHover.child(1, 1198, 91, 23);
		homeHover.child(2, 18998, 91, 34);
		homeHover.child(3, 18999, 91, 45);
	}*/
	
	public static void ancientSpellBook() {
		int air = 556;
		int mind = 558;
		int water = 555;
		int earth = 557;
		int fire = 554;
		int body = 559;
		int cosmic = 564;
		int chaos = 562;
		int astral = 9075;
		int nature = 561;
		int law = 563;
		int death = 560;
		int blood = 565;
		int soul = 566;
		int zuriel_staff = 13867;

		InterfaceTargetMask useOnNpcPlayer = new InterfaceTargetMask().useOnNpc().useOnPlayer();

		RSInterface tab = addInterface(12855, InterfaceType.TAB);

		//1st line
		addSprite(12856, 109, 24, 24);// home teleport
		addSpellData(12856, 12883, SpriteCache.get(109), SpriteCache.get(109), 1, "Home Teleport",
				"A teleport which requires no runes and no required level that teleports you to the main land.", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(12857, "magicon2", 10);
		addSpellData(12857, 12883, getSprite(10, aClass44, "magicoff2", mediaIndex), getSprite(10, aClass44, "magicon2", mediaIndex), 50,
				"Smoke Rush", "A single target smoke attack", death, 2, chaos, 2, fire, 1, air, 1, useOnNpcPlayer, true);
		addSpriteCache(12858, "magicon2", 18);
		addSpellData(12858, 12883, getSprite(18, aClass44, "magicoff2", mediaIndex), getSprite(18, aClass44, "magicon2", mediaIndex), 52,
				"Shadow Rush", "A single target shadow attack", soul, 1, death, 2, chaos, 2, air, 2, useOnNpcPlayer, true);
		addSprite(12859, 1272, 24, 24);// monster teleport
		addSpellData(12859, 12883, SpriteCache.get(1272), SpriteCache.get(1272), 1, "Monster Teleport",
				"A teleport which requires no runes and no required level that teleports you to monster locations", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		
		//line 2
		addSpriteCache(12860, "magicon2", 14);
		addSpellData(12860, 12883, getSprite(14, aClass44, "magicoff2", mediaIndex), getSprite(14, aClass44, "magicon2", mediaIndex), 56,
				"Blood Rush", "A single target blood attack", blood, 1, death, 2, chaos, 2, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(12861, "magicon2", 6);
		addSpellData(12861, 12883, getSprite(6, aClass44, "magicoff2", mediaIndex), getSprite(6, aClass44, "magicon2", mediaIndex), 58,
				"Ice Rush", "A single target ice attack", death, 2, chaos, 2, fire, 1, air, 1, useOnNpcPlayer, true);
		addSprite(12862, 1274, 24, 24);// miasmic blitz
		addSpellData(12862, 12883, SpriteCache.get(1275), SpriteCache.get(1274), 61,
				"Miasmic Rush", "A single target miasmic attack", zuriel_staff, 1, chaos, 2, earth, 1, soul, 1, useOnNpcPlayer, true);
		addSpriteCache(12863, "magicon2", 11);
		addSpellData(12863, 12883, getSprite(11, aClass44, "magicoff2", mediaIndex), getSprite(11, aClass44, "magicon2", mediaIndex), 62,
				"Smoke Burst", "A multi-target smoke attack", death, 2, chaos, 4, fire, 2, air, 2, useOnNpcPlayer, true);
		
		//line 3
		addSpriteCache(12864, "magicon2", 19);
		addSpellData(12864, 12883, getSprite(19, aClass44, "magicoff2", mediaIndex), getSprite(19, aClass44, "magicon2", mediaIndex), 64,
				"Shadow Burst", "A multi-target shadow attack", death, 2, chaos, 4, fire, 2, air, 2, useOnNpcPlayer, true);
		addSprite(12865, 1273, 24, 24);// Skilling teleport
		addSpellData(12865, 12883, SpriteCache.get(1273), SpriteCache.get(1273), 1, "City Teleport",
				"A teleport which requires no runes and no required level that teleports you to cities", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(12866, "magicon2", 15);
		addSpellData(12866, 12883, getSprite(15, aClass44, "magicoff2", mediaIndex), getSprite(15, aClass44, "magicon2", mediaIndex), 68,
				"Blood Burst", "A multi-target blood attack", blood, 2, death, 2, chaos, 4, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(12867, "magicon2", 7);
		addSpellData(12867, 12883, getSprite(7, aClass44, "magicoff2", mediaIndex), getSprite(7, aClass44, "magicon2", mediaIndex), 70,
				"Ice Burst", "A multi-target ice attack", death, 2, chaos, 4, water, 4, -1, -1, useOnNpcPlayer, true);
		
		//line 4
		addSprite(12868, 1276, 24, 24);// miasmic blitz
		addSpellData(12868, 12883, SpriteCache.get(1277), SpriteCache.get(1276), 73,
				"Miasmic Burst", "A multi-target miasmic attack", zuriel_staff, 1, chaos, 4, earth, 2, soul, 2, useOnNpcPlayer, true);
		addSpriteCache(12869, "magicon2", 12);
		addSpellData(12869, 12883, getSprite(12, aClass44, "magicoff2", mediaIndex), getSprite(12, aClass44, "magicon2", mediaIndex), 74,
				"Smoke Blitz", "A single target strong smoke attack", blood, 2, death, 2, fire, 2, air, 2, useOnNpcPlayer, true);
		addSpriteCache(12870, "magicon2", 20);
		addSpellData(12870, 12883, getSprite(20, aClass44, "magicoff2", mediaIndex), getSprite(20, aClass44, "magicon2", mediaIndex), 76,
				"Shadow Blitz", "A single target strong shadow attack", soul, 2, blood, 2, death, 2, air, 2, useOnNpcPlayer, true);
		addSprite(12871, 1278, 24, 24);// pking teleport
		addSpellData(12871, 12883, SpriteCache.get(1278), SpriteCache.get(1278), 1, "PKing Teleport",
				"A teleport which requires no runes and no required level that teleports you to Wilderness locations", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		
		//line 5
		addSpriteCache(12872, "magicon2", 16);
		addSpellData(12872, 12883, getSprite(16, aClass44, "magicoff2", mediaIndex), getSprite(16, aClass44, "magicon2", mediaIndex), 80,
				"Blood Blitz", "A single target strong blood attack", blood, 4, death, 2, -1, -1, -1, -1, useOnNpcPlayer, true);
		addSpriteCache(12873, "magicon2", 8);
		addSpellData(12873, 12883, getSprite(8, aClass44, "magicoff2", mediaIndex), getSprite(8, aClass44, "magicon2", mediaIndex), 82,
				"Ice Blitz", "A single target strong ice attack", blood, 2, death, 2, water, 3, -1, -1, useOnNpcPlayer, true);
		addSprite(12874, 1279, 24, 24);// miasmic blitz
		addSpellData(12874, 12883, SpriteCache.get(1280), SpriteCache.get(1279), 85,
				"Miasmic Blitz", "A single target strong miasmic attack", zuriel_staff, 1, blood, 2, earth, 3, soul, 3, useOnNpcPlayer, true);
		addSprite(12875, 1255, 24, 24);//Teleport to bounty target
		addSpellData(12875, 12883, SpriteCache.get(1254), SpriteCache.get(1255), 85,
				"Teleport to Bounty Target", "Teleports you near your Bounty Hunter target", law, 1, death, 1, chaos, 1, -1, -1, null, false);
		
		//line 6
		addSpriteCache(12876, "magicon2", 13);
		addSpellData(12876, 12883, getSprite(13, aClass44, "magicoff2", mediaIndex), getSprite(13, aClass44, "magicon2", mediaIndex), 86,
				"Smoke Barrage", "A multi-target strong smoke attack", blood, 2, death, 4, fire, 4, air, 4, useOnNpcPlayer, true);
		addSpriteCache(12877, "magicon2", 21);
		addSpellData(12877, 12883, getSprite(21, aClass44, "magicoff2", mediaIndex), getSprite(21, aClass44, "magicon2", mediaIndex), 88,
				"Shadow Barrage", "A multi-target strong shadow attack", soul, 3, blood, 2, death, 4, air, 4, useOnNpcPlayer, true);
		addSprite(12878, 1272, 24, 24);// monster teleport
		addSpellData(12878, 12883, SpriteCache.get(1272), SpriteCache.get(1272), 1, "Minigame Teleport",
				"A teleport which requires no runes and no required level that teleports you to minigames", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		addSpriteCache(12879, "magicon2", 17);
		addSpellData(12879, 12883, getSprite(17, aClass44, "magicoff2", mediaIndex), getSprite(17, aClass44, "magicon2", mediaIndex), 92,
				"Blood Barrage", "A multi-target strong blood attack", soul, 1, blood, 4, death, 4, -1, -1, useOnNpcPlayer, true);
		
		//line 7
		addSpriteCache(12880, "magicon2", 9);
		addSpellData(12880, 12883, getSprite(9, aClass44, "magicoff2", mediaIndex), getSprite(9, aClass44, "magicon2", mediaIndex), 94,
				"Ice Barrage", "A multi-target strong ice attack", blood, 2, death, 4, water, 6, -1, -1, useOnNpcPlayer, true);
		addSprite(12881, 1281, 24, 24);// monster teleport
		addSpellData(12881, 12883, SpriteCache.get(1281), SpriteCache.get(1281), 1, "Boss Teleport",
				"A teleport which requires no runes and no required level that teleports you to bosses", -1, -1,
				-1, -1, -1, -1, -1, -1, null, false);
		addSprite(12882, 1282, 24, 24);// miasmic blitz
		addSpellData(12882, 12883, SpriteCache.get(1283), SpriteCache.get(1282), 97,
				"Miasmic Barrage", "A multi-target strong miasmic attack", zuriel_staff, 1, blood, 4, earth, 4, soul, 4, useOnNpcPlayer, true);
		
		addInterface(12883);// Dynamic layer

		tab.totalChildren(28);
		int y = 8;
		int x = 16;
		tab.child(0, 12856, x, y);
		tab.child(1, 12857, x + 45, y);
		tab.child(2, 12858, x + 45 + 45, y);
		tab.child(3, 12859, x + 45 + 45 + 45, y);
		y += 28;
		
		tab.child(4, 12860, x, y);
		tab.child(5, 12861, x + 45, y);
		tab.child(6, 12862, x + 45 + 45, y);
		tab.child(7, 12863, x + 45 + 45 + 45, y);
		y += 29;
		
		tab.child(8, 12864, x, y);
		tab.child(9, 12865, x + 45, y);
		tab.child(10, 12866, x + 45 + 45, y);
		tab.child(11, 12867, x + 45 + 45 + 45, y);
		y += 28;
		
		tab.child(12, 12868, x, y);
		tab.child(13, 12869, x + 45, y);
		tab.child(14, 12870, x + 45 + 45, y);
		tab.child(15, 12871, x + 45 + 45 + 45, y);
		y += 28;
		
		tab.child(16, 12872, x, y);
		tab.child(17, 12873, x + 45, y);
		tab.child(18, 12874, x + 45 + 45, y);
		tab.child(19, 12875, x + 45 + 45 + 45, y);
		y += 28;
		
		tab.child(20, 12876, x, y);
		tab.child(21, 12877, x + 45, y);
		tab.child(22, 12878, x + 45 + 45, y);
		tab.child(23, 12879, x + 45 + 45 + 45, y);
		y += 28;
		
		tab.child(24, 12880, x, y);
		tab.child(25, 12881, x + 45, y);
		tab.child(26, 12882, x + 45 + 45, y);
		
		tab.child(27, 12883, 0, 0);
	}

	/*public static void ancientMagicTab() {
		RSInterface tab = addInterface(12855, InterfaceType.TAB);
		addButton(12856, 109, "Cast <col=ff00>Home Teleport");
		RSInterface homeButton = interfaceCache[12856];
		homeButton.hoverType = 1196;

		int[] itfChildren = { 12856, 12939, 12987, 13035, 12901, 12861, 13045, 12963, 13011, 13053, 12919, 12881, 13061,
				12951, 12999, 13069, 12911, 12871, 13079, 13095, 12975, 13023, 13087, 12929, 12891, 1196, 12940, 12988,
				13036, 12902, 12862, 13046, 12964, 13012, 13054, 12920, 12882, 13062, 12952, 13000, 13070, 12912, 12872,
				13080, 12976, 13024, 13088, 12930, 12892, 13096 };

		tab.totalChildren(itfChildren.length);
		for (int i1 = 0, xPos = 18, yPos = 8; i1 < itfChildren.length; i1++, xPos += 45) {
			if (xPos > 175) {
				xPos = 18;
				yPos += 28;
			}
			if (i1 < 25) {
				tab.child(i1, itfChildren[i1], xPos, yPos);
			} else if (i1 > 24) {
				yPos = i1 < 41 ? 181 : 1;
				tab.child(i1, itfChildren[i1], 4, yPos);
			}
		}
	}*/

	public static void barbarianAssault() {
		RSInterface tab = addScreenInterface(24439);

		addText(24440, "Team", 2, 0xff981f, true, true);
		addText(24441, "Player 1: <col=ff00>100%", 0, 0xff981f, true, true);
		addText(24442, "Player 2: <col=ffff00>75%", 0, 0xff981f, true, true);
		addText(24443, "Player 3: <col=ffb000>50%", 0, 0xff981f, true, true);
		addText(24444, "Player 4: <col=ff0000>25%", 0, 0xff981f, true, true);
		addText(24445, "Player 5: <col=ff00>100%", 0, 0xff981f, true, true);

		tab.totalChildren(6);

		tab.child(0, 24440, 45, 5); // title
		tab.child(1, 24441, 45, 5 + (20 * 1)); // player 1
		tab.child(2, 24442, 45, 5 + (20 * 2)); // player 2
		tab.child(3, 24443, 45, 5 + (20 * 3)); // player 3
		tab.child(4, 24444, 45, 5 + (20 * 4)); // player 4
		tab.child(5, 24445, 45, 5 + (20 * 5)); // player 5

	}

	public static void barbarianAssaultLobby() {
		RSInterface tab = addScreenInterface(23773);

		addText(23774, "Current team", 2, 0xff981f, true, true);
		addText(23775, "Leader:   -----", 1, 0xff981f, true, true);
		addText(23776, "Player 1: -----", 1, 0xff981f, true, true);
		addText(23777, "Player 2: -----", 1, 0xff981f, true, true);
		addText(23778, "Player 3: -----", 1, 0xff981f, true, true);
		addText(23779, "Player 4: -----", 1, 0xff981f, true, true);

		tab.totalChildren(6);

		tab.child(0, 23774, 375, 5); // title
		tab.child(1, 23775, 375, 5 + (20 * 1)); // leader
		tab.child(2, 23776, 375, 5 + (20 * 2)); // player 1
		tab.child(3, 23777, 375, 5 + (20 * 3)); // player 2
		tab.child(4, 23778, 375, 5 + (20 * 4)); // player 3
		tab.child(5, 23779, 375, 5 + (20 * 5)); // player 4

	}

	public static void barrowText() {
		RSInterface tab = addScreenInterface(16128);
		addText(16129, "Barrows Brothers", 2, 0xff981f, true, true);
		addText(16130, "Dharoks", 1, 0xff0000, true, true);
		addText(16131, "Veracs", 1, 0xff0000, true, true);
		addText(16132, "Ahrims", 1, 0xff0000, true, true);
		addText(16133, "Torags", 1, 0xff0000, true, true);
		addText(16134, "Guthans", 1, 0xff0000, true, true);
		addText(16135, "Karils", 1, 0xff0000, true, true);
		addText(16136, "Killcount:", 2, 0xff981f, true, true);
		addText(16137, "#", 1, 0x86B404, true, true);
		tab.totalChildren(9);
		tab.child(0, 16129, 447, 120);
		tab.child(1, 16130, 450, 140);
		tab.child(2, 16131, 450, 155);
		tab.child(3, 16132, 450, 170);
		tab.child(4, 16133, 450, 185);
		tab.child(5, 16134, 450, 200);
		tab.child(6, 16135, 450, 215);
		tab.child(7, 16136, 447, 105);
		tab.child(8, 16137, 485, 105);
	}

	public static void findFreeIds() {
		for (int i = 0; i < RSInterface.interfaceCache.length; i++) {
			RSInterface rsi = RSInterface.interfaceCache[i];

			if (rsi == null) {
				continue;
			}

			System.out.println(i);
		}

	}

	public static void toggles() {
		String[] togglesText = { "Toggle 1", "Toggle 2", "Toggle 3", "Toggle 4", "Toggle 5", "Toggle 6", "Toggle 7",
				"Toggle 8", "Toggle 9" };
		RSInterface tab = addInterface(50060, InterfaceType.MAIN);
		addText(50090, "Gameplay Toggles", 2, 0xff9b00, true, true);
		for (int i = 0; i < 9; i++) {
			int jk = i * 2;
			addRadioButton(50062 + jk, 50061, 108, 108, 14, 15, "Toggle", 1, 4, 544 + i);
			addText((50062 + 1) + jk, togglesText[i], 0, 0xff9b00, false, true);
		}
		tab.totalChildren(19);
		tab.child(0, 50090, 228, 1);
		tab.child(1, 50062, 0, 20 + 5);
		tab.child(2, 50063, 0 + 18, 20 + 5);
		tab.child(3, 50064, 0, 35 + 10);
		tab.child(4, 50065, 0 + 18, 35 + 10);
		tab.child(5, 50066, 0, 50 + 15);
		tab.child(6, 50067, 0 + 18, 50 + 15);
		tab.child(7, 50068, 150, 20 + 5);
		tab.child(8, 50069, 150 + 18, 20 + 5);
		tab.child(9, 50070, 150, 35 + 10);
		tab.child(10, 50071, 150 + 18, 35 + 10);
		tab.child(11, 50072, 150, 50 + 15);
		tab.child(12, 50073, 150 + 18, 50 + 15);
		tab.child(13, 50074, 310, 20 + 5);
		tab.child(14, 50075, 310 + 18, 20 + 5);
		tab.child(15, 50076, 310, 35 + 10);
		tab.child(16, 50077, 310 + 18, 35 + 10);
		tab.child(17, 50078, 310, 50 + 15);
		tab.child(18, 50079, 310 + 18, 50 + 15);
	}

	public static void testScroll() {
		RSInterface tab = addInterface(55000, InterfaceType.MAIN);
		RSInterface scroll = addInterface(55050, InterfaceType.MAIN);
		addSprite(55001, 702);
		addText(55048, "test", 3, 0xff9900, true, true);

		tab.totalChildren(2);
		tab.child(0, 55001, 10, 10);
		tab.child(1, 55050, 50, 67);

		for (int i = 55051; i <= 55150; i++) {
			addTextButton(i, " ", "Show", 0xFF981F, false, true, 0, 90);
		}
		scroll.totalChildren(101);
		scroll.child(0, 55048, 4, 2);
		for (int id = 1, cid = 55051; id <= 100 && cid <= 55150; id++, cid++) {
			scroll.childY[1] = 18;
			scroll.child(id, cid, 9, scroll.childY[id - 1] + 13);
		}
		scroll.width = 168;
		scroll.height = 225;
		scroll.scrollMax = 1320;
	}

	public static void settings() {
		RSInterface tab = addInterface(48050, InterfaceType.MAIN);

		addSprite(48051, 972);
		addButton(48052, 300, 301, "Close");
		addNewText(48053, "Advanced Options", 248, 20, 2, 0xff9900, 0, true, 1, 1, 0);
		addToggleButton(48054, 84, 85, ConfigCodes.DATA_ORBS, 40, 40, "Data orbs");
		addToggleButton(48055, 84, 85, ConfigCodes.ROOF_REMOVAL, 40, 40, "Roof-removal");
		addToggleButton(48056, 84, 85, ConfigCodes.SIDE_STONES_ARRANGEMENT, 40, 40, "Side-stones arrangement");
		addToggleButton(48057, 84, 85, ConfigCodes.TRANSPARENT_SIDE_PANEL, 40, 40, "Transparent side-panel");
		addToggleButton(48058, 84, 85, ConfigCodes.TRANSPARENT_CHATBOX, 40, 40, "Transparent Chatbox");
		addSprite(48059, 1194);
		addSprite(48060, 1193);
		addSprite(48061, 1195);
		addSprite(48062, 1196);
		addSprite(48063, 1197);

		int moveX = 16;
		int moveY = 8;
		tab.totalChildren(13);
		tab.child(0, 48051, 130, 100);
		tab.child(1, 48052, 355, 103);
		tab.child(2, 48053, 130, 100);
		tab.child(3, 48054, 332 - moveX, 122 + moveY);
		tab.child(4, 48055, 293 - (moveX * 2), 122 + moveY);
		tab.child(5, 48056, 254 - (moveX * 3), 122 + moveY);
		tab.child(6, 48057, 215 - (moveX * 4), 122 + moveY);
		tab.child(7, 48058, 215 - (moveX * 4), 161 + (moveY * 2));

		tab.child(8, 48059, 332 - moveX, 122 + moveY);
		tab.child(9, 48060, 293 - (moveX * 2), 122 + moveY);
		tab.child(10, 48061, 254 - (moveX * 3), 122 + moveY);
		tab.child(11, 48062, 215 - (moveX * 4), 122 + moveY);
		tab.child(12, 48063, 215 - (moveX * 4), 161 + (moveY * 2));

	}
	/*
	 * public static void settings() { RSInterface tab = addInterface(48050,
	 * InterfaceType.MAIN); RSInterface scroll = addInterface(48100,
	 * InterfaceType.MAIN); addSprite(48051, 972); addText(48052,
	 * "Graphic Options", 2, 0xff9900, true, true); addHover(48053, 3, 0, 48054,
	 * 300, "Close Window"); addHovered(48054, 301, 48055);
	 * addToggleButton(48056, 973, 974, 900, 50, 40, "Fixed");
	 * addToggleButton(48057, 975, 976, 901, 50, 40, "Resizable");
	 * addToggleButton(48058, 977, 978, 902, 50, 40, "Fullscreen");
	 * addSprite(48059, 979); addToggleButton(48060, 980, 981, 903, 130, 17,
	 * "Toggle"); addSprite(48061, 979); addToggleButton(48062, 980, 981, 904,
	 * 130, 17, "Toggle"); addSprite(48063, 979); addToggleButton(48064, 980,
	 * 981, 905, 130, 17, "Toggle"); addSprite(48065, 979);
	 * addToggleButton(48066, 980, 981, 906, 130, 17, "Toggle");
	 * addSprite(48067, 979); addToggleButton(48068, 980, 981, 907, 130, 17,
	 * "Toggle"); addSprite(48069, 979); addToggleButton(48070, 980, 981, 908,
	 * 130, 17, "Toggle"); addSprite(48071, 979); addToggleButton(48072, 980,
	 * 981, 909, 130, 17, "Toggle"); addSprite(48073, 979);
	 * addToggleButton(48074, 980, 981, 910, 130, 17, "Toggle");
	 * addSprite(48075, 979); addToggleButton(48076, 980, 981, 911, 130, 17,
	 * "Toggle"); addSprite(48077, 979); addToggleButton(48078, 980, 981, 912,
	 * 130, 17, "Toggle"); addSprite(48079, 979); addToggleButton(48080, 980,
	 * 981, 913, 130, 17, "Toggle"); addSprite(48081, 979);
	 * addToggleButton(48082, 980, 981, 914, 130, 17, "Toggle"); addText(48083,
	 * "X10 Hits", 1, 0xFFFFFF, true, true); addText(48084, "562 Hitmarks", 1,
	 * 0xFFFFFF, true, true); addText(48085, "562 HP Bar", 1, 0xFFFFFF, true,
	 * true); addText(48086, "HD Fog", 1, 0xFFFFFF, true, true); addText(48087,
	 * "Display FPS", 1, 0xFFFFFF, true, true); addText(48088, "Particles", 1,
	 * 0xFFFFFF, true, true); addText(48089, "HD Textures", 1, 0xFFFFFF, true,
	 * true); addText(48090, "Shift Drop Items", 1, 0xFFFFFF, true, true);
	 * addText(48091, "Ground Decoration", 1, 0xFFFFFF, true, true);
	 * addText(48092, "Data Orbs", 1, 0xFFFFFF, true, true); addText(48093,
	 * "Roofs", 1, 0xFFFFFF, true, true); addText(48094, "Display Ping", 1,
	 * 0xFFFFFF, true, true); tab.totalChildren(8); tab.child(0, 48051, 30, 80);
	 * tab.child(1, 48100, 36, 173); tab.child(2, 48052, 250, 83); tab.child(3,
	 * 48053, 458, 83); tab.child(4, 48054, 458, 83); tab.child(5, 48056, 150,
	 * 115); tab.child(6, 48057, 230, 115); tab.child(7, 48058, 310, 115);
	 * 
	 * scroll.totalChildren(36); scroll.child(0, 48059, 5, 5); scroll.child(1,
	 * 48060, 8, 8); scroll.child(2, 48061, 5, 30); scroll.child(3, 48062, 8,
	 * 33); scroll.child(4, 48063, 5, 55); scroll.child(5, 48064, 8, 58);
	 * scroll.child(6, 48065, 5, 80); scroll.child(7, 48066, 8, 83);
	 * scroll.child(8, 48067, 145, 5); scroll.child(9, 48068, 148, 8);
	 * scroll.child(10, 48069, 145, 30); scroll.child(11, 48070, 148, 33);
	 * scroll.child(12, 48071, 145, 55); scroll.child(13, 48072, 148, 58);
	 * scroll.child(14, 48073, 145, 80); scroll.child(15, 48074, 148, 83);
	 * scroll.child(16, 48075, 286, 5); scroll.child(17, 48076, 289, 8);
	 * scroll.child(18, 48077, 286, 30); scroll.child(19, 48078, 289, 33);
	 * scroll.child(20, 48079, 286, 55); scroll.child(21, 48080, 289, 58);
	 * scroll.child(22, 48081, 286, 80); scroll.child(23, 48082, 289, 83);
	 * scroll.child(24, 48083, 70, 8); scroll.child(25, 48084, 215, 8);
	 * scroll.child(26, 48085, 355, 8); scroll.child(27, 48086, 70, 33);
	 * scroll.child(28, 48087, 212, 33); scroll.child(29, 48088, 355, 33);
	 * scroll.child(30, 48089, 74, 57); scroll.child(31, 48090, 212, 57);
	 * scroll.child(32, 48091, 359, 57); scroll.child(33, 48092, 74, 82);
	 * scroll.child(34, 48093, 212, 82); scroll.child(35, 48094, 357, 82);
	 * 
	 * /* int configCode = 903; int scrollBoxChildrenSize = 0; for (int i =
	 * 48101; i <= 48115; i++) { addSprite(i, 979); scrollBoxChildrenSize++; }
	 * for (int i = 48116; i <= 48130; i++) { addToggleButton(i, 980, 981,
	 * configCode, 50, 40, "Toggle"); scrollBoxChildrenSize++; configCode++; }
	 * 
	 * int children = scrollBoxChildrenSize+1; scroll.totalChildren(children);
	 * scroll.child(0, 48099, 4, 2); for (int id = 1, cid = 48101; cid <= 48115;
	 * id++, cid++) { scroll.child(id, cid, 6, scroll.childY[id - 1] + 30); }
	 */
	// System.out.println("childrens" + scrollBoxChildrenSize + "");
	/*
	 * scroll.width = 422; scroll.height = 80; scroll.scrollMax = 108; }
	 */

	public static void market() {
		RSInterface tab = addInterface(45000, InterfaceType.MAIN);
		RSInterface scroll = addInterface(45050, InterfaceType.MAIN);
		addSprite(45001, 702);
		addText(45003, "Player Owned Shops", 3, 0xff9900, true, true);
		addHoverButton(45004, 104, "Search Item", 0, 45005, 1);
		addHoveredButton(45005, 105, 45006);
		addText(45007, "Search Item", 2, 0xff9900, true, true);
		addHoverButton(45010, 104, "Search Player", 0, 45011, 1);
		addHoveredButton(45011, 105, 45012);
		addText(45013, "Search Player", 2, 0xff9900, true, true);
		addHoverButton(45014, 292, "Open Shop", 0, 45015, 1);
		addHoveredButton(45015, 293, 45016);
		addText(45017, "Open Shop", 2, 0xff9900, true, true);
		addHoverButton(45018, 75, "Open Your Shop", 0, 45019, 1);
		addHoveredButton(45019, 70, 45020);
		addText(45040, "Open Your Shop", 2, 0xff9900, true, true);
		addSprite(45041, 964);
		addText(45042, "Shop Owner", 2, 0xff9900, true, true);
		addText(45043, "Item Searched", 2, 0xff9900, true, true);
		addText(45045, "Withdraw Money", 0, 0xff9900, true, true);
		addText(45048, "", 0, 0xff9900, true, true);
		addHover(46049, 3, 0, 46050, 300, "Close Window");
		addHovered(46050, 301, 46051);
		addHoverButton(46052, 965, "", 0, 46053, 1);
		addHoveredButton(46053, 407, 46054);
		tab.totalChildren(23);
		tab.child(0, 45001, 10, 10);
		tab.child(1, 45003, 245, 21);
		tab.child(2, 45004, 280, 60);
		tab.child(3, 45005, 280, 60);
		tab.child(4, 45007, 280 + (150 / 2), 65);
		tab.child(5, 45010, 70, 60);
		tab.child(6, 45011, 70, 60);
		tab.child(7, 45013, 70 + (150 / 2), 65);
		tab.child(8, 45014, 33, 265);
		tab.child(9, 45015, 33, 265);
		tab.child(10, 45017, 105, 275);
		tab.child(11, 45018, 430, 260);
		tab.child(12, 45019, 430, 260);
		tab.child(13, 45040, 370, 272);
		tab.child(14, 45041, 33, 95);
		tab.child(15, 45042, 108, 103);
		tab.child(16, 45043, 315, 103);
		tab.child(17, 45045, 250, 260);
		tab.child(18, 45050, 30, 126);
		tab.child(19, 46049, 470, 21);
		tab.child(20, 46050, 470, 21);
		tab.child(21, 46052, 236, 275);
		tab.child(22, 46053, 236, 275);

		int varValue = 1;
		int scrollBoxChildrenSize = 0;
		final int shopButtonsAmount = 20;
		for (int i = 45051; i <= 45070; i++) {
			// addTextButton(i, "hello world :"+i, "Select", 0xFF981F, false,
			// true, tda, 0, 90);
			// addSprite(i, 967);
			addRadioButton(i, 967, 966, 800, 500, 21, "Select", varValue);
			scrollBoxChildrenSize++;
			varValue++;
			addText(i + shopButtonsAmount, "Name:", 1, 0xff9900, false, true);
			scrollBoxChildrenSize++;

			addText(i + shopButtonsAmount * 2, "Price:", 1, 0xff9900, false, true);
			scrollBoxChildrenSize++;

			addText(i + shopButtonsAmount * 3, "Amount:", 1, 0xff9900, false, true);
			scrollBoxChildrenSize++;
		}
		int children = scrollBoxChildrenSize + 1;
		scroll.totalChildren(children);
		scroll.child(0, 45048, 4, 2);
		for (int id = 1, cid = 45051; cid <= 45070; id++, cid++) {
			scroll.child(id, cid, 9, scroll.childY[id - 1] + (id == 1 ? -1 : 21));

			// playername
			id++;
			scroll.child(id, cid + shopButtonsAmount, (id == 2 ? 11 : 12), scroll.childY[id - 1]);

			// Price
			id++;
			scroll.child(id, cid + shopButtonsAmount * 2, (id == 3 ? 151 : 152), scroll.childY[id - 1]);

			// sold amount
			id++;
			scroll.child(id, cid + shopButtonsAmount * 3, (id == 4 ? 297 : 298), scroll.childY[id - 1]);
		}
		scroll.width = 412;
		scroll.height = 121;
		scroll.scrollMax = children * 5 + 14;

	}

	public static void titles() {
		RSInterface tab = addInterface(42000, InterfaceType.MAIN);
		RSInterface scroll = addInterface(42020, InterfaceType.MAIN);
		addSprite(42001, 106);
		addText(42003, "", 3, 0xff9900, true, true);
		addHoverButton(42004, 104, "Confirm Title", 0, 42005, 1);
		addHoveredButton(42005, 105, 42006);
		addText(42007, "Confirm Title", 2, 0xff9900, true, true);
		addHoverButton(42010, 104, "Remove Title", 0, 42011, 1);
		addHoveredButton(42011, 105, 42012);
		addText(42013, "Remove Title", 2, 0xff9900, true, true);
		addHover(42014, 3, 0, 42015, 300, "Close Window");
		addHovered(42015, 301, 42016);
		tab.totalChildren(11);
		tab.child(0, 42001, 0, 0);
		tab.child(1, 42020, 50, 67);
		tab.child(3, 42003, 260, 46);
		tab.child(4, 42004, 270, 290);
		tab.child(5, 42005, 270, 290);
		tab.child(6, 42007, 270 + (150 / 2), 293);
		tab.child(7, 42010, 70, 290);
		tab.child(8, 42011, 70, 290);
		tab.child(9, 42013, 70 + (150 / 2), 293);
		tab.child(2, 42014, 461, 46);
		tab.child(10, 42015, 461, 46);
		loadTitles();
		scroll.totalChildren(titleAmount * 4);
		for (int i = 0; i < (titleAmount * 4); i += 4) {
			int f = 42050 + ((i / 4) * 10);

			int position = -40;

			String line = requirements[i / 4];
			int count = line.length() - line.replace("\n", "").length();
			switch (count) {
			case 1:
				position = -55;
				break;
			case 2:
				position = -85;
				break;
			case 3:
				position = -100;
				break;
			case 4:
				position = -115;
				break;
			case 5:
				position = -130;
				break;
			}

			if ((i / 4) > (titleAmount / 2) - 1) {
				scroll.child(i, f + 1, -15 + getTitleX((i / 4)), position + getTitleY((i / 4)));
				scroll.child(i + 1, f, getTitleX((i / 4)), getTitleY((i / 4)));
				scroll.child(i + 2, f + 4, 5 + getTitleX((i / 4)), 3 + getTitleY((i / 4)));
				scroll.child(i + 3, f + 3, 162 + getTitleX((i / 4)), 3 + getTitleY((i / 4)));
			} else {
				scroll.child(i, f + 1, -15 + getTitleX((i / 4)), position + getTitleY((i / 4)));
				scroll.child(i + 1, f, getTitleX((i / 4)), getTitleY((i / 4)));
				scroll.child(i + 2, f + 4, 5 + getTitleX((i / 4)), 3 + getTitleY((i / 4)));
				scroll.child(i + 3, f + 3, 162 + getTitleX((i / 4)), 3 + getTitleY((i / 4)));
			}
		}
		scroll.scrollMax = (titleAmount / 2) * 29;
		scroll.width = 411;
		scroll.height = 219;
	}

	public static String[] titles = { "The Real", "The Loyal", "The Devoted", "The Famous", "Skiller", "Maximum",
			"The Completionist", "Weakling", "Cowardly", "Sir Lame", "Grumpy", "Overlord", "PKer", "Cheerful",
			"War-Chief", "PK Master", "The Avenger", "Duel Master", "The Explorer", "The Wise", "The Wealthy",
			"Yt'Haar", "Champion", "The Skillful", "Insane", "Master", "Warrior", "Archer", "Magician", "Ironman",
			"The Mighty", "The Faithful", "The Lucky", "Gambler", "The Billionaire", "Lazy", "Rioter", "Clan Leader",
			"Void Knight", "HideNSeeker", "Wingman", "Savage", "King", "Queen", "PVM Master", "Donator", "Super",
			"Supreme", "Legendary", "Uber", "New donator rank", "Lord", "Legend", "Extreme" };

	public static String[] requirements = { "creating an account", "having 4+ days of playtime!",
			"having 10+ days of playtime", "having 20+ days of playtime",
			"obtaining all non-combat skills to\nmaximum level", "obtaining all skills to maximum level",
			"completing the game! you must obtain all\nstats to maximum level, complete\nall quests and complete\nall achievements",
			"dying in a PVP battle", "dying 10 times in a PVP battle", "dying 50 times in a PVP battle",
			"dying 100 times in a PVP battle", "defeating 10 players in PVP", "defeating 50 players in PVP",
			"defeating 100 players in PVP", "defeating 200 players in PVP", "defeating 1000 players in PVP",
			"defeating a player with vengeance", "Winning 100 duels in duel arena",
			"completing all of the achievements", "completing all of the quests",
			"throwing light tax bag into edge well", "defeating the kiln minigame", "defeating all of the champions",
			"achieving a 99 stat", "achieving a 120 stat", "achieving 200m experience in a stat",
			"achieving 200m experience in all\nmelee combat skill", "achieving 200m experience in range",
			"achieving 200m experience in magic", "Playing in Ironman mode",
			"Obtaining all stats to\nmaximum level in Ironman mode", "entering the lottery", "winning the lottery",
			"obtaining a dice bag", "collecting 10 bulging bags\nin your bank",
			"not achieving enough void seal\nin a game of pest control", "winning a game of clan wars",
			"winning a game of clan wars\nas a clan leader", "winning 50 games of pest control",
			"finding 7 penguins in a week", "completing 20 duo slayer tasks", "reaching #1 on PVP boards",
			"defeating the king black dragon\n10 times", "defeating the kalphite queen\n10 times",
			"killing 2000 monsters", "donating at least $10", "donating at least $50", "donating at least $250",
			"donating at least $1000", "donating at least $5000", "New donator rank", "Upgrading your account from normal\nto Lord", "Upgrading your account from Lord\nto Legend",
			"Upgrading your account from Legend\nto Extreme",

	};

	public static int titleAmount = titles.length;

	public static int getTitleX(int id) {
		if (id <= (titleAmount / 2) - 1) {
			return 10;
		} else {
			return 205;
		}
	}

	public static int getTitleY(int id) {
		if (id >= (titleAmount / 2)) {
			id -= titleAmount / 2;
		}
		return 5 + (id * 28);

	}

	public static void loadTitles() {
		for (int i = 0; i < (titleAmount * 10); i += 10) {
			addHoverButton(42050 + i, 107, "Buy", 0, 42051 + (i), 1);
			addSprite(42053 + i, 108);
			addText(42054 + i, titles[(i / 10)], 1, 0xff0000, false, true);
			drawTooltip(42051 + i, "Unlock this title by\n" + requirements[(i / 10)]);
		}
	}

	public static void equipmentTab() {
		removeInterface(15101);
		removeInterface(15102);
		removeInterface(15109);
		removeInterface(15103);
		removeInterface(15104);
		RSInterface rsi = interfaceCache[1644];
		rsi.children[23] = 15101;
		rsi.childX[23] = 40;
		rsi.childY[23] = 205;
		rsi.children[24] = 15102;
		rsi.childX[24] = 110;
		rsi.childY[24] = 205;
		rsi.children[25] = 15109;
		rsi.childX[25] = 39;
		rsi.childY[25] = 240;
		rsi.children[26] = 27650;
		rsi.childX[26] = 0;
		rsi.childY[26] = 0;
		rsi = addInterface(27650);

		addHoverButton(27653, 74, "Show equipment stats", 0, 15264, 1);
		addHoveredButton(15264, 297, 15268);

		addHoverButton(27654, 298, "Show items kept on death", 0, 27657, 1);
		addHoveredButton(27657, 299, 15269);
		
		addHoverButton(27655, 1300, "Show items in toolbelt", 0, 27658, 1);
		addHoveredButton(27658, 1301, 15270);

		rsi.totalChildren(6);
		rsi.child(0, 27653, 28, 205);
		rsi.child(1, 15264, 28, 205);
		rsi.child(2, 27654, 124, 205);
		rsi.child(3, 27657, 124, 205);
		rsi.child(4, 27655, 76, 205);
		rsi.child(5, 27658, 76, 205);
	}

	public static void logout() {
		RSInterface rsinterface = interfaceCache[2449];
		rsinterface.childX[0] = 8;
		rsinterface.childY[0] = 68;
		rsinterface.childX[1] = 10;
		rsinterface.childY[1] = 86;
		rsinterface.childX[2] = 12;
		rsinterface.childY[2] = 104;
		rsinterface.childX[8] = 29;
		rsinterface.childY[8] = 164;
		rsinterface = interfaceCache[2450];
		rsinterface.disabledColor = 0xff981f;
		rsinterface = interfaceCache[2451];
		rsinterface.disabledColor = 0xff981f;
		rsinterface = interfaceCache[2452];
		rsinterface.disabledColor = 0xff981f;
	}

	public static void equipmentScreen() {
		RSInterface Interface = RSInterface.interfaceCache[1644];
		addButton(19144, 70, "Show Equipment Stats");
		Interface.child(23, 19144, 21, 210);
		Interface.child(24, 19145, 40, 210);
		Interface.child(25, 19146, 40, 210);
		Interface.child(26, 19147, 40, 210);
		RSInterface tab = addInterface(15106, InterfaceType.MAIN);
		addSprite(19149, 71);
		addHover(19150, 3, 0, 19151, 72, "Close");
		addHovered(19151, 73, 19152);
		addText(19154, "Equip Your Character...", 0xFF981F, false, true, 52, 2);
		addText(1673, "Attack bonus", 0xFF981F, false, true, 52, 2);
		addText(1674, "Defense bonus", 0xFF981F, false, true, 52, 2);
		addText(1685, "Other bonuses", 0xFF981F, false, true, 52, 2);
		addText(1675, "Stab:", 0xFF981F, false, true, 52, 1);
		addText(1676, "Slash:", 0xFF981F, false, true, 52, 1);
		addText(1677, "Crush:", 0xFF981F, false, true, 52, 1);
		addText(1678, "Magic:", 0xFF981F, false, true, 52, 1);
		addText(1679, "Ranged:", 0xFF981F, false, true, 52, 1);
		addText(1680, "Stab:", 0xFF981F, false, true, 52, 1);
		addText(1681, "Slash:", 0xFF981F, false, true, 52, 1);
		addText(1682, "Crush:", 0xFF981F, false, true, 52, 1);
		addText(1683, "Magic:", 0xFF981F, false, true, 52, 1);
		addText(1684, "Ranged:", 0xFF981F, false, true, 52, 1);
		addText(1686, "Strength:", 0xFF981F, false, true, 52, 1);
		addText(1687, "Prayer:", 0xFF981F, false, true, 52, 1);
		addText(15291, "Range Str:", 0xFF981F, false, true, 52, 1);
		addText(15292, "Magic Boost:", 0xFF981F, false, true, 52, 1);
		addText(19155, "0%", 0xFF981F, false, true, 52, 1);
		addChar(19153);
		
		int offset = 10;
		
		int frame = 0;
		tab.totalChildren(47);
		tab.child(frame, 19149, 12, 20);
		frame++;
		tab.child(frame, 19150, 472, 27);
		frame++;
		tab.child(frame, 19151, 472, 27);
		frame++;
		tab.child(frame, 19153, 170, 190);
		frame++;
		tab.child(frame, 19154, 23, 29);
		frame++;
		tab.child(frame, 1673, 365, 51 - offset); //71
		frame++;
		tab.child(frame, 1674, 365, 143 - offset);
		frame++;
		tab.child(frame, 1675, 372, 65 - offset);
		frame++;
		tab.child(frame, 1676, 372, 79 - offset);
		frame++;
		tab.child(frame, 1677, 372, 93 - offset);
		frame++;
		tab.child(frame, 1678, 372, 107 - offset);
		frame++;
		tab.child(frame, 1679, 372, 121 - offset);
		frame++;
		tab.child(frame, 1680, 372, 157 - offset);
		frame++;
		tab.child(frame, 1681, 372, 171 - offset);
		frame++;
		tab.child(frame, 1682, 372, 185 - offset);
		frame++;
		tab.child(frame, 1683, 372, 199 - offset);
		frame++;
		tab.child(frame, 1684, 372, 213 - offset);
		frame++;
		tab.child(frame, 1685, 365, 233 - offset);
		frame++;
		tab.child(frame, 1686, 372, 247 - offset);
		frame++;
		tab.child(frame, 15291, 372, 261 - offset); //range str
		frame++;
		tab.child(frame, 15292, 372, 275 - offset); //magic str %
		frame++;
		tab.child(frame, 1687, 372, 289 - offset);
		frame++;
		tab.child(frame, 19155, 94, 286);
		frame++;
		tab.child(frame, 1645, 83, 106);
		frame++;
		tab.child(frame, 1646, 83, 135);
		frame++;
		tab.child(frame, 1647, 83, 172);
		frame++;
		tab.child(frame, 1648, 83, 213);
		frame++;
		tab.child(frame, 1649, 27, 185);
		frame++;
		tab.child(frame, 1650, 27, 221);
		frame++;
		tab.child(frame, 1651, 139, 185);
		frame++;
		tab.child(frame, 1652, 139, 221);
		frame++;
		tab.child(frame, 1653, 53, 148);
		frame++;
		tab.child(frame, 1654, 112, 148);
		frame++;
		tab.child(frame, 1655, 63, 109);
		frame++;
		tab.child(frame, 1656, 117, 108);
		frame++;
		tab.child(frame, 1657, 83, 71);
		frame++;
		tab.child(frame, 1658, 42, 110);
		frame++;
		tab.child(frame, 1659, 83, 110);
		frame++;
		tab.child(frame, 1660, 124, 110);
		frame++;
		tab.child(frame, 1661, 27, 149);
		frame++;
		tab.child(frame, 1662, 83, 149);
		frame++;
		tab.child(frame, 1663, 139, 149);
		frame++;
		tab.child(frame, 1664, 83, 189);
		frame++;
		tab.child(frame, 1665, 83, 229);
		frame++;
		tab.child(frame, 1666, 27, 229);
		frame++;
		tab.child(frame, 1667, 139, 229);
		frame++;
		tab.child(frame, 1688, 29, 111);
		frame++;
	}

	public static void emoteTab() {
		RSInterface tab = addInterface(147, InterfaceType.TAB);
		RSInterface scroll = addInterface(148, InterfaceType.TAB);
		tab.totalChildren(1);
		tab.child(0, 148, 0, 3);
		AddInterfaceButton(168, 32, "Yes", 15113, 1, 41, 47);
		addTooltip(15113, "Yes", 41, 47);
		AddInterfaceButton(169, 33, "No", 15115, 1, 41, 47);
		addTooltip(15115, "No", 41, 47);
		AddInterfaceButton(164, 34, "Bow", 15117, 1, 41, 47);
		addTooltip(15117, "Bow", 41, 47);
		AddInterfaceButton(165, 35, "Angry", 15119, 1, 41, 47);
		addTooltip(15119, "Angry", 41, 47);
		AddInterfaceButton(162, 36, "Think", 15121, 1, 41, 47);
		addTooltip(15121, "Think", 41, 47);
		AddInterfaceButton(163, 37, "Wave", 15123, 1, 41, 47);
		addTooltip(15123, "Wave", 41, 47);
		AddInterfaceButton(13370, 38, "Shrug", 15125, 1, 41, 47);
		addTooltip(15125, "Shrug", 41, 47);
		AddInterfaceButton(171, 39, "Cheer", 15127, 1, 41, 47);
		addTooltip(15127, "Cheer", 41, 47);
		AddInterfaceButton(167, 40, "Beckon", 15129, 1, 41, 47);
		addTooltip(15129, "Beckon", 41, 47);
		AddInterfaceButton(170, 41, "Laugh", 15131, 1, 41, 47);
		addTooltip(15131, "Laugh", 41, 47);
		AddInterfaceButton(13366, 42, "Jump for Joy", 15133, 1, 41, 47);
		addTooltip(15133, "Jump for joy", 41, 47);
		AddInterfaceButton(13368, 43, "Yawn", 15135, 1, 41, 47);
		addTooltip(15135, "Yawn", 41, 47);
		AddInterfaceButton(166, 44, "Dance", 15137, 1, 41, 47);
		addTooltip(15137, "Dance", 41, 47);
		AddInterfaceButton(13363, 35, "Jig", 15139, 1, 41, 47);
		addTooltip(15139, "Jig", 41, 47);
		AddInterfaceButton(13364, 36, "Spin", 15141, 1, 41, 47);
		addTooltip(15141, "Spin", 41, 47);
		AddInterfaceButton(13365, 47, "Headbang", 15143, 1, 41, 47);
		addTooltip(15143, "Headbang", 41, 47);
		AddInterfaceButton(161, 48, "Cry", 15145, 1, 41, 47);
		addTooltip(15145, "Cry", 41, 47);
		AddInterfaceButton(11100, 49, "Blow kiss", 15147, 1, 41, 47);
		addTooltip(15147, "Blow kiss", 41, 47);
		AddInterfaceButton(13362, 50, "Panic", 15149, 1, 41, 47);
		addTooltip(15149, "Panic", 41, 47);
		AddInterfaceButton(13367, 51, "Raspberry", 15151, 1, 41, 47);
		addTooltip(15151, "Raspberry", 41, 47);
		AddInterfaceButton(172, 52, "Clap", 15153, 1, 41, 47);
		addTooltip(15153, "Clap", 41, 47);
		AddInterfaceButton(13369, 53, "Salute", 15155, 1, 41, 47);
		addTooltip(15155, "Salute", 41, 47);
		AddInterfaceButton(13383, 54, "Goblin Bow", 15157, 1, 41, 47);
		addTooltip(15157, "Goblin bow", 41, 47);
		AddInterfaceButton(13384, 55, "Goblin Salute", 15159, 1, 41, 47);
		addTooltip(15159, "Goblin salute", 41, 47);
		AddInterfaceButton(667, 56, "Freeze", 15161, 1, 41, 47);
		addTooltip(15161, "Freeze", 41, 47);
		AddInterfaceButton(6503, 57, "Trick", 15163, 1, 41, 47);
		addTooltip(15163, "Trick", 41, 47);
		AddInterfaceButton(6506, 58, "Safety First", 15189, 1, 41, 47);
		addTooltip(15189, "Safety First", 41, 47);
		AddInterfaceButton(666, 59, "Snowman Dance", 15167, 1, 41, 47);
		addTooltip(15167, "Snowman Dance", 41, 47);

		AddInterfaceButton(250, 65, "Explore", 19000, 1, 41, 47);
		addTooltip(19000, "Explore", 41, 47);

		AddInterfaceButton(251, 66, "Stomp", 17181, 1, 41, 47);
		addTooltip(17181, "Stomp", 41, 47);

		AddInterfaceButton(15166, 67, "Flap", 15183, 1, 41, 47);
		addTooltip(15183, "Flap", 41, 47);
		AddInterfaceButton(18686, 68, "Slap head", 15185, 1, 43, 47);
		addTooltip(15185, "Slap head", 41, 47);
		AddInterfaceButton(18689, 60, "Zombie Walk", 15169, 1, 41, 47);
		addTooltip(15169, "Zombie walk", 41, 47);
		AddInterfaceButton(18688, 61, "Zombie Dance", 15171, 1, 41, 47);
		addTooltip(15171, "Zombie dance", 41, 47);
		AddInterfaceButton(18691, 62, "Bunny Hop", 15173, 1, 41, 47);
		addTooltip(15173, "Bunny Hop", 41, 47);
		AddInterfaceButton(18692, 69, "Zombie Hand", 15175, 1, 41, 47);
		addTooltip(15175, "Zombie hand", 41, 47);
		AddInterfaceButton(18687, 63, "Air Guitar", 15177, 1, 41, 47);
		addTooltip(15177, "Air Guitar", 41, 47);
		AddInterfaceButton(154, 64, "SkillCape", 15187, 1, 41, 47);
		addTooltip(15187, "Skillcape", 41, 47);
		scroll.totalChildren(76);
		/** Emotes **/
		scroll.child(0, 168, 7 + 3, 6);
		scroll.child(1, 169, 51 + 3, 6);
		scroll.child(2, 164, 95 + 3, 13);
		scroll.child(3, 165, 134 + 3, 6);// Row 1
		scroll.child(4, 162, 6 + 3, 55);
		scroll.child(5, 163, 45 + 3, 55);
		scroll.child(6, 13370, 92 + 3, 55);
		scroll.child(7, 171, 134 + 3, 55);// Row 2
		scroll.child(8, 167, 4 + 3, 104);
		scroll.child(9, 170, 48 + 3, 104);
		scroll.child(10, 13366, 92 + 3, 103);
		scroll.child(11, 13368, 136 + 3, 104);// Row 3
		scroll.child(12, 166, 3 + 3, 153);
		scroll.child(13, 13363, 47 + 3, 153);
		scroll.child(14, 13364, 87 + 3, 153);
		scroll.child(15, 13365, 132 + 3, 153);// Row 4
		scroll.child(16, 161, 5 + 3, 203);
		scroll.child(17, 11100, 48 + 3, 202);
		scroll.child(18, 13362, 96 + 3, 203);
		scroll.child(19, 13367, 134 + 3, 202);// Row 5
		scroll.child(20, 172, 7 + 3, 250);
		scroll.child(21, 13369, 50 + 3, 250);
		scroll.child(22, 13383, 85 + 3, 255);
		scroll.child(23, 13384, 135 + 3, 249);// Row 6
		scroll.child(24, 667, 0 + 3, 300);
		scroll.child(25, 6503, 49 + 3, 299);
		scroll.child(26, 6506, 93 + 3, 299);
		scroll.child(27, 666, 138 + 3, 299);// Row 7
		scroll.child(28, 250, 2 + 3, 349);
		scroll.child(29, 251, 50 + 3, 350);
		scroll.child(30, 15166, 85 + 3, 352);
		scroll.child(31, 18686, 139 + 3, 350);// Row 8
		scroll.child(32, 18687, 9 + 3, 452);
		scroll.child(33, 18689, 7 + 3, 401);
		scroll.child(34, 18688, 48 + 3, 402);
		scroll.child(35, 18692, 85 + 3, 402);// Row 9
		scroll.child(36, 18691, 136 + 3, 406);
		scroll.child(37, 154, 46 + 3, 450);// Row 10
		/** ToolTips **/
		scroll.child(38, 15113, 0, 58);
		scroll.child(39, 15115, 44, 58);
		scroll.child(40, 15117, 88, 58);
		scroll.child(41, 15119, 132, 58);// Row 1
		scroll.child(42, 15121, 0, 107);
		scroll.child(43, 15123, 44, 107);
		scroll.child(44, 15125, 88, 107);
		scroll.child(45, 15127, 132, 107);// Row 2
		scroll.child(46, 15129, 0, 156);
		scroll.child(47, 15131, 44, 156);
		scroll.child(48, 15133, 88, 156);
		scroll.child(49, 15135, 132, 156);// Row 3
		scroll.child(50, 15137, 0, 205);
		scroll.child(51, 15139, 44, 205);
		scroll.child(52, 15141, 88, 205);
		scroll.child(53, 15143, 113, 205);// Row 4
		scroll.child(54, 15145, 2, 178);
		scroll.child(55, 15147, 44, 179);
		scroll.child(56, 15149, 88, 179);
		scroll.child(57, 15151, 107, 179);// Row 5
		scroll.child(58, 15153, 0, 227);
		scroll.child(59, 15155, 44, 227);
		scroll.child(60, 15157, 88, 227);
		scroll.child(61, 15159, 93, 227);// Row 6
		scroll.child(62, 15161, 2, 276);
		scroll.child(63, 15163, 44, 276);
		scroll.child(64, 15189, 88, 276);
		scroll.child(65, 15167, 80, 276);// Row 7
		scroll.child(66, 15169, 0, 374);
		scroll.child(67, 15171, 44, 374);
		scroll.child(68, 15175, 88, 374);
		scroll.child(69, 15173, 110, 374);// Row 8
		// scroll.child(70, 18000, 0, 325); sharp eye in emotes tab???
		scroll.child(70, 17181, 44, 325);
		scroll.child(71, 15183, 88, 325);
		scroll.child(72, 15185, 112, 325);// Row 9
		scroll.child(73, 15177, 0, 423);
		scroll.child(74, 15187, 44, 423);// Row 10
		scroll.child(75, 19000, 0, 325);
		scroll.width = 173;
		scroll.height = 255;
		scroll.scrollMax = 500;
	}

	public static void statisticTab() {
		RSInterface tab = addInterface(638, InterfaceType.TAB);
		RSInterface scroll = addInterface(16025, InterfaceType.TAB);
		addText(640, Config.SERVER_NAME, 2, 0xeb981f, false, true);
		addText(663, "", 0, 0xff9900, false, true);
		addText(682, "", 2, 0xff9900, false, true);
		addSprite(16022, 30);
		// addText(16023, "Quest Points: ---", 0, 0xeb981f, false, true);
		addSprite(16024, 31);
		tab.totalChildren(8);
		tab.child(0, 640, 5, 5);
		tab.child(1, 16024, 0, 25);
		tab.child(2, 16025, 6, 24);
		tab.child(3, 16022, 0, 22);
		tab.child(4, 16022, 0, 249);
		tab.child(5, 62102, 150, 2);
		tab.child(6, 62103, 168, 2);
		tab.child(7, 62105, 132, 2);
		
		for (int i = 16026; i <= 16125; i++) {
			addTextButton(i, " ", "Show", 0xFF981F, false, true, 0, 90);
		}
		scroll.totalChildren(101);
		scroll.child(0, 663, 4, 2);
		for (int id = 1, cid = 16026; id <= 100 && cid <= 16125; id++, cid++) {
			scroll.childY[1] = 18;
			scroll.child(id, cid, 9, scroll.childY[id - 1] + 13);
		}
		scroll.width = 168;
		scroll.height = 225;
		scroll.scrollMax = 1320;
	}

	public static void questTab() {
		RSInterface tab = addInterface(62100, InterfaceType.TAB);
		RSInterface scroll = addInterface(62120, InterfaceType.TAB);
		addText(62101, "Quest Points: 0", 2, 0xeb981f, false, true);
		addButton(62105, 1245, 1246, "View Tasks");
		addButton(62102, 824, 825, "View Quests");
		addButton(62103, 826, 827, "View Player Statistics");

		tab.totalChildren(8);
		tab.child(0, 62101, 5, 5);
		tab.child(1, 16024, 0, 25);
		tab.child(2, 62120, 6, 24);
		tab.child(3, 16022, 0, 22);
		tab.child(4, 16022, 0, 249);
		tab.child(5, 62102, 150, 2);
		tab.child(6, 62103, 168, 2);
		tab.child(7, 62105, 132, 2);

		addText(62104, " SoulPlay Quests", 2, 0xeb981f, false, true);

		String[] quests = { "Cook's Assistant", "Mastering Enhancement" };
		for (int i = 0; i < 2; i++) {
			String questText = "";
			int questFont = 0;
			if (i < quests.length) {
				questText = quests[i];
			}
			String option = "Read Journal <col=ff9040>" + questText;
			addTextButtonLesik(62122 + i, questText, option, 0xff0000, false, true, questFont);
		}
		scroll.scrollMax = 1635;
		scroll.totalChildren(3);
		scroll.child(0, 62104, 4, 2);
		for (int s = 0; s < 2; s++) {
			scroll.child(1 + s, 62122 + s, 9, 20 + (s * 13));
		}

		scroll.width = 168;
		scroll.height = 225;
		// scroll.scrollMax = 1320;
	}

	public static void achievement() {
		RSInterface tab = addInterface(29265, InterfaceType.TAB);
		RSInterface list = addInterface(28025, InterfaceType.TAB);
		addText(28640, "Achievements", 2, 0xeb981f, false, true);
		addText(28641, " Easy Tasks", 2, 0xeb981f, false, true);
		addText(28642, "", 1, 0xff9900, false, true);

		addSprite(28022, 30);
		addText(28023, "Achievements Completed: 0/115", 0, 0xeb981f, false, true);
		addSprite(28024, 31);
		tab.totalChildren(6);
		tab.child(0, 28640, 5, 5);
		tab.child(1, 28024, 0, 25);
		tab.child(2, 28025, 6, 24);
		tab.child(3, 28022, 0, 22);
		tab.child(4, 28022, 0, 249);
		tab.child(5, 28023, 4, 251);
		String[] achievementsss = { "Set A Bank Pin", "Kill 20 Rock Crabs", "Bury 20 Bones", "Set Up A Cannon",
				"Pick 30 Flax", "Light 20 Fires", "Fish 30 Fish", "Complete A Slayertask", "Steal 50 Times",
				"Chop 30 Logs", "Mine 30 Ores", "Cook 30 Fish", "Burn A Fish", "Craft 20 Strength Amulets",
				"Farm A Herb", "Eat 20 Lobsters", "Perform A Special Attack", "Summon A Spirit Spider",
				"Make 15 Spirit Wolf Pouches", "Smelt 10 Bronze Bars", "Complete All Frozen Floors",
				"Craft 100 Air Runes", "Complete 10 Gnome Laps", "Defeat A Player In Wildy", "Change Appearance",
				"Vote For SP", "", "Medium Tasks", "", "Kill 500 Monsters", "Kill A Revenant",
				"Win A Fight In Duel Arena", "Summon 20 Granite Crabs", "Catch 75 Red Cinchompa", "Thiev 300000 Coins",
				"Kill A Frost Dragon", "Kill 20 Dragons", "Bury 100 Dragon Bones", "Get A Level 99 Skill",
				"Fish 200 Sharks", "Cook 100 Manta Rays", "Mine 200 Adamant Ores", "Chop 200 Yew Logs",
				"Craft 50 Phoenix Necks", "Craft 200 Law Runes", "Complete A Duo SlayerTask",
				"Complete A Task From Sumona", "Defeat 10 Players In Wildy", "Defeat 3 Players Without Dying",
				"Die 20 Times", "Participate In The Lottery", "Complete 10 Barbarian Lap", "Vote 10 Times For SP", "",
				"Hard Tasks", "", "Bury 25 Frost Bones", "Fish 500 Manta Rays", "Cook 500 Rocktails",
				"Mine 100 Rune Ores", "Smelt 50 Rune Ores", "Chop 500 Magic Logs", "Light 200 Magic Logs",
				"Thiev 10000000 Coins", "Craft 20 Amulets Of Fury", "Craft 10000 Runes", "Catch 100 Grenwalls",
				"Build A Gilded Altar", "Win 10 Games Of Pest Control", "Make 10 Overloads", "Defeat The Jad",
				"Kill The Chaos Elemental", "Find A Dark Bow", "Smith A Rune Platebody", "Exchange An Artifact",
				"Find A Dragon Chainbody", "Complete A Task From Kuradal", "Complete All Furnished Floors",
				"Hit 70+ With Magic", "Cast A DFS Special", "Earn 2000 PK Points", "Defeat 15 Players Without Dying",
				"Get Atleast 1400 Elo Rating", "Duel Atleast 50 Times", "Kill King Black Dragon",
				"Kill 5 Tormented Demons", "Vote 50 Times For SP", "", "Elite Tasks", "", "Kill 10000 Monsters",
				"Mine 300 Rune Ores", "Catch 500 Grenwalls", "Catch 500 Rocktails", "Craft 500 Black D'hide Pieces",
				"Win 50 Games Of Pest Control", "Defeat 60 Players In Wildy", "Kill A Player With Vengeance",
				"Defeat Your Target In Wildy", "Obtain a 20 killstreak", "Get Atleast 1600 Elo Rating",
				"Win A Game of Last Man Standing", "Open 50 Crystal Chests", "Defeat Nex 10 Times", "Defeat Corp Beast 20 Times",
				"Defeat 35 GWD Bosses", "Kill 150 Tormented Demons", "Complete Floor 60 C6 Large",
				"Defeat 40 Dagannoth Bosses", "Defeat Avatar Of Destruction", "Defeat WildyWyrm",
				"Get Progress 60 In Dung", "Complete 50 Warped Floors", "Earn 200 Slayer Points",
				"Earn 20 Duo Points", "Fire 5000 Cannon Balls", "Find A Dragon Pickaxe", "Build A Greater Magic Cage",
				"Obtain A Tokhaar-kal", "Build A Demonic Throne", "Make 600 Steel Titan Pouches", "Heal 50000 HP",
				"Hit 90+ With Melee", "Vote 250 Times For SP", "Chop 500 Mahogany Logs", "Earn 100% Tai Bwo Favour",
				"Complete 20 large floors", "Complete 100 Dung Puzzles", "Mine 500 Promethium Ores", "Fish 500 Raw Cave Moray",
				"Chop 500 Grave Creeper", "Pick 300 Spiritblooms", "Make 50 Strong Gatherer Pots", "Make 50 Strong Artisan Pots",
				"Make 50 Strong Natural Pots", "Make 50 Strong Survival Pots", "Capture At Least 5 Pets", "Level A Pet To Level 10",
				"Participate In 30 LMS Games",};
		for (int i = 0; i < 150; i++) {
			String textzzz = "";
			String option = "Current Progress";
			int fontss = 0;
			int color = 0xff0000;
			if (i < achievementsss.length) {
				textzzz = achievementsss[i];
			}
			if (i == 27 || i == 54 || i == 88) { // medium, hard, elite task
													// seperators
				fontss = 2;
				option = null;
				color = 0xff9040;
			}
			addTextButtonLesik(28030 + i, textzzz, option, color, false, true, fontss);
		}
		list.scrollMax = 1830;
		list.totalChildren(151);
		list.child(0, 28641, 4, 2);
		for (int s = 0; s < 150; s++) {
			list.child(1 + s, 28030 + s, 9, 20 + (s * 13));
		}
		list.width = 168;
		list.height = 225;
	}

	public static void friendsTab() {
		RSInterface tab = addInterface(5065, InterfaceType.TAB);
		tab.totalChildren(12);

		addText(5067, "Friends List", 1, 0xff9933, true, true);
		tab.child(0, 5067, 95, 4);

		addSprite(16127, 0);
		tab.child(1, 16127, 0, 25);

		addSprite(16126, 1);
		tab.child(2, 16126, 0, 221);
		tab.child(3, 16126, 0, 22);

		RSInterface list = interfaceCache[5066];
		tab.child(4, 5066, 0, 24);

		addHoverButton(5068, 2, "Add Friend", 201, 5072, 1);
		tab.child(5, 5068, 15, 226);

		addHoveredButton(5072, 3, 5073);
		tab.child(6, 5072, 15, 226);

		addHoverButton(5069, 2, "Delete Friend", 202, 5074, 1);
		tab.child(7, 5069, 103, 226);

		addHoveredButton(5074, 3, 5075);
		tab.child(8, 5074, 103, 226);

		addText(5070, "Add Friend", 0, 0xff9933, false, true);
		tab.child(9, 5070, 25, 237);

		addText(5071, "Delete Friend", 0, 0xff9933, false, true);
		tab.child(10, 5071, 106, 237);
		
		addButton(5076, 1261, 1260, "View Ignore List");
		tab.child(11, 5076, 165, 3);

		list.height = 196;
		list.width = 174;
		for (int id = 5092, i = 0; id <= 5191 && i <= 99; id++, i++) {
			list.children[i] = id;
			list.childX[i] = 3;
			list.childY[i] = (short) (list.childY[i] - 7);
		}

		for (int id = 5192, i = 100; id <= 5291 && i <= 199; id++, i++) {
			list.children[i] = id;
			list.childX[i] = 131;
			list.childY[i] = (short) (list.childY[i] - 7);
		}
	}

	public static void ignoreTab() {
		RSInterface tab = addInterface(5715, InterfaceType.TAB);
		RSInterface list = interfaceCache[5716];
		addText(5717, "Ignore List", 1, 0xff9933, true, true);
		addText(5720, "Add Name", 0, 0xff9933, false, true);
		addText(5721, "Delete Name", 0, 0xff9933, false, true);
		addHoverButton(5718, 2, "Add Name", 501, 5722, 1);
		addHoveredButton(5722, 3, 5723);
		addHoverButton(5719, 2, "Delete Name", 502, 5724, 1);
		addHoveredButton(5724, 3, 5725);
		addButton(5726, 1263, 1262, "View Friends List");
		tab.totalChildren(12);
		tab.child(0, 5717, 95, 4);
		tab.child(1, 16127, 0, 25);
		tab.child(2, 16126, 0, 221);
		tab.child(3, 5716, 0, 24);
		tab.child(4, 16126, 0, 22);
		tab.child(5, 5718, 15, 226);
		tab.child(6, 5722, 15, 226);
		tab.child(7, 5719, 103, 226);
		tab.child(8, 5724, 103, 226);
		tab.child(9, 5720, 27, 237);
		tab.child(10, 5721, 108, 237);
		tab.child(11, 5726, 165, 3);
		list.height = 196;
		list.width = 174;
		for (int id = 5742, i = 0; id <= 5841 && i <= 99; id++, i++) {
			list.children[i] = id;
			list.childX[i] = 3;
			list.childY[i] = (short) (list.childY[i] - 7);
		}

	}

	public static void skillTab602() {
		RSInterface rsinterface = addInterface(31110);
		//rsinterface.totalChildren(108);
		rsinterface.totalChildren(104);
		AddInterfaceButtons(31111, 4, "View <col=ffb000>Attack</col> guide", 31112, 1);
		drawTooltip(31112, "EXP: 14000000\nNext level at");
		addNewText(31114, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31115, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31116, 5, "View <col=ffb000>Strength</col> guide", 31117, 1);
		drawTooltip(31117, "EXP: 14000000");
		addNewText(31119, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31120, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31121, 6, "View <col=ffb000>Defence</col> guide", 31122, 1);
		drawTooltip(31122, "EXP: 14000000");
		addNewText(31124, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31125, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31126, 7, "View <col=ffb000>Ranged</col> guide", 31127, 1);
		drawTooltip(31127, "EXP: 14000000");
		addNewText(31129, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31130, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31131, 8, "View <col=ffb000>Prayer</col> guide", 31132, 1);
		drawTooltip(31132, "EXP: 14000000");
		addNewText(31134, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31135, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31136, 9, "View <col=ffb000>Magic</col> guide", 31137, 1);
		drawTooltip(31137, "EXP: 14000000");
		addNewText(31139, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31140, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31141, 10, "View <col=ffb000>Runecrafting</col> guide", 31142, 1);
		drawTooltip(31142, "EXP: 14000000");
		addNewText(31144, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31145, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31146, 11, "View <col=ffb000>Construction</col> guide", 31147, 1);
		drawTooltip(31147, "EXP: 1337");
		addNewText(31149, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31150, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31151, 12, "View <col=ffb000>Dungeoneering</col> guide", 31152, 1);
		drawTooltip(31152, "EXP: 1337");
		addNewText(31154, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31155, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31156, 13, "View <col=ffb000>Hitpoints</col> guide", 31157, 1);
		drawTooltip(31157, "EXP: 14000000");
		addNewText(31159, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31160, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31161, 14, "View <col=ffb000>Agility</col> guide", 31162, 1);
		drawTooltip(31162, "EXP: 14000000");
		addNewText(31164, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31165, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31166, 15, "View <col=ffb000>Herblore</col> guide", 31167, 1);
		drawTooltip(31167, "EXP: 14000000");
		addNewText(31169, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31170, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31171, 16, "View <col=ffb000>Thieving</col> guide", 31172, 1);
		drawTooltip(31172, "EXP: 14000000");
		addNewText(31174, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31175, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31176, 17, "View <col=ffb000>Crafting</col> guide", 31177, 1);
		drawTooltip(31177, "EXP: 14000000");
		addNewText(31179, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31180, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31181, 18, "View <col=ffb000>Fletching</col> guide", 31182, 1);
		drawTooltip(31182, "EXP: 14000000");
		addNewText(31184, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31185, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31186, 19, "View <col=ffb000>Slayer</col> guide", 31187, 1);
		drawTooltip(31187, "EXP: 14000000");
		addNewText(31189, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31190, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31191, 20, "View <col=ffb000>Hunter</col> guide", 31192, 1);
		drawTooltip(31192, "EXP: 14000000");
		addNewText(31194, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31195, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		//AddInterfaceButtons(31196, 1259/*21*/, null, 31197, 0);
		AddInterfaceButtons(31196, 21, null, 31197, 0);
		drawTooltip(31197, "Total EXP: 350000000");
		//addText(31199, "Total Level:", 0xffff00, false, true, 52, 0);
		//addText(31200, "2475", 0xffff00, false, true, 52, 0);
		addText(31199, "Total Lvl:", 0xffff00, false, true, 52, 0);
		addText(31200, "2595", 0xffff00, false, true, 52, 0);
		AddInterfaceButtons(31201, 22, "View <col=ffb000>Mining</col> guide", 31202, 1);
		drawTooltip(31202, "EXP: 14000000");
		addNewText(31204, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31205, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31206, 23, "View <col=ffb000>Smithing</col> guide", 31207, 1);
		drawTooltip(31207, "EXP: 14000000");
		addNewText(31209, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31210, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31211, 24, "View <col=ffb000>Fishing</col> guide", 31212, 1);
		drawTooltip(31212, "EXP: 14000000");
		addNewText(31214, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31215, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31216, 25, "View <col=ffb000>Cooking</col> guide", 31217, 1);
		drawTooltip(31217, "EXP: 14000000");
		addNewText(31219, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31220, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31221, 26, "View <col=ffb000>Firemaking</col> guide", 31222, 1);
		drawTooltip(31222, "EXP: 14000000");
		addNewText(31224, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31225, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31226, 27, "View <col=ffb000>Woodcutting</col> guide", 31227, 1);
		drawTooltip(31227, "EXP: 14000000");
		addNewText(31229, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31230, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31231, 28, "View <col=ffb000>Farming</col> guide", 31232, 1);
		drawTooltip(31232, "EXP: 14000000");
		addNewText(31234, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31235, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		AddInterfaceButtons(31236, 29, "View <col=ffb000>Summoning</col> guide", 31237, 1);
		drawTooltip(31237, "EXP: 14000000");
		addNewText(31239, "99", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		addNewText(31240, "99", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		//AddInterfaceButtons(31241, 1258, "View <col=ffb000>Warding</col> guide", 31242, 1);
		//drawTooltip(31242, "EXP: 14000000");
		//addNewText(31244, "1", 10, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		//addNewText(31245, "1", 13, 11, 0, 0xffff00, 0, true, 1, 1, 0);
		rsinterface.child(15, 31111, 3, 3);
		rsinterface.child(35, 31114, 31, 6);
		rsinterface.child(31, 31115, 43, 18);
		rsinterface.child(98, 31112, 3, 31);
		rsinterface.child(23, 31116, 3, 31);
		rsinterface.child(97, 31119, 31, 34);
		rsinterface.child(85, 31120, 43, 46);
		rsinterface.child(99, 31117, 3, 59);
		rsinterface.child(92, 31121, 3, 59);
		rsinterface.child(93, 31124, 31, 62);
		rsinterface.child(94, 31125, 43, 74);
		rsinterface.child(95, 31122, 3, 87);
		rsinterface.child(19, 31126, 3, 87);
		rsinterface.child(89, 31129, 31, 90);
		rsinterface.child(90, 31130, 43, 102);
		rsinterface.child(91, 31127, 3, 115);
		rsinterface.child(11, 31131, 3, 115);
		rsinterface.child(27, 31134, 31, 118);
		rsinterface.child(86, 31135, 43, 130);
		rsinterface.child(87, 31132, 3, 143);
		rsinterface.child(80, 31136, 3, 143);
		rsinterface.child(81, 31139, 31, 146);
		rsinterface.child(82, 31140, 43, 158);
		rsinterface.child(83, 31137, 3, 171);
		rsinterface.child(76, 31141, 3, 171);
		rsinterface.child(77, 31144, 31, 174);
		rsinterface.child(78, 31145, 43, 186);
		rsinterface.child(79, 31142, 3, 199);
		rsinterface.child(72, 31146, 3, 199);
		rsinterface.child(73, 31149, 31, 202);
		rsinterface.child(74, 31150, 43, 214);
		rsinterface.child(75, 31147, 3, 227);
		rsinterface.child(68, 31151, 3, 227);
		rsinterface.child(69, 31154, 31, 230);
		rsinterface.child(70, 31155, 43, 242);
		rsinterface.child(71, 31152, 3, 255);
		rsinterface.child(64, 31156, 64, 3);
		rsinterface.child(65, 31159, 92, 6);
		rsinterface.child(66, 31160, 104, 18);
		rsinterface.child(67, 31157, 64, 31);
		rsinterface.child(60, 31161, 64, 31);
		rsinterface.child(61, 31164, 92, 34);
		rsinterface.child(62, 31165, 104, 46);
		rsinterface.child(63, 31162, 64, 59);
		rsinterface.child(56, 31166, 64, 59);
		rsinterface.child(57, 31169, 92, 62);
		rsinterface.child(58, 31170, 104, 74);
		rsinterface.child(59, 31167, 64, 87);
		rsinterface.child(52, 31171, 64, 87);
		rsinterface.child(53, 31174, 92, 90);
		rsinterface.child(54, 31175, 104, 102);
		rsinterface.child(55, 31172, 64, 115);
		rsinterface.child(48, 31176, 64, 115);
		rsinterface.child(49, 31179, 92, 118);
		rsinterface.child(50, 31180, 104, 130);
		rsinterface.child(51, 31177, 64, 143);
		rsinterface.child(44, 31181, 64, 143);
		rsinterface.child(45, 31184, 92, 146);
		rsinterface.child(46, 31185, 104, 158);
		rsinterface.child(47, 31182, 64, 171);
		rsinterface.child(40, 31186, 64, 171);
		rsinterface.child(41, 31189, 92, 174);
		rsinterface.child(42, 31190, 104, 186);
		rsinterface.child(43, 31187, 64, 199);
		rsinterface.child(36, 31191, 64, 199);
		rsinterface.child(37, 31194, 92, 202);
		rsinterface.child(38, 31195, 104, 214);
		rsinterface.child(39, 31192, 64, 227);
		rsinterface.child(32, 31201, 125, 3);
		rsinterface.child(33, 31204, 153, 6);
		rsinterface.child(34, 31205, 165, 18);
		rsinterface.child(101, 31202, 90, 31);
		rsinterface.child(28, 31206, 125, 31);
		rsinterface.child(29, 31209, 153, 34);
		rsinterface.child(30, 31210, 165, 46);
		rsinterface.child(102, 31207, 90, 59);
		rsinterface.child(24, 31211, 125, 59);
		rsinterface.child(25, 31214, 153, 62);
		rsinterface.child(26, 31215, 165, 74);
		rsinterface.child(103, 31212, 90, 87);
		rsinterface.child(20, 31216, 125, 87);
		rsinterface.child(21, 31219, 153, 90);
		rsinterface.child(22, 31220, 165, 102);
		rsinterface.child(96, 31217, 90, 115);
		rsinterface.child(16, 31221, 125, 115);
		rsinterface.child(17, 31224, 153, 118);
		rsinterface.child(18, 31225, 165, 130);
		rsinterface.child(88, 31222, 90, 143);
		rsinterface.child(12, 31226, 125, 143);
		rsinterface.child(13, 31229, 153, 146);
		rsinterface.child(14, 31230, 165, 158);
		rsinterface.child(100, 31227, 90, 171);
		rsinterface.child(8, 31231, 125, 171);
		rsinterface.child(9, 31234, 153, 174);
		rsinterface.child(10, 31235, 165, 186);
		rsinterface.child(84, 31232, 90, 199);
		rsinterface.child(4, 31236, 125, 199);
		rsinterface.child(5, 31239, 153, 202);
		rsinterface.child(6, 31240, 165, 214);
		rsinterface.child(7, 31237, 90, 227);
		/*rsinterface.child(0, 31196, 125, 227); //total level
		rsinterface.child(1, 31199, 133, 229);
		rsinterface.child(2, 31200, 141, 241)*/
		rsinterface.child(0, 31196, 64, 227); //total level
		rsinterface.child(1, 31199, 105, 229);
		rsinterface.child(2, 31200, 116, 241);
		rsinterface.child(3, 31197, 64, 255);
		//warding skill
		/*rsinterface.child(104, 31241, 64, 227);
		rsinterface.child(105, 31244, 92, 230);
		rsinterface.child(106, 31245, 104, 242);
		rsinterface.child(107, 31242, 64, 255);*/
	}

	public static void quickPrayers() {
		int i = 0;
		RSInterface localRSInterface = addInterface(17200, InterfaceType.TAB);
		addSprite(17201, 985);
		addText(17240, "Select your quick prayers:", 0, 16750623, false, true);
		int j = 17202;

		addHoverButton(17231, 986, 4, 190, 24, "Confirm Selection", -1, 17232, 1);
		addHoveredButton(17232, 987, 17243);

		for (int k = 630; j <= 17234 || k <= 656; k++) {
			// if (RSInterface.interfaceCache[j] != null) {
			// j++;
			// continue;
			// }
			if (j == 17231 || j == 17232) {
				j++;
				continue;
			}
			addRadioButton(j, 17200, 984, 983, 37, 37, "Select", 0, 1, k);
			j++;
		}

		localRSInterface.totalChildren(65);

		localRSInterface.child(i++, 5632, 8, 28);
		localRSInterface.child(i++, 5633, 44, 28);
		localRSInterface.child(i++, 5634, 79, 28);
		localRSInterface.child(i++, 18001, 116, 28);
		localRSInterface.child(i++, 18003, 153, 28);

		localRSInterface.child(i++, 5635, 8, 63);
		localRSInterface.child(i++, 5636, 44, 63);
		localRSInterface.child(i++, 5637, 79, 63);
		localRSInterface.child(i++, 5638, 116, 63);
		localRSInterface.child(i++, 5639, 153, 63);

		localRSInterface.child(i++, 5640, 8, 100);
		localRSInterface.child(i++, 18005, 44, 100);
		localRSInterface.child(i++, 18007, 79, 100);
		localRSInterface.child(i++, 5641, 116, 100);
		localRSInterface.child(i++, 5642, 153, 100);

		localRSInterface.child(i++, 5643, 8, 135);
		localRSInterface.child(i++, 17901, 44, 135);
		localRSInterface.child(i++, 5644, 79, 135);
		localRSInterface.child(i++, 686, 116, 135);
		localRSInterface.child(i++, 5645, 153, 135);

		localRSInterface.child(i++, 18009, 8, 170);
		localRSInterface.child(i++, 18011, 44, 170);
		localRSInterface.child(i++, 5649, 79, 170);
		localRSInterface.child(i++, 5647, 116, 170);
		localRSInterface.child(i++, 5648, 153, 170);

		localRSInterface.child(i++, 18013, 14, 205);
		localRSInterface.child(i++, 17907, 44, 209);
		localRSInterface.child(i++, 18015, 79, 215);
		localRSInterface.child(i++, 17903, 118, 209);
		localRSInterface.child(i++, 17905, 153, 208);

		localRSInterface.child(i++, 17201, 0, 22);// Bars
		localRSInterface.child(i++, 17201, 0, 237);// Bars

		localRSInterface.child(i++, 17202, 8, 25);
		localRSInterface.child(i++, 17203, 42, 25);
		localRSInterface.child(i++, 17204, 79, 25);
		localRSInterface.child(i++, 17205, 113, 25);
		localRSInterface.child(i++, 17206, 152, 25);

		localRSInterface.child(i++, 17207, 8, 60);
		localRSInterface.child(i++, 17208, 42, 60);
		localRSInterface.child(i++, 17209, 79, 60);
		localRSInterface.child(i++, 17210, 113, 60);
		localRSInterface.child(i++, 17211, 152, 60);

		localRSInterface.child(i++, 17212, 8, 95);
		localRSInterface.child(i++, 17213, 42, 95);
		localRSInterface.child(i++, 17214, 79, 95);
		localRSInterface.child(i++, 17215, 113, 95);
		localRSInterface.child(i++, 17216, 152, 95);

		localRSInterface.child(i++, 17217, 8, 132);
		localRSInterface.child(i++, 17218, 42, 132);
		localRSInterface.child(i++, 17219, 79, 132);
		localRSInterface.child(i++, 17220, 113, 132);
		localRSInterface.child(i++, 17221, 152, 132);

		localRSInterface.child(i++, 17222, 8, 165);
		localRSInterface.child(i++, 17223, 42, 165);
		localRSInterface.child(i++, 17224, 79, 165);
		localRSInterface.child(i++, 17225, 113, 165);
		localRSInterface.child(i++, 17226, 152, 165);

		localRSInterface.child(i++, 17227, 8, 204);
		localRSInterface.child(i++, 17228, 42, 204);
		localRSInterface.child(i++, 17229, 79, 204);
		localRSInterface.child(i++, 17230, 113, 204);
		localRSInterface.child(i++, 17234, 152, 204);

		localRSInterface.child(i++, 17240, 5, 5);
		localRSInterface.child(i++, 17231, 0, 237);
		localRSInterface.child(i++, 17232, 0, 237);
	}

	public static void quickCurses() {
		int frame = 0;
		RSInterface tab = addTabInterface(21000);
		addText(17235, "Select your quick curses:", 0, 16750623, false, true);
		// int j = 17202;
		// for (int k = 630; (j <= 17222) || (k <= 656); k++) {
		// addConfigButton(j, 17200, 984, 983, 33, 33, "Select", 0, 1, k);
		// j++;
		// }
		tab.totalChildren(45);
		tab.child(frame++, 22504, 5, 8 + 17);
		tab.child(frame++, 22506, 44, 8 + 20);
		tab.child(frame++, 22508, 79, 11 + 19);
		tab.child(frame++, 22510, 116, 10 + 18);
		tab.child(frame++, 22512, 153, 9 + 20);
		tab.child(frame++, 22514, 5, 48 + 18);
		tab.child(frame++, 22516, 44, 47 + 21);
		tab.child(frame++, 22518, 79, 49 + 20);
		tab.child(frame++, 22520, 116, 50 + 19);
		tab.child(frame++, 22522, 154, 50 + 20);
		tab.child(frame++, 22524, 4, 84 + 21);
		tab.child(frame++, 22526, 44, 87 + 19);
		tab.child(frame++, 22528, 81, 85 + 20);
		tab.child(frame++, 22530, 117, 85 + 20);
		tab.child(frame++, 22532, 156, 87 + 18);
		tab.child(frame++, 22534, 5, 125 + 19);
		tab.child(frame++, 22536, 43, 124 + 19);
		tab.child(frame++, 22538, 83, 124 + 20);
		tab.child(frame++, 22540, 115, 125 + 21);
		tab.child(frame++, 22542, 154, 126 + 22);
		tab.child(frame++, 17201, 0, 22);
		tab.child(frame++, 17201, 0, 237);
		tab.child(frame++, 17202, 2, 25);
		tab.child(frame++, 17203, 41, 25);
		tab.child(frame++, 17204, 76, 25);
		tab.child(frame++, 17205, 113, 25);
		tab.child(frame++, 17206, 150, 25);
		tab.child(frame++, 17207, 2, 65);
		tab.child(frame++, 17208, 41, 65);
		tab.child(frame++, 17209, 76, 65);
		tab.child(frame++, 17210, 113, 65);
		tab.child(frame++, 17211, 150, 65);
		tab.child(frame++, 17212, 2, 102);
		tab.child(frame++, 17213, 41, 102);
		tab.child(frame++, 17214, 76, 102);
		tab.child(frame++, 17215, 113, 102);
		tab.child(frame++, 17216, 150, 102);
		tab.child(frame++, 17217, 2, 141);
		tab.child(frame++, 17218, 41, 141);
		tab.child(frame++, 17219, 76, 141);
		tab.child(frame++, 17220, 113, 141);
		tab.child(frame++, 17221, 150, 141);
		tab.child(frame++, 17235, 5, 5);
		tab.child(frame++, 17231, 0, 237);
		tab.child(frame++, 17232, 0, 237);
	}

}