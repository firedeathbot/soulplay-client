package com.soulplayps.client.interfaces;

import java.util.LinkedList;
import java.util.List;

public class InterfaceBuilder {

	protected final int id;
	public int nextChild;

	protected final RSInterface rsInterface;

	public InterfaceBuilder(int id, int startingChild) {
		this.id = id;
		nextChild = startingChild;
		rsInterface = RSInterface.addInterface(id);
	}

	public InterfaceBuilder(int id) {
		this(id, id + 1);
	}

	private final List<Component> components = new LinkedList<Component>();

	public void add(Component component) {
		components.add(component);
	}

	public int getNextChild() {
		return nextChild;
	}

	public void setNextChild(int nextChild) {
		this.nextChild = nextChild;
	}

	public RSInterface build() {
		rsInterface.totalChildren(components.size());
		int childIndex = 0;
		for (Component component : components) {
			rsInterface.child(childIndex++, component.getID(), component.getX(), component.getY());
		}
		return rsInterface;
	}

	public ComponentBuilder sprite(int spriteID) {
		int childID = nextChild++;
		RSInterface.addSprite(childID, spriteID);
		return new ComponentBuilder(this, childID);
	}
	
	public ComponentBuilder rectangle(int width, int height, int color, boolean fill, int trans, int clientCode) {
		int childID = nextChild++;
		RSInterface.addRectangle(childID, trans, color, fill, width, height, clientCode);
		return new ComponentBuilder(this, childID);
	}
	
	public ComponentBuilder sprite(int spriteID, int hoverSpriteId, int varId, int varValue, int varSpriteId, int width, int height, String text) {
		int childID = nextChild++;
		RSInterface.addSprite(childID, spriteID, varSpriteId, varId, varValue, hoverSpriteId, width, height, text, true);
		return new ComponentBuilder(this, childID);
	}

	public ComponentBuilder hover(String tip, int unhoveredID, int hoveredID, int a, int b) {
		int childID = nextChild++;
		int hoveredChild = nextChild;
		RSInterface.addHover(childID, a, b, hoveredChild, unhoveredID, tip);

		nextChild++;
		return hovered(new ComponentBuilder(this, childID), hoveredChild, hoveredID);
	}

	private ComponentBuilder hovered(ComponentBuilder hover, int hoveredChild, int hoveredID) {
		RSInterface.addHovered(hoveredChild, hoveredID, nextChild++);
		return new LinkedComponentBuilder(hover, hoveredChild);
	}

	public ComponentBuilder button(int spriteID, String tooltip, int hoveredID, int atAction, int width, int height) {
		int childID = nextChild++;
		RSInterface.addButton(childID, spriteID, tooltip, hoveredID, atAction, width, height);
		return new ComponentBuilder(this, childID);
	}

	public ComponentBuilder text(String text, int width, int idx, int color, boolean center, boolean shadow) {
		int childID = nextChild++;
		RSInterface.addText(childID, width, text, idx, color, center, shadow);
		return new ComponentBuilder(this, childID);
	}
	
	public ComponentBuilder text(String text, int idx, int color, boolean center, boolean shadow) {
		return text(text, 0, idx, color, center, shadow);
	}
	
	public ComponentBuilder model(int id, int type, int width, int height, int zoom, int rotationX,
			int rotationY, int animId, int clientCode) {
		int childID = nextChild++;
		RSInterface.addModel(childID, width, height, rotationX, rotationY, zoom, id, type, animId, clientCode);
		return new ComponentBuilder(this, childID);
	}
	
	public ComponentBuilder npcModel(int id, int width, int height, int zoom, int rotationX,
			int rotationY, int animId) {
		return model(id, 5, width, height, zoom, rotationX, rotationY, animId, 0);
	}
	
	public ComponentBuilder npcModel(int id, int width, int height, int zoom, int rotationX,
			int rotationY, int animId, int clientCode) {
		return model(id, 5, width, height, zoom, rotationX, rotationY, animId, clientCode);
	}

}
