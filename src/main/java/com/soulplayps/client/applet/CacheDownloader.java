package com.soulplayps.client.applet;

import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.data.SignLink;
//import net.lingala.zip4j.core.ZipFile;
//import net.lingala.zip4j.exception.ZipException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileLock;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.JOptionPane;

public class CacheDownloader {
	
	private Client client;
	
	public static int downloadPercent;
	public static boolean downloadingCache;

	private final int BUFFER = 1024;
	
	private String fileToExtract = SignLink.cacheDir + getArchivedName();
	
	public CacheDownloader(Client client) {
		this.client = client;
	}
	
	private void drawLoadingText(String text) {
		client.drawLoadingBar(35, text);
	}
	
	
	private void drawLoadingText(int amount, String text) {
		client.drawLoadingBar(amount, text);
	}
	
	private String getCacheLink() {
//		try {
//			if ((int)(Math.random()*3) == 0 && validResponse(Config.cacheLink2))
//				return Config.cacheLink2;
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		try {
			if (!validResponse(Config.cacheLink)) {
				return Config.cacheLink2;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Config.cacheLink;
	}
	
	public static boolean validResponse(String urlString) throws MalformedURLException, IOException {
		System.setProperty("http.agent", "Chrome");
	    URL u = new URL(urlString);
	    HttpURLConnection huc =  (HttpURLConnection)  u.openConnection();
	    huc.setRequestProperty("User-Agent", "Mozilla/4.76");
	    huc.setRequestMethod("HEAD");
	    huc.connect();
	    return huc.getResponseCode() == HttpURLConnection.HTTP_OK;
	}
	
	private int getCacheVersion() {
		return Config.CACHE_VERSION;
	}
	
	public CacheDownloader downloadCache() {
		try {
			File location = new File(SignLink.cacheDir);
			File version = new File(SignLink.cacheDir + "/cacheVersion" + getCacheVersion() + ".dat");
			
			boolean downloaded = false;
			
			if (!location.exists()) {
				//drawLoadingText("Downloading Cache Please wait...");
				downloaded = downloadFile(getCacheLink(), getArchivedName());
				
				unZip();
				System.out.println("UNZIP");
				
				if (downloaded) {
					BufferedWriter versionFile = new BufferedWriter(new FileWriter(version));
					versionFile.close();
				}
			} else {
				if (!version.exists()) {
					
					//scan for old version files
					for (int k = 0; k < 30; k++) {
						File versions = new File(SignLink.cacheDir + "/cacheVersion" + k + ".dat");
						if (versions.exists())
							versions.delete();
					}
					
					if (!lockInstance(SignLink.cacheDir + "/lock.dat")) {
						JOptionPane.showMessageDialog(null, "Cache is already updating on a different client.");
						System.exit(0);
						return null;
					}
					
					drawLoadingText("Downloading Cache Please wait...");
					downloaded = downloadFile(getCacheLink(), getArchivedName());
					
					unZip();
					System.out.println("UNZIP");
					
					if (downloaded) {
						BufferedWriter versionFile = new BufferedWriter(new FileWriter(version));
						versionFile.close();
					}
					
				} else {
					return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private boolean downloadFile(String adress, String localFileName) {

		OutputStream dest = null;
		HttpURLConnection download;
		InputStream in = null;
		
		try {
			
			URL url = new URL(adress);
			dest = new BufferedOutputStream(
					new FileOutputStream(SignLink.cacheDir + "/" + localFileName));
			
			download = (HttpURLConnection) url.openConnection();
			download.addRequestProperty("User-Agent", "Mozilla/4.76");
			in = download.getInputStream();
			
			byte[] data = new byte[BUFFER];
			
			int numRead;
			long numWritten = 0;
			int length = download.getContentLength();
			int lastPercent = 0;
			
			while ((numRead = in.read(data)) != -1) {
				dest.write(data, 0, numRead);
				numWritten += numRead;
				
//				if (numWritten % 2000000 < 1000) {
				downloadPercent = (int) (((double) numWritten / (double) length) * 100D);
				if (downloadPercent != lastPercent) {
					lastPercent = downloadPercent;
					drawLoadingText(downloadPercent, "Downloading Cache");
				}
//				}
			}
			
			System.out.println(localFileName + "\t" + numWritten);
			drawLoadingText("Unzipping...");
			return true;
			
		} catch (Exception exception) {
			exception.printStackTrace();
			return false;
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (dest != null) {
					dest.close();
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		
	}
	
	private String getArchivedName() {
		int lastSlashIndex = getCacheLink().lastIndexOf('/');
		if (lastSlashIndex >= 0
				&& lastSlashIndex < getCacheLink().length() - 1) {
			return getCacheLink().substring(lastSlashIndex + 1);
		} else {
			System.err.println("error retrieving archive name.");
		}
		return "";
	}
	
	
	private void unZip() {
		
		try {
			InputStream in =
					new BufferedInputStream(new FileInputStream(fileToExtract));
			ZipInputStream zin = new ZipInputStream(in);
			ZipEntry e;
			
			while ((e = zin.getNextEntry()) != null) {
				
				String fileName = e.getName();
				File newFile = new File(SignLink.cacheDir + File.separator + fileName);
				
				if (e.isDirectory()) {
					(new File(SignLink.cacheDir + e.getName())).mkdirs();
				} else {
					
					if (e.getName().equals(fileToExtract)) {
						unzip(zin, fileToExtract);
						break;
					}
					new File(newFile.getParent()).mkdirs();
					unzip(zin, SignLink.cacheDir + e.getName());
				}
				System.out.println("unzipping2 " + e.getName());
			}
			zin.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

//		try {
//	         ZipFile zipFile = new ZipFile(fileToExtract);
//	         zipFile.extractAll(getCacheDir());
//	    } catch (ZipException e) {
//	        e.printStackTrace();
//	    }
		
	}
	
	private void unzip(ZipInputStream zin, String s)
			throws IOException {
		
		FileOutputStream out = new FileOutputStream(s);
		//System.out.println("unzipping " + s);
		byte[] b = new byte[BUFFER];
		int len = 0;
		
		while ((len = zin.read(b)) != -1) {
			out.write(b, 0, len);
		}
		out.close();
	}

	private boolean lockInstance(final String lockFile) {
		try {
			final File file = new File(lockFile);
			final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
			final FileLock fileLock = randomAccessFile.getChannel().tryLock();
			if (fileLock != null) {
				Runtime.getRuntime().addShutdownHook(new Thread() {
					public void run() {
						try {
							fileLock.release();
							randomAccessFile.close();
							file.delete();
						} catch (Exception e) {
							System.out.println("unable to release lock");
							e.printStackTrace();
						}
					}
				});
				return true;
			}
		} catch (Exception e) {
			System.out.println("unable to lock file.");
			e.printStackTrace();
		}
		return false;
	}

}