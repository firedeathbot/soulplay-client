package com.soulplayps.client.applet;

import com.soulplayps.client.node.raster.Raster;
import com.soulplayps.client.opengl.GraphicsDisplay;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.WritableRaster;

public final class RSImageProducer {

	public final int[] canvasRaster;
	public final int canvasWidth;
	public final int canvasHeight;
	private final BufferedImage bufferedImage;
	private final Component component;

	public RSImageProducer(int canvasWidth, int canvasHeight, Component c) {
		this.canvasWidth = canvasWidth;
		this.canvasHeight = canvasHeight;
		if (GraphicsDisplay.enabled) {
			canvasRaster = null;
			bufferedImage = null;
			component = null;
			return;
		}

		canvasRaster = new int[canvasWidth * canvasHeight];
		DataBufferInt databufferint = new DataBufferInt(canvasRaster, canvasRaster.length);
		DirectColorModel directcolormodel = new DirectColorModel(32, 16711680, 65280, 255);
		WritableRaster writableraster = java.awt.image.Raster.createWritableRaster(directcolormodel.createCompatibleSampleModel(canvasWidth, canvasHeight), databufferint, null);
		bufferedImage = new BufferedImage(directcolormodel, writableraster, false, null);
		component = c;
		initDrawingArea();
	}

	public void drawGraphics(Graphics graphics, int x, int y) {
		if (GraphicsDisplay.enabled) {
			return;
		}

		try {
			graphics.drawImage(bufferedImage, x, y, component);
		} catch (Exception var5) {
			component.repaint();
		}
	}

	public void initDrawingArea() {
		Raster.initDrawingArea(canvasHeight, canvasWidth, canvasRaster);
	}
}