package com.soulplayps.client.applet;

public enum Links {

	DISCORD("Discord", "https://discord.gg/jSBB6VZr98"),
	DONATE("Donate", "http://soulplayps.com/store"),
	FACEBOOK("Facebook", "https://www.facebook.com/SoulPlayps/"),
	FORUMS,
	HISCORES,
	HOME,
	VOTE,
	YOUTUBE("YouTube", "https://www.youtube.com/channel/UC1DZmIg_SdsxVVaFoYW51MQ");

	public final String name, icon, link;

	Links() {
		this.name = name().charAt(0) + name().substring(1).toLowerCase();
		this.icon = name.toLowerCase() + ".png";
		this.link = "http://soulplayps.com/" + name.toLowerCase();
	}

	Links(String name) {
		this.name = name;
		this.icon = name.toLowerCase() + ".png";
		this.link = "http://soulplayps.com/" + name.toLowerCase();
	}

	Links(String name, String link) {
		this.name = name;
		this.icon = name.toLowerCase() + ".png";
		this.link = link;
	}

}
