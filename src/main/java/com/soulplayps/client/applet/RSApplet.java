package com.soulplayps.client.applet;

import com.soulplayps.client.CanvasWrapper;
import com.soulplayps.client.Client;
import com.soulplayps.client.Config;
import com.soulplayps.client.ConfigCodes;
import com.soulplayps.client.ErrorHandler;
import com.soulplayps.client.interfaces.CustomInterfaces;
import com.soulplayps.client.node.raster.sprite.Sprite;
import com.soulplayps.client.node.raster.sprite.SpriteCache;
import com.soulplayps.client.opengl.GraphicsDisplay;
import com.soulplayps.client.settings.Settings;
import com.soulplayps.client.settings.SettingsManager;
import com.soulplayps.client.util.HotKeys;
import com.soulplayps.client.util.HotkeyIcon;
import com.soulplayps.client.util.Misc;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.IntBuffer;
import java.util.concurrent.atomic.AtomicInteger;

import javax.imageio.ImageIO;
import javax.swing.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import static com.soulplayps.client.Client.clientHeight;
import static com.soulplayps.client.Client.clientWidth;

public class RSApplet extends Applet implements Runnable, MouseListener,
		MouseMotionListener, MouseWheelListener, KeyListener, FocusListener,
		WindowListener {

	private static final String ICON_RESOURCE = "/icons/";
	private static final long serialVersionUID = 1473917011474991756L;
	
	public final void createClientFrame(int i, int j) {
		clientWidth = canvasWidth = j;
		clientHeight = canvasHeight = i;
		createFrame(false, false);
		startRunnable(this, 1, "Applet thread");
	}

	private final Point hotspot = new Point(0, 0);

	public void setCursor(Sprite sprite) {
		if (GraphicsDisplay.enabled) {
			try {
				Sprite newSprite = sprite.flipVertical();
				IntBuffer buffer = BufferUtils.createIntBuffer(newSprite.myWidth * newSprite.myHeight);
				buffer.put(newSprite.myPixels);
				buffer.rewind();

				org.lwjgl.input.Cursor cursor = new org.lwjgl.input.Cursor(newSprite.myWidth, newSprite.myHeight, 0,
						newSprite.myHeight - 1, 1, buffer, null);
				Mouse.setNativeCursor(cursor);
			} catch (LWJGLException e) {
				e.printStackTrace();
			}
		} else {
			BufferedImage image = new BufferedImage(sprite.myWidth, sprite.myHeight, BufferedImage.TYPE_INT_ARGB);
			image.setRGB(0, 0, sprite.myWidth, sprite.myHeight, sprite.myPixels, 0, sprite.myWidth);
			canvas.setCursor(canvas.getToolkit().createCustomCursor(image, hotspot, null));
		}
	}

	public void removeCursor() {
		if (GraphicsDisplay.enabled) {
			try {
				Mouse.setNativeCursor(null);
			} catch (LWJGLException e) {
				e.printStackTrace();
			}
		} else {
			canvas.setCursor(null);
		}
	}

	public synchronized void addCanvas() {
		if (canvas != null) {
			canvas.removeFocusListener(this);
			frame.remove(canvas);
		}

		canvas = new CanvasWrapper(this);
		frame.add(canvas);
		canvas.setBackground(Color.BLACK);
		canvas.setSize(canvasWidth, canvasHeight);
		canvas.setVisible(true);
		canvas.addFocusListener(this);
		canvas.requestFocus();
		fullRedraw = true;
		
		loadingTextFontMetrics = canvas.getFontMetrics(loadingTextFont);
		canvas.setFocusTraversalKeysEnabled(false);
	}
	
	public final void createFrame(boolean resizable, boolean undecorated) {
		frame = new JFrame();

		try {
		    InputStream is = RSApplet.class.getResourceAsStream("/icons/icon.png");
            frame.setIconImage(ImageIO.read(is));
            is.close();
        } catch (Exception ex) {
		    ex.printStackTrace();
        }

		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setTitle(Config.SERVER_NAME);
		frame.setResizable(resizable);
		frame.setUndecorated(undecorated);

		screenshot.setActionCommand("Screenshot");
		screenshot.setFocusable(false);
		screenshot.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Client.instance.screenShot();
					}
				});
			}
		});
		
		settings.setFocusable(false);
		settings.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (settingsManager != null && settingsManager.isVisible()) {
							settingsManager.toFront();
						} else {
							settingsManager = new SettingsManager();
							settingsManager.setVisible(true);
						}
					}
				});
			}
		});

		menuItemExit.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				promptClientClose();
			}
		});

		// adding menubar
		menuBar.add(menu);
		frame.setJMenuBar(menuBar);
		menuBar.setVisible(true);
		JMenuItem viewScreenshotBtn = new JMenuItem("View Screenshots");

			try {
				InputStream is = RSApplet.class.getResourceAsStream(ICON_RESOURCE + "view_screenshots.png");
				viewScreenshotBtn.setIcon(new ImageIcon(ImageIO.read(is)));
				is.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		viewScreenshotBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Settings.getSetting("screenshot_dir") == null) {
					Client.instance.screenShot();
					return;
				}

				if (!Desktop.isDesktopSupported()) {
					return;
				}

				String dir = (String)Settings.getSetting("screenshot_dir");

				File screenshotDir = new File(dir);
				if (!screenshotDir.exists()) {
					return;
				}

				try {
					Desktop.getDesktop().open(screenshotDir);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

		menu.add(viewScreenshotBtn);

		menu.add(menuItemExit);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(linksMenu);
		
		for (final Links link : Links.values()) {
			try {
				JMenuItem menuItem = new JMenuItem(link.name);
				try {
                    InputStream is = RSApplet.class.getResourceAsStream(ICON_RESOURCE + link.icon);
                    menuItem.setIcon(new ImageIcon(ImageIO.read(is)));
                    is.close();
                } catch (Exception ex) {
				    ex.printStackTrace();
                }

				menuItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								try {
									if (!Desktop.isDesktopSupported()) {
										return;
									}

									Desktop.getDesktop().browse(new URL(link.link).toURI());
								} catch (Exception e1) {
									e1.printStackTrace();
								}
							}
						});
					}
				});
				
				linksMenu.add(menuItem);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}

		try {
			InputStream is = RSApplet.class.getResourceAsStream(ICON_RESOURCE + "screenshot.png");
			screenshot.setIcon(new ImageIcon(ImageIO.read(is)));
			screenshot.setText("");
			screenshot.setFocusPainted(false);
			is.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		menuBar.add(screenshot);

		try {
			InputStream is = RSApplet.class.getResourceAsStream(ICON_RESOURCE + "settings.png");
			settings.setIcon(new ImageIcon(ImageIO.read(is)));
			settings.setText("");
			settings.setFocusPainted(false);
			is.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		menuBar.add(settings);
		frame.setVisible(true);
		frame.toFront();
		int menuOffsetY = menuBar.isVisible() ? menuBar.getHeight() : 0;
		Insets insets = frame.getInsets();
		Dimension size = new Dimension(insets.left + clientWidth + insets.right, insets.bottom + clientHeight + insets.top + menuOffsetY);
		frame.setMinimumSize(size);
		frame.setSize(size);
		
		frame.setLocationRelativeTo(null);
	}
	
	public final void initClientFrame(int i, int j) {
		clientWidth = canvasWidth = j;
		clientHeight = canvasHeight = i;
		startRunnable(this, 1, "Applet thread");
		setFocusTraversalKeysEnabled(false);
	}
	
	public void initClientFrame2(int i, int j) {
		if (GraphicsDisplay.enabled) {
			return;
		}
		
		clientWidth = canvasWidth = j;
		clientHeight = canvasHeight = i;
		graphics = canvas.getGraphics();
		fullGameScreen = new RSImageProducer(clientWidth, clientHeight, canvas);
		//startRunnable(this, 1);
		//setFocusTraversalKeysEnabled(false);
	}

	public void mouseWheelMoved(MouseWheelEvent event) {
		mouseWheelMoved(event.getWheelRotation());
	}
	
	public void mouseWheelMoved(int rotation) {
		if (Client.variousSettings[ConfigCodes.WHEEL_SCROLLING] == 0) {
			boolean canScroll = true;

			if (Client.isFixed()) {
				if (mouseX < 4 || mouseY < 4 || mouseX >= 512 || mouseY >= 334 || Client.openModalInterfaceId != -1) {
					canScroll = false;
				}
			} else {
				int x = clientWidth / 2 - 356;
				int y = clientHeight / 2 - 267;
				if (!Client.instance.canClick(x, y)) {
					canScroll = false;
				}
			}

			if (canScroll) {
				int diff = rotation == -1 ? -150 : 150;
				Client.cameraZoom += diff;
				Client.cameraZoom = Math.min(Math.max(Client.cameraZoom, 251), 2000);
				int newPosition = Misc.interpolate(0, CustomInterfaces.scrollLayer.width - CustomInterfaces.scrollLayer.dynamicComponents[4].width, 201, 2100, Client.cameraZoom);
				if (newPosition < 3) {
					newPosition = 3;
				}

				CustomInterfaces.scrollLayer.dynamicComponents[4].x = newPosition;
			}
		}

		notches += rotation;
	}
	
	public void addListeners() {
		graphics = canvas.getGraphics();
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addKeyListener(this);
		canvas.addFocusListener(this);
		canvas.addMouseWheelListener(this);
	}
	
	public void removeListeners() {
		graphics = null;
		canvas.removeMouseListener(this);
		canvas.removeMouseMotionListener(this);
		canvas.removeKeyListener(this);
		canvas.removeFocusListener(this);
		canvas.removeMouseWheelListener(this);
	}
	
	@Override
	public void run() {
		addCanvas();
		
		loadSettingsClass();
		
		if (GraphicsDisplay.enabled) {
			Client c = (Client) this;
			GraphicsDisplay.getInstance().initialize(c);
		}
		
		Client.applySettings();
		
		if (!GraphicsDisplay.enabled) {
			addListeners();
			// drawLoadingText(0, "Loading...");
		}

		if (frame != null)
			frame.addWindowListener(this);
		startUp();
		
		loop();
	}
	
	int i = 0;
	int j = 256;
	int k = 1;
	int i1 = 0;
	int j1 = 0;
	
	private void loop() {
		try {
			for (int k1 = 0; k1 < 10; k1++)
				aLongArray7[k1] = System.currentTimeMillis();
			
			while (anInt4 >= 0) {
				if (anInt4 > 0) {
					anInt4--;
					if (anInt4 == 0) {
						exit(true);
						return;
					}
				}
				
				if (GraphicsDisplay.enabled) {
					GraphicsDisplay.getInstance().handleEvents(this);
				}
				softwareLoop();
			}
		} catch(Exception e) {
			ErrorHandler.submitError("mainloop", e);
			exit(true);
		}

		if (anInt4 == -1)
			exit(true);
	}

	private void softwareLoop() {
		int i2 = j;
		int j2 = k;
		j = 300;
		k = 1;
		long l1 = System.currentTimeMillis();
		if (aLongArray7[i] == 0L) {
			j = i2;
			k = j2;
		} else if (l1 > aLongArray7[i])
			j = (int) (2560 * delayTime / (l1 - aLongArray7[i]));
		if (j < 25)
			j = 25;
		if (j > 256) {
			j = 256;
			k = (int) (delayTime - (l1 - aLongArray7[i]) / 10L);
		}
		if (k > delayTime)
			k = delayTime;
		aLongArray7[i] = l1;
		i = (i + 1) % 10;
		if (k > 1) {
			for (int k2 = 0; k2 < 10; k2++)
				if (aLongArray7[k2] != 0L)
					aLongArray7[k2] += k;
			
		}
		if (k < minDelay)
			k = minDelay;
		try {
			Thread.sleep(k);
		} catch (InterruptedException _ex) {
			j1++;
		}
		for (; i1 < 256; i1 += j) {
			clickMode3 = clickMode1.get();
			saveClickX = clickX.get();
			saveClickY = clickY.get();
			aLong29 = clickTime;
			clickMode1.set(0);
			clickMode2 = clickMode2Atomic.get();
			processGameLoop();
			readIndex = writeIndex;
		}
		
		i1 &= 0xff;
		if (delayTime > 0)
			fps = (1000 * j) / (delayTime * 256);
		processDrawing();
		// System.out.println("amount:"+Client.chatDrawAmount);
		// Client.chatDrawAmount = 0;
		if (shouldDebug) {
			System.out.println("ntime:" + l1);
			for (int l2 = 0; l2 < 10; l2++) {
				int i3 = ((i - l2 - 1) + 20) % 10;
				System.out.println("otim" + i3 + ":" + aLongArray7[i3]);
			}
			
			System.out.println("fps:" + fps + " ratio:" + j + " count:" + i1);
			System.out.println("del:" + k + " deltime:" + delayTime + " mindel:" + minDelay);
			System.out.println("intex:" + j1 + " opos:" + i);
			shouldDebug = false;
			j1 = 0;
		}
	}
	
	private void exit(boolean mainThread) {
		anInt4 = -2;
		cleanUpForQuit();
		if (frame != null) {
			if (GraphicsDisplay.enabled && mainThread) {
				Display.destroy();
			}

			try {
				System.exit(0);
			} catch (Throwable _ex) {
				_ex.printStackTrace();
			}
		}
	}
	
	public final void method4(int i) {
		delayTime = 1000 / i;
	}
	
	@Override
	public final void start() {
		if (anInt4 >= 0)
			anInt4 = 0;
	}
	
	@Override
	public final void stop() {
		if (anInt4 >= 0)
			anInt4 = 4000 / delayTime;
	}
	
	@Override
	public final void destroy() {
		anInt4 = -1;
		try {
			Thread.sleep(5000L);
		} catch (Exception _ex) {
			_ex.printStackTrace();
		}
		if (anInt4 == -1) {
			exit(false);
		}
	}
	
	@Override
	public final void update(Graphics g) {
		paint(g);
	}
	
	@Override
	public final void paint(Graphics g) {
		if (graphics == null)
			graphics = g;
		shouldClearScreen = true;
		raiseWelcomeScreen();
	}

    private int getButtonId(MouseEvent mouseEvent) {
        int button = mouseEvent.getButton();
        return !mouseEvent.isAltDown() && button != 2?(!mouseEvent.isMetaDown() && button != 3?1:2):3;
    }

	public final void mousePressed(MouseEvent event) {
		mousePressed(event.getX(), event.getY(), getButtonId(event));
	}

	public final void mousePressed(int i, int j, int type) {
		idleTime = 0;
		clickX.set(i);
		clickY.set(j);
		clickTime = System.currentTimeMillis();
		clickMode1.set(type);
		if (clickMode1.get() != 0) {
			clickMode2Atomic.set(clickMode1.get());
		}
	}

	public final void mouseReleased(MouseEvent mouseevent) {
		mouseReleased();
	}
	
	public final void mouseReleased() {
		idleTime = 0;
		clickMode2Atomic.set(0);
	}
	
	public final void mouseClicked(MouseEvent mouseevent) {
	}
	
	public final void mouseEntered(MouseEvent mouseevent) {
	}
	
	public final void mouseExited(MouseEvent mouseevent) {
		idleTime = 0;
		mouseX = -1;
		mouseY = -1;
	}
	
	public final void mouseDragged(MouseEvent e) {
		idleTime = 0;
		mouseX = e.getX();
		mouseY = e.getY();
//		clickType = DRAG;
	}

//	@Override
//	public final void mouseDragged(MouseEvent mouseevent) {
//		int i = mouseevent.getX();
//		int j = mouseevent.getY();
//		if (frame != null) {
//			i -= 4;
//			j -= 22;
//		}
//		if (System.currentTimeMillis() - clickTime >= 250L
//				|| Math.abs(saveClickX - i) > 5 || Math.abs(saveClickY - j) > 5) {
//			idleTime = 5;
//			mouseX = i;
//			mouseY = j;
//		}
//	}
	
	public final void mouseMoved(MouseEvent mouseevent) {
		mouseMoved(mouseevent.getX(), mouseevent.getY(), 0, 0);
	}
	
	public int deltaX;
	public int deltaY;
	
	public final void mouseMoved(int i, int j, int dx, int dy) {
		idleTime = 0;
		mouseX = i;
		mouseY = j;
		deltaX = dx;
		deltaY = dy;
	}
	
	
	public final void keyPressed(KeyEvent keyevent) {
		keyPressed(keyevent.getKeyCode(), keyevent.getKeyChar());
	}
	
	public final void keyPressed(int i, int j) {
		idleTime = 0;
		switch (i) {
			case KeyEvent.VK_ESCAPE:
				if (Client.getInputDialogState() == Client.SEARCH_STATE) {
					Client.instance.setVarp(ConfigCodes.OPEN_CHATBOX_INTERFACE, 0);
					Client.instance.openChatInterface(Client.SEARCH_STATE);
				} else if (Client.instance.messagePromptRaised) {
					Client.instance.messagePromptRaised = false;
					Client.inputTaken = true;
				} else if (Client.openModalInterfaceId == -1 && (Client.HOT_KEYS != null && !Client.HOT_KEYS.isVisible())) {
					Client.setTab(3);
				} else if (HotKeys.ESC_CLOSE) {
					if (Client.openModalInterfaceId != -1) {
						Client.closeInterfacesTransmit();
					}
					return;
				}
				break;
			case KeyEvent.VK_SHIFT:
				Client.holdingShiftKey = true;
				break;
			case KeyEvent.VK_CONTROL:
				Client.holdingCtrlKey = true;
				break;
		}

		if (HotKeys.ICONS != null) {
			for (int a = HotKeys.ICONS.size() - 1; a >= 0; a--) {
				HotkeyIcon icon = HotKeys.ICONS.get(a);
				if (icon.key == i) {
					handleHotKey(icon.tabId);
					break;
				}
			}
		}

		if (j < 30)
			j = 0;
		if (i == 37)
			j = 1;
		if (i == 39)
			j = 2;
		if (i == 38)
			j = 3;
		if (i == 40)
			j = 4;
		if (i == 17)
			j = 5;
		if (i == 8)
			j = 8;
		if (i == 127)
			j = 8;
		if (i == 9)
			j = 9;
		if (i == 10)
			j = 10;
		if (i >= 112 && i <= 123)
			j = (1008 + i) - 112;
		if (i == 36)
			j = 1000;
		if (i == 35)
			j = 1001;
		if (i == 33)
			j = 1002;
		if (i == 34)
			j = 1003;
		if (j >= 0 && j < 128)
			keyArray[j] = 1;
		if (j > 4) {
			charQueue[writeIndex] = j;
			writeIndex = writeIndex + 1 & 0x7f;
		}
	}
	
	public final void keyReleased(KeyEvent keyevent) {
		keyReleased(keyevent.getKeyCode(), keyevent.getKeyChar());
	}
	
	public final void keyReleased(int i, char c) {
		idleTime = 0;
		if (c < '\036')
			c = '\0';
		if (i == 37)
			c = '\001';
		if (i == 39)
			c = '\002';
		if (i == 38)
			c = '\003';
		if (i == 40)
			c = '\004';
		if (i == 17)
			c = '\005';
		if (i == 8)
			c = '\b';
		if (i == 127)
			c = '\b';
		if (i == 9)
			c = '\t';
		if (i == 10)
			c = '\n';
		if (c > 0 && c < '\200')
			keyArray[c] = 0;
		if (i == KeyEvent.VK_SHIFT) {
			Client.holdingShiftKey = false;
		} else if (i == KeyEvent.VK_CONTROL) {
			Client.holdingCtrlKey = false;
		}
	}
	
	public final void keyTyped(KeyEvent keyevent) {
	}
	
	public final int readChar() {
		int k = -1;
		if (writeIndex != readIndex) {
			k = charQueue[readIndex];
			readIndex = readIndex + 1 & 0x7f;
		}
		return k;
	}
	
	public final void focusGained(FocusEvent focusevent) {
		awtFocus = true;
		shouldClearScreen = true;
		raiseWelcomeScreen();
	}
	
	public final void focusLost(FocusEvent focusevent) {
		awtFocus = false;
		for (int i = 0; i < 128; i++)
			keyArray[i] = 0;
	}
	
	public final void windowActivated(WindowEvent windowevent) {
	}
	
	public final void windowClosed(WindowEvent windowevent) {
	}

	public final void windowClosing(WindowEvent windowevent) {
		promptClientClose();
	}

	public void promptClientClose() {
		final int result = Client.loggedIn ? JOptionPane.showConfirmDialog(frame, "Are you sure you wish to exit?", Config.SERVER_NAME, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) : JOptionPane.YES_OPTION;

		if (result == JOptionPane.YES_OPTION) {
			if (Client.loggedIn) {
				Client.instance.forceClosed();
			}

			destroy();
		}
	}
	
	public final void windowDeactivated(WindowEvent windowevent) {
	}
	
	
	public final void windowDeiconified(WindowEvent windowevent) {
	}
	
	
	public final void windowIconified(WindowEvent windowevent) {
	}
	
	
	public final void windowOpened(WindowEvent windowevent) {
	}
	
	public void startUp() {
	}

	public void processGameLoop() {
	}
	
	public void cleanUpForQuit() {
	}
	
	public void processDrawing() {
	}
	
	public void raiseWelcomeScreen() {
	}
	
	public void handleHotKey(int keyId) {
	}
	
	public void loadSettingsClass() {
	}
	
	public void startRunnable(Runnable runnable, int priority, String name) {
		Thread thread = new Thread(runnable, name);
		thread.setDaemon(true);
		thread.start();
		thread.setPriority(priority);
	}
	
	private Color color = new Color(255, 194, 89);
	private Font loadingTextFont = new Font("Helvetica", 1, 13);
	private FontMetrics loadingTextFontMetrics;
	
	public void clearLoading() {
		color = null;
		loadingTextFont = null;
		loadingTextFontMetrics = null;
		SpriteCache.background = null;
	}
	
	public void drawLoadingBar(int progress, String text) {
		if (GraphicsDisplay.enabled) {
			Client.drawLoadingHD(text, progress);
			return;
		}
		
		while (graphics == null) {
			graphics = canvas.getGraphics();
			try {
				canvas.repaint();
			} catch (Exception _ex) {
				_ex.printStackTrace();
			}
			try {
				Thread.sleep(1000L);
			} catch (Exception _ex) {
				_ex.printStackTrace();
			}
		}
		
		if (shouldClearScreen) {
			graphics.setColor(Color.black);
			graphics.fillRect(0, 0, clientWidth, clientHeight);
			shouldClearScreen = false;
		}
		
		if (SpriteCache.background != null) {
			graphics.drawImage(SpriteCache.background, 0, 0, null);
		}
		
		graphics.setColor(color);
		int rectHeight = 18;
		graphics.drawRect(clientWidth / 2 - 152, clientHeight / 2 - rectHeight/*475*/, 304, 20);
		graphics.fillRect(clientWidth / 2 - 150, clientHeight / 2 + 2 - rectHeight/*477*/, progress * 3, 17);
		graphics.setColor(Color.black);
		graphics.fillRect((clientWidth / 2 - 150) + progress * 3, clientHeight / 2 + 1 - rectHeight/*476*/, 300 - progress * 3, rectHeight);
		graphics.setFont(loadingTextFont);
		graphics.setColor(Color.DARK_GRAY);
		graphics.drawString(text + " - " + progress + "%", (clientWidth - loadingTextFontMetrics.stringWidth(text)) / 2, clientHeight / 2 + 15 - rectHeight/*490*/);
	}
	
	public RSApplet() {
		delayTime = 20;
		minDelay = 1;
		aLongArray7 = new long[10];
		shouldDebug = false;
		shouldClearScreen = true;
		awtFocus = true;
		keyArray = new int[128];
		charQueue = new int[128];
	}
	
	public int anInt4;
	private int delayTime;
	public int minDelay;
	private final long[] aLongArray7;
	public static int fps;
	public boolean shouldDebug;
	public int canvasWidth;
	public int canvasHeight;
	public Graphics graphics;
	public RSImageProducer fullGameScreen;
	public JFrame frame;
	public Canvas canvas;
	private boolean shouldClearScreen;
	public boolean awtFocus;
	public int idleTime;
	public int clickMode2;
	public AtomicInteger clickMode2Atomic = new AtomicInteger(0);
	public int mouseX;
	public int mouseY;
	private AtomicInteger clickMode1 = new AtomicInteger(0);
	protected AtomicInteger clickX = new AtomicInteger(0);
	protected AtomicInteger clickY = new AtomicInteger(0);
	private volatile long clickTime;
	public int clickMode3;
	public AtomicInteger clickMode3Atomic = new AtomicInteger(0);
	public int saveClickX;
	public int saveClickY;
	public long aLong29;
	public final int[] keyArray;
	private final int[] charQueue;
	private int readIndex;
	private volatile int writeIndex;
	public static int anInt34;
	public volatile boolean fullRedraw;
	public JMenuBar menuBar = new JMenuBar();
	private static JMenu menu = new JMenu("File");
	private static JButton settings = new JButton("Settings");
	private static JMenuItem menuItemExit = new JMenuItem("Exit");
	private static JButton screenshot = new JButton("Screenshot");
	private static JMenu linksMenu = new JMenu("Links");
	public boolean isFullScreenSupported = true;
	public static DisplayMode originalDisplayMode;
	public static GraphicsDevice device;
	private SettingsManager settingsManager = null;
	public int notches;
	
	public int getYStart() {
		Insets insets = frame.getInsets();
		return insets.top + insets.bottom + (menuBar != null && menuBar.isVisible() ? menuBar.getHeight() : 0) + frame.getY();
	}
	
}
