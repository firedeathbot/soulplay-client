package com.soulplayps.client;

public enum EntityAttackOptionSetting {

	COMBAT_DEPEDANT(0),
	ALWAYS_RIGHT_CLICK(1),
	LEFT_CLICK_WHERE_AVAILABLE(2),
	HIDDEN(3),
	LEFT_CLICK_ATTACKING_ONLY(4);

	private final int id;

	private EntityAttackOptionSetting(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static EntityAttackOptionSetting find(int id) {
		switch (id) {
			case 0:
				return COMBAT_DEPEDANT;
			case 1:
				return ALWAYS_RIGHT_CLICK;
			case 2:
				return LEFT_CLICK_WHERE_AVAILABLE;
			case 3:
				return HIDDEN;
			case 4:
				return LEFT_CLICK_ATTACKING_ONLY;
			default:
				throw new RuntimeException();
		}
	}

}
